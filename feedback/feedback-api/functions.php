<?php
	// Parâmetro da URL
	$id = strtoupper($_GET["id"]);
	
	// Resultado
	$result = '-2';
	
	// Conexão com o banco de dados
	{
		include($_SERVER['DOCUMENT_ROOT'].'/franquias/application/controllers/reisdev-api/dao.php');
		$array = DAO::Get("feedbacks", "`idFeedbacks`='".$id."'");
	}
	
	// Variáveis de resultado
	if ($array == null)
		$result = '-1';
	else
	{
		$status = $array['status'];
		$clienteId = $array['cliente_id'];
		$idFranquia = $array['idFranquia'];
		$os = $array['os'];
		$valor = $array['valor'];
		$result = '0';
		
		$nomeFranquia = DAO::Get("franquias", "`idFranquias`='".$idFranquia ."'")['nome'];
	}
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript">
	var result =  "<?php echo $result; ?>";
	var id =  "<?php echo $id; ?>";
	switch (result)
	{
		case '-2': // Sem comunicação com o servidor
		case '-1': // ID inválido
			document.write
			(
				"<div class=\"item\ id=\"pnWork\">" +
					"<br/>" +
					"<br/>" +
					"<label><h2> Desculpe. Não encontramos este ID em nossos servidores. </h2></label>" +
					"<br/>" +
					"<label><h3> Obriado por colaborar. </h3></label>" +
				"</div>"
			);
			break;
			
		case '0': // Achou
			var maxlength = 500;
			var status =  "<?php echo $status; ?>";
			var clienteId =  "<?php echo $clienteId; ?>";
			var idFranquia =  "<?php echo $idFranquia; ?>";
			var nomeFranquia =  "<?php echo $nomeFranquia; ?>";
			var os =  "<?php echo $os; ?>";
			var valor =  "<?php echo $valor; ?>";
			switch (status)
			{
				case '1': // Já avaliado
					document.write
					(
						"<div class=\"item\" id=\"pnWork\">" +
							"<br/>" +
							"<br/>" +
							"<label><h2> Desculpe. Este ID já foi avalido. </h2></label>" +
							"<br/>" +
							"<label><h3> Obriado por colaborar. </h3></label>" +
						"</div>"
					);
					break;
					
				case '0': // Avaliar
					document.write
					(
						"<div class=\"item\" id=\"pnWork\">" +
							"<br/>" +
							"<label> Dados da franquia: </label>" +
							"<br/>" +
							"<input class=\"textbox\" value=\""+ idFranquia + " - " + nomeFranquia + "\" disabled></input>" +
							"<br/>" +
							"<br/>" +
							"<label> Qual o seu nível de satisfação com nosso atendimento? </label>" +
							"<br/>" +
							"<div><h4>" +
								"<div class=\"rating\">" +
										"<input id=\"star5\" name=\"star\" type=\"radio\" value=\"5\" class=\"radio-btn hide\" />" +
										"<label for=\"star5\">☆</label>" +
										"<input id=\"star4\" name=\"star\" type=\"radio\" value=\"4\" class=\"radio-btn hide\" />" +
										"<label for=\"star4\">☆</label>" +
										"<input id=\"star3\" name=\"star\" type=\"radio\" value=\"3\" class=\"radio-btn hide\" />" +
										"<label for=\"star3\">☆</label>" +
										"<input id=\"star2\" name=\"star\" type=\"radio\" value=\"2\" class=\"radio-btn hide\" />" +
										"<label for=\"star2\">☆</label>" +
										"<input id=\"star1\" name=\"star\" type=\"radio\" value=\"1\" class=\"radio-btn hide\" />" +
										"<label for=\"star1\">☆</label>" +
								"</div>" +
							"</h4></div>" +
							"<br/>" +
							"<br/>" +
							"<label> Diga-nos com podemos melhorar? </label>" +
							"<textarea name=\"tbMessage\" id=\"tbMessage\" class=\"textbox\" maxlength=\""+maxlength+"\"></textarea>" +
							"<label for=\"tbMessage\" id=\"count\" class=\"count\"> "+maxlength+" caractere(s) </label>" +
							"<br/>" +
							"<br/>" +
    							"<button class=\"save\" id=\"btn\" onclick=\"sendFeedback();\">Enviar avaliação</button>" +
						"</div>"
					);
					break;
			}				
			break;
		
		default: // indefinido
			document.write("\< null >");
			break;
	}

	var star = 0;
    	function sendFeedback()
    	{
	    	var valor = document.getElementById("tbMessage").value;
	    	if (star == 0)
	        	alert("Insira uma avaliação selecionando as estrelas.");
	        	
	    	else if (valor == "")
	        	alert("Insira uma mensagem.");
	        		        	
	    	else
	    	{
		      $.ajax(
		      {
		        	url:"./feedback-api/update.php",
		        	data: { id:"<?php echo $id; ?>" , s:star, v:valor },
		        	success:function(result)
		        	{
		        		var _div = document.getElementById('pnWork');
		        		_div.innerHTML = "<div class=\"item\" id=\"pnWork\">" +
							  	"<br/>" +
								"<br/>" +
								"<label><h2> Avaliação enviada com sucesso. </h2></label>" +
								"<br/>" +
								"<label><h3> Obriado por colaborar. </h3></label>" +
							 "</div>";
		       		}
		     });
		}
    	}
	$(document).ready(function()
	{
		$(".rating input:radio").attr("checked", false);
		$('.rating input').click(function ()
		{
			$(".rating span").removeClass('checked');
			$(this).parent().addClass('checked');
		});
		
		$('input:radio').change(function()
		{
		        var userRating = this.value;
		        star = userRating;
		}); 
		
		$('textarea').keyup(function()
		{
		        var len = this.value.length;
        		var _div = document.getElementById('count');
        		_div.innerHTML = (maxlength - len) +" caractere(s)";
        		
		});
	});
</script>