<!DOCTYPE html>
<html>
	<head>
		<title>
			Feedback - ASSISTÊNCIA TÉCNICA ONLINE
		</title>
		<link rel="shortcut icon" href="../assets/img/logo-small.png">
		<style type="text/css">@import url("feedback-api/styles.css");</style>
		<meta charset="utf-8"/>
	</head>
	
	<body class="form">
		<div class="header">
			   <h1>Formulário de avaliação</h1>
		</div>
		<div class="body">
			<?php include_once('./feedback-api/functions.php'); ?>
		</div>
		<div class="footer">
			<h5><?php echo date('Y'); ?> &copy; ASSISTÊNCIA TÉCNICA ONLINE</h5>
		</div>
	</body>
</html>