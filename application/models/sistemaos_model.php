<?php

class Sistemaos_model extends CI_Model {



    /**

     * author: Ramon Silva 

     * email: silva018-mg@yahoo.com.br

     * 

     */

    

    function __construct() {

        parent::__construct();

    }



    

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){



        $this->db->select($fields);

        $this->db->from($table);

        $this->db->limit($perpage,$start);

        if($where){

            $this->db->where($where);

        }

        

        $query = $this->db->get();

        

        $result =  !$one  ? $query->result() : $query->row();

        return $result;

    }



    function getById($id){

        $this->db->from('franquias');

        $this->db->select('franquias.*, permissoes.nome as permissao');

        $this->db->join('permissoes', 'permissoes.idPermissao = franquias.permissoes_id', 'left');

        $this->db->where('idFranquias',$id);

        $this->db->limit(1);

        return $this->db->get()->row();

    }



    public function alterarSenha($senha,$oldSenha,$id){



        $this->db->where('idFranquias', $id);

        $this->db->limit(1);

        $franquia = $this->db->get('franquias')->row();



        if($franquia->senha != $oldSenha){

            return false;

echo $id;



        }

        else{

            $this->db->set('senha',$senha);

            $this->db->where('idFranquias',$id);

            return $this->db->update('franquias');    

        }



        

    }



    function pesquisar($termo){

         $data = array();

         // buscando clientes

         $this->db->like('nomeCliente',$termo);

         $this->db->limit(5);

         $data['clientes'] = $this->db->get('clientes')->result();



         // buscando os

         $this->db->like('idOs',$termo);

         $this->db->limit(5);

         $data['os'] = $this->db->get('os')->result();



         // buscando produtos

         $this->db->like('descricao',$termo);

         $this->db->limit(5);

         $data['produtos'] = $this->db->get('produtos')->result();



         //buscando serviços

         $this->db->like('nome',$termo);

         $this->db->limit(5);

         $data['servicos'] = $this->db->get('servicos')->result();

         //buscando marcas

         $this->db->like('nome',$termo);

         $this->db->limit(5);

         $data['marcas'] = $this->db->get('marcas')->result();





         return $data;





    }



    

    function add($table,$data){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

        {

            return TRUE;

        }

        

        return FALSE;       

    }

    

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->update($table, $data);



        if ($this->db->affected_rows() >= 0)

        {

            return TRUE;

        }

        

        return FALSE;       

    }

    

    function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

        {

            return TRUE;

        }

        

        return FALSE;        

    }   

    

    function count($table){

        return $this->db->count_all($table);

    }

    function buscaUsuarioFranquia($email, $senha, $cod){
        $this->db->select('franquias.*');
        $this->db->from('franquias');
        $this->db->where('franquias.email', $email);
        $this->db->where('franquias.senha', $senha);
        $this->db->where('franquias.idFranquias', $cod);
        $this->db->where('situacao',1);
        $this->db->limit(1);
        return $result = $this->db->get()->row();
    }

    function buscaUsuarioUser($email, $senha, $cod){
        $this->db->select('usuarios.*');
        $this->db->select('franquias.idFranquias,franquias.id_plano,franquias.id_plano2,franquias.cep');
        $this->db->from('usuarios');
        $this->db->join('franquias', 'franquias.idFranquias = usuarios.idFranquia');
        $this->db->where('usuarios.email', $email);
        $this->db->where('usuarios.senha', $senha);
        $this->db->where('usuarios.idFranquia', $cod);
        $this->db->where('usuarios.situacao',1);
        $this->db->limit(1);
        return $result = $this->db->get()->row();
    }

    function dadosFranquiaLogin($cod){
        $this->db->select('*');
        $this->db->from('franquias');
        $this->db->where('franquias.idFranquias', $cod);
        $this->db->where('situacao',1);
        $this->db->limit(1);
        return $result = $this->db->get()->row();
    }

    function getOsAbertas(){

        $idFranquia=$this->session->userdata('id');

        $this->db->select('os.*, clientes.nomeCliente');

        $this->db->from('os');

        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');

        $this->db->where('os.status','Aberto');

        if ($idFranquia != "1"){

            $this->db->where('idFranquia', $idFranquia);

        }

        $this->db->limit(10);

        return $this->db->get()->result();

    }



    function getProdutosMinimo(){

        $idFranquia=$this->session->userdata('id');

        $this->db->select('*, (estoque_ideal - estoque) AS reposicao');
        $this->db->from("produtos");
        $this->db->order_by('(estoque_ideal - estoque)','desc');

        $this->db->where('estoque < estoque_ideal');

        if ($idFranquia != "1"){

            $this->db->where('idFranquia', $idFranquia);

        }

        //$sql = "SELECT * FROM produtos WHERE estoque <= estoqueMinimo LIMIT 10";

        //return $this->db->query($sql)->result();

        return $this->db->get()->result();



    }



    function getOsEstatisticas(){

        $idFranquia=$this->session->userdata('id');

        $this->db->select("status, count(status) as total");

        $this->db->from("os");

        $this->db->group_by("status");

        $this->db->order_by("status", "asc");

        if ($idFranquia != "1"){

            $this->db->where('idFranquias', $idFranquia);

        }

        //$sql = "SELECT status, COUNT(status) as total FROM os GROUP BY status ORDER BY status";

        //return $this->db->query($sql)->result();

        return $this->db->get()->result();

    }



    public function getEstatisticasFinanceiro(){

        $idFranquia=$this->session->userdata('id');
        $sql = "SELECT SUM(CASE WHEN baixado = 1 AND tipo = 'receita' THEN valor END) as total_receita, 

                       SUM(CASE WHEN baixado = 1 AND tipo = 'despesa' THEN valor END) as total_despesa,

                       SUM(CASE WHEN baixado = 0 AND tipo = 'receita' THEN valor END) as total_receita_pendente,

                       SUM(CASE WHEN baixado = 0 AND tipo = 'despesa' THEN valor END) as total_despesa_pendente FROM lancamentos WHERE idFranquia = {$idFranquia}";

        return $this->db->query($sql)->row();

    }



    public function getOrcamentosPendentes() {

        $idFranquia = $this->session->userdata('id');

        if(!$idFranquia)
            return (object) array('total' => 0);

        $this->db->select("count(*) as total");
        $this->db->from("orcamentos");
        $this->db->where("idFranquia", $idFranquia);
        $this->db->where("status != 2");
        $result = $this->db->get()->row();

        if($result->total <= 0){
            $this->db->select("count(*) as total");
            $this->db->from("orcamentos");
            $this->db->where("idFranquia", $idFranquia);
            $this->db->where("status", "4");
            $result = $this->db->get()->row();
            $result->tipo = 4;
        }

        if($result->total <= 0){
            $this->db->select("count(*) as total");
            $this->db->from("orcamentos");
            $this->db->where("idFranquia", $idFranquia);
            $this->db->where("status", "3");
            $result = $this->db->get()->row();
            $result->tipo = 3;
        }


        return $result;

    }





    public function getEmitente($id)

    {
        $this->db->where('idFranquias', $id);

        return $this->db->get('emitente')->result();

    }



    public function addEmitente($id, $nome, $cnpj, $ie, $logradouro, $numero, $complemento ,$bairro, $cidade, $uf,$telefone,$email, $logo, $hora_inicio, $hora_termino, $fuso_horario){

       

       $this->db->set('idFranquias', $id);

       $this->db->set('nome', $nome);

       $this->db->set('cnpj', $cnpj);

       $this->db->set('ie', $ie);

       $this->db->set('rua', $logradouro);

       $this->db->set('numero', $numero);

       $this->db->set('complemento', $complemento);

       $this->db->set('bairro', $bairro);

       $this->db->set('cidade', $cidade);

       $this->db->set('uf', $uf);

       $this->db->set('telefone', $telefone);

       $this->db->set('email', $email);

       $this->db->set('url_logo', $logo);

       $this->db->set('hora_inicio', $hora_inicio);

       $this->db->set('hora_termino', $hora_termino);

       $this->db->set('fuso_horario', $fuso_horario);

       return $this->db->insert('emitente');

    }





    public function editEmitente($id, $nome, $cnpj, $ie, $logradouro, $numero, $complemento, $bairro, $cidade, $uf,$telefone,$email, $hora_inicio, $hora_termino, $fuso_horario){

        

       $this->db->set('nome', $nome);

       $this->db->set('cnpj', $cnpj);

       $this->db->set('ie', $ie);

       $this->db->set('rua', $logradouro);

       $this->db->set('numero', $numero);

       $this->db->set('complemento', $complemento);

       $this->db->set('bairro', $bairro);

       $this->db->set('cidade', $cidade);

       $this->db->set('uf', $uf);

       $this->db->set('telefone', $telefone);

       $this->db->set('email', $email);

       $this->db->set('hora_inicio', $hora_inicio);

       $this->db->set('hora_termino', $hora_termino);

       $this->db->set('fuso_horario', $fuso_horario);

        $this->db->where('idFranquias', $id);

       return $this->db->update('emitente');

    }





    public function editLogo($id, $logo){

        

        $this->db->set('url_logo', $logo); 

        $this->db->where('idFranquias', $id);

        return $this->db->update('emitente'); 

         

    }

}