<?php
class Produtos_model extends CI_Model {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     *  
     */
    function __construct() {
        parent::__construct();
    }
function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $idFranquia=$this->session->userdata('id');
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idProdutos','desc');
        $this->db->limit($perpage,$start);
        $this->db->where("idFranquia IN(4,{$idFranquia})");
        $this->db->order_by('idFranquia');

       if(!empty($where))
        $this->db->where($where);
        else
        $this->db->where('pai_id IS NULL');

    $query = $this->db->get();

    $result =  !$one  ? $query->result() : $query->row();
    return $result;
}

// criando function para server side processing
    function count_totalProdutos(){
        $query = $this->db->where('idFranquia', $this->session->userdata('id'));
        $query = $this->db->get('produtos');
        return $query->num_rows();
    }
    function allProdutos($limit,$start,$col,$dir){
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->from('produtos');
        $this->db->where("idFranquia IN(4,{$idFranquia})");
        //$this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        //$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }else{
            return null;
        }
    }
    function idProdutos_search($limit,$start,$search,$col,$dir){
        // codigo alterado para evitar conflitos de pesquisas, devido a versão de mysql presente nesta versão do framework
        $idfranquia =  $this->session->userdata('id');
        //$fields = " * FROM produtos WHERE idFranquia IN(4,{".$idfranquia."}) 
        $fields = " * FROM produtos WHERE idFranquia = 4 AND idFranquia = ".$idfranquia." 
                    AND idProdutos LIKE'%".$search."%'
                    OR descricao LIKE'%".$search."%' 
                    OR codigo_barras LIKE'%".$search."%' 
                    OR codigo_produto LIKE'%".$search."%'
                    HAVING idFranquia = ".$idfranquia."";
        
        $this->db->select($fields);
        //$this->db->from('produtos');
        //$this->db->where('produtos.idFranquia',$this->session->userdata('id'));
        //$this->db->like('produtos.idProdutos',$search);
        //$this->db->or_like('produtos.descricao',$search);
        //$this->db->or_like('produtos.codigo_produto',$search);
        //$this->db->or_like('produtos.codigo_barras',$search);
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return null;
        }
    }
    function produtos_search_count($search){
        $this->db->where('produtos.idFranquia', $this->session->userdata('id'));
        $this->db->like('produtos.idProdutos',$search);
        $this->db->or_like('produtos.idProdutos',$search);
        $query = $this->db->get('produtos');
        return $query->num_rows(); 
    }
// criando function para server side processing


function getVariacao($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
   $this->db->select($fields);
   $this->db->from($table);
   $this->db->join('produto_variacoes_rel', 'produto_variacoes_rel.id_variacao = produto_variacoes.idVariacao', 'left outer');

   if(!empty($where))
       $this->db->where($where);

   $this->db->group_by('idVariacao');
   $this->db->order_by('idVariacao','desc');

$query = $this->db->get();

$result =  !$one  ? $query->result() : $query->row();
return $result;
}

function getTipos($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
   $idFranquia=$this->session->userdata('id');
   $this->db->select($fields);
   $this->db->from($table);
   $this->db->order_by('idTipos','desc');
   $this->db->limit($perpage,$start);
   if ($idFranquia != "1"){
    $this->db->where('idFranquia', $idFranquia);
}

$query = $this->db->get();

$result =  !$one  ? $query->result() : $query->row();
return $result;
}

public function autoCompleteVariacao($q){

    $this->db->select('*');
    $this->db->limit(5);
    $this->db->where("(titulo LIKE '%$q%')");
    $this->db->where('pai_id', null);

    $query = $this->db->get('produto_variacoes');

    if ($query->num_rows > 0) {

        foreach ($query->result_array() as $row)

            $row_set[] = array('label' => $row['titulo'], 'id' => $row['idVariacao']);

        echo json_encode($row_set);
    }
}

function getTaxas($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
   $idFranquia=$this->session->userdata('id');
   $this->db->select($fields);
   $this->db->from($table);
   $this->db->order_by('idTaxas','desc');
   $this->db->limit($perpage,$start);
   if ($idFranquia != "1"){
    $this->db->where('idFranquia', $idFranquia);
}

$query = $this->db->get();

$result =  !$one  ? $query->result() : $query->row();
return $result;
}
function getById($id){

    $this->db->select('produtos.*,produtos.idFranquia as id_franquia, marcas.*, marcas.nome, fornecedores.*, fornecedores.nomeFornecedor');
    $this->db->from('produtos');
    $this->db->join('marcas','marcas.idMarcas = produtos.marcas_id', 'left outer');
    $this->db->join('fornecedores','fornecedores.idFornecedores = produtos.fornecedor_id', 'left outer');
    $this->db->where('produtos.idProdutos',$id);
    $this->db->limit(1);
    return $this->db->get()->row();
}
function getVariacaoById($id){

    $this->db->select('produto_variacoes.*');
    $this->db->from('produto_variacoes');
    $this->db->where('produto_variacoes.idVariacao',$id);
    $this->db->limit(1);
    return $this->db->get()->row();
}
function getTaxaById($id){

    $this->db->select('produto_taxas.*');
    $this->db->from('produto_taxas');
    $this->db->where('produto_taxas.idTaxas',$id);
    $this->db->limit(1);
    return $this->db->get()->row();
}


function add($table,$data){
    $this->db->insert($table, $data);
    if ($this->db->affected_rows() == '1' || !empty($result))
    {
        return TRUE;
    }

    return FALSE;       
}

function addReturnId($table,$data){
    $this->db->insert($table, $data);         
    if ($this->db->affected_rows() == '1')
    {
        return $this->db->insert_id();
    }

    return FALSE;       
}

function buscar_codigo($table, $data){

    $this->db->select('produtos.*, marcas.*, marcas.nome, fornecedores.*, produtos.idFranquia as idFranquia, fornecedores.nomeFornecedor');
    $this->db->from('produtos');
    $this->db->join('marcas','marcas.idMarcas = produtos.marcas_id', 'left outer');
    $this->db->join('fornecedores','fornecedores.idFornecedores = produtos.fornecedor_id', 'left outer');
    $this->db->like($data);
    $this->db->where("produtos.idFranquia IN({$this->session->userdata('id')}, 4)");
    $this->db->order_by('produtos.idFranquia', 'desc');
    $result = $this->db->get()->row();

    if(@$result->idFranquia == 4 && $this->session->userdata('id') != 4){
        $result->estoque_ideal = "";
        $result->precoCompra = "";
        $result->precoVenda = "";
        $result->cfop = "";
        $result->estoque = "";
        $result->estoqueMinimo = "";
    }
    return $result;

    return FALSE;       
}

function edit($table,$data,$fieldID,$ID){
    $this->db->where($fieldID,$ID);
    $this->db->update($table, $data);
    if ($this->db->affected_rows() >= 0)
    {
        return TRUE;
    }

    return FALSE;       
}

function delete($table,$fieldID,$ID){
    $this->db->where($fieldID,$ID);
    $this->db->delete($table);
    if ($this->db->affected_rows() == '1')
    {
        return TRUE;
    }

    return FALSE;        
}
public function autoCompleteMarca($q){
    $idFranquia=$this->session->userdata('id');
    $this->db->select('*');
    $this->db->limit(5);
    $this->db->like('nome', $q);
    $this->db->where('status','ativo');
    $this->db->where('idFranquia', $idFranquia);
    $query = $this->db->get('marcas');
    if($query->num_rows > 0){
        foreach ($query->result_array() as $row){
            $row_set[] = array('label'=>$row['nome'],'id'=>$row['idMarcas']);
        }
        echo json_encode($row_set);
    }

}    

function count($table, $id, $franquia = true)
{
    $this->db->select('*');
    $this->db->from($table);
    if($franquia)
        $this->db->where('idFranquia', $id);
    $query = $this->db->get();
    return $query->num_rows();
}
public function autoCompleteFornecedores($q){
    $idFranquia=$this->session->userdata('id');
    $this->db->select('*');
    $this->db->limit(5);
    $this->db->like('nomeFornecedor', $q);
        //$this->db->where('status','ativo');
    // $this->db->where('idFranquia', $idFranquia);
    $query = $this->db->get('fornecedores');

    if($query->num_rows > 0){
        foreach ($query->result_array() as $row){
            $row_set[] = array('label'=>$row['nomeFornecedor'],'id'=>$row['idFornecedores']);
        }
        echo json_encode($row_set);
    }

}
}