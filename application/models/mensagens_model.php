<?php
class Mensagens_model extends CI_Model
{
    	/**
     	* author: ReisDEV 
     	* email: contato@reisdev.com
     	*/
    
    	function __construct()
    	{
        	parent::__construct();
    	}

    
    	function get($idFranquia)
    	{
    		$idFranquia = 4;
    		
        	$this->db->select('*');
        	$this->db->from('mensagens');
            	$this->db->where('idFranquia', $idFranquia);
        	$query = $this->db->get();        
        	return (array)$query->row();
    	}
    	
    	function edit($idFranquia, $data)
    	{
    		$idFranquia = 4;
    		
            	$this->db->where('idFranquia', $idFranquia);
        	$this->db->update('mensagens', $data);
        	return  ($this->db->affected_rows() >= 0);
    	}
}

/* End of file mensagens_model.php */
/* Location: ./application/models/mensagens_model.php */