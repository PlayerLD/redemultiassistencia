<?php
class Atendentes_model extends CI_Model {


    /**
     * author: Raphael Paulino
     * email: raphael.paulino.web@gmail.com
     * 
     */
    
    public function __construct() 
    {
        parent::__construct();
    }

    public function get($perpage=0,$start=0,$one=false)
    {
        $idFranquia=$this->session->userdata('id');

        $this->db->from('atendentes');
        // $this->db->select('atendentes.*, permissoes.nome as permissao');
        $this->db->select('atendentes.*');
        $this->db->limit($perpage,$start);
        // $this->db->join('permissoes', 'atendentes.permissoes_id = permissoes.idPermissao', 'left');

        if ($idFranquia != "1"){
            $this->db->where('idFranquia', $idFranquia);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();

        return $result;
    }

    public function getAllTipos()
    {
        $this->db->where('situacao',1);
        return $this->db->get('tiposUsuario')->result();
    }

    function getById($id){
        $this->db->where('idAtendentes',$id);
        $this->db->limit(1);
        return $this->db->get('atendentes')->row();
    }
    
    public function add($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->affected_rows() == '1' ? TRUE : FALSE;
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
	function count($table){
		return $this->db->count_all($table);
	}
}