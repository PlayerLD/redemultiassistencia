<?php


class Novos_slots_model extends CI_Model
{

    /**
     * author: Raphael Paulino
     * email: raphael.paulino.web@gmail.com
     */
    public function __construct() 
    {
        parent::__construct();
    }

    public function obterSlots($idFranquia = null)
    {
        $idFranquia = is_null($idFranquia) ? $this->session->userdata('id') : $idFranquia;

        $this->db->select('*');
        $this->db->where('idFranquias', $idFranquia);
        // $this->db->limit(1);
        
        $query = $this->db->get('novos_slots');

        // var_dump($this->db->last_query());

        if ($query->num_rows > 0) {

            // foreach ($query->result_array() as $row):

            //     switch ($prefix):

            //         case 'P':
            //         case 'p':
            //             $array = $row['p'];
            //             break;

            //         case 'M':
            //         case 'm':
            //             $array = $row['m'];
            //             break;

            //         case 'G':
            //         case 'g':
            //             $array = $row['g'];
            //             break;

            //         default:

            //             return false;

            //     endswitch;

            //     return json_decode($array);

            // endforeach;

            return $query->result_array();

        }

        return false;
    }
}