<?php
class Planos_model extends CI_Model {
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
    }
    function get($perpage=0,$start=0,$one=false){
        
        $this->db->from('planos');
        $this->db->select('planos.*');
        $this->db->limit($perpage,$start);
  
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    function listar($where = ''){
        
        $this->db->select('planos.*');
        $this->db->from('planos');
        if(!empty($where))
            $this->db->where($where);
  
        $result = $this->db->get()->result();
        
        return $result;
    }
    function cartoes($where = ''){
        
        $this->db->select('cartoes.*');
        $this->db->from('cartoes');
        if(!empty($where))
            $this->db->where($where);
  
        $result = $this->db->get()->result();
        
        return $result;
    }
    function pagamentos($where = ''){
        
        $this->db->select('pagamentos.*');
        $this->db->from('pagamentos');
        if(!empty($where))
            $this->db->where($where);
        $this->db->order_by('data', 'desc');
  
        $result = $this->db->get()->result();
        
        return $result;
    }

    function getById($id){
        $this->db->select('f.*');
        $this->db->where('f.idPlanos',$id);
        $this->db->limit(1);
        return $this->db->get('planos as f')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
        {
            return  $this->db->insert_id();
        }
        
        return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        
        return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE;        
    }   
    
    function count($table){
            return $this->db->count_all($table);
    }
}