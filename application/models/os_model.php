<?php 
class Os_model extends CI_Model
{
 
    /** 
     * author: Ramon Silva
     * email: silva018-mg@yahoo.com.br
     *
     */
    function __construct()
    {
        parent::__construct();
        //$this->table = 'os';
        // // Set orderable column fields
        //$this->column_order = array(null,'idOs','nomeCliente','nomeTecnico','nomeAtendente');
        //// Set searchable column fields
        //$this->column_search = array('idOs','nomeCliente','nomeTecnico','nomeAtendente');
        //// Set default order
        //$this->order = array('idOs' => 'asc');
    }
    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $idFranquia = $this->session->userdata('id');
        //$this->db->select($fields . ',clientes.nomeCliente,usuarios.nome as nomeTecnico, equipamentos.tipo as tipoEquipamento');
		$this->db->select($fields . ',clientes.nomeCliente,usuarios.nome as nomeTecnico, atendentes.nomeAtendente');
        $this->db->from($table);
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
		$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
		$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left outer');
		//$this->db->join('equipamentos_os', 'equipamentos_os.os_id = os.idOs');
        $this->db->limit($perpage, $start);
        $this->db->order_by('idOs', 'desc');
        $this->db->where('idFranquias', $idFranquia);
        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
		// echo '<pre>';
		// var_dump($result);
		// echo '</pre>';
		// exit;
        return $result;
    }
    // criando function para server side processing
    function count_totalOS(){
        $query = $this->db->where('idFranquias', $this->session->userdata('id'));
        $query = $this->db->get('os');
        return $query->num_rows();
    }
    function allOs($limit,$start,$col,$dir){
        $this->db->select('idOs,dataInicial,dataFinal,solicitou_garantia,garantia,descricaoProduto,visita,backup,os.status,observacoes,laudoTecnico,clientes.idClientes as idClientes,clientes.nomeCliente as nomeCliente,usuarios.nome as nomeTecnico,atendentes.nomeAtendente as nomeAtendente, os.clientes_id');
        $this->db->from('os');
        $this->db->where('os.idFranquias',$this->session->userdata('id'));
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        //$this->db->join('equipamentos_os', 'equipamentos_os.os_id = os.idOs', 'left outer');
        //$this->db->join('equipamentos', 'equipamentos_os.equipamentos_id = equipamentos.idEquipamentos', 'left outer');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }else{
            return null;
        }
    }
    function idOs_search($limit,$start,$search,$col,$dir){
        //$this->db->select('idOs,dataInicial,dataFinal,garantia,solicitou_garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente as nomeCliente,usuarios.nome as nomeTecnico,atendentes.nomeAtendente as nomeAtendente');
        //$this->db->from('os');
        //$this->db->where('os.idFranquias',$this->session->userdata('id'));
        //$this->db->like('idOs',$search);
        //$this->db->or_like('clientes.nomeCliente',$search);
        //$this->db->or_like('usuarios.nome',$search);
        //$this->db->or_like('atendentes.nomeAtendente',$search);
        // codigo alterado para evitar conflitos de pesquisas, devido a versão de mysql presente nesta versão do framework
        $idfranquia =  $this->session->userdata('id');
        $fields = "*,clientes.idClientes as idClientes, clientes.nomeCliente as nomeCliente,usuarios.nome as nomeTecnico,atendentes.nomeAtendente as nomeAtendente
        FROM os 
        INNER JOIN clientes ON clientes.idClientes = os.clientes_id
        INNER JOIN usuarios ON usuarios.idUsuarios = os.usuarios_id
        LEFT JOIN atendentes ON atendentes.idAtendentes = os.atendentes_id
        WHERE os.idFranquias = '".$idfranquia."'AND 
        os.idOs LIKE'%".$search."%'
        OR clientes.nomeCliente LIKE'%".$search."%' 
        OR atendentes.nomeAtendente LIKE'%".$search."%' 
        OR usuarios.nome LIKE'%".$search."%'
        
        HAVING idFranquias = '".$idfranquia."'";        
        $this->db->select($fields);
        //INNER JOIN equipamentos_os ON equipamentos_os.os_id = os.idOs
        //INNER JOIN equipamentos ON equipamentos_os.equipamentos_id = equipamentos.idEquipamentos
        //OR equipamentos.tipo LIKE'%".$search."%'
        //$this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        //$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return null;
        }
    }
    function os_search_count($search){
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->like('idOs',$search);
        $this->db->or_like('idOs',$search);
        $query = $this->db->get('os');
        return $query->num_rows(); 
    }
    // end criando function para server side processing

    // pegando os dados do tipo de equipamento atraveis do numero da os
    function tipoEquipamentoOs($idos){
        $selects = " equipamentos.equipamento as tpequi FROM equipamentos_os 
        INNER JOIN equipamentos ON equipamentos_os.equipamentos_id = equipamentos.idEquipamentos
        WHERE equipamentos_os.os_id = '".$idos."'";
        $this->db->select($selects);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $value) {
                return $value['tpequi'];
            }
        }else{
            return null;
        }
    }
    // pegando dados especificos do cliente para colocar no botão whatsapp
    function btndadosClientes($idcliente){
        $selects = "* FROM clientes WHERE clientes.idClientes = '".$idcliente."'";
        $this->db->select($selects);
        $this->db->limit(1);
        
        $result = $this->db->get()->result();
        return $result;
    }

    // pegando link do google atraves do numero da session
    function linkResposta(){
        $selects = " linkgoogle FROM franquias WHERE franquias.idFranquias = '".$this->session->userdata('id')."'";
        $this->db->select($selects);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $value) {
                return $value['linkgoogle'];
            }
        }else{
            return null;
        }
    }

    function updateEstoque($idOs)
    {
//         $this->db->select('produtos_os.produtos_id, produtos.estoque, produtos_os.quantidade');
//         $this->db->from('produtos');
//         $this->db->join('produtos_os', 'produtos.idProdutos = produtos_os.idProdutos_os');
//         $this->db->where('produtos_os.os_id', $idOs);
//         $query = $this->db->get()->result();
// var_dump($query); exit;
//         if(!empty($query)){
//             foreach ($query as $q) {
//                 $this->db->set('estoque', $q->estoque-$q->quantidade);
//                 $this->db->where('idProdutos', $q->produtos_id);
//                 $this->db->update('produtos');
//             }
//         }
        return;
    }
    function getByStatusOr($table, $id, $w1, $wv1, $w2, $wv2, $one = false)
    {
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs', 'desc');
        $this->db->where("idFranquias",$id);
        $this->db->where_in('status', array($wv1, $wv2));
        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }
    function getByStatusDif($table, $id, $w1, $wv1, $w2, $wv2, $one = false)
    {
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs', 'desc');
        if ($w1) $this->db->where($w1, $wv1);
        if ($w2) $this->db->where($w2, $wv2);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }
    function getBeforeStatus($id, $limit = 2)
    {
        $this->db->select('status, data');
        $this->db->from('os_status_rel');
        $this->db->where('os_id', $id);
        $this->db->order_by('data', 'desc');
        $this->db->limit($limit);
        $result = $this->db->get()->result();
        return $result;
    }
    function getStatus($table, $status, $id, $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs', 'desc');
        $this->db->where('status', $status);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }
    public function getStatusIn(array $values)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente,usuarios.nome as nomeTecnico,atendentes.nomeAtendente,franquias.nome as nomeFranquia');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->join('franquias', 'franquias.idFranquias = os.idFranquias');
        $this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left outer');
        $this->db->order_by('idOs', 'desc');
        $this->db->like('os.idFranquias', $idFranquia);
        $this->db->or_like('franquias.id_pai', $idFranquia);
        $this->db->where_in('status', $values);
        $query = $this->db->get();
        return $query->result();
    }
    public function getOrcamentos()
    {
        $idFranquia = $this->session->userdata('id');
        // $this->db->select('orcamentos.*, IF(filial.nome IS NOT NULL, filial.nome, franquias.nome) as nomeFranquia');
        // $this->db->from('orcamentos');
        // $this->db->join('franquias', 'orcamentos.idFranquia = franquias.idFranquias');
        // $this->db->join('franquias as filial', 'orcamentos.idFilial = filial.idFranquias', 'left outer');
        // $this->db->order_by('idFranquia', 'desc');
        // $this->db->where('orcamentos.idFranquia', $idFranquia);
        // $query = $this->db->get();
        $query = "SELECT orcamentos.*, IF(filial.nome IS NOT NULL, filial.nome, franquias.nome) AS nomeFranquia 
        FROM orcamentos 
        JOIN franquias ON orcamentos.idFranquia = franquias.idFranquias 
        LEFT OUTER JOIN franquias as filial ON orcamentos.idFilial = filial.idFranquias 
        WHERE orcamentos.idFranquia = {$idFranquia}
        ORDER BY orcamentos.status asc, orcamentos.data desc";
        return $this->db->query($query)->result();
    }
    function getStatusNotIn(array $values)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente,usuarios.nome as nomeTecnico');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
		$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->order_by('idOs', 'desc');
        $this->db->where('idFranquias', $idFranquia);
        $this->db->where_not_in('status', $values);
        $query = $this->db->get();
        return $query->result();
    }
    function updateStatus($os, $status)
    {
        $this->db->from('os_status_rel');
        $this->db->where(array('os_id' => $os, 'status' => $status));
        $result = $this->db->get()->row();
        if(empty($result)){
            $this->db->insert('os_status_rel', array('os_id' => $os, 'status' => $status));
        }
        return;
    }
    function getStatusCount($table, $status, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('status', $status);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function getEquipamentoById($id)
    {
        $this->db->select('equipamentos.*, clientes.*');
        $this->db->from('equipamentos');
        $this->db->join('clientes', 'clientes.idClientes = equipamentos.clientes_id');
        $this->db->where('idEquipamentos', $id);
        $this->db->limit(1);
        return (array)$this->db->get()->row();
    }
    public function getById($id)
    {
        $this->db->select('os.*, clientes.*, usuarios.telefone AS telefoneUser, usuarios.email, usuarios.nome, atendentes.nomeAtendente');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->where('os.idOs', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    public function getProdutos($id = null)
    {
        $this->db->select('produtos_os.*, produtos.*');
        $this->db->from('produtos_os');
        $this->db->join('produtos', 'produtos.idProdutos = produtos_os.produtos_id');
        $this->db->where('os_id', $id);
        return $this->db->get()->result();
    }
    public function getEquipamentos($id = null)
    {
        $this->db->select('equipamentos_os.*, equipamentos.*');
        $this->db->from('equipamentos_os');
        $this->db->join('equipamentos', 'equipamentos.idEquipamentos = equipamentos_os.equipamentos_id');
        $this->db->where('os_id', $id);
        return $this->db->get()->result();
    }
    public function getServicos($id = null)
    {
        $this->db->select('*');
        $this->db->from('servicos_os');
        $this->db->where('os_id', $id);
        return $this->db->get()->result();
    }
    public function getPagamentos($id = null)
    {
        $this->db->select('*');
        $this->db->from('pagamento_os');
        $this->db->where('os_id', $id);
        return $this->db->get()->result();
    }
    public function getMarcaNome($idMarca)
    {
        $this->db->select('*');
        $this->db->where('idMarcas', $idMarca);
        $this->db->limit(1);
        $query = $this->db->get('marcas');
        if ($query->num_rows < 1)
            return "<não cadastrada>";
        else {
            foreach ($query->result_array() as $row)
                return $row['nome'];
        }
    }
    public function getSlotMax($prefix)
    {
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('franquias');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        return json_decode($row['slotsp']);
                    case 'M':
                    case 'm':
                        return json_decode($row['slotsm']);
                    case 'G':
                    case 'g':
                        return json_decode($row['slotsg']);
                }
            }
        }
        return 0;
    }
    public function getSlotData($prefix, $slot)
    {
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('slots');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        $array = $row['p'];
                        break;
                    case 'M':
                    case 'm':
                        $array = $row['m'];
                        break;
                    case 'G':
                    case 'g':
                        $array = $row['g'];
                        break;
                    default:
                        goto retzero;
                }
                @$equip_os = json_decode($array)[$slot];
                if ($equip_os == 0)
                    goto retzero;
                $this->db->select('*');
                $this->db->where('idEquipamentos_os', $equip_os);
                $this->db->limit(1);
                $query = $this->db->get('equipamentos_os');
                if ($query->num_rows > 0) {
                    foreach ($query->result_array() as $row) {
                        $os = $row['os_id'];
                        if ($os == 0)
                            goto retzero;
                        $this->db->select('*');
                        $this->db->where('idEquipamentos', $row['equipamentos_id']);
                        $this->db->limit(1);
                        $query = $this->db->get('equipamentos');
                        if ($query->num_rows > 0) {
                            foreach ($query->result_array() as $row) {
                                return array('os' => $os,
                                    'tipo' => $row['tipo'],  // tipo de equipamento
                                    'nome' => $this->getMarcaNome($row['marcas_id']) . ' ' . $row['equipamento'] . ' ' . $row['modelo']); // nome do equipamento
                            }
                        }
                    }
                }
            }
        }
        retzero:
        return array('os' => '0', 'tipo' => '', 'nome' => '');
    }
    public function getSlotPrefix($equip_id)
    {
        $this->db->select('*');
        $this->db->where('idEquipamentos', $equip_id);
        $this->db->limit(1);
        $query = $this->db->get('equipamentos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($row['tipo']) {
                    case 'celular':
                    case 'Celular':
                        return 'P';
                    case 'tablet':
                    case 'Tablet':
                    case 'notebook':
                    case 'Notebook':
                    case 'videogame':
                    case 'Videogame':
                    case 'VideoGame':
                    case 'Video Game':
                    case 'video game':
                        return 'M';
                    case 'computador':
                    case 'Computador':
                    case 'tv':
                    case 'TV':
                        return 'G';
                    default:
                        goto retzero;
                }
            }
        }
        retzero:
        return '';
    }
   public function getSlot($prefix, $idFranquia = null)
    {
        $idFranquia = $idFranquia == null ? $this->session->userdata('id') : $idFranquia;
        $this->db->select('*');
        $this->db->where('idFranquias', $idFranquia);
        $this->db->limit(1);
        $query = $this->db->get('slots');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        $array0 = $row['p'];
                        $array = str_replace(',"":{"":"0"}', '', $array0);
                        break;
                    case 'M':
                    case 'm':
                        $array0 = $row['m'];
                        $array = str_replace(',"":{"":"0"}', '', $array0);
                        break;
                    case 'G':
                    case 'g':
                        $array0 = $row['g'];
                        $array = str_replace(',"":{"":"0"}', '', $array0);
                        break;
                    default:
                        return false;
                }
                return json_decode($array,true);
            }
        }
        return false;
    }
    public function getSlotById($equip_id, $os_id)
    {
        $prefix = $this->getSlotPrefix($equip_id);
        $array = $this->getSlot($prefix);
        if (!$array)
            return -1;
        $this->db->select('*');
        $this->db->where('os_id', $os_id);
        $this->db->where('equipamentos_id', $equip_id);
        $this->db->limit(1);
        $query = $this->db->get('equipamentos_os');
        if ($query->num_rows > 0)
            foreach ($query->result_array() as $row)
                for ($i = 0; $i < count($array); $i++) {
                    if ($array[$i] == $row['idEquipamentos_os'])
                        return $prefix . ($i + 1);
                }
        return -1;
    }
    public function updSlot($cmd, $idFranquia = null)
    {
        $idFranquia = $idFranquia == null ? $this->session->userdata('id') : $idFranquia;
        $this->db->where('idFranquias', $idFranquia);
        $this->db->update('slots', $cmd);
    }
    // programador
    // nessa parte aqui é preciso descomentar o nome por categoria de cada tipo de equipamento.
    public function getDadosSlotEmpty($equip_id){
        $prefix = $this->getSlotPrefix($equip_id);
        $array = $this->getSlot($prefix);
        // pegando dados da franquia
        $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $qtd_fl_p = $franquia->qtd_fileiras_slots_p;
        $qtd_fl_m = $franquia->qtd_fileiras_slots_m;
        $qtd_fl_g = $franquia->qtd_fileiras_slots_g;
        //$arrayddslots = $this->franquias_model->slotsDados($this->session->userdata('id'), $prefix);
        // end dados franquia
        switch ($prefix) {
            case 'p':
            case 'P':
            case 'Celular':
            case 'celular':
                // pegando todos os espaços livres do array p
                //$array = json_decode($array['p'], true);
                $limite = 0;
                foreach ($array as $key => $value) {
                    for ($j=1, $i = 0; $j <= $qtd_fl_p ; $j++, $i++) { 
                    // tratando os dados e formando um novo array
                        if ($value[$i] == "0") {
                            $livres[] = $key.','.$i.','.$value[$i];
                            $vazio = 0;
                        }else if($value[$i] != "0"){
                            $vazio = 1;
                            
                        }
                    }
                }
                if ($vazio == 0) {
                    for ($i=0; $i <= $limite ; $i++) { 
                        $keyvalor = explode(',', $livres[$i]);          
                    }
                    
                    $keyupt = $keyvalor[0];
                    $valuind = $keyvalor[1];
                    $valoralter = $keyvalor[2];
                    $posicao = $keyupt.$valuind; 
                    return array('posicao' => $posicao,'livres' => $livres,'array' => $array, 'prefix' => $prefix, 'keyupt' => $keyupt, 'valuind' => $valuind, 'valoralter' => $valoralter);        
                    
                }else if($vazio == 1){
                    $posicao = 0;
                    return $posicao;
                }
                //return array('posicao' => $posicao,'array' => $array, 'prefix' => $prefix);        
                //return $posicao;      
                
                break;
            case 'm':
            case 'M':
            case 'tablet':
            case 'Tablet':
            case 'Videogame':
            case 'videogame':
            case 'VideoGame':
            case 'Video Game':
            case 'video game':
            case 'Notebook':
            case 'notebook':
                // pegando todos os espaços livres do array m
               // $array = json_decode($array['m'], true);
                $limite = 0;
                foreach ($array as $key => $value) {
                    for ($j=1, $i = 0; $j <= $qtd_fl_m ; $j++, $i++) { 
                    // tratando os dados e formando um novo array
                        if ($value[$i] == "0") {
                            $livres[] = $key.','.$i.','.$value[$i];
                            $vazio = 0;
                        }else if($value[$i] != "0"){
                            $vazio = 1;
                        }
                    }
                }
                if ($vazio == 0) {
                    for ($i=0; $i <= $limite ; $i++) { 
                        $keyvalor = explode(',', $livres[$i]);          
                    }
                    $keyupt = $keyvalor[0];
                    $valuind = $keyvalor[1];
                    $valoralter = $keyvalor[2];
                    $posicao = $keyupt.$valuind;
                    // return array('posicao' => $posicao,'array' => $array, 'prefix' => $prefix);        
                    return array('posicao' => $posicao,'array' => $array, 'prefix' => $prefix, 'keyupt' => $keyupt, 'valuind' => $valuind, 'valoralter' => $valoralter);
                }else if($vazio == 1){
                    $posicao = 0;
                    return $posicao;
                }
                
                break;
            case 'g':
            case 'G':
            case 'Computador':
            case 'computador':
            case 'tv':
            case 'TV':
                // pegando todos os espaços livres do array g
                //$array = json_decode($array['g'], true);
                $limite = 0;
                foreach ($array as $key => $value) {
                    for ($j=1, $i = 0; $j <= $qtd_fl_g ; $j++, $i++) { 
                    // tratando os dados e formando um novo array
                        if ($value[$i] == "0") {
                            $livres[] = $key.','.$i.','.$value[$i];
                            $vazio = 0;
                        }else if($value[$i] != "0"){
                            $vazio = 1;
                        }
                    }
                }
                if ($vazio == 0) {
                    
                    for ($i=0; $i <= $limite ; $i++) { 
                        $keyvalor = explode(',', $livres[$i]);          
                    }
                    $keyupt = $keyvalor[0];
                    $valuind = $keyvalor[1];
                    $valoralter = $keyvalor[2];
                    $posicao = $keyupt.$valuind;
                
                    return array('posicao' => $posicao,'array' => $array, 'prefix' => $prefix, 'keyupt' => $keyupt, 'valuind' => $valuind, 'valoralter' => $valoralter);        
                }else if($vazio == 1){
                    $posicao = 0;
                    return $posicao;
                }
                break;
        }
       // return $posicao;     
    }
    /*public function getSlotEmpty($equip_id)
    {
        $prefix = $this->getSlotPrefix($equip_id);
        $array = $this->getSlot($prefix);
        if (!$array)
            return -1;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] == 0)
                return array('slot' => $i, 'array' => $array, 'prefix' => $prefix);
        }
        return -1;
    }  */
    /*function addInAll($table, $data, $empty)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            $id = $this->db->insert_id();
            $array = $empty['array'];
            //$array[$empty['slot']] = $this->db->insert_id($table);
            $array = $this->db->insert_id($table);
            $array = json_encode(array_values($array));
            $this->updSlot(array(strtolower($empty['prefix']) => $array));
            return $id;
        }
        return FALSE;
    }*/
    // programador
    function addSlotsUpdt($table, $data, $empty){
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
        
            $id = $this->db->insert_id();
        
            //$array = $empty['array'];
        
            //$empty['posicao'] = $this->db->insert_id($table);
            //$arary = json_encode($empty['array']);
            $keyupt  = $empty['keyupt'];
            $valuind  = $empty['valuind'];
            $valoralter  = $empty['valoralter'] = $data['os_id'];
            $alterdados = array($keyupt => array($valuind => $valoralter));
            $combinados = array_replace_recursive($empty['array'], $alterdados);
            //echo "<pre>";
            //print_r($alterdados);
            //print_r($empty['valuind']);
            //var_dump($alterdados);
            //var_dump($empty['array']);
            //echo "</pre>";
            
            $updslots = json_encode($combinados);
            
            $ddslot = array(
                'prefix' => strtolower($empty['prefix']),
                'array'  => $updslots
            );
            $this->updtSlots($ddslot);
            return $id;
        }
        return FALSE;
    }
    public function updtSlots($cmd, $idFranquia = null){
        // alterando a table slot onde prefixo e posicao foi definido
        $idFranquia = $this->session->userdata('id');
        //$this->db->where('idFranquias', $idFranquia);
        //$this->db->update('slots', $cmd);
        switch ($cmd['prefix']) {
            case 'p':
            case 'P':
                $this->db->set('p',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                //$this->db->like('p', $idos);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
            case 'm':
            case 'M':
                $this->db->set('m',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                //$this->db->like('m', $idos);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
            case 'g':
            case 'G':
                $this->db->set('g',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                //$this->db->like('g', $idos);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
        }
    }
    public function updtSlotsLike($cmd, $idFranquia = null){
        // alterando a table slot onde prefixo e posicao foi definido
        $idFranquia = $this->session->userdata('id');
        //$this->db->where('idFranquias', $idFranquia);
        //$this->db->update('slots', $cmd);
        switch ($cmd['prefix']) {
            case 'p':
            case 'P':
                $this->db->set('p',$cmd['array']);
                $this->db->set('m',$cmd['array']);
                $this->db->set('g',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                $this->db->like('p', $cmd['idos']);
                $this->db->like('m', $cmd['idos']);
                $this->db->like('g', $cmd['idos']);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
            
            case 'm':
            case 'M':
            
                $this->db->set('m',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                //$this->db->like('m', $idos);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
            
            case 'g':
            case 'G':
                $this->db->set('g',$cmd['array']);
                $this->db->where('idFranquias', $idFranquia);
                //$this->db->like('g', $idos);
                //$this->db->where('p', $cmd['prefix']);
                $this->db->update('slots');
                break;
        }
    }
    function add($table, $data, $returnId = false)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }
    public function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);
        // if ($this->db->affected_rows() >= 0) {
        //     return TRUE;
        // }
        // return FALSE;
        return $this->db->affected_rows() >= 0 ? TRUE : FALSE;
    }
    function deleteAll($table, $fieldID, $ID)
    {
        deleteSlot($ID);
        return delete($table, $fieldID, $ID);
    }
    function deleteSlot($ID)
    {
        $this->db->select('*');
        $this->db->where('idEquipamentos_os', $ID);
        $query = $this->db->get('equipamentos_os');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $prefix = $this->getSlotPrefix($row['equipamentos_id']);
                $array = $this->getSlot($prefix);
                if (!$array)
                    return
                        $array[$row['slot']] = 0;
                //$array = json_encode($array);
                $this->updSlot(array(strtolower($prefix) => $array));
            }
        }
    }
    function deleteEquipamento($id)
    {
        $prefix = $this->getSlotPrefix($id);
        $array = $this->getSlot($prefix);
        if (!$array)
            return false;
        // SETA COMO ATENDIMENTO ENCERRADO
        $this->db->where('idEquipamentos_os', $id);
        $this->db->delete('equipamentos_os');
        // REMOVE DA TABELA SLOTS
        for ($i = 0; $i < count($array); $i++)
            if ($array[$i] == $id)
                $array[$i] = 0;
        $array = json_encode(array_values($array));
        $this->updSlot(array(strtolower($prefix) => $array));
        return TRUE;
    }
    function deleteEquiLiberaSlots($os){
        // pegando dados da franquia
        $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $qtd_fl_p = $franquia->qtd_fileiras_slots_p;
        $qtd_fl_m = $franquia->qtd_fileiras_slots_m;
        $qtd_fl_g = $franquia->qtd_fileiras_slots_g;
        
        $reset = "0";
       
        $this->db->select('*');
        $this->db->where('os_id', $os);
        $query = $this->db->get('equipamentos_os');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $prefix = $this->getSlotPrefix($row['equipamentos_id']);
                $array = $this->getSlot($prefix);
                if ($array){
                    
                    //continue;
                    // SETA COMO ATENDIMENTO ENCERRADO
                    $this->db->where('idEquipamentos_os', $row['idEquipamentos_os']);
                    
                    $this->db->where('os_id', $row['os_id']);
                    
                    $this->db->where('equipamentos_id', $row['equipamentos_id']);
                    
                    $this->db->where('modelo', $row['modelo']);
                    
                    $this->db->where('slot', $row['slot']);
                    
                    $this->db->update('equipamentos_os', array('slot' => $reset));
                    
                    // faltar terminar condição para cada tamanho de slot's aqui.
                    
                    $limite = 0;
                    switch ($prefix) {
                        case 'p':
                        case 'P':
                            foreach ($array as $key => $value) {
                                for ($j=1, $i = 0; $j <= $qtd_fl_p; $j++, $i++) {           
                                    if ($value[$i] == $os) {
                                        echo $ocupado[] = $key.','.$i.','.$value[$i];
                                    }
                                }
                            }
                            for ($i=0; $i <= $limite; $i++) { 
                                $ocu = explode(',', $ocupado[$i]);
                            }
                            $keyupt = $ocu[0];
                            $valuind = $ocu[1];
                            $valoralter = $ocu[2] = $reset; 
                            $alterdados =  array($keyupt => array($valuind => $valoralter));
                            $combinados = array_replace_recursive($array,$alterdados);
                            
                            $updslots = json_encode($combinados);
                            
                            $array = str_replace(',"":{"":"0"}', '', $updslots);
                                        
                            $ddslot = array(
                                'prefix' => strtolower($prefix),
                                'array'  => $array
                                //'idos'  => $os
                            );
                            //$this->updtSlotsLike($ddslot);
                            $this->updtSlots($ddslot);
                            break;
                        case 'm':
                        case 'M':
                            foreach ($array as $key => $value) {
                                for ($j=1, $i = 0; $j <= $qtd_fl_m; $j++, $i++) {           
                                    if ($value[$i] == $os) {
                                        echo $ocupado[] = $key.','.$i.','.$value[$i];
                                    }
                                }
                            }
                            for ($i=0; $i <= $limite; $i++) { 
                                $ocu = explode(',', $ocupado[$i]);
                            }
                            $keyupt = $ocu[0];
                            $valuind = $ocu[1];
                            $valoralter = $ocu[2] = $reset; 
                            $alterdados =  array($keyupt => array($valuind => $valoralter));
                            $combinados = array_replace_recursive($array,$alterdados);
                            
                            $updslots = json_encode($combinados);
                            $array = str_replace(',"":{"":"0"}', '', $updslots);
                                        
                            $ddslot = array(
                                'prefix' => strtolower($prefix),
                                'array'  => $array
                            );
                            $this->updtSlots($ddslot);
                            break;
                        case 'g':
                        case 'G':
                            foreach ($array as $key => $value) {
                                for ($j=1, $i = 0; $j <= $qtd_fl_m; $j++, $i++) {           
                                    if ($value[$i] == $os) {
                                        echo $ocupado[] = $key.','.$i.','.$value[$i];
                                    }
                                }
                            }
                            for ($i=0; $i <= $limite; $i++) { 
                                $ocu = explode(',', $ocupado[$i]);
                            }
                            $keyupt = $ocu[0];
                            $valuind = $ocu[1];
                            $valoralter = $ocu[2] = $reset; 
                            $alterdados =  array($keyupt => array($valuind => $valoralter));
                            $combinados = array_replace_recursive($array,$alterdados);
                            $updslots = json_encode($combinados);
                            $array = str_replace(',"":{"":"0"}', '', $updslots);
                                                                    
                            $ddslot = array(
                                'prefix' => strtolower($prefix),
                                'array'  => $array,
                            );
                            $this->updtSlots($ddslot);
                            break;
                        
                    }
                }
            }
        }
    }
    /*function deleteListaEquipamentos($os)
    {
        $this->db->select('*');
        $this->db->where('os_id', $os);
        $query = $this->db->get('equipamentos_os');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $prefix = $this->getSlotPrefix($row['equipamentos_id']);
                $array = $this->getSlot($prefix);
                if (!$array)
                    continue;
                // SETA COMO ATENDIMENTO ENCERRADO
                $this->db->where('idEquipamentos_os', $row['idEquipamentos_os']);
                $this->db->where('os_id', $row['os_id']);
                $this->db->where('equipamentos_id', $row['equipamentos_id']);
                $this->db->where('modelo', $row['modelo']);
                $this->db->where('slot', $row['slot']);
                $this->db->update('equipamentos_os', array('slot' => -1));
                // REMOVE DA TABELA SLOTS
                $array[$row['slot']] = 0;
                $array = json_encode(array_values($array));
                $this->updSlot(array(strtolower($prefix) => $array));
            }
        }
    }*/
    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }
    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function autoCompleteProduto($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('descricao', $q);
        $this->db->where('idFranquia', $idFranquia);
        $this->db->where('precoVenda IS NOT NULL');
        $this->db->where('precoCompra IS NOT NULL');
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['descricao'] . ' | Preço: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'], 'estoque' => $row['estoque'], 'id' => $row['idProdutos'], 'preco' => $row['precoVenda']);
            }
            echo json_encode($row_set);
        }
    }
    public function autoCompleteCliente($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nomeCliente', $q);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('clientes');
        if($query->num_rows > 0):
            foreach ($query->result_array() as $row):
                $row_set[] = array('label' => $row['nomeCliente'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idClientes']);
            endforeach;
            echo json_encode($row_set);
        endif;
    }
    public function autoCompleteAtendente($q)
    {
        $row_set = [];

        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nomeAtendente', $q);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('atendentes');
        
        if($query->num_rows > 0):
            foreach ($query->result_array() as $row):
                $row_set[] = array('label' => $row['nomeAtendente'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idAtendentes']);
            endforeach;
        endif;

        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('usuarios');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idUsuarios']);
            }
        }

            echo json_encode($row_set);
    }
    public function autoCompleteUsuario($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('usuarios');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }
    public function autoCompleteFranquia($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('nome, idFranquias');
        $this->db->like('nome', $q);
        $query = $this->db->get('franquias');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'], 'id' => $row['idFranquias']);
            }
            echo json_encode($row_set);
        }
    }
    public function autoCompleteServico($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('servicos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array
                (
                    'label' => $row['nome'],
                    'id' => $row['idServicos'],
                    'preco' => $row['preco'],
                    'descricao' => $row['descricao']
                );
            }
            echo json_encode($row_set);
        }
    }
    public function getNotaRespostas()
    {
        $this->load->model('sistemaos_model');
        $idFranquia = $this->session->userdata('id');
        $dados = $this->sistemaos_model->getEmitente($idFranquia);
// formula
// (X/(X+Y))*100
        $entrada = (int)str_replace(':', '', date('H:i', strtotime($dados[0]->hora_inicio.' '.$dados[0]->fuso_horario.' hours')));
        $saida = (int)str_replace(':', '', date('H:i', strtotime($dados[0]->hora_termino.' '.$dados[0]->fuso_horario.' hours')));

        $query = "
        SELECT 
            ROUND(
                (
                    (SELECT COUNT(o.idOrcamento) FROM orcamentos as o WHERE TIMESTAMPDIFF(MINUTE,data,dataResposta) <= 30 AND DATE_FORMAT(data, '%H%i') >= ".$entrada." AND DATE_FORMAT(data, '%H%i') <= ".$saida." AND DATEDIFF(DATE(NOW()),data) <= 30 AND o.idFranquia = p.idFranquia)
                    /
                    (
                        (SELECT COUNT(o.idOrcamento) FROM orcamentos as o WHERE TIMESTAMPDIFF(MINUTE,data,dataResposta) > 30 AND DATE_FORMAT(data, '%H%i') >= ".$entrada." AND DATE_FORMAT(data, '%H%i') <= ".$saida." AND DATEDIFF(DATE(NOW()),data) <= 30 AND o.idFranquia = p.idFranquia)
                        +
                        (SELECT COUNT(o.idOrcamento) FROM orcamentos as o WHERE TIMESTAMPDIFF(MINUTE,data,dataResposta) <= 30 AND DATE_FORMAT(data, '%H%i') >= ".$entrada." AND DATE_FORMAT(data, '%H%i') <= ".$saida." AND DATEDIFF(DATE(NOW()),data) <= 30 AND o.idFranquia = p.idFranquia)
                    )
                ) * 100
            ) AS nota
        FROM orcamentos as p
        WHERE p.idFranquia = {$idFranquia} LIMIT 1";
        $result = $this->db->query($query)->row();

        $result->nota = (int)$result->nota;
        if($result->nota <= 25){
            $html = '<li class="alert alert-danger" style="margin-right: 10px;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;"><i class="icon icon-info"></i>Respondido em até 30 min: '.$result->nota.'% <span class="popover-wrapper" id="popover-nota">

                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question" onclick="$(\'#popover-nota\').toggleClass(\'open\')"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                                <strong>SEU TEMPO DE RESPOSTA ESTÁ MUITO RUIM</strong>
                                                <p>Responda os orçamentos antes de 30 minutos e aumente sua taxa de conversão de orcamentos.</p>
                                            </div>
                                          </div>
                                        </span></div></li>';
        }
        elseif($result->nota <= 50){
            $html = '<li class="alert alert-danger" style="margin-right: 10px;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;"><i class="icon icon-info"></i>Respondido em até 30 min: '.$result->nota.'% <span class="popover-wrapper" id="popover-nota">
                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question" onclick="$(\'#popover-nota\').toggleClass(\'open\')"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                                <strong>SEU TEMPO DE RESPOSTA ESTÁ RUIM </strong>
                                                <p>Você pode melhorar no tempo de respostas dos orçamentos, se esforçando ainda mais para resppnder antes de 30 minutos e aumente sua taxa de conversão de orcamentos.</p>
                                            </div>
                                          </div>
                                        </span></div></li>';
        }
        elseif($result->nota <= 80){
            $html = '<li class="alert alert-info" style="margin-right: 10px;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;"><i class="icon icon-info"></i>Respondido em até 30 min: '.$result->nota.'% <span class="popover-wrapper" id="popover-nota">
                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question" onclick="$(\'#popover-nota\').toggleClass(\'open\')"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                                <strong>VOCÊ ESTÁ QUASE LÁ</strong>
                                                <p>Não está ruim, mas sabemos que consegue melhorar ainda mais o seu tempo de resposta do orcamentos. Só mais um pouco, e manterá sua chance de conversão de orçamentos em níveis altos.</p>
                                            </div>
                                          </div>
                                        </span></div></li>';
        }
        elseif($result->nota <= 90){
            $html = '<li class="alert alert-info" style="margin-right: 10px;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;"><i class="icon icon-info"></i>Respondido em até 30 min: '.$result->nota.'% <span class="popover-wrapper" id="popover-nota">
                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question" onclick="$(\'#popover-nota\').toggleClass(\'open\')"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                                <strong>SEU TEMPO DE RESPOSTA ESTA BOM</strong>
                                                <p>Continue melhorando assim, mantenha seu tempo de resposta baixo que consequentemente terá maiores chances de conversão de orçamento.</p>
                                            </div>
                                          </div>
                                        </span></div></li>';
        }
        elseif($result->nota > 90){
            $html = '<li class="alert alert-succes" style="margin-right: 10px;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;"><i class="icon icon-info"></i>Respondido em até 30 min: '.$result->nota.'% <span class="popover-wrapper" id="popover-nota">
                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question" onclick="$(\'#popover-nota\').toggleClass(\'open\')"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                                <strong>SEU TEMPO DE RESPOSTA ESTÁ MUITO RUIM</strong>
                                                <p>Responda os orçamentos antes de 30 minutos e aumente sua taxa de conversão de orcamentos.</p>
                                            </div>
                                          </div>
                                        </span></div></li>';
        }
        
        return $html;
    }
    public function autoCompleteEquipamento($q)
    {
        $idFranquia = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->where("(equipamento LIKE '%$q%' OR mei LIKE '%$q%' OR modelo LIKE '%$q%')");
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('equipamentos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row)
                $row_set[] = array('label' => $row['equipamento'] . ' | Marca: ' . $this->getMarcaNome($row['marcas_id']) . ' | Modelo: ' . $row['modelo'] . ' | IMEI: ' . $row['mei'], 'id' => $row['idEquipamentos']);
            echo json_encode($row_set);
        }
    }
    public function adicionarObservacaoInterna($os, $observacao, $img01 = "", $img02 = "", $img03 = "", $img04 = "", $img05 = "", $img06 = "")
    {

        $this->db->set('os_id', $os);
        $this->db->set('observacao_interna', $observacao);
        $this->db->set('data', date('Y-m-d H:i:s') );

        if($this->session->userdata('user_id'))
            $this->db->set('usuario_id', $this->session->userdata('user_id'));

        $this->db->insert('os_observacoes_internas');
        $id = $this->db->insert_id();

        if(!empty($img01)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img01);
            $this->db->insert('os_observacoes_internas');
        }

        if(!empty($img02)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img02);
            $this->db->insert('os_observacoes_internas');
        }

        if(!empty($img03)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img03);
            $this->db->insert('os_observacoes_internas');
        }

        if(!empty($img04)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img04);
            $this->db->insert('os_observacoes_internas');
        }

        if(!empty($img05)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img05);
            $this->db->insert('os_observacoes_internas');
        }

        if(!empty($img06)){
            $this->db->set('pai_id', $id);
            $this->db->set('arquivo', $img06);
            $this->db->insert('os_observacoes_internas');
        }

        return  $id;
    }
    public function adicionarObservacaoInternaAnexo($id, $arquivo)
    {
        $this->db->set('pai_id', $id);
        $this->db->set('arquivo', $arquivo);
        $this->db->set('data', date('Y-m-d H:i:s') );
        return $this->db->insert('os_observacoes_internas');
    }
    public function anexar($os, $anexo, $url, $thumb, $path)
    {
        $this->db->set('anexo', $anexo);
        $this->db->set('url', $url);
        $this->db->set('thumb', $thumb);
        $this->db->set('path', $path);
        $this->db->set('os_id', $os);
        return $this->db->insert('anexos');
    }
    public function getObservacoesInternas($os)
    {
        $this->db->select('os_observacoes_internas.*, usuarios.nome as usuario');
        $this->db->from('os_observacoes_internas');
        $this->db->join('usuarios', 'os_observacoes_internas.usuario_id = usuarios.idUsuarios', 'left outer');
        $this->db->where('os_id', $os);
        $result = $this->db->get()->result();

        if(!empty($result)){
            foreach ($result as $r => $v) {
                $this->db->select('os_observacoes_internas.arquivo');
                $this->db->from('os_observacoes_internas');
                $this->db->where('pai_id', $v->id);
                $result[$r]->fotos = $this->db->get()->result();
            }
        }

        return $result;
    }
    public function getAnexos($os)
    {
        $this->db->where('os_id', $os);
        return $this->db->get('anexos')->result();
    }
	
	public function alterarStatus($status, $idOs){
		$data = array('status' => $status);
		
		$this->db->where('idOs', $idOs);
        $this->db->update('os', $data);
        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
	}
    // programador
    public function franquiasSlots($prefix){
        // trazendo os dados de slots da franquia      
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('franquias');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                //$row_result = array('label' =>  $row['idOs'], $row['idFranquias']);
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        return $row['slotsp'];
                    case 'M':
                    case 'm':
                        return $row['slotsm'];
                    case 'G':
                    case 'g':
                        return $row['slotsg'];
                }
            }
        }
        return 0;
    }    
    //public function equipamentoOs($idos,$idfranquia,$clientes){
    //    
    //}
    public function dadosOS($idfranquia,$dtinicial){
        $this->db->select('idOs,idFranquias,clientes_id');
        $this->db->from('os');
        $this->db->where('os.idFranquias', $idfranquia);
        //$this->db->where('os.status', $status);
        $this->db->where("(os.dataInicial LIKE '%$dtinicial%' )");
        $query = $this->db->get();
            if ($query->num_rows > 0) {
                //foreach ($query->result_array() as $row) {
                    //return $query->result();
                    //return $query->row();
                    return $query->result_array();
                    //return $row['idOs'].'-'.$row['idFranquias'].'-'.$row['clientes_id'];
                   //return $row;
               // }
            }else{
                return false; 
            }
    }
  // buscando os dados dos equipamentos via num os, idfranquia e cliente_id
    public function dadosEquipamentos($idos,$idfranquia,$clientes_id){
        //$this->db->select('equi.*,equios.*');
        //$this->db->select('equi.idEquipamentos, equi.equipamento,equios.*');
        $this->db->select('equi.idEquipamentos, equi.equipamento,equios.slot');
        $this->db->from('equipamentos as equi');
        $this->db->join('equipamentos_os as equios','equi.idEquipamentos = equios.equipamentos_id','left');
        $this->db->where('equios.os_id', $idos);
        $this->db->where('equi.idFranquia', $idfranquia);
        $this->db->where('equi.clientes_id', $clientes_id);
        $query = $this->db->get();
        if ($query->num_rows != 0) {
            //foreach ($query->result_array() as $row) {
                return $query->result_array();
                //return $row;
            //}
        }else{
            return 'Verificar function dadosEquipamentos.';
        }
    }
    public function slotsDados($idfranquia,$prefix){
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('slots');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                //$row_result = array('label' =>  $row['idOs'], $row['idFranquias']);
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        return $row = json_decode($row['p']);                       
                    case 'M':
                    case 'm':
                        return $row = json_decode($row['m']);
                    case 'G':
                    case 'g':
                        return $row = json_decode($row['g']);
                }
                
            }
        }
        return 0;
    }
    public function slotsDadoscofre($idfranquia){
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('slots_cofre');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                //$row_result = array('label' =>  $row['idOs'], $row['idFranquias']);
                return $row = json_decode($row['slotscofre']);                       
            }
        }
        //return 0;
    }
    public function iconesEquiSlots($idos){
        $this->db->select('equipamentos.equipamento');
        //$this->select('equipamentos_os.*');
        //$this->select('os.*');
        $this->db->from('os');
        $this->db->join('equipamentos_os', 'os.idOs = equipamentos_os.os_id');
        $this->db->join('equipamentos', 'equipamentos_os.equipamentos_id = equipamentos.idEquipamentos');
        $this->db->where('idOs', $idos);
        $this->db->limit(1);
        $query = $this->db->get();
    
        foreach ($query->result_array() as $value) {
            switch ($nome = $value['equipamento']) {
                case 'Celular':
                case 'celular':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/cell.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                case 'Tablet':
                case 'tablet':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/tablet.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                
                case 'Notebook':
                case 'notebook':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/notebook.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                case 'Computador':
                case 'computador':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/pc.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                
                case 'Videogame':
                case 'videogame':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/videogame.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                case 'TV':
                case 'tv':
                    return '<a href='.base_url()."index.php/os/visualizar/".$idos.'><img class="tip-bottom iconimg" src="'.base_url().'assets/img/tv.png" data-original-title="Equipamento:&nbsp;'.$nome.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$idos.')"></a>';
                break;
                  
            }
            //return $icones;
        }
    }
    public function getSlotParaOS($prefix, $idFranquia = null, $qtd){
        $idFranquia = $idFranquia == null ? $this->session->userdata('id') : $idFranquia;
        $this->db->select('*');
        $this->db->where('idFranquias', $idFranquia);
        $this->db->limit(1);
        $query = $this->db->get('slots');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($prefix) {
                    case 'P':
                    case 'p':
                        $pdecode = json_decode($row['p'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                                if ($value[$i] == "0") {
                                    $livres[] = $key.','.$j.','.$value[$i];
                                    $vazio = 0;
                                }else if($value[$i] != "0"){
                                    $vazio = 1;
                                }
                            }
                        }
                        if ($vazio == 0) {
                            
                            for ($i=0; $i <= $limite ; $i++) { 
                                $keyvalor = explode(',', $livres[$i]);          
                            }
                            $keyupt = $keyvalor[0];
                            $valuind = $keyvalor[1];
                            $posicao = $keyupt.$valuind; 
                        }else if($vazio == 1){
                            $posicao = "Sem espaços livres, favor liberar Slots";
                        }
                        //$valoralter = $keyvalor[2] = $numidos;
                        //// fazendo o array com os dados acima, para insert do valor
                        //$alterdados =  array($keyupt => array($valuind => $valoralter) );
                        //// combinando os dados
                        //$combinados = array_replace_recursive($pdecode,$alterdados);
                        //// transformando novamente os dados combinados em json_decode
                        //json_encode($combinados);
                        break;
                    case 'M':
                    case 'm':
                        $pdecode = json_decode($row['m'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                                if ($value[$i] == "0") {
                                    $livres[] = $key.','.$j.','.$value[$i];
                                    $vazio = 0;
                                }else if($value[$i] != "0"){
                                    $vazio = 1;
                                }
                            }
                        }
                        if ($vazio == 0) {
                            
                            for ($i=0; $i <= $limite ; $i++) { 
                                $keyvalor = explode(',', $livres[$i]);          
                            }
                            $keyupt = $keyvalor[0];
                            $valuind = $keyvalor[1];
                            $posicao = $keyupt.$valuind; 
                        }else if($vazio == 1){
                            $posicao = "Sem espaços livres, favor liberar Slots";
                        }
                        break;
                    case 'G':
                    case 'g':
                        $pdecode = json_decode($row['g'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                                if ($value[$i] == "0") {
                                    $livres[] = $key.','.$j.','.$value[$i];
                                    $vazio = 0;
                                }else if($value[$i] != "0"){
                                    $vazio = 1;
                                }
                            }
                        }
                        if ($vazio == 0) {
                            
                            for ($i=0; $i <= $limite ; $i++) { 
                                $keyvalor = explode(',', $livres[$i]);          
                            }
                            $keyupt = $keyvalor[0];
                            $valuind = $keyvalor[1];
                            $posicao = $keyupt.$valuind; 
                        }else if($vazio == 1){
                            $posicao = "Sem espaços livres, favor liberar Slots";
                        }
                        break;
                }
                return $posicao;
            }
        }
        return false;
    }
    // pegando dados da franquia cadastrada.
    public function getDadosFranquias($idfranq){
        $this->db->select('*');
        $this->db->from('franquias');
        $this->db->where('idFranquias', $idfranq);
        $this->db->limit(1);
        
        //$query = $this->db->get();  
        return $this->db->get()->row();
        
        //return $query->result_array();
        //foreach ($query->result_array() as $row) {
        //    return $row;
        //}
    }
    // pegando os dados da OS já lançada.
    public function getSlotOcupado($prefix, $idFranquia, $idos){
        $idFranquia = $this->session->userdata('id');
        $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $qtd_fl_p = $franquia->qtd_fileiras_slots_p;
        $qtd_fl_m = $franquia->qtd_fileiras_slots_m;
        $qtd_fl_g = $franquia->qtd_fileiras_slots_g;
        $this->db->select('*');
        $this->db->where('idFranquias', $idFranquia);
        $this->db->limit(1);
        $query = $this->db->get('slots');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                switch ($prefix) {
                    case 'celular':
                    case 'Celular':                
                    case 'P':
                    case 'p':
                        $pdecode = json_decode($row['p'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd_fl_p ; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                            if ($value[$i] == $idos) {
                                    //$posicao[] = $key.$j; // usar esse aqui quando for para mais de um equipamento, mas tem gera bugs.
                                    $posicao = $key.$j;
                                }
                            }
                        }
                        if (@$posicao == NULL) {
                            return $posicao = "Verificar Status";
                        }else{
                            return $posicao;
                        }
                        
                        break;
                    case 'tablet':
                    case 'Tablet':
                    case 'notebook':
                    case 'Notebook':
                    case 'videogame':
                    case 'Videogame':
                    case 'M':
                    case 'm':
                        $pdecode = json_decode($row['m'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd_fl_m ; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                            if ($value[$i] == $idos) {
                                    $posicao = $key.$j;
                                }
                            }
                        }
                        if (@$posicao == NULL) {
                            return $posicao = "Verificar Status";
                        }else{
                            return $posicao;
                        }
                        break;
                    case 'computador':
                    case 'Computador':
                    case 'tv':
                    case 'TV':
                    case 'G':
                    case 'g':
                        $pdecode = json_decode($row['g'],true);
                        $limite = 0;
                        // pegando todos os espaços livres do array p
                        foreach ($pdecode as $key => $value) {
                            for ($j=1, $i = 0; $j <= $qtd_fl_g ; $j++, $i++) { 
                            // tratando os dados e formando um novo array
                            if ($value[$i] == $idos) {
                                    $posicao = $key.$j;
                                }
                            }
                        }
                        if (@$posicao == NULL) {
                            return $posicao = "Verificar Status";
                        }else{
                            return $posicao;
                        }
                        break;
                }
               
            }
        }
    }
}
