<?php
class Os_model extends CI_Model {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
    }

    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
                                    $idFranquia=$this->session->userdata('id');

        $this->db->select($fields.',clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->limit($perpage,$start);
        $this->db->order_by('idOs','desc');
        $this->db->where('idFranquias', $idFranquia);
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    function getByStatusOr($table,$id,$w1,$wv1,$w2,$wv2,$one=false){
        
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs','desc');
        if ($w1) $this->db->where($w1, $wv1);
        if ($w2) $this->db->or_where($w2, $wv2);
        $this->db->where('idFranquias', $id);
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    function getByStatusDif($table,$id,$w1,$wv1,$w2,$wv2,$one=false){
        
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs','desc');
        if ($w1) $this->db->where($w1, $wv1);
        if ($w2) $this->db->where($w2, $wv2);
        $this->db->where('idFranquias', $id);
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    function getStatus($table,$status,$id,$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select('idOs,dataInicial,dataFinal,garantia,descricaoProduto,visita,backup,status,observacoes,laudoTecnico,clientes.nomeCliente');
        $this->db->from($table);
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->order_by('idOs','desc');
        $this->db->where('status', $status);
        $this->db->where('idFranquias', $id);
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    function getStatusCount($table,$status,$id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('status', $status);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function getEquipamentoById($id)
    {
        $this->db->select('equipamentos.*, clientes.*');
        $this->db->from('equipamentos');
        $this->db->join('clientes','clientes.idClientes = equipamentos.clientes_id');
        $this->db->where('idEquipamentos',$id);
        $this->db->limit(1);
        return (array)$this->db->get()->row();
    }

    function getById($id){
        $this->db->select('os.*, clientes.*, usuarios.telefone AS telefoneUser, usuarios.email, usuarios.nome');
        $this->db->from('os');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id');
        $this->db->where('os.idOs',$id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null){
        
        $this->db->select('produtos_os.*, produtos.*');
        $this->db->from('produtos_os');
        $this->db->join('produtos','produtos.idProdutos = produtos_os.produtos_id');
        $this->db->where('os_id',$id);
        return $this->db->get()->result();
    }
    public function getEquipamentos($id = null)
    {
        $this->db->select('equipamentos_os.*, equipamentos.*');
        $this->db->from('equipamentos_os');
        $this->db->join('equipamentos','equipamentos.idEquipamentos = equipamentos_os.equipamentos_id');
        $this->db->where('os_id',$id);
        return $this->db->get()->result();
    }

    public function getServicos($id = null){
        $this->db->select('*');
        $this->db->from('servicos_os');
        $this->db->where('os_id',$id);
        return $this->db->get()->result();
    }
    
    public function getPagamentos($id = null){
        $this->db->select('*');
        $this->db->from('pagamento_os');
        $this->db->where('os_id',$id);
        return $this->db->get()->result();
    }
    
    public function getMarcaNome($idMarca)
    {
        $this->db->select('*');
        $this->db->where('idMarcas',$idMarca);
        $this->db->limit(1);
        $query = $this->db->get('marcas');
        if($query->num_rows < 1)
        	return "<não cadastrada>";
        else
        {
            foreach ($query->result_array() as $row)
        	return $row['nome'];
        }
    }
    
    public function getSlotMax($prefix)
    {
        $this->db->select('*');
        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('franquias');
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
            	switch ($prefix)
            	{
            		case 'P': case 'p': return $row['slotsp'];
            		case 'M': case 'm': return $row['slotsm'];
            		case 'G': case 'g': return $row['slotsg'];
            	}
            }
        }
        return 0;
    }    
    
    public function getSlotData($prefix, $slot)
    {
        $this->db->select('*');
        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('slots');        
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
            	switch ($prefix)
            	{
            		case 'P': case 'p': $array = $row['p']; break;
            		case 'M': case 'm': $array = $row['m']; break;
            		case 'G': case 'g': $array = $row['g']; break;
            		default: goto retzero;
            	}
            	
            	$equip_os = json_decode($array)[$slot];
            	if ($equip_os == 0)
            		goto retzero;
            	
	        $this->db->select('*');
	        $this->db->where('idEquipamentos_os', $equip_os);
	        $this->db->limit(1);
	        $query = $this->db->get('equipamentos_os');        
	        if($query->num_rows > 0)
	        {
	            foreach ($query->result_array() as $row)
            	    {
            	    	$os = $row['os_id'];            	
            		if ($os == 0)
            			goto retzero;
        
	        	$this->db->select('*');
		        $this->db->where('idEquipamentos', $row['equipamentos_id']);
		        $this->db->limit(1);
		        $query = $this->db->get('equipamentos');        
		        if($query->num_rows > 0)
		        {
		            foreach ($query->result_array() as $row)
        		    {
			    	return array('os'=> $os,
			    		     'tipo' => $row['tipo'],  // tipo de equipamento
			    		     'nome' => $this->getMarcaNome($row['marcas_id']).' '.$row['equipamento'].' '.$row['modelo']); // nome do equipamento
        		    }
        		}
            	    }
            	}
            }
        }
        
    retzero:     
    	return array('os'=>'0','tipo'=>'','nome'=>'');        
    }
    
    public function getSlotPrefix($equip_id)
    {
	$this->db->select('*');
        $this->db->where('idEquipamentos', $equip_id);
        $this->db->limit(1);
        $query = $this->db->get('equipamentos');    
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
            	switch ($row['tipo'])
            	{
            		case 'celular':  case 'tablet':    return 'M';            			
            		case 'notebook': case 'videogame': return 'G';            			
            		default: goto retzero;
            	}
            }
        }
    retzero:     
        return '';
    }
    
    public function getSlot($prefix)
    {
        $this->db->select('*');
        $this->db->where('idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        $query = $this->db->get('slots');        
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
            	switch ($prefix)
            	{
            		case 'P': case 'p': $array = $row['p']; break;
            		case 'M': case 'm': $array = $row['m']; break;
            		case 'G': case 'g': $array = $row['g']; break;
            		default: return false;
            	}
    		return json_decode($array);
    	     }
    	 }
    	 return false;
    }
    
    public function getSlotById($equip_id, $os_id)
    {
	$prefix = $this->getSlotPrefix($equip_id);	
    	$array = $this->getSlot($prefix);
    	if (!$array)
    		return -1;
    		
	$this->db->select('*');
        $this->db->where('os_id', $os_id);
        $this->db->where('equipamentos_id', $equip_id);
        $this->db->limit(1);
        $query = $this->db->get('equipamentos_os');  
        if($query->num_rows > 0)
            foreach ($query->result_array() as $row)
	    	for ($i = 0; $i < count($array); $i++)
	    	{
	    		if ($array[$i] == $row['idEquipamentos_os'])
	    			return $prefix.($i+1);
	    	}
    	return -1;
    }
    
    public function updSlot($cmd)
    {
	$this->db->where('idFranquias',$this->session->userdata('id'));
	$this->db->update('slots', $cmd); 
    }
    
    
    public function getSlotEmpty($equip_id)
    {
	$prefix = $this->getSlotPrefix($equip_id);	
    	$array = $this->getSlot($prefix);
    	if (!$array)
    		return -1;
    		
    	for ($i = 0; $i < count($array); $i++)
    	{
    		if ($array[$i] == 0)
    			return array('slot' => $i, 'array' => $array, 'prefix' => $prefix);
    	}
    	return -1;
    }	
        
    function addInAll($table,$data,$empty)
    {
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
        {
        	$array = $empty['array'];
	    	$array[$empty['slot']] = $this->db->insert_id($table);
		$array = json_encode($array);
		
		$this->updSlot(array(strtolower($empty['prefix']) => $array));
	    	return TRUE;
        }
	return FALSE;

    }
    
    function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
                        if($returnId == true){
                            return $this->db->insert_id($table);
                        }
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function deleteAll($table,$fieldID,$ID)
    {
    	deleteSlot($ID);
    	return delete($table,$fieldID,$ID);
    }
    
    function deleteSlot($ID)
    {
	$this->db->select('*');
        $this->db->where('idEquipamentos_os', $ID);
        $query = $this->db->get('equipamentos_os');  
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
		$prefix = $this->getSlotPrefix($row['equipamentos_id']);	
	    	$array = $this->getSlot($prefix);
	    	if (!$array)
	    		return
	    		
	    	$array[$row['slot']] = 0;
		$array = json_encode($array);
		$this->updSlot(array(strtolower($prefix) => $array));
            }
        }
    }
    
    function deleteEquipamento($id)
    {
	$prefix = $this->getSlotPrefix($id);	
    	$array = $this->getSlot($prefix);
	if (!$array)
    		continue;
    		
    	// SETA COMO ATENDIMENTO ENCERRADO
        $this->db->where('idEquipamentos_os', $id);
        $this->db->delete('equipamentos_os');
	
	// REMOVE DA TABELA SLOTS
	for ($i = 0; $i < count($array); $i++)
		if ($array[$i] == $id)
			$array[$i] = 0;
	$array = json_encode($array);
	$this->updSlot(array(strtolower($prefix) => $array));
	return TRUE;
    }
    
    function deleteListaEquipamentos($os)
    {
	$this->db->select('*');
        $this->db->where('os_id', $os);
        $query = $this->db->get('equipamentos_os'); 
        
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            {
		$prefix = $this->getSlotPrefix($row['equipamentos_id']);	
	    	$array = $this->getSlot($prefix);
    		if (!$array)
	    		continue;
	    		
	    	// SETA COMO ATENDIMENTO ENCERRADO
		$this->db->where('idEquipamentos_os',$row['idEquipamentos_os']);
		$this->db->where('os_id',$row['os_id']);
		$this->db->where('equipamentos_id',$row['equipamentos_id']);
		$this->db->where('modelo',$row['modelo']);
		$this->db->where('slot',$row['slot']);
		$this->db->update('equipamentos_os', array('slot'=>-1)); 
		
		// REMOVE DA TABELA SLOTS
	    	$array[$row['slot']] = 0;
		$array = json_encode($array);
		$this->updSlot(array(strtolower($prefix) => $array));
            }
        }
    }
    
    function delete($table,$fieldID,$ID)
    {
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() > 0)
		return true;
		
	return false;        
    }   

    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquias', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function autoCompleteProduto($q){
                            $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('descricao', $q);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('produtos');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['descricao'].' | Preço: R$ '.$row['precoVenda'].' | Estoque: '.$row['estoque'],'estoque'=>$row['estoque'],'id'=>$row['idProdutos'],'preco'=>$row['precoVenda']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q){
                            $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nomeCliente', $q);
        $this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('clientes');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nomeCliente'].' | Telefone: '.$row['telefone'],'id'=>$row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuario($q){
                            $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('situacao',1);
$this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('usuarios');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nome'].' | Telefone: '.$row['telefone'],'id'=>$row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteServico($q){
                            $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
$this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('servicos');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array
                (
                	'label'=>$row['nome'],
                	'id'=>$row['idServicos'],
                	'preco'=>$row['preco'],
                	'descricao'=>$row['descricao']
                );
            }
            echo json_encode($row_set);
        } 
    }
    
    
    public function autoCompleteEquipamento($q){
                            $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('equipamento', $q);
$this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('equipamentos');
        if($query->num_rows > 0)
        {
            foreach ($query->result_array() as $row)
            	$row_set[] = array('label'=>$row['equipamento'].' | Marca: '. $this->getMarcaNome($row['marcas_id']).' | Modelo: '.$row['modelo'] ,'id'=>$row['idEquipamentos']);
            	
            echo json_encode($row_set);
        }
    } 



    public function anexar($os, $anexo, $url, $thumb, $path){
        
        $this->db->set('anexo',$anexo);
        $this->db->set('url',$url);
        $this->db->set('thumb',$thumb);
        $this->db->set('path',$path);
        $this->db->set('os_id',$os);

        return $this->db->insert('anexos');
    }

    public function getAnexos($os){
        
        $this->db->where('os_id', $os);
        return $this->db->get('anexos')->result();
    }
}