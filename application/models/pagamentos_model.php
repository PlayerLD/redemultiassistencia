<?php
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Pagamentos_model extends CI_Model {
    protected $error_msg = array();
    protected $success_msg = array();
    protected $sess_destroy = false;
    protected $redirect = false;
    protected $status = array(
        0 => 'Failed',
        1 => 'new',
        2 => 'waiting',
        3 => 'paid',
        4 => 'unpaid',
        5 => 'refunded',
        6 => 'contested',
        7 => 'canceled',
        8 => 'link',
        9 => 'expired'
    );

    function __construct() {
      parent::__construct();
      $this->load->model('planos_model');
  }

  public function criar($set) {        

    $this->db->set($set);
    $this->db->insert('pagamentos');
    return $this->db->insert_id();
}

public function rel($pagamento_id, $plano_id, $qtd) {

        // $set = array('pagamento_id' => $pagamento_id, 'plano_id' => $plano_id, 'qtd' => $qtd);
        // $this->db->set($set);
        // $this->db->insert('pagamentos_itens');
  return;
}

public function editar($id, $set) {        

    $this->db->where('id', $id);
    $this->db->set($set);
    $this->db->update('pagamentos');
}

public function excluir($where) {        
    $this->db->delete('pagamentos', $where);
}

public function obter($where = array()) {        

    $this->db->from('pagamentos');
    $this->db->where($where);
    $this->db->limit(1);
    $pagamento = $this->db->get()->row();
    if(empty($pagamento))
        return;
    else{
            // $this->db->from('pagamentos_itens');
            // $this->db->where('pagamento_id', $pagamento->id);
            // $result = $this->db->get()->result();
            // $itens = array();
            // if(!empty($result)){
            //     foreach($result as $pi){
            //         $itens[] = $this->planos->getById($pi->plano_id);
            //     }
            //     $pagamento->itens = $itens;
            // }
    }
    $pagamento->status_t = $this->status[$pagamento->status];
    return $pagamento;
}

public function listar($where = array(), $limit = 0, $offset = 0) {

    $this->db->from('pagamentos');
    $this->db->where($where);
    $this->db->order_by('id', 'desc');
    if(!empty($limit))
        $this->db->limit($limit);
    if(!empty($offset))
        $this->db->offset($offset);
    $pagamentos = $this->db->get()->result();
    foreach ($pagamentos as $p) {
        $p->status_t = $this->status[$p->status];
            // $this->db->from('pagamentos_itens');
            // $this->db->where('pagamento_id', $p->id);
            // $result = $this->db->get()->result();
            // $itens = array();
            // if(!empty($result)){
            //     foreach($result as $pi){
            //         $itens[] = $this->produtos->obter(array('id' => $pi->plano_id), $pi->qtd);
            //     }
            //     $p->itens = $itens;
            // }
    }
    return $pagamentos;
}

public function verify($dados, $ajax, $redirect = false, $verificacao_orcamento = false){

    if($dados->permissoes_id != 1){
        if(
            //empty($dados->nome) || 
            //(strlen($dados->cpf) != 14) || 
            //strlen($dados->telefone) != 10 || 
            //(strlen($dados->celular) != 0 && strlen($dados->celular) != 11) || 
            //empty($dados->rua) || 
            //empty($dados->numero) || 
            //empty($dados->bairro) || 
            //empty($dados->cidade) || 
            //empty($dados->estado) || 
            //strlen($dados->cep) != 8 || 
            empty($dados->email)
            ){
            if($ajax == true){
                $json = array('result' => false, 'type' => '1' , 'error' => 'Não foi possível gerar a cobrança pois as informações do seu cadastro estão inválidas, entre em contato com o administrador.');
                echo json_encode($json);
                exit;
            }else{
                $this->session->set_flashdata('error', 'Não foi possível gerar a cobrança pois as informações do seu cadastro estão inválidas, entre em contato com o administrador.');
                $this->session->sess_destroy();
                redirect('sistemaos/login');
            }
        }

        $recorrencia = array(
            1 => 1,
            2 => 3,
            3 => 6,
        );

        $cartao = $this->planos_model->cartoes(array('franquia_id' => $dados->idFranquias));
        $plano1 =  $this->planos_model->getById($dados->id_plano);
        $plano2 =  $this->planos_model->getById($dados->id_plano2);

        if(!empty($plano1) || !empty($plano2)){
            if(!empty($plano1)){
                $this->verify_plano($plano1, $dados, $cartao, $ajax, $redirect, $verificacao_orcamento);
            }

            if(!empty($plano2)){
                $this->verify_plano($plano2, $dados, $cartao, $ajax, $redirect, $verificacao_orcamento);
            }

            if(!empty($this->error_msg)){
                    if(!$this->session->flashdata('error'))
                        $this->session->set_flashdata('error', $this->error_msg);

            }

            if(!empty($this->success_msg)){
                    if(!$this->session->flashdata('success'))
                        $this->session->set_flashdata('success', $this->success_msg);
            }
            
            if(!$ajax && $this->sess_destroy){
                $this->session->sess_destroy();
                redirect('sistemaos/login');
            }
            
            if(!$ajax && $this->redirect){
                    redirect('sistemaos');
            }
        }
            
            
    }else{
        $where = array(
            'status' => 0,
            'franquia_id > ' => 0
            );
        $pagamentos_erros = $this->planos_model->pagamentos($where);
        $erros = array();
        
        if(!empty($pagamentos_erros)){
            foreach($pagamentos_erros as $v){
                $erros[] = $v->boleto;
            }

            $this->session->set_flashdata('error', $erros);
        }
    }

    return;
}

function verify_plano($plano, $dados, $cartao, $ajax, $redirect = false, $verificacao_orcamento = false) {

    // Busca pagamentos em aberto no banco (status 3 = pago, status 0 = erro)
    $where = array(
        'plano_id'              => $plano->idPlanos,
        'franquia_id'           => $dados->idFranquias,
        'status !='             => '3',
        'status !='             => '0',
        'SUBSTR(data,1,10) >= ' =>  substr($plano->dataCadastro, 0, 8).$plano->dia_recorrencia, //O pagamento deve ter sido criado depois do plano
    );

    $pagamentos = $this->planos_model->pagamentos($where);
    if(count($pagamentos) <= 1){
        $where = array(
            'plano_id'                => $plano->idPlanos,
            'franquia_id'             => $dados->idFranquias,
            'SUBSTR(data, 1, 10) >= ' => date('Y-m-d', strtotime('-'.@$recorrencia[$plano->recorrencia].' month')),
            'SUBSTR(data,1,10) >= '   => substr($plano->dataCadastro, 0, 8).$plano->dia_recorrencia, //O pagamento deve ter sido criado depois do plano
        );

        $pagamento = $this->planos_model->pagamentos($where);
        $transacao = false;
        if(!empty($pagamento)){
            if($pagamento[0]->status != 3 && $pagamento[0]->status != 0){
                $data = substr($pagamento[0]->data, 0, 8).$plano->dia_recorrencia;
                $data = new DateTime($data);

                // verifica se pagamento com mais de 15 dias de atraso
                // antes da alteração do Jr.   if($data->format('Ymd') < date('Ymd', strtotime('-15 days'))){
					if($data->format('Ymd') < date('Ymd', strtotime('-20 days'))){
                    if($ajax == true){
                        $json = array('result' => false, 'type' => '1' , 'error' => 'Você tem um pagamento com mais de 15 dias de atraso e seu acesso foi bloqueado automaticamente, regularize sua unidade para acessar novamente.');
                        if(!empty($pagamento[0]->boleto))
                            $json['boleto'] = $pagamento[0]->boleto;

                        echo json_encode($json);
                        exit;
                    }else{
                        $this->error_msg[] = 'Você tem um pagamento com mais de 15 dias de atraso e seu acesso foi bloqueado automaticamente, regularize sua unidade para acessar novamente.';
                        $this->sess_destroy = true;
                    }
                // antes da alteração do Jr.        }elseif($data->format('Ymd') >= date('Ymd', strtotime('-15 days')) && $data->format('Ymd') <= date('Ymd')){
					}elseif($data->format('Ymd') >= date('Ymd', strtotime('-20 days')) && $data->format('Ymd') <= date('Ymd')){

                    if($verificacao_orcamento){
                        $json = array('result' => true, 'total' => 0);

                        echo json_encode($json);
                        exit;
                    }
                    if($redirect){
                        $this->error_msg[] = "Para visualizar os orçamentos, pague o boleto <a href=\"{$pagamento[0]->boleto}\" target=\"_blank\">clicando aqui.</a> Caso o boleto não seja pago em até 15 dias, o acesso será completamente bloqueado.";
                        $this->redirect = true;
                    }else{
                        $this->error_msg[] = "Você tem um pagamento em aberto ({$plano->nome}), evite o bloqueio automático da conta. Pague o boleto <a href=\"{$pagamento[0]->boleto}\" target=\"_blank\">clicando aqui.</a> Caso o boleto não seja pago em até 15 dias, o acesso será completamente bloqueado.";

                    }
                // antes da alteração do Jr.    }elseif(date('Ymd') >= date('Ymd', strtotime($data->format('Ymd').' -10 days')) && date('Ymd') <= $data->format('Ymd')){
					}elseif(date('Ymd') >= date('Ymd', strtotime($data->format('Ymd').' -20 days')) && date('Ymd') <= $data->format('Ymd')){
                    $this->success_msg[] = "Você tem um boleto que irá vencer em breve, pague o boleto antes do vencimento <a href=\"{$pagamento[0]->boleto}\" target=\"_blank\">clicando aqui</a> e evite bloqueios no sistema.";
                }
            }
        }else{

            if(!empty($plano)){
                $dados  = (object) array(
                    'os_id'        => (int)'',
                    'orcamento_id' => (int)'',
                    'plano_id'     => $plano->idPlanos,
                    'franquia_id'  => $dados->idFranquias,
                    'nome'         => $dados->nome,
                    'empresa'      => $dados->nome,
                    'cpf'          => (int)'',
                    'cnpj'         => $dados->cpf,
                    'telefone'     => $dados->telefone,
                    'celular'      => $dados->celular,
                    'logradouro'   => $dados->rua,
                    'numero'       => $dados->numero,
                    'complemento'  => (int)'',
                    'bairro'       => $dados->bairro,
                    'cidade'       => $dados->cidade,
                    'estado'       => $dados->estado,
                    'cep'          => $dados->cep,
                    'email'        => $dados->email,
                );

                if(!empty($cartao)){

                    $cartao  = (object) array(

                        'cpf'             => $cartao[0]->cpf_cartao,
                        // 'cartao_bandeira' => $bandeira,
                        'cartao_validade' => $cartao[0]->mes_cartao."/".$cartao[0]->ano_cartao,
                        'cartao_nome'     => $cartao[0]->nome_cartao,
                        'cartao_numero'   => $cartao[0]->num_cartao,
                        'cartao_codigo'   => $cartao[0]->cvv_cartao
                    );
                }

                $item    = array(
                    (object) array(
                        'id'          => $plano->idPlanos,
                        'titulo'      => $plano->nome,
                        'valor'       => $plano->valor,
                            // 'desconto' => $plano->,
                        'qty'         => 1
                    )
                );
                $total    = $item[0]->valor;

                $pag = $this->pagamento($dados, $item, $total, $cartao, $plano->dia_recorrencia, count($pagamentos));
            }
            // else{
            //     if($ajax == true){
            //         $json = array('result' => false, 'type' => '1' , 'error' => 'Sua franquia não tem um plano definido.');
            //         echo json_encode($json);
            //         exit;
            //     }else{
            //         $this->session->set_flashdata('e$this->error_msg);
            //         $this->session->sess_destroy();
            //         redirect('sistemaos/login');
            //     }
            // }
        }
    }else{
        if($ajax == true){
            $json = array('result' => false, 'type' => '1' , 'error' => 'Você tem mais de dois pagamento em aberto e seu acesso foi bloqueado automaticamente, regularize sua franquia para acessar novamente.');
            if(!empty($pagamentos[0]->boleto))
                $json['boleto'] = $pagamentos[0]->boleto;

            echo json_encode($json);
            exit;
        }else{
            $this->error_msg[] = 'Você tem mais de dois pagamento em aberto e seu acesso foi bloqueado automaticamente, regularize sua franquia para acessar novamente.';
            $this->sess_destroy = true;
        }
    }
}

function pagamento($dados, $itens, $total, $cartao = array(), $dia_recorrencia = 1, $blocked = false) {
    $pagamento = !empty($cartao) ? 'cartao' : 'boleto';

    // $bandeira = $this->bcash->get_payment_method($cartao->cartao_bandeira);
            // Pedidos
    $data_expiracao = date("Y-m-{$dia_recorrencia}");
    if($data_expiracao < date("Y-m-d", strtotime("+3 days")))
        $data_expiracao = date('Y-m-d', strtotime('+3 days'));

    $pagamentos_id = $this->criar(array(
        'os_id'         => (int)@$dados->os_id,
        'plano_id'      => (int)@$dados->plano_id,
        'franquia_id'   => (int)@$dados->franquia_id,
        'orcamento_id'  => (int)@$dados->orcamento_id,
        'data'          => $data_expiracao." 00:00:00",
        'total'         => $total,
    ));
    if(!empty($dados->clientId) && !empty($dados->clientSecret)){
        $clientId = $dados->clientId;
        $clientSecret = $dados->clientSecret;
    }else{
        $clientId = 'Client_Id_946fe867391443411ea92a569460cac54a4dc9c4'; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
        $clientSecret = 'Client_Secret_6d9be7458f2df3c4eeb196dcfaf8f56b91499e14'; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
    }
    $options = [
      'client_id' => $clientId,
      'client_secret' => $clientSecret,
      ];



  $products = array();
  foreach($itens as $k => $item) {

        // Adiciona o produto ao pedido
    $products[$k]['name'] = "Multi Assistência - {$item->titulo}";
    $products[$k]['amount'] = 1;
    $products[$k]['value'] = (int)str_replace('.', '', $item->valor);
}

$products = array_values($products);

        //Exemplo para receber notificações da alteração do status da transação.
$metadata = array(
    'custom_id' => (string)$pagamentos_id,
    'notification_url' => site_url("sistemaos/notificacao/{$pagamentos_id}")
);
        //Outros detalhes em: https://dev.gerencianet.com.br/docs/notificacoes

        //Como enviar seu $body com o $metadata
$body  =  array(
   'items' => $products,
   'metadata' => $metadata
);

try {
    $api = new Gerencianet($options);
    $charge = $api->createCharge([], $body);

    if(isset($charge['data']['charge_id'])){
        if($pagamento == 'boleto'){
            $this->editar($pagamentos_id, array(
                'bcash_id'      => $charge['data']['charge_id'],
                'bcash_token'   => isset($charge['data']['custom_id']) ? $charge['data']['custom_id'] : ''
            ));

                    // ID da transação gerada anteriormente
            $params = [
              'id' => $charge['data']['charge_id']
            ];

            $customer = [
                'name' => $dados->nome,
                'phone_number' => !empty($dados->celular)? $dados->celular : $dados->telefone,
                'email' => $dados->email
            ];

            if(!empty($dados->cnpj)){
                $customer['juridical_person'] = (object)array(
                    'corporate_name' => $dados->empresa,
                    'cnpj' => $dados->cnpj
                );
            }else{
                $customer['cpf'] = !empty($cartao->cpf) ? $cartao->cpf : $dados->cpf;
            }

                $bankingBillet = [
                          'expire_at' => $data_expiracao, // data de vencimento do boleto (formato: YYYY-MM-DD)
                          'customer' => $customer
                      ];

                      $payment = [
                          'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
                      ];

                      $body = [
                          'payment' => $payment
                      ];

                try {
                    $api = new Gerencianet($options);
                    $charge = $api->payCharge($params, $body);

                    $this->excluir(array('franquia_id' => $dados->franquia_id,'status' => 0));
                    $this->editar($pagamentos_id, array(
                        'boleto'      => $charge['data']['link']
                    ));

                    $msg = "Você tem um novo pagamento em aberto no valor de R$".(number_format($total, 2, ',', '.')).". Efetue o pagamento e evite o bloqueio automático de sua conta.<br><a href=\"{$charge['data']['link']}\" target=\"_blank\" style=\"color: white;text-decoration: none;background: #0f70b7;padding: 10px 20px;font-size: 18px;margin-top: 30px;display: inline-block;\">Pagar boleto</a>";
                    $this->email_pedido($pagamentos_id, $msg);

                } catch (GerencianetException $e) {
                    $this->excluir(array('franquia_id' => $dados->franquia_id,'status' => 0));
                    $this->editar($pagamentos_id, array(
                        'boleto'      => 'Erro ao gerar boleto para '.$dados->nome.' - #'.$e->code.': '.(is_array($e->errorDescription) ? $e->errorDescription['message'] : $e->errorDescription),
                        'status'      => 0
                    ));
                } catch (Exception $e) {
                    $this->excluir(array('franquia_id' => $dados->franquia_id,'status' => 0));
                    $this->editar($pagamentos_id, array(
                        'boleto'      => 'Erro ao gerar boleto para '.$dados->nome.' - #'.$e->getMessage(),
                        'status'      => 0
                    ));
                }
            }elseif($pagamento == 'cartao'){

                $this->editar($pagamentos_id, array(
                    'bcash_id'      => $charge['data']['charge_id'],
                    'bcash_token'   => isset($charge['data']['custom_id']) ? $charge['data']['custom_id'] : ''
                ));

                // $charge_id refere-se ao ID da transação gerada anteriormente
                $params = [
                  'id' => $charge['data']['charge_id']
                ];
                 
                $paymentToken = $cartao->token; // payment_token obtido na 1ª etapa (através do Javascript único por conta Gerencianet)
                $customer = [
                  'name' => $dados->nome, // nome do cliente
                  'cpf' => $cartao->cpf , // cpf do cliente
                  'email' => $dados->email , // endereço de email do cliente
                  'phone_number' => !empty($dados->celular)? $dados->celular : $dados->telefone, // telefone do cliente
                  'birth' => $cartao->nascimento // data de nascimento do cliente
                ];
                 
                $billingAddress = [
                  'street' => $dados->logradouro,
                  'number' => (int)$dados->numero,
                  'neighborhood' => $dados->bairro,
                  'city' => $dados->cidade,
                  'state' => $dados->estado,
                  'zipcode' => $dados->cep,
                ];

                $creditCard = [
                  'installments' => (int)$cartao->vezes, // número de parcelas em que o pagamento deve ser dividido
                  'billing_address' => $billingAddress,
                  'payment_token' => $paymentToken,
                  'customer' => $customer
                ];
                 
                $payment = [
                  'credit_card' => $creditCard // forma de pagamento (credit_card = cartão)
                ];

                $body = [
                  'payment' => $payment
                ];
                 
                 
                try {
                    $api = new Gerencianet($options);
                    $charge = $api->payCharge($params, $body);

                    $msg = "Parabens, seu pagamento referente ao orçamento #{$dados->orcamento_id} foi efetuado com sucesso!";
                    $this->email_pedido($pagamentos_id, $msg);
                    return $msg;

                } catch (GerencianetException $e) {
                    print_r($e->errorDescription);
                    $this->excluir(array('orcamento_id' => $dados->orcamento_id,'status' => 0));
                    $this->editar($pagamentos_id, array(
                        'boleto'      => 'Erro no pagamento do cartão para o orcamento #'.$dados->orcamento_id.' - #'.$e->code.': '.(is_array($e->errorDescription) ? $e->errorDescription['message'] : $e->errorDescription),
                        'status'      => 0
                    ));
                } catch (Exception $e) {
                    $this->excluir(array('orcamento_id' => $dados->orcamento_id,'status' => 0));
                    $this->editar($pagamentos_id, array(
                        'boleto'      => 'Erro no pagamento do cartão para o orcamento #'.$dados->orcamento_id.' - #'.$e->getMessage(),
                        'status'      => 0
                    ));
                }
            }

        }

        if($blocked){
            if($ajax == true){
                $json = array('result' => false, 'type' => '1' , 'error' => 'Você tem mais de dois pagamento em aberto e seu acesso foi bloqueado automaticamente, regularize sua franquia para acessar novamente.');
                if(!empty($charge['data']['link']))
                    $json['boleto'] = $charge['data']['link'];

                echo json_encode($json);
                exit;
            }else{
                $this->session->set_flashdata('error', 'Você tem mais de dois pagamento em aberto e seu acesso foi bloqueado automaticamente, regularize sua franquia para acessar novamente.');
                $this->session->sess_destroy();
                redirect('sistemaos/login');
            }
        }

    } catch (GerencianetException $e) {
        $this->editar($pagamentos_id, array(
            'boleto'      => 'Erro ao gerar pagamento para '.$dados->nome.' - #'.$e->code.': '.(is_array($e->errorDescription) ? $e->errorDescription['message'] : $e->errorDescription),
            'status'      => 0
        ));
    } catch (Exception $e) {
        $this->editar($pagamentos_id, array(
            'boleto'      => 'Erro ao gerar pagamento para '.$dados->nome.' - #'.$e->getMessage(),
            'status'      => 0
        ));
    }
}

public function email_pedido($pagamentos_id, $msg = ''){
    $pagamento    = $this->pagamentos_model->obter(array('id'=>$pagamentos_id));

    if(!empty($pagamento->os_id)){
            // $equipamentos = $this->os_model->getEquipamentos($pagamento->os_id);
        $os           = $this->os_model->getById($pagamento->os_id);
        $cliente      =  $this->clientes_model->getById($os->clientes_id);
        $franquia     = $this->franquias_model->getById($os->idFranquia);

        $data = array
        (
            'os_id'     => $pagamento->os_id,
            'formaPg'   => 'Cartão de crédito (ONLINE)',
            'valorPg'   => $pagamento->total,
            'valorDesc' => 0,
            'data'      => date('Y-m-d H:i:s.000000'),
            'dadosPg'   => "Pagamento total feito online"
        );
        
        $this->os_model->add('pagamento_os', $data);

    }elseif(!empty($pagamento->franquia_id)){
        $plano     = $this->planos_model->getById($pagamento->plano_id);
        $franquia     = $this->franquias_model->getById($pagamento->franquia_id);

        if(!empty($plano) && !empty($pagamento->boleto)){
            $data = array(
                'mensagem' => $msg
                );
            $message = $this->load->view('os/template_email_resposta', $data, true);

            $from     = 'sac@redemultiassistencia.com.br';
            $fromname = "Rede Multi Assistência";
            $to       = $franquia->email;
            $toname   = $franquia->nome;
            $subject  = 'PAGAMENTO #'.$pagamento->bcash_id.' - ' . $plano->nome;
            $this->load->library('email');
            $this->email->from($from, $fromname);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }
    }
    return;

}
}