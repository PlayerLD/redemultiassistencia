<?php
class Franquias_model extends CI_Model {
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     *    
     */
    
    function __construct() {
        parent::__construct();
    }
    function getByStatusOrcamento($status = 1, $equip = '') {
        $this->db->from('franquias');
        $this->db->select('idFranquias, nome, cidade, estado');
        $this->db->where('statusOrcamento', $status);
        // $this->db->like('permissoes_equip', $equip);
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }
    function get($perpage=0,$start=0,$one=false){
        
        $this->db->from('franquias');
        $this->db->select('franquias.*, permissoes.nome as permissao');
        $this->db->limit($perpage,$start);
        $this->db->join('permissoes', 'franquias.permissoes_id = permissoes.idPermissao', 'left');
        $this->db->where('id_pai IS NULL');
  
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    function listar($where = 'id_pai IS NULL'){
        
        $this->db->select('franquias.*, permissoes.nome as permissao');
        $this->db->from('franquias');
        $this->db->join('permissoes', 'franquias.permissoes_id = permissoes.idPermissao', 'left');
        $this->db->where($where);
  
        $result = $this->db->get()->result();
         
        return $result;
    }
     function getAllTipos(){
        $this->db->where('situacao',1);
        return $this->db->get('tiposFranquia')->result();
    } 
    function getById($id){
        $this->db->select('f.*, fr.nome as nome_pai, c.recorrencia, c.nome_cartao, c.cpf_cartao, c.num_cartao, c.mes_cartao, c.ano_cartao, c.cvv_cartao, c.data as data_cartao, c.dia_recorrencia, f.nome as permissao',$id);
        $this->db->where('f.idFranquias',$id);
        $this->db->join('franquias as fr', 'f.id_pai = fr.idFranquias', 'left outer');
        $this->db->join('cartoes as c', 'f.idFranquias = c.franquia_id', 'left outer');
        $this->db->join('permissoes as p', 'p.idPermissao = f.permissoes_id', 'left');
        $this->db->limit(1);
        return $this->db->get('franquias as f')->row();
    }

    // Ainda Falta Criar Limpar e Reconfigurar Slots

    // nova function para pegar dados de slots da table de slots.
    function getSlotsFranq($id){
        $this->db->select('*');
        $this->db->where('idFranquias',$id);
        $this->db->limit(1);
        return $this->db->get('slots')->row();
    }

    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
        {

            $qtdp     = $data['slotsp'];
            $qtdm     = $data['slotsm'];
            $qtdg     = $data['slotsg'];
            //$qtdtot   = $data['fileiras_slots'];
            $qtd_fl_p = $data['qtd_fileiras_slots_p'];       
            $qtd_fl_m = $data['qtd_fileiras_slots_m'];   
            $qtd_fl_g = $data['qtd_fileiras_slots_g']; 

            $arrayletras = array('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        
            $slots_p = [];
            $slots_m = [];
            $slots_g = [];

            for ($j=1; $j <= $qtdp; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasp[] = $l[$j-1];
                }
            }

            for ($j=1; $j <= $qtdm; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasm[] = $l[$j-1];
                }
            }

             for ($j=1; $j <= $qtdg; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasg[] = $l[$j-1];
                }
            }

            //$letrasarray = implode(',',$numletras);
            
            for ($i = 1, $v = 0; $i <= $qtdp; $i++, $v++) {
                $slots_p[$numletrasp[$v]] = array_fill(0, $qtd_fl_p, '0');
            }

            for ($i = 1, $v = 0; $i <= $qtdm; $i++, $v++) {
                $slots_m[$numletrasm[$v]] = array_fill(0, $qtd_fl_m, '0');
            }

            for ($i = 1, $v = 0; $i <= $qtdg; $i++, $v++) {
                $slots_g[$numletrasg[$v]] = array_fill(0, $qtd_fl_g, '0');
            }

            $pslots = json_encode($slots_p);
            $mslots = json_encode($slots_m);
            $gslots = json_encode($slots_g);

           
            // Adiciona slots           
            //$cp = $data['slotsp']; // total de slots pequenos
            //$sp = array($cp); for ($i = 0; $i < $cp; $i++) $sp[$i] = 0; $sp = json_encode($sp);
            //
            //$cm = $data['slotsm']; // total de slots médios
            //$sm = array($cm); for ($i = 0; $i < $cm; $i++) $sm[$i] = 0; $sm = json_encode($sm);
            //
            //$cg = $data['slotsg']; // total de slots grandes
            //$sg = array($cg); for ($i = 0; $i < $cg; $i++) $sg[$i] = 0; $sg = json_encode($sg);
                        
            $dataSlots = array('idFranquias' => $this->db->insert_id(), 'p' => $pslots, 'm' => $mslots, 'g' => $gslots);            
            $this->db->insert('slots', $dataSlots);
            if ($this->db->affected_rows() == '1')
                return  $dataSlots['idFranquias'];
        }
        
        return FALSE;       
    }
    
    function addCartao($data){
        $this->db->from('cartoes');
        
        if(!empty($data['franquia_id']))
        $this->db->where('franquia_id', $data['franquia_id']);
        
        if(!empty($data['cliente_id']))
        $this->db->where('cliente_id', $data['cliente_id']);

        if(!empty($data['orcamento_id']))
        $this->db->where('orcamento_id', $data['orcamento_id']);
  
        $result = $this->db->get()->row();
        if(empty($result)){
            $this->db->insert('cartoes', $data);  
        }elseif(!empty($data['franquia_id'])){
            if($data['franquia_id'] == $result->franquia_id){
                $this->db->where('franquia_id',$data['franquia_id']);
                $this->db->update('cartoes', $data);
            }
        }elseif(!empty($data['cliente_id'])){
            if($data['cliente_id'] == $result->cliente_id){
                $this->db->where('cliente_id',$data['cliente_id']);
                $this->db->update('cartoes', $data);
            }
        }elseif(!empty($data['orcamento_id'])){
            if($data['orcamento_id'] == $result->orcamento_id){
                $this->db->where('orcamento_id',$data['orcamento_id']);
                $this->db->update('cartoes', $data);
            }
        }

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE;       
    }
    
    function info_email($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);
    }
    
    function excluir_ass_email($id){
        $this->db->select('ass_email');
        $this->db->from('franquias');
        $this->db->where('idFranquias', $id);
  
        $result = $this->db->get()->row();
        if(!empty($result)){
            $this->db->where('idFranquias',$id);
            $this->db->update('franquias', array('ass_email' => ''));
            unlink(str_replace('/system', '', BASEPATH).'assets/uploads/'.$result->ass_email);
        }
    }
    
    function excluir_email_marketing($id){
        $this->db->select('email_marketing');
        $this->db->from('franquias');
        $this->db->where('idFranquias', $id);
  
        $result = $this->db->get()->row();
        if(!empty($result)){
            $this->db->where('idFranquias',$id);
            $this->db->update('franquias', array('email_marketing' => ''));
            unlink(str_replace('/system', '', BASEPATH).'assets/uploads/'.$result->email_marketing);
        }
    }
    
    function obter_ass_email($ID){
        $this->db->select('ass_email');
        $this->db->from('franquias');
        $this->db->where('idFranquias', $ID);
  
        $result = $this->db->get()->row();
        
        return !empty($result) ? base_url().'assets/uploads/'.$result->ass_email : '';
    }
    
    function obter_email_marketing($ID){
        $this->db->select('email_marketing');
        $this->db->from('franquias');
        $this->db->where('idFranquias', $ID);
  
        $result = $this->db->get()->row();
        
        return !empty($result) ? base_url().'assets/uploads/'.$result->email_marketing : '';
    }
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);
        /*if ($this->db->affected_rows() >= 0)
        {
            $this->load->model('os_model');
            $slotsP = (array) $this->os_model->getSlot('p', $ID);
            $slotsM = (array) $this->os_model->getSlot('m', $ID);
            $slotsG = (array) $this->os_model->getSlot('g', $ID);

            $franquia = $this->franquias_model->getById($this->session->userdata('id'));
            $nump = $franquia->slotsp;

            $cp = $data['slotsp'];
            $cm = $data['slotsm']; 
            $cg = $data['slotsg']; 
            $qtd_fl_p = $data['qtd_fileiras_slots_p'];
            $qtd_fl_m = $data['qtd_fileiras_slots_m'];
            $qtd_fl_g = $data['qtd_fileiras_slots_g'];
            
            $arrayletras = array('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
            
            $slots_p = [];
            $slots_m = [];
            $slots_g = [];
            // SLOT PEQUENO
            for ($j=1; $j <= $cp; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasp[] = $l[$j-1];
                }
            }
            
            for ($i = 1, $v = 0; $i <= $cp; $i++, $v++) {
                if (isset($data['slotsp'])) {
                    $slots_p[$numletrasp[$v]] = array_fill(0, $qtd_fl_p, '0');
                }
            }

            $pslots = json_encode($slots_p);
            

            // SLOT MÉDIO
            for ($j=1; $j <= $cm; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasm[] = $l[$j-1];
                }
            }
            for ($i = 1, $v = 0; $i <= $cm; $i++, $v++) {
                if (isset($data['slotsm'])) {
                    $slots_m[$numletrasm[$v]] = array_fill(0, $qtd_fl_m, '0');
                }
            }
            $mslots = json_encode($slots_m);
            // SLOT GRANDE
            for ($j=1; $j <= $cg; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasg[] = $l[$j-1];
                }
            }
            for ($i = 1, $v = 0; $i <= $cg; $i++, $v++) {
                if (isset($data['slotsg'])) {
                    $slots_g[$numletrasg[$v]] = array_fill(0, $qtd_fl_g, '0');
                }
            }
            $gslots = json_encode($slots_g);

            $this->os_model->updSlot(array('p' => $pslots), $ID);
            $this->os_model->updSlot(array('m' => $mslots), $ID);
            $this->os_model->updSlot(array('g' => $gslots), $ID);
            return TRUE;
        }*/
        
        return FALSE;       
    }

    function editarSlots($table,$data,$idfranq){

        $this->db->where('idFranquias',$idfranq);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0){

        
            $this->load->model('os_model');
            $slotsP = (array) $this->os_model->getSlot('p', $idfranq);
            $slotsM = (array) $this->os_model->getSlot('m', $idfranq);
            $slotsG = (array) $this->os_model->getSlot('g', $idfranq);

            $franquia = $this->franquias_model->getById($this->session->userdata('id'));
            $nump = $franquia->slotsp;

            $cp = $data['slotsp'];
            $cm = $data['slotsm']; 
            $cg = $data['slotsg']; 
            $qtd_fl_p = $data['qtd_fileiras_slots_p'];
            $qtd_fl_m = $data['qtd_fileiras_slots_m'];
            $qtd_fl_g = $data['qtd_fileiras_slots_g'];
            
            $arrayletras = array('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
            
            $slots_p = [];
            $slots_m = [];
            $slots_g = [];
            // SLOT PEQUENO
            for ($j=1; $j <= $cp; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasp[] = $l[$j-1];
                }
            }
            
            for ($i = 1, $v = 0; $i <= $cp; $i++, $v++) {
                if ($data['slotsp']) {
                    $slots_p[$numletrasp[$v]] = array_fill(0, $qtd_fl_p, '0');
                }else{
                    $slots_p = $slotsP[$v];
                }
            }

            $pslots = json_encode($slots_p);
            

            // SLOT MÉDIO
            for ($j=1; $j <= $cm; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasm[] = $l[$j-1];
                }
            }
            for ($i = 1, $v = 0; $i <= $cm; $i++, $v++) {
                if (isset($data['slotsm'])) {
                    $slots_m[$numletrasm[$v]] = array_fill(0, $qtd_fl_m, '0');
                }
            }
            $mslots = json_encode($slots_m);
            // SLOT GRANDE
            for ($j=1; $j <= $cg; $j++) {
                foreach ($arrayletras as $l) {
                    $numletrasg[] = $l[$j-1];
                }
            }
            for ($i = 1, $v = 0; $i <= $cg; $i++, $v++) {
                if (isset($data['slotsg'])) {
                    $slots_g[$numletrasg[$v]] = array_fill(0, $qtd_fl_g, '0');
                }
            }
            $gslots = json_encode($slots_g);

            $this->os_model->updSlot(array('p' => $pslots), $idfranq);
            $this->os_model->updSlot(array('m' => $mslots), $idfranq);
            $this->os_model->updSlot(array('g' => $gslots), $idfranq);
            return TRUE;
        }
        return FALSE;       

    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE;        
    }   
    
    function count($table, $id_pai = ''){
        if(empty($id_pai))
            return $this->db->count_all($table);
        else{
            $this->db->from($table);
            $this->db->where('id_pai', $id_pai);
            return $this->db->get()->num_rows();
        }
    }
}