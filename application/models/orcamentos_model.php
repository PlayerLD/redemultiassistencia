<?php
class Orcamentos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
    }
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $idFranquia = $this->session->userdata('id');
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idOrcamento','desc');
        $this->db->limit($perpage,$start);
        if ($idFranquia != "1"){
            $this->db->where('idFranquia', $idFranquia);
        } 

        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    // criando function para server side processing
    function count_totalOrcamentos(){ 
        $query = $this->db->where('idFranquia', $this->session->userdata('id'));
        $query = $this->db->get('orcamentos');
        return $query->num_rows();
    }

    function allOrcamento($limit,$start,$col,$dir){
        $query = "SELECT orcamentos.*,franquias.idFranquias, IF(filial.nome IS NOT NULL, filial.nome, franquias.nome) AS franquianome, IF(filial.nome IS NOT NULL, 'Sim', 'Não') AS as_filial 
        FROM orcamentos 
        JOIN franquias ON orcamentos.idFranquia = franquias.idFranquias 
        LEFT OUTER JOIN franquias as filial ON orcamentos.idFilial = filial.idFranquias 
        WHERE orcamentos.idFranquia = {$this->session->userdata('id')}
        ORDER BY orcamentos.status, orcamentos.data desc
        LIMIT {$limit} OFFSET {$start}";
        return $this->db->query($query)->result();
    }
    
    function idOrcamento_search($limit,$start,$search,$col,$dir){

        $query = "SELECT orcamentos.*,franquias.idFranquias, IF(filial.nome IS NOT NULL, filial.nome, franquias.nome) AS franquianome, IF(filial.nome IS NOT NULL, 'Sim', 'Não') AS as_filial 
        FROM orcamentos 
        JOIN franquias ON orcamentos.idFranquia = franquias.idFranquias 
        LEFT OUTER JOIN franquias as filial ON orcamentos.idFilial = filial.idFranquias 
        WHERE orcamentos.idFranquia = {$this->session->userdata('id')} AND
        (
            idOrcamento LIKE '%{$search}%' OR
            orcamentos.nome LIKE '%{$search}%' OR
            orcamentos.telefone LIKE '%{$search}%' OR
            orcamentos.tipo LIKE '%{$search}%'
        )
        ORDER BY orcamentos.status, orcamentos.data desc
        LIMIT {$limit} OFFSET {$start}";
        $result = $this->db->query($query);

        if($result->num_rows()>0){
            return $result->result();
        }else{
            return null;
        }
    }
    function orcamento_search_count($search){
        $this->db->where('idFranquia', $this->session->userdata('id'));
        $this->db->like('idOrcamento',$search);
        $this->db->or_like('idOrcamento',$search);
        $query = $this->db->get('orcamentos');
        return $query->num_rows();
    }
    // criando function para server side processing



    function getById($id)
    {
        $this->db->select('*');
        $this->db->from('orcamentos');
        $this->db->where('idOrcamento', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    function add($table, $data, $returnId = false)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }
    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }
    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquia', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }
    function updateStatus($id, $status)
    {
        $this->db->where('idOrcamento', $id);
        $this->db->update('orcamentos', array('status' => $status));
    }
  
  function updateRespostaEmail($id, $resposta)
    {
        $this->db->where('idOrcamento', $id);
    
    $dataResposta = date('Y-m-d H:i:s');
    
        $this->db->update('orcamentos', array('emailResposta' => $resposta, 'dataResposta' => $dataResposta));
    }
}