<?php
class Clientes_model extends CI_Model {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
    } 


	function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
		$idFranquia=$this->session->userdata('id');

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idClientes','desc');
        $this->db->limit($perpage,$start);
		if ($idFranquia != "1"){
			$this->db->where('idFranquia', $idFranquia);
		}
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    } 

    function getById($id){
        $this->db->where('idClientes',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
	
	function addReturnId($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return $this->db->insert_id();
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1'){
			return TRUE;
		}
		
		return FALSE;        
    }

    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquia', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function getOsByCliente($id){
        $this->db->where('clientes_id',$id);
        $this->db->order_by('idOs','desc');
        $this->db->limit(10);
        return $this->db->get('os')->result();
    }
    public function getEquipamentoByCliente($id){
        $this->db->where('clientes_id',$id);
        $this->db->order_by('idEquipamentos','desc');
        $this->db->limit(10);
        return $this->db->get('equipamentos')->result();
    }

}