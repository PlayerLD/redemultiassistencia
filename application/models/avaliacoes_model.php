<?php
class Avaliacoes_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $idFranquia = $this->session->userdata('id');

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idAvaliacao','desc');
        $this->db->join("os", "os.idOs = avaliacoes.os");
        $this->db->join("clientes c", "c.idClientes = os.clientes_id");
        $this->db->limit($perpage,$start);
        $this->db->where('os.idFranquias', $idFranquia);

        $query = $this->db->get();

        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->select('*');
        $this->db->from('orcamentos');
        $this->db->where('idOrcamento', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function add($table, $data, $returnId = false)
    {

        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join("os", "os.idOs = avaliacoes.os");
        $this->db->where('os.idFranquias', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() > 0)
            return true;

        return false;
    }

    function updateStatus($id, $status)
    {
        $this->db->where('idAvaliacao', $id);
        $this->db->update('avaliacoes', array('status' => $status));
    }

    function getOsById($idOs) {
        $this->db->select('os.*, c.nomeCliente, e.equipamento, e.modelo, e.defeito, a.idAvaliacao, a.obs AS obsAvaliacao, a.nota');
        $this->db->from('os');
        $this->db->join('clientes c', 'c.idClientes = os.clientes_id');
        $this->db->join('equipamentos_os eo', 'eo.os_id = os.idOs', 'left');
        $this->db->join('equipamentos e', 'e.idEquipamentos = eo.equipamentos_id', 'left');
        $this->db->join('avaliacoes a', 'a.os = os.idOs', "left");
        $this->db->where('os.idOs', $idOs);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

}