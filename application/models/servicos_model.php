<?php
class Servicos_model extends CI_Model {


    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
    }  

    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $idFranquia=$this->session->userdata('id');
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idServicos','desc');
        $this->db->limit($perpage,$start);
        if ($idFranquia != "1"){
        $this->db->where('idFranquia', $idFranquia);
        }
              
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


     // criando function para server side processing
    function count_totalServicos(){ 
        $query = $this->db->where('idFranquia', $this->session->userdata('id'));
        $query = $this->db->get('servicos');
        return $query->num_rows();
    }

    function allServicos($limit,$start,$col,$dir){
        $this->db->select('*');
        $this->db->from('servicos');
        $this->db->where('servicos.idFranquia', $this->session->userdata('id'));
        //$this->db->join('franquias', 'franquias.idFranquias = orcamentos.idFranquia');
        //$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }else{
            return null;
        }
    }
    function idServicos_search($limit,$start,$search,$col,$dir){
        //$this->db->select('*');
        //$this->db->from('servicos');
        //$this->db->where('servicos.idFranquia', $this->session->userdata('id'));
        //$this->db->like('idServicos',$search);
        //$this->db->or_like('nome',$search);
        //$this->db->or_like('descricao',$search);
        //$this->db->or_like('preco',$search);
        // codigo alterado para evitar conflitos de pesquisas, devido a versão de mysql presente nesta versão do framework
        $idfranquia =  $this->session->userdata('id');
        $fields = " * FROM servicos 
        WHERE servicos.idFranquia = '".$idfranquia."'AND servicos.idServicos LIKE'%".$search."%'
        OR nome LIKE'%".$search."%' 
        OR descricao LIKE'%".$search."%' 
        OR preco LIKE'%".$search."%' 
        HAVING servicos.idFranquia = '".$idfranquia."'";  
        $this->db->select($fields);
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return null;
        }
    }
    function servicos_search_count($search){
        $this->db->where('idFranquia', $this->session->userdata('id'));
        $this->db->like('idServicos',$search);
        $this->db->or_like('idServicos',$search);
        $query = $this->db->get('servicos');
        return $query->num_rows();
    }
    // criando function para server side processing


    function getById($id){
        $this->db->where('idServicos',$id);
        $this->db->limit(1);
        return $this->db->get('servicos')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
    
    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquia', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
}