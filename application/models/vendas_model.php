<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendas_model extends CI_Model {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */

	function __construct() {
        parent::__construct();
    }  
 
    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array')
    {
        $this->db->select($fields.',vendas.chaveNfe as numNFE, vendas.idFranquia, clientes.nomeCliente, clientes.idClientes');
        $this->db->from($table);
        $this->db->limit($perpage,$start);
	    $this->db->where($table.'.idFranquia', $this->session->userdata('id'));
        $this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
	    
        $this->db->order_by('idVendas','desc');
	    $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    // criando function para server side processing
    function count_totalVendas(){ 
        $query = $this->db->where('idFranquia', $this->session->userdata('id'));
        $query = $this->db->get('vendas');
        return $query->num_rows();
    }

    function allVendas($limit,$start,$col,$dir){
        $this->db->select('vendas.*,clientes.idClientes, clientes.nomeCliente as cliente');
        $this->db->from('vendas');
        $this->db->where('vendas.idFranquia', $this->session->userdata('id'));
        $this->db->like('idVendas',$search);
        //$this->db->or_like('clientes.nomeCliente',$search);
        //$this->db->or_like('vendas.dataVenda',$search);
        //$this->db->or_like('atendentes.nomeAtendente',$search);
        //$this->db->join('franquias', 'franquias.idFranquias = orcamentos.idFranquia', 'left');
        $this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        //$this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }else{
            return null;
        }
    }
    function idVendas_search($limit,$start,$search,$col,$dir){
        //$this->db->select('vendas.*,clientes.idClientes, clientes.nomeCliente as cliente');
        //$this->db->from('vendas');
        //$this->db->where('vendas.idFranquia', $this->session->userdata('id'));
        //$this->db->like('idVendas',$search);
        //$this->db->or_like('clientes.nomeCliente',$search);
        //$this->db->or_like('vendas.dataVenda',$search);
        //$this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        // codigo alterado para evitar conflitos de pesquisas, devido a versão de mysql presente nesta versão do framework
        $idfranquia =  $this->session->userdata('id');
        $fields = "*, clientes.idClientes, clientes.nomeCliente as cliente
        FROM vendas 
        INNER JOIN clientes ON clientes.idClientes = vendas.clientes_id
        WHERE vendas.idFranquia = '".$idfranquia."'AND vendas.idVendas LIKE'%".$search."%'
        OR clientes.nomeCliente LIKE'%".$search."%' 
        OR vendas.dataVenda LIKE'%".$search."%' 
        HAVING vendas.idFranquia = '".$idfranquia."'";  
        $this->db->select($fields);

        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return null;
        }
    }
    function vendas_search_count($search){
        $this->db->where('idFranquia', $this->session->userdata('id'));
        $this->db->like('idVendas',$search);
        $this->db->or_like('idVendas',$search);
        $query = $this->db->get('vendas');
        return $query->num_rows();
    }
    // criando function para server side processing

    function getById($id){
        $this->db->select('vendas.*, clientes.*, usuarios.telefone, usuarios.email,usuarios.nome');
        $this->db->from('vendas');
        $this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.idVendas',$id);
	   $this->db->where('vendas.idFranquia', $this->session->userdata('id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    public function getProduto($id = null){
        $this->db->select('produtos.*');
        $this->db->from('produtos');
        $this->db->where('idProdutos',$id);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null){
        $this->db->select('itens_de_vendas.*, produtos.*');
        $this->db->from('itens_de_vendas');
        $this->db->join('produtos','produtos.idProdutos = itens_de_vendas.produtos_id');
        $this->db->where('vendas_id',$id);
        return $this->db->get()->result();
    }

    
    function add($table,$data,$returnId = false){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
                        if($returnId == true){
                            return $this->db->insert_id($table);
                        }
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   

    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquia', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function autoCompleteProduto($q){
                    $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('descricao', $q);
        $this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('produtos');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['descricao'].' | Preço: R$ '.$row['precoVenda'].' | Estoque: '.$row['estoque'],'estoque'=>$row['estoque'],'id'=>$row['idProdutos'],'preco'=>$row['precoVenda']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q){
                    $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nomeCliente', $q);
        $this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('clientes');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nomeCliente'].' | Telefone: '.$row['telefone'],'id'=>$row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuario($q){
                    $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('situacao',1);
        $this->db->where('idFranquia', $idFranquia);

        $query = $this->db->get('usuarios');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nome'].' | Telefone: '.$row['telefone'],'id'=>$row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }

    function getByIdNumNFE($id){
        $this->db->select("vendas.*, vendas.chaveNfe as numNFE, clientes.*, usuarios.telefone, usuarios.email, usuarios.nome");
        $this->db->from("vendas");
        $this->db->join("clientes","clientes.idClientes = vendas.clientes_id");
        $this->db->join("usuarios","usuarios.idUsuarios = vendas.usuarios_id");
        $this->db->where("vendas.chaveNfe", $id);
        $this->db->where("vendas.idFranquia", $this->session->userdata('id'));
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    function LoteDataArquivos($idfra,$dtInicial,$dtFinal){
        $this->db->select("vendas.chaveNfe as numNFE, vendas.dataVenda");
        $this->db->from("vendas"); 
        $this->db->join("clientes", "clientes.idClientes = vendas.clientes_id"); 
        $this->db->join("usuarios", "usuarios.idUsuarios = vendas.usuarios_id");
        $this->db->where("vendas.dataVenda BETWEEN '".$dtInicial."' AND '".$dtFinal."'");
        $this->db->where("vendas.chaveNfe != '(NULL)'");
        $this->db->where("vendas.idFranquia", $idfra);
        $this->db->order_by('vendas.chaveNfe', 'asc');
        $query = $this->db->get();
        $result = $query->result();

        //$result =  !$one  ? $query->result() : $query->row();

        return $result;
    }

    function LoteDataArquivosDanfe($idfra,$dtInicial,$dtFinal){
        $this->db->select("vendas.chaveNfe as numNFE, vendas.dataVenda");
        $this->db->from("vendas"); 
        $this->db->join("clientes", "clientes.idClientes = vendas.clientes_id"); 
        $this->db->join("usuarios", "usuarios.idUsuarios = vendas.usuarios_id");
        $this->db->where("vendas.dataVenda BETWEEN '".$dtInicial."' AND '".$dtFinal."'");
        $this->db->where("vendas.chaveNfe != '(NULL)'");
        $this->db->where("vendas.idFranquia", $idfra);
        $this->db->order_by('vendas.chaveNfe', 'asc');
        $query = $this->db->get();
        $result = $query->result();

        //$result =  !$one  ? $query->result() : $query->row();

        return $result;
    }


}

/* End of file vendas_model.php */
/* Location: ./application/models/vendas_model.php */