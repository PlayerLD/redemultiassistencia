<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xml_model extends CI_Model {

    /** 
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */

    function __construct() { 
        parent::__construct();
    } 

    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array')
    {
        //$this->db->select($fields.', vendas.idFranquia, clientes.nomeCliente, clientes.idClientes');
        //$this->db->from($table);
        //$this->db->limit($perpage,$start);
        //$this->db->where($table.'.idFranquia', $this->session->userdata('id'));
        //$this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
        //$this->db->order_by('idVendas','desc');

        $this->db->select($fields);
        $this->db->from($table);
        //$this->db->limit($perpage,$start);
        $this->db->where($table.'.idFranquias', $this->session->userdata('id'));
        $this->db->where($table.".numeroNfse != '(NULL)'");
        $this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
        //$this->db->order_by('idVendas','desc');

        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
 
    function countNotas($table){
        $this->db->select('count(os.idOs)');
        $this->db->from($table);
        $this->db->where($table.'.idFranquias', $this->session->userdata('id'));
        $this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
        $query = $this->db->get()->row();
        
        //$result = !$query ? $query->result() : $query->row();      
        $result = $query;      
        return $result;
    }

    
    function getById($id){
        $this->db->select('vendas.*, clientes.*, usuarios.telefone, usuarios.email,usuarios.nome');
        $this->db->from('vendas');
        $this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.idVendas',$id);
        $this->db->where('vendas.idFranquia', $this->session->userdata('id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getByIdNumNFSE($id){
        $this->db->select('os.*, os.numeroNfse as numNFSE, clientes.*, usuarios.telefone, usuarios.email,usuarios.nome');
        $this->db->from('os');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id');
        $this->db->where('os.numeroNfse',$id);
        $this->db->where('os.idFranquias', $this->session->userdata('id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function loteIdNumNFSE($idfranq){
        //$this->db->select('os.*, os.numeroNfse as numNFSE, clientes.*, usuarios.telefone, usuarios.email,usuarios.nome');
        $this->db->select('os.numeroNfse as numNFSE');
        $this->db->from('os');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id');
        $this->db->where("os.numeroNfse != '(NULL)'");
        $this->db->where('os.idFranquias',$idfranq);
        $this->db->order_by('os.numeroNfse', 'asc');
        //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function LoteDataArquivos($idfra,$dtInicial,$dtFinal){
        $this->db->select("os.numeroNfse as numNFSE, os.dataInicial, os.dataFinal");
        $this->db->from("os"); 
        $this->db->join("clientes", "clientes.idClientes = os.clientes_id"); 
        $this->db->join("usuarios", "usuarios.idUsuarios = os.usuarios_id");
        $this->db->where("os.dataInicial BETWEEN '".$dtInicial."' AND '".$dtFinal."'");
        $this->db->where("os.numeroNfse != '(NULL)'");
        $this->db->where("os.idFranquias", $idfra);
        $this->db->order_by('os.numeroNfse', 'asc');
        $query = $this->db->get();
        $result = $query->result();

        //$result =  !$one  ? $query->result() : $query->row();

        return $result;
    }

}

/* End of file vendas_model.php */
/* Location: ./application/models/vendas_model.php */