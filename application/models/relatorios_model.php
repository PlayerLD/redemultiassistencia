<?php
class Relatorios_model extends CI_Model {
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
    }
    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
    {
      return TRUE;
    }
    
    return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() >= 0)
    {
      return TRUE;
    }
    
    return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
    {
      return TRUE;
    }
    
    return FALSE;        
    }
    function count($table) {
        return $this->db->count_all($table);
    }
    
    public function clientesCustom($dataInicial = null,$dataFinal = null){
        
        if($dataInicial == null || $dataFinal == null){
            $dataInicial = date('Y-m-d');
            $dataFinal = date('Y-m-d');
        }
        
    $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND franquias.idFranquias = {$idFranquia}";

    $query = "SELECT clientes.nomeCliente, clientes.idFranquia, franquias.nome, clientes.telefone, clientes.email, 
        clientes.dataCadastro 
      FROM clientes 
      INNER JOIN franquias ON franquias.idFranquias = clientes.idFranquia 
      WHERE clientes.dataCadastro BETWEEN ? AND ? 
      {$where}
      ORDER BY clientes.idFranquia ASC, clientes.nomeCliente ASC";
      
    $valores = array($dataInicial,$dataFinal);
    
        return $this->db->query($query, $valores)->result();
    }
    public function clientesRapid(){
    $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)  
      $where = "WHERE franquias.idFranquias = {$idFranquia}";

       $query = "SELECT clientes.nomeCliente, clientes.idFranquia, franquias.nome, clientes.telefone, clientes.email, 
        clientes.dataCadastro 
      FROM clientes 
      INNER JOIN franquias ON franquias.idFranquias = clientes.idFranquia 
      {$where}
      ORDER BY clientes.idFranquia ASC, clientes.nomeCliente ASC";
      
    $valores = array();
    
        return $this->db->query($query, $valores)->result();
    }
  
  public function fornecedoresCustom($dataInicial = null,$dataFinal = null){
        
        if($dataInicial == null || $dataFinal == null){
            $dataInicial = date('Y-m-d');
            $dataFinal = date('Y-m-d');
        }

    $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND franquias.idFranquias = {$idFranquia}";
        
     $query = "SELECT fornecedores.nomeFornecedor, fornecedores.idFranquia, franquias.nome, fornecedores.telefone, fornecedores.email, 
        fornecedores.dataCadastro 
      FROM fornecedores 
      INNER JOIN franquias ON franquias.idFranquias = fornecedores.idFranquia 
      WHERE fornecedores.dataCadastro BETWEEN ? AND ? 
      {$where}
      ORDER BY fornecedores.idFranquia ASC, fornecedores.nomeFornecedor ASC";
      
    $valores = array($dataInicial,$dataFinal);
    
        return $this->db->query($query, $valores)->result();
    }
  
  public function fornecedoresRapid(){
    $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "WHERE franquias.idFranquias = {$idFranquia}";
      
       $query = "SELECT fornecedores.nomeFornecedor, fornecedores.idFranquia, franquias.nome, fornecedores.telefone, fornecedores.email, 
        fornecedores.dataCadastro 
      FROM fornecedores 
      INNER JOIN franquias ON franquias.idFranquias = fornecedores.idFranquia 
      {$where}
      ORDER BY fornecedores.idFranquia ASC, fornecedores.nomeFornecedor ASC";
      
    $valores = array();
    
        return $this->db->query($query, $valores)->result();
    }
  public function marcasCustom($dataInicial = null,$dataFinal = null, $franquia = null){
        
        if($dataInicial == null || $dataFinal == null){
            $dataInicial = date('Y-m-d');
            $dataFinal = date('Y-m-d');
        }
        
    
        $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "WHERE franquias.idFranquias = {$idFranquia}";
        
    $query = "SELECT m.nome AS nomeMarca, COUNT(m.nome) AS quantidade, f.nome, f.idFranquias FROM equipamentos_os AS eos
      INNER JOIN equipamentos AS e ON e.idEquipamentos = eos.equipamentos_id
      INNER JOIN marcas AS m ON m.idMarcas = e.marcas_id
      INNER JOIN os AS o ON o.idOs = eos.equipamentos_id
      INNER JOIN franquias AS f ON f.idFranquias = o.idFranquias 
      {$where}
      ";
    
    $valores = array(); 
    
    if($dataInicial != null || $dataFinal != null || $franquia != null){  
      if(empty($where))
      $query .= "WHERE "; 
      
      if($dataInicial != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "o.dataInicial >= ? ";
        $valores[] = $dataInicial;
      }
      if($dataFinal != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "o.dataInicial <= ? ";
        $valores[] = $dataFinal;
      }
      if($franquia != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "o.idFranquias = ? ";
        $valores[] = $franquia;
      }
    }
    
    $query .= "GROUP BY m.nome, f.idFranquias
      ORDER BY f.idFranquias ASC, m.nome;";
    
        return $this->db->query($query, $valores)->result();
    }
  
  public function marcasRapid($und = false){  
        $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "WHERE f.idFranquias = {$idFranquia}";
        $query = 'SELECT m.nome AS nomeMarca, COUNT(m.nome) AS quantidade, f.nome, f.idFranquias FROM equipamentos_os AS eos
      INNER JOIN equipamentos AS e ON e.idEquipamentos = eos.equipamentos_id
      INNER JOIN marcas AS m ON m.idMarcas = e.marcas_id
      INNER JOIN os AS o ON o.idOs = eos.equipamentos_id
            INNER JOIN franquias AS f ON f.idFranquias = o.idFranquias
            '.$where.'
      GROUP BY m.nome, f.idFranquias
      ORDER BY f.idFranquias ASC, m.nome;
            ';
      
    $valores = array();
    
        return $this->db->query($query, $valores)->result();
    }
    public function orcamentosRapid(){
      $dataInicial = date('Y-m-01');
      $dataFinal = date("Y-m-t");

      $where = "";
    $idFranquia=$this->session->userdata('id');

    $query = "SELECT 
    orcamentos.tipo, COUNT(orcamentos.idOrcamento) AS total FROM orcamentos
    INNER JOIN franquias ON franquias.idFranquias = orcamentos.idFranquia
    WHERE data BETWEEN ? and ?
    {$where}
    AND orcamentos.idFranquia = ?
    GROUP BY orcamentos.tipo
      ";
      
    $valores = array($dataInicial, $dataFinal, $this->session->userdata('id'));
    
        return $this->db->query($query, $valores)->result();  
    }
  
   public function orcamentosCustom($dataInicial,$dataFinal,$franquia){    
    $valores = array();
    $dataInicial = $dataInicial;
    $dataFinal   = $dataFinal;
    $franquia    = $franquia;

    if(empty($franquia)){
      $franquia = $this->session->userdata('id');
    }
    
    $where = "WHERE franquias.idFranquias = {$franquia}";
    
    if(!empty($dataInicial) || !empty($dataFinal) || !empty($franquia)){  
      
      if($dataInicial != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "SUBSTR(s.data, 1, 10) >= ? ";
        $valores[] = $dataInicial;
      }
      if($dataFinal != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "SUBSTR(s.data, 1, 10) <= ? ";
        $valores[] = $dataFinal;
      }
    }

    $query = "SELECT 
    orcamentos.tipo, COUNT(orcamentos.idOrcamento) AS total FROM orcamentos
    INNER JOIN franquias ON franquias.idFranquias = orcamentos.idFranquia
    {$where}
    GROUP BY orcamentos.tipo
      ";
    
        return $this->db->query($query, $valores)->result();  
    }
    public function servicosRapid(){
      $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "WHERE franquias.idFranquias = {$idFranquia}";
      
        $query = "SELECT s.nomeServico, s.descricaoServico, o.idFranquias, franquias.nome, s.precoServico 
      FROM servicos_os AS s
      INNER JOIN os AS o ON s.os_id = o.idOs
      INNER JOIN franquias ON franquias.idFranquias = o.idFranquias 
      {$where}
      ORDER BY o.idFranquias ASC, s.nomeServico ASC";
      
    $valores = array();
        return $this->db->query($query, $valores)->result();  
    }
  
   public function servicosCustom($precoInicial = null,$precoFinal = null, $franquia = null){    
    $valores = array();
    
    $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "WHERE franquias.idFranquias = {$idFranquia}";
      
    $query = "SELECT s.nomeServico, s.descricaoServico, o.idFranquias, franquias.nome, s.precoServico 
      FROM servicos_os AS s
      INNER JOIN os AS o ON s.os_id = o.idOs
      INNER JOIN franquias ON franquias.idFranquias = o.idFranquias
      {$where}
      ";

    if($precoInicial != null || $precoFinal != null || $franquia != null){  
      if(empty($where))
      $query .= "WHERE "; 
      
      if($precoInicial != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "s.precoServico >= ? ";
        $valores[] = $precoInicial;
      }
      if($precoFinal != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "s.precoServico <= ? ";
        $valores[] = $precoFinal;
      }
      if($franquia != null) {
        if(count($valores) > 0){
          $query .= "AND "; 
        }
        $query .= "o.idFranquias = ? ";
        $valores[] = $franquia;
      }
    }
    
    $query .= "ORDER BY o.idFranquias ASC, s.nomeServico ASC";  
    
        return $this->db->query($query, $valores)->result();  
    }
  
    public function osRapid(){
        $idFranquia=$this->session->userdata('id');

        $this->db->select('os.*,clientes.nomeCliente');
        $this->db->from('os');

        if($idFranquia != 4)
        $this->db->where('idFranquia', $idFranquia);

        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        return $this->db->get()->result();
    }
    public function produtosRapid(){

        $idFranquia=$this->session->userdata('id');
        $this->db->order_by('descricao','asc');

        if($idFranquia != 4)
        $this->db->where('idFranquia', $idFranquia);

        return $this->db->get('produtos')->result();
    }
    public function produtosRapidMin(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('*, (estoque_ideal - estoque) AS reposicao');
        $this->db->order_by('(estoque_ideal - estoque)','desc');
        $this->db->where('estoque < estoque_ideal');    
        if($idFranquia != 4)
        $this->db->where('idFranquia', $idFranquia);
        return $this->db->get('produtos')->result();
    }
    public function produtosCustom($precoInicial = null,$precoFinal = null,$estoqueInicial = null,$estoqueFinal = null){
        $wherePreco = "";
        $whereEstoque = "";
        $idFranquia=$this->session->userdata('id');
        if($precoInicial != null){
            $wherePreco = "AND precoVenda BETWEEN ".$this->db->escape($precoInicial)." AND ".$this->db->escape($precoFinal);
        }
        if($estoqueInicial != null){
            $whereEstoque = "AND estoque BETWEEN ".$this->db->escape($estoqueInicial)." AND ".$this->db->escape($estoqueFinal);
        }
        $query = "SELECT *, IF((estoque_ideal - estoque) < 0, 0, (estoque_ideal - estoque)) AS reposicao FROM produtos WHERE estoque >= 0 ".($idFranquia != 4 ? "AND idFranquia = $idFranquia" : "")." $wherePreco $whereEstoque";
        return $this->db->query($query)->result();
    }
    public function despesasRapid(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente');
        $this->db->join('fornecedores','fornecedores.idFornecedores = despesas.fornecedor', 'left outer');
        $this->db->join('clientes','clientes.idClientes = despesas.cliente', 'left outer');
        $this->db->order_by('data_vencimento', 'desc');
        $this->db->where('tipo', 1);
    if($idFranquia != 4)
        $this->db->where('despesas.idFranquia', $idFranquia);
        return $this->db->get('despesas')->result();
    }
    public function despesasRapidP(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente');
        $this->db->join('fornecedores','fornecedores.idFornecedores = despesas.fornecedor', 'left outer');
        $this->db->join('clientes','clientes.idClientes = despesas.cliente', 'left outer');
        $this->db->order_by('data_vencimento', 'desc');
        $this->db->where('tipo', 1);
      if($idFranquia != 4)
          $this->db->where('despesas.idFranquia', $idFranquia);
        $this->db->where('despesas.status != "P"');
        return $this->db->get('despesas')->result();
    }
    public function receitasRapid(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente');
        $this->db->join('fornecedores','fornecedores.idFornecedores = despesas.fornecedor', 'left outer');
        $this->db->join('clientes','clientes.idClientes = despesas.cliente', 'left outer');
        $this->db->order_by('data_vencimento', 'desc');
        $this->db->where('tipo', 2);
    if($idFranquia != 4)
        $this->db->where('despesas.idFranquia', $idFranquia);
        return $this->db->get('despesas')->result();
    }
    public function despesaRapidP(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente');
        $this->db->join('fornecedores','fornecedores.idFornecedores = despesas.fornecedor', 'left outer');
        $this->db->join('clientes','clientes.idClientes = despesas.cliente', 'left outer');
        $this->db->order_by('data_vencimento', 'desc');
        $this->db->where('tipo', 1);
    if($idFranquia != 4)
        $this->db->where('despesas.idFranquia', $idFranquia);
        $this->db->where('status !=', 'P');
        return $this->db->get('despesas')->result();
    }
    public function contasRapidR(){
    $idFranquia=$this->session->userdata('id');

        $this->db->select('despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente');
        $this->db->join('fornecedores','fornecedores.idFornecedores = despesas.fornecedor', 'left outer');
        $this->db->join('clientes','clientes.idClientes = despesas.cliente', 'left outer');
        $this->db->order_by('data_vencimento', 'desc');
        $this->db->where('tipo', 2);
    if($idFranquia != 4)
        $this->db->where('despesas.idFranquia', $idFranquia);
        $this->db->where('status !=', 'P');
        return $this->db->get('despesas')->result();
    }
    public function despesasCustom($dataInicial=null, $dataFinal=null, $dataInicial_emissao=null, $dataFinal_emissao=null, $dataInicial_pagamento=null, $dataFinal_pagamento=null, $cliente=null, $fornecedor=null, $tipo=null, $status=null){
        $whereData =
        $whereData_emissao =
        $whereData_pagamento =
        $whereCliente     =
        $whereFornecedor  =
        $whereTipo        =
        $whereStatus      = "";
        $where = "";
        $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND despesas.idFranquia = {$idFranquia}";

        if($dataInicial != null){
            $whereData = "AND data_vencimento BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($dataInicial_emissao != null){
            $whereData_emissao = "AND data_vencimento BETWEEN ".$this->db->escape($dataInicial_emissao)." AND ".$this->db->escape($dataFinal_emissao);
        }
        if($dataInicial_pagamento != null){
            $whereData_pagamento = "AND data_vencimento BETWEEN ".$this->db->escape($dataInicial_pagamento)." AND ".$this->db->escape($dataFinal_pagamento);
        }
        if($cliente != null){
            $whereCliente = "AND cliente = ".$this->db->escape($cliente);
        }
        if($fornecedor != null){
            $whereFornecedor = "AND fornecedor = ".$this->db->escape($fornecedor);
        }
        if($tipo != null){
            $whereStatus = "AND tipo = ".$this->db->escape($tipo);
        }
        if($status != null){
            $whereStatus = "AND status = ".$this->db->escape($status);
        }
      
        $query = "SELECT despesas.*,fornecedores.nomeFornecedor as fornecedor,clientes.nomeCliente as cliente FROM despesas LEFT OUTER JOIN clientes ON despesas.cliente = clientes.idClientes LEFT OUTER JOIN fornecedores ON despesas.fornecedor = fornecedores.idFornecedores WHERE idDespesas != 0 {$where} {$whereData} {$whereCliente} {$whereFornecedor} {$whereTipo} {$whereStatus}";
        return $this->db->query($query)->result();
    }
    public function osCustom($dataInicial = null,$dataFinal = null,$cliente = null,$responsavel = null,$status = null){
        $whereData = "";
        $whereCliente = "";
        $whereResponsavel = "";
        $whereStatus = "";
        $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND os.idFranquias = {$idFranquia}";
      
        if($dataInicial != null){
            $whereData = "AND dataInicial BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($cliente != null){
            $whereCliente = "AND clientes_id = ".$this->db->escape($cliente);
        }
        if($responsavel != null){
            $whereResponsavel = "AND usuarios_id = ".$this->db->escape($responsavel);
        }
        if($status != null){
            $whereStatus = "AND status = ".$this->db->escape($status);
        }
        $query = "SELECT os.*,clientes.nomeCliente FROM os LEFT JOIN clientes ON os.clientes_id = clientes.idClientes WHERE idOs != 0 $whereData {$where} $whereCliente $whereResponsavel $whereStatus";
        return $this->db->query($query)->result();
    }
    public function financeiroRapid(){
        
      $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND idFranquia = {$idFranquia}";
        $dataInicial = date('Y-m-01');
        $dataFinal = date("Y-m-t");
      
        $query = "SELECT * FROM lancamentos WHERE data_vencimento BETWEEN ? and ? {$where} ORDER BY tipo";
        return $this->db->query($query, array($dataInicial,$dataFinal))->result();
    }
    public function financeiroCustom($dataInicial, $dataFinal, $tipo = null, $situacao = null){
        
        $whereTipo = "";
        $whereSituacao = "";
        $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND idFranquia = {$idFranquia}";
        if($dataInicial == null){
            $dataInicial = date('Y-m-01');
        }
        if($dataFinal == null){
            $dataFinal = date("Y-m-t");  
        }
        if($tipo == 'receita'){
            $whereTipo = "AND tipo = 'receita'";
        }
        if($tipo == 'despesa'){
            $whereTipo = "AND tipo = 'despesa'";
        }
        if($situacao == 'pendente'){
            $whereSituacao = "AND baixado = 0";
        }
        if($situacao == 'pago'){
            $whereSituacao = "AND baixado = 1";
        } 
        
        
      
        $query = "SELECT * FROM lancamentos WHERE data_vencimento BETWEEN ? and ? {$where} $whereTipo $whereSituacao";
        return $this->db->query($query, array($dataInicial,$dataFinal))->result();
    }
    public function vendasRapid(){
    $idFranquia=$this->session->userdata('id');
        $this->db->select('vendas.*,clientes.nomeCliente, usuarios.nome');
        $this->db->from('vendas');
        $this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
    if($idFranquia != 4)
        $this->db->where('vendas.idFranquia', $idFranquia);
        $this->db->where('usuarios.idUsuarios', $this->session->userdata('id'));
        return $this->db->get()->result();
    }
    public function vendasCustom($dataInicial = null,$dataFinal = null,$cliente = null,$responsavel = null){
        $whereData = "";
        $whereCliente = "";
        $whereResponsavel = "";
        $whereStatus = "";
        $where = "";
    $idFranquia=$this->session->userdata('id');
    if($idFranquia != 4)
      $where = "AND vendas.idFranquia = {$idFranquia}";
        if($dataInicial != null){
            $whereData = "AND dataVenda BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($cliente != null){
            $whereCliente = "AND clientes_id = ".$this->db->escape($cliente);
        }
        if($responsavel != null){
            $whereResponsavel = "AND usuarios_id = ".$this->db->escape($responsavel);
        }
       
      
        $query = "SELECT vendas.*,clientes.nomeCliente,usuarios.nome FROM vendas LEFT JOIN clientes ON vendas.clientes_id = clientes.idClientes LEFT JOIN usuarios ON vendas.usuarios_id = usuarios.idUsuarios WHERE idVendas != 0 {$where} $whereData $whereCliente $whereResponsavel";
        return $this->db->query($query)->result();
    }
}