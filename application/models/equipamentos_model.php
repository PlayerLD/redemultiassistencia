<?php
class Equipamentos_model extends CI_Model {


    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br 
     *  
     */
    
    function __construct() {
        parent::__construct();
    }
 
    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $idFranquias = $this->session->userdata('id');

        $this->db->select($fields.', clientes.nomeCliente, clientes.idClientes');
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        $this->db->join('clientes', 'clientes.idClientes = '.$table.'.clientes_id');
        $this->db->where('equipamentos.idFranquia', $idFranquias);
        $this->db->order_by('idEquipamentos','desc');

        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


    // criando function para server side processing
    function count_totalEquipamentos(){
        $query = $this->db->where('idFranquia', $this->session->userdata('id'));
        $query = $this->db->get('equipamentos');
        return $query->num_rows();
    }
    function allEquipamentos($limit,$start,$col,$dir){
        $this->db->select('*, clientes.idClientes, clientes.nomeCliente as cliente, marcas.idMarcas, marcas.nome as marca');
        $this->db->from('equipamentos');
        $this->db->where('equipamentos.idFranquia',$this->session->userdata('id'));
        // $this->db->like('equipamentos.idEquipamentos',$search);
        //$this->db->or_like('equipamentos.equipamento',$search);
        //$this->db->or_like('equipamentos.mei',$search);
        //$this->db->or_like('equipamentos.defeito',$search);
        //$this->db->or_like('marcas.nome',$search);
        //$this->db->or_like('clientes.nomeCliente',$search);
        $this->db->join('clientes', 'clientes.idClientes = equipamentos.clientes_id');
        $this->db->join('marcas', 'marcas.idMarcas = equipamentos.marcas_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }else{
            return null;
        }
    }
    function idEquipamentos_search($limit,$start,$search,$col,$dir){
        //$this->db->select('*, clientes.idClientes, clientes.nomeCliente as cliente, marcas.idMarcas, marcas.nome as marca');
        //$this->db->from('equipamentos');
        //$this->db->where('equipamentos.idFranquia',$this->session->userdata('id'));
        //$this->db->like('equipamentos.idEquipamentos',$search);
        //$this->db->or_like('equipamentos.equipamento',$search);
        //$this->db->or_like('equipamentos.mei',$search);
        //$this->db->or_like('equipamentos.defeito',$search);
        //$this->db->or_like('marcas.nome',$search);
        //$this->db->or_like('clientes.nomeCliente',$search);
        //$this->db->join('clientes', 'clientes.idClientes = equipamentos.clientes_id');
        //$this->db->join('marcas', 'marcas.idMarcas = equipamentos.marcas_id');
        //$this->db->join('atendentes', 'atendentes.idAtendentes = os.atendentes_id', 'left');
        // codigo alterado para evitar conflitos de pesquisas, devido a versão de mysql presente nesta versão do framework
        $idfranquia =  $this->session->userdata('id');
        $fields = "*, clientes.idClientes, clientes.nomeCliente as cliente, marcas.idMarcas, marcas.nome as marca
        FROM equipamentos 
        INNER JOIN clientes ON clientes.idClientes = equipamentos.clientes_id
        INNER JOIN marcas ON marcas.idMarcas = equipamentos.marcas_id
        WHERE equipamentos.idFranquia = '".$idfranquia."'AND equipamentos.idEquipamentos LIKE'%".$search."%'
        OR equipamentos.equipamento LIKE'%".$search."%' 
        OR equipamentos.mei LIKE'%".$search."%' 
        OR equipamentos.defeito LIKE'%".$search."%' 
        OR clientes.nomeCliente LIKE'%".$search."%' 
        OR marcas.nome LIKE'%".$search."%' 
        HAVING equipamentos.idFranquia = '".$idfranquia."'";  
        $this->db->select($fields);
        
        $this->db->order_by($col,$dir);
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return null;
        }
    }
    function equipamentos_search_count($search){
        $this->db->where('idFranquia', $this->session->userdata('id'));
        $this->db->like('idEquipamentos',$search);
        $this->db->or_like('idEquipamentos',$search);
        $query = $this->db->get('equipamentos');
        return $query->num_rows(); 
    }
// criando function para server side processing


    function getById($id)
    {
        $this->db->select('equipamentos.*, clientes.*, marcas.*, marcas.nome');
        $this->db->from('equipamentos');
        $this->db->join('marcas','marcas.idMarcas = equipamentos.marcas_id');
        $this->db->join('clientes','clientes.idClientes = equipamentos.clientes_id');
        $this->db->where('equipamentos.idEquipamentos',$id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
      function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
                        if($returnId == true){
                            return $this->db->insert_id($table);
                        }
			return TRUE;
		}
		
		return FALSE;       
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    } 
        public function autoCompleteCliente($q){
                    $idFranquia=$this->session->userdata('id');

        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nomeCliente', $q);
        $this->db->where('idFranquia', $idFranquia);
        $query = $this->db->get('clientes');
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nomeCliente'].' | Telefone: '.$row['telefone'],'id'=>$row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }
    public function autoCompleteMarca($q){
        $idFranquia = $this->session->userdata('id');
        $idFranUniversal = 0;
        //$this->db->select('marcas.*,marcas_padrao.*');
        //$this->db->from('marcas');
        //$this->db->like('marcas.nome', $q);
        //$this->db->like('marcas_padrao.nome', $q);
        //$this->db->where('marcas.status','ativo');
        //$this->db->where('marcas_padrao.status','ativo');
        //$this->db->where('marcas.idFranquia', $idFranquia);
        //$this->db->where('marcas_padrao.idFranquia', $idFranUniversal);
        //$this->db->join('marcas_padrao','marcas_padrao.status = marcas.status');
        //$this->db->group_by('marcas.idMarcas');
        $fields = "* FROM marcas WHERE nome LIKE'".$q."' AND status = 'ativo'
                     AND idFranquia ='".$idFranquia."' OR idFranquia ='".$idFranUniversal."'
                     GROUP BY idMarcas
                     HAVING nome LIKE'%".$q."%' 
                     ORDER BY idMarcas ASC";
        $this->db->select($fields);
        $this->db->limit(5);
        $query = $this->db->get();
        if($query->num_rows > 0){
            foreach ($query->result_array() as $row){
                $row_set[] = array('label'=>$row['nome'],'id'=>$row['idMarcas']);
            }
            echo json_encode($row_set);
        }
        
    }
      
	
    function count($table, $id)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('idFranquia', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function tranformDefeito($defeito){
        $df = json_decode($defeito);

        if (is_array($df)) {
            foreach ($df as $value) {
               return $value;
            }
        }
    }
}