<?php 

$config = 
array(
    'clientes' => 
        array(        
            array(
                'field' => 'nomeCliente', 
                'label' => 'Nome', 
                'rules' => 'required|trim|xss_clean'
            ),
            array(
                'field' => 'rg', 
                'label' => 'RG', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'documento', 
                'label' => 'CPF/CNPJ', 
                'rules' => ''
            ),        
            array(
                'field' => 'telefone', 
                'label' => 'Telefone', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'whatsapp', 
                'label' => 'Whatsapp', 
                'rules' => ''
            ),        
            array(
                'field' => 'email', 
                'label' => 'Email', 
                'rules' => 'required|trim|valid_email|xss_clean'
            ),        
            array(
                'field' => 'rua', 
                'label' => 'Rua', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'numero', 
                'label' => 'Número', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'complemento', 
                'label' => 'Complemento', 
                'rules' => ''
            ),        
            array(
                'field' => 'bairro', 
                'label' => 'Bairro', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'cidade', 
                'label' => 'Cidade', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'estado', 
                'label' => 'Estado', 
                'rules' => 'required|trim|xss_clean'
            ),        
            array(
                'field' => 'obs', 
                'label' => 'OBS', 
                'rules' => ''
            ),        
            array(
                'field' => 'cep', 
                'label' => 'CEP', 
                'rules' => 'required|trim|xss_clean'
            )
        ), 

    'atendentes' => 
        array(        
            array(
                'field' => 'nomeAtendente', 
                'label' => 'Nome',
                'rules' => 'required|trim|xss_clean'
            ),
            array(
                'field' => 'rg', 
                'label' => 'RG', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'documento', 
                'label' => 'CPF/CNPJ', 
                'rules' => ''
            ),        
            array(
                'field' => 'telefone', 
                'label' => 'Telefone', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'whatsapp', 
                'label' => 'Whatsapp', 
                'rules' => ''
            ),        
            array(
                'field' => 'email', 
                'label' => 'Email', 
                'rules' => 'trim|valid_email|xss_clean'
            ),        
            array(
                'field' => 'rua', 
                'label' => 'Rua', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'numero', 
                'label' => 'Número', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'complemento', 
                'label' => 'Complemento', 
                'rules' => ''
            ),        
            array(
                'field' => 'bairro', 
                'label' => 'Bairro', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'cidade', 
                'label' => 'Cidade', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'estado', 
                'label' => 'Estado', 
                'rules' => 'trim|xss_clean'
            ),        
            array(
                'field' => 'obs', 
                'label' => 'OBS', 
                'rules' => ''
            ),        
            array(
                'field' => 'cep', 
                'label' => 'CEP', 
                'rules' => 'trim|xss_clean'
            )
        ), 

    'fornecedores' => 
        array(
            array('field'=>'nomeFornecedor',         'label'=>'Nome',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'documento',         'label'=>'CPF/CNPJ',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'responsavel',         'label'=>'Responsável',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'telefone',         'label'=>'Telefone',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'email',         'label'=>'Email',         'rules'=>'required|trim|valid_email|xss_clean'         ),        
            array('field'=>'site',         'label'=>'Site',         'rules'=>''         ),        
            array('field'=>'rua',         'label'=>'Rua',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'numero',         'label'=>'Número',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'complemento',         'label'=>'Complemento',         'rules'=>''         ),        
            array('field'=>'bairro',         'label'=>'Bairro',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'cidade',         'label'=>'Cidade',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'estado',         'label'=>'Estado',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'descritivo',         'label'=>'Descritivo',         'rules'=>''         ),        
            array('field'=>'obs',         'label'=>'Obs',         'rules'=>''         ),        
            array('field'=>'cep',         'label'=>'CEP',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'fixo',         'label'=>'Fixo para todas franquias',         'rules'=>'required|trim|xss_clean'         )
        ),

    'cadastro_cartao' => 
        array(        
            array('field'=>'nome_cartao',            'label'=>'Nome do cartão',            'rules'=>'required|trim|xss_clean'            ),
            array('field'=>'cpf_cartao',            'label'=>'CPF do titular',            'rules'=>'required|trim|xss_clean'            ),
            array('field'=>'num_cartao',            'label'=>'Número do cartão',            'rules'=>'required|trim|xss_clean'            ),
            array('field'=>'vencimento_cartao',            'label'=>'Vencimento do cartão',            'rules'=>'required|trim|xss_clean'            )
        ),

    'servicos' => 
        array(        
            array('field'=>'nome',            'label'=>'Nome',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'idFranquia',            'label'=>'idFranquia',            'rules'=>''            ),        
            array('field'=>'descricao',            'label'=>'',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'itemListaServico',            'label'=>'Código da lista de serviços',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'preco',            'label'=>'',            'rules'=>'required|trim|xss_clean'            )
        ),

    'equipamentos' => 
        array(        
            array('field'=>'equipamento',            'label'=>'Equipamento',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'idFranquia',            'label'=>'idFranquia',            'rules'=>''            ),        
            array('field'=>'marcas_id',            'label'=>'Marca',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'mei',            'label'=>'',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'modelo',            'label'=>'',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'clientes_id',            'label'=>'Cliente',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'obser',            'label'=>'',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'tipo',            'label'=>'Tipo',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'status',            'label'=>'Status',            'rules'=>'required|trim|xss_clean'            )
        ),

    'marcas' => 
        array(
            array(            'field'=>'nome',            'label'=>'Nome',            'rules'=>'required|trim|xss_clean'            ),        
            array(            'field'=>'status',            'label'=>'Status',            'rules'=>''            )
        ),

    'produtos' => 
        array(        
            array('field'=>'descricao',            'label'=>'',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'idFranquia',            'label'=>'idFranquia',            'rules'=>''            ),        
            array('field'=>'unidade',            'label'=>'Unidade',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'marcas_id',            'label'=>'marcas',            'rules'=>'trim|xss_clean|required'            ),        
            array('field'=>'precoCompra',            'label'=>'Preo de Compra',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'precoVenda',            'label'=>'Preo de Venda',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'estoque',            'label'=>'Estoque',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'estoqueMinimo',            'label'=>'Estoque Mnimo',            'rules'=>'trim|xss_clean'            )
        ),

    'produto_variacoes' => 
        array(        
            array('field'=>'titulo',            'label'=>'',            'rules'=>'required|trim|xss_clean'            ),        
        ),

    'produto_taxas' => 
        array(        
            array('field'=>'titulo',            'label'=>'',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'valor',            'label'=>'',            'rules'=>'required|trim|xss_clean'            ),        
        ),

    'despesas' => 
        array(        
            array('field'=>'tipo',             'label'=>'Tipo',               'rules'=>'required|trim|xss_clean'),        
            array('field'=>'status',             'label'=>'Status',               'rules'=>'required|trim|xss_clean'),        
            array('field'=>'nome',             'label'=>'Nome',               'rules'=>'required|trim|xss_clean'),        
            array('field'=>'data_vencimento',  'label'=>'Data de Vencimento',     'rules'=>''),        
            array('field'=>'valor',            'label'=>'Valor',        'rules'=>'required|trim|xss_clean'),
            array('field'=>'forma_pagamento',  'label'=>'Forma de pagamento',         'rules'=>'trim|xss_clean|required'),
            array('field'=>'codigo',           'label'=>'Código', 'rules'=>'required|trim|xss_clean'),
            array('field'=>'data_emissao',     'label'=>'Data de emissão',  'rules'=>'required|trim|xss_clean'),
            array('field'=>'data_pagamento',   'label'=>'Data de pagamento',        'rules'=>'required|trim|xss_clean')
        ),

    'usuarios' => 
        array(        
            array('field'=>'nome',            'label'=>'Nome',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'rg',            'label'=>'RG',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'cpf',            'label'=>'CPF',            'rules'=>'required|trim|xss_clean|is_unique[usuarios.cpf]'            ),        
            array('field'=>'rua',            'label'=>'Rua',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'numero',            'label'=>'Numero',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'bairro',            'label'=>'Bairro',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'cidade',            'label'=>'Cidade',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'estado',            'label'=>'Estado',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'email',            'label'=>'Email',            'rules'=>'required|trim|valid_email|xss_clean'            ),        
            array('field'=>'senha',            'label'=>'Senha',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'telefone',            'label'=>'Telefone',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'situacao',            'label'=>'Situacao',            'rules'=>'required|trim|xss_clean'            )
        ),

    'franquias' => 
        array(        
            array('field'=>'nome',            'label'=>'Nome',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'rg',            'label'=>'RG',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'cpf',            'label'=>'CPF',            'rules'=>'required|trim|xss_clean|is_unique[usuarios.cpf]'            ),        
            array('field'=>'rua',            'label'=>'Rua',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'numero',            'label'=>'Numero',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'bairro',            'label'=>'Bairro',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'cidade',            'label'=>'Cidade',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'estado',            'label'=>'Estado',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'email',            'label'=>'Email',            'rules'=>'required|trim|valid_email|xss_clean'            ),       
            array('field'=>'senha',            'label'=>'Senha',            'rules'=>'required|trim|xss_clean'            ),       
            array('field'=>'telefone',            'label'=>'Telefone',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'slotsm',            'label'=>'Slots Médios',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'slotsg',            'label'=>'Slots Grandes',            'rules'=>'required|trim|xss_clean'            ),       
            array('field'=>'fileiras_slots',            'label'=>'Qtd. por fileira',            'rules'=>'required|trim|xss_clean'            ),      
            array('field'=>'situacao',            'label'=>'Situacao',            'rules'=>'required|trim|xss_clean'            ),       
            array('field'=>'statusOrcamento',            'label'=>'Orçamentos Site',            'rules'=>'required|trim|xss_clean') 
        ),

    'planos' =>
        array(       
            array('field'=>'nome',            'label'=>'Nome',            'rules'=>'required|trim|xss_clean'            ),       
            array('field'=>'valor',            'label'=>'Valor',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'recorrencia',            'label'=>'Período de recorrência',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'dia_recorrencia',            'label'=>'Dia de recorrência',            'rules'=>'required|trim|xss_clean')
        ),

    'os' => 
        array(        
            array('field'=>'dataInicial',            'label'=>'DataInicial',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'dataFinal',            'label'=>'DataFinal',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'garantia',            'label'=>'Garantia',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'descricaoProduto',            'label'=>'DescricaoProduto',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'defeito',            'label'=>'Defeito',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'status',            'label'=>'Status',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'observacoes',            'label'=>'Observacoes',            'rules'=>'trim|xss_clean'            ),        
            array('field'=>'clientes_id',            'label'=>'clientes',            'rules'=>'trim|xss_clean|required'            ),        
            array('field'=>'usuarios_id',            'label'=>'usuarios_id',            'rules'=>'trim|xss_clean|required'            ),        
            array('field'=>'laudoTecnico',            'label'=>'Laudo Tecnico',            'rules'=>'trim|xss_clean'            )    
        ),

    'tiposUsuario' => 
        array(        
            array('field'=>'nomeTipo',         'label'=>'NomeTipo',         'rules'=>'required|trim|xss_clean'         ),        
            array('field'=>'situacao',         'label'=>'Situacao',         'rules'=>'required|trim|xss_clean'         )    
        ),

    'receita' => 
        array(        
            array('field'=>'descricao',            'label'=>'Descrição',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'valor',            'label'=>'Valor',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'vencimento',            'label'=>'Data Vencimento',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'cliente',            'label'=>'Cliente',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'tipo',            'label'=>'Tipo',            'rules'=>'required|trim|xss_clean'            )     
        ),

    'despesa' => 
        array(        
            array('field'=>'descricao',            'label'=>'Descrição',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'valor',            'label'=>'Valor',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'vencimento',            'label'=>'Data Vencimento',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'fornecedor',            'label'=>'Fornecedor',            'rules'=>'required|trim|xss_clean'            ),        
            array('field'=>'tipo',            'label'=>'Tipo',            'rules'=>'required|trim|xss_clean'            )   
        ),

    'sistemaos' => 
        array(        
            array('field'=>'email',            'label'=>'Email',            'rules'=>'valid_email|required|xss_clean|trim'            ),        
            array('field'=>'senha',            'label'=>'Senha',            'rules'=>'required|xss_clean|trim'            ),        
            array('field'=>'idv',            'label'=>'Id',            'rules'=>'required|xss_clean|trim'            )     
        ),

    'vendas' => 
        array(        
            array('field' => 'dataVenda',            'label' => 'Data da Venda',            'rules' => 'required|trim|xss_clean'            ),        
            array('field'=>'clientes_id',            'label'=>'clientes',            'rules'=>'trim|xss_clean|required'            ),        
            array('field'=>'usuarios_id',            'label'=>'usuarios_id',            'rules'=>'trim|xss_clean|required'            )   
        ),

    'mensagens' => 
        array(        
            array('field' => 'emailUser',            'label' => 'Usuário',            'rules' => 'required|trim|xss_clean'            ),        
            array('field' => 'emailName',            'label' => 'Nome',            'rules' => 'required|trim|xss_clean'            )   
        ),

    'orcamento' => 
        array(        
            array('field' => 'nome',            'label' => 'Nome',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'email',            'label' => 'E-Mail',            'rules' => 'trim|xss_clean|valid_email'            ),        
            array('field' => 'telefone',            'label' => 'Telefone',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'marca',            'label' => 'Marca',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'modelo',            'label' => 'Modelo',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'defeito',            'label' => 'Defeito',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'franquia',            'label' => 'Franquia',            'rules' => 'trim|xss_clean|required'            ),        
            array('field' => 'obs',            'label' => 'Observação',            'rules' => 'trim|xss_clean'            )
        ),   

    'avaliacao' => 
        array(        
            array('field' => 'nota',            'label' => 'Nota',            'rules' => 'trim|xss_clean|integer|required'            ),       
            array('field' => 'obs',            'label' => 'Observação',            'rules' => 'trim|xss_clean'            )        
        )
);