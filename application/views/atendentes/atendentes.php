<a href="<?php echo base_url()?>index.php/atendentes/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Atendente</a>

<?php if(!$results) { ?>

    <div class="widget-box">
        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Atendentes</h5>

        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>    
                    <tr>
                        <td colspan="5">Nenhum Atendente Cadastrado</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php } else { ?>

    <div class="widget-box">
        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Atendentes</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Nome</th>
                        <th>Rg</th>
                        <th>CPF/CNPJ</th>
                        <th>Telefone</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    
                        foreach ($results as $r) :
                            echo '<tr>';
                            echo '<td>'.$r->idAtendentes.'</td>';
                            echo '<td>'.$r->nomeAtendente.'</td>';
                            echo '<td>'.$r->rg.'</td>';
                            echo '<td>'.$r->documento.'</td>';
                            echo '<td>'.$r->telefone.'</td>';
                            echo '<td>
                                    <a href="'.base_url().'index.php/atendentes/editar/'.$r->idAtendentes.'" class="btn btn-info tip-top" title="Editar Atendente"><i class="icon-pencil icon-white"></i></a>
                                </td>';
                            echo '</tr>';
                        endforeach;
                    ?>
                    <tr>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
	
    <?php echo $this->pagination->create_links();?>

<?php } ?>