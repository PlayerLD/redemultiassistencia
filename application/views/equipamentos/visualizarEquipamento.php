<?php if (!$result) { echo ('<div class="alert alert-danger">Equipamento não encontrado.</div>'); return; } ?>

<div class="accordion" id="collapse-group">
    <div class="accordion-group widget-box">
        <div class="accordion-heading">
            <div class="widget-title">
                <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                    <span class="icon"><i class="icon-list"></i></span><h5>Dados do Produto</h5>
                </a>
            </div>
        </div>
        <div class="collapse in accordion-body">
            <div class="widget-content">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="text-align: right; width: 30%"><strong>Equipamento</strong></td>
                            <td><?php echo $result->equipamento ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Marca</strong></td>
                            <td><?php echo $result->nome ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Tipo</strong></td>
                            <td><?php echo $result->tipo ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Número de Série/IMEI</strong></td>
                            <td><?php echo $result->mei; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Modelo</strong></td>
                            <td><?php echo $result->modelo; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Cliente</strong></td>
                            <td><?php echo $result->nomeCliente; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Status</strong></td>
                            <td><?php echo $result->status; ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>OBS</strong></td>
                            <td><?php echo $result->obser; ?></td>
                        </tr>
<tr>
                            <td style="text-align: right"><strong>Imagens</strong></td>
                            <td>
<!-- API WEBCAM INIT ---------------------------------------------------------------------------------------------------------------------------------------------- -->
                            	<script type="text/javascript" src="<?php echo base_url().reisdev('url')?>cam-api/functions.js"></script>
				<style type="text/css">@import url("<?php echo base_url().reisdev('url')?>cam-api/styles.css");</style><script type="text/javascript">
					window.onload = function(event) { updatePreview(); resize(); };
					window.onresize = function(event) { resize(); };
				</script>
			    	<div id="imgPopupBack" class="modal">
					<span class="imgPopupClose">×</span>
					<img class="modal-content" id="imgPopupView">
					<div id="imgPopupText"></div>
			    	</div>
                            	<div id="pnPrev" class="pnPrev">
                            	
					<div class="pnI">
	                            	    	<div id="divImg01" class="pnImg"> </div>
				        </div>
					<div class="pnI">
	                            	    	<div id="divImg02" class="pnImg"> </div>
				        </div>
					<div class="pnI">
	                            	    	<div id="divImg03" class="pnImg"> </div>
				        </div>
					<div class="pnI">
	                            	    	<div id="divImg04" class="pnImg"> </div>
				        </div>
					<div class="pnI">
	                            	    	<div id="divImg05" class="pnImg"> </div>
				        </div>
					<div class="pnI">
	                            	    	<div id="divImg06" class="pnImg"> </div>
				        </div>
			    	</div>                            
<!--  API WEBCAM END ----------------------------------------------------------------------------------------------------------------------------------------------- -->
                	    </td>
                	</tr>                  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

