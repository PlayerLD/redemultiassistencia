<div class="row-fluid" style="margin-top: 0">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"> <i class="icon-align-justify"></i></span>
				<h5>Cadastro de Equipamento</h5>
			</div>     
			<div class="widget-content">
                <?php if ($custom_error != '') { 
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
                <div id="conteudo">
                	<div class="formulario">
                		<form action="<?php echo current_url(); ?><?php echo $iframe ? '/1' : ''; ?>" id="formEquipamento" method="post" class="form-horizontal">
					 		<?php $idFranquia = $this->session->userdata('id');?>
                  			<input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
                            <div class="dados">
                    			<div class="campos">
                                	<div class="campo" style="width: 39%">
                                    	<label for="equipamento">Equipamento <span class="required">*</span></label>
                                        <select name="equipamento" id="equipamento" value="">
                                           
                                           <?php foreach(@unserialize($franquia->permissoes_equip) as $e){ ?>
                                           <option value="<?php echo $e; ?>"><?php echo $e; ?></option>
                                           <?php } ?>

                                        </select>
                                  	</div>  
                                    <div class="campo" style="width: 29%">    
                                    	<label for="marca">Marca<span class="required">*</span></label>
                                        <input id="marca" type="text" name="marca" value="" /> 
										<input id="marcas_id" type="hidden" class="span12" name="marcas_id" value="" />
                                    </div>
                                    <div class="campo last" style="width: 30%">
                                    	<label for="mei">Número de Série/IMEI <span class="required">*</span></label>
                                        <input id="mei" type="text" name="mei" value="<?php echo set_value('mei'); ?>" />
                                    </div>
                                    <div class="campo" style="width: 39%">
                                    	<label for="modelo">Modelo</label>
                                        <input id="modelo" type="text" name="modelo" value="<?php echo set_value('modelo'); ?>" />
                                    </div>
                                    <div class="campo last" style="width: 60%">
                                    	<label for="cliente">Cliente <span class="required">*</span></label>
                                        <input id="cliente" type="text" name="cliente" value="<?php echo isset($cliente->nomeCliente)?$cliente->nomeCliente:"";?>" /> 
										<input id="clientes_id" class="span12" type="hidden" name="clientes_id" value="<?php echo isset($cliente->idClientes)?$cliente->idClientes:"";?>" />
                                    </div>
                                    <div class="control-group" style="display: none;">
                                        <label for="tipo">Tipo<span class="required">*</span></label>
                                        <div class="controls">
                                            <select name="tipo" id="tipo" value="">
                                                <option value="celular">Celular</option>
                                                <option value="tablet">Tablet</option>
                                                <option value="videogame">Videogame</option>
                                                <option value="notebook">Notebook</option>
                                                <option value="tv">TV</option>
                                                <option value="computador">Computador</option>
                                                <option value="imac">iMac</option> 
                                            	<option value="mac">Mac</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group" style="display: none;">
                                        <label for="status">Status <span class="required">*</span></label>
                                        <div class="controls">
                                            <select name="status" id="status" value="">
                                                <option value="ativo">Ativo</option>
                                                <option value="inativo">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="campo last" style="width: 100%">
                                    	<label for="obser">Observação</label>
                                        <textarea id="obser" name="obser"><?php echo set_value('obser'); ?></textarea>
                                    </div>
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                    	<button type="submit" class="btn btn-success">
                                            <i class="icon-plus icon-white"></i> Adicionar e incluir fotos
                                        </button>
                                        <a href="<?php echo base_url() ?>index.php/equipamentos" id="btnAdicionar" class="btn" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>">
                                            <i class="icon-arrow-left"></i> Voltar
                                        </a>
                                    </div>
                              	</div>
                         	</div>              
						</form>
                  	</div>
             	</div>             
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#equipamento").change(function(){
		nomeEquipamento = $(this).val().toLowerCase();
		$("#tipo").val(nomeEquipamento);
	});
	
	$("#marca").keyup(function(e){
		if($(this).val() != ""){
        	$(this).autocomplete({
            	source: "<?php echo base_url(); ?>index.php/equipamentos/autoCompleteMarca",
                minLength: 1,
                select: function( event, ui) {
                	if(ui.item.id != undefined){
                    	$("#marcas_id").val(ui.item.id);
                    }else{
                    	$("#marcas_id").val("");
                    }
                }
    		});
		}else{
			$("#marcas_id").val("");
		}
    });
    
    $("#cliente").keyup(function(e){
		if($(this).val() != ""){
			$(this).autocomplete({
				source: "<?php echo base_url(); ?>index.php/vendas/autoCompleteCliente",
				minLength: 1,
				select: function( event, ui ) {
					if(ui.item.id != undefined){
						$("#clientes_id").val(ui.item.id);
					}else{
						$("#clientes_id").val("");
					}
				}
			});
		}else{
			$("#clientes_id").val("");
		}
	});
    
    
    
    $('#formEquipamento').validate({
    	rules :{
        	equipamento:{ required: true},
            marca:{ required: true},
            marcas_id:{ required: true},
            mei:{ required: true},
            cliente:{ required: true},
            clientes_id:{ required: true},
            tipo:{ required: true},
            status:{ required: true}
        },
        messages:{
        	equipamento :{ required: ''},
            cliente :{required: ''},
            clientes_id :{ required: ''},
            marca :{ required: ''},
            marcas_id :{ required: ''},
            mei:{ required: ''},
            tipo :{ required: ''},
            status :{ required: ''}
        },
        highlight:function(element, errorClass, validClass) {
         	$(element).parents('.campo').removeClass('success');
		    $(element).parents('.campo').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.campo').removeClass('error');
            $(element).parents('.campo').addClass('success');
        }
     }); 
});
</script>