<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.css" />

<style type="text/css">
    .popover-modal .popover-body{
        overflow: hidden;
        padding: 10px;
        text-align: left;
    }
    .popover-question{
        padding: 0px 5px;
        background: #0088cc;
        color: white;
        border-radius: 50%;
        font-size: 11px;
        margin-left: 5px;
    }
</style>

<?php if (!$result) {
    echo('<div class="alert alert-danger">Equipamento não encontrado.</div>');
    return;
} ?> 
 
<?php if (@$fecharGuia) { ?>
<script type="text/javascript">
    window.opener='X';window.close();
</script>
<?php } ?>
<script type="text/javascript">
    document.idEquipamento = '<?php echo $result->idEquipamentos; ?>';
</script>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                                    <i class="icon-align-justify"></i>
                                </span>
                <h5>
                    Editar Equipamento
                </h5>
            </div>
            <div class="widget-content nopadding">
                <form action="<?php echo current_url(); ?>" id="formEquipamento" method="post" class="form-horizontal">
                    <?php echo form_hidden('idEquipamentos', $result->idEquipamentos) ?>
                    <div class="control-group">
                        <label for="equipamento" class="control-label">
                            Equipamento
                            <span class="required">*</span>
                        </label>
                        <div class="controls">
                            <select name="equipamento" id="equipamento">
                                <?php if(in_array('Celular', (array)@unserialize($franquia->permissoes_equip)) || in_array('celular', (array)@unserialize($franquia->permissoes_equip)) || in_array('CELULAR', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Celular" || $result->equipamento == "celular" || $result->equipamento == "CELULAR"){?>selected<?php }?> value="Celular">Celular</option>
                                <?php } ?>
                                <?php if(in_array('Tablet', (array)@unserialize($franquia->permissoes_equip)) || in_array('tablet', (array)@unserialize($franquia->permissoes_equip)) || in_array('TABLET', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Tablet"){?>selected<?php }?> value="Tablet">Tablet</option>
                                <?php } ?>
                                <?php if(in_array('Videogame', (array)@unserialize($franquia->permissoes_equip)) || in_array('videogame', (array)@unserialize($franquia->permissoes_equip)) || in_array('VIDEOGAME', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Videogame" || $result->equipamento == "videogame" || $result->equipamento == "VIDEOGAME"){?>selected<?php }?> value="Videogame">Videogame</option>
                                <?php } ?>
                                <?php if(in_array('Notebook', (array)@unserialize($franquia->permissoes_equip)) || in_array('notebook', (array)@unserialize($franquia->permissoes_equip)) || in_array('NOTEBOOK', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Notebook" || $result->equipamento == "notebook" || $result->equipamento == "NOTEBOOK"){?>selected<?php }?> value="Notebook">Notebook</option>
                                <?php } ?>
                                <?php if(in_array('Tv', (array)@unserialize($franquia->permissoes_equip)) || in_array('tv', (array)@unserialize($franquia->permissoes_equip)) || in_array('TV', (array)@unserialize($franquia->permissoes_equip)) ){ ?>
                                <option <?php if($result->equipamento == "TV" || $result->equipamento == "Tv" || $result->equipamento == "tv"){?>selected<?php }?> value="TV">TV</option>
                                <?php } ?>
                                <?php if(in_array('Computador', (array)@unserialize($franquia->permissoes_equip)) || in_array('computador', (array)@unserialize($franquia->permissoes_equip)) || in_array('COMPUTADOR', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Computador" || $result->equipamento == "computador" || $result->equipamento == "COMPUTADOR"){?>selected<?php }?> value="Computador">Computador</option>
                                <?php } ?>
                                <?php if(in_array('iMac', (array)@unserialize($franquia->permissoes_equip)) || in_array('imac', (array)@unserialize($franquia->permissoes_equip)) || in_array('IMAC', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "iMac" || $result->equipamento == "imac" || $result->equipamento == "IMAC"){?>selected<?php }?> value="iMac">iMac</option>
                                <?php } ?>
                                <?php if(in_array('Mac', (array)@unserialize($franquia->permissoes_equip)) || in_array('mac', (array)@unserialize($franquia->permissoes_equip)) || in_array('MAC', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Mac" || $result->equipamento == "mac" || $result->equipamento == "MAC"){?>selected<?php }?> value="Mac">Mac</option>
                                <?php } ?>
                                <?php if(in_array('Drone', (array)@unserialize($franquia->permissoes_equip)) || in_array('drone', (array)@unserialize($franquia->permissoes_equip)) || in_array('DRONE', (array)@unserialize($franquia->permissoes_equip))){ ?>
                                <option <?php if($result->equipamento == "Drone" || $result->equipamento == "drone" || $result->equipamento == "DRONE"){?>selected<?php }?> value="Drone">Drone</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="marca" class="control-label">
                            Marca
                            <span class="required">*</span>
                        </label>
                        <div class="controls">
                            <input id="marca" type="text" name="marca" value="<?php echo $result->nome ?>"/>
                            <input id="marcas_id" type="hidden" class="span12" name="marcas_id"
                                   value="<?php echo $result->marcas_id; ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="mei" class="control-label">
                            Número de Série/IMEI
                        </label>
                        <div class="controls">
                            <input id="mei" type="text" name="mei" value="<?php echo $result->mei ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="modelo" class="control-label">
                            Modelo
                        </label>
                        <div class="controls">
                            <input id="modelo" type="text" name="modelo" value="<?php echo $result->modelo ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cliente" class="control-label">
                            Cliente
                            <span class="required">*</span>
                        </label>
                        <div class="controls">
                            <input id="cliente" type="text" name="cliente" value="<?php echo $result->nomeCliente ?>"/>
                            <input id="clientes_id" type="hidden" class="span12" name="clientes_id"
                                   value="<?php echo $result->clientes_id ?>"/></div>
                    </div>
                    <div class="control-group">
                        <label for="obser" class="control-label">
                            OBS
                        </label>
                        <div class="controls">
                            <textarea id="obser" name="obser"
                                      style="resize: none"><?php echo $result->obser ?></textarea>
                        </div>
                    </div>
                    <div class="control-group" style="display:none;">
                        <label for="tipo" class="control-label">
                            Tipo
                        </label>
                        <div class="controls">
                            <select name="tipo" id="tipo">
                                <option value="Celular" <?php if($result->tipo == "Celular"){?>selected<?php }?>>Celular</option>
                                <option value="Tablet" <?php if($result->tipo == "Tablet"){?>selected<?php }?>>Tablet</option>
                                <option value="Videogame" <?php if($result->tipo == "Videogame"){?>selected<?php }?>>Videogame</option>
                                <option value="Notebook" <?php if($result->tipo == "Notebook"){?>selected<?php }?>>Notebook</option>
                                <option value="TV" <?php if($result->tipo == "TV"){?>selected<?php }?>>TV</option>
                                <option value="Computador" <?php if($result->tipo == "Computador"){?>selected<?php }?>>Computador</option>
                                <option value="iMac" <?php if($result->tipo == "iMac"){?>selected<?php }?>>iMac</option>
                                
                                <option value="Mac" <?php if($result->tipo == "Mac"){?>selected<?php }?>>Mac</option>
                                <option value="Drone" <?php if($result->tipo == "Drone"){?>selected<?php }?>>Drone</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" style="display:none;">
                        <label for="status" class="control-label">
                            Status
                        </label>
                        <div class="controls">
                            <select name="status" id="status" value="">
                                <option <?php if ($result->status == 'ativo') {
                                    echo 'selected';
                                } ?> value="ativo">Ativo
                                </option>
                                <option <?php if ($result->status == 'inativo') {
                                    echo 'selected';
                                } ?> value="inativo">Inativo
                                </option>
                            </select>
                        </div>
                    </div>
                    <!-- API WEBCAM INIT ---------------------------------------------------------------------------------------------------------------------------------------------- -->
                    <div class="col-md-6">
                    <div class="widget-title">
                        <span class="icon">
                                        <i class="icon-warning-sign"></i>
                                    </span>
                        <h5>
                            Defeito do equipamento - <font style="color: #F66;"> Marque as funções que aparentam detalhes que não funcionam. </font>
                        </h5>
                    </div>
                    <div>
                        <div class="control-group">
                            <label class="control-label"> Defeito </label>
                            <div class="controls">
                                <select name="tv" onchange="selectDefeito('tv');" id="tv" style="display: none;" required>
                                    <option value="">
                                        > Tipo TV
                                    </option>
                                    <option value="Não Liga"> Não Liga</option>
                                    <option value="Tela manchada"> Tela manchada</option>
                                    <option value="Aparelho molhou"> Aparelho molhou</option>
                                    <option value="Não funciona WI-FI"> Não funciona WI-FI</option>
                                    <option value="Imagem travada"> Imagem travada</option>
                                    <option value="C. Remoto não funciona"> C. Remoto não funciona</option>
                                    <option value="Não acessa Internet"> Não acessa Internet</option>
                                    <option value="Não funciona função smart"> Não funciona função smart</option>
                                    <option value="Touch não funciona';"> Touch não funciona</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="videogame" onchange="selectDefeito('videogame');" id="videogame" style="display: none;" required>
                                    <option value="">
                                        > Tipo Videogame
                                    </option>
                                    <option value="Não Liga"> Não Liga</option>
                                    <option value="Tela Quebrada"> Tela Quebrada</option>
                                    <option value="Desbloqueio"> Desbloqueio</option>
                                    <option value="Atualização"> Atualização</option>
                                    <option value="Fonte de Energia com Problema"> Fonte de Energia com Problema
                                    </option>
                                    <option value="Travando ou Lento"> Travando ou Lento</option>
                                    <option value="Aquecendo demais"> Aquecendo demais</option>
                                    <option value="Não reconhece jogos"> Não reconhece jogos</option>
                                    <option value="Luz Vermelha / Verde"> Luz Vermelha / Verde</option>
                                    <option value="Desligando sozinho"> Desligando sozinho</option>
                                    <option value="Não reconhece Controle"> Não reconhece Controle</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="celular" onchange="selectDefeito('celular');" id="celular" style="display: none;" required>
                                    <option value="">
                                        > Tipo Celular
                                    </option>
                                    <option value="Não liga"> Não liga</option>
                                    <option value="Tela Quebrada"> Tela Quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Tela com Manchas"> Tela com Manchas</option>
                                    <option value="Touch Screen Quebrado"> Touch Screen Quebrado</option>
                                    <option value="Celular Molhou"> Celular Molhou</option>
                                    <option value="Não funciona Wifi"> Não funciona Wifi</option>
                                    <option value="Não Funciona Câmera Frontal"> Não Funciona Câmera Frontal</option>
                                    <option value="Não Funciona Câmera Traseira"> Não Funciona Câmera Traseira</option>
                                    <option value="Botão Home"> Botão Home</option>
                                    <option value="Botão de LIgar"> Botão de LIgar</option>
                                    <option value="Botões de Volume"> Botões de Volume</option>
                                    <option value="Não tem Sinal de Operadora"> Não tem Sinal de Operadora</option>
                                    <option value="Não Carrega"> Não Carrega</option>
                                    <option value="Desligando sozinho"> Desligando sozinho</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Com erros de aplicativo"> Com erros de aplicativo</option>
                                    <option value="Não reconhece Chip"> Não reconhece Chip</option>
                                    <option value="Descarregando rápido"> Descarregando rápido</option>
                                    <option value="Eu não escuto em ligações"> Eu não escuto em ligações</option>
                                    <option value="Não me escutam em ligações"> Não me escutam em ligações</option>
                                    <option value="Não toca músicas"> Não toca músicas</option>
                                    <option value="Não Funciona Viva Voz"> Não Funciona Viva Voz</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="notebook" onchange="selectDefeito('notebook');" id="notebook" style="display: none;" required>
                                    <option value="">
                                        > Tipo Notebook
                                    </option>
                                    <option value="Tela quebrada"> Tela quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Formatação / Atualização"> Formatação / Atualização</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Não Inicia"> Não Inicia</option>
                                    <option value="Tela Preta / Azul"> Tela Preta / Azul</option>
                                    <option value="Reparo de placa mãe / BGA"> Reparo de placa mãe / BGA</option>
                                    <option value="Muito Lento"> Muito Lento</option>
                                    <option value="Remoção de Vírus"> Remoção de Vírus</option>
                                    <option value="Reiniciando Sozinho"> Reiniciando Sozinho</option>
                                    <option value="Esquentando Demais"> Esquentando Demais</option>
                                    <option value="Problemas na Bateria"> Problemas na Bateria</option>
                                    <option value="Problemas no Carregador"> Problemas no Carregador</option>
                                    <option value="Problemas no Teclado"> Problemas no Teclado</option>
                                    <option value="Carcaça quebrada"> Carcaça quebrada</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="tablet" onchange="selectDefeito('tablet');" id="tablet" style="display: none;" required>
                                    <option value="">
                                        > Tipo Tablet
                                    </option>
                                    <option value="Não liga"> Não liga</option>
                                    <option value="Tela Quebrada"> Tela Quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Tela com Manchas"> Tela com Manchas</option>
                                    <option value="Touch Screen Quebrado"> Touch Screen Quebrado</option>
                                    <option value="Tablet Molhou"> Tablet Molhou</option>
                                    <option value="Não funciona Wifi"> Não funciona Wifi</option>
                                    <option value="Não Funciona Câmera Frontal"> Não Funciona Câmera Frontal</option>
                                    <option value="Não Funciona Câmera Traseira"> Não Funciona Câmera Traseira</option>
                                    <option value="Botão Home"> Botão Home</option>
                                    <option value="Botão de LIgar"> Botão de LIgar</option>
                                    <option value="Botões de Volume"> Botões de Volume</option>
                                    <option value="Não tem Sinal de Operadora"> Não tem Sinal de Operadora</option>
                                    <option value="Não Carrega"> Não Carrega</option>
                                    <option value="Desligando sozinho"> Desligando sozinho</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Com erros de aplicativo"> Com erros de aplicativo</option>
                                    <option value="Não reconhece Chip"> Não reconhece Chip</option>
                                    <option value="Descarregando rápido"> Descarregando rápido</option>
                                    <option value="Eu não escuto em ligações"> Eu não escuto em ligações</option>
                                    <option value="Não me escutam em ligações"> Não me escutam em ligações</option>
                                    <option value="Não toca músicas"> Não toca músicas</option>
                                    <option value="Não Funciona Viva Voz"> Não Funciona Viva Voz</option>
                                    <option value="Outros..."> Outros...</option>
                                </select>
                                <select name="computador" onchange="selectDefeito('computador');" id="computador" style="display: none;" required>
                                    <option value="">
                                        > Tipo Computador
                                    </option>
                                    <option value="Não liga"> Não liga</option>
                                    <option value="Tela Quebrada"> Tela Quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Tela com Manchas"> Tela com Manchas</option>
                                    <option value="Touch Screen Quebrado"> Touch Screen Quebrado</option>
                                    <option value="Computador Molhou"> Computador Molhou</option>
                                    <option value="Não funciona Wifi"> Não funciona Wifi</option>
                                    <option value="Não Funciona Câmera Frontal"> Não Funciona Câmera Frontal</option>
                                    <option value="Não Funciona Câmera Traseira"> Não Funciona Câmera Traseira</option>
                                    <option value="Botão Home"> Botão Home</option>
                                    <option value="Botão de LIgar"> Botão de LIgar</option>
                                    <option value="Botões de Volume"> Botões de Volume</option>
                                    <option value="Não tem Sinal de Operadora"> Não tem Sinal de Operadora</option>
                                    <option value="Não Carrega"> Não Carrega</option>
                                    <option value="Desligando sozinho"> Desligando sozinho</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Com erros de aplicativo"> Com erros de aplicativo</option>
                                    <option value="Não reconhece Chip"> Não reconhece Chip</option>
                                    <option value="Descarregando rápido"> Descarregando rápido</option>
                                    <option value="Eu não escuto em ligações"> Eu não escuto em ligações</option>
                                    <option value="Não me escutam em ligações"> Não me escutam em ligações</option>
                                    <option value="Não toca músicas"> Não toca músicas</option>
                                    <option value="Não Funciona Viva Voz"> Não Funciona Viva Voz</option>
                                    <option value="Outros..."> Outros...</option>
                                </select>
                                <select name="iMac" onchange="selectDefeito('iMac');" id="iMac" style="display: none;" required>
                                    <option value="">
                                        > Tipo iMac
                                    </option>
                                    <option value="Tela quebrada"> Tela quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Formatação / Atualização"> Formatação / Atualização</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Não Inicia"> Não Inicia</option>
                                    <option value="Tela Preta / Azul"> Tela Preta / Azul</option>
                                    <option value="Reparo de placa mãe / BGA"> Reparo de placa mãe / BGA</option>
                                    <option value="Muito Lento"> Muito Lento</option>
                                    <option value="Remoção de Vírus"> Remoção de Vírus</option>
                                    <option value="Reiniciando Sozinho"> Reiniciando Sozinho</option>
                                    <option value="Esquentando Demais"> Esquentando Demais</option>
                                    <option value="Problemas na Bateria"> Problemas na Bateria</option>
                                    <option value="Problemas no Carregador"> Problemas no Carregador</option>
                                    <option value="Problemas no Teclado"> Problemas no Teclado</option>
                                    <option value="Carcaça quebrada"> Carcaça quebrada</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="Mac" onchange="selectDefeito('Mac');" id="Mac" style="display: none;" required>
                                    <option value="">
                                        > Tipo Mac
                                    </option>
                                    <option value="Tela quebrada"> Tela quebrada</option>
                                    <option value="Não aparece imagem na tela"> Não aparece imagem na tela</option>
                                    <option value="Formatação / Atualização"> Formatação / Atualização</option>
                                    <option value="Travado"> Travado</option>
                                    <option value="Não Inicia"> Não Inicia</option>
                                    <option value="Tela Preta / Azul"> Tela Preta / Azul</option>
                                    <option value="Reparo de placa mãe / BGA"> Reparo de placa mãe / BGA</option>
                                    <option value="Muito Lento"> Muito Lento</option>
                                    <option value="Remoção de Vírus"> Remoção de Vírus</option>
                                    <option value="Reiniciando Sozinho"> Reiniciando Sozinho</option>
                                    <option value="Esquentando Demais"> Esquentando Demais</option>
                                    <option value="Problemas na Bateria"> Problemas na Bateria</option>
                                    <option value="Problemas no Carregador"> Problemas no Carregador</option>
                                    <option value="Problemas no Teclado"> Problemas no Teclado</option>
                                    <option value="Carcaça quebrada"> Carcaça quebrada</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <select name="drone" onchange="selectDefeito('drone');" id="drone" style="display: none;" required>
                                    <option value="">
                                        > Tipo Drone
                                    </option>
                                    <option value="Não liga"> Não liga</option>
                                    <option value="Outros"> Outros</option>
                                </select>
                                <input id="defoutros" type="text" name="defoutros"/>
                            </div>
                        </div>
                        <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                            <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                <b>Marcar Todos</b>
                            </label>
                            <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                                <input type="checkbox" id="checkMarcarTodos" />
                                <br><br>
                            </div>
                        </div>
                        <div id="celular2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela Display
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o display não aparecer imagem corretamente ou apresente alguma avaria(trincos, manchas ou riscos). IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1celular" id="check1celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Touch Screen
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o Touch do aparelho não funcionar corretamente, mesmo que apenas em algum ponto especifico da tela que não funcione. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2celular" id="check2celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3celular">
                                            <div class="popover-body">
                                              Selecione esta opção se as teclas(Home, Volume, Power) não funcione corretamente. Se alguma especificamente não funcionar, informe no campo de OBSERVAÇÕES na próxima tela quando tiver inserindo as informações da ordem de serviços. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3celular" id="check3celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sensores de Proximidade
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4celular">
                                            <div class="popover-body">
                                              Selecione esta opção se após ligar o aparelho e realizar uma ligação ele não apagar a tela, ou seja, mesmo durante uma ligação ele fica sempre com tela ligada(o normal é apagar ao encostar na orelha). IMPORTANTE: se o celular não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4celular" id="check4celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imei Zerado/Replicado/Em branco
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5celular">
                                            <div class="popover-body">
                                              Selecione esta opção se digitar no Celular o código *#06# e aparecer o IMEI do celular tudo zerado, apagado ou em branco pois o padrão é aparecer seu código de 15 até 17 dígitos. IMPORTANTE: Se o celular não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5celular" id="check5celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imei Bloqueado
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6celular">
                                            <div class="popover-body">
                                              Selecione esta opção após consultar o imei no site da anatel. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6celular" id="check6celular" value="1"> Sim <a href="javascript:;" onclick="$('#form-consulta-imei').attr('target', '_blank').submit();" style="background: #005580;color: white;padding: 2px 10px;margin-left: 15px;">Consultar Aqui</a><br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Bluetooth ele desabilitar sozinho no mesmo instante, ou se mesmo habilitando não conseguir se conectar com outro celular ou com outro equipamento como uma caixinha de som por exemplo. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7celular" id="check7celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Wifi ele desabilitar sozinho no mesmo instante, ou se mesmo habilitando não conseguir se conectar em sua rede Wifi. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8celular" id="check8celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Wifi e mesmo habilitando e conectando a sua rede, ele não navegar na internet. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9celular" id="check9celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não faz Ligações
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check10celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check10celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho não realizar ligações nem com o chip do cliente nem com um chip da loja(um que tenha certeza que esteja bom). IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10celular" id="check10celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não recebe Ligações
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check11celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check11celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um chip da loja(um que tenha certeza que esta bom) no celular e mesmo assim ele não recebe ligação. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11celular" id="check11celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Alto Falante
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check12celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check12celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar uma música para tocar e não conseguir escutar ou quando habilitar o viva Voz e também não conseguir escutar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12celular" id="check12celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio Auricular
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check13celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check13celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação com o aparelho do cliente você não conseguir escutar o que a outra pessoa esta falando, muita vezes só escuta quando ativa o viva Voz. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13celular" id="check13celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Microfone principal
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check14celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check14celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação normal com o aparelho do cliente a outra pessoa não escute o que você falar, porém muitas vezes com o viva voz ativado a pessoa consegue ouvir o que você falar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check14celular" id="check14celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Microfone Secundário
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check15celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check15celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação através do Whatsapp com o celular do cliente a outra pessoa não escutar o que você fala, na maioria dos casos se ativar o viva voz ou se fizer a ligação normalmente sem ser por Whatsapp a pessoa escuta você falar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check15celular" id="check15celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Câmera Frontal
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check16celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check16celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando ativar a câmera frontal do celular ele travar ou não abrir a função da câmera. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check16celular" id="check16celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Câmera Traseira
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check17celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check17celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando ativar a câmera traseira do celular ele travar ou não abrir a função da câmera. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check17celular" id="check17celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check18celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check18celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar o aparelho para carregar com um carregador da loja(um que tenha certeza que esteja bom) e ele não mostrar que esta carregando em sua tela. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check18celular" id="check18celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bateria não carrega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check19celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check19celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando colocar o celular para carregar e ele mostrar que foi conectado, porém não subir a porcentagem de carga após alguns minutos carregando. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check19celular" id="check19celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check20celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check20celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho tiver entrada para cartão de memória e ao conectar um cartão de memória, ele não mostrar nenhuma informação que foi inserido ou removido esse cartão de memória, e nas configurações a memória não for detectada 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check20celular" id="check20celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de fone de ouvido
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check21celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check21celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho tiver entrada para fone de ouvido e não reconhecer mostrando em sua tela quando um fone foi conectado e desconectado. Se mesmo sem o fone de ouvido ele tiver apresentando a informação de que esta conectado um fone nele, também deverá marcar esta opção. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check21celular" id="check21celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Bateria Estufada
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check22celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check22celular">
                                            <div class="popover-body">
                                              Selecione esta opção ao perceber visualmente que a bateria do celular esta cheia "estufada", nesse caso também deverá colocar nas OBSERVAÇÕES na próxima tela quando for inserir as informações da ordem de serviços. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check22celular" id="check22celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check23celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check23celular">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na celular. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check23celular" id="check23celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Celular sem acessórios
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check24celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check24celular">
                                            <div class="popover-body">
                                              Evite pegar qualquer tipo de acessórios do cliente(chip, carregador, cabos, cartão de memória, capas e etc) com esta opção só dará maior relevância ao fato de que realmente não pegou nada do cliente a não ser o próprio aparelho. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check24celular" id="check24celular" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="tablet2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tela
                                    Display </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1tablet" id="check1tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Touch
                                    Screen </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2tablet" id="check2tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Teclas </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3tablet" id="check3tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Sensores de Proximidade </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4tablet" id="check4tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Bluetooth </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5tablet" id="check5tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Wifi </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6tablet" id="check6tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Ligações </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7tablet" id="check7tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Alto
                                    Falante </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8tablet" id="check8tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio
                                    Auricular </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check0tablet" id="check0tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Microfone </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10tablet" id="check10tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Câmera </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11tablet" id="check11tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de carregador </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12tablet" id="check12tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de cartão de memória </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13tablet" id="check13tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de fone de ouvido </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check14tablet" id="check14tablet" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="tv2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv ligar porém não mostrar imagem. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1tv2" id="check1tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver com avarias na tela(manchas, ricos ou trincos).  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2tv2" id="check2tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imagem travada
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver ligando mas ficar com a tela travada em determinada imagem ou travando repetidamente. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3tv2" id="check3tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não sai som
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver ligando mas o audio dela não estiver saindo. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4tv2" id="check4tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não funciona Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não funcionar a função de Wifi. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5tv2" id="check5tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não acessa internet
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver conectando no Wifi porém não acessar a internet ou navegar. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6tv2" id="check6tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Controle remoto não funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver sem reconhecer o controle remoto e apenas as teclas da tv que estiver funcionando para mudar canais, aumentar volume etc.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7tv2" id="check7tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece Carregador ou cabo de força
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o carregador ou cabo de força. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8tv2" id="check8tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece HDMI
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o cabo HDMI. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.   
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9tv2" id="check9tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece VGA
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check10tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check10tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o cabo VGA. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10tv2" id="check10tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tecladas da TV
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check11tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check11tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver funcionando as teclas de volume, mudar canal, menu etc. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11tv2" id="check11tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check12tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check12tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a carcaça da Tv estiver visualmente com avarias(trincos, risco, partes quebradas). IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12tv2" id="check12tv2" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="videogame2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o vídeo game não mostrar imagem. IMPORTANTE: se o Vídeo Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1videogame" id="check1videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Lentidão/Travamento
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Video Game estiver com lentidão ou travando após alguns instantes ligado ou com algum jogo. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2videogame" id="check2videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece Jogos
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um jogo adequado a sua atualização e modelo e ele não reconhecer. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3videogame" id="check3videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problemas na Bandeja
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4videogame">
                                            <div class="popover-body">
                                              Selecione esta opção de a bandeja tiver dificuldade ou não abrir e fechar com facilidade. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4videogame" id="check4videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi não conecta
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Wifi não conectar. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5videogame" id="check5videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Super Aquecimento
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Video Games esquentar muito após alguns instantes ligado ou jogando. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6videogame" id="check6videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não tem HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o video game estiver se o HD. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7videogame" id="check7videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não tem cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o vídeo game estiver se cartão de memória. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8videogame" id="check8videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Video Game. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9videogame" id="check9videogame" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="notebook2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    aparelho esta ligando? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1notebook" id="check1notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    aparelho mostra imagem? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2notebook" id="check2notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    display está sem avarias? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3notebook" id="check3notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    notebook está detectando o HD? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4notebook" id="check4notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    sistema operacional inicia? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5notebook" id="check5notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Se o
                                    sistema inicia, as teclas do teclado estão respondendo? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6notebook" id="check6notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teste
                                    de Áudio </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7notebook" id="check7notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teste
                                    de WIFI </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8notebook" id="check8notebook" value="1"> Não
                                    <br></div>
                            </div>
                        </div>
                        <div id="computador2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1computador">
                                            <div class="popover-body">
                                              Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1computador" id="check1computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela/Monitor com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2computador">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela ou monitor desse computador, se tiver algum risco ou trinco marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2computador" id="check2computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3computador">
                                            <div class="popover-body">
                                              Selecione esta opção se ao ligar o computador se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3computador" id="check3computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4computador">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4computador" id="check4computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5computador">
                                            <div class="popover-body">
                                              Selecione esta opção se qualquer tecla do teclado ou CPU não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5computador" id="check5computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6computador">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6computador" id="check6computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7computador">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso tembém deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7computador" id="check7computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Cliente entregou apenas a CPU
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8computador">
                                            <div class="popover-body">
                                              (Evite ficar com acessórios como mouses ou teclados a não ser em último caso, se não ficar marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8computador" id="check8computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça CPU com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9computador">
                                            <div class="popover-body">
                                              Marque esta opção se visualmente perceber marcas como riscos, amassados etc... na CPU. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9computador" id="check9computador" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="drone2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não ativa Motores
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone ligar porém os motores não ativar ou ativar parcialmente. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1drone" id="check1drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não Liga após atualização
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando cliente informar que drone parou de ligar após uma atualização. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2drone" id="check2drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sem imagem na Câmera
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone não exibir imagem de vídeo seja no aplicativo ou monitor. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3drone" id="check3drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bateria Não Carrega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando a Bateria for conectada no carregador e não subir carga ou carregador não reconhecer a bateria que foi conectada nele. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4drone" id="check4drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Rádio não se comunica com Drone
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5drone">
                                            <div class="popover-body">
                                              Selecione este opção quando o Drone e o rádio ligar porém não dar nenhum comando. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5drone" id="check5drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Drone não estabiliza
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Cliente relatar quando durante o voo o Drone não ficar parado de forma mais estabilizada. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6drone" id="check6drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Drone vibrando
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone estiver vibrando nitidamente ou com ondulações durante filmagem. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7drone" id="check7drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Leds Intermintente
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone sinalizar algum erro através dos Leds como: piscando entre verde laranja e vermelho, piscando só vermelho, piscando laranja rapidamente etc. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8drone" id="check8drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Mensagem Gimbal Error
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando na tela do monitor ou aplicativo aparecer a mensagem Guimbal Error. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9drone" id="check9drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Gimbal mexedo sozinho
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check10drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check10drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar se movimentando sozinho. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10drone" id="check10drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Guimbal não mexe e com imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check11drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check11drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar sem movimento mas com imagem no monitor ou aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11drone" id="check11drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Gimbal não mexe e sem imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check12drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check12drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar sem movimento e sem imagem no monitor ou aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12drone" id="check12drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Helice com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check13drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check13drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando visualmente perceber que a Helice apresentar avarias(riscos, amassados, trincos e etc). OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13drone" id="check13drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de carga não funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check14drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check14drone">
                                            <div class="popover-body">
                                              Selecione esta opção se Drone tiver um conector de carga nele e se colocar ele para carregar com um carregador(um que tenha certeza que esteja bom) e ele não mostrar que esta carregando. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check14drone" id="check14drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check15drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check15drone">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um cartão de memória no Drone e ele não reconhecer que foi inserido um cartão nele. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check15drone" id="check15drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não grava video e fotos
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check16drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check16drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone reconhece o cartão de memória mas não grava nele, porém grava no aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check16drone" id="check16drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Fora de foco
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check17drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check17drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o cliente relatar que a imagem gravada esta embaçada ou manchas. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check17drone" id="check17drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Perca no alcance do Rádio
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check18drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check18drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o cliente relatar que o drone perde sinal todo instante e bem próximo do rádio. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check18drone" id="check18drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não Conecta no Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check19drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check19drone">
                                            <div class="popover-body">
                                              Selecione esta opção se o Drone não conectar na sua rede Wifi. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check19drone" id="check19drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça/Shell com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check20drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check20drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando visualmente perceber que a carcaça/Shell apresenta avarias(riscos, amassados, trincos e etc). OBS: as fotos que serão batidas deverão focar nessas avarias observadas.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check20drone" id="check20drone" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="iMac2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1iMac">
                                            <div class="popover-body">
                                              (Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1iMac" id="check1iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2iMac">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela desse notebook, se tiver alguma avaria(risco, trinco, mancha etc...) marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2iMac" id="check2iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o notebook se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3iMac" id="check3iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4iMac" id="check4iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se qualquer tecla do teclado não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5iMac" id="check5iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6iMac" id="check6iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7iMac">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse, HD externo etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso também deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7iMac" id="check7iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Bateria
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook a bateria, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8iMac" id="check8iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook o carregador, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9iMac" id="check9iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Não Reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check10iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check10iMac">
                                            <div class="popover-body">
                                              (Para saber se este reconhecendo o Wifi, se o Notebook estiver ligando deverá tentar conectar ele em sua rede Wifi e navegar com ele. Se o Notebook não estiver ligando deverá marcar como não esta funcionando o Wifi para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10iMac" id="check10iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Conecta mas não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check11iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check11iMac">
                                            <div class="popover-body">
                                              (Após conectar o Wifi abra um navegador como o Mozila ou Chrome e tente "navegar" na internet com ele com ele. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11iMac" id="check11iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth Não reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check12iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check12iMac">
                                            <div class="popover-body">
                                              (Se o notebook estiver ligando e ele tiver esta opção de Bluetooth ativer e veja se o celular reconhece e transfere algum arquivo, normalmente têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Procure por um interruptor no seu notebook ou por uma tecla no teclado. Frequentemente, a tecla é acessada com a ajuda da tecla Fn na cor azul.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12iMac" id="check12iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check13iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check13iMac">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Notebook. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13iMac" id="check13iMac" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="Mac2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check1iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check1iMac">
                                            <div class="popover-body">
                                              (Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1iMac2" id="check1iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check2iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check2iMac">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela desse notebook, se tiver alguma avaria(risco, trinco, mancha etc...) marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2iMac2" id="check2iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check3iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check3iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o notebook se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3iMac2" id="check3iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check4iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check4iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4iMac2" id="check4iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check5iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check5iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se qualquer tecla do teclado não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5iMac2" id="check5iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check6iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check6iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6iMac2" id="check6iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check7iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check7iMac">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse, HD externo etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso também deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7iMac2" id="check7iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Bateria
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check8iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check8iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook a bateria, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8iMac2" id="check8iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check9iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check9iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook o carregador, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check9iMac2" id="check9iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Não Reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check10iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check10iMac">
                                            <div class="popover-body">
                                              (Para saber se este reconhecendo o Wifi, se o Notebook estiver ligando deverá tentar conectar ele em sua rede Wifi e navegar com ele. Se o Notebook não estiver ligando deverá marcar como não esta funcionando o Wifi para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10iMac2" id="check10iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Conecta mas não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check11iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check11iMac">
                                            <div class="popover-body">
                                              (Após conectar o Wifi abra um navegador como o Mozila ou Chrome e tente "navegar" na internet com ele com ele. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11iMac2" id="check11iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth Não reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check12iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check12iMac">
                                            <div class="popover-body">
                                              (Se o notebook estiver ligando e ele tiver esta opção de Bluetooth ativer e veja se o celular reconhece e transfere algum arquivo, normalmente têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Procure por um interruptor no seu notebook ou por uma tecla no teclado. Frequentemente, a tecla é acessada com a ajuda da tecla Fn na cor azul.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12iMac2" id="check12iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-check13iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-check13iMac">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Notebook. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13iMac2" id="check13iMac2" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="widget-title" id="title-checklist-saida">
                        <span class="icon">
                                        <i class="icon-warning-sign"></i>
                                    </span>
                        <h5>
                            Check list de saída - <font style="color: #F66;"> Marque as funções após a entrega do equipamento. </font>
                        </h5>
                    </div>
                    <div>
                        <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                            <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                <b>Marcar Todos</b>
                            </label>
                            <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                                <input type="checkbox" id="saida_checkMarcarTodos" />
                                <br><br>
                            </div>
                        </div>
                        <div id="saida_celular2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela Display
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o display não aparecer imagem corretamente ou apresente alguma avaria(trincos, manchas ou riscos). IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1celular" id="saida_check1celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Touch Screen
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o Touch do aparelho não funcionar corretamente, mesmo que apenas em algum ponto especifico da tela que não funcione. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2celular" id="saida_check2celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3celular">
                                            <div class="popover-body">
                                              Selecione esta opção se as teclas(Home, Volume, Power) não funcione corretamente. Se alguma especificamente não funcionar, informe no campo de OBSERVAÇÕES na próxima tela quando tiver inserindo as informações da ordem de serviços. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3celular" id="saida_check3celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sensores de Proximidade
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4celular">
                                            <div class="popover-body">
                                              Selecione esta opção se após ligar o aparelho e realizar uma ligação ele não apagar a tela, ou seja, mesmo durante uma ligação ele fica sempre com tela ligada(o normal é apagar ao encostar na orelha). IMPORTANTE: se o celular não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4celular" id="saida_check4celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imei Zerado/Replicado/Em branco
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5celular">
                                            <div class="popover-body">
                                              Selecione esta opção se digitar no Celular o código *#06# e aparecer o IMEI do celular tudo zerado, apagado ou em branco pois o padrão é aparecer seu código de 15 até 17 dígitos. IMPORTANTE: Se o celular não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5celular" id="saida_check5celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imei Bloqueado
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6celular">
                                            <div class="popover-body">
                                              Selecione esta opção após consultar o imei no site da anatel. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6celular" id="saida_check6celular" value="1"> Sim <a href="javascript:;" onclick="$('#form-consulta-imei').attr('target', '_blank').submit();" style="background: #005580;color: white;padding: 2px 10px;margin-left: 15px;">Consultar Aqui</a><br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Bluetooth ele desabilitar sozinho no mesmo instante, ou se mesmo habilitando não conseguir se conectar com outro celular ou com outro equipamento como uma caixinha de som por exemplo. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7celular" id="saida_check7celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Wifi ele desabilitar sozinho no mesmo instante, ou se mesmo habilitando não conseguir se conectar em sua rede Wifi. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8celular" id="saida_check8celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9celular">
                                            <div class="popover-body">
                                              Selecione esta opção se for nas configurações do Celular e ao clicar para habilitar o Wifi e mesmo habilitando e conectando a sua rede, ele não navegar na internet. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9celular" id="saida_check9celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não faz Ligações
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check10celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check10celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho não realizar ligações nem com o chip do cliente nem com um chip da loja(um que tenha certeza que esteja bom). IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check10celular" id="saida_check10celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não recebe Ligações
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check11celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check11celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um chip da loja(um que tenha certeza que esta bom) no celular e mesmo assim ele não recebe ligação. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check11celular" id="saida_check11celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Alto Falante
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check12celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check12celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar uma música para tocar e não conseguir escutar ou quando habilitar o Viva Voz e também não conseguir escutar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check12celular" id="saida_check12celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio Auricular
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check13celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check13celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação com o aparelho do cliente você não conseguir escutar o que a outra pessoa esta falando, muita vezes só escuta quando ativa o viva Voz. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check13celular" id="saida_check13celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Microfone principal
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check14celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check14celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação normal com o aparelho do cliente a outra pessoa não escute o que você falar, porém muitas vezes com o viva voz ativado a pessoa consegue ouvir o que você falar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check14celular" id="saida_check14celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Microfone Secundário
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check15celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check15celular">
                                            <div class="popover-body">
                                              Selecione esta opção se durante uma ligação através do Whatsapp com o celular do cliente a outra pessoa não escutar o que você fala, na maioria dos casos se ativar o viva voz ou se fizer a ligação normalmente sem ser por Whatsapp a pessoa escuta você falar. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check15celular" id="saida_check15celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Câmera Frontal
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check16celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check16celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando ativar a câmera frontal do celular ele travar ou não abrir a função da câmera. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check16celular" id="saida_check16celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Câmera Traseira
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check17celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check17celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando ativar a câmera traseira do celular ele travar ou não abrir a função da câmera. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check17celular" id="saida_check17celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check18celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check18celular">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar o aparelho para carregar com um carregador da loja(um que tenha certeza que esteja bom) e ele não mostrar que esta carregando em sua tela. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check18celular" id="saida_check18celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bateria não carrega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check19celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check19celular">
                                            <div class="popover-body">
                                              Selecione esta opção se quando colocar o celular para carregar e ele mostrar que foi conectado, porém não subir a porcentagem de carga após alguns minutos carregando. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check19celular" id="saida_check19celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check20celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check20celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho tiver entrada para cartão de memória e ao conectar um cartão de memória, ele não mostrar nenhuma informação que foi inserido ou removido esse cartão de memória, e nas configurações a memória não for detectada 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check20celular" id="saida_check20celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de fone de ouvido
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check21celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check21celular">
                                            <div class="popover-body">
                                              Selecione esta opção se o aparelho tiver entrada para fone de ouvido e não reconhecer mostrando em sua tela quando um fone foi conectado e desconectado. Se mesmo sem o fone de ouvido ele tiver apresentando a informação de que esta conectado um fone nele, também deverá marcar esta opção. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check21celular" id="saida_check21celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Bateria Estufada
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check22celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check22celular">
                                            <div class="popover-body">
                                              Selecione esta opção ao perceber visualmente que a bateria do celular esta cheia "estufada", nesse caso também deverá colocar nas OBSERVAÇÕES na próxima tela quando for inserir as informações da ordem de serviços. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check22celular" id="saida_check22celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check23celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check23celular">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na celular. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check23celular" id="saida_check23celular" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Celular sem acessórios
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check24celular" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check24celular">
                                            <div class="popover-body">
                                              Evite pegar qualquer tipo de acessórios do cliente(chip, carregador, cabos, cartão de memória, capas e etc) com esta opção só dará maior relevância ao fato de que realmente não pegou nada do cliente a não ser o próprio aparelho. IMPORTANTE: se o aparelho não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check24celular" id="saida_check24celular" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_tablet2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tela
                                    Display </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1tablet" id="saida_check1tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Touch
                                    Screen </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2tablet" id="saida_check2tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Teclas </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3tablet" id="saida_check3tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Sensores de Proximidade </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4tablet" id="saida_check4tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Bluetooth </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5tablet" id="saida_check5tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Wifi </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6tablet" id="saida_check6tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Ligações </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7tablet" id="saida_check7tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Alto
                                    Falante </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8tablet" id="saida_check8tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio
                                    Auricular </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check0tablet" id="saida_check0tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Microfone </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check10tablet" id="saida_check10tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Câmera </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check11tablet" id="saida_check11tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de carregador </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check12tablet" id="saida_check12tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de cartão de memória </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check13tablet" id="saida_check13tablet" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px">
                                    Conector de fone de ouvido </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check14tablet" id="saida_check14tablet" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_tv2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv ligar porém não mostrar imagem. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1tv2" id="saida_check1tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver com avarias na tela(manchas, ricos ou trincos).  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2tv2" id="saida_check2tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Imagem travada
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver ligando mas ficar com a tela travada em determinada imagem ou travando repetidamente. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3tv2" id="saida_check3tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não sai som
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver ligando mas o audio dela não estiver saindo. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4tv2" id="saida_check4tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não funciona Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não funcionar a função de Wifi. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5tv2" id="saida_check5tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não acessa internet
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver conectando no Wifi porém não acessar a internet ou navegar. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6tv2" id="saida_check6tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Controle remoto não funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv estiver sem reconhecer o controle remoto e apenas as teclas da tv que estiver funcionando para mudar canais, aumentar volume etc.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7tv2" id="saida_check7tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece Carregador ou cabo de força
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o carregador ou cabo de força. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8tv2" id="saida_check8tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece HDMI
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o cabo HDMI. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.   
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9tv2" id="saida_check9tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece VGA
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check10tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check10tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver reconhecendo o cabo VGA. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check10tv2" id="saida_check10tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tecladas da TV
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check11tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check11tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a Tv não estiver funcionando as teclas de volume, mudar canal, menu etc. IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check11tv2" id="saida_check11tv2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check12tv2" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check12tv2">
                                            <div class="popover-body">
                                              Selecione esta opção se a carcaça da Tv estiver visualmente com avarias(trincos, risco, partes quebradas). IMPORTANTE: se a Tv não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check12tv2" id="saida_check12tv2" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_videogame2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o vídeo game não mostrar imagem. IMPORTANTE: se o Vídeo Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1videogame" id="saida_check1videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Lentidão/Travamento
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Video Game estiver com lentidão ou travando após alguns instantes ligado ou com algum jogo. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2videogame" id="saida_check2videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece Jogos
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um jogo adequado a sua atualização e modelo e ele não reconhecer. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3videogame" id="saida_check3videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problemas na Bandeja
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4videogame">
                                            <div class="popover-body">
                                              Selecione esta opção de a bandeja tiver dificuldade ou não abrir e fechar com facilidade. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4videogame" id="saida_check4videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi não conecta
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Wifi não conectar. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5videogame" id="saida_check5videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Super Aquecimento
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o Video Games esquentar muito após alguns instantes ligado ou jogando. IMPORTANTE: se o Video Game não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6videogame" id="saida_check6videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não tem HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o video game estiver se o HD. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7videogame" id="saida_check7videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não tem cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se o vídeo game estiver se cartão de memória. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8videogame" id="saida_check8videogame" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9videogame" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9videogame">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Video Game. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9videogame" id="saida_check9videogame" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_notebook2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    aparelho esta ligando? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check1notebook" id="saida_check1notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    aparelho mostra imagem? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check2notebook" id="saida_check2notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    display está sem avarias? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check3notebook" id="saida_check3notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    notebook está detectando o HD? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check4notebook" id="saida_check4notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> O
                                    sistema operacional inicia? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check5notebook" id="saida_check5notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Se o
                                    sistema inicia, as teclas do teclado estão respondendo? </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check6notebook" id="saida_check6notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teste
                                    de Áudio </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check7notebook" id="saida_check7notebook" value="1"> Não
                                    <br></div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teste
                                    de WIFI </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="check8notebook" id="saida_check8notebook" value="1"> Não
                                    <br></div>
                            </div>
                        </div>
                        <div id="saida_computador2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1computador">
                                            <div class="popover-body">
                                              Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1computador" id="saida_check1computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela/Monitor com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2computador">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela ou monitor desse computador, se tiver algum risco ou trinco marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2computador" id="saida_check2computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3computador">
                                            <div class="popover-body">
                                              Selecione esta opção se ao ligar o computador se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3computador" id="saida_check3computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4computador">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4computador" id="saida_check4computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5computador">
                                            <div class="popover-body">
                                              Selecione esta opção se qualquer tecla do teclado ou CPU não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5computador" id="saida_check5computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6computador">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6computador" id="saida_check6computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7computador">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso tembém deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços. IMPORTANTE: se o computador não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7computador" id="saida_check7computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Cliente entregou apenas a CPU
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8computador">
                                            <div class="popover-body">
                                              (Evite ficar com acessórios como mouses ou teclados a não ser em último caso, se não ficar marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8computador" id="saida_check8computador" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça CPU com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9computador" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9computador">
                                            <div class="popover-body">
                                              Marque esta opção se visualmente perceber marcas como riscos, amassados etc... na CPU. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9computador" id="saida_check9computador" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_drone2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não ativa Motores
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone ligar porém os motores não ativar ou ativar parcialmente. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1drone" id="saida_check1drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Não Liga após atualização
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando cliente informar que drone parou de ligar após uma atualização. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2drone" id="saida_check2drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sem imagem na Câmera
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone não exibir imagem de vídeo seja no aplicativo ou monitor. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3drone" id="saida_check3drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bateria Não Carrega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando a Bateria for conectada no carregador e não subir carga ou carregador não reconhecer a bateria que foi conectada nele. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4drone" id="saida_check4drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Rádio não se comunica com Drone
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5drone">
                                            <div class="popover-body">
                                              Selecione este opção quando o Drone e o rádio ligar porém não dar nenhum comando. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5drone" id="saida_check5drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Drone não estabiliza
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Cliente relatar quando durante o voo o Drone não ficar parado de forma mais estabilizada. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6drone" id="saida_check6drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Drone vibrando
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone estiver vibrando nitidamente ou com ondulações durante filmagem. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7drone" id="saida_check7drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Leds Intermintente
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone sinalizar algum erro através dos Leds como: piscando entre verde laranja e vermelho, piscando só vermelho, piscando laranja rapidamente etc. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8drone" id="saida_check8drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Mensagem Gimbal Error
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando na tela do monitor ou aplicativo aparecer a mensagem Guimbal Error. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9drone" id="saida_check9drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Gimbal mexedo sozinho
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check10drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check10drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar se movimentando sozinho. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check10drone" id="saida_check10drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Guimbal não mexe e com imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check11drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check11drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar sem movimento mas com imagem no monitor ou aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check11drone" id="saida_check11drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Gimbal não mexe e sem imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check12drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check12drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Gimbal(suporte da câmera) ficar sem movimento e sem imagem no monitor ou aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check12drone" id="saida_check12drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Helice com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check13drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check13drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando visualmente perceber que a Helice apresentar avarias(riscos, amassados, trincos e etc). OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check13drone" id="saida_check13drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Conector de carga não funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check14drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check14drone">
                                            <div class="popover-body">
                                              Selecione esta opção se Drone tiver um conector de carga nele e se colocar ele para carregar com um carregador(um que tenha certeza que esteja bom) e ele não mostrar que esta carregando. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check14drone" id="saida_check14drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece cartão de memória
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check15drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check15drone">
                                            <div class="popover-body">
                                              Selecione esta opção se colocar um cartão de memória no Drone e ele não reconhecer que foi inserido um cartão nele. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check15drone" id="saida_check15drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não grava video e fotos
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check16drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check16drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o Drone reconhece o cartão de memória mas não grava nele, porém grava no aplicativo. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check16drone" id="saida_check16drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Fora de foco
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check17drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check17drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o cliente relatar que a imagem gravada esta embaçada ou manchas. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check17drone" id="saida_check17drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Perca no alcance do Rádio
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check18drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check18drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando o cliente relatar que o drone perde sinal todo instante e bem próximo do rádio. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check18drone" id="saida_check18drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não Conecta no Wifi
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check19drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check19drone">
                                            <div class="popover-body">
                                              Selecione esta opção se o Drone não conectar na sua rede Wifi. IMPORTANTE: se o Drone não estiver ligando você também deverá marcar esta opção para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check19drone" id="saida_check19drone" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça/Shell com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check20drone" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check20drone">
                                            <div class="popover-body">
                                              Selecione esta opção quando visualmente perceber que a carcaça/Shell apresenta avarias(riscos, amassados, trincos e etc). OBS: as fotos que serão batidas deverão focar nessas avarias observadas.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check20drone" id="saida_check20drone" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_iMac2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1iMac">
                                            <div class="popover-body">
                                              (Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1iMac" id="saida_check1iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2iMac">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela desse notebook, se tiver alguma avaria(risco, trinco, mancha etc...) marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2iMac" id="saida_check2iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o notebook se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3iMac" id="saida_check3iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4iMac" id="saida_check4iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se qualquer tecla do teclado não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5iMac" id="saida_check5iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6iMac" id="saida_check6iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7iMac">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse, HD externo etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso também deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7iMac" id="saida_check7iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Bateria
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook a bateria, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8iMac" id="saida_check8iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook o carregador, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9iMac" id="saida_check9iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Não Reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check10iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check10iMac">
                                            <div class="popover-body">
                                              (Para saber se este reconhecendo o Wifi, se o Notebook estiver ligando deverá tentar conectar ele em sua rede Wifi e navegar com ele. Se o Notebook não estiver ligando deverá marcar como não esta funcionando o Wifi para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check10iMac" id="saida_check10iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Conecta mas não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check11iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check11iMac">
                                            <div class="popover-body">
                                              (Após conectar o Wifi abra um navegador como o Mozila ou Chrome e tente "navegar" na internet com ele com ele. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check11iMac" id="saida_check11iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth Não reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check12iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check12iMac">
                                            <div class="popover-body">
                                              (Se o notebook estiver ligando e ele tiver esta opção de Bluetooth ativer e veja se o celular reconhece e transfere algum arquivo, normalmente têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Procure por um interruptor no seu notebook ou por uma tecla no teclado. Frequentemente, a tecla é acessada com a ajuda da tecla Fn na cor azul.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check12iMac" id="saida_check12iMac" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check13iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check13iMac">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Notebook. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check13iMac" id="saida_check13iMac" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <div id="saida_Mac2" style="display: none;">
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não mostra Imagem
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check1iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check1iMac">
                                            <div class="popover-body">
                                              (Se ele tiver ligando veja se aparece alguma imagem na tela, caso não esteja ligando marque esta opção. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check1iMac2" id="saida_check1iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Tela com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check2iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check2iMac">
                                            <div class="popover-body">
                                              (Faça uma análise visual na tela desse notebook, se tiver alguma avaria(risco, trinco, mancha etc...) marque este opção e informe no campo de Observação da Ordem de Serviço na próxima tela. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check2iMac2" id="saida_check2iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Não reconhece o HD
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check3iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check3iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o notebook se ele aparecer uma tela preta com a informação "Reboot and Select proper Boot device or Insert Boot Media in selected device and press a key" ou muito parecida com essa significa que ele não esta reconhecendo o HD e também o sistema operacional não inicial. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check3iMac2" id="saida_check3iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Sistema Operacional não inicia
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check4iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check4iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se ao ligar o Windows ou o linux não iniciar após ele ligar. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check4iMac2" id="saida_check4iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Teclas com problema
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check5iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check5iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se qualquer tecla do teclado não estiver funcionando, em seguida relate quais teclas no campo de observação quando for abrir a ordem de serviço na próxima tela.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check5iMac2" id="saida_check5iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Áudio não Funciona
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check6iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check6iMac">
                                            <div class="popover-body">
                                              (Selecione esta opção se colocar uma caixa de som no computador e ele mesmo assim não sair nenhum áudio, lembrando que se o computador não estiver ligando esta opção também deverá ser selecionada para informar que não esta funcionando e não tenha reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check6iMac2" id="saida_check6iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Problema nas portas USB
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check7iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check7iMac">
                                            <div class="popover-body">
                                              (Se conectar um Pendrive, Mouse, HD externo etc... e mesmo assim o computador não aparecer nada avisando que foi conectado, indica que tem problema no USB. Nesse caso também deverá ser informado quantas portas não estão funcionando no campo de observação na próxima tela quando for inserir as informações na Ordem de serviços.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check7iMac2" id="saida_check7iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Bateria
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check8iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check8iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook a bateria, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check8iMac2" id="saida_check8iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Notebook sem Carregador
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check9iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check9iMac">
                                            <div class="popover-body">
                                              (Marque esta opção caso o cliente não tenha deixado junto com o notebook o carregador, com isso irá evitar reclamações posterior. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check9iMac2" id="saida_check9iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Não Reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check10iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check10iMac">
                                            <div class="popover-body">
                                              (Para saber se este reconhecendo o Wifi, se o Notebook estiver ligando deverá tentar conectar ele em sua rede Wifi e navegar com ele. Se o Notebook não estiver ligando deverá marcar como não esta funcionando o Wifi para se resguardar de reclamações posteriores. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check10iMac2" id="saida_check10iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Wifi Conecta mas não navega
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check11iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check11iMac">
                                            <div class="popover-body">
                                              (Após conectar o Wifi abra um navegador como o Mozila ou Chrome e tente "navegar" na internet com ele com ele. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check11iMac2" id="saida_check11iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Bluetooth Não reconhece
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check12iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check12iMac">
                                            <div class="popover-body">
                                              (Se o notebook estiver ligando e ele tiver esta opção de Bluetooth ativer e veja se o celular reconhece e transfere algum arquivo, normalmente têm um interruptor ou uma combinação de teclas para ligar e desligar o Bluetooth. Procure por um interruptor no seu notebook ou por uma tecla no teclado. Frequentemente, a tecla é acessada com a ajuda da tecla Fn na cor azul.  
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check12iMac2" id="saida_check12iMac2" value="1"> Não <br>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 5px 0;">
                                <label class="control-label"
                                       style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> * Carcaça com avarias
                                       <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_check13iMac" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_check13iMac">
                                            <div class="popover-body">
                                              Selecione esta opção se visualmente perceber marcas como riscos, amassados, trincos etc... na Notebook. OBS: as fotos que serão batidas deverão focar nessas avarias observadas. 
                                            </div>
                                          </div>
                                        </span>
                                </label>
                                <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;"><input
                                            type="checkbox" name="saida_check13iMac2" id="saida_check13iMac2" value="1"> Não <br>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                    </div>
                    <div class="widget-title">
                        <span class="icon">
                                        <i class="icon-picture"></i>
                                    </span>
                        <h5>
                            Imagens do equipamento
                        </h5>
                    </div>
                    <div id="imgPopupBack" class="modal">
                        <span class="imgPopupClose">×</span>
                        <img class="modal-content" id="imgPopupView">
                        <div id="imgPopupText"></div>
                    </div>
                    <br/>
                    <script type="text/javascript"
                            src="<?php echo base_url() . reisdev('url') ?>cam-api/functions.js"></script>
                    <style type="text/css">@import url("<?php echo base_url().reisdev('url')?>cam-api/styles.css");</style>
                    <style>
                        .pnPrev{
                            width: 100% !important;
                            max-width:2500px !important;
                            min-height: 0 !important;
                            height: 150px !important;
                            box-sizing:border-box;
                        }
                        
                        .pnPrev .pnI{
                            width: 16%;
                            margin: 0.33%;
                            max-width:100%; 
                            display: block;
                            float:left;
                            box-sizing:border-box;
                            position:relative;
                        }
                        
                        .pnPrev .pnI .btn-danger{
                            top: 0;
                            right:-5px;
                        }
                        
                        .pnPrev .pnImg{
                            width: 100%;
                            height: 110px;
                        }
                        
                        .pnPrev .pnImg img{
                            height: auto !important;
                        }
                    </style>
                    <div id="conteudo">
                        <div class="formulario">
                            <div class="dados">
                                <div class="campos">
                                    <div class="campo last" style="width:100%; text-align:center;">
                                        <h3> ENTREGA DO EQUIPAMENTO PARA A LOJA</h3>
                                        <div id="pnPrev" class="pnPrev">
                                            <div id="pnImg01" class="pnI">
                                                <a onclick="excludeImage(1)" id="delImg01" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg01" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg02" class="pnI">
                                                <a onclick="excludeImage(2)" id="delImg02" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg02" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg03" class="pnI">
                                                <a onclick="excludeImage(3)" id="delImg03" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg03" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg041" class="pnI">
                                                <a onclick="excludeImage(4)" id="delImg04" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg04" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg05" class="pnI">
                                                <a onclick="excludeImage(5)" id="delImg05" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg05" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg06" class="pnI">
                                                <a onclick="excludeImage(6)" id="delImg06" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg06" class="pnImg"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="campo last" style="width:100%; text-align:center;">
                                        <div class="panel" style="width:100%;">
                                            <div id="pnPreview" class="preview" height="240px" style="left: 50%; transform: translate(-50%,0);"></div>
                                            <img src="<?php echo base_url(); ?>assets/img/ex-flash.png" id="img-flash">
                                            <div id="pnCam" height="240px"></div>
                                        </div>
                                        <select id="tipoEntrega" class="cbox" name="tipoEntrega" size="1" style="width:50%; margin: 5px; float:none;">
                                            <option value="0">Entrega do equipamento para loja
                                            <option value="1">Entrega do equipamento para o cliente
                                        </select>
                                        <select id="cbQuality" class="cbox" name="pages" size="1" onChange="changedQuality(this);" style="width:50%; margin: 5px; float:none;">
                                            <option value="25">Muito Baixa(25%)
                                            <option value="50">Baixa(50%)
                                            <option value="75" selected>Normal(75%)
                                            <option value="90">Alta(90%)
                                            <option value="100">Muito Alta(100%)
                                        </select>
                                        <div>
                                            <button type="button" id="btCapture" class="button"> Capturar</button>
                                            <button type="button" id="btSave" hidden="true" class="button"> Manter</button>
                                            <button type="button" id="btDiscard" hidden="true" class="buttonred"> Descartar</button>
                                        </div>
                                    </div>
                                    <div class="campo last" style="width:100%; text-align:center;">
                                        <h3> ENTREGA DO EQUIPAMENTO PARA AO CLIENTE</h3>
                                        <div id="pnNext" class="pnPrev" style="width:100%;  ">
                                            <div id="pnImg07" class="pnI">
                                                <a onclick="excludeImage(7)" id="delImg07" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg07" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg08" class="pnI">
                                                <a onclick="excludeImage(8)" id="delImg08" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg08" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg09" class="pnI">
                                                <a onclick="excludeImage(9)" id="delImg09" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg09" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg010" class="pnI">
                                                <a onclick="excludeImage(10)" id="delImg10" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg10" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg11" class="pnI">
                                                <a onclick="excludeImage(11)" id="delImg11" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg11" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg12" class="pnI">
                                                <a onclick="excludeImage(12)" id="delImg12" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg12" class="pnImg"></div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  API WEBCAM END ----------------------------------------------------------------------------------------------------------------------------------------------- -->
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" name="save" value="" class="btn btn-primary"><i
                                            class="icon-ok icon-white"></i> Salvar
                                </button>
                                <button type="submit" name="save" value="os" class="btn btn-success" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>"><i
                                            class="icon-plus icon-white"></i> Salvar e Adicionar OS
                                </button>
                                <a href="<?php echo base_url() ?>index.php/equipamentos" id="btnAdicionar"
                                   class="btn" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<form action="https://www.consultaserialaparelho.com.br/public-web/homeSiga" method="post" id="form-consulta-imei"><input name="token" type="hidden" value="2015001"></form>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.js"></script>
<script type="text/javascript">
    $('[data-role="popover"]').popover();
    $(document).ready(function(){
        $("#equipamento").change(function(){
            nomeEquipamento = $(this).find('option:selected').attr('value');
            $("#tipo").val(nomeEquipamento);
        });

    }); 

    localStorage.removeItem('redemult_idEquipamento');
    localStorage.removeItem('redemult_equipamento');

    <?php if(@$iframe){ ?> 
    localStorage.setItem('redemult_idEquipamento', '<?php echo $result->idEquipamentos; ?>');

    localStorage.setItem('redemult_equipamento', "<?php echo $result->tipo; ?> | <?php echo $result->nome ?>, IMEI <?php echo $result->mei ?> - MODELO: <?php echo $result->modelo ?>");
    <?php } ?>
    
    function selectDefeito(tipo) {
        switch (tipo) {
            case "celular":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#check1celular').attr('checked', false);

                        if(!$('#check2celular').is(':checked'))
                            $('#check2celular').trigger('click');

                        if(!$('#check3celular').is(':checked'))
                            $('#check3celular').trigger('click');

                        if(!$('#check4celular').is(':checked'))
                            $('#check4celular').trigger('click');

                        if(!$('#check5celular').is(':checked'))
                            $('#check5celular').trigger('click');

                        if(!$('#check6celular').is(':checked'))
                            $('#check6celular').trigger('click');

                        if(!$('#check7celular').is(':checked'))
                            $('#check7celular').trigger('click');

                        if(!$('#check8celular').is(':checked'))
                            $('#check8celular').trigger('click');

                        if(!$('#check9celular').is(':checked'))
                            $('#check9celular').trigger('click');

                        if(!$('#check10celular').is(':checked'))
                            $('#check10celular').trigger('click')
                        ;
                        if(!$('#check11celular').is(':checked'))
                            $('#check11celular').trigger('click')
                        ;
                        if(!$('#check12celular').is(':checked'))
                            $('#check12celular').trigger('click')
                        ;
                        if(!$('#check13celular').is(':checked'))
                            $('#check13celular').trigger('click')
                        ;
                        if(!$('#check14celular').is(':checked'))
                            $('#check14celular').trigger('click')
                        ;
                        if(!$('#check15celular').is(':checked'))
                            $('#check15celular').trigger('click')
                        ;
                        if(!$('#check16celular').is(':checked'))
                            $('#check16celular').trigger('click')
                        ;
                        if(!$('#check17celular').is(':checked'))
                            $('#check17celular').trigger('click')
                        ;
                        if(!$('#check18celular').is(':checked'))
                            $('#check18celular').trigger('click')
                        ;
                        if(!$('#check19celular').is(':checked'))
                            $('#check19celular').trigger('click')
                        ;
                        if(!$('#check20celular').is(':checked'))
                            $('#check20celular').trigger('click')
                        ;
                        if(!$('#check21celular').is(':checked'))
                            $('#check21celular').trigger('click')
                        ;
                        $('#check22celular').attr('checked', false);
                        $('#check23celular').attr('checked', false);
                        $('#check24celular').attr('checked', false);
                    break;
                }
            break;
            case "computador":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#check1computador').attr('checked', false);

                        if(!$('#check2computador').is(':checked'))
                            $('#check2computador').trigger('click');

                        if(!$('#check3computador').is(':checked'))
                            $('#check3computador').trigger('click');

                        if(!$('#check4computador').is(':checked'))
                            $('#check4computador').trigger('click');

                        if(!$('#check5computador').is(':checked'))
                            $('#check5computador').trigger('click');

                        if(!$('#check6computador').is(':checked'))
                            $('#check6computador').trigger('click');

                        if(!$('#check7computador').is(':checked'))
                            $('#check7computador').trigger('click');

                        $('#check8computador').attr('checked', false);
                        $('#check9computador').attr('checked', false);
                    break;
                }
            break;
            case "iMac":
            case "iMac2":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#check1iMac').attr('checked', false);

                        if(!$('#check2iMac').is(':checked'))
                            $('#check2iMac').trigger('click');

                        if(!$('#check3iMac').is(':checked'))
                            $('#check3iMac').trigger('click');

                        if(!$('#check4iMac').is(':checked'))
                            $('#check4iMac').trigger('click');

                        if(!$('#check5iMac').is(':checked'))
                            $('#check5iMac').trigger('click');

                        if(!$('#check6iMac').is(':checked'))
                            $('#check6iMac').trigger('click');

                        if(!$('#check7iMac').is(':checked'))
                            $('#check7iMac').trigger('click');

                        $('#check8iMac').attr('checked', false);
                        $('#check9iMac').attr('checked', false);

                        if(!$('#check10iMac').is(':checked'))
                            $('#check10iMac').trigger('click');

                        if(!$('#check11iMac').is(':checked'))
                            $('#check11iMac').trigger('click');

                        if(!$('#check12iMac').is(':checked'))
                            $('#check12iMac').trigger('click');

                        $('#check13iMac').attr('checked', false);
                    break;
                }
            break;
            case "videogame":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":

                        if(!$('#check1videogame').is(':checked'))
                            $('#check1videogame').trigger('click');

                        if(!$('#check2videogame').is(':checked'))
                            $('#check2videogame').trigger('click');

                        if(!$('#check3videogame').is(':checked'))
                            $('#check3videogame').trigger('click');

                        if(!$('#check4videogame').is(':checked'))
                            $('#check4videogame').trigger('click');

                        if(!$('#check5videogame').is(':checked'))
                            $('#check5videogame').trigger('click');

                        if(!$('#check6videogame').is(':checked'))
                            $('#check6videogame').trigger('click');

                        $('#check7videogame').attr('checked', false);
                        $('#check8videogame').attr('checked', false);
                        $('#check9videogame').attr('checked', false);
                    break;
                }
            break;
            case "tv":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não Liga":

                        if(!$('#check1tv2').is(':checked'))
                            $('#check1tv2').trigger('click');

                        $('#check2tv2').attr('checked', false);

                        if(!$('#check3tv2').is(':checked'))
                            $('#check3tv2').trigger('click');

                        if(!$('#check4tv2').is(':checked'))
                            $('#check4tv2').trigger('click');

                        if(!$('#check5tv2').is(':checked'))
                            $('#check5tv2').trigger('click');

                        if(!$('#check6tv2').is(':checked'))
                            $('#check6tv2').trigger('click');

                        if(!$('#check7tv2').is(':checked'))
                            $('#check7tv2').trigger('click');

                        if(!$('#check8tv2').is(':checked'))
                            $('#check8tv2').trigger('click');

                        if(!$('#check9tv2').is(':checked'))
                            $('#check9tv2').trigger('click');

                        if(!$('#check10tv2').is(':checked'))
                            $('#check10tv2').trigger('click');

                        if(!$('#check11tv2').is(':checked'))
                            $('#check11tv2').trigger('click');

                        $('#check12tv2').attr('checked', false);
                    break;
                }
            break;
            case "drone":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não Liga":

                        $('#check1drone').attr('checked', false);

                        if(!$('#check2drone').is(':checked'))
                            $('#check2drone').trigger('click');

                        if(!$('#check3drone').is(':checked'))
                            $('#check3drone').trigger('click');

                        if(!$('#check4drone').is(':checked'))
                            $('#check4drone').trigger('click');

                        if(!$('#check5drone').is(':checked'))
                            $('#check5drone').trigger('click');

                        if(!$('#check6drone').is(':checked'))
                            $('#check6drone').trigger('click');

                        if(!$('#check7drone').is(':checked'))
                            $('#check7drone').trigger('click');

                        if(!$('#check8drone').is(':checked'))
                            $('#check8drone').trigger('click');

                        if(!$('#check9drone').is(':checked'))
                            $('#check9drone').trigger('click');

                        if(!$('#check10drone').is(':checked'))
                            $('#check10drone').trigger('click');

                        if(!$('#check11drone').is(':checked'))
                            $('#check11drone').trigger('click');

                        if(!$('#check12drone').is(':checked'))
                            $('#check12drone').trigger('click');

                        $('#check13drone').attr('checked', false);

                        if(!$('#check14drone').is(':checked'))
                            $('#check14drone').trigger('click');

                        if(!$('#check15drone').is(':checked'))
                            $('#check15drone').trigger('click');

                        if(!$('#check16drone').is(':checked'))
                            $('#check16drone').trigger('click');

                        if(!$('#check17drone').is(':checked'))
                            $('#check17drone').trigger('click');

                        if(!$('#check18drone').is(':checked'))
                            $('#check18drone').trigger('click');

                        if(!$('#check19drone').is(':checked'))
                            $('#check19drone').trigger('click');

                        $('#check20drone').attr('checked', false);
                    break;
                }
            break;
        }
    }

    function changeTipo(initializing = false) {
        $('#celular').css('display', 'none');
        
        $('#computador').css('display', 'none');
        $('#tablet').css('display', 'none');
        $('#videogame').css('display', 'none');
        $('#tv').css('display', 'none');
        $('#notebook').css('display', 'none');
        $('#MaciMac').css('display', 'none');
        $('#celular2').css('display', 'none');
        $('#drone').css('display', 'none');
        $('#drone2').css('display', 'none');
        
        $('#computador2').css('display', 'none');
        $('#tablet2').css('display', 'none');
        $('#videogame2').css('display', 'none');
        $('#tv2').css('display', 'none');
        $('#notebook2').css('display', 'none');
        $('#iMac2').css('display', 'none');
        $('#Mac2').css('display', 'none');
        var json;
        var i = 0;
        var defeito = '<?php echo $result->defeito; ?>';
        if (!defeito)
            initializing = false;
        else
            json = JSON.parse(defeito);
        switch ($("#equipamento").find('option:selected').attr('value')) {
            case "Celular":
            case "celular":
                $('#celular').css('display', 'block');
                $('#celular2').css('display', 'block');
                if (initializing) {
                    $('#celular').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1celular').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1celular').attr('value', parseInt(json[2]));
                    $('#check2celular').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2celular').attr('value', parseInt(json[3]));
                    $('#check3celular').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3celular').attr('value', parseInt(json[4]));
                    $('#check4celular').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4celular').attr('value', parseInt(json[5]));
                    $('#check5celular').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5celular').attr('value', parseInt(json[6]));
                    $('#check6celular').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6celular').attr('value', parseInt(json[7]));
                    $('#check7celular').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7celular').attr('value', parseInt(json[8]));
                    $('#check8celular').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8celular').attr('value', parseInt(json[9]));
                    $('#check9celular').attr('checked', parseInt(json[10]) == 1 ? true : false);
                    // $('#check9celular').attr('value', parseInt(json[10]));
                    $('#check10celular').attr('checked', parseInt(json[11]) == 1 ? true : false);
                    // $('#check10celular').attr('value', parseInt(json[11]));
                    $('#check11celular').attr('checked', parseInt(json[12]) == 1 ? true : false);
                    // $('#check11celular').attr('value', parseInt(json[12]));
                    $('#check12celular').attr('checked', parseInt(json[13]) == 1 ? true : false);
                    // $('#check12celular').attr('value', parseInt(json[13]));
                    $('#check13celular').attr('checked', parseInt(json[14]) == 1 ? true : false);
                    // $('#check13celular').attr('value', parseInt(json[14]));
                    $('#check14celular').attr('checked', parseInt(json[15]) == 1 ? true : false);
                    // $('#check14celular').attr('value', parseInt(json[15]));
                }
                break;
            case "Tablet":
            case "tablet":
                $('#tablet').css('display', 'block');
                $('#tablet2').css('display', 'block');
                if (initializing) {
                    $('#tablet').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1tablet').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1tablet').attr('value', parseInt(json[2]));
                    $('#check2tablet').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2tablet').attr('value', parseInt(json[3]));
                    $('#check3tablet').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3tablet').attr('value', parseInt(json[4]));
                    $('#check4tablet').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4tablet').attr('value', parseInt(json[5]));
                    $('#check5tablet').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5tablet').attr('value', parseInt(json[6]));
                    $('#check6tablet').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6tablet').attr('value', parseInt(json[7]));
                    $('#check7tablet').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7tablet').attr('value', parseInt(json[8]));
                    $('#check8tablet').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8tablet').attr('value', parseInt(json[9]));
                    $('#check9tablet').attr('checked', parseInt(json[10]) == 1 ? true : false);
                    // $('#check9tablet').attr('value', parseInt(json[10]));
                    $('#check10tablet').attr('checked', parseInt(json[11]) == 1 ? true : false);
                    // $('#check10tablet').attr('value', parseInt(json[11]));
                    $('#check11tablet').attr('checked', parseInt(json[12]) == 1 ? true : false);
                    // $('#check11tablet').attr('value', parseInt(json[12]));
                    $('#check12tablet').attr('checked', parseInt(json[13]) == 1 ? true : false);
                    // $('#check12tablet').attr('value', parseInt(json[13]));
                    $('#check13tablet').attr('checked', parseInt(json[14]) == 1 ? true : false);
                    // $('#check13tablet').attr('value', parseInt(json[14]));
                    $('#check14tablet').attr('checked', parseInt(json[15]) == 1 ? true : false);
                    // $('#check14tablet').attr('value', parseInt(json[15]));
                }
                break;
            case "Notebook":
            case "notebook":
                $('#notebook').css('display', 'block');
                $('#notebook2').css('display', 'block');
                if (initializing) {
                    $('#notebook').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1notebook').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1notebook').attr('value', parseInt(json[2]));
                    $('#check2notebook').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2notebook').attr('value', parseInt(json[3]));
                    $('#check3notebook').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3notebook').attr('value', parseInt(json[4]));
                    $('#check4notebook').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4notebook').attr('value', parseInt(json[5]));
                    $('#check5notebook').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5notebook').attr('value', parseInt(json[6]));
                    $('#check6notebook').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6notebook').attr('value', parseInt(json[7]));
                    $('#check7notebook').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7notebook').attr('value', parseInt(json[8]));
                    $('#check8notebook').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8notebook').attr('value', parseInt(json[9]));
                }
                break;
            case "Videogame":
            case "videogame":
                $('#videogame').css('display', 'block');
                $('#videogame2').css('display', 'block');
                if (initializing) {
                    $('#videogame').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1videogame').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1videogame').attr('value', parseInt(json[2]));
                    $('#check2videogame').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2videogame').attr('value', parseInt(json[3]));
                    $('#check3videogame').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3videogame').attr('value', parseInt(json[4]));
                    $('#check4videogame').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4videogame').attr('value', parseInt(json[5]));
                    $('#check5videogame').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5videogame').attr('value', parseInt(json[6]));
                    $('#check6videogame').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6videogame').attr('value', parseInt(json[7]));
                    $('#check7videogame').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7videogame').attr('value', parseInt(json[8]));
                    $('#check8videogame').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8videogame').attr('value', parseInt(json[9]));
                }
                break;
            case "TV":
            case "tv":
                $('#tv').css('display', 'block');
                $('#tv2').css('display', 'block');
                if (initializing) {
                    $('#tv').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1tv').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    $('#check2tv').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    $('#check3tv').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    $('#check4tv').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    $('#check5tv').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    $('#check6tv').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    $('#check7tv').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    $('#check8tv').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    $('#check9tv').attr('checked', parseInt(json[10]) == 1 ? true : false);
                    $('#check10tv').attr('checked', parseInt(json[11]) == 1 ? true : false);
                    $('#check11tv').attr('checked', parseInt(json[12]) == 1 ? true : false);
                    $('#check12tv').attr('checked', parseInt(json[13]) == 1 ? true : false);
                } 
                break;
            case "Computador":
            case "computador":
                $('#computador').css('display', 'block');
                $('#computador2').css('display', 'block');
                if (initializing) {
                    $('#computador').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1computador').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    $('#check2computador').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    $('#check3computador').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    $('#check4computador').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    $('#check5computador').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    $('#check6computador').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    $('#check7computador').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    $('#check8computador').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    $('#check9computador').attr('checked', parseInt(json[10]) == 1 ? true : false);
                    $('#check10computador').attr('checked', parseInt(json[11]) == 1 ? true : false);
                    $('#check11computador').attr('checked', parseInt(json[12]) == 1 ? true : false);
                    $('#check12computador').attr('checked', parseInt(json[13]) == 1 ? true : false);
                }
                    break;
            case "iMac":
            case "imac":
                $('#iMac').css('display', 'block');
                $('#iMac2').css('display', 'block');
                if (initializing) {
                    $('#iMac').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1iMac').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1iMac').attr('value', parseInt(json[2]));
                    $('#check2iMac').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2iMac').attr('value', parseInt(json[3]));
                    $('#check3iMac').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3iMac').attr('value', parseInt(json[4]));
                    $('#check4iMac').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4iMac').attr('value', parseInt(json[5]));
                    $('#check5iMac').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5iMac').attr('value', parseInt(json[6]));
                    $('#check6iMac').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6iMac').attr('value', parseInt(json[7]));
                    $('#check7iMac').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7iMac').attr('value', parseInt(json[8]));
                    $('#check8iMac').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8notebook').attr('value', parseInt(json[9]));
                }
                break;
                case "Mac":
                case "mac":
                $('#Mac').css('display', 'block');
                $('#Mac2').css('display', 'block');
                if (initializing) {
                    $('#Mac2').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1Mac2').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    // $('#check1Mac2').attr('value', parseInt(json[2]));
                    $('#check2Mac2').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    // $('#check2Mac2').attr('value', parseInt(json[3]));
                    $('#check3Mac2').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    // $('#check3Mac2').attr('value', parseInt(json[4]));
                    $('#check4Mac2').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    // $('#check4Mac2').attr('value', parseInt(json[5]));
                    $('#check5Mac2').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    // $('#check5Mac2').attr('value', parseInt(json[6]));
                    $('#check6Mac2').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    // $('#check6Mac2').attr('value', parseInt(json[7]));
                    $('#check7Mac2').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    // $('#check7Mac2').attr('value', parseInt(json[8]));
                    $('#check8Mac2').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    // $('#check8Mac2').attr('value', parseInt(json[9]));
                }
                break;
                case "Drone":
                case "drone":
                $('#drone').css('display', 'block');
                $('#drone2').css('display', 'block');
                if (initializing) {
                    $('#Mac2').val(json[0]);
                    $('#defoutros').val(json[1]);
                    $('#check1Mac2').attr('checked', parseInt(json[2]) == 1 ? true : false);
                    $('#check2Mac2').attr('checked', parseInt(json[3]) == 1 ? true : false);
                    $('#check3Mac2').attr('checked', parseInt(json[4]) == 1 ? true : false);
                    $('#check4Mac2').attr('checked', parseInt(json[5]) == 1 ? true : false);
                    $('#check5Mac2').attr('checked', parseInt(json[6]) == 1 ? true : false);
                    $('#check6Mac2').attr('checked', parseInt(json[7]) == 1 ? true : false);
                    $('#check7Mac2').attr('checked', parseInt(json[8]) == 1 ? true : false);
                    $('#check8Mac2').attr('checked', parseInt(json[9]) == 1 ? true : false);
                    $('#check9Mac2').attr('checked', parseInt(json[10]) == 1 ? true : false);
                    $('#check10Mac2').attr('checked', parseInt(json[11]) == 1 ? true : false);
                    $('#check11Mac2').attr('checked', parseInt(json[12]) == 1 ? true : false);
                    $('#check12Mac2').attr('checked', parseInt(json[13]) == 1 ? true : false);
                    $('#check13Mac2').attr('checked', parseInt(json[14]) == 1 ? true : false);
                    $('#check14Mac2').attr('checked', parseInt(json[15]) == 1 ? true : false);
                    $('#check15Mac2').attr('checked', parseInt(json[16]) == 1 ? true : false);
                    $('#check16Mac2').attr('checked', parseInt(json[17]) == 1 ? true : false);
                    $('#check17Mac2').attr('checked', parseInt(json[18]) == 1 ? true : false);
                    $('#check18Mac2').attr('checked', parseInt(json[19]) == 1 ? true : false);
                    $('#check19Mac2').attr('checked', parseInt(json[20]) == 1 ? true : false);
                    $('#check20Mac2').attr('checked', parseInt(json[21]) == 1 ? true : false);
                }
                break;
        }
    }
        function saida_selectDefeito(tipo) {
        switch (tipo) {
            case "celular":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#saida_check1celular').attr('checked', false);

                        if(!$('#saida_check2celular').is(':checked'))
                            $('#saida_check2celular').trigger('click');

                        if(!$('#saida_check3celular').is(':checked'))
                            $('#saida_check3celular').trigger('click');

                        if(!$('#saida_check4celular').is(':checked'))
                            $('#saida_check4celular').trigger('click');

                        if(!$('#saida_check5celular').is(':checked'))
                            $('#saida_check5celular').trigger('click');

                        if(!$('#saida_check6celular').is(':checked'))
                            $('#saida_check6celular').trigger('click');

                        if(!$('#saida_check7celular').is(':checked'))
                            $('#saida_check7celular').trigger('click');

                        if(!$('#saida_check8celular').is(':checked'))
                            $('#saida_check8celular').trigger('click');

                        if(!$('#saida_check9celular').is(':checked'))
                            $('#saida_check9celular').trigger('click');

                        if(!$('#saida_check10celular').is(':checked'))
                            $('#saida_check10celular').trigger('click')
                        ;
                        if(!$('#saida_check11celular').is(':checked'))
                            $('#saida_check11celular').trigger('click')
                        ;
                        if(!$('#saida_check12celular').is(':checked'))
                            $('#saida_check12celular').trigger('click')
                        ;
                        if(!$('#saida_check13celular').is(':checked'))
                            $('#saida_check13celular').trigger('click')
                        ;
                        if(!$('#saida_check14celular').is(':checked'))
                            $('#saida_check14celular').trigger('click')
                        ;
                        if(!$('#saida_check15celular').is(':checked'))
                            $('#saida_check15celular').trigger('click')
                        ;
                        if(!$('#saida_check16celular').is(':checked'))
                            $('#saida_check16celular').trigger('click')
                        ;
                        if(!$('#saida_check17celular').is(':checked'))
                            $('#saida_check17celular').trigger('click')
                        ;
                        if(!$('#saida_check18celular').is(':checked'))
                            $('#saida_check18celular').trigger('click')
                        ;
                        if(!$('#saida_check19celular').is(':checked'))
                            $('#saida_check19celular').trigger('click')
                        ;
                        if(!$('#saida_check20celular').is(':checked'))
                            $('#saida_check20celular').trigger('click')
                        ;
                        if(!$('#saida_check21celular').is(':checked'))
                            $('#saida_check21celular').trigger('click')
                        ;
                        $('#saida_check22celular').attr('checked', false);
                        $('#saida_check23celular').attr('checked', false);
                        $('#saida_check24celular').attr('checked', false);
                    break;
                }
            break;
            case "computador":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#saida_check1computador').attr('checked', false);

                        if(!$('#saida_check2computador').is(':checked'))
                            $('#saida_check2computador').trigger('click');

                        if(!$('#saida_check3computador').is(':checked'))
                            $('#saida_check3computador').trigger('click');

                        if(!$('#saida_check4computador').is(':checked'))
                            $('#saida_check4computador').trigger('click');

                        if(!$('#saida_check5computador').is(':checked'))
                            $('#saida_check5computador').trigger('click');

                        if(!$('#saida_check6computador').is(':checked'))
                            $('#saida_check6computador').trigger('click');

                        if(!$('#saida_check7computador').is(':checked'))
                            $('#saida_check7computador').trigger('click');

                        $('#saida_check8computador').attr('checked', false);
                        $('#saida_check9computador').attr('checked', false);
                    break;
                }
            break;
            case "iMac":
            case "iMac2":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":
                        $('#saida_check1iMac').attr('checked', false);

                        if(!$('#saida_check2iMac').is(':checked'))
                            $('#saida_check2iMac').trigger('click');

                        if(!$('#saida_check3iMac').is(':checked'))
                            $('#saida_check3iMac').trigger('click');

                        if(!$('#saida_check4iMac').is(':checked'))
                            $('#saida_check4iMac').trigger('click');

                        if(!$('#saida_check5iMac').is(':checked'))
                            $('#saida_check5iMac').trigger('click');

                        if(!$('#saida_check6iMac').is(':checked'))
                            $('#saida_check6iMac').trigger('click');

                        if(!$('#saida_check7iMac').is(':checked'))
                            $('#saida_check7iMac').trigger('click');

                        $('#saida_check8iMac').attr('checked', false);
                        $('#saida_check9iMac').attr('checked', false);

                        if(!$('#saida_check10iMac').is(':checked'))
                            $('#saida_check10iMac').trigger('click');

                        if(!$('#saida_check11iMac').is(':checked'))
                            $('#saida_check11iMac').trigger('click');

                        if(!$('#saida_check12iMac').is(':checked'))
                            $('#saida_check12iMac').trigger('click');

                        $('#saida_check13iMac').attr('checked', false);
                    break;
                }
            break;
            case "videogame":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não liga":

                        if(!$('#saida_check1videogame').is(':checked'))
                            $('#saida_check1videogame').trigger('click');

                        if(!$('#saida_check2videogame').is(':checked'))
                            $('#saida_check2videogame').trigger('click');

                        if(!$('#saida_check3videogame').is(':checked'))
                            $('#saida_check3videogame').trigger('click');

                        if(!$('#saida_check4videogame').is(':checked'))
                            $('#saida_check4videogame').trigger('click');

                        if(!$('#saida_check5videogame').is(':checked'))
                            $('#saida_check5videogame').trigger('click');

                        if(!$('#saida_check6videogame').is(':checked'))
                            $('#saida_check6videogame').trigger('click');

                        $('#saida_check7videogame').attr('checked', false);
                        $('#saida_check8videogame').attr('checked', false);
                        $('#saida_check9videogame').attr('checked', false);
                    break;
                }
            break;
            case "tv":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não Liga":

                        if(!$('#saida_check1tv2').is(':checked'))
                            $('#saida_check1tv2').trigger('click');

                        $('#saida_check2tv2').attr('checked', false);

                        if(!$('#saida_check3tv2').is(':checked'))
                            $('#saida_check3tv2').trigger('click');

                        if(!$('#saida_check4tv2').is(':checked'))
                            $('#saida_check4tv2').trigger('click');

                        if(!$('#saida_check5tv2').is(':checked'))
                            $('#saida_check5tv2').trigger('click');

                        if(!$('#saida_check6tv2').is(':checked'))
                            $('#saida_check6tv2').trigger('click');

                        if(!$('#saida_check7tv2').is(':checked'))
                            $('#saida_check7tv2').trigger('click');

                        if(!$('#saida_check8tv2').is(':checked'))
                            $('#saida_check8tv2').trigger('click');

                        if(!$('#saida_check9tv2').is(':checked'))
                            $('#saida_check9tv2').trigger('click');

                        if(!$('#saida_check10tv2').is(':checked'))
                            $('#saida_check10tv2').trigger('click');

                        if(!$('#saida_check11tv2').is(':checked'))
                            $('#saida_check11tv2').trigger('click');

                        $('#saida_check12tv2').attr('checked', false);
                    break;
                }
            break;
            case "drone":
                switch ($("#"+tipo).find('option:selected').attr('value')) {
                    case "Não Liga":

                        $('#saida_check1drone').attr('checked', false);

                        if(!$('#saida_check2drone').is(':checked'))
                            $('#saida_check2drone').trigger('click');

                        if(!$('#saida_check3drone').is(':checked'))
                            $('#saida_check3drone').trigger('click');

                        if(!$('#saida_check4drone').is(':checked'))
                            $('#saida_check4drone').trigger('click');

                        if(!$('#saida_check5drone').is(':checked'))
                            $('#saida_check5drone').trigger('click');

                        if(!$('#saida_check6drone').is(':checked'))
                            $('#saida_check6drone').trigger('click');

                        if(!$('#saida_check7drone').is(':checked'))
                            $('#saida_check7drone').trigger('click');

                        if(!$('#saida_check8drone').is(':checked'))
                            $('#saida_check8drone').trigger('click');

                        if(!$('#saida_check9drone').is(':checked'))
                            $('#saida_check9drone').trigger('click');

                        if(!$('#saida_check10drone').is(':checked'))
                            $('#saida_check10drone').trigger('click');

                        if(!$('#saida_check11drone').is(':checked'))
                            $('#saida_check11drone').trigger('click');

                        if(!$('#saida_check12drone').is(':checked'))
                            $('#saida_check12drone').trigger('click');

                        $('#saida_check13drone').attr('checked', false);

                        if(!$('#saida_check14drone').is(':checked'))
                            $('#saida_check14drone').trigger('click');

                        if(!$('#saida_check15drone').is(':checked'))
                            $('#saida_check15drone').trigger('click');

                        if(!$('#saida_check16drone').is(':checked'))
                            $('#saida_check16drone').trigger('click');

                        if(!$('#saida_check17drone').is(':checked'))
                            $('#saida_check17drone').trigger('click');

                        if(!$('#saida_check18drone').is(':checked'))
                            $('#saida_check18drone').trigger('click');

                        if(!$('#saida_check19drone').is(':checked'))
                            $('#saida_check19drone').trigger('click');

                        $('#saida_check20drone').attr('checked', false);
                    break;
                }
            break;
        }
    }

    function saida_changeTipo(initializing = false) {
        $('#saida_celular').css('display', 'none');
        
        $('#saida_computador').css('display', 'none');
        $('#saida_tablet').css('display', 'none');
        $('#saida_videogame').css('display', 'none');
        $('#saida_tv').css('display', 'none');
        $('#saida_notebook').css('display', 'none');
        $('#saida_MaciMac').css('display', 'none');
        $('#saida_celular2').css('display', 'none');
        $('#saida_drone').css('display', 'none');
        $('#saida_drone2').css('display', 'none');
        
        $('#saida_computador2').css('display', 'none');
        $('#saida_tablet2').css('display', 'none');
        $('#saida_videogame2').css('display', 'none');
        $('#saida_tv2').css('display', 'none');
        $('#saida_notebook2').css('display', 'none');
        $('#saida_iMac2').css('display', 'none');
        $('#saida_Mac2').css('display', 'none');
        var jsonSaida;
        var i = 0;
        var entrega = '<?php echo $result->entrega; ?>';
        if (!entrega)
            initializing = false;
        else
            jsonSaida = JSON.parse(entrega);
        switch ($("#equipamento").find('option:selected').attr('value')) {
            case "Celular":
            case "celular":
                $('#saida_celular').css('display', 'block');
                $('#saida_celular2').css('display', 'block');
                if (initializing) {
                    $('#saida_celular').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1celular').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1celular').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2celular').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2celular').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3celular').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3celular').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4celular').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4celular').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5celular').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5celular').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6celular').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6celular').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7celular').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7celular').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8celular').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8celular').attr('value', parseInt(jsonSaida[9]));
                    $('#saida_check9celular').attr('checked', parseInt(jsonSaida[10]) == 1 ? true : false);
                    // $('#saida_check9celular').attr('value', parseInt(jsonSaida[10]));
                    $('#saida_check10celular').attr('checked', parseInt(jsonSaida[11]) == 1 ? true : false);
                    // $('#saida_check10celular').attr('value', parseInt(jsonSaida[11]));
                    $('#saida_check11celular').attr('checked', parseInt(jsonSaida[12]) == 1 ? true : false);
                    // $('#saida_check11celular').attr('value', parseInt(jsonSaida[12]));
                    $('#saida_check12celular').attr('checked', parseInt(jsonSaida[13]) == 1 ? true : false);
                    // $('#saida_check12celular').attr('value', parseInt(jsonSaida[13]));
                    $('#saida_check13celular').attr('checked', parseInt(jsonSaida[14]) == 1 ? true : false);
                    // $('#saida_check13celular').attr('value', parseInt(jsonSaida[14]));
                    $('#saida_check14celular').attr('checked', parseInt(jsonSaida[15]) == 1 ? true : false);
                    // $('#saida_check14celular').attr('value', parseInt(jsonSaida[15]));
                }
                break;
            case "Tablet":
            case "tablet":
                $('#saida_tablet').css('display', 'block');
                $('#saida_tablet2').css('display', 'block');
                if (initializing) {
                    $('#saida_tablet').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1tablet').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1tablet').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2tablet').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2tablet').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3tablet').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3tablet').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4tablet').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4tablet').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5tablet').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5tablet').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6tablet').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6tablet').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7tablet').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7tablet').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8tablet').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8tablet').attr('value', parseInt(jsonSaida[9]));
                    $('#saida_check9tablet').attr('checked', parseInt(jsonSaida[10]) == 1 ? true : false);
                    // $('#saida_check9tablet').attr('value', parseInt(jsonSaida[10]));
                    $('#saida_check10tablet').attr('checked', parseInt(jsonSaida[11]) == 1 ? true : false);
                    // $('#saida_check10tablet').attr('value', parseInt(jsonSaida[11]));
                    $('#saida_check11tablet').attr('checked', parseInt(jsonSaida[12]) == 1 ? true : false);
                    // $('#saida_check11tablet').attr('value', parseInt(jsonSaida[12]));
                    $('#saida_check12tablet').attr('checked', parseInt(jsonSaida[13]) == 1 ? true : false);
                    // $('#saida_check12tablet').attr('value', parseInt(jsonSaida[13]));
                    $('#saida_check13tablet').attr('checked', parseInt(jsonSaida[14]) == 1 ? true : false);
                    // $('#saida_check13tablet').attr('value', parseInt(jsonSaida[14]));
                    $('#saida_check14tablet').attr('checked', parseInt(jsonSaida[15]) == 1 ? true : false);
                    // $('#saida_check14tablet').attr('value', parseInt(jsonSaida[15]));
                }
                break;
            case "Notebook":
            case "notebook":
                $('#saida_notebook').css('display', 'block');
                $('#saida_notebook2').css('display', 'block');
                if (initializing) {
                    $('#saida_notebook').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1notebook').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1notebook').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2notebook').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2notebook').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3notebook').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3notebook').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4notebook').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4notebook').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5notebook').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5notebook').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6notebook').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6notebook').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7notebook').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7notebook').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8notebook').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8notebook').attr('value', parseInt(jsonSaida[9]));
                }
                break;
            case "Videogame":
            case "videogame":
                $('#saida_videogame').css('display', 'block');
                $('#saida_videogame2').css('display', 'block');
                if (initializing) {
                    $('#saida_videogame').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1videogame').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1videogame').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2videogame').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2videogame').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3videogame').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3videogame').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4videogame').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4videogame').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5videogame').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5videogame').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6videogame').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6videogame').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7videogame').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7videogame').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8videogame').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8videogame').attr('value', parseInt(jsonSaida[9]));
                }
                break;
            case "TV":
            case "tv":
                $('#saida_tv').css('display', 'block');
                $('#saida_tv2').css('display', 'block');
                if (initializing) {
                    $('#saida_tv').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1tv').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    $('#saida_check2tv').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    $('#saida_check3tv').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    $('#saida_check4tv').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    $('#saida_check5tv').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    $('#saida_check6tv').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    $('#saida_check7tv').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    $('#saida_check8tv').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    $('#saida_check9tv').attr('checked', parseInt(jsonSaida[10]) == 1 ? true : false);
                    $('#saida_check10tv').attr('checked', parseInt(jsonSaida[11]) == 1 ? true : false);
                    $('#saida_check11tv').attr('checked', parseInt(jsonSaida[12]) == 1 ? true : false);
                    $('#saida_check12tv').attr('checked', parseInt(jsonSaida[13]) == 1 ? true : false);
                } 
                break;
            case "Computador":
            case "computador":
                $('#saida_computador').css('display', 'block');
                $('#saida_computador2').css('display', 'block');
                if (initializing) {
                    $('#saida_computador').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1computador').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    $('#saida_check2computador').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    $('#saida_check3computador').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    $('#saida_check4computador').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    $('#saida_check5computador').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    $('#saida_check6computador').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    $('#saida_check7computador').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    $('#saida_check8computador').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    $('#saida_check9computador').attr('checked', parseInt(jsonSaida[10]) == 1 ? true : false);
                    $('#saida_check10computador').attr('checked', parseInt(jsonSaida[11]) == 1 ? true : false);
                    $('#saida_check11computador').attr('checked', parseInt(jsonSaida[12]) == 1 ? true : false);
                    $('#saida_check12computador').attr('checked', parseInt(jsonSaida[13]) == 1 ? true : false);
                }
                    break;
            case "iMac":
            case "imac":
                $('#saida_iMac').css('display', 'block');
                $('#saida_iMac2').css('display', 'block');
                if (initializing) {
                    $('#saida_iMac').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1iMac').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1iMac').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2iMac').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2iMac').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3iMac').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3iMac').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4iMac').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4iMac').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5iMac').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5iMac').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6iMac').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6iMac').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7iMac').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7iMac').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8iMac').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8notebook').attr('value', parseInt(jsonSaida[9]));
                }
                break;
                case "Mac":
                case "mac":
                $('#saida_Mac').css('display', 'block');
                $('#saida_Mac2').css('display', 'block');
                if (initializing) {
                    $('#saida_Mac2').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1Mac2').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    // $('#saida_check1Mac2').attr('value', parseInt(jsonSaida[2]));
                    $('#saida_check2Mac2').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    // $('#saida_check2Mac2').attr('value', parseInt(jsonSaida[3]));
                    $('#saida_check3Mac2').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    // $('#saida_check3Mac2').attr('value', parseInt(jsonSaida[4]));
                    $('#saida_check4Mac2').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    // $('#saida_check4Mac2').attr('value', parseInt(jsonSaida[5]));
                    $('#saida_check5Mac2').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    // $('#saida_check5Mac2').attr('value', parseInt(jsonSaida[6]));
                    $('#saida_check6Mac2').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    // $('#saida_check6Mac2').attr('value', parseInt(jsonSaida[7]));
                    $('#saida_check7Mac2').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    // $('#saida_check7Mac2').attr('value', parseInt(jsonSaida[8]));
                    $('#saida_check8Mac2').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    // $('#saida_check8Mac2').attr('value', parseInt(jsonSaida[9]));
                }
                break;
                case "Drone":
                case "drone":
                $('#saida_drone').css('display', 'block');
                $('#saida_drone2').css('display', 'block');
                if (initializing) {
                    $('#saida_Mac2').val(jsonSaida[0]);
                    $('#saida_defoutros').val(jsonSaida[1]);
                    $('#saida_check1Mac2').attr('checked', parseInt(jsonSaida[2]) == 1 ? true : false);
                    $('#saida_check2Mac2').attr('checked', parseInt(jsonSaida[3]) == 1 ? true : false);
                    $('#saida_check3Mac2').attr('checked', parseInt(jsonSaida[4]) == 1 ? true : false);
                    $('#saida_check4Mac2').attr('checked', parseInt(jsonSaida[5]) == 1 ? true : false);
                    $('#saida_check5Mac2').attr('checked', parseInt(jsonSaida[6]) == 1 ? true : false);
                    $('#saida_check6Mac2').attr('checked', parseInt(jsonSaida[7]) == 1 ? true : false);
                    $('#saida_check7Mac2').attr('checked', parseInt(jsonSaida[8]) == 1 ? true : false);
                    $('#saida_check8Mac2').attr('checked', parseInt(jsonSaida[9]) == 1 ? true : false);
                    $('#saida_check9Mac2').attr('checked', parseInt(jsonSaida[10]) == 1 ? true : false);
                    $('#saida_check10Mac2').attr('checked', parseInt(jsonSaida[11]) == 1 ? true : false);
                    $('#saida_check11Mac2').attr('checked', parseInt(jsonSaida[12]) == 1 ? true : false);
                    $('#saida_check12Mac2').attr('checked', parseInt(jsonSaida[13]) == 1 ? true : false);
                    $('#saida_check13Mac2').attr('checked', parseInt(jsonSaida[14]) == 1 ? true : false);
                    $('#saida_check14Mac2').attr('checked', parseInt(jsonSaida[15]) == 1 ? true : false);
                    $('#saida_check15Mac2').attr('checked', parseInt(jsonSaida[16]) == 1 ? true : false);
                    $('#saida_check16Mac2').attr('checked', parseInt(jsonSaida[17]) == 1 ? true : false);
                    $('#saida_check17Mac2').attr('checked', parseInt(jsonSaida[18]) == 1 ? true : false);
                    $('#saida_check18Mac2').attr('checked', parseInt(jsonSaida[19]) == 1 ? true : false);
                    $('#saida_check19Mac2').attr('checked', parseInt(jsonSaida[20]) == 1 ? true : false);
                    $('#saida_check20Mac2').attr('checked', parseInt(jsonSaida[21]) == 1 ? true : false);
                }
                break;
        }
    }function changeDefeito(value, initializing = false) {
        if (initializing) {
            switch ($("#tipo").val()) {
                case "Celular":
                case "celular":
                    value = $('#celular').val();
                    break;
                case "Tablet":
                case "tablet":
                    value = $('#tablet').val();
                    break;
                case "Notebook":
                case "notebook":
                    value = $('#notebook').val();
                    break;
                case "Videogame":
                case "videogame":
                    value = $('#videogame').val();
                    break;
                case "TV":
                case "tv":
                    value = $('#tv').val();
                    break;
                case "Computador":
                case "computador":
                    value = $('#computador').val();
                        break;
                case "iMac":
                case "imac":
                    value = $('#iMac').val();
                        break;
                case "Mac":
                case "mac":
                    value = $('#Mac').val();
                        break;
                case "Drone":
                case "drone":
                    value = $('#Drone').val();
                        break;
            }
        }
        if (value.toLowerCase() == 'outros')
            $('#defoutros, #saida_defoutros').show();
        else {
            $('#defoutros, #saida_defoutros').hide();
            if (initializing) $('#defoutros, #saida_defoutros').val('');
        }
    }
    window.onload = function (event) {
        updatePreview();
        resize();
        changeTipo(true);
        saida_changeTipo(true);
        changeDefeito('', true);
    };
    window.onresize = function (event) {
        resize();
    };
    $(document).ready(function () {
        $("#checkMarcarTodos").click(function () {
            if ($(this).is(":checked")) {
                $("input[name^='check']").removeAttr("checked").change();
                $("input[name^='check']").trigger('click');
            } else {
                $("input[name^='check']").removeAttr("checked").change();
            }
        });
        $("#saida_checkMarcarTodos").click(function () {
            if ($(this).is(":checked")) {
                $("input[name^='saida_check']").removeAttr("checked").change();
                $("input[name^='saida_check']").trigger('click');
            } else {
                $("input[name^='saida_check']").removeAttr("checked").change();
            }
        });
        $('#celular, #tablet, #notebook, #videogame, #tv, #computador, #iMac, #Mac, #drone').change(function () {
            changeDefeito($(this).val());
        });
        $('#tipo').change(function () {
            changeTipo();
            saida_changeTipo();
        });
        $("#marca").keyup(function(e){
            if($(this).val() != ""){
                $(this).autocomplete({
                    source: "<?php echo base_url(); ?>index.php/equipamentos/autoCompleteMarca",
                    minLength: 1,
                    select: function( event, ui) {
                        if(ui.item.id != undefined){
                            $("#marcas_id").val(ui.item.id);
                        }else{
                            $("#marcas_id").val("");
                        }
                    }
                });
            }else{
                $("#marcas_id").val("");
            }
        });
        
        $("#cliente").keyup(function(e){
            if($(this).val() != ""){
                $(this).autocomplete({
                    source: "<?php echo base_url(); ?>index.php/vendas/autoCompleteCliente",
                    minLength: 1,
                    select: function( event, ui ) {
                        if(ui.item.id != undefined){
                            $("#clientes_id").val(ui.item.id);
                        }else{
                            $("#clientes_id").val("");
                        }
                    }
                });
            }else{
                $("#clientes_id").val("");
            }
        });
        
        
        
        $('#formEquipamento').validate
        ({
            rules: {
                equipamento: {required: true},
                marcas_id: {required: true},
                clientes_id: {required: true},
                tipo: {required: true},
                status: {required: true}
            },
            messages: {
                equipamento: {required: 'Campo Requerido.'},
                marcas_id: {required: 'Campo Requerido.'},
                clientes_id: {required: 'Campo Requerido.'},
                tipo: {required: 'Campo Requerido.'},
                status: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

    });
</script>