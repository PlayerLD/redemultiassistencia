<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Rápidos</h5>
            </div>
            <div class="widget-content">
                <ul class="site-stats">
                    <li><a target="_blank" href="<?php echo base_url()?>index.php/relatorios/despesasRapid"><i class="icon-list"></i>  <small>Todas as Despesas</small></a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>index.php/relatorios/receitasRapid"><i class="icon-list"></i>  <small>Todas as Receitas</small></a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>index.php/relatorios/despesasRapidP"><i class="icon-list"></i> <small>Contas a pagar</small></a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>index.php/relatorios/contasRapidR"><i class="icon-list"></i>   <small>Contas a receber</small></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="span8">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Customizáveis</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">
                    <div class="span12 alert alert-info">Deixe em branco caso não deseje utilizar o parâmetro.</div>
                    <form target="_blank" action="<?php echo base_url() ?>index.php/relatorios/despesasCustom" method="get">
                        <div class="span12 well">
                            <div class="span6">
                                <label for="">Data de vencimento de:</label>
                                <input type="date" name="dataInicial" class="span12" />
                            </div>
                            <div class="span6">
                                <label for="">até:</label>
                                <input type="date"  name="dataFinal" class="span12" />
                            </div>
                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Data de emissão de:</label>
                                <input type="date" name="dataInicial_emissao" class="span12" />
                            </div>
                            <div class="span6">
                                <label for="">até:</label>
                                <input type="date"  name="dataFinal_emissao" class="span12" />
                            </div>
                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Data de pagamento de:</label>
                                <input type="date" name="dataInicial_pagamento" class="span12" />
                            </div>
                            <div class="span6">
                                <label for="">até:</label>
                                <input type="date"  name="dataFinal_pagamento" class="span12" />
                            </div>
                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Cliente:</label>
                                <input type="text"  id="cliente" class="span12" />
                                <input type="hidden" name="cliente" id="clienteHide" />

                            </div>
                            <div class="span6">
                                <label for="">Fornecedor:</label>
                                <input type="text" id="fornecedor"   class="span12" />
                                <input type="hidden" name="fornecedor" id="fornecedorHide" />
                            </div>
                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Tipo:</label>
                                <select name="tipo" id="" class="span12">
                                    <option value="1">Despesa</option>
                                    <option value="2">Receita</option>
                                </select>
                            </div>

                        </div>
                        <div class="span12 well" style="margin-left: 0">
                            <div class="span6">
                                <label for="">Status:</label>
                                <select name="status" id="" class="span12">
                                    <option value=""></option>
                                    <option value="P">Pago</option>
                                    <option value="A">Agendado</option>
                                    <option value="AT">Atrasado</option>
                                </select>

                            </div>

                        </div>

                        <div class="span12" style="margin-left: 0; text-align: center">
                            <input type="reset" class="btn" value="Limpar" />
                            <button class="btn btn-inverse"><i class="icon-print icon-white"></i> Imprimir</button>
                        </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        
        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 2,
            select: function( event, ui ) {

                 $("#clienteHide").val(ui.item.id);


            }
      });

      $("#fornecedor").autocomplete({
            source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
            minLength: 2,
            select: function( event, ui ) {

                 $("#fornecedorHide").val(ui.item.id);


            }
      });
      
    });
</script>