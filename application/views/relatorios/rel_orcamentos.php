<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Rápidos</h5>
            </div>
            <div class="widget-content">
                <ul class="site-stats">
                    <li><a target="_blank" href="<?php echo base_url()?>index.php/relatorios/orcamentosRapid"><i class="icon-wrench"></i> <small>Orçamentos do mês</small></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="span8">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Customizáveis</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">
                    
                    <form target="_blank" action="<?php echo base_url() ?>index.php/relatorios/orcamentosCustom" method="get">
                        <div class="span12 well">
                    
                    <div class="span6">
                        <label for="">Vencimento de:</label>
                        <input type="date" name="dataInicial" class="span12" />
                    </div>
                    <div class="span6">
                        <label for="">até:</label>
                        <input type="date" name="dataFinal" class="span12" />
                    </div>

                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rOrcamento') && $this->session->userdata('id') == 4){ ?>
                    <div class="span12" style="margin-left:0;">
                        <label for="">Unidade</label>
                        <select name="franquia"class="span12 money">
                            <?php foreach($franquias as $f){
                                ?>
                                <option value="<?php echo $f->idFranquias?>"><?php echo $f->nome?></option>
                            <?php }?>
                        </select>
                    </div>
                    <?php } ?>
                    
                </div>
                        <div class="span12" style="margin-left: 0; text-align: center">
                            <input type="reset" class="btn" value="Limpar" />
                            <button class="btn btn-inverse"><i class="icon-print icon-white"></i> Visualizar</button>
                        </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        


    });
</script>