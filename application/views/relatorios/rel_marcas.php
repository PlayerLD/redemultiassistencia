<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Rápidos</h5>
            </div>
            <div class="widget-content">
                <ul class="site-stats">
                    <li><a href="<?php echo base_url()?>index.php/relatorios/marcasRapid" target="_blank"><i class="icon-user"></i> <small>Todas as Marcas</small></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="span8">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Customizáveis</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">
                    <form target="_blank" action="<?php echo base_url()?>index.php/relatorios/marcasCustom" method="get">
                        <div class="span6">
                            <label for="">Cadastrado de:</label>
                            <input type="date" name="dataInicial" class="span12" />
                        </div>
                        <div class="span6">
                            <label for="">até:</label>
                            <input type="date" name="dataFinal" class="span12" />
                        </div>
                        <?php if($this->session->userdata('id') == 4){ ?>
                        <div class="span12" style="margin-left:0;">
                            <label for="">Unidade</label>
                            <select name="franquia"class="span12 money">
                                <?php foreach($franquias as $f){
                                    ?>
                                    <option value="<?php echo $f->idFranquias?>"><?php echo $f->nome?></option>
                                <? }?>
                            </select>
                        </div>
                        <?php }else{ ?>
                            <input type="hidden" name="franquia" value="<?php echo $this->session->userdata('id') ?>">
                        <?php } ?>
                        <div class="span12" style="margin-left: 0; text-align: center">
                            <input type="reset" class="btn" value="Limpar" />
                            <button class="btn btn-inverse"><i class="icon-print icon-white"></i> Imprimir</button>
                        </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>