  <head>
    <title>SISTEMAOS</title>
    <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

  <body style="background-color: transparent">



      <div class="container-fluid">

          <div class="row-fluid">
              <div class="span12">

                  <div class="widget-box">
                      <div class="widget-title">
                          <h4 style="text-align: center">Serviços</h4>
                      </div>
                      <div class="widget-content nopadding">

                  <table class="table table-bordered">
                      <thead>
                          <tr align="center">
                          	  <th style="font-size: 1em; padding: 5px; width:8%;" >POS</th>
                              <th style="font-size: 1em; padding: 5px; width:25%;">Nome</th>
                              <th style="font-size: 1em; padding: 5px; width:30%;">Descrição</th>
                              <th style="font-size: 1em; padding: 5px; width:25%;">Unidade</th>
                              <th style="font-size: 1em; padding: 5px; width:12%;">Preço</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
						  $idFranquia = 0;
						  $totalFranquia = 0;
						  $totalServicos = 0;
                          foreach ($servicos as $s) {
 							  $novaIdFranquia = $s->idFranquia;
							  
							  if($idFranquia != 0 && $novaIdFranquia != $idFranquia){
								echo '<tr>';
                              	echo '<td colspan="6" align="center"><b>TOTAL DE SERVIÇOS DA FRANQUIA '.$idFranquia.': '.$totalFranquia.' serviços</b></td>';
                              	echo '</tr>';
								$totalFranquia = 1;
							  }else{
							  	$totalFranquia++;
							  }
                              echo '<tr>';
							  echo '<td>' . $totalFranquia . '</td>';
                              echo '<td>' . $s->nomeServico. '</td>';
                              echo '<td>' . $s->descricaoServico . '</td>';
							  echo '<td>' . $s->idFranquias ." - ". str_replace("Rede Multi Assistencia - ","Rede - ",$s->nome) . '</td>';
                              echo '<td>R$ ' . number_format($s->precoServico,2,",","."). '</td>';
                              echo '</tr>';
							  
							  $idFranquia = $novaIdFranquia;
							  $totalServicos++;
                          }
						  
						  echo '<tr>';
						  echo '<td colspan="6" align="center"><b>TOTAL DE SERVIÇOS DA FRANQUIA '.$idFranquia.': '.$totalFranquia.' serviços</b></td>';
					      echo '</tr>';
						  
						  echo '<tr>';
						  echo '<td colspan="6" align="center"><b>TOTAL DE SERVIÇOS : '.$totalServicos.' serviços</b></td>';
						  echo '</tr>';
                          ?>
                      </tbody>
                  </table>

                  </div>

              </div>
                  <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>

          </div>



      </div>
</div>




            <!-- Arquivos js-->

            <script src="<?php echo base_url();?>js/excanvas.min.js"></script>
            <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
            <script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
            <script src="<?php echo base_url();?>js/sosmc.js"></script>
            <script src="<?php echo base_url();?>js/dashboard.js"></script>
  </body>
</html>







