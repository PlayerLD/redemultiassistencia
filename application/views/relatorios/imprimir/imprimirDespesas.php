  <head>
    <title>SISTEMAOS</title>
    <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

  <body style="background-color: transparent">



      <div class="container-fluid">

          <div class="row-fluid">
              <div class="span12">

                  <div class="widget-box">
                      <div class="widget-title">
                          <h4 style="text-align: center">Produtos</h4>
                      </div>
                      <div class="widget-content nopadding">

                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th style="font-size: 0.9em; padding: 5px;">Código</th>
                              <th style="font-size: 0.9em; padding: 5px;">Nome</th>
                              <th style="font-size: 0.9em; padding: 5px;">Fornecedor</th>
                              <th style="font-size: 0.9em; padding: 5px;">Cliente</th>
                              <th style="font-size: 0.9em; padding: 5px;">Valor a pagar</th>
                              <th style="font-size: 0.9em; padding: 5px;">Valor pago</th>
                              <th style="font-size: 0.9em; padding: 5px;">Juros</th>
                              <th style="font-size: 0.9em; padding: 5px;">Desconto</th>
                              <th style="font-size: 0.9em; padding: 5px;">Data de vencimento</th>
                              <th style="font-size: 0.9em; padding: 5px;">Data de emissão</th>
                              <th style="font-size: 0.9em; padding: 5px;">Data de pagamento</th>
                              <th style="font-size: 0.9em; padding: 5px;">Observação</th>
                              <th style="font-size: 0.9em; padding: 5px;">Status</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php $status = array('P' => 'Pago', 'A' => 'Agendado', 'AT' => 'Atrasado') ?>
                          <?php
                          foreach ($despesas as $p) {
                              echo '<tr>';
                              echo '<td>' . $p->codigo. '</td>';
                              echo '<td>' . $p->nome. '</td>';
                              echo '<td>' . $p->fornecedor. '</td>';
                              echo '<td>' . $p->cliente. '</td>';
                              echo '<td>' . number_format($p->valor, 2, ',', '.'). '</td>';
                              echo '<td>' . number_format($p->valor_pago, 2, ',', '.'). '</td>';
                              echo '<td>' . $p->juros. '</td>';
                              echo '<td>' . $p->desconto. '</td>';
                              echo '<td>' . date('d/m/Y', strtotime($p->data_vencimento)). '</td>';
                              echo '<td>' . date('d/m/Y', strtotime($p->data_emissao)). '</td>';
                              echo '<td>' . date('d/m/Y', strtotime($p->data_pagamento)). '</td>';
                              echo '<td>' . $p->obs. '</td>';
                              echo '<td>' . @$status[$p->status]. '</td>';
                              echo '</tr>';
                          }
                          ?>
                      </tbody>
                  </table>

                  </div>

              </div>
                  <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>

          </div>



      </div>
</div>




            <!-- Arquivos js-->

            <script src="<?php echo base_url();?>js/excanvas.min.js"></script>
            <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
            <script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
            <script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
            <script src="<?php echo base_url();?>js/sosmc.js"></script>
            <script src="<?php echo base_url();?>js/dashboard.js"></script>
  </body>
</html>

