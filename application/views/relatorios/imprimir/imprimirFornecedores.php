<head>
	<title>SISTEMAOS</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    
	<script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="background-color: transparent">
	<div class="container-fluid">
    	<div class="row-fluid">
        	<div class="span12">
            	<div class="widget-box">
                	<div class="widget-title">
                    	<h4 style="text-align: center">Fornecedores</h4>
                   	</div>
                    <div class="widget-content nopadding">
                  		<table class="table table-bordered">
                      		<thead>
                          		<tr>
                          	  		<th style="font-size: 1.0em; padding: 5px; width:5%;">POS</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:25%;">Nome</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:25%;">Unidade</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:12%;">Telefone</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:19%;">Email</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:12%;">Cadastro</th>
                          		</tr>
                      		</thead>
                      		<tbody>
								<?php $idFranquia = 0;
                                $totalFranquia = 0;
								$nomeFranquia = "";
                                $totalFornecedores = 0;
                              
                                foreach ($fornecedores as $f) {
                                    $novaIdFranquia = $f->idFranquia;
                                  
                                    if($idFranquia != 0 && $novaIdFranquia != $idFranquia){?>
                                        <tr>
                                            <td colspan="6" align="center"><b>TOTAL DE FORNECEDORES DA FRANQUIA <?php echo $idFranquia ." - ".$nomeFranquia ;?>: <?php echo $totalFranquia;?> fornecedores</b></td>
                                        </tr>
                                    <?php $totalFranquia = 1;
                                    }else{
                                        $totalFranquia++;
                                    }
                                  
                                    $dataCadastro = date('d/m/Y', strtotime($f->dataCadastro));?>
                                    <tr>
                                        <td><?php echo $totalFranquia;?></td>
                                        <td><?php echo $f->nomeFornecedor;?></td>
                                        <td><?php echo $f->idFranquia." - ".str_replace("Rede Multi Assistencia -","Rede ",$f->nome);?></td>
                                        <td><?php echo $f->telefone;?></td>
                                        <td><?php echo $f->email;?></td>
                                        <td><?php echo $dataCadastro;?></td>
                                    </tr>
                                    <?php $idFranquia = $novaIdFranquia;
									$nomeFranquia = mb_strtoupper(str_replace("Rede Multi Assistencia -","Rede ",$f->nome),"UTF-8");
                                    $totalFornecedores++;
                                }?>
                                <tr>
                                    <td colspan="6" align="center"><b>TOTAL DE FORNECEDORES DA FRANQUIA <?php echo $idFranquia. " - ". mb_strtoupper(str_replace("Rede Multi Assistencia -","Rede ",$f->nome),"UTF-8");?>: <?php echo $totalFranquia;?> fornecedores</b></td>';
                                </tr>
                              
                                <tr>
                                	<td colspan="6" align="center"><b>TOTAL DE FORNECEDORES : <?php echo $totalFornecedores?> fornecedores</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>   
                </div>
                
                <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>
            </div>
        </div>
    </div>
    <!-- Arquivos js-->
    
    <script src="<?php echo base_url();?>js/excanvas.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
    <script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
    <script src="<?php echo base_url();?>js/sosmc.js"></script>
    <script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>







