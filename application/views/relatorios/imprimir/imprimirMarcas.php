<head>
	<title>SISTEMAOS</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    
	<script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="background-color: transparent">
	<div class="container-fluid">
    	<div class="row-fluid">
        	<div class="span12">
            	<div class="widget-box">
                	<div class="widget-title">
                    	<h4 style="text-align: center">Marcas</h4>
                   	</div>
                    <div class="widget-content nopadding">
                  		<table class="table table-bordered">
                      		<thead>
                          		<tr>
                          	  		<th style="font-size: 1.0em; padding: 5px; width:15%;">POS</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:35%;">Nome</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:35%;">Unidade</th>
                              		<th style="font-size: 1.0em; padding: 5px; width:15%;">Quantidade</th>
                          		</tr>
                      		</thead>
                      		<tbody>
								<?php $idFranquia = 0;
                                $totalFranquia = 0;
                                $totalMarcas = 0;
                              	$nomeFranquia = "";
								$i = 0;
								
                                foreach ($marcas as $m) {
                                    $novaIdFranquia = $m->idFranquias;
									
                                    if($idFranquia != 0 && $novaIdFranquia != $idFranquia){?>
                                        <tr>
                                            <td colspan="6" align="center"><b>TOTAL DE MARCAS DA FRANQUIA <?php echo $idFranquia." - ".mb_strtoupper($nomeFranquia,'UTF-8');?>: <?php echo $totalFranquia;?> marcas</b></td>
                                        </tr>
                                    <?php $totalFranquia = $m->quantidade;
										$i = 1;
                                    }else{
                                        $totalFranquia += $m->quantidade;
										$i++;
                                    }
									
									$nomeFranquia = str_replace("Rede Multi Assistencia","Rede",$m->nome);
									$nomeFranquia = str_replace("Rede Multi Assistência","Rede",$m->nome);
									?>
                                    <tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $m->nomeMarca;?></td>
                                        <td><?php echo $m->idFranquias." - ".$nomeFranquia?></td>
                                        <td><?php echo $m->quantidade;?></td>
                                    </tr>
                                    <?php $idFranquia = $novaIdFranquia;
                                    $totalMarcas += $m->quantidade;
                                }?>
                                <tr>
                                    <td colspan="6" align="center"><b>TOTAL DE MARCAS DA FRANQUIA <?php echo $idFranquia." - ".mb_strtoupper($nomeFranquia,'UTF-8');?>: <?php echo $totalFranquia;?> marcas</b></td>';
                                </tr>
                              
                                <tr>
                                	<td colspan="6" align="center"><b>TOTAL DE MARCAS : <?php echo $totalMarcas?> marcas</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>   
                </div>
                
                <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>
            </div>
        </div>
    </div>
    <!-- Arquivos js-->
    
    <script src="<?php echo base_url();?>js/excanvas.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
    <script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
    <script src="<?php echo base_url();?>js/sosmc.js"></script>
    <script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>







