<?php $permissoes = unserialize($result->permissoes);?>

<div class="span12" style="margin-left: 0">

    <form action="<?php echo base_url();?>index.php/permissoes/editar" id="formPermissao" method="post">
 
    <div class="span12" style="margin-left: 0">

        <div class="widget-box">
            <div class="widget-title"> 
                <span class="icon">
                    <i class="icon-lock"></i>
                </span>

                <h5>Editar Permissão</h5>
            </div> 

            <div class="widget-content">
                <div class="span4">
                    <label>Nome da Permissão</label>

                    <input name="nome" type="text" id="nome" class="span12" value="<?php echo $result->nome; ?>" />
                    <input type="hidden" name="idPermissao" value="<?php echo $result->idPermissao; ?>">
                </div>

                <div class="span3">
                    <label>Situação</label>

                    <select name="situacao" id="situacao" class="span12">
                        <?php if($result->situacao == 1){$sim = 'selected'; $nao ='';}else{$sim = ''; $nao ='selected';}?>
                        <option value="1" <?php echo $sim;?>>Ativo</option>
                        <option value="0" <?php echo $nao;?>>Inativo</option>
                    </select>
                </div>

                <div class="span4">
                    <br/>
                    <label>
                        <input name="" type="checkbox" value="1" id="marcarTodos" />
                        <span class="lbl"> Marcar Todos</span>
                    </label>
                    <br/>
                </div>

                <div class="control-group">
                    <label for="documento" class="control-label"></label>

                    <div class="controls">
                        <table class="table table-bordered">
                            <tbody>

                                <!-- CLIENTE -->
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vCliente'])){ if($permissoes['vCliente'] == '1'){echo 'checked';}}?> name="vCliente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Cliente</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aCliente'])){ if($permissoes['aCliente'] == '1'){echo 'checked';}}?> name="aCliente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Cliente</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eCliente'])){ if($permissoes['eCliente'] == '1'){echo 'checked';}}?> name="eCliente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Cliente</span>
                                        </label>
                                    </td>
                                    <td colspan="5">
                                        <label>
                                            <input <?php if(isset($permissoes['dCliente'])){ if($permissoes['dCliente'] == '1'){echo 'checked';}}?> name="dCliente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Cliente</span>
                                        </label>
                                    </td>
                                </tr>
                                <!-- FIM CLIENTE -->

                                <tr>
                                    <td colspan="8"></td>
                                </tr>

                                <!-- ATENDENTE -->
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vAtendente'])){ if($permissoes['vAtendente'] == '1'){echo 'checked';}}?> name="vAtendente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Atendente</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aAtendente'])){ if($permissoes['aAtendente'] == '1'){echo 'checked';}}?> name="aAtendente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Atendente</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eAtendente'])){ if($permissoes['eAtendente'] == '1'){echo 'checked';}}?> name="eAtendente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Atendente</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dAtendente'])){ if($permissoes['dAtendente'] == '1'){echo 'checked';}}?> name="dAtendente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Atendente</span>
                                        </label>
                                    </td>
                                    <td colspan="4">
                                        <label>
                                            <input <?php if(isset($permissoes['cAtendente'])){ if($permissoes['cAtendente'] == '1'){echo 'checked';}}?>  name="cAtendente" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Configurar Atendente</span>
                                        </label>
                                    </td>
                                </tr>
                                <!-- FIM ATENDENTE -->

                                <tr>
                                    <td colspan="8"></td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vFeedback'])){ if($permissoes['vFeedback'] == '1'){echo 'checked';}}?> name="vFeedback" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Feedback</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aFeedback'])){ if($permissoes['aFeedback'] == '1'){echo 'checked';}}?> name="aFeedback" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Feedback</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eFeedback'])){ if($permissoes['eFeedback'] == '1'){echo 'checked';}}?> name="eFeedback" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Feedback</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dFeedback'])){ if($permissoes['dFeedback'] == '1'){echo 'checked';}}?> name="dFeedback" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Feedback</span>
                                        </label>
                                    </td>
                                    <td colspan="4">
                                        <label>
                                            <input <?php if(isset($permissoes['detFeedback'])){ if($permissoes['detFeedback'] == '1'){echo 'checked';}}?> name="detFeedback" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Feedback Detalhado</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vFornecedor'])){ if($permissoes['vFornecedor'] == '1'){echo 'checked';}}?> name="vFornecedor" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Fornecedor</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aFornecedor'])){ if($permissoes['aFornecedor'] == '1'){echo 'checked';}}?> name="aFornecedor" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Fornecedor</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eFornecedor'])){ if($permissoes['eFornecedor'] == '1'){echo 'checked';}}?> name="eFornecedor" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Fornecedor</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dFornecedor'])){ if($permissoes['dFornecedor'] == '1'){echo 'checked';}}?> name="dFornecedor" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Fornecedor</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vProduto'])){ if($permissoes['vProduto'] == '1'){echo 'checked';}}?> name="vProduto" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Produto</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aProduto'])){ if($permissoes['aProduto'] == '1'){echo 'checked';}}?> name="aProduto" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Produto</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eProduto'])){ if($permissoes['eProduto'] == '1'){echo 'checked';}}?> name="eProduto" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Produto</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dProduto'])){ if($permissoes['dProduto'] == '1'){echo 'checked';}}?> name="dProduto" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Produto</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vDespesa'])){ if($permissoes['vDespesa'] == '1'){echo 'checked';}}?> name="vDespesa" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Despesa</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aDespesa'])){ if($permissoes['aDespesa'] == '1'){echo 'checked';}}?> name="aDespesa" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Despesa</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eDespesa'])){ if($permissoes['eDespesa'] == '1'){echo 'checked';}}?> name="eDespesa" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Despesa</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dDespesa'])){ if($permissoes['dDespesa'] == '1'){echo 'checked';}}?> name="dDespesa" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Despesa</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vServico'])){ if($permissoes['vServico'] == '1'){echo 'checked';}}?> name="vServico" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Serviço</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aServico'])){ if($permissoes['aServico'] == '1'){echo 'checked';}}?> name="aServico" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Serviço</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eServico'])){ if($permissoes['eServico'] == '1'){echo 'checked';}}?> name="eServico" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Serviço</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dServico'])){ if($permissoes['dServico'] == '1'){echo 'checked';}}?> name="dServico" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Serviço</span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vEquipamento'])){ if($permissoes['vEquipamento'] == '1'){echo 'checked';}}?> name="vEquipamento" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Equipamento</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aEquipamento'])){ if($permissoes['aEquipamento'] == '1'){echo 'checked';}}?> name="aEquipamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Equipamento</span>

                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eEquipamento'])){ if($permissoes['eEquipamento'] == '1'){echo 'checked';}}?> name="eEquipamento" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Equipamento</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dEquipamento'])){ if($permissoes['dEquipamento'] == '1'){echo 'checked';}}?> name="dEquipamento" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Equipamento</span>

                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['vMarca'])){ if($permissoes['vMarca'] == '1'){echo 'checked';}}?> name="vMarca" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Visualizar Marca</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['aMarca'])){ if($permissoes['aMarca'] == '1'){echo 'checked';}}?> name="aMarca" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Adicionar Marca</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['eMarca'])){ if($permissoes['eMarca'] == '1'){echo 'checked';}}?> name="eMarca" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Editar Marca</span>
                                        </label>
                                    </td>
                                    <td>
                                        <label>
                                            <input <?php if(isset($permissoes['dMarca'])){ if($permissoes['dMarca'] == '1'){echo 'checked';}}?> name="dMarca" class="marcar" type="checkbox" value="1" />
                                            <span class="lbl"> Excluir Marca</span>
                                        </label>
                                    </td>
                                </tr>











                                

                                <tr><td colspan="8"></td></tr>

                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vOs'])){ if($permissoes['vOs'] == '1'){echo 'checked';}}?> name="vOs" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar OS</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aOs'])){ if($permissoes['aOs'] == '1'){echo 'checked';}}?> name="aOs" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar OS</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eOs'])){ if($permissoes['eOs'] == '1'){echo 'checked';}}?> name="eOs" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar OS</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dOs'])){ if($permissoes['dOs'] == '1'){echo 'checked';}}?> name="dOs" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir OS</span>

                                        </label>

                                    </td>

                                 

                                </tr>

                                <tr><td colspan="8"></td></tr>

                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vOrcamento'])){ if($permissoes['vOrcamento'] == '1'){echo 'checked';}}?> name="vOrcamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Orçamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aOrcamento'])){ if($permissoes['aOrcamento'] == '1'){echo 'checked';}}?> name="aOrcamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Orçamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eOrcamento'])){ if($permissoes['eOrcamento'] == '1'){echo 'checked';}}?> name="eOrcamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Orçamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dOrcamento'])){ if($permissoes['dOrcamento'] == '1'){echo 'checked';}}?> name="dOrcamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Orçamento</span>

                                        </label>

                                    </td>

                                 

                                </tr>

                                <tr><td colspan="8"></td></tr>

                                

                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vVenda'])){ if($permissoes['vVenda'] == '1'){echo 'checked';}}?> name="vVenda" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Venda</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aVenda'])){ if($permissoes['aVenda'] == '1'){echo 'checked';}}?> name="aVenda" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Venda</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eVenda'])){ if($permissoes['eVenda'] == '1'){echo 'checked';}}?> name="eVenda" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Venda</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dVenda'])){ if($permissoes['dVenda'] == '1'){echo 'checked';}}?> name="dVenda" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Venda</span>

                                        </label>

                                    </td>

                                 

                                </tr>

                                

                                <tr><td colspan="8"></td></tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vArquivo'])){ if($permissoes['vArquivo'] == '1'){echo 'checked';}}?> name="vArquivo" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Arquivo</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aArquivo'])){ if($permissoes['aArquivo'] == '1'){echo 'checked';}}?> name="aArquivo" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Arquivo</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eArquivo'])){ if($permissoes['eArquivo'] == '1'){echo 'checked';}}?> name="eArquivo" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Arquivo</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dArquivo'])){ if($permissoes['dArquivo'] == '1'){echo 'checked';}}?> name="dArquivo" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Arquivo</span>

                                        </label>

                                    </td>

                                 

                                </tr>

                                <tr><td colspan="8"></td></tr>

                                <tr>
                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vXML'])){ if($permissoes['vXML'] == '1'){echo 'checked';}}?> name="vXML" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar XML</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aXML'])){ if($permissoes['aXML'] == '1'){echo 'checked';}}?> name="aXML" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar XML</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eXML'])){ if($permissoes['eXML'] == '1'){echo 'checked';}}?> name="eXML" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar XML</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dXML'])){ if($permissoes['dXML'] == '1'){echo 'checked';}}?> name="dXML" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir XML</span>

                                        </label>

                                    </td>

                                 

                                </tr>

                                

                                <tr><td colspan="8"></td></tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vLancamento'])){ if($permissoes['vLancamento'] == '1'){echo 'checked';}}?> name="vLancamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Lançamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aLancamento'])){ if($permissoes['aLancamento'] == '1'){echo 'checked';}}?> name="aLancamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Lançamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eLancamento'])){ if($permissoes['eLancamento'] == '1'){echo 'checked';}}?> name="eLancamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Lançamento</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dLancamento'])){ if($permissoes['dLancamento'] == '1'){echo 'checked';}}?> name="dLancamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Lançamento</span>

                                        </label>

                                    </td>

                                 

                                </tr>

<tr><td colspan="8"></td></tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vFranquia'])){ if($permissoes['vFranquia'] == '1'){echo 'checked';}}?> name="vFranquia" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Franquia</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aFranquia'])){ if($permissoes['aFranquia'] == '1'){echo 'checked';}}?> name="aFranquia" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Franquia</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['eFranquia'])){ if($permissoes['eFranquia'] == '1'){echo 'checked';}}?> name="eFranquia" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Franquia</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dFranquia'])){ if($permissoes['dFranquia'] == '1'){echo 'checked';}}?> name="dFranquia" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Franquia</span>

                                        </label>

                                    </td>

                                 

                                </tr>



                                <tr><td colspan="8"></td></tr><tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['vPlanos'])){ if($permissoes['vPlanos'] == '1'){echo 'checked';}}?> name="vPlanos" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Visualizar Planos</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['aPlanos'])){ if($permissoes['aPlanos'] == '1'){echo 'checked';}}?> name="aPlanos" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Adicionar Planos</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['ePlanos'])){ if($permissoes['ePlanos'] == '1'){echo 'checked';}}?> name="ePlanos" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Editar Planos</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['dPlanos'])){ if($permissoes['dPlanos'] == '1'){echo 'checked';}}?> name="dPlanos" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Excluir Planos</span>

                                        </label>

                                    </td>

                                 

                                </tr>



                                <tr><td colspan="8"></td></tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rCliente'])){ if($permissoes['rCliente'] == '1'){echo 'checked';}}?> name="rCliente" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Cliente</span>

                                        </label>

                                    </td>

<td>

                                        <label>

                                            <input <?php if(isset($permissoes['rFeedback'])){ if($permissoes['rFeedback'] == '1'){echo 'checked';}}?> name="rFeedback" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Feedback</span>

                                        </label>

                                    </td>

<td>

                                        <label>

                                            <input <?php if(isset($permissoes['rFornecedor'])){ if($permissoes['rFornecedor'] == '1'){echo 'checked';}}?> name="rFornecedor" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Fornecedor</span>

                                        </label>

                                    </td>





                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rServico'])){ if($permissoes['rServico'] == '1'){echo 'checked';}}?> name="rServico" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Serviço</span>

                                        </label>

                                    </td>

<td>

                                        <label>

                                            <input <?php if(isset($permissoes['rEquipamento'])){ if($permissoes['rEquipamento'] == '1'){echo 'checked';}}?> name="rEquipamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Equipamento</span>

                                        </label>

                                    </td>

<td>

                                        <label>

                                            <input <?php if(isset($permissoes['rMarca'])){ if($permissoes['rMarca'] == '1'){echo 'checked';}}?> name="rMarca" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Marca</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rOs'])){ if($permissoes['rOs'] == '1'){echo 'checked';}}?> name="rOs" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório OS</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rProduto'])){ if($permissoes['rProduto'] == '1'){echo 'checked';}}?> name="rProduto" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Produto</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rDespesa'])){ if($permissoes['rDespesa'] == '1'){echo 'checked';}}?> name="rDespesa" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Despesas</span>

                                        </label>

                                    </td>

                                 

                                </tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rVenda'])){ if($permissoes['rVenda'] == '1'){echo 'checked';}}?> name="rVenda" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Venda</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rFinanceiro'])){ if($permissoes['rFinanceiro'] == '1'){echo 'checked';}}?> name="rFinanceiro" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Financeiro</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['rOrcamento'])){ if($permissoes['rOrcamento'] == '1'){echo 'checked';}}?> name="rOrcamento" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Relatório Orçamento</span>

                                        </label>

                                    </td>

                                    <td colspan="2"></td>

                                 

                                </tr>

                                <tr><td colspan="8"></td></tr>



                                <tr>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cUsuario'])){ if($permissoes['cUsuario'] == '1'){echo 'checked';}}?> name="cUsuario" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Configurar Usuário</span>

                                        </label>

                                    </td>

                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cFranquia'])){ if($permissoes['cFranquia'] == '1'){echo 'checked';}}?> name="cFranquia" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Configurar Franquia</span>

                                        </label>

                                    </td>

                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cPlanos'])){ if($permissoes['cPlanos'] == '1'){echo 'checked';}}?> name="cPlanos" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Configurar Planos</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cEmitente'])){ if($permissoes['cEmitente'] == '1'){echo 'checked';}}?> name="cEmitente" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Configurar Emitente</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cPermissao'])){ if($permissoes['cPermissao'] == '1'){echo 'checked';}}?> name="cPermissao" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Configurar Permissão</span>

                                        </label>

                                    </td>



                                    <td>

                                        <label>

                                            <input <?php if(isset($permissoes['cBackup'])){ if($permissoes['cBackup'] == '1'){echo 'checked';}}?> name="cBackup" class="marcar" type="checkbox" value="1" />

                                            <span class="lbl"> Backup</span>

                                        </label>

                                    </td>

                                 

                                </tr>



                            </tbody>

                        </table>

                    </div>

                </div>



              

    

            <div class="form-actions">

                <div class="span12">

                    <div class="span6 offset3">

                        <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>

                        <a href="<?php echo base_url() ?>index.php/permissoes" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>

                    </div>

                </div>

            </div>

           

            </div>

        </div>



                   

    </div>



</form>



</div>





<script type="text/javascript" src="<?php echo base_url()?>assets/js/validate.js"></script>

<script type="text/javascript">

    $(document).ready(function(){



    $("#marcarTodos").change(function () {

        $("input:checkbox").prop('checked', $(this).prop("checked"));

    });   



 

    $("#formPermissao").validate({

        rules :{

            nome: {required: true}

        },

        messages:{

            nome: {required: 'Campo obrigatório'}

        }

    });     



        



    });

</script>

