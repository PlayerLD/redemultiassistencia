<html lang="pt-br">
<head>
<title>ASSISTÊNCIA TÉCNICA ONLINE</title>
<meta charset="UTF-8" />
</head>
<body>
<table width="700px" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6">
<tbody><tr align="center" height="85px">
		<td style="border-bottom:1px solid #e2e2e2">
			<a href="<?php echo $_SERVER['HTTP_ORIGIN']; ?>" alt="Rede Multi Assistência" title="Acessar site">
				<img src="<?php echo base_url();?>assets/img/logo.png" style="width: 150px;">
			</a>
		</td>
	</tr>
	<tr align="center" height="120">
		<td style="padding: 0 15px;">
			<br>
			<br>
			<font face="Arial, Helvetica, sans-serif" color="#616163" size="5"><strong>Você tem uma nova mensagem de nossa equipe!</strong></font>
			<br>
			<br>
			<font face="Arial, Helvetica, sans-serif" color="#959595" size="2">
				<p>Olá, <strong><?php echo $nome?></strong></p>
				<p>Obrigado por solicitar um orçamento conosco, entraremos em contato com você através dos dados informados.</p>
				<br />
				<p style="color: #0088cc;font-weight: bold;">Rede Multi Assistência</p>
				<p style="color: #0088cc;font-size: 11px;">A Franquia de Assistência Técnica mais qualificada do Brasil</p>
			</font>
			<br>
			<br>
			<br>
		</td>
	</tr>
<tr>
</tr>
<tr>
	<td>
		<table width="700" border="0" align="center" cellspacing="15" cellpadding="30" bgcolor="#0088cc">
			<tbody><tr>

				<?php if(!empty($facebook)){ ?>
				<td>
					<a href="<?php echo $facebook; ?>" alt="Facebook" title="Facebook" target="_blank"><img src="<?php echo base_url(); ?>assets/img/email_face.png"></a>
				</td>
				<?php } ?>

				<?php if(!empty($ass)){ ?>
				<td align="right" valign="top" style="padding: 0;">
					<img src="<?php echo $ass; ?>" alt="Assinatura">
				</td>
				<?php } ?>

			</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table></body></html>