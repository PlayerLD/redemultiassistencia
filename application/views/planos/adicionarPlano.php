<script type="text/javascript">
$(document).ready(function(){
    
});
</script>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <form action="<?php echo current_url(); ?>" id="formFranquia" method="post" class="form-horizontal" >
                <div class="widget-title">
                    <span class="icon"><i class="icon-user"></i></span>
                    <h5>Cadastro de Franquia</h5>
                </div>
                <div class="widget-content nopadding">
                    <?php if ($custom_error != '') {
                        echo '<div class="alert alert-danger">'.$custom_error.'</div>';
                    } ?>

                    <div class="control-group">
                        <label for="recorrencia" class="control-label">Recorrência<span class="required">*</span></label>
                        <div class="controls">
                            <select id="recorrencia" name="recorrencia">
                                <option value="1">Mensal</option>
                                <option value="2">Trimestral</option>
                                <option value="3">Semestral</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="dia_recorrencia" class="control-label">Dia da recorrência<span class="required">*</span></label>
                        <div class="controls">
                            <input id="dia_recorrencia" type="number" max="31" min="1" name="dia_recorrencia" value="<?php echo date('d'); ?>">
                        </div>
                    </div>
                
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="valor" class="control-label">Valor<span class="required">*</span></label>
                        <div class="controls">
                            <input id="valor" type="text" name="valor" class="money" value="<?php echo set_value('valor'); ?>"  />
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/planos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </div>

                </form>
        </div>
    </div>
</div>


<script  src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){


           $('#formFranquia').validate({
            rules : {
                  nome :{ required: true},
                  valor:{ required: true}
            },
            messages: {
                  nome :{ required: 'Campo Requerido.'},
                  valor:{ required: 'Campo Requerido.'}

            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });

      });
</script>




