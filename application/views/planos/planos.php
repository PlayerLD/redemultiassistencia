<a href="<?php echo base_url()?>index.php/planos/adicionar<?php echo !empty($id_filial) ? '/'.$id_filial : ''; ?>" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Plano</a>





<div class="widget-box">

        <div class="widget-title">

            <span class="icon"><i class="icon-user"></i></span>

            <h5>Planos</h5>

        </div>

</div>

<div class="widget-content nopadding">

<?php

    require (reisdev('bootstrap'));

    

    $dt = new DataTable();

    $dt->Columns(array('C&#243;d', 'Nome', 'Valor', ''));

    if($results)

    {
        foreach ($results as $r)

        {

            $buttons = '<a href="'.base_url().'index.php/planos/editar/'.$r->idPlanos.'" class="btn btn-info tip-top" title="Editar Plano"><i class="icon-pencil icon-white"></i></a>';


            if ($this->permission->checkPermission($this->session->userdata('permissao'),'dPlanos'))

                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" planos="'.$r->idPlanos.'" class="btn btn-danger tip-top" title="Excluir Plano"><i class="icon-remove icon-white"></i></a>';



            $dt->add_Item(array

            (

                $r->idPlanos,

                $r->nome,

                $r->valor,

                $buttons

            ));

        }

    }

    $dt->End();

?>

</div>



<!-- Modal -->

<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <form action="<?php echo base_url() ?>index.php/planos/excluir" method="post" >

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h5 id="myModalLabel">Excluir Plano</h5>

        </div>

        <div class="modal-body">

            <input type="hidden" id="idPlanoExcluir" name="id" value="" />

            <h5 style="text-align: center">Deseja realmente excluir este Plano?</h5>

        </div>

        <div class="modal-footer">

            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>

            <button class="btn btn-danger">Excluir</button>

        </div>

    </form>

</div>



<script>

    $( document ).ready(function()

    {

          $(document).on('click', 'a', function(event){

                var planos = $(this).attr('planos');

                $('#idPlanoExcluir').val(planos);

          });

     })

</script>