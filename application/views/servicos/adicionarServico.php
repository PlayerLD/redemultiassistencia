<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Serviço</h5>
            </div>
            <div class="widget-content">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
                <div id="conteudo">
                	<div class="formulario">
                		<form action="<?php echo current_url(); ?>" id="formServico" method="post" class="form-horizontal" >
                    		<?php $idFranquia=$this->session->userdata('id');?>
                           	<input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
                            <div class="dados">
                    			<div class="campos">
                                	<div class="campo" style="width: 69%">
                                    	<label for="nome">Nome <span class="required">*</span></label>
                                        <input id="nome" type="text" name="nome" value="<?php echo set_value('nome'); ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 30%">
                                        <label for="preco">Preço <span class="required">*</span></label>
                                        <input id="preco" class="money" type="text" name="preco" value="<?php echo set_value('preco'); ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 100%">
                                        <label for="descricao">Descrição</label>
                                        <input id="descricao" type="text" name="descricao" value="<?php echo set_value('descricao'); ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 30%">
                                    	<label for="preco">Código da lista de serviços <span class="required">*</span></label>
                                        <input id="itemListaServico" type="text" name="itemListaServico" value="<?php echo set_value('itemListaServico'); ?>" />
                                    </div>
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                    	<button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                		<a href="<?php echo base_url() ?>index.php/servicos" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                             	</div>
                           	</div>
                		</form>
                 	</div>
             	</div>              
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
    	
        
		$('#formServico').validate({
        	rules :{
            	nome:{ required: true},
            	preco:{ required: true}
            },
            messages:{
            	nome :{ required: ''},
            	preco :{ required: ''}
            },
            highlight:function(element, errorClass, validClass) {
          		$(element).parents('.campo').removeClass('success');
				$(element).parents('.campo').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.campo').removeClass('error');
                $(element).parents('.campo').addClass('success');
            }
		}); 
	});
</script>