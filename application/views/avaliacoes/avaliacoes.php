<?php

  require (reisdev('bootstrap'));

  //$CI = &get_instance(); 
 ?>

<div class="widget-box">    
	<div class="widget-title">        	
		<span class="icon">            		
			<i class="icon icon-star"></i> 
			        	</span>        
			        	<h5>Avaliações</h5>    
	</div>
</div>
<div class="widget-content nopadding">    
	<?php    $dt = new DataTable();  

	if ($this->permission->checkPermission($this->session->userdata('permissao'),'detFeedback')) {        
		$dt->Columns(array('OS', 'Cliente', 'Avaliação', 'Data', 'Comentário'));    
	} else {        
		$dt->Columns(array('Avaliação', 'Data', 'Comentário'));    
	}    

if($results){  
 
	//echo "<pre>";
	//var_dump($results);
	//echo "</pre>";

	foreach ($results as $r){           
		$comentario = "<a href='#modalComents".$r->idOs."' data-toggle='modal' role='button' class='btn btn-success'>Visualizar</a>";

		$star = ' ';

	    if ($r->nota > 0) {                

	     	for ($i = 0; $i < $r->nota; $i++)                    

	     		$star = $star.'<i class="icon icon-star"></i>';                

	     	if ($r->nota > 1)                    
	     		$star = $star.' ('.$r->nota.' estrelas)';                
	     	else                   
	     	 $star = $star.' (1 estrela)';        

	     	$data = date('d/m/Y',  strtotime($r->data));           
		    
		    if ($this->permission->checkPermission($this->session->userdata('permissao'),'detFeedback')) {                
		      	$dt->add_Item(array(                    
		      	$r->idOs,$r->nomeCliente,$star,$data,$comentario)); 
		    } else {                
		       	$dt->add_Item(array($star,$data,$comentario));     
		    }

	   	} else {  

	     	$star = 'Nenhuma estrela.';            
	     	$data = date('d/m/Y',  strtotime($r->data));           
		    
		    if ($this->permission->checkPermission($this->session->userdata('permissao'),'detFeedback')) {                
		      	echo $dt->add_Item(array(                    
		      	$r->idOs,$r->nomeCliente,$star,$data,$comentario)); 
		    } else {                
		       	$dt->add_Item(array($star,$data,$comentario));     
		    }      
	    }  
	} // foreach $r
}    // if $results

$dt->End();    
	

	foreach ($results as $r){   
?>
</div>
	
<div id="modalComents<?php echo $r->idOs; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="formAlterar">
  
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="">Visualização - Comentários</h3>
  </div>
  <div class="modal-body">
        
     <div class="control-group">
        <label for="nome" class="control-label">Comentário: </label>
        <div class="controls">
            <textarea disabled><?php echo $r->obs; ?></textarea>
        </div>
    </div>      

  </div>
  <div class="modal-footer">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">FECHAR</button>
  </div>
  </form>
</div>

<?php } ?>