<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>ASSISTÊNCIA TÉCNICA ONLINE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/star-rating.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-krajee-fa.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-krajee-svg.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-krajee-uni.css" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>

    <style>

        body {
             background-color: #0E6FB6;
         }
        body {
            color: white;
        }

        footer, header {
            color: gray;
            text-align: center;
            padding: 40px;
            background-color: white;
            margin-bottom: 0;
        }
        legend {
            font-size: 20pt;
            color: white;
        }

    </style>
</head>
<body>

<center>
    <a href="http://redemultiassistencia.com.br">
        <img src="<?php echo base_url();?>assets/img/franquia-rede-multi-assistencia.png" alt="Logo" class="img" />
    </a>
</center>


<header>
    <?php if($this->session->flashdata('success')): ?>
        <h2><?php echo $this->session->flashdata('success')?></h2>
    <?php else: ?>
        <h2>Avalie Nosso Trabalho</h2>
    <?php endif; ?>

</header>

<div class="container">

    <br />

    <div class="row">

        <h3 class="text-justify">Sua Opinião é muito importante para nós, responda algumas perguntas para que possamos melhorar cada dia. </h3>

        <table class="table table-condensed">
            <tr>
                <td>Cliente: </td>
                <td><?php echo $result->nomeCliente ?></td>
            </tr>
            <tr>
                <td>Data do Atendimento: </td>
                <td><?php echo date("d/m/Y", strtotime($result->dataInicial)) ?></td>
            </tr>
            <tr>
                <td>Data da Entrega: </td>
                <td><?php echo date("d/m/Y", strtotime($result->dataFinal)) ?></td>
            </tr>
            <tr>
                <td>Equipamento: </td>
                <td><?php echo $result->equipamento ?></td>
            </tr>
            <tr>
                <td>Modelo: </td>
                <td><?php echo $result->modelo ?></td>
            </tr>
            <tr>
                <td>Defeito: </td>
                <td><?php echo json_decode($result->defeito)[0] ?></td>
            </tr>
        </table>

        <?php if($custom_error == true){ ?>
            <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique se os campos estão preenchidos corretamente.</div>
        <?php } ?>

        <form method="post" action="<?php echo current_url();?>" class="form" id="frmAvaliacao">

            <input type="hidden" name="key" value="<?php echo $key?>" />
            <input type="hidden" name="idAvaliacao" value="<?php echo $result->idAvaliacao ?>" />
            <fieldset>

                <label class="control-label" for="nota"><h4>Como você avalia esse atendimento?</h4></label>
                <input class="kv-svg rating-loading" value="<?php echo $result->nota ?>" data-size="xl" id="nota" name="nota">
                <div class="clearfix"></div>

                <!-- Textarea -->
                <div class="control-group">
                    <label class="control-label" for="obs"><h4>Observação</h4></label>
                    <div class="controls">
                        <textarea id="obs" name="obs" style="min-width: 100%" rows="10" class="span7"><?php echo $result->obsAvaliacao ?></textarea>
                    </div>
                </div>

                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="btnEnviar"></label>
                    <div class="controls">
                        <button id="btnEnviar" name="btnEnviar" class="btn btn-success">Enviar</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>

<footer>
    © 2016 Rede Multi Assistência. All Rights Reserved
</footer>


<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/star-rating.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/star-rating_locale_pt-br.js"></script>

<script>
    $(function () {
        $('.kv-svg').rating({
            theme: 'krajee-svg',
            filledStar: '<span class="krajee-icon krajee-icon-star"></span>',
            emptyStar: '<span class="krajee-icon krajee-icon-star"></span>',
            step: 1,
            language: 'pt-BR'
        });
    });
</script>

</body>
</html>
