<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>ASSISTÊNCIA TÉCNICA ONLINE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/mask.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#celular').mask('99999999999');
            $('#cep').mask('99999999');
        });
    </script>

    <?php if(!empty($card_process)): ?>
        <script type="text/javascript">
            alert('<?php echo $card_process; ?>');
            window.close();
        </script>
    <?php exit; ?>
    <?php endif; ?>
    <style>
        body {
            background-color: #08c;
        }
        body {
            color: white;
        }
        h3 {
            font-size: 18px;
        }
        footer, header {
            color: gray;
            text-align: center;
            padding: 40px;
            background-color: white;
            margin-bottom: 0;
        }
        legend {
            font-size: 20pt;
            color: white;
        }
        table {
            background: white;
            color: #666666;
            width: 100%;
            border-radius: 7px;
            overflow: hidden;
        }

        td {
            padding: 10px 20px;
        }

        thead {
            background: #f5ebb0;
        }

        tr + tr {
            border-top: 1px solid #e0dede;
        }
        @media (max-width: 767px){
            .modal {
                position: fixed;
                top: 20px;
                right: none;
                left: 50%;
                margin: 0;
                padding: 15px;
            }
            div#sticky h1 {
                font-size: 23px !important;
            }
        }
    </style>
</head>
<body>
<br />
<center>
    <a href="http://redemultiassistencia.com.br">
        <img src="<?php echo base_url();?>assets/img/franquia-rede-multi-assistencia.png" alt="Logo" class="img" />
    </a>
</center>
<header>
    <?php if($this->session->flashdata('success') != null && !is_array($this->session->flashdata('success'))){?>
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h2><?php echo $this->session->flashdata('success');?></h2>
           </div>
      <?php }elseif(is_array($this->session->flashdata('success'))){?>
        <?php foreach($this->session->flashdata('success') as $v){?>
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h2><?php echo $v;?></h2>
             </div>
        <?php }?>
      <?php }else{ ?>
        <h2>Pagamento da ordem de serviço</h2>
    <?php } ?>
</header>
<div class="container" style="padding: 0 15px;">
    <br />
    <div class="row">
        <div class="span7">
            <?php if($custom_error == true){ ?>
                <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique se os campos estão preenchidos corretamente.</div>
            <?php } ?>
            
            <?php //dd($emitente); exit; ?>
            <?php $total = (float)0; ?>
            <?php if(!empty($equipamentos)){ ?>
            <h3>Equipamento(s) recolhido(s)</h3>
            <table>
                <thead>
                    <tr>
                        <td class="name">Modelo</td>
                        <td class="quantity">Tipo</td>
                        <td class="price" nowrap>Observação</td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($equipamentos as $p): ?>
                    <tr>
                        <td class="name"><?php echo $p->modelo; ?></td>
                        <td class="quantity" nowrap><?php echo $p->tipo; ?></td>
                        <td class="price" nowrap><?php echo $p->obser; ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
            <?php } ?>
            
            <?php if(!empty($servicos)){ ?>
            <br>
            <hr>
            <h3>Serviços (mão de obra)</h3>
            <table>
                <thead>
                    <tr>
                        <td class="name">Nome</td>
                        <td class="quantity">Descrição</td>
                        <td class="price" nowrap>Preço</td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($servicos as $p): ?>
                    <tr>
                        <td class="name"><?php echo $p->nomeServico; ?></td>
                        <td class="quantity" nowrap><?php echo $p->descricaoServico; ?></td>
                        <td class="price" nowrap>R$ <?php echo number_format($p->precoServico, 2, ',', '.'); ?></td>
                    </tr>
                <?php $total+=$p->precoServico; ?>
                <?php endforeach ?>
                </tbody>
            </table>
            <?php } ?>
            
            <?php if(!empty($produtos)){ ?>
            <br>
            <hr>
            <h3>Produtos/Peças</h3>
            <table>
                <thead>
                    <tr>
                        <td class="name">Produto</td>
                        <td class="quantity">Quantidade</td>
                        <td class="price" nowrap>Valor unitário</td>
                        <td class="total">Total</td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($produtos as $p): ?>
                    <tr>
                        <td class="name"><?php echo $p->descricao; ?></td>
                        <td class="quantity" nowrap><?php echo $p->quantidade; ?></td>
                        <td class="price" nowrap>R$ <?php echo number_format($p->subTotal, 2, ',', '.'); ?></td>
                        <td class="total" nowrap>R$ <?php echo number_format($p->precoVenda, 2, ',', '.'); ?></td>
                    </tr>
                <?php $total+=$p->precoVenda; ?>
                <?php endforeach ?>
                </tbody>
            </table>
            <?php } ?>
            <br>
            <br>
            <br>
        </div>
        <div class="span5">
            <form method="post" class="form" id="frmCartao"> 

                <?php if(!empty($orcamento)){ ?>
                <h2>Pagar em: <span class="installments"><input type="hidden" name="installments" value="<?php echo $orcamento->vezes; ?>"><?php echo $orcamento->vezes; ?>X</span></h2>
                <h3>Total: R$ <span id="valorTotal"><?php echo number_format($total, 2, ',', '.'); ?></span></h3>
                <?php }else{ ?>
                <h2>Total: R$ <?php echo number_format($total, 2, ',', '.'); ?></h2>
                <?php } ?>



                <fieldset>
                
                    <!-- Text input-->
                    <?php if(@$endereco){ ?>
                    <div class="control-group">
                        <label class="control-label" style="cursor: default;" for="celular">Endereço</label>
                        <div class="controls">
                            <input id="celular" name="celular" min="1000000000" max="99999999999"type="text" placeholder="Celular" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="cep" name="cep" type="text" placeholder="CEP" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="rua" name="rua" type="text" placeholder="Rua" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="numero" name="numero" type="text" placeholder="Número" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="complemento" name="complemento" type="text" placeholder="Complemento" class="input-xxlarge span5" >
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="bairro" name="bairro" type="text" placeholder="Bairro" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input id="cidade" name="cidade" type="text" placeholder="Cidade" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <select id="estado" type="text" name="estado" class="input-xxlarge span5" required>
                                <option>Escolha o estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label" style="cursor: default;" for="cvv_cartao">CVV do cartão final <?php echo $final_cartao; ?> </label>
                        <div class="controls">
                            <input id="cvv_cartao" name="cvv_cartao" type="password" <?php echo !empty($cvv_cartao) ? 'value="'.$cvv_cartao.'"' : ''; ?> placeholder="" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="control-group">
                        <label class="control-label" style="cursor: default;" for="cvv_cartao">CVV do cartão final <?php echo $final_cartao; ?> <?php if(!empty($clientes_id)){ ?> ou <a href="<?php echo site_url('sistemaos/cadastrar_cartao/'.$clientes_id.'/'.$os_id); ?>"  style="color: white;">use outro cartão</a><?php } ?></label>
                        <div class="controls">
                            <input id="cvv_cartao" name="cvv_cartao" type="password" <?php echo !empty($cvv_cartao) ? 'value="'.$cvv_cartao.'"' : ''; ?> placeholder="" class="input-xxlarge span5" required>
                        </div>
                    </div>
                    <?php } ?>
                   
                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnEnviar"></label>
                        <div class="controls">
                            <button id="btnEnviar" name="btnEnviar" class="btn btn-success">Efetuar Pagamento</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<footer>
    © 2016 Rede Multi Assistência. All Rights Reserved
</footer>

<!-- Gerencianet -->
<?php if(!empty($franquia->identificador_conta)){ ?>
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/<?php echo $franquia->identificador_conta; ?>/'+v;s.async=false;s.id='<?php echo $franquia->identificador_conta; ?>';if(!document.getElementById('<?php echo $franquia->identificador_conta; ?>')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<?php }else{ ?>
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/1a02e21dd07c5eae79ac5863a0ff22da/'+v;s.async=false;s.id='1a02e21dd07c5eae79ac5863a0ff22da';if(!document.getElementById('1a02e21dd07c5eae79ac5863a0ff22da')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<?php } ?>
<!-- ----------- -->

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate_pt-br.js"></script>
<script>

function number_format( numero, decimal, decimal_separador, milhar_separador ){ 

      numero = (numero + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+numero) ? 0 : +numero,
      prec = !isFinite(+decimal) ? 0 : Math.abs(decimal),
      sep = (typeof milhar_separador === 'undefined') ? ',' : milhar_separador,
      dec = (typeof decimal_separador === 'undefined') ? '.' : decimal_separador,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
      };
  // Fix para IE: parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}


$(document).ready(function(){

    $gn.ready(function(checkout){
 
      checkout.getInstallments(<?php echo number_format($total, 2, '', ''); ?>,'<?php echo $bandeira; ?>', function(error, response){
        if(error) {
          // Trata o erro ocorrido
          console.log(error);
        } else {
            var select = '<select name="installments" id="installments">';
          for (var i = 0; i < response.data.installments.length; i++) {
            var valor = response.data.installments[i].value.toString();
            select += '<option data-valor="'+valor+'" value="'+response.data.installments[i].installment+'">';
            select += response.data.installments[i].installment+'X';
            select += ' de ' + number_format(valor.slice(0, -2)+'.'+valor.substr(-2, 2), 2, ',', '.');

            if(response.data.installments[i].has_interest == true)
                select += ' (com juros)';

            select += '</option>';
              
          }
        select += '</select>';

        $('.installments').html(select);
        calc_installments();


        }
      });
     
    });

    function calc_installments(){
        console.log('funciona porra!')
        $('#installments').on('change', function(){
            var valor = parseInt($('#installments option:selected').data('valor')),
                vezes = parseInt($('#installments').val()),
                total = (valor*vezes).toString();
            $('#valorTotal').text(number_format(total.slice(0, -2)+'.'+total.substr(-2, 2), 2, ',', '.'));
        });
    }

    $(function () {
        $("#frmCartao").validate({
            rules: {
                cvv_cartao:       'required'
            }
        });
    });

    $('input[name*=cep]').on('keyup', function(){
        var cep = $(this).val().replace('-', '');
        if(cep.length == 8) {
          form = $(this).closest('form');
          $.ajax({
            url: 'https://api.pagar.me/1/zipcodes/'+cep
          }).done(function( data ) {
            form.find('input[name="rua"]').val(data['street']);
            form.find('select[name="estado"]').find('option[value="'+data['state']+'"]').prop('selected', true);
            form.find('input[name="cidade"]').val(data['city']);
            form.find('input[name="bairro"]').val(data['neighborhood']);
          }).fail(function(){
          });
        }
      });

});
</script>
</body>
</html>