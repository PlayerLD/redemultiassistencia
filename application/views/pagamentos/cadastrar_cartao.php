<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>ASSISTÊNCIA TÉCNICA ONLINE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/mask.js"></script>
    <script type="text/javascript">
        $('#vencimento_cartao').mask('99/99');
        $('#nascimento').mask('99/99/9999');
        $('#cpf_cartao').mask('99999999999');
        $('#num_cartao').mask('9999999999999999');
        $('#cvv_cartao').mask('999');
    </script>
    <style>
        body {
            background-color: #0E6FB6;
        }
        body {
            color: white;
        }
        footer, header {
            color: gray;
            text-align: center;
            padding: 40px;
            background-color: white;
            margin-bottom: 0;
        }
        legend {
            font-size: 20pt;
            color: white;
        }
        @media (max-width: 767px){
            .modal {
                position: fixed;
                top: 20px;
                right: none;
                left: 50%;
                margin: 0;
                padding: 15px;
            }
            div#sticky h1 {
                font-size: 23px !important;
            }
        }
    </style>
</head>
<body>
<br />
<center>
    <a href="http://redemultiassistencia.com.br">
        <img src="<?php echo base_url();?>assets/img/franquia-rede-multi-assistencia.png" alt="Logo" class="img" />
    </a>
</center>
<header>
    <?php if($this->session->flashdata('success')): ?>
        <h2><?php echo $this->session->flashdata('success')?></h2>
    <?php else: ?>
        <h2>Cadastro de cartão de crédito</h2>
        <h4>Para pagamento do orçamento #<?php echo $id_orcamento; ?></h4>
    <?php endif; ?>
</header>
<div class="container" style="padding: 0 15px;">
    <br />
    <div class="row">
        <div class="span7">
            <?php if($custom_error == true){ ?>
                <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique se os campos estão preenchidos corretamente.</div>
            <?php } ?>
            <form method="post" class="form" id="frmCartao">    
                <fieldset>
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="nome_cartao">Nome do titular (obrigatório)</label>
                        <div class="controls">
                            <input id="nome_cartao" name="nome_cartao" type="text" placeholder="" class="input-xxlarge span7" required="" >
                        </div>
                    </div>
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="cpf_cartao">CPF do titular (obrigatório)</label>
                        <div class="controls">
                            <input id="cpf_cartao" name="cpf_cartao" type="text" placeholder="" class="input-xxlarge span7" required="" >
                        </div>
                    </div>
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="nascimento">Data de nascimento do titular (obrigatório)</label>
                        <div class="controls">
                            <input id="nascimento" name="nascimento" type="text" placeholder="" class="input-xxlarge span7" required="" >
                        </div>
                    </div>
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="num_cartao">Número do cartão (obrigatório)</label>
                        <div class="controls">
                            <input id="num_cartao" name="num_cartao" type="text" placeholder="" class="input-xxlarge span7" required="" >
                            <input type="hidden" name="token_pagamento">
                            <input type="hidden" name="cartao_bandeira">
                            <ul class="col-sm-12 form-group card-list">

                                <li class="visa"><img src="<?php echo base_url();?>assets/img/cards/visa.jpg" alt="visa"></li>
                                <li class="mastercard"><img src="<?php echo base_url();?>assets/img/cards/mastercard.jpg" alt="mastercard"></li>
                                <li class="diners"><img src="<?php echo base_url();?>assets/img/cards/diners.jpg" alt="diners"></li>
                                <li class="AMERICAN_EXPRESS"><img src="<?php echo base_url();?>assets/img/cards/american.jpg" alt="american"></li>
                                <li class="elo"><img src="<?php echo base_url();?>assets/img/cards/elo.jpg" alt="elo"></li>
                                <li class="aura"><img src="<?php echo base_url();?>assets/img/cards/aura.jpg" alt="aura"></li>
                                <li class="hipercard"><img src="<?php echo base_url();?>assets/img/cards/hipercard.jpg" alt="hipercard"></li>

                            </ul>
                        </div>
                    </div>
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="vencimento_cartao">Vencimento do cartão (obrigatório)</label>
                        <div class="controls">
                            <input id="vencimento_cartao" name="vencimento_cartao" type="text" placeholder="" class="input-xxlarge span7" required="" >
                        </div>
                    </div>
                    
                    <?php if(@$cvv){ ?>
                    <!-- Text input -->
                    <div class="control-group">
                        <label class="control-label" for="cvv_cartao">Código de segurança (CVV)</label>
                        <div class="controls">
                            <input id="cvv_cartao" name="cvv_cartao" type="text" placeholder="" class="input-xxlarge span7" required="" >
                        </div>
                    </div>
                    <?php } ?>
                   
                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnEnviar"></label>
                        <div class="controls">
                            <button id="btnEnviar" name="btnEnviar" class="btn btn-success" disabled>Cadastrar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="span5">
            <img src="<?php echo base_url();?>assets/img/garantia-conserto-de-celular-tablets-notebook-video-game.png" alt="Garantia" class="img" />
        </div>
    </div>
</div>
<footer>
    © 2016 Rede Multi Assistência. All Rights Reserved
</footer>

<!-- script do gerencianet.com -->
<?php if(!empty($franquia->identificador_conta)){ ?>
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/<?php echo $franquia->identificador_conta; ?>/'+v;s.async=false;s.id='<?php echo $franquia->identificador_conta; ?>';if(!document.getElementById('<?php echo $franquia->identificador_conta; ?>')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<?php }else{ ?>
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/1a02e21dd07c5eae79ac5863a0ff22da/'+v;s.async=false;s.id='1a02e21dd07c5eae79ac5863a0ff22da';if(!document.getElementById('1a02e21dd07c5eae79ac5863a0ff22da')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<?php } ?>
<!-- ------------------------ -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate_pt-br.js"></script>
<script>
$(document).ready(function(){

    $(function () {
        $("#frmCartao").validate({
            rules: {
                nome_cartao:       'required',
                cpf_cartao:        'required',
                num_cartao:        'required',
                vencimento_cartao: 'required'
            }
        });
    });

    $('input[name="num_cartao"]').on('blur', function(){
        var card = detect_card_type($(this).val());
        $('input[name="cartao_bandeira"]').val(card);
        $('.card-list').find('li').addClass('unselected').removeClass('active');
        if(card != undefined) 
            $('.card-list').find('li.'+card).removeClass('unselected').addClass('active');
    });

    $gn.ready(function(checkout) {
            $('input[name="num_cartao"], input[name="mes_cartao"], input[name="ano_cartao"], input[name="cvv_cartao"]').on('blur', function(){
                verify_cartao(checkout);
            });
        });

        function verify_cartao(checkout) {
            var num_cartao = $('input[name="num_cartao"]').val();
            var mes_cartao = $('input[name="vencimento_cartao"]').val().split('/')[0];
            var ano_cartao = $('input[name="vencimento_cartao"]').val().split('/')[1];
            var cvv_cartao = $('input[name="cvv_cartao"]').val();
            var bandeira = detect_card_type(num_cartao);

            if(num_cartao != '' && mes_cartao != '' && ano_cartao != '' && cvv_cartao != ''){


              var callback = function(error, response) {
                if(error) {
                          // Trata o erro ocorrido
                          console.error(error);
                      } else {
                          // Trata a resposta
                          token = response.data.payment_token;
                          $('input[name="token_pagamento"]').val(token);
                          $('#btnEnviar').attr('disabled', false);
                      }
                  };

                  checkout.getPaymentToken({
                        brand: bandeira, // bandeira do cartão
                        number: num_cartao, // número do cartão
                        cvv: cvv_cartao, // código de segurança
                        expiration_month: mes_cartao, // mês de vencimento
                        expiration_year: ano_cartao // ano de vencimento
                    }, callback);

              }
        }

        function detect_card_type(number) {
            var re = {
                visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                mastercard: /^5[1-5][0-9]{14}$/,
                amex: /^3[47][0-9]{13}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                elo: /^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/,
                jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
                hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})$/,
                aura: /^50[0-9]{17}$/
            };
            if (re.visa.test(number)) {
                return 'visa';
            } else if (re.mastercard.test(number)) {
                return 'mastercard';
            } else if (re.amex.test(number)) {
                return 'amex';
            } else if (re.diners.test(number)) {
                return 'diners';
            } else if (re.elo.test(number)) {
                return 'elo';
            } else if (re.hipercard.test(number)) {
                return 'hipercard';
            } else if (re.aura.test(number)) {
                return 'aura';
            } else if (re.discover.test(number)) {
                return 'discover';
            }else if (re.jcb.test(number)) {
                return 'jcb';
            } else {
                return undefined;
            }
        }

});
</script>
</body>
</html>