<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>ASSISTÊNCIA TÉCNICA ONLINE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <style>
        body {
            background-color: #0E6FB6;
        }
        body {
            color: white;
        }
        footer, header {
            color: gray;
            text-align: center;
            padding: 40px;
            background-color: white;
            margin-bottom: 0;
        }
        legend {
            font-size: 20pt;
            color: white;
        }
        .box-anexar {
            margin-top: 100px;
        }
        @media (max-width: 767px){
            .box-anexar {
                margin-top: 0;
            }

            .modal {
                position: fixed;
                top: 20px;
                right: none;
                left: 50%;
                margin: 0;
                padding: 15px;
            }

            div#sticky h1 {
                font-size: 23px !important;
            }
        }
    </style>
</head>
<body>
<br />
<center>
    <a href="http://redemultiassistencia.com.br">
        <img src="<?php echo base_url();?>assets/img/franquia-rede-multi-assistencia.png" alt="Logo" class="img" />
    </a>
</center>
<header>
    <?php if($this->session->flashdata('success')): ?>
        <h2><?php echo $this->session->flashdata('success')?></h2>
    <?php else: ?>
        <h2>Envie o comprovante de depósito bancário</h2>
        <h4>Para nossos técnicos dar início ao serviço.</h4>
    <?php endif; ?>
</header>
<div class="container" style="padding: 0 15px;">
    <br />
    <div class="row">
        <div class="span7">
            <form method="post" class="form" id="frmCartao" enctype="multipart/form-data">
                <fieldset class="box-anexar">
                
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="nome_cartao">Anexar arquivo:</label>
                        <div class="controls">
                            <input type="file" class="form-control" name="arquivo">
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnEnviar"></label>
                        <div class="controls">
                            <button id="btnEnviar" name="btnEnviar" class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="span5">
            <img src="<?php echo base_url();?>assets/img/garantia-conserto-de-celular-tablets-notebook-video-game.png" alt="Garantia" class="img" />
        </div>
    </div>
</div>
<footer>
    © 2016 Rede Multi Assistência. All Rights Reserved
</footer>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate_pt-br.js"></script>
</body>
</html>