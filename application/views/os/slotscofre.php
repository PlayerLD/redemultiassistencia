<?php

$CI = &get_instance();

?>
<style>
    .number-column {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        color: #0088cc;
        height: 36px;
        width: 36px;
    }
    .letter-column {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        color: #0088cc;
        padding-right: 9px;
    }
    .small-slot-title {
        background: #e0c9a9;
        width: 36px;
        border: 1px solid;
        height: 36px;
        display: inline-block;
        margin-right: 15px;
    }
    .p{
        background: #e0c9a9;
        width: 36px;
        border: 1px solid;
        height: 36px;
        margin-right: 15px;
        text-align: center;
    }
    .medium-slot-title {
        background: #ecca63;
        width: 36px;
        border: 1px solid;
        height: 55px;
        display: inline-block;
        margin-right: 15px;
    }
    .m{
        background: #ecca63;
        width: 36px;
        border: 1px solid;
        height: 55px;
        margin-right: 15px;
        text-align: center;
    }
    .large-slot-title {
        background: #f1ddc1;
        width: 36px;
        border: 1px solid;
        height: 79px;
        display: inline-block;
        margin-right: 15px;
    }
    .g{
        background: #f1ddc1;
        width: 36px;
        border: 1px solid;
        height: 79px;
        margin-right: 15px;
        text-align: center;
    }
    .itembg1
    {
        width: 36px;
        height: 36px;
        background: #e0c9a9;
        padding: 0;
        /* padding-left: 2px; */
        border: 1px solid;
        text-align: center;
    }
    .slotname
    {
        margin-top: 7px;
        text-align: center;
    }
    .divimg
    { 
        margin-top: -12px;
        position: absolute;
        margin-left: 8px;
    }
    .itembg1:hover {
        background-color: #c7ac87;
    }

    td.itembg1.P {
        height: 36px;
        background: #e0c9a9;
    }
    td.itembg1.P:hover {
        background: #c7ac87;
    }

    td.itembg1.M {
        height: 55px;
        background: #ecca63;
    }
    td.itembg1.M:hover {
        background: #dcad20;
    }

    td.itembg1.G {
        height: 79px;
        background: #f1ddc1;
    }
    td.itembg1.G:hover {
        background: #e0c9a9;
    }

    .os-link {
        display:block;
    }

    .iconimg
    {
        border: 0px; 
        width: 20px;
        height: 20px;
    }
    @media(min-width:1200px){
        .container{width: 600px;}
    }
</style>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-sitemap"></i>
                </span>

                <h5>Slots - COFRE</h5>
            </div>
            <div class="widget-content" style="overflow-x: auto;position:relative;">

                <div class="container">
                    <h2>
                        <div class="small-slot-title" style=""></div>SLOTS PEQUENOS <br>
                        <i style="font-size:14px;line-height:14px">Celulares</i>
                    </h2>

                    <h2>
                        <div class="medium-slot-title" style=""></div>SLOTS MÉDIOS <br>
                        <i style="font-size:14px;line-height:14px">Notebooks, tablets, Video Games</i>
                    </h2>

                    <h2>
                        <div class="large-slot-title" style=""></div>SLOTS GRANDES <br>
                        <i style="font-size:14px;line-height:14px">Tvs, Computadores (PCs)</i>
                    </h2>
                </div>

            <a href="<?php echo site_url('os/slots/'); ?>" class="btn btn-warning"> <- Localização</a>
                
                <?php 
                
                    

                    $cofre = 25;
                    $qtd_cofre_fl = 26;

                    //$scofre = $CI->os_model->slotsDadoscofre($this->session->userdata('id'));
                    //echo "<pre>";
                    //print_r($scofre);
                    //echo "</pre>";
                 ?>
                <hr>
                <p>COFRE</p>

                    <table>
                        <tbody>
                            <tr>
                                <td></td>
                                <?php for($i = 1; $i <= $qtd_cofre_fl; $i++){ ?>
                                    <td class='letter-column'>
                                        <?php echo $i ?>
                                    </td>
                                <?php } ?>
                            </tr>
                            
                                <?php $CI->osSlotsCofre($this->session->userdata('id')); ?>                   
                            
                        </tbody>
                    </table>
            
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#table2 tr').each(function(){
        if($(this).find('td').length == 1){
            $(this).remove();
        }
    });
</script>