<?php

$CI = &get_instance();

?>
<style>
    .number-column {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        color: #0088cc;
        height: 36px;
        width: 36px;
    }
    .letter-column {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        color: #0088cc;
        padding-right: 9px;
    }
    .small-slot-title {
        background: #e0c9a9;
        width: 36px;
        border: 1px solid;
        height: 36px;
        display: inline-block;
        margin-right: 15px;
    }
    .p{
        background: #e0c9a9;
        width: 36px;
        border: 1px solid;
        height: 36px;
        margin-right: 15px;
        text-align: center;
    }
    .medium-slot-title {
        background: #ecca63;
        width: 36px;
        border: 1px solid;
        height: 55px;
        display: inline-block;
        margin-right: 15px;
    }
    .m{
        background: #ecca63;
        width: 36px;
        border: 1px solid;
        height: 55px;
        margin-right: 15px;
        text-align: center;
    }
    .large-slot-title {
        background: #f1ddc1;
        width: 36px;
        border: 1px solid;
        height: 79px;
        display: inline-block;
        margin-right: 15px;
    }
    .g{
        background: #f1ddc1;
        width: 36px;
        border: 1px solid;
        height: 79px;
        margin-right: 15px;
        text-align: center;
    }
    .itembg1
    {
        width: 36px;
        height: 36px;
        background: #e0c9a9;
        padding: 0;
        /* padding-left: 2px; */
        border: 1px solid;
        text-align: center;
    }
    .slotname
    {
        margin-top: 7px;
        text-align: center;
    }
    .divimg
    { 
        margin-top: -12px;
        position: absolute;
        margin-left: 8px;
    }
    .itembg1:hover {
        background-color: #c7ac87;
    }

    td.itembg1.P {
        height: 36px;
        background: #e0c9a9;
    }
    td.itembg1.P:hover {
        background: #c7ac87;
    }

    td.itembg1.M {
        height: 55px;
        background: #ecca63;
    }
    td.itembg1.M:hover {
        background: #dcad20;
    }

    td.itembg1.G {
        height: 79px;
        background: #f1ddc1;
    }
    td.itembg1.G:hover {
        background: #e0c9a9;
    }

    .os-link {
        display:block;
    }

    .iconimg
    {
        border: 0px; 
        width: 20px;
        height: 20px;
    }
    @media(min-width:1200px){
        .container{width: 600px;}
    }
</style>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-sitemap"></i>
                </span>

                <h5>Slots</h5>
            </div>
            <div class="widget-content" style="overflow-x: auto;position:relative;">

                <div class="container">
                    <h2>                        
                    <div class="row">
                        <div class="span4" style="border: solid 1px black; padding: 5px;">
                            <div class="small-slot-title" style=""></div><small>SLOTS</small> PEQUENOS <br> 
                            <i style="font-size:14px;line-height:14px">Celulares</i>
                        </div>
                        <div class="span4" style="border: solid 1px black; padding: 5px;">
                            <div class="medium-slot-title" style=""></div><small>SLOTS</small> MÉDIOS <br> 
                            <i style="font-size:14px;line-height:14px">Notebooks, tablets, Video Games</i>
                        </div>
                        <div class="span4" style="border: solid 1px black; padding: 5px;">
                            <div class="large-slot-title" style=""></div><small>SLOTS</small> GRANDES <br>
                            <i style="font-size:14px;line-height:14px;">Tvs, Computadores (PCs)</i>
                        </div>
                    </div>
                    </h2>
                </div>

            <a href="<?php echo site_url('os/slotscofre/'); ?>" class="btn btn-warning">SLOTS - COFRE</a>
                <br>
                <?php 
                //echo "<p>Verificar melhor area de editar franquias, pois na edição somente do slot P, todos os outros foram alterados.</p>";
                // com esses parametros funcionou numa boa.
                //echo $id = 10822;
                //$eq = 9883;
                //$idos = 10833;
                //echo $CI->os_model->iconesEquiSlots($idos);
                //echo $CI->os_model->deleteEquiLiberaSlots($id);
                //$empty = 'A0';
                //$empty = $CI->os_model->getDadosSlotEmpty($eq);
                //echo "<pre>";
                //var_dump($empty);
                //echo "</pre>";
                //if ($empty['posicao'] != 0) {
                //    
                //    $data = array(
                //            'equipamentos_id' => $eq,
                //            'os_id' => $id,
                //            'slot' => $empty['posicao']
                //    );
                //
                //$CI->os_model->addSlotsUpdt('equipamentos_os', $data, $empty);
                //}else if($empty['posicao'] == 0){
                //    echo "Sem espaço para guardar aparelhos, favor liberar slots </br>";
                //}

                //echo "</br>".$CI->getDadosSlotEmpty('Celular')."</br>";
                //echo "</br>";

                //echo ('<small>Slot:  '.$CI->posicaoSlots('p').' - posição livre para P</small></br>');
                //echo ('<small>Slot:  '.$CI->posicaoSlots('m').' - posição livre para M</small></br>');
                //echo ('<small>Slot:  '.$CI->posicaoSlots('g').' - posição livre para G</small></br>');
                //$prefix = 'P';
                //$pequenos = $CI->GetFranquiasSlots($prefix);

                //$num = $CI->numSlots($prefix); 
                //$slots = $CI->osSlots($prefix);

                //echo $num.'-'.$slots;
                //$ff = $CI->franquias_model->getSlotsFranq($this->session->userdata('id'));
                //$slp = json_decode($ff->p, true);
                //echo "</br>";
                //foreach ($slp as $key => $value) {
                //    echo "<table>
                //            <tbody>
                //                <tr>";
                //                    echo "<td></td> ";
                //                for($i = 1; $i <= count($value); $i++){
                //                    echo "<td class='letter-column'>";
                //                        echo $i;
                //                    echo "</td>"; 
                //                }
                //           echo "</tr>";
                //           echo "<tr>";
                //        echo "<td class='number-column'>".$key."</td>";
                //            for ($j=1; $j <= count($value); $j++) { 
                //                //$os = $CI->os_model->iconesEquiSlots($value[$j-1]);
                //                echo "<td class='p'>".$value[$j-1]."</td>";
                //            }    
                //        echo "</tr>";
                //    echo "</table>";
                //   
                //}               

                 ?>
                 
                <hr>
                <p>PEQUENOS / <a href="" class="btn btn-default btn-small">Limpar Slots</a></p>
                <?php
                    $prefix = 'P';
                    $pequenos = $CI->GetFranquiasSlots($prefix);
                    if ($pequenos > 0) :             
                ?>
                    <?php $CI->numSlotsOs($prefix); ?>

                <?php endif; ?>

                <hr>

                <p>MÉDIOS</p>
                <?php
                    $prefix = 'M';
                    $pequenos = $CI->GetFranquiasSlots($prefix);
                    if ($pequenos > 0) :             
                ?>
                    <?php $CI->numSlotsOs($prefix); ?>
                    
                <?php endif; ?>

                <hr>

                <p>GRANDES</p>
                <?php
                    $prefix = 'G';
                    $pequenos = $CI->GetFranquiasSlots($prefix);
                    if ($pequenos > 0) :             
                ?>
                    <?php $CI->numSlotsOs($prefix); ?>
                    
                <?php endif; ?>
              
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#table2 tr').each(function(){
        if($(this).find('td').length == 1){
            $(this).remove();
        }
    });
</script>