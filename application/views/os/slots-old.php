<?php 
$letras = array(
    0 => 'A',
    1 => 'B',
    2 => 'C',
    3 => 'D',
    4 => 'E',
    5 => 'F',
    6 => 'G',
    7 => 'H',
    8 => 'I',
    9 => 'J',
    10 => 'K',
    11 => 'L',
    12 => 'M',
    13 => 'N',
    14 => 'O',
    15 => 'P',
    16 => 'Q',
    17 => 'R',
    18 => 'S',
    19 => 'T',
    20 => 'U',
    21 => 'V',
    22 => 'X',
    23 => 'Y',
    24 => 'Z'
    );
 ?>
<style>
    .itembg1
    {
        width: 36px;
        height: 36px;
        background: #e0c9a9;
        padding: 0;
        /* padding-left: 2px; */
        border: 1px solid;
    }
    .slotname
    {
        margin-top: 7px;
        text-align: center;
    }
    .divimg
    {
        margin-top: -12px;
        position: absolute;
        margin-left: 8px;
    }
    .itembg1:hover {
        background-color: #c7ac87;
    }
    td.itembg1.G {
        height: 55px;
        background: #f1ddc1;
    }
    td.itembg1.G:hover {
        background: #e0c9a9;
    }
    .iconimg
    {
        border: 0px; 
        width: 20px;
        height: 20px;
    }
    @media(min-width:1200px){
        .container{width: 600px;}
    }
</style>
<?php
$CI = &get_instance();
function echoSlot($i, $breakline, $prefix, $os, $tipo, $name, $total, $parou = 0)
{
    echo '    <td class="itembg1 1 '.$prefix.'">';
    echo '        <div class="divimg">';                            
    if ($os != 0)
    {
        echo '            <a href="editar/'.$os.'">';
        
        echo '<img class="tip-bottom iconimg" src="'.base_url();
        switch ($tipo)
        {
            case 'celular': case 'Celular':   echo 'assets/img/cell.png"';      break;
            case 'tablet': case 'Tablet':     echo 'assets/img/tablet.png"';    break;
            case 'notebook': case 'Notebook':  echo 'assets/img/notebook.png"';  break;
            case 'computador': case 'Computador':  echo 'assets/img/pc.png"';  break;
            case 'videogame': case 'Videogame': echo 'assets/img/videogame.png"'; break;
            case 'tv': case 'TV': echo 'assets/img/tv.png"'; break;
            default:      echo 'assets/img/null"';      break;
        }                           
        echo 'data-original-title="Equipamento:&nbsp;'.$name.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$os.')">';                             
            echo '            </a>';
    }
    echo '        </div>';
    // echo '        <div class="slotname"> '.$prefix.($i+1).' </div>';
    echo '    </td>';
    
    if (((($i + 1) % $breakline) == 0))
        echo '</tr><tr><td style="font-size: 18px;font-weight: bold;text-align: center;color: #0088cc;">dd'.(ceil(((($i+1)+$parou)/($breakline))+1)).'</td>';
}
?>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
            <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon">
                                <i class="icon-sitemap"></i>
                        </span>
                        <h5>Slots</h5>
                    </div>
                <div class="widget-content nopadding">
                <br/>
                <?php
                $prefix = 'M';
                $cm = $CI->GetSlotMax($prefix);
                if ($cm > 0) :
                ?>
                <div class="container">
                    <h2><div style="background: #e0c9a9;width: 36px;border: 1px solid;height: 36px;display: inline-block;margin-right: 15px;"></div>SLOTS MÉDIOS</h2>
                    <h2><div style="background: #f1ddc1;width: 36px;border: 1px solid;height: 55px;display: inline-block;margin-right: 15px;"></div>SLOTS GRANDES</h2>
                </div>
                <script type="text/javascript">
                    $(window).scroll(function(){
                        var top = $('#letras').scrollTop();
                        if($(window).scrollTop() >= top){
                        }
                    });
                </script>
                <table id="table2" align="center">
                    <tbody>
                    <tr id="letras">
                        <td></td>
                        <?php for ($i = 0; $i < $franquia->fileiras_slots; $i++){ ?>
                        <td style="font-size: 18px;font-weight: bold;text-align: center;color: #0088cc; height: 36px; width: 36px;"><?php echo $letras[$i]; ?></td>
                        <?php } ?>
                    </tr>
                        <?php
                                                
                            echo '<tr><tr><tr>';
                            echo '<td style="font-size: 18px;font-weight: bold;text-align: center;color: #0088cc;">';
                            echo '1';
                            echo '</td>';
                            for ($i = 0; $i < $cm; $i++)
                            {
                                $array = $CI->GetSlotData($prefix, $i);
                                echoSlot($i, $franquia->fileiras_slots, $prefix, $array['os'], $array['tipo'], str_replace(' ','&nbsp;',$array['nome']), $cm);
                            }   
                            echo '</tr>';           
                        ?>
                        <!-- </tbody>
                    </table> -->
                <?php
                endif;
                $prefix = 'G';
                $cg = $CI->GetSlotMax($prefix);
                if ($cg > 0):
                ?>
                <!-- <br/>
                <center>
                    <h2>SLOTS GRANDES</h2>
                </center>
                <table id="table2" align="center">
                    <tbody> -->
                        <?php
                            $parou = $i;
                            echo '<td style="font-size: 18px;font-weight: bold;text-align: center;color: #0088cc;">';
                            echo ceil((($parou)/$franquia->fileiras_slots)+1);
                            echo '</td>';
                            for ($i = 0; $i < $cg; $i++)
                            {
                                $array = $CI->GetSlotData($prefix, $i);
                                echoSlot($i, $franquia->fileiras_slots, $prefix, $array['os'], $array['tipo'], str_replace(' ','&nbsp;',$array['nome']), $cm, $parou);
                            }   
                            echo '</tr>';           
                        ?>
                    </tbody>
                </table>
                <br/>
                <?php
                endif;
                ?>
            </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    $('#table2 tr').each(function(){
        if($(this).find('td').length == 1){
            $(this).remove();
        }
    });
</script>