<?php $tab = 0; if (isset($_GET['tab'])) $tab = $_GET['tab']; if ($tab > 4) $tab = 0; $totalS = 0; $totalP = 0; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.css" />

<style type="text/css">
    .popover-modal .popover-body{
        overflow: hidden;
        padding: 10px;
        text-align: left;
    }
    .popover-question{
        padding: 0px 5px;
        background: #0088cc;
        color: white;
        border-radius: 50%;
        font-size: 11px;
        margin-left: 5px;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">

            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span> 
            
                <h5>Editar #OS <?php echo $result->idOs; ?> <?php echo !empty($result->solicitou_garantia_os) ? " - Solicitou garantia de <a href=\"".base_url() . 'index.php/os/editar/'.$result->solicitou_garantia_os."\" alt=\"Abrir OS em nova aba\" target=\"_blank\"> #{$result->solicitou_garantia_os}</a>" : ""; ?></h5>
            </div>

            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li <?php if ($tab == 0) echo 'class="active"'; ?> id="tabDetalhes"><a href="#tab0" data-toggle="tab">Detalhes da OS</a></li>
                        <li <?php if ($tab == 1) echo 'class="active"'; ?> id="tabEquipamentos">
                            <a href="#tab1" data-toggle="tab">Equipamentos</a> 
                        </li>
                        <li>
                            <span class="popover-wrapper">
                              <a href="#" data-role="popover" data-target="popover-saida_tab1equi" class="popover-question"><strong>?</strong></a>
                              <div class="popover-modal popover-saida_tab1equi">
                                <div class="popover-body">
                                    <b>Equipamentos: </b>Informe aqui o equipamento que entrou para consertar.
                                </div>
                              </div>
                            </span>
                        </li>
                        <li <?php if ($tab == 2) echo 'class="active"'; ?> id="tabProdutos">
                            <a href="#tab2" data-toggle="tab">Componentes Utilizados</a>
                        </li>
                        <li>
                            <span class="popover-wrapper">
                                <a href="#" data-role="popover" data-target="popover-saida_tabCompUti" class="popover-question"><strong>?</strong></a>
                                <div class="popover-modal popover-saida_tabCompUti">
                                    <div class="popover-body">
                                        <b>Componentes Utilizados: </b>Informe quais peças esta usando nesse conserto, se estiver usando alguma peça.
                                    </div>
                                </div>
                            </span>
                        </li>
                        <li <?php if ($tab == 3) echo 'class="active"'; ?> id="tabServicos">
                            <a href="#tab3" data-toggle="tab">Serviços (Mão de obra)</a>
                        </li>
                        <li>
                            <span class="popover-wrapper">
                                <a href="#" data-role="popover" data-target="popover-saida_tabServicoMaos" class="popover-question"><strong>?</strong></a>
                                <div class="popover-modal popover-saida_tabServicoMaos">
                                    <div class="popover-body">
                                        <b>Serviços (Mão de obra): </b>Descreva qual serviço esta fazendo nesse aparelho e qual valor esta cobrando.
                                    </div>
                                </div>
                            </span>
                        </li>
                        <li <?php if ($tab == 4) echo 'class="active"'; ?> id="tabPagamentos">
                            <a href="#tab4" data-toggle="tab">Formas de Pagamento</a>
                        </li>
                        <li>
                            <span class="popover-wrapper">
                                <a href="#" data-role="popover" data-target="popover-saida_tabFormPagamento" class="popover-question"><strong>?</strong></a>
                                <div class="popover-modal popover-saida_tabFormPagamento">
                                    <div class="popover-body">
                                        <b>Formas de Pagamento: </b>Informe a forma de pagamento que combinou com seu cliente.
                                    </div>
                                </div>
                            </span>
                        </li>
                        <li <?php if ($tab == 5) echo 'class="active"'; ?> id="tabAnexos">
                            <a href="#tab5" data-toggle="tab">Anexos</a>
                        </li>
                        <li>
                            <span class="popover-wrapper right">
                                <a href="#" data-role="popover" data-target="popover-saida_tabAnexos" class="popover-question"><strong>?</strong></a>
                                <div class="popover-modal popover-saida_tabAnexos">
                                    <div class="popover-body">
                                        <b>Anexos: </b>Scanear Anexos como cópias de notas fiscais ou garantia.
                                    </div>
                                </div>
                            </span>
                        </li>
                        <li <?php if ($tab == 6) echo 'class="active"'; ?> id="tabObservacoesInternas">
                            <a href="#tab6" data-toggle="tab">Observações Internas</a>
                        </li>
                        <li>
                            <span class="popover-wrapper right">
                                <a href="#" data-role="popover" data-target="popover-saida_tabObsInternas" class="popover-question"><strong>?</strong></a>
                                <div class="popover-modal popover-saida_tabObsInternas">
                                    <div class="popover-body">
                                        <b>Observações Internas: </b>Observações internas que não aparecem na ordem de serviço, mas que pode ser usadas pelo setor técnico.
                                    </div>
                                </div>
                            </span>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <!-- TAB 0 -->
                        <div class="tab-pane<?php if ($tab == 0) echo ' active'; ?>" id="tab0">
                            <div class="span12" id="divCadastrarOs">

                                <form action="<?php echo current_url(); ?>" method="post" id="formOs" autocomplete="off">

                                    <?php echo form_hidden('idOs', $result->idOs) ?>
                                    
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6" style="margin-left: 0; position:relative;">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="span12" type="text" name="cliente" value="<?php echo $result->nomeCliente ?>"  />
                                            <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-cliente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Cliente"><i class="icon-plus icon-white"></i></a>
                                            <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value="<?php echo $result->clientes_id ?>"  />
                                            <input id="valorTotal" type="hidden" name="valorTotal" value=""  />
                                        </div>

                                        <div class="span6">
                                            <label for="tecnico">Técnico / Responsável
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_tecnico" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_tecnico">
                                                    <div class="popover-body">
                                                        Selecione o Técnico que fará a manutenção. Esse técnico já deve esta cadastrado previamente na guia de CONFIGURAÇÕES.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="tecnico" class="span12" type="text" name="tecnico" value="<?php echo $result->nome ?>"  />
                                            <input id="usuarios_id" class="span12" type="hidden" name="usuarios_id" value="<?php echo $result->usuarios_id ?>"  />
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="atendente">Atendente
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_atendente" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_atendente">
                                                    <div class="popover-body">
                                                        Selecione aqui o(a) atendente que realizou o atendimento inicial do cliente. Esse(a) atendente já deve esta cadastrado previamente na guia de CONFIGURAÇÕES. 
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="atendente" class="span12" type="text" name="atendente" value="<?php echo $result->nomeAtendente ?>"  />
                                            <input id="atendentes_id" class="span12" type="hidden" name="atendentes_id" value="<?php echo $result->atendentes_id ?>"  />
                                        </div>
                                    </div>
                                    
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span4">
                                            <input id="old_status" type="hidden" name="old_status" value="<?php echo $result->status ?>"  />
                                            <label for="status">Status<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_status" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_status">
                                                    <div class="popover-body">
                                                        Selecione aqui o STATUS no qual o aparelho esta entrando na lokja. Tente sempre atualizar esse status em tempo real para o cliente receber iinformações por e-mail e por Whatsapp.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>

                                            <select class="span12" name="status" id="status" value="">
                                                <option <?php if($result->status == 'Orçamento'){echo 'selected';} ?> value="Orçamento">Orçamento</option>
                                                <option <?php if($result->status == 'Aguardando Aprovacao'){echo 'selected';} ?> value="Aguardando Aprovacao">Aguardando Aprovação</option>
                                                <option <?php if($result->status == 'Aguardando Pecas'){echo 'selected';} ?> value="Aguardando Pecas">Aguardando Peças</option>
                                                <option <?php if($result->status == 'Em Andamento'){echo 'selected';} ?> value="Em Andamento">Em Andamento</option>
                                                <option <?php if($result->status == 'Finalizado'){echo 'selected';} ?> value="Finalizado">Finalizado</option>
                                                <option <?php if($result->status == 'Faturado'){echo 'selected';} ?> value="Faturado">Faturado</option>
                                                <option <?php if($result->status == 'Entregue Para o Cliente'){echo 'selected';} ?> value="Entregue Para o Cliente">Entregue Para o Cliente</option>
                                                <option <?php if($result->status == 'Saiu para a entrega'){echo 'selected';} ?> value="Saiu para a entrega">Saiu para a entrega</option>
                                                <option <?php if($result->status == 'Conserto Nao Realizado'){echo 'selected';} ?> value="Conserto Nao Realizado">Conserto Não Realizado</option>
                                                <option <?php if($result->status == 'Nao Aprovado'){echo 'selected';} ?> value="Nao Aprovado">Não Aprovado</option>
                                                <option <?php if($result->status == 'Cancelado'){echo 'selected';} ?> value="Cancelado">Cancelado</option>
                                                <option <?php if($result->status == 'Aprovado'){echo 'selected';} ?> value="Aprovado">Aprovado</option>                                                
                                                <option <?php if($result->status == 'Liberar Slot'){echo 'selected';} ?> value="Liberar Slot">Liberar Slot</option>                                                
                                                <option <?php if($result->status == 'Guardar Aparelho'){echo 'selected';} ?> value="Guardar Aparelho">Guardar Aparelho</option>                                                
                                            </select>

                                            <?php if(!empty($status)){ ?>
                                                <h6>Status Anteriores</h6>
                                                <?php foreach($status as $s){ ?>
                                                <label class="label label-primary" style="display: block; cursor: default; text-align: center;"><?php echo $s->status; ?> - <?php echo substr(date_usa2br($s->data), 0, 16); ?></label>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                        <div class="span3">
                                            <label for="dataInicial">Data Inicial<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_dtinicial" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_dtinicial">
                                                    <div class="popover-body">
                                                        Data que o aparelho esta entrando na loja.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="dataInicial" class="span12 datepicker" type="text" name="dataInicial" value="<?php echo date('d/m/Y', strtotime($result->dataInicial)); ?>"  />
                                        </div>

                                        <div class="span3">
                                            <label for="dataFinal">Data Final
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_dtfinal" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_dtfinal">
                                                    <div class="popover-body">
                                                        Informe a previsão de entrega do aparelho ou previsão de orçamento.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="dataFinal" class="span12 datepicker" type="text" name="dataFinal" value="<?php echo date('d/m/Y', strtotime($result->dataFinal)); ?>"  />
                                        </div>

                                        <div class="span2">
                                            <label for="garantia">Garantia<span class="required">*</span>
                                                <span class="popover-wrapper right">
                                                  <a href="#" data-role="popover" data-target="popover-saida_garantia" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_garantia">
                                                    <div class="popover-body">
                                                        Selecione o tempo de garantia, lembrando que por lei são 90 dias. Só escolha a opção SEM GARANTIA se o cliente trouxer a peça de outro lugar e você não souber a qualidade ou se esta funcionando ou não. Nesse caso relate isso na pate de OBSERVAÇÕES.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <select class="span12" name="garantia" id="garantia" value="">
                                                <option <?php if($result->garantia == '90 dias'){echo 'selected';} ?> value="90 dias">90 dias</option>
                                                <option <?php if($result->garantia == 'Sem Garantia'){echo 'selected';} ?> value="Sem Garantia">Sem Garantia</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="descricaoProduto">Descrição Produto/Serviço
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_descproduto" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_descproduto">
                                                    <div class="popover-body">
                                                        Descrição do serviço que será executado. Por exemplo: Manutenção da tela, manutenção do conector de carregador, Desoxidação, Atualização de Software etc.                                              
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="6" style="min-height: 147px;"><?php echo $result->descricaoProduto; ?></textarea>
                                        </div>
                                        <div class="span6">
                                            <label for="visita_data">Visita Técnica/Retirada e Entrega
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_visitaentrega" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_visitaentrega">
                                                    <div class="popover-body">
                                                        Se marcou para entregar o aparelho consertado, informe a data, faixa de horário para entregar, e valor para segunda tentativa de entrega caso na primeira não tenha ninguém para receber o aparelho.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <div  style="padding: 15px;border: 1px solid #d1d1d1;background: white;min-height: 114px;">

                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Agendamento para: </span>
                                                    <input style="    display: block;" id="visita_data" class="span12 datepicker" type="text" name="visita_data" value="<?php echo date('d/m/Y', strtotime(!empty($result->visita_data) ? $result->visita_data : date('Y-m-d'))); ?>"  />
                                                </div>

                                                <div style="display: inline-block; float: left;width: 15%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">De: </span>
                                                    <input style="display: block;" id="visita_hora1" class="span12" type="number" name="visita_hora1" value="<?php echo @$result->visita_hora1; ?>"  placeholder="horas"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 15%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;"> Até: </span>
                                                    <input style="display: block;" id="visita_hora2" class="span12" type="number" name="visita_hora2" value="<?php echo @$result->visita_hora2; ?>"  placeholder="horas"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 35%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Segunda tentativa:</span>
                                                    <input style="display: block;" id="visita_valor" class="span12 money" type="text" name="visita_valor" value="<?php echo @$result->visita_valor; ?>"  placeholder="Segunda tentativa 0,00"/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="backup">Aparelho de backup
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_backup" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_backup">
                                                    <div class="popover-body">
                                                        Se deixou algum aparelho para o cliente ir usando enquanto conserta o dele, informe os dados desse aparelho.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <div  style="padding: 15px;border: 1px solid #d1d1d1;background: white; min-height: 114px;">
                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style="  margin-bottom: 10px; display: inline-block;float: left;">Modelo: </span>
                                                    <input style="    display: inline-block;" id="backup_modelo" class="span12" type="text" name="backup_modelo" value="<?php echo @$result->backup_modelo; ?>"  />
                                                </div>

                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style="  margin-bottom: 10px; display: inline-block;float: left; margin-left: 8px;">IMEI: </span>
                                                    <input style="    display: inline-block;" id="backup_imei" class="span12" type="text" name="backup_imei" value="<?php echo @$result->backup_imei; ?>"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 42%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Preço de mercado:</span>
                                                    <input style="float: none;display: block; max-width: 70px;" id="backup_valor" class="span12 money" type="text" name="backup_valor" value="<?php echo @$result->backup_valor; ?>"  placeholder="0,00"/>
                                                    <span style="display: block;    "> Aparelho em perfeitas condições </span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <label for="laudoTecnico">Laudo Técnico
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_laudotecnico" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_laudotecnico">
                                                    <div class="popover-body">
                                                        Laudo técnico deverá ser usado principalmente quando o cliente solicitar, especialmente em casos onde o equipamento esta assegurado por uma seguradora ou em casos onde o conserto será pago por outra empresa por questão diversas.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5" style="min-height: 147px;"><?php echo $result->laudoTecnico ?></textarea>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span12 text-right">
                                            <label for="observacoes" class="text-left">Observações
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_obs" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_obs">
                                                    <div class="popover-body">
                                                        Escreva as observações desse equipamento, ou selecione no final desta guia.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="observacoes" id="observacoes" cols="30" rows="5"><?php echo $result->observacoes ?></textarea>
                                            
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_parelela2" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, <br> motivo falta de disponibilidade no mercado de peças.</label><input type="checkbox" name="paralela2" value="S" id="field_parelela2"><br></p>
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_queda" style="display: inline-block;margin-right: 6px;">Já sofreu queda ou pancada?</label><input type="checkbox" name="queda" value="S" id="field_queda"><br></p>
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_umidade" style="display: inline-block;margin-right: 6px;">Teve contato com a umidade?</label><input type="checkbox" name="umidade" value="S" id="field_umidade"><br></p>
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_telaandroid" style="display: inline-block;margin-right: 6px;">Troca de Tela de Android?</label><input type="checkbox" name="telaandroid" value="S" id="field_telaandroid"><br></p>
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_parelela1" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, motivo preço.</label><input type="checkbox" name="paralela1" value="S" id="field_parelela1"><br></p>
                                            <p style="display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_estufada" style="display: inline-block;margin-right: 6px;">Bateria está estufada?</label><input type="checkbox" name="estufada" value="S" id="field_estufada"><br></p>

                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <?php if($result->faturado == 0){ ?>
                                            <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                            <?php } ?>
                                            <button class="btn btn-primary" id="btnContinuar"><i class="icon-white icon-ok"></i> Alterar</button>
                                            <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                            <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!-- FIM TAB 0 -->

                        <!-- TAB 1 --> <!--Equipamentos-->
                        <div class="tab-pane<?php if ($tab == 1) echo ' active'; ?>" id="tab1">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <form id="formEquipamentos" action="<?php echo base_url() ?>index.php/os/adicionarEquipamento" method="post">
                                    <div class="span8" style="position:relative;" id="inputsdivequipamentos">
                                        <input type="hidden" name="idEquipamento" id="idEquipamento" />
                                        <input type="hidden" name="idOsEquipamento" id="idOsEquipamento" value="<?php echo $result->idOs?>" />
                                        <label for="">Equipamento</label>
                                        <input type="text" class="span12" name="equipamento" id="equipamento" placeholder="Digite o nome do equipamento" />
                                        <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-equipamento" role="button" data-toggle="modal" data-backdrop="static" class="btn btn-primary" title="Inserir Novo Equipamento"><i class="icon-plus icon-white"></i></a>
                                    </div>

                                    <div class="span2">
                                        <label for="">.</label>
                                            <button class="btn btn-success span12" id="btnAdicionarEquipamento"><i class="icon-white icon-plus"></i> Adicionar</button>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <?php if($result->faturado == 0){ ?>
                                            <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                            <?php } ?>
                                            <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                            <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            <div class="span12" id="divEquipamentos" style="margin-left: 0">
                                <table class="table table-bordered" id="tblEquipamentos">
                                    <thead>
                                        <tr>
                                            <th>Equipamento</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Numero de Série</th>
                                            <th>Defeito</th>
                                            <th>Slot</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                        foreach ($equipamentos as $e){
                                            if ($e->idEquipamentos) {
                                            echo '<script type="text/javascript">$(document).ready(function(){$("#inputsdivequipamentos").hide();$("#btnAdicionarEquipamento").hide();});</script>';
                                                                                            
                                            }
                                            $CI = &get_instance();
                                            /*$letras = array(
                                                    0 => 'A',
                                                    1 => 'B',
                                                    2 => 'C',
                                                    3 => 'D',
                                                    4 => 'E',
                                                    5 => 'F',
                                                    6 => 'G',
                                                    7 => 'H',
                                                    8 => 'I',
                                                    9 => 'J',
                                                    10 => 'K',
                                                    11 => 'L',
                                                    12 => 'M',
                                                    13 => 'N',
                                                    14 => 'O',
                                                    15 => 'P',
                                                    16 => 'Q',
                                                    17 => 'R',
                                                    18 => 'S',
                                                    19 => 'T',
                                                    20 => 'U',
                                                    21 => 'V',
                                                    22 => 'X',
                                                    23 => 'Y',
                                                    24 => 'Z'
                                                    );
                                            if ($e->slot < 0)
                                                $slot = '<font color="red"><strong>Atendimento encerrado</strong></font>';
                                            else{
                                               $slotSum = 0;
                                               if($CI->os_model->getSlotPrefix($e->idEquipamentos) == 'G')
                                                $slotSum = count($CI->os_model->getSlot('M'));
                                            $slot = $letras[$e->slot-(floor($e->slot/$franquia->fileiras_slots)*$franquia->fileiras_slots)].ceil(((($e->slot+1)+$slotSum)/($franquia->fileiras_slots)));

                                        }*/
                                            $marcaNome = $CI->os_model->getMarcaNome($e->marcas_id);
                                            $posicao = $CI->os_model->getSlotOcupado($e->equipamento, $e->idFranquia, $result->idOs);
                                            
                                        //echo '<pre>';
                                        //var_dump($e);
                                        //echo '</pre>';

                                        echo '<tr>';
                                        echo '<td>'.$e->equipamento.'</td>';                                                                                
                                        echo '<td>'.$marcaNome.'</td>';
                                        echo '<td>'.$e->modelo.'</td>';
                                        echo '<td>'.$e->mei.'</td>';
                                        echo '<td>'.$CI->getDefeito(json_decode($e->defeito)).'</td>';
                                        //echo '<td> $slot / posicao: '.$posicao.'</td>';
                                        echo '<td>'.$posicao.'</td>';
                                        echo '<td>';
                                        
                                        if($this->permission->checkPermission($this->session->userdata('permissao'),'eEquipamento'))
                                            echo '<a style="margin-right: 1%" href="'.base_url().'index.php/equipamentos/editar/'.$e->idEquipamentos.'" class="btn btn-info tip-top" title="Editar Equipamento"><i class="icon-pencil icon-white"></i></a>';
                                            echo '<a href="" idAcao="'.$e->idEquipamentos_os.'" equiAcao="'.$e->idEquipamentos.'"  marcaAcao="'.$e->marcas_id.'" modeloAcao="'.$e->modelo.'" title="Remover Equipamento da OS" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>';
                                            echo '</tr>';
                                        } ?>
                                   <tr>
                                   </tr>
                               </tbody>
                           </table>
                        </div>
                        <!-- FIM TAB 1 -->
                   </div>
                    <div class="tab-pane<?php if ($tab == 2) echo ' active'; ?>" id="tab2">
                    <div class="span12 well" style="padding: 1%; margin-left: 0">
                        <form id="formProdutos" action="<?php echo base_url() ?>index.php/os/adicionarProduto" method="post">
                            <div class="span8" style="position: relative;">
                                <input type="hidden" name="idProduto" id="idProduto" />
                                <input type="hidden" name="idOsProduto" id="idOsProduto" value="<?php echo $result->idOs?>" />
                                <input type="hidden" name="estoque" id="estoque" value=""/>
                                <input type="hidden" name="preco" id="preco" value=""/>
                                <label for="">Produto
                                    <span class="popover-wrapper">
                                      <a href="#" data-role="popover" data-target="popover-saida_inforProduto" class="popover-question"><strong>?</strong></a>
                                      <div class="popover-modal popover-saida_inforProduto">
                                        <div class="popover-body">
                                            Informe a peça que será utilizada neste serviço. (Ex: Telas, conectores, microfones e etc.) Se a peça não tiver cadastro, poderá cadastrar ela na hora clicando no botão de +
                                        </div>
                                      </div>
                                    </span>
                                </label>
                                <input type="text" class="span12" name="produto" id="produto" placeholder="Digite o nome do produto" />
                                <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-componente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Produto"><i class="icon-plus icon-white"></i></a>
                                <a style="position: absolute;top: 25px;right: 57px;height: 25px;line-height: 25px;" href="#modal-codigo-barras" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="btn btn-warning" title="Escanear código de barras" onclick="modalCodigo();"><i class="icon-barcode icon-white"></i></a>
                            </div>
                            <div class="span2">
                                <label for="">Quantidade
                                    <span class="popover-wrapper">
                                      <a href="#" data-role="popover" data-target="popover-saida_descpagamento" class="popover-question"><strong>?</strong></a>
                                      <div class="popover-modal popover-saida_descpagamento">
                                        <div class="popover-body">
                                            Informar a quantidade dessa peça que irá usar no conserto. Em seguida clique em +Adicionar.
                                        </div>
                                      </div>
                                    </span>
                                </label>
                                <input type="text" placeholder="Quantidade" id="quantidade" name="quantidade" class="span12" />
                            </div>
                            <div class="span2">
                                <label for="">.</label>
                                <button class="btn btn-success span12" id="btnAdicionarProduto"><i class="icon-white icon-plus"></i> Adicionar</button>
                            </div>
                            <div class="span12" style="padding: 1%; margin-left: 0">
                                <div class="span6 offset3" style="text-align: center">
                                    <?php if($result->faturado == 0){ ?>
                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                    <?php } ?>
                                    <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                    <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="span12" id="divProdutos" style="margin-left: 0">
                        <table class="table table-bordered" id="tblProdutos">
                            <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Quantidade</th>
                                    <th>Ações</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($produtos as $p) {
                                    $total = $total + $p->subTotal;
                                    echo '<tr>';
                                    echo '<td>'.$p->descricao.'</td>';
                                    echo '<td>'.$p->quantidade.'</td>';
                                    echo '<td><a href="" idAcao="'.$p->idProdutos_os.'" prodAcao="'.$p->idProdutos.'" quantAcao="'.$p->quantidade.'" title="Excluir Produto" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                    echo '<td>R$ '.number_format($p->subTotal,2,',','.').'</td>';
                                    echo '</tr>';
                                }?>
                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($total,2,',','.');?><input type="hidden" id="total-venda" value="<?php echo number_format($total,2); ?>"></strong></td>
                                </tr>
                            </tbody>
                            <?php $totalP = $total; ?>
                        </table>
                    </div>
                </div>

                <!--Serviços-->
                <div class="tab-pane<?php if ($tab == 3) echo ' active'; ?>" id="tab3">
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <form id="formServicos" action="<?php echo base_url() ?>index.php/os/adicionarServico" method="post">
                                <div class="span10">
                                    <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs?>" />
                                    <input type="hidden" name="idServico" id="idServico"/>
                                    <label for="servico" class="control-label">Nome<span class="required">*</span>
                                        <span class="popover-wrapper">
                                          <a href="#" data-role="popover" data-target="popover-saida_nomesmaodeobras" class="popover-question"><strong>?</strong></a>
                                          <div class="popover-modal popover-saida_nomesmaodeobras">
                                            <div class="popover-body">
                                                Informe o nome do serviço que será feito (Ex: Manutenção da tela, manutenção do conector de carregador, atualização de software etc.)
                                            </div>
                                          </div>
                                        </span>
                                    </label>
                                    <input id="servico" type="text" class="span12 servico" name="servico" descricaoServico="descricaoServico" placeholder="Digite o nome do serviço" value="<?php echo !empty($result->descricaoProduto) ? $result->descricaoProduto : ""; ?>" />
                                    <div class="span12" style="padding: 0%; margin-left: 0">
                                      <div class="span2" style="padding: 0%; margin-left: 0; width: auto;">
                                        <label for="precoServico" class="control-label">Preço<span class="required">*</span>
                                            <span class="popover-wrapper">
                                              <a href="#" data-role="popover" data-target="popover-saida_servicopreco" class="popover-question"><strong>?</strong></a>
                                              <div class="popover-modal popover-saida_servicopreco">
                                                <div class="popover-body">
                                                    Informe o preço desse serviço. LEMBRANDO: Esse valor será somando na peça, se houver peças selecionadas.
                                                </div>
                                              </div>
                                            </span>
                                        </label>
                                        <input id="precoServico" name="precoServico" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span9" style="padding-left: 1%; margin-left: 0;">
                                        <label for="descricaoServico" class="control-label">Descrição
                                            
                                        </label>
                                        <input id="descricaoServico" name="descricaoServico" type="text" style="width: 98%" name="descricao" placeholder="Digite a descrição do serviço" value="<?php echo !empty($result->descricaoProduto) ? $result->descricaoProduto : ""; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <label for="">.</label>
                                <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                            </div>
                            <div class="span12" style="padding: 1%; margin-left: 0">
                                <div class="span6 offset3" style="text-align: center">
                                    <?php if($result->faturado == 0){ ?>
                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                    <?php } ?>
                                    <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                    <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="span12" id="divServicos" style="margin-left: 0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Serviço (Mão de Obra)</th>
                                    <th>Descrição</th>
                                    <th>Sub-total</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($servicos as $s) {
                                    $preco = $s->precoServico;
                                    $total = $total + $preco;
                                    echo '<tr>';
                                    echo '<td>'.$s->nomeServico.'</td>';
                                    echo '<td>'.$s->descricaoServico.'</td>';
                                    echo '<td>R$ '.number_format($s->precoServico,2,',','.').'</td>';
                                    echo '<td><span idAcao="'.$s->idServicos_os.'" title="Excluir Serviço" class="btn btn-danger"><i class="icon-remove icon-white"></i></span></td>';
                                    echo '</tr>';
                                }?>
                                <tr>
                                    <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($total,2,',','.');?><input type="hidden" id="total-servico" value="<?php echo number_format($total,2); ?>"></strong></td>
                                </tr>
                            </tbody>
                            <?php $totalS = $total; ?>
                        </table>
                    </div>
                </div>
            </div>
            <!--Pagamentos-->
            <div class="tab-pane<?php if ($tab == 4) echo ' active'; ?>" id="tab4">
                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span12 well" style="padding: 1%; margin-left: 0">
                        <form id="formPagamentos" action="<?php echo base_url() ?>index.php/os/adicionarPagamento" method="post">
                            <div class="span10"> 
                                <input type="hidden" name="idOsPagamento" id="idOsPagamento" value="<?php echo $result->idOs?>" />       
                                <label for="formaPg">Data<span class="required">*</span>
                                    <span class="popover-wrapper">
                                      <a href="#" data-role="popover" data-target="popover-saida_dataPagamentoPag" class="popover-question"><strong>?</strong></a>
                                      <div class="popover-modal popover-saida_dataPagamentoPag">
                                        <div class="popover-body">
                                            Data que será feito este pagamento, campo automático.
                                        </div>
                                      </div>
                                    </span>
                                </label>
                                <input id="data" class="span12 datepicker" type="text" name="data" value="<?php echo date('d/m/Y'); ?>">
                                <label for="formaPg">Forma de Pagamento<span class="required">*</span>
                                    <span class="popover-wrapper">
                                      <a href="#" data-role="popover" data-target="popover-saida_formPagamentopag" class="popover-question"><strong>?</strong></a>
                                      <div class="popover-modal popover-saida_formPagamentopag">
                                        <div class="popover-body">
                                            Selecione a forma de pagamento que combinou com esse cliente, principalmente se deu algum desconto para ele pagar a vista em dinheiro.
                                        </div>
                                      </div>
                                    </span>
                                </label>
                                <select name="formaPg" id="formaPg" class="span12">
                                   <option value="Dinheiro">Dinheiro</option>
                                   <option value="Cartão de Crédito">Cartão de Crédito</option>
                                   <option value="Cartão de Débito">Cartão de Débito</option>
                                   <option value="Desconto">Entrada</option>
                                   <option value="Boleto">Boleto</option>
                               </select>         
                               <div class="span12" style="padding: 0%; margin-left: 0">
                                    <div class="span2" style="padding: 0%; margin-left: 0; width: auto;">
                                        <label for="valorPg" class="control-label">Valor/Entrada<span class="required">*</span>
                                            <span class="popover-wrapper">
                                              <a href="#" data-role="popover" data-target="popover-saida_valorentrada" class="popover-question"><strong>?</strong></a>
                                              <div class="popover-modal popover-saida_valorentrada">
                                                <div class="popover-body">
                                                    Informe qunato o cliente vai pagar naquele momento. Total ou um sinal para começar o serviço.
                                                </div>
                                              </div>
                                            </span>
                                        </label>
                                        <input id="valorPg" name="valorPg" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span2" style="padding-left: 1%; margin-left: 0; width: auto;">
                                        <label for="valorDesc" class="control-label">Desconto
                                            <span class="popover-wrapper">
                                              <a href="#" data-role="popover" data-target="popover-saida_descpagamento" class="popover-question"><strong>?</strong></a>
                                              <div class="popover-modal popover-saida_descpagamento">
                                                <div class="popover-body">
                                                    Informe se deu algum desconto em reais e não em porcentagem.
                                                </div>
                                              </div>
                                            </span>
                                        </label>
                                        <input id="valorDesc" name="valorDesc" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span6" style="padding-left: 1%; margin-left: 0;">
                                        <label for="dadosPg" class="control-label">Observações
                                            <span class="popover-wrapper">
                                              <a href="#" data-role="popover" data-target="popover-saida_descpagamento" class="popover-question"><strong>?</strong></a>
                                              <div class="popover-modal popover-saida_descpagamento">
                                                <div class="popover-body">
                                                    Informe alguma observação sobre esse pagamento (Ex: Restante no dia de entrega, Boleto da empresa X etc.)
                                                </div>
                                              </div>
                                            </span>
                                        </label>
                                        <input id="dadosPg" name="dadosPg" type="text" style="width: 98%" name="descricao" placeholder="Digite dados do pagamento" />
                                        <p style="display: inline-block;padding: 0 5px;">
                                            <label for="field_obs_faturado" style="display: inline-block;margin-right: 6px;display: inline-block;padding: 0 5px;">Faturado</label><input type="radio" name="obs_radio" value="obs_faturado" id="field_obs_faturado"><br>
                                        </p>
                                        <p style="display: inline-block;padding: 0 5px;">
                                            <label for="field_obs_retirada" style="display: inline-block;margin-right: 6px;display: inline-block;padding: 0 5px;">Restante na retirada do equipamento</label><input type="radio" name="obs_radio" value="obs_retirada" id="field_obs_retirada"><br>
                                        </p>
                                        <p style="display: inline-block;padding: 0 5px;">
                                            <label for="field_obs_entrega" style="display: inline-block;margin-right: 6px;display: inline-block;padding: 0 5px;">Restante na entrega do equipamento</label><input type="radio" name="obs_radio" value="obs_entrega" id="field_obs_entrega"><br>
                                        </p>
                                    </div>
                            </div>
                        </div>
                        <div class="span2">
                            <label for="">.</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                        </div>
                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <?php echo '<a title="" href="'.base_url().'agile/gerar-nfe.php?idOs='.$result->idOs.'&idServicos='.(@$servicos[0]->idServicos_os).'&idFranquia='.$this->session->userdata('id').'" class="btn btn-warning" target="_blank"><i class="icon-file icon-white"></i> Gerar NF-e</a>'; ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="span12" id="divPagamentos" style="margin-left: 0">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Forma de pagamento</th>
                                <th>Dados</th>
                                <th>Valor</th>
                                <th>Desconto</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            foreach ($pagamentos as $p)
                            {
                                $total += $p->valorPg;
                                $total -= $p->valorDesc;
                                echo '<tr>';
                                echo '<td>'.$p->formaPg.'</td>';
                                echo '<td>'.$p->dadosPg.'</td>';
                                echo '<td>R$ '.number_format($p->valorPg,2,',','.').'</td>';
                                echo '<td>R$ '.number_format($p->valorDesc,2,',','.').'</td>';
                                echo '<td><span idAcao="'.$p->idPagamento_os.'" title="Excluir Pagamento" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></span></td>';
                                echo '</tr>';
                            }?>
                            <tr>
                                <td colspan="2" style="text-align: right"><strong>Total em aberto:</strong></td>
                                <td><strong>R$ <?php echo number_format($total,2,',','.');?><input type="hidden" id="total-pagamento" value="<?php echo number_format($total,2); ?>"></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <!--Anexos-->
        <div class="tab-pane<?php if ($tab == 5) echo ' active'; ?>" id="tab5">
            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12 well" style="padding: 1%; margin-left: 0" id="form-anexos">
                    <form id="formAnexos" enctype="multipart/form-data" action="javascript:;" accept-charset="utf-8"s method="post">
                        <div class="span10">
                            <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs?>" />
                            <label for="">Anexo 
                                <span class="popover-wrapper">
                                  <a href="#" data-role="popover" data-target="popover-saida_anexodesc" class="popover-question"><strong>?</strong></a>
                                  <div class="popover-modal popover-saida_anexodesc">
                                    <div class="popover-body">
                                        Use esta opção se quiser adicionar algum documento como nota fiscal ou termo de garantia para deixar arquivado com a OS.
                                    </div>
                                  </div>
                                </span>
                            </label>
                            <input type="file" class="span12" name="userfile[]" multiple size="20" />
                        </div>
                        <div class="span2">
                            <label for="">.</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Anexar</button>
                        </div>
                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="span12" id="divAnexos" style="margin-left: 0">
                    <?php 
                        $cont = 1;
                        $flag = 5;
                        foreach ($anexos as $a):

                            if($a->thumb == null){
                                $thumb = base_url().'assets/img/icon-file.png';
                                $link = base_url().'assets/img/icon-file.png';
                            }
                            else{
                                $thumb = base_url().'assets/anexos/thumbs/'.$a->thumb;
                                $link = $a->url.$a->anexo;
                            }

                            if($cont == $flag){
                                echo '<div style="margin-left: 0" class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                                $flag += 4;
                            }
                            else{
                                echo '<div class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                            }

                            $cont++;

                        endforeach; 
                    ?>
                </div>
            </div>
        </div>
        <!--Fim Anexos-->

        <!--Observações Internas-->
        <div class="tab-pane<?php if ($tab == 6) echo ' active'; ?>" id="tab6">
            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12 well" style="padding: 1%; margin-left: 0" id="form-observacoes-internas">
                    <form id="formObservacoesInternas" action="javascript:;" accept-charset="utf-8"s method="post">

                        <div class="span8">
                            <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs ?>" />
                            <input type="hidden" name="img01" id="observacoes_internas_img01"/>
                            <input type="hidden" name="img02" id="observacoes_internas_img02"/>
                            <input type="hidden" name="img03" id="observacoes_internas_img03"/>
                            <input type="hidden" name="img04" id="observacoes_internas_img04"/>
                            <input type="hidden" name="img05" id="observacoes_internas_img05"/>
                            <input type="hidden" name="img06" id="observacoes_internas_img06"/>

                            <label for="observacoes_internas">Observações Internas</label>
                            <input id="observacoes_internas" type="text" class="span12" name="observacoes_internas" size="160" />
                        </div>

                        <div class="span2">
                            <a style="position: relative;top: 24px;height: 25px;line-height: 25px;" href="#modal-inserir-fotos" role="button" data-toggle="modal" data-backdrop="static" class="btn btn-primary" title="Anexar fotos"><i class="icon-camera icon-white"></i></a>
                        </div>

                        <div class="span2">
                            <label for="">&nbsp;</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                        </div>

                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>

                    </form>
                </div>
                
                <div class="span12" id="divObservacoesInternas" style="margin-left: 0">
                    <?php 
                        $cont = 1;
                        $flag = 5;
                        foreach ($observacoesInternas as $obs_interna):

                            $fotos = '';
                            if(!empty($obs_interna->fotos)){
                                foreach ($obs_interna->fotos as $v){
                                    $fotos .= '<a href="'.base_url().'application/controllers/reisdev-api/cam-api/saved_img/'.$v->arquivo.'" data-fancybox="anexos_'.$obs_interna->id.'"><img style="width: 40px;margin-left: 10px;" src="'.base_url().'application/controllers/reisdev-api/cam-api/saved_img/'.$v->arquivo.'" ></a>';
                                }
                            }

                            echo '<p><span style="display:block-inline;width:150px;">' . (!empty($obs_interna->usuario) ? $obs_interna->usuario.' em ' : '') .date(('d/m/Y H:i'), strtotime($obs_interna->data)) . ' - </span>' . $obs_interna->observacao_interna.$fotos. '</p>';
                            //echo $obs_interna->observacao_interna . ' - ' . date(('d/m/Y H:i'), strtotime($obs_interna->data)) . ' <br>';

                        //     if($a->thumb == null){
                        //         $thumb = base_url().'assets/img/icon-file.png';
                        //         $link = base_url().'assets/img/icon-file.png';
                        //     }
                        //     else{
                        //         $thumb = base_url().'assets/anexos/thumbs/'.$a->thumb;
                        //         $link = $a->url.$a->anexo;
                        //     }

                        //     if($cont == $flag){
                        //         echo '<div style="margin-left: 0" class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                        //         $flag += 4;
                        //     }
                        //     else{
                        //         echo '<div class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                        //     }

                        //     $cont++;

                        endforeach; 
                    ?>
                </div>
            </div>
        </div>
        <!--Fim Observações Internas-->
        
    </div>
</div>
.
</div>
</div>
</div>
</div>
<div id="modal-inserir-cliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/clientes/inserirClienteAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Cliente</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="nomeCliente" placeholder="Nome do cliente">
            <input type="text" name="rgCliente" placeholder="RG">
            <input type="text" name="telefoneCliente" placeholder="Telefone">
            <input type="text" name="emailCliente" placeholder="Email">
            <input type="text" name="cepCliente" placeholder="CEP">
            <input type="text" name="ruaCliente" placeholder="Rua">
            <input type="text" name="numeroCliente" placeholder="Número">
            <input type="text" name="bairroCliente" placeholder="Bairro">
            <input type="text" name="cidadeCliente" placeholder="Cidade">
            <input type="text" name="estadoCliente" placeholder="Estado">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnInserirClienteAjax">Inserir</button>
        </div>
    </form>
</div>
<!-- Modal excluir produto -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>
<div id="modal-inserir-equipamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: auto;max-width: none;margin-left: 0;left: 0;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Eqluipamento</h5>
        </div>
        <div class="modal-body" style="overflow: hidden;">
            <iframe src="<?php echo site_url('/equipamentos/adicionar/1'); ?>" id="iframe-add_equipamento" height="500" width="900"></iframe>
       </div>
       <div class="modal-footer">
            <button style="float: right;" class="btn" data-dismiss="modal" id="modal-inserir-equipamento-button" aria-hidden="true" onclick="adicionar_equipamento_modal();" disabled>Voltar a OS</button>
            <div class="span5" style="margin-left: 0;float: right;width: 600px;text-align: right;padding-right: 15px;box-sizing: border-box;">
                <label for="button_confirm" style="/* color: white; */display: inline-block;">Fiz as alterações necessárias e cliquei no botão salvar no final dessa tela.</label>
                <input id="button_confirm" type="radio" name="button_confirm" value="button_confirm" onchange="$('#modal-inserir-equipamento-button').attr('disabled', false);"> 
            </div>
     </div>
</div>
<div id="modal-inserir-fotos" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: auto;max-width: none;margin-left: 0;left: 0;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Anexar fotos da câmera</h5>
        </div>
        <div class="modal-body" style="overflow: hidden; max-height: none;">
            <iframe src="<?php echo site_url('/os/obs_addFoto/'.$result->idOs); ?>" id="iframe-add_fotos" height="1000" width="900"></iframe>
       </div>
       <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="adicionar_fotos_modal();">Anexar fotos</button>
     </div>
</div>
<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Visualizar Anexo</h3>
</div>
<div class="modal-body">
    <div class="span12" id="div-visualizar-anexo" style="text-align: center">
        <div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
    <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
    <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
</div>
</div>
<!-- Modal Faturar-->
<div id="modal-faturar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formFaturar" action="<?php echo current_url() ?>" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Faturar Venda</h3>
      </div>
      <div class="modal-body">
        <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>
        <div class="span12" style="margin-left: 0"> 
          <label for="descricao">Descrição</label>
          <input class="span12" id="descricao" type="text" name="descricao" value="Fatura de Venda - #<?php echo $result->idOs; ?> "  />
      </div>  
      <div class="span12" style="margin-left: 0"> 
          <div class="span12" style="margin-left: 0"> 
            <label for="cliente">Cliente*</label>
            <input class="span12" id="cliente" type="text" name="cliente" value="<?php echo $result->nomeCliente ?>" />
            <input type="hidden" name="clientes_id" id="clientes_id" value="<?php echo $result->clientes_id ?>">
            <input type="hidden" name="os_id" id="os_id" value="<?php echo $result->idOs; ?>">
        </div>
    </div>
    <div class="span12" style="margin-left: 0"> 
      <div class="span4" style="margin-left: 0">  
        <label for="valor">Valor*</label>
        <input type="hidden" id="tipo" name="tipo" value="receita" /> 
        <input class="span12 money" id="valor" type="text" name="valor" value="<?php echo number_format($total,2); ?> "  />
    </div>
    <div class="span4" >
        <label for="vencimento">Data Vencimento*</label>
        <input class="span12 datepicker" id="vencimento" type="text" name="vencimento"  />
    </div>
</div>
<div class="span12" style="margin-left: 0"> 
  <div class="span4" style="margin-left: 0">
    <label for="recebido">Recebido?</label>
    &nbsp &nbsp &nbsp &nbsp <input  id="recebido" type="checkbox" name="recebido" value="1" /> 
</div>
<div id="divRecebimento" class="span8" style=" display: none">
    <div class="span6">
      <label for="recebimento">Data Recebimento</label>
      <input class="span12 datepicker" id="recebimento" type="text" name="recebimento" /> 
  </div>
  <div class="span6">
      <label for="formaPgto">Forma Pgto</label>
      <select name="formaPgto" id="formaPgto" class="span12">
        <option value="Dinheiro">Dinheiro</option>
        <option value="Cartão de Crédito">Cartão de Crédito</option>
        <option value="Cheque">Cheque</option>
        <option value="Boleto">Boleto</option>
        <option value="Depósito">Depósito</option>
        <option value="Débito">Débito</option>        
    </select> 
</div>
</div>
</div>
<div class="modal-footer">
  <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
  <button class="btn btn-primary">Faturar</button>
</div>
</form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".servico").on("input", function(){
            var text1 = $(this).val();
            var text2 = $(this).attr("descricaoServico");
            $("#"+text2).val(text1); 
        });
    });
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.js"></script>
<script type="text/javascript">
    $('[data-role="popover"]').popover({
        trigger: 'hover' // click | hover
    });
    //$(document).ready(function(){
    //    $("#equipamento").change(function(){
    //        nomeEquipamento = $(this).find('option:selected').attr('value');
    //        $("#tipo").val(nomeEquipamento);
    //    });
    //
    //}); 
</script>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#marca").autocomplete({
            source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
            minLength: 1,
            select: function( event, ui ) {
                $("#componenteMarcas_id").val(ui.item.id);
            }
        });

        $("#equip_marca").keyup(function(e){
            if($(this).val() != ""){
                $(this).autocomplete({
                    source: "<?php echo site_url(); ?>/equipamentos/autoCompleteMarca",
                    minLength: 1,
                    select: function( event, ui) {
                        if(ui.item.id != undefined){
                            $("#equip_marcas_id").val(ui.item.id);
                        }else{
                            $("#equip_marcas_id").val("");
                        }
                    }
                });
            }else{
                $("#equip_marcas_id").val("");
            }
        });
         
        $('#recebido').click(function(event) {
            var flag = $(this).is(':checked');
            if(flag == true){
                $('#divRecebimento').show();
            }
            else{
                $('#divRecebimento').hide();
            }
        });

        $(document).on('click', '#btn-faturar', function(event) {
            event.preventDefault();
            valor = $('#total-venda').val();
            total_servico = $('#total-servico').val();
            valor = valor.replace(',', '' );
            total_servico = total_servico.replace(',', '' );
            total_servico = parseFloat(total_servico); 
            valor = parseFloat(valor);
            $('#valor').val(valor + total_servico);
        });

        $("#formFaturar").validate({
            rules:{
                descricao: {required:true},
                cliente: {required:true},
                valor: {required:true},
                vencimento: {required:true}
            },
            messages:{
                descricao: {required: 'Campo Requerido.'},
                cliente: {required: 'Campo Requerido.'},
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            },
            submitHandler: function( form ) {

                var dados = $( form ).serialize();
                $('#btn-cancelar-faturar').trigger('click');
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/faturar",
                    data: dados,
                    dataType: 'json',
                    success: function(data)
                    {
                    if(data.result == true){
                        window.location.reload(true);
                    }
                    else{
                        alert('Ocorreu um erro ao tentar faturar OS.');
                        $('#progress-fatura').hide();
                    }
                }

                });
                return false;
            }
        });

        $("#produto").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteProduto",
            minLength: 2,
            select: function( event, ui ) {
               $("#idProduto").val(ui.item.id);
               $("#estoque").val(ui.item.estoque);
               $("#preco").val(ui.item.preco);
               $("#quantidade").focus();
            }
        });

        $("#equipamento").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteEquipamento",
            minLength: 2,
            select: function( event, ui ) {
                $("#idEquipamento").val(ui.item.id);
            }
        });

        $("#servico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteServico",
            minLength: 2,
            select: function( event, ui )
            {
                $("#idServico").val(ui.item.id);
                $("#precoServico").val(ui.item.preco);
                $("#descricaoServico").val(ui.item.descricao);
            }
        });

        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 2,
            select: function( event, ui ) {
               $("#clientes_id").val(ui.item.id);
            }
        });

        $("#tecnico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteUsuario",
            minLength: 2,
            select: function( event, ui ) {
               $("#usuarios_id").val(ui.item.id);
            }
        });
        
        $("#atendente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteAtendente",
            minLength: 1,
            select: function( event, ui ){
                $("#atendentes_id").val(ui.item.id);
            }
        });

        $("#formOs").validate({
            rules:{
                cliente: {required:true},
                dataInicial: {required:true},
                garantia: {required:true}
            },
            messages:{
                cliente: {required: 'Campo Requerido.'},
                dataInicial: {required: 'Campo Requerido.'},
                garantia: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formPagamentos").validate({
            rules:
            {
                formaPg: {required:true},
                valorPg: {required:true}
            },
            messages:{
                formaPg: {required: 'Insira uma forma de pagamento'},
                valorPg: {required: 'Insira o valor do pagamento'}
            },
            submitHandler: function( form )
            {       
                var dados = $( form ).serialize();
                $("#divPagamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/adicionarPagamento",
                    data: dados,
                    dataType: 'json',
                    success: function(data)
                    {
                        if(data.result == true){
                            $( "#divPagamentos" ).load("<?php echo current_url();?> #divPagamentos" );
                            $("#dadosPg").val('');
                            $("#valorPg").val('');
                            $("#formaPg").focus();
                        }
                        else{
                            alert('Ocorreu um erro ao tentar adicionar serviço.');
                        }
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules:{
                quantidade: {required:true} 
            },
            messages:{
                quantidade: {required: 'Insira a quantidade'}
            },
            submitHandler: function( form ){

                var quantidade = parseInt($("#quantidade").val());
                var estoque = parseInt($("#estoque").val());

                if(estoque < quantidade){

                    alert('Você não possui estoque suficiente.');

                } else {

                    var dados = $( form ).serialize();
                    
                    $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/os/adicionarProduto",
                        data: dados,
                        dataType: 'json',
                        success: function(data) {
                            if(data.result == true){
                                $( "#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                                $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                                $("#quantidade").val('');
                                $("#produto").val('').focus();
                            }
                            else{
                                alert('Ocorreu um erro ao tentar adicionar produto.');
                            }
                        }
                    });
                    
                    return false;
                }
            }
        });

        $("#formEquipamentos").validate({
          rules:{
           equipamento: {required:true} 
       },
       messages:{
           equipamento: {required: 'Insira o nome do equipamento'}
       },
       submitHandler: function( form )
       {
          var dados = $( form ).serialize();
          $("#divEquipamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
          $.ajax(
          {
              type: "POST",
              url: "<?php echo base_url();?>index.php/os/adicionarEquipamento",
              data: dados,
              dataType: 'json',
              success: function(data)
              {
                console.log(data);
                if(data.result == true)
                {
                 $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                 $("#equipamento").val('');
                 $("#marca_id").val('');
                 //alert("OS criada com sucesso!");
                 /*window.location = "<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>";*/
                $("#btnAdicionarEquipamento").hide();
                $("#inputsdivequipamentos").hide();
             }
             else
             {
                 alert("Não há mais espaço para adicionar um equipamento, favor liberar para continuar adicionando;");
                 $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                 $("#equipamento").val('');
                 $("#marca_id").val('');
             }
            //alert("OS criada com sucesso!");
            /*
            window.location = "<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>";*/
         },
         error: function(){
             $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
             $("#equipamento").val('');
             $("#marca_id").val('');
         }
     });
          return false;
      }
  });
        $("#formServicos").validate({
          rules:{
           servico: {required:true},
           precoServico: {required:true}
       },
       messages:{
           servico: {required: 'Insira um serviço'},
           precoServico: {required: 'Insira o valor do serviço'}
       },
       submitHandler: function( form ){       
           var dados = $( form ).serialize();
           $("#divServicos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
           $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>index.php/os/adicionarServico",
              data: dados,
              dataType: 'json',
              success: function(data)
              {
                if(data.result == true){
                    $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    $( "#divServicos" ).load("<?php echo current_url();?> #divServicos" );
                    $("#precoServico").val('');
                    $("#descricaoServico").val('');
                    $("#servico").val('').focus();
                }
                else{
                    alert('Ocorreu um erro ao tentar adicionar serviço.');
                }
            }
        });
           return false;
       }
   });

        $("#formAnexos").validate({
          submitHandler: function( form ){       
                //var dados = $( form ).serialize();
                var dados = new FormData(form); 
                $("#form-anexos").hide('1000');
                $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/anexar",
                  data: dados,
                  mimeType:"multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData:false,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divAnexos" ).load("<?php echo current_url();?> #divAnexos" );
                        $("#userfile").val('');
                    }
                    else{
                        $("#divAnexos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> '+data.mensagem+'</div>');      
                    }
                },
                error : function() {
                  $("#divAnexos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');      
              }
          });
                $("#form-anexos").show('1000');
                return false;
            }
        });

        $("#formObservacoesInternas").validate({
            submitHandler: function( form ){       
                //var dados = $( form ).serialize();
                var dados = new FormData(form); 
                $("#form-observacoes-internas").hide('1000');
                $("#divObservacoesInternas").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/os/adicionarObservacoesInternas",
                    data: dados,
                    // mimeType:"multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'json',
                    success: function(data) {
                        if(data.result == true){
                            $("#divObservacoesInternas" ).load("<?php echo current_url();?> #divObservacoesInternas" );
                            $("#observacoes_internas").val(null);
                            $("#observacoes_internas_img01").val(null);
                            $("#observacoes_internas_img02").val(null);
                            $("#observacoes_internas_img03").val(null);
                            $("#observacoes_internas_img04").val(null);
                            $("#observacoes_internas_img05").val(null);
                            $("#observacoes_internas_img06").val(null);
                        }
                        else{
                            $("#divObservacoesInternas").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> '+data.mensagem+'</div>');      
                        }
                    },
                    error : function() {
                        $("#divObservacoesInternas").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você informou a observação.</div>');      
                    }
                });
                $("#form-observacoes-internas").show('1000');
                return false;
            }
        });


        $(document).on('click', '#divEquipamentos a', function(event) {
            var idEquipamento = $(this).attr('idacao');
            if((idEquipamento % 1) == 0)
            {
                $("#divEquipamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirEquipamento",
                  data: "idEquipamento="+idEquipamento,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true)
                    {
                        $( "#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir equipamento.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '#divProdutos a', function(event) {
            var idProduto = $(this).attr('idAcao');
            var quantidade = $(this).attr('quantAcao');
            var produto = $(this).attr('prodAcao');
            if((idProduto % 1) == 0){
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirProduto",
                  data: "idProduto="+idProduto+"&quantidade="+quantidade+"&produto="+produto,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $( "#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                        
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir produto.');
                    }
                }
            });
                return false;
            }
            
        });
        $(document).on('click', '#divServicos span', function(event)
        {
            var idServico = $(this).attr('idAcao');
            if((idServico % 1) == 0){
                $("#divServicos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirServico",
                  data: "idServico="+idServico,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divServicos").load("<?php echo current_url();?> #divServicos" );
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir serviço.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '#divPagamentos span', function(event)
        {
            var idPagamento = $(this).attr('idAcao');
            if((idPagamento % 1) == 0){
                $("#divPagamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirPagamento",
                  data: "idPagamento="+idPagamento,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir pagamento.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '.anexo', function(event) {
         event.preventDefault();
         var link = $(this).attr('link');
         var id = $(this).attr('imagem');
         var url = '<?php echo base_url(); ?>os/excluirAnexo/';
         $("#div-visualizar-anexo").html('<img src="'+link+'" alt="">');
         $("#excluir-anexo").attr('link', url+id);
         $("#download").attr('href', "<?php echo base_url(); ?>index.php/os/downloadanexo/"+id);
     });
        $(document).on('click', '#excluir-anexo', function(event) {
         event.preventDefault();
         var link = $(this).attr('link'); 
         $('#modal-anexo').modal('hide');
         $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
         $.ajax({
          type: "POST",
          url: link,
          dataType: 'json',
          success: function(data)
          {
            if(data.result == true){
                $("#divAnexos" ).load("<?php echo current_url();?> #divAnexos" );
            }
            else{
                alert(data.mensagem);
            }
        }
    });
     });
        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
        $(document).on('click', '#btnInserirClienteAjax', function(event) {
            var preenchido = true;
            $(this).closest("form").find("input[type=text]").each(function(){
                if($(this).val() == ""){
                    preenchido = false;
                    return false;
                }
            });
            if(!preenchido){
                alert("Informe todos os campos do cliente pra inserir!");
            }else{
                var dados = $(this).closest("form").serialize();
                console.log(dados); 
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/clientes/inserirClienteAjax",
                    data: dados,
                    dataType: 'json',
                    success: function(data){
                        if(data.result == true){
                            $('#modal-inserir-cliente').modal('hide');
                            $("#cliente").val(data.nomeCliente);
                            $("#clientes_id").val(data.idCliente);
                        }else{
                            alert("Ocorreu um erro ao tentar adicionar um cliente!");
                        }
                    },
                    error: function(){
                        alert("ERRO!");
                    }
                });
            }
            return false;
        });

    $('input[value=obs_faturado]').on('change', function(){
        var val = 'Faturado.';
        $('input[name=dadosPg]').val(val);
    });
    $('input[value=obs_retirada]').on('change', function(){
        var val = 'Restante na retirada do equipamento.';
        $('input[name=dadosPg]').val(val);
    });
    $('input[value=obs_entrega]').on('change', function(){
        var val = 'Restante na entrega do equipamento.';
        $('input[name=dadosPg]').val(val);
    });
    $('input[name=queda]').on('change', function(){
        var val = $('textarea[name=observacoes]').val();
        
        if($('input[name=queda]').is(':checked')){
            if(val != ""){
                val += '\n';            
            }
            val += 'ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção corretiva destes danos terá custo adicional de acordo com cada equipamento. \n';            
        }else{
            val = val.split('ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção corretiva destes danos terá custo adicional de acordo com cada equipamento.').join('').trim();
        }
        $('textarea[name=observacoes]').val(val);
    });
    $('input[name=umidade]').on('change', function(){
        var val = $('textarea[name=observacoes]').val();
        
        if($('input[name=umidade]').is(':checked')){
            if(val != ""){
                val += '\n';            
            }
            val += 'ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo a não ligar após este procedimento por questões adversas. \n';            
        }else{
            val = val.split('ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo a não ligar após este procedimento por questões adversas.').join('').trim();
        }
        $('textarea[name=observacoes]').val(val);
    });

    $('input[name=telaandroid]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=telaandroid]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente. ';

        }else{
            val = val.split('ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });


    $('input[name=paralela1]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela1]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo preço. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo preço.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=paralela2]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela2]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo falta de disponibilidade no mercado de peças. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo falta de disponibilidade no mercado de peças.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=estufada]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=estufada]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'Em situações de troca ou qualquer outro reparo em que a bateria já esteja "estufada", existe o risco de explosão por questões adversas, caso ocorra, causando novo defeito o reparo terá custo adicional após nova avaliação. ';

        }else{
            val = val.split('Em situações de troca ou qualquer outro reparo em que a bateria já esteja "estufada", existe o risco de explosão por questões adversas, caso ocorra, causando novo defeito o reparo terá custo adicional após nova avaliação.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    });

    function modalCodigo() {
        $("#modal-codigo-barras .modal-header h5").text('Escanear código de barras').find('i').remove();
        $("#modal-codigo-barras .modal-body .div-scan").show();
        $("#modal-codigo-barras .modal-body .div-wrapper").addClass('hidden');
    }

    function adicionar_equipamento_modal() {
        $('#idEquipamento').val(localStorage.redemult_idEquipamento);
        $('#equipamento').val(localStorage.redemult_equipamento);
        document.getElementById('iframe-add_equipamento').contentDocument.location.href = '<?php echo site_url('/equipamentos/adicionar/1'); ?>';
    }

    function adicionar_fotos_modal() {
        var fotosItens = localStorage.redemult_OI_Fotos.split(',');

        if(fotosItens.length >= 1){
            for (var i = fotosItens.length - 1; i >= 0; i--) {
                $('#observacoes_internas_img0'+(i+1)).val(fotosItens[i]);
            }
        }
        document.getElementById('iframe-add_fotos').contentDocument.location.href = "<?php echo site_url('/os/obs_addFoto/'.$result->idOs); ?>";
    }

</script>

