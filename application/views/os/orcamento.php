<?php
	require (reisdev('bootstrap'));
	$CI = &get_instance();
	if($this->permission->checkPermission($this->session->userdata('permissao'),'aOs'))
	{ ?>
    		<a href="<?php echo base_url();?>index.php/os/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar OS</a>
<?php 	} ?>

<div class="widget-box">
     	<div class="widget-title">
        	<span class="icon"><i class="icon-tags"></i></span>
        	<h5>Ordens de Serviço - Orçamento</h5>
     	</div>
</div>
<div class="widget-content nopadding">
<?php
	
	$dt = new DataTable();

	$dt->Columns(
		array('O.S', 'Cliente', 'Técnico', 'Atendente', 'Data Inicial', 'Data Final', 'Status', '')
	);	

	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
			if ($this->permission->checkPermission($this->session->userdata('permissao'),'vOs'))
				$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/os/visualizar/'.$r->idOs.'" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
			 
			if ($this->permission->checkPermission($this->session->userdata('permissao'),'eOs'))
				$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/os/editar/'.$r->idOs.'" class="btn btn-info tip-top" title="Editar OS"><i class="icon-pencil icon-white"></i></a>';
			 
			if ($this->permission->checkPermission($this->session->userdata('permissao'),'dOs'))
				$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" os="'.$r->idOs.'" class="btn btn-danger tip-top" title="Excluir OS"><i class="icon-remove icon-white"></i></a>';
			
			$dataInicial = date(('d/m/Y'),strtotime($r->dataInicial));
			$dataFinal = date(('d/m/Y'),strtotime($r->dataFinal));
					
			$dt->add_Item(array
			(
				$r->idOs,
				$r->nomeCliente,
				$r->nomeTecnico,
				$r->nomeAtendente,
				$dataInicial,
				$dataFinal,
				$CI->GetStatusLabel($r->status),
				$buttons
			));
		}
	}
	$dt->End();
?>
</div>



<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/os/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir OS</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idOs" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>






<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var os = $(this).attr('os');
        $('#idOs').val(os);

    });

});

</script>
