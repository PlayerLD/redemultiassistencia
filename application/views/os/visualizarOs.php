<?php
  $totalS = 0;
  $totalP = 0;
  $total = 0;
  $desconto = 0;   

  require (reisdev('bootstrap'));

  function draw_def($def, $tipo)
  {
    $c = array('', '', 'Tela Display', 'Touch Screen', 'Teclas', 'Sensores de Proximidade', 'Bluetooth', 'Wifi', 'Ligações', 'Alto', 'Falante', 'Áudio', 'Auricular', 'Microfone', 'Câmera', 'Conector de carregador', 'Conector de cartão de memória', 'Conector de fone de ouvido');
    $v = array('', '', 'O aparelho não esta ligando', 'O aparelho não mostra imagem', 'O aparelho está com lentidão/travando', 'O aparelho não contem HD', 'O aparelho não contém cartão de memória', 'O aparelho não reconhece os jogos', 'Dificuldades para abrir/fechar a bandeja', 'Não conecta ao Wifi');
    $t = array('', '', 'Tela display ', 'Touch screen', 'Teclas ', 'Bluetooth ', 'Wi-fi', 'Alto-falantes', 'Controle remoto', 'Conversor digital', 'Sintonizador várias', 'Entrada HDMI', 'Acesso Internet ', 'Função smart ');
    $d = array('', '',' Não ativa Motores','Não Liga após atualização',' Sem imagem na Câmera',' Bateria Não Carrega',' Rádio não se comunica com Drone',' Drone não estabiliza',' Drone vibrando',' Leds Intermintente',' Mensagem Gimbal Error',' Gimbal mexedo sozinho',' Guimbal não mexe e com imagem',' Gimbal não mexe e sem imagem','Helice com avarias',' Conector de carga não funciona',' Não reconhece cartão de memória',' Não grava video e fotos',' Fora de foco',' Perca no alcance do Rádio',' Não Conecta no Wifi','Carcaça/Shell com avarias');
    
    switch ($tipo)
    {
      case 'celular':
      case 'tablet':
      case 'Celular':
      case 'Tablet':
        $arr = $c;
        break;
        
      case 'tv':
      case 'TV':
        $arr = $t;
        break;
        
      case 'videogame':
      case 'Videogame':
        $arr = $v;
        break;
        
      case 'notebook':
      case 'Notebook':
        $arr = $c;
        break;
      case 'computador':
      case 'Computador':
        $arr = $c;
      case 'mac':
      case 'Mac':
      case 'iMac':
      case 'imac':
        $arr = $c;
        break;
        
      default:
        return;
    }     
    
    $one = true;
    for ($i = 0; $i < count($def); $i++)
      if ($def[$i] == 1 && $i < count($arr))
      {
        
        echo ('<small>'.$arr[($i)].'</small><br/>');
      }
  }
  
  function draw_def_saida($def, $tipo)
  {
    $c = array('', '', 'Tela Display', ' Touch Screen', ' Teclas', ' Sensores de Proximidade', ' Imei Zerado/Replicado/Em branco', ' Imei Bloqueado', ' Bluetooth', ' Wifi', ' Wifi não navega', ' Não faz Ligações', ' Não recebe Ligações', ' Alto Falante', ' Áudio Auricular', ' Microfone principal', ' Microfone Secundário', ' Câmera Frontal', ' Câmera Traseira', ' Conector de carregador', ' Bateria não carrega', ' Conector de cartão de memória', ' Conector de fone de ouvido', 'Bateria Estufada', 'Carcaça com avarias', 'Celular sem acessórios',);
    $v = array('', '', ' Não mostra imagem', ' Lentidão/Travamento', ' Não reconhece Jogos', ' Problemas na Bandeja', ' Wifi não conecta', ' Super Aquecimento', 'Não tem HD', 'Não tem cartão de memória', 'Carcaça com avarias');
    $t = array('', '',  ' Não mostra imagem',  'Tela com avarias',  ' Imagem travada',  ' Não sai som',  ' Não funciona Wifi',  ' Não acessa internet',  ' Controle remoto não funciona',  ' Não reconhece Carregador ou cabo de força',  ' Não reconhece HDMI',  ' Não reconhece VGA',  ' Tecladas da TV',  'Carcaça com avarias');
    $d = array('', '',' Não ativa Motores','Não Liga após atualização',' Sem imagem na Câmera',' Bateria Não Carrega',' Rádio não se comunica com Drone',' Drone não estabiliza',' Drone vibrando',' Leds Intermintente',' Mensagem Gimbal Error',' Gimbal mexedo sozinho',' Guimbal não mexe e com imagem',' Gimbal não mexe e sem imagem','Helice com avarias',' Conector de carga não funciona',' Não reconhece cartão de memória',' Não grava video e fotos',' Fora de foco',' Perca no alcance do Rádio',' Não Conecta no Wifi','Carcaça/Shell com avarias');
    
    switch ($tipo)
    {
      case 'celular':
      case 'tablet':
      case 'Celular':
      case 'Tablet':
        $arr = $c;
        break;
        
      case 'tv':
      case 'TV':
        $arr = $t;
        break;
        
      case 'videogame':
      case 'Videogame':
        $arr = $v;
        break;
        
      case 'notebook':
      case 'Notebook':
        $arr = $c;
        break;
      case 'computador':
      case 'Computador':
        $arr = $c;
      case 'mac':
      case 'Mac':
      case 'iMac':
      case 'imac':
        $arr = $c;
        break;
        
      default:
        return;
    }     
    
    $one = true;
    for ($i = 0; $i < count($def); $i++)
      if ($def[$i] == 1 && $i < count($arr))
      {
        
        echo ('<small>'.$arr[($i)].'</small><br/>');
      }
  }
  
  function draw_img($i)
  {
    // IMAGENS PEQUENAS
    echo('<tr class="imgPequenaLoja"><td colspan="6" align="center">ENTREGA DO EQUIPAMENTO PARA A LOJA</td></tr>');
    echo ('     <tr class="imgPequenaLoja" id="div0'.$i.'T1" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px; ">');
          echo ('             <div id="div0'.$i.'_Img01T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img02T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img03T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img04T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img05T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img06T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');  
    echo('<tr class="imgPequenaCliente"><td colspan="6" align="center">ENTREGA DO EQUIPAMENTO PARA AO CLIENTE</td></tr>');
    echo ('     <tr class="imgPequenaCliente" id="div0'.$i.'T2" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img07T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img08T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img09T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img10T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img11T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');  
    echo ('         <td style="width: 15%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img12T1" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');            
    echo ('     </tr>');
    
    // IMAGENS GRANDES
    echo('<tr class="imgGrandeLoja"><td colspan="6" align="center">ENTREGA DO EQUIPAMENTO PARA A LOJA</td></tr>');
    echo ('     <tr class="imgGrandeLoja" id="div0'.$i.'T21" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px; ">');
          echo ('             <div id="div0'.$i.'_Img01T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img02T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
    echo ('     <tr class="imgGrandeLoja" id="div0'.$i.'T22" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img03T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img04T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
    echo ('     <tr class="imgGrandeLoja" id="div0'.$i.'T23" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img05T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img06T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
    echo('<tr class="imgGrandeCliente"><td colspan="6" align="center">ENTREGA DO EQUIPAMENTO PARA AO CLIENTE</td></tr>');
    echo ('     <tr class="imgGrandeCliente" id="div0'.$i.'T24" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px; ">');
          echo ('             <div id="div0'.$i.'_Img07T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img08T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
    echo ('     <tr class="imgGrandeCliente" id="div0'.$i.'T25" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img09T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img10T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
    echo ('     <tr class="imgGrandeCliente" id="div0'.$i.'T26" style="margin: 0px; border-left: 1px solid #ddd;  border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; display: none;">');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img11T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('         <td colspan="3" style="width: 49%; padding: 2px; border: 0px;">');
          echo ('       <div id="div0'.$i.'_Img12T2" class="pnImg" style="width:100%; height: auto; margin: 0px; padding: 0px;"> </div>');
          echo ('         </td>');
    echo ('     </tr>');
      }
?>
<style type="text/css">
  h6{margin: 0;}
</style>
<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Ordem de Serviço</h5>
                <div class="buttons">
                    <?php echo '<a title="" href="'.base_url().'agile/gerar-nfe.php?idOs='.$result->idOs.'&idServicos='.(@$servicos[0]->idServicos_os).'&idFranquia='.$this->session->userdata('id').'" class="btn btn-mini btn-warning" target="_blank"><i class="icon-file icon-white"></i> Gerar NF-e</a>'; ?>
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                        echo '<a title="Icon Title" class="btn btn-mini btn-info" href="'.base_url().'index.php/os/editar/'.$result->idOs.'"><i class="icon-pencil icon-white"></i> Editar</a>'; 
                    } ?>
                    <button id="imprimir" title="Imprimir" class="btn btn-mini btn-inverse fotos">
                      <i class="icon-print icon-white"></i> Imprimir
                    </button>
                    
                    <select id="imprimirfotos" class="btn btn-mini btn-inverse fotos">
                      <option value="0"> Sem fotos </option>
                      <option value="1"> Entregue na loja (pequena) </option>
                      <option value="2"> Entregue para o cliente (pequena)</option>
                        <option value="3" selected> Todas (pequena)</option>
                        <option value="4"> Entregue na loja (grande) </option>  
                        <option value="5"> Entregue para o cliente (grande) </option> 
                        <option value="6"> Todas (grande)</option>
                    </select>
                    &nbsp;&nbsp;<input type="checkbox" id="cbCondicoes" name="cbCondicoes" value="1" checked> Condições de Uso </input>
                    &nbsp;&nbsp;<input type="checkbox" id="cbDetalhes" name="cbDetalhes" value="1" checked> Analítica </input>
                    &nbsp;&nbsp;<input type="checkbox" id="cbTermos" name="cbTermos" value="1" checked> Termos </input>
                    &nbsp;&nbsp;<input type="checkbox" id="2viaos" name="2viaos" value="1"> 2º VIA</input>
                </div>
            </div>
            <div class="widget-content" id="printOs">     
<style>
h5
{
    font-size: 12px !important;
    font-weight: bold !important;
    margin-top: 5px !important;
    margin-bottom: 0px !important;
}
table
{
    margin-bottom: 0px !important;
}
hr
{
    margin-top: 1px !important;
    margin-bottom: 1px !important;
}
th
{
    background-color: #efefef !important;
}
td, th
{
    padding: 2px 2px 2px 2px !important;
}
button.fotos
{
  margin-right: -3px !important;
}
select.fotos
{
  margin: 0px !important;
  height: 22px;
  width: 19px;
  box-shadow: none;
  float: none;
  border: 0 none;
}
select.fotos option
{
  padding: 0px !important;
  margin: 0px !important;
}
.multiselect-container>li>a>label {
  padding: 4px 20px 3px 20px;
}
.imgPequenaLoja td,
.imgPequenaCliente td,
.imgGrandeLoja td,
.imgGrandeCliente td{
  text-align:center;
  font-weight:bold;
  border-right: 1px solid #ddd;
  border-left: 1px solid #ddd;
}
@media print {
  #header, #user-nav, #search, #sidebar, #content-header, .widget-title, body>.row-fluid, .btn{
    visibility: hidden;
    display:none;
  }
  html, body { height: auto; }
  body{
    margin: 0;
    padding: 0;
  }
  #content{
    margin: 0;
    paddnig:0
  }
}
</style>
                <div class="invoice-content" style="padding: 0 20px;">
                    <div class="invoice-head" style="margin-bottom: 0">
                        <table class="table" style="font-size: 10px;">
                            <tbody>
                                <?php if($emitente == null) {?>
                                            
                                <tr>
                                    <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/sistemaos/emitente">Configurar</a><<<</td>
                                </tr>
                                <?php } else {?>
                                <tr>
                                    <td style="width: 16%; line-height: 15px;"><img src=" <?php echo $emitente[0]->url_logo; ?> "></td>
                                    <td> <span style="font-size: 13px; "> <?php echo $emitente[0]->nome; ?> (CNPJ: <span><?php echo $emitente[0]->cnpj; ?></span>) </span> </br>
                                    Endereço: <?php echo $emitente[0]->rua.', nº:'.$emitente[0]->numero.', '.$emitente[0]->bairro.' - '.$emitente[0]->cidade.' - '.$emitente[0]->uf; ?> </span> </br> 
                                    <span> E-mail: <?php echo $emitente[0]->email.' - Telefone: '.Mask::Tel($emitente[0]->telefone); ?></span><br>
                                    <?php if($result->status == "Orçamento"){ ?>
                                    <span>* Orçamento válido por 7 dias <?php if(!empty($observacoesInternas)){ ?> | <?php } ?></span>
                                    <?php } ?>
                                    <?php if(!empty($observacoesInternas)){ ?>
                                    <span> * Esta OS possui observações.</span>
                                    <?php } ?>
                                  </td>
                                    
                                    <td style="width: 23%; text-align: center">
                                      #OS: <span ><?php echo $result->idOs?></span>
                                      <span style="display:block; line-height: 1.2;">Início: <?php echo date(('d/m/Y H:i'),strtotime($result->dataInicial)); ?></span>
                                      <span style="display:block; line-height: 1.2;">Emissão: <?php echo date('d/m/Y H:i')?></span>
                                      <span style="display:block; line-height: 1.2;">Status: <?php echo $result->status?></span>
                                      <span style="display:block; line-height: 1.2;">Garantia: <?php echo $result->garantia?></span>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <table class="table" style="font-size: 10px;">
                            <tbody>
                                <tr>
                                    <td style="width: 60%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h5 style="margin-bottom: 0px;">Cliente: <span><?php echo $result->nomeCliente?></span></h5><span>
                                                <span>RG: <?php echo $result->rg?>  CPF: <?php echo $result->documento?> 
                                                <span>Telefone:
                                                <?php
                                                  if ($result->telefone)
                                                    echo (Mask::Tel($result->telefone));
                                                  
                                                  if ($result->celular)
                                                  {
                                                    if ($result->celular != $result->telefone)
                                                    {
                                                      if ($result->telefone)
                                                        echo (' / ');
                                                      
                                                      echo (Mask::Tel($result->celular));
                                                    }
                                                  }
                                                ?></span><br/>
                                                <span>Endereço: <?php echo $result->rua?>, <?php echo $result->numero?>, <?php echo $result->bairro?></span><span>, <?php echo $result->cidade?> - <?php echo $result->estado?>, CEP: <?php echo $result->cep; ?></span>
<!-- <span style="position:absolute; margin-left: 30px; margin-top: -20px;">RG: <?php echo $result->rg?> <br />
   Doc: <?php echo $result->documento?>  </span> -->
                                            
                                            </li>
                                        </ul>
                                    </td>
                                    <td style="width: 40%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h5>Responsável técnico: <span><?php echo $result->nome?></span> <br/> </h5></span>
                                                <span><h5>Atendente: <span><?php echo $result->nomeAtendente?></span> <br/> </h5></span>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                    
                    <table class="table" style="font-size: 10px; margin-bottom: 0; border-width: 1px">
                      <tbody>
                       
                        <?php if (strlen($result->descricaoProduto) >= 41): ?>
                          <?php if ($result->descricaoProduto != null): ?>
                            <tr>
                               <td colspan="5" style="width: 15%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Descrição: </strong></small>
                                  <?php echo $result->descricaoProduto?>
                                </td>
                            </tr>
                          <?php endif; ?>
                        <?php endif; ?>
                          <?php
                          $visita = [];
                          
                          // if(!empty($result->visita_data))
                          //   $visita[] = "Agendamento para ".(date('d/m/Y', strtotime($result->visita_data)));
                          if(!empty($result->visita_hora1))
                            $visita[] = "das ".$result->visita_hora1;
                          if(!empty($result->visita_hora2))
                            $visita[] = "até ".$result->visita_hora2.".";
                          if(!empty($result->visita_valor))
                            $visita[] = "Segunda tentativa: R$".number_format($result->visita_valor, 2, ',', '.');

                          $visita = implode(' ', $visita);
                          // dd($visita); exit;
                          ?>
                          <?php if (!empty($visita) || !empty(@$result->visita)): ?>
                            <tr>
                              <td colspan="5" style="width: 25%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Visita Técnica/Retirada e Entrega: </strong></small>
                                  <?php echo !empty($visita) ? $visita : $result->visita; ?>
                              </td>
                            </tr>
                          <?php endif; ?>

                          <?php
                          $backup = [];
                          
                          if(!empty($result->backup_modelo))
                            $backup[] = "Modelo: ".$result->backup_modelo;
                          if(!empty($result->backup_imei))
                            $backup[] = "IMEI: ".$result->backup_imei;
                          if(!empty($result->backup_valor))
                            $backup[] = "Preço de mercado: R$".number_format($result->backup_valor, 2, ',', '.');

                          $backup = implode(' ', $backup);
                          ?>

                          <?php if (!empty($backup)): ?>
                            <tr>
                              <td colspan="5" style="width: 25%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Aparelho de backup: </strong></small>
                                  <?php echo $backup ?>
                              </td>
                            </tr>
                          <?php endif; ?>

                        <?php if (strlen($result->observacoes) >= 41): ?>
                          <?php if ($result->observacoes != null): ?>
                            <tr>
                              <td colspan="5" style="width: 20%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                <small><strong>Observações: </strong></small>
                                <?php $CI = &get_instance(); ?>
                                <?php if($CI->getDefeito(@$def) == "Não liga"){ ?>
                                  Equipamento não liga, portanto após análise ou ligar novamente poderá apresentar os seguintes detalhes ou erros abaixo, a correção destes detalhes ou erros terá custo adicional após nova avaliação.
                                <?php } ?>
                                <?php echo $result->observacoes ?>
                              </td>
                            </tr>
                          <?php endif; ?>
                        <?php endif; ?>
                        <?php if (strlen($result->laudoTecnico) >= 41): ?>
                          <?php if ($result->laudoTecnico != null): ?>
                            <tr>
                              <td colspan="5" style="width: 20%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                <small><strong>Laudo Técnico: </strong></small>
                                <?php echo $result->laudoTecnico ?>
                              </td>
                            </tr>
                          <?php endif; ?>
                        <?php endif; ?>
                        <!-- Separando FORMAS -->
                        <tr>
                            <?php if (strlen($result->descricaoProduto) <= 41): ?>
                              <?php if ($result->descricaoProduto != null): ?>
                                <td style="width: 15%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                    <small><strong>Descrição</strong></small>
                                    <?php echo $result->descricaoProduto?>
                                </td>
                              <?php endif; ?>
                            <?php endif; ?>    

                            <?php
                          $visita = [];
                          
                          // if(!empty($result->visita_data))
                          //   $visita[] = "Agendamento para ".(date('d/m/Y', strtotime($result->visita_data)));
                          if(!empty($result->visita_hora1))
                            $visita[] = "das ".$result->visita_hora1;
                          if(!empty($result->visita_hora2))
                            $visita[] = "até ".$result->visita_hora2.".";
                          if(!empty($result->visita_valor))
                            $visita[] = "Segunda tentativa: R$".number_format($result->visita_valor, 2, ',', '.');

                          $visita = implode(' ', $visita);
                          // dd($visita); exit;
                          ?>

                              <?php if (!empty($visita) || !empty(@$result->visita)): ?>
                                <td style="width: 25%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Visita Técnica/Retirada e Entrega: </strong></small>
                                  <?php echo !empty($visita) ? $visita : $result->visita; ?>
                                </td>
                              <?php endif; ?>


                            <?php
                            $backup = [];
                            
                            if(!empty($result->backup_modelo))
                              $backup[] = "Modelo: ".$result->backup_modelo;
                            if(!empty($result->backup_imei))
                              $backup[] = "IMEI: ".$result->backup_imei;
                            if(!empty($result->backup_valor))
                              $backup[] = "Preço de mercado: R$".number_format($result->backup_valor, 2, ',', '.');

                            $backup = implode(' ', $backup);
                            ?>

                              <?php if (!empty($backup)): ?>
                                <td style="width: 20%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Aparelho de backup: </strong></small>
                                  <?php echo $backup?>
                                </td>
                              <?php endif; ?>

                            <?php if (strlen($result->observacoes) <= 41): ?>
                              <?php if ($result->observacoes != null): ?>
                                <td style="width: 20%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                  <small><strong>Observações: </strong></small>
                                  <?php echo $result->observacoes ?>
                                </td>
                              <?php endif; ?>
                            <?php endif; ?>
                            
                            <?php if (strlen($result->laudoTecnico) <= 41): ?>
                              <?php if ($result->laudoTecnico != null): ?>
                                  <td style="width: 20%; padding: 1px 5px 5px 5px; border: 1px solid #dadada;">
                                    <small><strong>Laudo Técnico: </strong></small>
                                    <?php echo $result->laudoTecnico ?>
                                  </td>
                              <?php endif; ?>
                            <?php endif; ?>
                        </tr>
                      </tbody>
                    </table>
<!-- API WEBCAM INIT ---------------------------------------------------------------------------------------------------------------------------------------------- -->
                              <script type="text/javascript" src="<?php echo base_url().reisdev('url')?>cam-api/functions.js"></script>
        <style type="text/css">@import url("<?php echo base_url().reisdev('url')?>cam-api/styles.css");</style>
        <div id="imgPopupBack" class="modal">
          <span class="imgPopupClose">×</span>
          <img class="modal-content" id="imgPopupView">
          <div id="imgPopupText"></div>
            </div>
            <h5 class="no-print">Equipamentos</h5>
          <hr class="no-print"/>
                    <table class="table" style="font-size: 11px; border: 0px;">
                      <tbody>
                      <?php
                                    $totaldeequipametos = count($equipamentos);                               
                      
                      for ($i = 0; $i < $totaldeequipametos; $i++)
                                    {
                                      $def = json_decode($equipamentos[$i]->defeito);
                                      $def_saida = json_decode($equipamentos[$i]->entrega);
                                    //echo "<pre>";
                                    //var_dump($def);
                                    //echo "<pre>";
                          echo ('<tr style="border: 1px solid #ddd;">');
                                      //echo ('<a href='.base_url().'index.php/equipamentos/editar/'.$equipamentos[$i]->idEquipamentos.'>');
                                      
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      echo ('<small>#'.($i+1).' - '.$equipamentos[$i]->equipamento.'</small>');
                                      echo ('</th>');
                                      
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      echo ('<small>Marca: '. $this->os_model->getMarcaNome($equipamentos[$i]->marcas_id)).'</small>';
                                      echo ('</th>');
                                      
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      echo ('<small>Modelo:  '. $equipamentos[$i]->modelo).'</small>';
                                      echo ('</th>');
                                       
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      echo ('<small>Nº Série:  '. $equipamentos[$i]->mei).'</small>';
                                      echo ('</th>');
                                      
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      $CI = &get_instance();
                                      echo ('<small>Defeito:  '. $CI->getDefeito($def)).'</small>';
                                     // echo ('<small>Defeito:  '. $CI->getDefeito($def)).'</small>';
                                      echo ('</th>');
                                      /*
                                      $letras = array(
                                                    0 => 'A',
                                                    1 => 'B',
                                                    2 => 'C',
                                                    3 => 'D',
                                                    4 => 'E',
                                                    5 => 'F',
                                                    6 => 'G',
                                                    7 => 'H',
                                                    8 => 'I',
                                                    9 => 'J',
                                                    10 => 'K',
                                                    11 => 'L',
                                                    12 => 'M',
                                                    13 => 'N',
                                                    14 => 'O',
                                                    15 => 'P',
                                                    16 => 'Q',
                                                    17 => 'R',
                                                    18 => 'S',
                                                    19 => 'T',
                                                    20 => 'U',
                                                    21 => 'V',
                                                    22 => 'X',
                                                    23 => 'Y',
                                                    24 => 'Z'
                                                    );
                                            $marcaNome = $CI->os_model->getMarcaNome($equipamentos[$i]->marcas_id);
                                            if ($equipamentos[$i]->slot < 0)
                                              $slot = '<font color="red"><strong>Atendimento encerrado</strong></font>';
                                            else{
                                               $slotSum = 0;
                                               if($CI->os_model->getSlotPrefix($equipamentos[$i]->idEquipamentos) == 'G')
                                                $slotSum = count($CI->os_model->getSlot('M'));
                                            $slot = $letras[$equipamentos[$i]->slot-(floor($equipamentos[$i]->slot/$franquia->fileiras_slots)*$franquia->fileiras_slots)].ceil(((($equipamentos[$i]->slot+1)+$slotSum)/($franquia->fileiras_slots)));
                                          }
                                          */
                                      $posicao = $CI->os_model->getSlotOcupado($equipamentos[$i]->equipamento, $equipamentos[$i]->idFranquia, $equipamentos[$i]->os_id);
                                      
                                      //$posicao = $CI->os_model->getSlotOcupado($equipamentos[$i]->equipamento, $equipamentos[$i]->idFranquia, $result->idOs);
                                      ?>
                                      <div id="modal-inserir-equipamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: auto;max-width: none;margin-left: 0;left: 0;">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                  <h5 id="myModalLabel">Marcar Checklist de saída</h5>
                                              </div>
                                              <div class="modal-body" style="overflow: hidden;">
                                                  <iframe src="<?php echo site_url("/equipamentos/editar/".$equipamentos[$i]->idEquipamentos."/1#title-checklist-saida"); ?>" id="iframe-add_equipamento" height="500" width="900"></iframe>
                                             </div>
                                             <div class="modal-footer">
                                                <button style="float: right;" id="modal-inserir-equipamento-button" class="btn" data-dismiss="modal" aria-hidden="true" onclick="adicionar_check_saida();" disabled>Voltar a OS</button>
                                                <div class="span5" style="margin-left: 0;float: right;width: 600px;text-align: right;padding-right: 15px;box-sizing: border-box;">
                                                  <label for="button_confirm" style="/* color: white; */display: inline-block;">Fiz as alterações necessárias e cliquei no botão salvar no final dessa tela.</label>
                                                  <input id="button_confirm" type="radio" name="button_confirm" value="button_confirm" onchange="$('#modal-inserir-equipamento-button').attr('disabled', false);"> 
                                                </div>
                                           </div>
                                      </div>
                                      <?php
                                      echo ('<th style="max-width: 15%;min-width: 15%;width:15%; background-color: #efefef; font-weight: bold; padding-top: 2px; padding-bottom: 2px;">');
                                      //echo ('<small>Slot:  '.$slot.'</small>');
                                      echo ('<small>Slot:  '.$posicao.'</small>');
                                      echo ('</th>');
                                      //echo ('</a>');

                                      $this->load->model('sistemaos_model');
                                      $_emitente = $this->sistemaos_model->getEmitente($this->session->userdata('id'));
                                      $data_entrega = date('d/m/Y H:i', strtotime($equipamentos[$i]->data_entrega.' '.$_emitente[0]->fuso_horario.' hours'));                                      
                                      echo ('</tr>');
                                      echo (' <tr style="margin: 0px; border-left: 1px solid #dadada; border-right: 1px solid #ddd;">');
                                      echo ('     <td colspan="3" style="width: 100%; padding-left: 15px; margin-bottom: 0; padding-bottom: 0; border: 0px; ">');
                                      echo ('   <span style="color: black; font-weight: bold;">Check list de entrada.</span> <span style="color: red">As funções abaixo NÃO estão funcionando no aparelho.</span>');
                                      echo ('     </td>');
                                      echo ('     <td colspan="3" style="width: 100%; padding-left: 15px; margin-bottom: 0; padding-bottom: 0; border: 0px; padding-left: 15px !important;border-left: 1px solid #ddd;">');
                                      echo ('   <span style="color: black; font-weight: bold;">Check list de saída. '.($equipamentos[$i]->data_entrega == null ? '<a style="line-height: 10px;margin-left: 10px;font-size: 10px;" href="#modal-inserir-equipamento" role="button" data-toggle="modal" data-backdrop="static" class="btn btn-primary" title="Marcar Checklist de saída"><i class="icon-plus icon-check"></i> Selecionar</a>' : "Realizado em ".$data_entrega).'</span>');
                                      echo ('     </td>');      
                                      echo (' </tr>');

                                      echo (' <tr style="margin: 0px; border-left: 1px solid #dadada; border-right: 1px solid #ddd;">');
                                      echo ('     <td colspan="3" style="width: 100%; padding-left: 35px; padding-top: 0; border: 0px; ">');
                                      echo ('   <div style=" -webkit-column-count: 4; /* Chrome, Safari, Opera */
                                                   -moz-column-count: 4; /* Firefox */
                                                   column-count: 4;     line-height: 1;">');
                                      draw_def($def, $equipamentos[$i]->equipamento);
                                      echo ('   </div>');
                                      echo ('     </td>');
                                      echo ('     <td colspan="3" style="width: 100%;padding-left: 15px !important;padding-top: 0;border: 0px;border-left: 1px solid #ddd;">');
                                      echo ('   <div style=" -webkit-column-count: 4; /* Chrome, Safari, Opera */
                                                   -moz-column-count: 4; /* Firefox */
                                                   column-count: 4;     line-height: 1;">');
                                      draw_def_saida($def_saida, $equipamentos[$i]->equipamento);
                                      echo ('   </div>');
                                      echo ('     </td>');
                                      echo (' </tr>');
                                      draw_img($i);
                          echo ('<tr><td style="border: 0px; margin: 7px"></td></tr>');
                                    }
                                    echo ('<script type="text/javascript"> window.onload = function(event) {');
              for ($i = 0; $i < $totaldeequipametos; $i++)
                                    {                               
                                      echo ('updatePreview2("div0'.$i.'_", '.$equipamentos[$i]->idEquipamentos.');');
                                    }
                                    echo ('} </script>');
                            ?>
                            </tbody>
                        </table>                  
<!--  API WEBCAM END ----------------------------------------------------------------------------------------------------------------------------------------------- -->
      <div id="divDetalhes">
                        <hr class="no-print"/>
                        <table class="table table-bordered" style="font-size: 10px">
                                        <thead>
                                            <tr>
                                                <th><small>Serviço (Mão de Obra)</small></th>
                                                <th><small>Descrição</small></th>
                                                <th><small>Sub-total</small></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php if($servicos != null){?>
                                            <?php
                                        setlocale(LC_MONETARY, 'en_US');
                                        foreach ($servicos as $s) {
                                            $preco = $s->precoServico;
                                            $totalS = $totalS + $preco;
                                            echo '<tr>';
                                            echo '<td><small>'.$s->nomeServico.'</small></td>';
                                            echo '<td><small>'.$s->descricaoServico.'</small></td>';
                                            echo '<td><small>R$ '.number_format($s->precoServico, 2, ',', '.').'</small></td>';
                                            echo '</tr>';
                                        }?>
                        <?php }?>
                                        <tr>
                                            <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                            <td><strong>R$ <?php  echo number_format($totalS, 2, ',', '.');?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <br />
                      <table class="table table-bordered" style="font-size: 10px; margin-top: -15px" id="tblProdutos">
                            <thead>
                                <tr>
                                    <th><small>Componente</small></th>
                                    <th><small>Quantidade</small></th>
                                    <th><small>Sub-total</small></th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php if($produtos != null) { ?>
                                <?php
                                    foreach ($produtos as $p) {
                                        $totalP = $totalP + $p->subTotal;
                                        echo '<tr>';
                                        echo '<td><small>'.$p->descricao.'</small></td>';
                                        echo '<td><small>'.$p->quantidade.'</small></td>';
                                        
                                        echo '<td><small>R$ '.number_format($p->subTotal,2,',','.').'</small></td>';
                                        echo '</tr>';
                                    }
                ?>
                            <?php } ?>
                                <tr>
                                    <td colspan="2" style="text-align: right"><strong><small>Total:</small></strong></td>
                                    <td><strong>R$ <?php echo number_format($totalP,2,',','.');?></strong></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <table class="table table-bordered" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th><small>Forma de pagamento</small></th>
                                    <th><small>Dados</small></th>
                                    <th><small>Desconto</small></th>
                                    <th><small>Sub-total</small></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($pagamentos != null) { ?>
                                <?php
                                        setlocale(LC_MONETARY, 'en_US');
                                        foreach ($pagamentos as $p) {
                                            $total += $p->valorPg;
                                            $total -= $p->valorDesc;
                                            $desconto += $p->valorDesc;
                                            echo '<tr>';
                                            echo '<td><small>'.$p->formaPg.'</small></td>';
                                            echo '<td><small>'.$p->dadosPg.'</small></td>';

                                            if($p->valorDesc > 0)
                                              echo '<td><small>R$ '.number_format($p->valorDesc, 2, ',', '.').'</small></td>';
                                            else
                                              echo '<td></td>';

                                            echo '<td><small>R$ '.number_format($p->valorPg, 2, ',', '.').'</small></td>';
                                            echo '</tr>';
                                        } 
                ?>
                            <?php } ?>
                                    <tr>
                                        <td colspan="3" style="text-align: right"><strong><small>Total:</small></strong></td>
                                        <td><strong><small>R$ <?php  echo number_format($total, 2, ',', '.');?></small></strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                  <hr />
                      </div>
                      <div id="divDetalhes2" style="display: none">
                          <h5 style="margin: 0">Total Despesas/Pagamentos</h5>
                          <hr style="margin-top: 0" />
                      </div>                       
                        <table class="table table-bordered" style="font-size: 10px;">
                        <thead>
                            <tr>
                                <th><small>Total despesas</small></th>
                                
                                <?php if($desconto > 0): ?>
                                <th><small>Desconto</small></th>
                                <?php endif; ?>

                                <th><small>Total pago</small></th>
                                <th><small>Total em aberto</small></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center !important;"><small>R$ <?php echo number_format(($totalP + $totalS),2,',','.') ?></small></td>
                                <?php if($desconto > 0): ?>
                                <td style="text-align: center !important;"><small>R$ <?php echo number_format(($desconto),2,',','.') ?></small></td>
                                <?php endif; ?>
                                <td style="text-align: center !important;"><small>R$ <?php echo number_format(($total),2,',','.') ?></small></td>
                                <?php $total_geral = (($totalP + $totalS) - $total); ?>
                                <td style="text-align: center !important;"><small>R$ <?php echo number_format(($total_geral > 0 ? $total_geral : 0 ),2,',','.') ?></small></td>
                            </tr>
                        </tbody>
                    </table>
                        <br />
                    <div id="divTermos">
                  <p style="font-size: 7px; text-align: justify; line-height: 10px">
            <b>01</b> - O aparelho não retirado no prazo de até 90 dias após parecer ou aprovação do serviço, será cobrado um taxa de R$ 25,00(vinte e cinco reais) ao mês a título de seguro. <br/>
            <!-- <b>02</b> - A Assistência oferece garantia de 90 dias após a entrega do aparelho na peça trocada ou serviço executado sem a violação do lacre de segurança ou tinta de segurança. A Assistência Técnica não oferece garantia para serviços como: Limpezas, desoxidações, troca de botões ou conectores diversos, serviços de software, formatações, desbloqueios, senhas, troca de botões. O mal uso do equipamento poderá acarretar em defeito posterior a manutenção que a garantia não irá "cobrir". <br/> -->
            <b>02</b> - A Assistência oferece garantia de 90 dias após a entrega do aparelho na peça trocada ou serviço executado sem a violação do lacre de segurança ou tinta de segurança. Serviços como: Limpezas, desoxidações, troca de botões ou conectores diversos, serviços de software, formatações, desbloqueios, senhas, troca de botões após a manutenção o mal uso do equipamento irá acarretar em defeito posterior à manutenção que a garantia não irá "cobrir".</br>
            <b>03</b> - O cliente é total responsável pela procedência do aparelho, estando assim a Assistência Técnica, isenta de qualquer de qualquer responsabilidade. <br/>
            <b>04</b> - A Assistência Técnica não é responsável por arquivos contidos no aparelho, como: fotos, músicas, vídeos, agenda de contatos, programas, aplicativos e jogos. Este arquivos podem ser removidos em determinados serviços sem aviso prévio. <br/>
            <b>05</b> - O cliente esta ciente de que a Assistência Técnica não atende serviços de garantia(gratuito), e a manutenção do seu aparelho ocasionará em perda da garantia do fabricante se assim houver.<br/>
            
            <b>06</b> - Atenção: No caso de orçamento não aprovado ou cancelamento, reservamos um período mínimo de 1 hora para montagem do equipamento, televisão reservamos o período de 2 horas para montagem em dia útil.<br/>
            <span id="divCondicoes"><b>07</b> - O cliente concorda que está recebendo seu equipamento com todas as funções de utilização funcionando perfeitamente. <br/></span>
                    
                    </p>
                </div>
                
                <div id="segundaviaos">
                  
                    <p> <strong>OS: Segunda VIA </strong></p>
                  
                </div>
                
                    <div id="divAssinatura">
                <p style="font-size: 9px; text-align: center">
                  Ass.________________________________ Entregue a loja: ____/____/______ &emsp; &emsp; Ass.________________________________  Entregue ao cliente: ____/____/______
                </p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() . 'assets/js/jQuery.print.js'?>" type="application/javascript"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#cbDetalhes").change(function()
        {
          if ($(this).is(':checked'))
    { $("#divDetalhes").show(); $("#divDetalhes2").hide(); }
    else
    { $("#divDetalhes").hide(); $("#divDetalhes2").show(); }
        });
        $("#cbCondicoes").change(function()
        {
          if ($(this).is(':checked'))
    { $("#divCondicoes").show(); $("#divCondicoes2").hide(); }
    else
    { $("#divCondicoes").hide(); $("#divCondicoes2").show(); }
        });
        
        $("#cbTermos").change(function()
        {
          if ($(this).is(':checked'))
      $("#divTermos").show();
    else
      $("#divTermos").hide();
        });
        $("#segundaviaos").hide(); // escondendo de primeiro a informação de 2º via de OS
        $("#2viaos").change(function(){
            if ($(this).is(':checked')) {
              $("#segundaviaos").show();
            }else{
              $("#segundaviaos").hide();
            }
        });
        
        $("#imprimir").click(function()
        {
//          PrintElem('#printOs');
            window.print();
        });
  $('#imprimirfotos').change(function()
  {
    var i = 0;
    switch ($(this).val())
    {
      case "0": // SEM IMANGES
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").hide();
          $(".imgPequenaCliente").hide();
          $(".imgGrandeLoja").hide();
          $(".imgGrandeCliente").hide();
        }
        break;
        
      case "1": // IMAGENS PEQUENAS LOJA
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").show();
          $(".imgPequenaCliente").hide();
          $(".imgGrandeLoja").hide();
          $(".imgGrandeCliente").hide();
        }
        break;
        
      case "2": // IMAGENS PEQUENAS CLIENTE 
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").hide();
          $(".imgPequenaCliente").show();
          $(".imgGrandeLoja").hide();
          $(".imgGrandeCliente").hide();
        }
        break;
      case "3": // TODAS PEQUENAS
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").show();
          $(".imgPequenaCliente").show();
          $(".imgGrandeLoja").hide();
          $(".imgGrandeCliente").hide();
        }
        break;
      case "4": // IMAGENS GRANDES LOJA
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").hide();
          $(".imgPequenaCliente").hide();
          $(".imgGrandeLoja").show();
          $(".imgGrandeCliente").hide();
        }
        break;
      case "5": // IMAGENS GRANDES CLIENTE
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").hide();
          $(".imgPequenaCliente").hide();
          $(".imgGrandeLoja").hide();
          $(".imgGrandeCliente").show();
        }
        break;
      case "6": // TODAS GRANDES
        for (; i < "<?php echo $totaldeequipametos; ?>"; i++)
        {
          $(".imgPequenaLoja").hide();
          $(".imgPequenaCliente").hide();
          $(".imgGrandeLoja").show();
          $(".imgGrandeCliente").show();
        }
        break;        
    }
  });
  
  $('#imprimirfotos').change();
  
        function PrintElem(elem)
        {
            Popup($(elem).html());
        }
        function Popup(data)
        {
            var mywindow = window.open('', '#OS: <?php echo $result->idOs ?>', 'height=600,width=900');
            mywindow.document.write('<html><head><title>Imprimir #OS: <?php echo $result->idOs ?></title>');
            // mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap.min.css' />");
            // mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap-responsive.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-style.css' />");
            // mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-media.css' />");
            // mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/font-awesome/css/font-awesome.css' />");
            // mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/fullcalendar.css' />");
            mywindow.document.write("<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' />");
            mywindow.document.write("</head><body >");
      mywindow.document.write(data);          
            mywindow.document.write("</body></html>");
            setTimeout(function() { mywindow.print(); } , 50);
            return true;
        }
    });
    function adicionar_check_saida() {
        document.location.reload(true);

    }
</script>
