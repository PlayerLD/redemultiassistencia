<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.css" />

<style type="text/css">
    .popover-modal .popover-body{
        overflow: hidden;
        padding: 10px;
        text-align: left;
    }
    .popover-question{
        padding: 0px 5px;
        background: #0088cc;
        color: white;
        border-radius: 50%;
        font-size: 11px;
        margin-left: 5px;
    }
</style>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span> 
                <h5>Cadastro de OS</h5> 
            </div>
            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                    </ul>   
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divCadastrarOs">
                                <?php if(!empty($custom_error)){ ?>
                                <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;"><?php echo $custom_error; ?></div>
                                <?php } ?>
                                <form action="<?php echo current_url().(!empty($_GET['eq']) ? '/?eq='.$_GET['eq'] : ''); ?>" method="post" id="formOs" autocomplete="off">
                                    <div class="span12" style="padding: 1%;">
                                        <div class="span6" style="position:relative;">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="span12" type="text" name="cliente" value="<?php if (isset($results)) echo $results['nomeCliente']; ?>"  />
                                            <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value="<?php if (isset($results)) echo $results['idClientes']; ?>"  />
                                            <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-cliente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Cliente"><i class="icon-plus icon-white"></i></a>
                                        </div>
                                        <div class="span6">
                                            <label for="tecnico">Técnico / Responsável
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_tecnico" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_tecnico">
                                                    <div class="popover-body">
                                                        Selecione o Técnico que fará a manutenção. Esse técnico já deve esta cadastrado previamente na guia de CONFIGURAÇÕES.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="tecnico" class="span12" type="text" name="tecnico" value=""  />
                                            <input id="usuarios_id" class="span12" type="hidden" name="usuarios_id" value=""  />
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="atendente">Atendente

                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_atendente" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_atendente">
                                                    <div class="popover-body">
                                                        Selecione aqui o atendente que realizou o atendimento inicial do cliente. Esse atendente já deve esta cadastrado previamente na guia de CONFIGURAÇÕES. 
                                                    </div>
                                                  </div>
                                                </span>

                                            </label>
                                            <input id="atendente" class="span12" type="text" name="atendente" value=""  />
                                            <input id="atendentes_id" class="span12" type="hidden" name="atendentes_id" value=""  />

                                            

                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span3">
                                            <label for="status">Status<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_status" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_status">
                                                    <div class="popover-body">
                                                        Selecione aqui o STATUS no qual o aparelho esta entrando na lokja. Tente sempre atualizar esse status em tempo real para o cliente receber iinformações por e-mail e por Whatsapp.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <select class="span12" name="status" id="status" value="">
                                                <option value="Orçamento">Orçamento</option>
                                                <option value="Aguardando Aprovacao">Aguardando Aprovação</option>
                                                <option value="Aprovado">Aprovado</option>
                                                <option value="Nao Aprovado">Não Aprovado</option>
                                                <option value="Aguardando Pecas">Aguardando Peças</option>
                                                <option value="Em Andamento">Em Andamento</option>
                                                <option value="Finalizado">Finalizado</option>
                                                <option value="Entregue Para o Cliente">Entregue Para o Cliente</option>
                                                <option value="Conserto Nao Realizado">Conserto Não Realizado</option>
                                                <option value="Liberar Slot">Liberar Slot</option>
                                                <option value="Cancelado">Cancelado</option>
                                                <option value="Faturado">Faturado</option>
                                                <option value="Guardar Aparelho">Guardar Aparelho</option>
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="dataInicial">Data Inicial<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_dtinicial" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_dtinicial">
                                                    <div class="popover-body">
                                                        Data que o aparelho esta entrando na loja.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="dataInicial" class="span12 datepicker" type="text" name="dataInicial" value="<?php echo date('d/m/Y'); ?>"  />
                                        </div>
                                        <div class="span3">
                                            <label for="dataFinal">Data Final<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_dtfinal" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_dtfinal">
                                                    <div class="popover-body">
                                                        Informe a previsão de entrega do aparelho ou previsão de orçamento.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <input id="dataFinal" class="span12 datepicker" type="text" name="dataFinal" value=""  />
                                        </div>

                                        <div class="span3">
                                            <label for="garantia">Garantia<span class="required">*</span>
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_garantia" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_garantia">
                                                    <div class="popover-body">
                                                        Selecione o tempo de garantia, lembrando que por lei são 90 dias. Só escolha a opção SEM GARANTIA se o cliente trouxer a peça de outro lugar e você não souber a qualidade ou se esta funcionando ou não. Nesse caso relate isso na pate de OBSERVAÇÕES.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <select class="span12" name="garantia" id="garantia" value="">
                                                <option value="90 dias">90 dias</option>
                                                <option value="Sem Garantia">Sem Garantia</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="descricaoProduto">Descrição Produto/Serviço
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_descproduto" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_descproduto">
                                                    <div class="popover-body">
                                                        Descrição do serviço que será executado. Por exemplo: Manutenção da tela, manutenção do conector de carregador, Desoxidação, Atualização de Software etc.                                              
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="6" style="min-height: 147px;"></textarea>
                                        </div>
                                        <div class="span6">
                                            <label for="visita_data">Visita Técnica/Retirada e Entrega
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_visitaentrega" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_visitaentrega">
                                                    <div class="popover-body">
                                                        Se marcou para entregar o aparelho consertado, informe a data, faixa de horário para entregar, e valor para segunda tentativa de entrega caso na primeira na tenha ninguém para receber o aparelho.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <div  style="padding: 15px;border: 1px solid #d1d1d1;background: white;min-height: 114px;">

                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Agendamento para: </span>
                                                    <input style="    display: block;" id="visita_data" class="span12 datepicker" type="text" name="visita_data"  />
                                                </div>

                                                <div style="display: inline-block; float: left;width: 15%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">De: </span>
                                                    <input style="display: block;" id="visita_hora1" class="span12" type="number" name="visita_hora1"  placeholder="horas"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 15%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;"> Até: </span>
                                                    <input style="display: block;" id="visita_hora2" class="span12" type="number" name="visita_hora2"  placeholder="horas"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 35%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Segunda tentativa:</span>
                                                    <input style="display: block;" id="visita_valor" class="span12 money" type="text" name="visita_valor"  placeholder="Segunda tentativa 0,00"/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="backup">Aparelho de backup
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_backup" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_backup">
                                                    <div class="popover-body">
                                                        Se deixou algum aparelho para o cliente ir usando enquanto conserta o dele, informe os dados desse aparelho.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <div  style="padding: 15px;border: 1px solid #d1d1d1;background: white; min-height: 114px;">
                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style="  margin-bottom: 10px; display: inline-block;float: left;">Modelo: </span>
                                                    <input style="    display: inline-block;" id="backup_modelo" class="span12" type="text" name="backup_modelo"  />
                                                </div>

                                                <div style="display: inline-block; float: left;width: 25%;padding-right: 5px;">
                                                    <span style="  margin-bottom: 10px; display: inline-block;float: left; margin-left: 8px;">IMEI: </span>
                                                    <input style="    display: inline-block;" id="backup_imei" class="span12" type="text" name="backup_imei"/>
                                                </div>

                                                <div style="display: inline-block; float: left;width: 42%;padding-right: 5px;">
                                                    <span style=" margin-bottom: 10px; display: block;">Preço de mercado:</span>
                                                    <input style="float: none;display: block; max-width: 70px;" id="backup_valor" class="span12 money" type="text" name="backup_valor"  placeholder="0,00"/>
                                                    <span style="display: block;    "> Aparelho em perfeitas condições </span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <label for="laudoTecnico">Laudo Técnico
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_laudotecnico" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_laudotecnico">
                                                    <div class="popover-body">
                                                        Laudo técnico deverá ser usado principalmente quando o cliente solicitar, especialmente em casos onde o equipamento esta segurando por uma seguradora ou em casos onde o conserto será pago por outra empresa por questão diversas.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5" style="min-height: 147px;"></textarea>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span12 text-right">
                                            <label for="observacoes" class="text-left">Observações
                                                <span class="popover-wrapper">
                                                  <a href="#" data-role="popover" data-target="popover-saida_obs" class="popover-question"><strong>?</strong></a>
                                                  <div class="popover-modal popover-saida_obs">
                                                    <div class="popover-body">
                                                        Coloque as observações desse equipamento.
                                                    </div>
                                                  </div>
                                                </span>
                                            </label>
                                            <textarea class="span12" name="observacoes" id="observacoes" cols="30" rows="5"></textarea>
                                            
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_parelela2" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, <br> motivo falta de disponibilidade no mercado de peças.</label><input type="checkbox" name="paralela2" value="S" id="field_parelela2"><br></p>
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_queda" style="display: inline-block;margin-right: 6px;">Já sofreu queda ou pancada?</label><input type="checkbox" name="queda" value="S" id="field_queda"><br></p>
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_umidade" style="display: inline-block;margin-right: 6px;">Teve contato com a umidade?</label><input type="checkbox" name="umidade" value="S" id="field_umidade"><br></p>
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_telaandroid" style="display: inline-block;margin-right: 6px;">Troca de Tela de Android?</label><input type="checkbox" name="telaandroid" value="S" id="field_telaandroid"><br></p>
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_parelela1" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, motivo preço.</label><input type="checkbox" name="paralela1" value="S" id="field_parelela1"><br></p>
                                            <p style="border: 1px solid; display: inline-block;padding: 0 5px;white-space: pre-wrap;"><label for="field_estufada" style="display: inline-block;margin-right: 6px;">Bateria está estufada?</label><input type="checkbox" name="estufada" value="S" id="field_estufada"><br></p>

                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                            <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                    <?php
                        $id = -1; if (isset($results['idEquipamentos'])) $id = $results['idEquipamentos'];
                        echo form_hidden('idEquipamentos', $id)
                    ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-inserir-cliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/clientes/inserirClienteAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Cliente</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="nomeCliente" placeholder="Nome do cliente">
            <input type="text" name="rgCliente" placeholder="RG">
            <input type="text" name="docCliente" placeholder="CPF">
            <input type="text" name="telefoneCliente" placeholder="Telefone">
            <input type="text" name="celularCliente" placeholder="Celular">
            <input type="text" name="smsCliente" placeholder="sms">
            <input type="text" name="whatsappCliente" placeholder="whatsapp">
            <input type="text" name="emailCliente" placeholder="Email">
            <input type="text" name="cepCliente" placeholder="CEP">
            <input type="text" name="ruaCliente" placeholder="Rua">
            <input type="text" name="numeroCliente" placeholder="Número">
            <input type="text" name="bairroCliente" placeholder="Bairro">
            <input type="text" name="cidadeCliente" placeholder="Cidade">
            <input type="text" name="estadoCliente" placeholder="Estado">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnInserirClienteAjax">Inserir</button>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.js"></script>
<script type="text/javascript">
    $('[data-role="popover"]').popover();
    //$(document).ready(function(){
    //    $("#equipamento").change(function(){
    //        nomeEquipamento = $(this).find('option:selected').attr('value');
    //        $("#tipo").val(nomeEquipamento);
    //    });
    //
    //}); 
</script>

<script type="text/javascript">
$(document).ready(function()
{
        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 1,
            select: function( event, ui ){
                $("#clientes_id").val(ui.item.id);
            }
        });

        $("#tecnico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteUsuario",
            minLength: 1,
            select: function( event, ui ) {
                $("#usuarios_id").val(ui.item.id);
            }
        });

        $("#atendente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteAtendente",
            minLength: 2,
            select: function( event, ui ){
                $("#atendentes_id").val(ui.item.id);
            }
        });

      $('input[name=queda]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        

        if($('input[name=queda]').is(':checked')){

            if(val != ""){

                val += '\n';            

            }

            val += 'ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção corretiva destes danos terá custo adicional de acordo com cada equipamento. ';            

        }else{

            val = val.split('ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção corretiva destes danos terá custo adicional de acordo com cada equipamento.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=umidade]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        

        if($('input[name=umidade]').is(':checked')){

            if(val != ""){

                val += '\n';            

            }

            val += 'ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo a não ligar após este procedimento por questões adversas. ';            

        }else{

            val = val.split('ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo a não ligar após este procedimento por questões adversas.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=telaandroid]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=telaandroid]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente. ';

        }else{
            val = val.split('ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });


    $('input[name=paralela1]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela1]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo preço. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo preço.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=paralela2]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela2]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo falta de disponibilidade no mercado de peças. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo falta de disponibilidade no mercado de peças.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });


      $("#formOs").validate(
      {
          rules:
          {
             cliente: {required:true},
             atendente: {required:true},
             dataInicial: {required:true},
             dataFinal: {required:true},
             garantia: {required:true}
          },
          messages:
          {
             cliente: {required: 'Campo Requerido.'},
             atendente: {required: 'Campo Requerido.'},
             dataInicial: {required: 'Campo Requerido.'},
             dataFinal: {required: 'Campo Requerido.'},
             garantia: {required: 'Campo Requerido.'}
          },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass)
            {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass)
            {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
       });

    $(".datepicker" ).datepicker(
    {
        dateFormat: 'dd/mm/yy'
    });
    
    $(document).on('click', '#btnInserirClienteAjax', function(event) {
        var preenchido = true;
        $(this).closest("form").find("input[type=text]").each(function(){
            if($(this).val() == ""){
                preenchido = false;
                return false;
            }
        });
        
        if(!preenchido){
            alert("Informe todos os campos do cliente pra inserir!");
        }else{
            var dados = $(this).closest("form").serialize();
            
            console.log(dados); 
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/clientes/inserirClienteAjax",
                data: dados,
                dataType: 'json',
                success: function(data){
                    if(data.result == true){
                        $('#modal-inserir-cliente').modal('hide');
                        $("#cliente").val(data.nomeCliente);
                        $("#clientes_id").val(data.idCliente);
                    }else{
                        alert("Ocorreu um erro ao tentar adicionar um cliente!");
                    }
                },
                error: function(){
                    alert("ERRO!");
                }
            });
        }
        
        return false;
    });
});

</script>