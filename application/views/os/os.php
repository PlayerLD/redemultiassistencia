<?php
  require (reisdev('bootstrap'));

  $CI = &get_instance(); 
  if($this->permission->checkPermission($this->session->userdata('permissao'),'aOs'))

  { ?> 

    <a href="<?php echo base_url();?>index.php/os/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar OS</a>

<?php   } ?> 
 
<div class="widget-box">
  <div class="widget-title">
    <span class="icon">
      <i class="icon-tags"></i>
    </span>
    <h5>Ordens de Serviço</h5>
  </div>
</div>
<div class="widget-content nopadding">
  <!-- aqui ficava o codigo original -->
    <table id="memListTable" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>
              <th>idOs</th>
              <th>Nome Cliente</th> 
              <th>Nome Tecnico</th>
              <th>Nome Atendente</th>
              <th>Equipamento</th>
              <th>Dt. Inicial</th>
              <th>Dt. Final</th>
              <th>Responder Por:</th>
              <th>Status</th>
              <th>Botão de Ação</th>
          </tr>
      </thead>
      
    </table>
</div>
<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/os/excluir" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Excluir OS</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idOsExcluir" name="id" value="" />
      <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Excluir</button>
    </div>
  </form>
</div>

<div id="modal-alterar-status" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/os/alterarStatus" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Alterar Status OS</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idOsStatus" name="id" value="" />
            <select class="span12" name="status" id="status" value="">
                <option value="">Selecione...</option>
                <option value="Orçamento">Orçamento</option>
                <option value="Aguardando Aprovacao">Aguardando Aprovação</option>
                
                <option value="Aprovado">Aprovado</option>
                
                <option value="Nao Aprovado">Não Aprovado</option>
                
                <option value="Aguardando Pecas">Aguardando Peças</option>
                
                <option value="Em Andamento">Em Andamento</option>
                
                <option value="Finalizado">Finalizado</option>
                
                <option value="Saiu Para o Entrega">Saiu para a entrega</option>
                
                <option value="Entregue Para o Cliente">Entregue Para o Cliente</option>
                
                <option value="Conserto Nao Realizado">Conserto Não Realizado</option>
                
                <option value="Liberar Slot">Liberar Slot</option>
                
                <option value="Cancelado">Cancelado</option>
                
                <option value="Faturado">Faturado</option>
                <option value="Guardar Aparelho">Guardar Aparelho</option>                                
                
            </select>
      <h5 style="text-align: center"></h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Alterar</button>
    </div>
  </form>
</div>

<!-- Modal -->
<div class="modal hide fade" id="opcaoavaliacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form action="<?php echo base_url() ?>index.php/os/alterarStatus" method="post">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="true">X</button>
        <h5 id="myModalLabel">Informe o Link de Avaliação</h5>
      </div>

      <div class="modal-body">
          <input type="hidden" id="idOsStatus2" name="id" value="" />
          <input type="hidden" id="clienteid2" name="clienteid" value="" />
          <input type="hidden" id="status" name="status" value="Entregue Para o Cliente">
          <h3><input type="radio" name="avaliacao" value="interno" checked> Avaliação Interna</h3><br> 
          <h4><input type="radio" name="avaliacao" value="google"> Avaliação do Google</h4><br>
      <h5 style="text-align: center"></h5>

      </div>

      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" data-dismiss="modal">Cancelar</button>
        <button class="btn btn-primary">Enviar</button>
      </div>
  
  </form>
</div>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>  
<script type="text/javascript">
    $(document).ready(function () {
      $('#memListTable').DataTable({
          "language": {
            "lengthMenu": "Nº Os _MENU_ total por páginas",
            "zeroRecords": "Nada encontrado - Desculpe!",
            "info": "Mostrando _PAGE_ de _PAGES_ Total de _MAX_",
            "infoEmpty": "Sem registo disponível ",
            "infoFiltered": "(Filtrados _MAX_ total de dados)",
            "infoPostFix":    "",
            "thousands":      ",",
            "loadingRecords": "Buscando...",
            "processing":     "Processando...",
            "search":         "Procurar:",
            "zeroRecords":    "Nenhum registro correspondente encontrado",
            "paginate": {
                "first":      "Próximo",
                "last":       "Anterior",
                "next":       "Avançar",
                "previous":   "Retornar"
              }
          },
          "order": [[ 0, "desc" ]],
          "processing": true,
          "serverSide": true,
          "ajax":{
          "url": "<?php echo base_url('index.php/os/getlists'); ?>",
          "dataType": "json",
          "type": "POST",
          "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                         },
        "columns": [
          { "data": "idOs" },
          { "data": "nomeCliente" },
          { "data": "nomeTecnico" },
          { "data": "nomeAtendente" },
          { "data": "equipamento" },
          { "data": "dataInicial" },
          { "data": "dataFinal" },
          //{ "data": "diafinal"},
          //{ "data": "mesfinal"},
          //{ "data": "anofinal"},
          { "data": "btnwhats"},
          { "data": "status" },
          { "data": "todosB"},
        ]

      });
    });
</script>

<script>
    $(document).ready(function(){
        $(document).on('click', 'a', function(event){
            var os = $(this).attr('os');
            $('#idOsExcluir').val(os);
            $('#idOsStatus').val(os);
            var status = $(this).attr('data-status');
            $("#status").val(status);
        });
    });
</script>

<script type="text/javascript">
  //$(document).ready(function(){
      $('select').on('change', function () {
        if ($(this).val() == "Entregue Para o Cliente") {
          $('#opcaoavaliacao').modal('show');
          $('#modal-alterar-status').modal('hide');
           var v1 = document.getElementById("idOsStatus").value;
           document.getElementById("idOsStatus2").value = v1;
           
           var v3 = document.getElementById("clienteid").value;
           document.getElementById("clienteid2").value = v3;

        }
    });
  //});
</script>

