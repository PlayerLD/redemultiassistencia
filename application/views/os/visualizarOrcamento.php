<?php 
require (reisdev('bootstrap'));
$CI = &get_instance();
?> 
<div class="widget-box">
    <div class="widget-title">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Dados do Cliente</a></li>
        </ul>
    </div>   
    <div class="widget-content tab-content">
        <div class="accordion" id="collapse-group">
            <div class="accordion-group widget-box">
                <div class="accordion-heading">
                    <div class="widget-title">
                        <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                            <span class="icon"><i class="icon-list"></i></span><h5>Dados</h5>
                        </a>
                    </div>
                </div>
                <div class="collapse in accordion-body" id="collapseGOne">
                    <div class="widget-content">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Cod</strong></td>
                                <td><?php echo $result->idOrcamento ?></td>
                            </tr>
                            <?php if(!empty($filial)){ ?>
                            <tr>
                                <td style="text-align: right"><strong>Filial</strong></td>
                                <td><?php echo $filial->nome; ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td style="text-align: right"><strong>Data</strong></td>
                                <td><?php echo date(('d/m/Y'),strtotime($result->data)); ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>E-mail</strong></td>
                                <td><?php echo $result->email ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Celular/WhatsApp</strong></td>
                                <td><?php echo Mask::Tel(@$result->telwhats) ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Telefone</strong></td>
                                <td><?php echo Mask::Tel($result->telefone) ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="accordion-group widget-box">
                <div class="accordion-heading">
                    <div class="widget-title">
                        <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
                            <span class="icon"><i class="icon-list"></i></span><h5>Equipamento</h5>
                        </a>
                    </div>
                </div>
                <div class="collapse accordion-body" id="collapseGTwo">
                    <div class="widget-content">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Tipo</strong></td>
                                <td><?php echo ucfirst($result->tipo) ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Marca</strong></td>
                                <td><?php echo $result->marca ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Modelo</strong></td>
                                <td><?php echo $result->modelo ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Defeito</strong></td>
                                <td><?php echo $result->defeito ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Observação</strong></td>
                                <td><?php echo $result->obs ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="accordion-group widget-box">
                <div class="accordion-heading">
                    <div class="widget-title">
                        <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">
                            <span class="icon"><i class="icon-list"></i></span><h5>Responder</h5>
                        </a>
                    </div>
                </div>
                <div class="collapse accordion-body" id="collapseGThree">
                    <div class="widget-content">
                        <table class="table table-bordered">
                            <tbody>
                            
                            <tr>
                                <td style="text-align: right"><strong>Última resposta</strong></td>
                                <td>
                                    <?php echo $result->emailResposta;?>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>WHATSAPP</strong></td>
                                <td>
                                    <?php 
                                        function removerCaracters($msg){
                                            $porcento = '%';
                                            $sifrao   = 'R$';
                                            //$cod = 'R%F0%9F%92%B2';
                                            //$cod = 'R&#x1F4B2';
                                            $cod = 'R&#36;';
                                            $por = '&#8240;';
                                            $r1 = str_replace($porcento, $por, $msg);
                                            $r2 = str_replace($sifrao, $cod, $r1);
                                            return $r2;
                                        }
                                            $rp = "Olá, ".$result->nome.', '; 
                                            //$rp .= 'Email: '.$result->email.', '; 
                                            $rp .= "somos da Assistência Técnica, na qual fez o orçamento referente ao ";
                                            //$rp .= "Segue os dados do Aparelho: ";
                                            $rp .= $result->marca.'. '.$result->modelo.'. '.$result->defeito.'. ';
                                            //$rp .= $result->modelo.'. '.$result->defeito.'. ';
                                            //$rp .= ' Segue nossa Resposta: '.removerCaracters($result->emailResposta);
                                            $rp .= removerCaracters($result->emailResposta);
                                        $tel = @$result->telwhats;
                                    ?>
                                    <?php if ($result->emailResposta != NULL  && @$result->telwhats != NULL): ?>
                    
                                        <a href="https://api.whatsapp.com/send?phone=55<?php echo $tel; ?>&text=<?php echo $rp;?>" target="_blank">
                                            <div class="alert alert-success">Clique Aqui Para Enviar Mensagem de Resposta para o cliente.</div>
                                        </a>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong>Responder</strong></td>
                                <td>
                                    

<script type="text/javascript">
$(document).ready(function(){
    $(".money1").maskMoney();
    $(".money2").maskMoney();
});
    function escolherMsg(este){
        if(este == 1){
            $('#texto-resposta').html('Olá!<br>Valor em <input type="number" name="vezes" style="width: 32px; float: none;" class="money1">X de R$ <input type="text" name="parcela" style="width: 70px;float: none;"> <select style="width: 110px;float: none;" name="juros"><option>Sem Juros</option><option>Com Juros</option></select> nos cartões ou R$<input type="text" name="preco" style="width: 70px; float: none;" class="money2"> á vista.<br><?php if(!empty($filial)){ ?>Para sua região, atendemos com serviço delivery!<br>Nosso pessoal de delivery retira em sua casa ou seu local de trabalho, traz até nossa loja, em seguida consertamos e entregamos novamente em sua casa ou trabalho, tudo isso já incluso no valor do conserto.<?php } ?>');
        }else if(este == 2){
            $('#texto-resposta').html('Olá!<br>No caso deste defeito, para um orçamento correto, nossos técnicos verificaram a necessidade de uma análise técnica do equipamento em nosso laboratório técnico.');
        		
        }else if(este == 3){
            $('#texto-resposta').html('');
        }		
		
	
    }
</script>
                                    <form action="<?php echo base_url() . '/index.php/os/enviarEmailOrcamento'?>" method="post">
                                        <input type="hidden" name="idOrcamento" id="idOrcamento" value="<?php echo $result->idOrcamento?>" />
                                        <select onchange="escolherMsg($(this).val());" style="float: none;" name="qualMsg">
                                            <option value="1">Orçamento</option>
                                            <option value="2">Análise</option>
                                            <option value="3">Mensagem Livre</option>
                                        </select>
                                        <hr>
                                        <p style="font-size: 17px;" id="texto-resposta">Olá!<br>
                                            Valor em <input type="number" name="vezes" style="width: 32px; float: none;">X de R$ <input type="text" name="parcela" style="width: 70px;float: none;" class="money"> <select style="width: 110px;float: none;" name="juros"><option>Sem Juros</option><option>Com Juros</option></select> nos cartões ou R$<input type="text" name="preco" style="
                                            width: 70px; float: none;
                                        " class="money"> á vista.<br>
                                        
                                        <?php if(!empty($filial)){ ?>
                                        <input type="hidden" name="isFilial" value="S" />
                                        Para sua região, atendemos com serviço delivery!<br>
                                        Nosso pessoal de delivery retira em sua casa ou seu local de trabalho, traz até nossa loja, em seguida consertamos e entregamos novamente em sua casa ou trabalho, tudo isso já incluso no valor do conserto.
                                        <?php }else{ ?>
                                        <input type="hidden" name="isFilial" value="N" />
                                        <?php } ?>
                                        </p>
                                        <textarea style="min-width: 90%" rows="10" name="txtEmail" id="txtEmail"></textarea>
                                        <br />
                                        <button class="btn btn-default">Enviar E-mail</button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <div class="span12">
                    <div class="span6 offset3">
                        <a href="<?php echo base_url() ?>index.php/os/orcamentos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                    </div>
                </div>
            </div>

            <?php if(!empty($result->comprovante_pagamento)){ ?>
            <div class="accordion-group widget-box">
                <div class="accordion-heading">
                    <div class="widget-title">
                        <a data-parent="#collapse-group" href="#collapseGFour" data-toggle="collapse">
                            <span class="icon"><i class="icon-list"></i></span><h5>Comprovante de depósito/transferência</h5>
                        </a>
                    </div>
                </div>
                <div class="collapse accordion-body" id="collapseGFour">
                    <div class="widget-content">
                        <table class="table table-bordered">
                            <tbody>

                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Arquivo</strong></td>
                                <td> <img src="<?php echo base_url().'assets/uploads/'.$result->comprovante_pagamento; ?>"> </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if(!empty($result->pagamento_status)){ ?>
            <div class="accordion-group widget-box">
                <div class="accordion-heading">
                    <div class="widget-title">
                        <a data-parent="#collapse-group" href="#collapseGFour" data-toggle="collapse">
                            <span class="icon"><i class="icon-list"></i></span><h5>Pagamento</h5>
                        </a>
                    </div>
                </div>

                <div class="collapse accordion-body" id="collapseGFour">
                    <div class="widget-content">
                        <table class="table table-bordered">
                            <tbody>

                            <tr>
                                <td style="text-align: right; width: 30%"><strong>ID</strong></td>
                                <td> #<?php echo $result->pagamento_id; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Data</strong></td>
                                <td> <?php echo $result->pagamento_data; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Status</strong></td>
                                <td> <?php echo $result->pagamento_status; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%"><strong>Total</strong></td>
                                <td> R$ <?php echo number_format($result->preco, 2, ',', '.'); ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php 
    //echo '<pre>';
    //var_dump($result);
    //echo '</pre>';
    
 ?>
<?php if ($result->emailResposta != NULL && @$result->telwhats != NULL): 
//if ($result->status != 1): ?>
    <script type="text/javascript">
        $(window).load(function() {
            $('#myModalRedirect').modal('show');
        });
    </script>
<?php endif ?>
<!-- Trigger the modal with a button 
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalRedirect">Open Modal</button>
-->

<!-- Modal -->
<div id="myModalRedirect" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        
        <?php 
            function removerCaracters2($msg){
                $porcento = '%';
                $sifrao   = 'R$';
                //$cod = 'R%F0%9F%92%B2';
                //$cod = 'R&#x1F4B2';
                $cod = 'R&#36;';
                $por = '&#8240;';
                $r1 = str_replace($porcento, $por, $msg);
                $r2 = str_replace($sifrao, $cod, $r1);
                return $r2;
            }
                $rp = "Olá, ".$result->nome.', '; 
                //$rp .= 'Email: '.$result->email.', '; 
                $rp .= "somos da Assistência Técnica, na qual fez o orçamento referente ao ";
                //$rp .= "Segue os dados do Aparelho: ";
                $rp .= $result->marca.'. '.$result->modelo.'. '.$result->defeito.'. ';
                //$rp .= $result->modelo.'. '.$result->defeito.'. ';
                //$rp .= ' Segue nossa Resposta: '.removerCaracters($result->emailResposta);
                $rp .= removerCaracters2($result->emailResposta);
            $tel = @$result->telwhats;
        ?>
        <?php if ($result->emailResposta != NULL): ?>

            <a href="https://api.whatsapp.com/send?phone=55<?php echo $tel; ?>&text=<?php echo $rp;?>" target="_blank">
                <div class="alert alert-success">Clique Aqui Para Enviar Mensagem de Resposta para o cliente.</div>
            </a>
        <?php endif ?>


      </div>
      <div class="modal-footer">
        <a href="<?php echo base_url().'index.php/os/orcamentos/' ?>" class="btn btn-warning pull-left">Retornar para Orçamentos</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>