<html lang="pt-br">
<head>
<title>ASSISTÊNCIA TÉCNICA ONLINE</title>
<meta charset="UTF-8" />
</head>
<body>
<table width="700px" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6">
<tbody><tr align="center" height="85px">
		<td style="border-bottom:1px solid #e2e2e2">
			<a href="<?php echo site_url(); ?>" alt="Rede Multi Assistência" title="Acessar site">
				<img src="<?php echo $emitente[0]->url_logo; ?>" style="width: 150px;">
			</a>
		</td>
	</tr>
	<tr align="center" height="120">
		<td style="padding: 0 15px;">
			<br>
			<br>
			<font face="Arial, Helvetica, sans-serif" color="#616163" size="5"><strong>A franquia <?php echo $franquia; ?> fez uma solicitação de estoque.</strong></font>
			<br>
			<br>
			<br>
		</td>
	</tr>
	<tr align="center" height="85px">
		<td> 
			<span style="font-size: 13px; "> <?php echo $emitente[0]->nome; ?> (CNPJ: <span><?php echo $emitente[0]->cnpj; ?></span>) </span> </br>
            Endereço: <?php echo $emitente[0]->rua.', nº:'.$emitente[0]->numero.', '.$emitente[0]->bairro.' - '.$emitente[0]->cidade.' - '.$emitente[0]->uf; ?> </span> </br> 
            <span> E-mail: <?php echo $emitente[0]->email.' - Telefone: '.$emitente[0]->telefone; ?></span><br>
        </td>	
	</tr>
	<tr>
		<td><table width="532" border="0" align="center" cellspacing="0" cellpadding="15" bgcolor="#ffffff" style="background:#ffffff;border-radius:5px;border:1px solid #e2e2e2!important">
			<tbody><tr>
				<td width="500" style="padding: 0;">
				<table cellspacing="0" style="background-color:#fff;border:1px #0088cc solid;border-radius:6px;width:100%;text-align:left;color:#0088cc;font-size:1.05em">
					<thead>
                       <th><strong>Produto</strong></th>
                       <th><strong>Código de Barras</strong></th>
                       <th><strong>Peso</strong></th>
                       <th><strong>Preço</strong></th>
                       <th><strong>Quantidade</strong></th>
                    </thead>
					<tbody>
					<?php echo $produtos; ?>
					</tbody>
			</table><br><br></td>
		</tr>
	</tbody></table></td>
</tr>
<tr>
	<td>
		<table width="700" border="0" align="center" cellspacing="15" cellpadding="30" bgcolor="#0088cc">
			<tbody><tr>
				<?php if(!empty($facebook)){ ?>
				<td>
					<a href="<?php echo $facebook; ?>" alt="Facebook" title="Facebook" target="_blank"><img src="<?php echo base_url(); ?>assets/img/email_face.png"></a>
				</td>
				<?php } ?>
				<?php if(!empty($ass)){ ?>
				<td align="right" valign="top" style="padding: 0;">
					<img src="<?php echo $ass; ?>" alt="Assinatura">
				</td>
				<?php } ?>
			</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table></body></html>