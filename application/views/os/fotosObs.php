<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.css" />

<style type="text/css">
    .popover-modal .popover-body{
        overflow: hidden;
        padding: 10px;
        text-align: left;
    }
    .popover-question{
        padding: 0px 5px;
        background: #0088cc;
        color: white;
        border-radius: 50%;
        font-size: 11px;
        margin-left: 5px;
    }
</style>

<?php if (!$result) {
    echo('<div class="alert alert-danger">Observação não encontrada.</div>');
    return;
} ?> 

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
                                <div class="widget-title">
                        <span class="icon">
                                        <i class="icon-picture"></i>
                                    </span>
                        <h5>
                            Anexar fotos
                        </h5>
                    </div>
                    <div id="imgPopupBack" class="modal">
                        <span class="imgPopupClose">×</span>
                        <img class="modal-content" id="imgPopupView">
                        <div id="imgPopupText"></div>
                    </div>
                    <br/>
                    <script type="text/javascript"
                            src="<?php echo base_url() . reisdev('url') ?>cam-api/functions.js"></script>
                    <style type="text/css">@import url("<?php echo base_url().reisdev('url')?>cam-api/styles.css");</style>
                    <style>
                        .pnPrev{
                            width: 100% !important;
                            max-width:2500px !important;
                            min-height: 0 !important;
                            height: 150px !important;
                            box-sizing:border-box;
                        }
                        
                        .pnPrev .pnI{
                            width: 16%;
                            margin: 0.33%;
                            max-width:100%; 
                            display: block;
                            float:left;
                            box-sizing:border-box;
                            position:relative;
                        }
                        
                        .pnPrev .pnI .btn-danger{
                            top: 0;
                            right:-5px;
                        }
                        
                        .pnPrev .pnImg{
                            width: 100%;
                            height: 110px;
                        }
                        
                        .pnPrev .pnImg img{
                            height: auto !important;
                        }
                    </style>
                    <div id="conteudo">
                        <div class="formulario">
                            <div class="dados">
                                <div class="campos">
                                    <div class="campo last" style="width:100%; text-align:center;">
                                        <h3> Fotos</h3>
                                        <div id="pnPrev" class="pnPrev">
                                            <div id="pnImg01" class="pnI">
                                                <a onclick="excludeImage(1)" id="delImg01" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg01" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg02" class="pnI">
                                                <a onclick="excludeImage(2)" id="delImg02" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg02" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg03" class="pnI">
                                                <a onclick="excludeImage(3)" id="delImg03" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg03" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg041" class="pnI">
                                                <a onclick="excludeImage(4)" id="delImg04" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg04" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg05" class="pnI">
                                                <a onclick="excludeImage(5)" id="delImg05" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg05" class="pnImg"></div>
                                            </div>
                                            <div id="pnImg06" class="pnI">
                                                <a onclick="excludeImage(6)" id="delImg06" class="btn btn-danger bt btDel btHidden tip-top"
                                                   title="Excluir"> <i class="icon-remove icon-white"></i></a>
                                                <div id="divImg06" class="pnImg"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="campo last" style="width:100%; text-align:center;">
                                        <div class="panel" style="width:100%;">
                                            <div id="pnPreview" class="preview" height="240px" style="left: 50%; transform: translate(-50%,0);"></div>
                                            <img src="<?php echo base_url(); ?>assets/img/ex-flash.png" id="img-flash">
                                            <div id="pnCam" height="240px"></div>
                                        </div>
                                        <select id="tipoEntrega" class="cbox" name="tipoEntrega" size="1" style="width:50%; margin: 5px; float:none;">
                                            <option value="0">Entrega do equipamento para loja
                                            <option value="1">Entrega do equipamento para o cliente
                                        </select>
                                        <select id="cbQuality" class="cbox" name="pages" size="1" onChange="changedQuality(this);" style="width:50%; margin: 5px; float:none;">
                                            <option value="25">Muito Baixa(25%)
                                            <option value="50">Baixa(50%)
                                            <option value="75" selected>Normal(75%)
                                            <option value="90">Alta(90%)
                                            <option value="100">Muito Alta(100%)
                                        </select>
                                        <div>
                                            <button type="button" id="btCapture" class="button"> Capturar</button>
                                            <button type="button" id="btSave" hidden="true" class="button"> Manter</button>
                                            <button type="button" id="btDiscard" hidden="true" class="buttonred"> Descartar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<form action="https://www.consultaserialaparelho.com.br/public-web/homeSiga" method="post" id="form-consulta-imei"><input name="token" type="hidden" value="2015001"></form>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.js"></script>
<script type="text/javascript">
    $('[data-role="popover"]').popover();

    localStorage.removeItem('redemult_OI_Fotos');
    localStorage.setItem('redemult_OI_Fotos', []);
    
    window.onresize = function (event) {
        resize();
    };
    $(document).ready(function () {

    });
</script>