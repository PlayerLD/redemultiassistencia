<?php $tab = 0; if (isset($_GET['tab'])) $tab = $_GET['tab']; if ($tab > 4) $tab = 0; $totalS = 0; $totalP = 0; ?>

<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">

            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                 </span> 
            
                <h5>Editar #OS <?php echo $result->idOs ?></h5>
            </div>

            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li <?php if ($tab == 0) echo 'class="active"'; ?> id="tabDetalhes"><a href="#tab0" data-toggle="tab">Detalhes da OS</a></li>
                        <li <?php if ($tab == 1) echo 'class="active"'; ?> id="tabEquipamentos"><a href="#tab1" data-toggle="tab">Equipamentos</a></li>
                        <li <?php if ($tab == 2) echo 'class="active"'; ?> id="tabProdutos"><a href="#tab2" data-toggle="tab">Componentes Utilizados</a></li>
                        <li <?php if ($tab == 3) echo 'class="active"'; ?> id="tabServicos"><a href="#tab3" data-toggle="tab">Serviços (Mão de obra)</a></li>
                        <li <?php if ($tab == 4) echo 'class="active"'; ?> id="tabPagamentos"><a href="#tab4" data-toggle="tab">Formas de Pagamento</a></li>
                        <li <?php if ($tab == 5) echo 'class="active"'; ?> id="tabAnexos"><a href="#tab5" data-toggle="tab">Anexos</a></li>
                        <li <?php if ($tab == 6) echo 'class="active"'; ?> id="tabObservacoesInternas"><a href="#tab6" data-toggle="tab">Observações Internas</a></li>
                    </ul>

                    <div class="tab-content">

						<!-- TAB 0 -->
                        <div class="tab-pane<?php if ($tab == 0) echo ' active'; ?>" id="tab0">
                            <div class="span12" id="divCadastrarOs">

                                <form action="<?php echo current_url(); ?>" method="post" id="formOs">

                                    <?php echo form_hidden('idOs', $result->idOs) ?>
                                    
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6" style="margin-left: 0; position:relative;">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="span12" type="text" name="cliente" value="<?php echo $result->nomeCliente ?>"  />
                                            <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-cliente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Cliente"><i class="icon-plus icon-white"></i></a>
                                            <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value="<?php echo $result->clientes_id ?>"  />
                                            <input id="valorTotal" type="hidden" name="valorTotal" value=""  />
                                        </div>

                                        <div class="span6">
                                            <label for="tecnico">Técnico / Responsável<span class="required">*</span></label>
                                            <input id="tecnico" class="span12" type="text" name="tecnico" value="<?php echo $result->nome ?>"  />
                                            <input id="usuarios_id" class="span12" type="hidden" name="usuarios_id" value="<?php echo $result->usuarios_id ?>"  />
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="atendente">Atendente</label>
                                            <input id="atendente" class="span12" type="text" name="atendente" value="<?php echo $result->nomeAtendente ?>"  />
                                            <input id="atendentes_id" class="span12" type="hidden" name="atendentes_id" value="<?php echo $result->atendentes_id ?>"  />
                                        </div>
                                    </div>
                                    
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span4">
                                            <input id="old_status" type="hidden" name="old_status" value="<?php echo $result->status ?>"  />
                                            <label for="status">Status<span class="required">*</span></label>

                                            <select class="span12" name="status" id="status" value="">
                                                <option <?php if($result->status == 'Orçamento'){echo 'selected';} ?> value="Orçamento">Orçamento</option>
                                                <option <?php if($result->status == 'Aguardando Aprovacao'){echo 'selected';} ?> value="Aguardando Aprovacao">Aguardando Aprovação</option>
                                                <option <?php if($result->status == 'Aguardando Pecas'){echo 'selected';} ?> value="Aguardando Pecas">Aguardando Peças</option>
                                                <option <?php if($result->status == 'Em Andamento'){echo 'selected';} ?> value="Em Andamento">Em Andamento</option>
                                                <option <?php if($result->status == 'Finalizado'){echo 'selected';} ?> value="Finalizado">Finalizado</option>
                                                <option <?php if($result->status == 'Faturado'){echo 'selected';} ?> value="Faturado">Faturado</option>
                                                <option <?php if($result->status == 'Entregue Para o Cliente'){echo 'selected';} ?> value="Entregue Para o Cliente">Entregue Para o Cliente</option>
                                                <option <?php if($result->status == 'Saiu para a entrega'){echo 'selected';} ?> value="Saiu para a entrega">Saiu para a entrega</option>
                                                <option <?php if($result->status == 'Conserto Nao Realizado'){echo 'selected';} ?> value="Conserto Nao Realizado">Conserto Não Realizado</option>
                                                <option <?php if($result->status == 'Nao Aprovado'){echo 'selected';} ?> value="Nao Aprovado">Não Aprovado</option>
                                                <option <?php if($result->status == 'Cancelado'){echo 'selected';} ?> value="Cancelado">Cancelado</option>
                                                <option <?php if($result->status == 'Aprovado'){echo 'selected';} ?> value="Aprovado">Aprovado</option>                                                
                                                <option <?php if($result->status == 'Liberar Slot'){echo 'selected';} ?> value="Liberar Slot">Liberar Slot</option>                                                
                                                <option <?php if($result->status == 'Guardar Aparelho'){echo 'selected';} ?> value="Guardar Aparelho">Guardar Aparelho</option>                                                
                                            </select>

                                            <?php if(!empty($status)){ ?>
                                                <h6>Status Anteriores</h6>
                                                <?php foreach($status as $s){ ?>
                                                <label class="label label-primary" style="display: block; cursor: default; text-align: center;"><?php echo $s->status; ?> - <?php echo substr(date_usa2br($s->data), 0, 16); ?></label>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                        <div class="span3">
                                            <label for="dataInicial">Data Inicial<span class="required">*</span></label>
                                            <input id="dataInicial" class="span12 datepicker" type="text" name="dataInicial" value="<?php echo date('d/m/Y', strtotime($result->dataInicial)); ?>"  />
                                        </div>

                                        <div class="span3">
                                            <label for="dataFinal">Data Final</label>
                                            <input id="dataFinal" class="span12 datepicker" type="text" name="dataFinal" value="<?php echo date('d/m/Y', strtotime($result->dataFinal)); ?>"  />
                                        </div>

                                        <div class="span2">
                                            <label for="garantia">Garantia<span class="required">*</span></label>
                                            <select class="span12" name="garantia" id="garantia" value="">
                                                <option <?php if($result->garantia == '90 dias'){echo 'selected';} ?> value="90 dias">90 dias</option>
                                                <option <?php if($result->garantia == 'Sem Garantia'){echo 'selected';} ?> value="Sem Garantia">Sem Garantia</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="descricaoProduto">Descrição Produto/Serviço</label>
                                            <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"><?php echo $result->descricaoProduto?></textarea>
                                        </div>
                                        <div class="span3">
                                            <label for="visita">Visita Técnica/Retirada e Entrega</label>
                                            <textarea class="span12" name="visita" id="visita" cols="30" rows="5"><?php echo $result->visita?></textarea>
                                        </div>
                                        <div class="span3">
                                            <label for="backup">Aparelho de backup</label>
                                            <textarea class="span12" name="backup" id="backup" cols="30" rows="5"><?php echo $result->backup?></textarea>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 text-right">
                                            <label for="observacoes" class="text-left">Observações</label>
                                            <textarea class="span12" name="observacoes" id="observacoes" cols="30" rows="5"><?php echo $result->observacoes ?></textarea>
                                            
                                            <p style="border: 1px solid;">
                                                <label for="field_queda" style="display: inline-block;margin-right: 6px;">Já sofreu queda ou pancada?</label><input type="checkbox" name="queda" value="S" id="field_queda"><br>
                                            </p>
                                            <p style="border: 1px solid;">
                                                <label for="field_umidade" style="display: inline-block;margin-right: 6px;">Teve contato com a umidade?</label><input type="checkbox" name="umidade" value="S" id="field_umidade"><br>
                                            </p>
                                            <p style="border: 1px solid;">
                                                <label for="field_telaandroid" style="display: inline-block;margin-right: 6px;">Troca de Tela de Android?</label><input type="checkbox" name="telaandroid" value="S" id="field_telaandroid"><br>
                                            </p>
                                            <p style="border: 1px solid;">
                                                <label for="field_parelela1" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, motivo preço.</label><input type="checkbox" name="paralela1" value="S" id="field_parelela1"><br>
                                            </p>
                                            <p style="border: 1px solid;">
                                                <label for="field_parelela2" style="display: inline-block;margin-right: 6px;">Cliente optou por tela paralela, <br> motivo falta de disponibilidade no mercado de peças.</label><input type="checkbox" name="paralela2" value="S" id="field_parelela2"><br>
                                            </p>

                                        </div>
                                        <div class="span6">
                                            <label for="laudoTecnico">Laudo Técnico</label>
                                            <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5"><?php echo $result->laudoTecnico ?></textarea>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <?php if($result->faturado == 0){ ?>
                                            <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                            <?php } ?>
                                            <button class="btn btn-primary" id="btnContinuar"><i class="icon-white icon-ok"></i> Alterar</button>
                                            <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                            <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!-- FIM TAB 0 -->

                        <!-- TAB 1 --> <!--Equipamentos-->
                        <div class="tab-pane<?php if ($tab == 1) echo ' active'; ?>" id="tab1">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <form id="formEquipamentos" action="<?php echo base_url() ?>index.php/os/adicionarEquipamento" method="post">
                                    <div class="span8" style="position:relative;" id="inputsdivequipamentos">
                                        <input type="hidden" name="idEquipamento" id="idEquipamento" />
                                        <input type="hidden" name="idOsEquipamento" id="idOsEquipamento" value="<?php echo $result->idOs?>" />
                                        <label for="">Equipamento</label>
                                        <input type="text" class="span12" name="equipamento" id="equipamento" placeholder="Digite o nome do equipamento" />
                                        <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-equipamento" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Equipamento"><i class="icon-plus icon-white"></i></a>
                                    </div>

                                    <div class="span2">
                                        <label for="">.</label>
                                            <button class="btn btn-success span12" id="btnAdicionarEquipamento"><i class="icon-white icon-plus"></i> Adicionar</button>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <?php if($result->faturado == 0){ ?>
                                            <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                            <?php } ?>
                                            <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                            <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            <div class="span12" id="divEquipamentos" style="margin-left: 0">
                                <table class="table table-bordered" id="tblEquipamentos">
                                    <thead>
                                        <tr>
                                            <th>Equipamento</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Numero de Série</th>
                                            <th>Defeito</th>
                                            <th>Slot</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                        foreach ($equipamentos as $e){
                                            if ($e->idEquipamentos) {
                                            echo '<script type="text/javascript">$(document).ready(function(){$("#inputsdivequipamentos").hide();$("#btnAdicionarEquipamento").hide();});</script>';
                                                                                            
                                            }
                                            $CI = &get_instance();
                                            /*$letras = array(
                                                    0 => 'A',
                                                    1 => 'B',
                                                    2 => 'C',
                                                    3 => 'D',
                                                    4 => 'E',
                                                    5 => 'F',
                                                    6 => 'G',
                                                    7 => 'H',
                                                    8 => 'I',
                                                    9 => 'J',
                                                    10 => 'K',
                                                    11 => 'L',
                                                    12 => 'M',
                                                    13 => 'N',
                                                    14 => 'O',
                                                    15 => 'P',
                                                    16 => 'Q',
                                                    17 => 'R',
                                                    18 => 'S',
                                                    19 => 'T',
                                                    20 => 'U',
                                                    21 => 'V',
                                                    22 => 'X',
                                                    23 => 'Y',
                                                    24 => 'Z'
                                                    );
                                            if ($e->slot < 0)
                                                $slot = '<font color="red"><strong>Atendimento encerrado</strong></font>';
                                            else{
                                               $slotSum = 0;
                                               if($CI->os_model->getSlotPrefix($e->idEquipamentos) == 'G')
                                                $slotSum = count($CI->os_model->getSlot('M'));
                                            $slot = $letras[$e->slot-(floor($e->slot/$franquia->fileiras_slots)*$franquia->fileiras_slots)].ceil(((($e->slot+1)+$slotSum)/($franquia->fileiras_slots)));

                                        }*/
                                            $marcaNome = $CI->os_model->getMarcaNome($e->marcas_id);
                                            $posicao = $CI->os_model->getSlotOcupado($e->equipamento, $e->idFranquia, $result->idOs);
                                            
                                        //echo '<pre>';
                                        //var_dump($e);
                                        //echo '</pre>';

                                        echo '<tr>';
                                        echo '<td>'.$e->equipamento.'</td>';                                                                                
                                        echo '<td>'.$marcaNome.'</td>';
                                        echo '<td>'.$e->modelo.'</td>';
                                        echo '<td>'.$e->mei.'</td>';
                                        echo '<td>'.$CI->getDefeito(json_decode($e->defeito)).'</td>';
                                        //echo '<td> $slot / posicao: '.$posicao.'</td>';
                                        echo '<td>'.$posicao.'</td>';
                                        echo '<td>';
                                        
                                        if($this->permission->checkPermission($this->session->userdata('permissao'),'eEquipamento'))
                                            echo '<a style="margin-right: 1%" href="'.base_url().'index.php/equipamentos/editar/'.$e->idEquipamentos.'" class="btn btn-info tip-top" title="Editar Equipamento"><i class="icon-pencil icon-white"></i></a>';
                                            echo '<a href="" idAcao="'.$e->idEquipamentos_os.'" equiAcao="'.$e->idEquipamentos.'"  marcaAcao="'.$e->marcas_id.'" modeloAcao="'.$e->modelo.'" title="Remover Equipamento da OS" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>';
                                            echo '</tr>';
                                        } ?>
                                   <tr>
                                   </tr>
                               </tbody>
                           </table>
                        </div>
                        <!-- FIM TAB 1 -->
                   </div>
                    <div class="tab-pane<?php if ($tab == 2) echo ' active'; ?>" id="tab2">
                    <div class="span12 well" style="padding: 1%; margin-left: 0">
                        <form id="formProdutos" action="<?php echo base_url() ?>index.php/os/adicionarProduto" method="post">
                            <div class="span8" style="position: relative;">
                                <input type="hidden" name="idProduto" id="idProduto" />
                                <input type="hidden" name="idOsProduto" id="idOsProduto" value="<?php echo $result->idOs?>" />
                                <input type="hidden" name="estoque" id="estoque" value=""/>
                                <input type="hidden" name="preco" id="preco" value=""/>
                                <label for="">Produto</label>
                                <input type="text" class="span12" name="produto" id="produto" placeholder="Digite o nome do produto" />
                                <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-componente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Produto"><i class="icon-plus icon-white"></i></a>
                                <a style="position: absolute;top: 25px;right: 57px;height: 25px;line-height: 25px;" href="#modal-codigo-barras" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="btn btn-warning" title="Escanear código de barras" onclick="modalCodigo();"><i class="icon-barcode icon-white"></i></a>
                            </div>
                            <div class="span2">
                                <label for="">Quantidade</label>
                                <input type="text" placeholder="Quantidade" id="quantidade" name="quantidade" class="span12" />
                            </div>
                            <div class="span2">
                                <label for="">.</label>
                                <button class="btn btn-success span12" id="btnAdicionarProduto"><i class="icon-white icon-plus"></i> Adicionar</button>
                            </div>
                            <div class="span12" style="padding: 1%; margin-left: 0">
                                <div class="span6 offset3" style="text-align: center">
                                    <?php if($result->faturado == 0){ ?>
                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                    <?php } ?>
                                    <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                    <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="span12" id="divProdutos" style="margin-left: 0">
                        <table class="table table-bordered" id="tblProdutos">
                            <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Quantidade</th>
                                    <th>Ações</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($produtos as $p) {
                                    $total = $total + $p->subTotal;
                                    echo '<tr>';
                                    echo '<td>'.$p->descricao.'</td>';
                                    echo '<td>'.$p->quantidade.'</td>';
                                    echo '<td><a href="" idAcao="'.$p->idProdutos_os.'" prodAcao="'.$p->idProdutos.'" quantAcao="'.$p->quantidade.'" title="Excluir Produto" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                    echo '<td>R$ '.number_format($p->subTotal,2,',','.').'</td>';
                                    echo '</tr>';
                                }?>
                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($total,2,',','.');?><input type="hidden" id="total-venda" value="<?php echo number_format($total,2); ?>"></strong></td>
                                </tr>
                            </tbody>
                            <?php $totalP = $total; ?>
                        </table>
                    </div>
                </div>

                <!--Serviços-->
                <div class="tab-pane<?php if ($tab == 3) echo ' active'; ?>" id="tab3">
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <form id="formServicos" action="<?php echo base_url() ?>index.php/os/adicionarServico" method="post">
                                <div class="span10">
                                    <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs?>" />
                                    <input type="hidden" name="idServico" id="idServico"/>
                                    <label for="servico" class="control-label">Nome<span class="required">*</span></label>
                                    <input id="servico" type="text" class="span12" name="servico" placeholder="Digite o nome do serviço" />
                                    <div class="span12" style="padding: 0%; margin-left: 0">
                                      <div class="span2" style="padding: 0%; margin-left: 0; width: auto;">
                                        <label for="precoServico" class="control-label">Preço<span class="required">*</span></label>
                                        <input id="precoServico" name="precoServico" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span9" style="padding-left: 1%; margin-left: 0;">
                                        <label for="descricaoServico" class="control-label">Descrição</label>
                                        <input id="descricaoServico" name="descricaoServico" type="text" style="width: 98%" name="descricao" placeholder="Digite a descrição do serviço" />
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <label for="">.</label>
                                <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                            </div>
                            <div class="span12" style="padding: 1%; margin-left: 0">
                                <div class="span6 offset3" style="text-align: center">
                                    <?php if($result->faturado == 0){ ?>
                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                    <?php } ?>
                                    <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                    <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="span12" id="divServicos" style="margin-left: 0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Serviço (Mão de Obra)</th>
                                    <th>Descrição</th>
                                    <th>Sub-total</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($servicos as $s) {
                                    $preco = $s->precoServico;
                                    $total = $total + $preco;
                                    echo '<tr>';
                                    echo '<td>'.$s->nomeServico.'</td>';
                                    echo '<td>'.$s->descricaoServico.'</td>';
                                    echo '<td>R$ '.number_format($s->precoServico,2,',','.').'</td>';
                                    echo '<td><span idAcao="'.$s->idServicos_os.'" title="Excluir Serviço" class="btn btn-danger"><i class="icon-remove icon-white"></i></span></td>';
                                    echo '</tr>';
                                }?>
                                <tr>
                                    <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($total,2,',','.');?><input type="hidden" id="total-servico" value="<?php echo number_format($total,2); ?>"></strong></td>
                                </tr>
                            </tbody>
                            <?php $totalS = $total; ?>
                        </table>
                    </div>
                </div>
            </div>
            <!--Pagamentos-->
            <div class="tab-pane<?php if ($tab == 4) echo ' active'; ?>" id="tab4">
                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span12 well" style="padding: 1%; margin-left: 0">
                        <form id="formPagamentos" action="<?php echo base_url() ?>index.php/os/adicionarPagamento" method="post">
                            <div class="span10"> 
                                <input type="hidden" name="idOsPagamento" id="idOsPagamento" value="<?php echo $result->idOs?>" />       
                                <label for="formaPg">Data<span class="required">*</span></label>
                                <input id="data" class="span12 datepicker" type="text" name="data" value="<?php echo date('d/m/Y'); ?>">
                                <label for="formaPg">Forma de Pagamento<span class="required">*</span></label>
                                <select name="formaPg" id="formaPg" class="span12">
                                   <option value="Dinheiro">Dinheiro</option>
                                   <option value="Cartão de Crédito">Cartão de Crédito</option>
                                   <option value="Cartão de Débito">Cartão de Débito</option>
                                   <option value="Desconto">Entrada</option>
                               </select>         
                               <div class="span12" style="padding: 0%; margin-left: 0">
                                    <div class="span2" style="padding: 0%; margin-left: 0; width: auto;">
                                        <label for="valorPg" class="control-label">Valor<span class="required">*</span></label>
                                        <input id="valorPg" name="valorPg" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span2" style="padding-left: 1%; margin-left: 0; width: auto;">
                                        <label for="valorDesc" class="control-label">Desconto</label>
                                        <input id="valorDesc" name="valorDesc" class="money" class="span2" type="text" placeholder="0,00" value=""  />
                                    </div>
                                    <div class="span6" style="padding-left: 1%; margin-left: 0;">
                                        <label for="dadosPg" class="control-label">Observações</label>
                                        <input id="dadosPg" name="dadosPg" type="text" style="width: 98%" name="descricao" placeholder="Digite dados do pagamento" />
                                    </div>
                            </div>
                        </div>
                        <div class="span2">
                            <label for="">.</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                        </div>
                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <?php echo '<a title="" href="'.base_url().'agile/gerar-nfe.php?idOs='.$result->idOs.'&idServicos='.(@$servicos[0]->idServicos_os).'&idFranquia='.$this->session->userdata('id').'" class="btn btn-warning" target="_blank"><i class="icon-file icon-white"></i> Gerar NF-e</a>'; ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="span12" id="divPagamentos" style="margin-left: 0">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Forma de pagamento</th>
                                <th>Dados</th>
                                <th>Valor</th>
                                <th>Desconto</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            foreach ($pagamentos as $p)
                            {
                                $total = $total + $p->valorPg - $p->valorDesc;
                                echo '<tr>';
                                echo '<td>'.$p->formaPg.'</td>';
                                echo '<td>'.$p->dadosPg.'</td>';
                                echo '<td>R$ '.number_format($p->valorPg,2,',','.').'</td>';
                                echo '<td>R$ '.number_format($p->valorDesc,2,',','.').'</td>';
                                echo '<td><span idAcao="'.$p->idPagamento_os.'" title="Excluir Pagamento" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></span></td>';
                                echo '</tr>';
                            }?>
                            <tr>
                                <td colspan="2" style="text-align: right"><strong>Total em aberto:</strong></td>
                                <td><strong>R$ <?php echo number_format((($totalP + $totalS) - $total),2,',','.');?><input type="hidden" id="total-pagamento" value="<?php echo number_format($total,2); ?>"></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <!--Anexos-->
        <div class="tab-pane<?php if ($tab == 5) echo ' active'; ?>" id="tab5">
            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12 well" style="padding: 1%; margin-left: 0" id="form-anexos">
                    <form id="formAnexos" enctype="multipart/form-data" action="javascript:;" accept-charset="utf-8"s method="post">
                        <div class="span10">
                            <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs?>" />
                            <label for="">Anexo</label>
                            <input type="file" class="span12" name="userfile[]" multiple size="20" />
                        </div>
                        <div class="span2">
                            <label for="">.</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Anexar</button>
                        </div>
                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="span12" id="divAnexos" style="margin-left: 0">
                    <?php 
                        $cont = 1;
                        $flag = 5;
                        foreach ($anexos as $a):

                            if($a->thumb == null){
                                $thumb = base_url().'assets/img/icon-file.png';
                                $link = base_url().'assets/img/icon-file.png';
                            }
                            else{
                                $thumb = base_url().'assets/anexos/thumbs/'.$a->thumb;
                                $link = $a->url.$a->anexo;
                            }

                            if($cont == $flag){
                                echo '<div style="margin-left: 0" class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                                $flag += 4;
                            }
                            else{
                                echo '<div class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                            }

                            $cont++;

                        endforeach; 
                    ?>
                </div>
            </div>
        </div>
        <!--Fim Anexos-->

        <!--Observações Internas-->
        <div class="tab-pane<?php if ($tab == 6) echo ' active'; ?>" id="tab6">
            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12 well" style="padding: 1%; margin-left: 0" id="form-observacoes-internas">
                    <form id="formObservacoesInternas" action="javascript:;" accept-charset="utf-8"s method="post">

                        <div class="span10">
                            <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $result->idOs ?>" />
                            <label for="observacoes_internas">Observações Internas</label>
                            <input id="observacoes_internas" type="text" class="span12" name="observacoes_internas" size="200" />
                        </div>

                        <div class="span2">
                            <label for="">&nbsp;</label>
                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i> Adicionar</button>
                        </div>

                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span6 offset3" style="text-align: center">
                                <?php if($result->faturado == 0){ ?>
                                <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                <?php } ?>
                                <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>

                    </form>
                </div>
                
                <div class="span12" id="divObservacoesInternas" style="margin-left: 0">
                    <?php 
                        $cont = 1;
                        $flag = 5;
                        foreach ($observacoesInternas as $obs_interna):

                            echo '<p><span style="display:block-inline;width:150px;">' . date(('d/m/Y H:i'), strtotime($obs_interna->data)) . ' - </span>' . $obs_interna->observacao_interna . '</p>';
                            //echo $obs_interna->observacao_interna . ' - ' . date(('d/m/Y H:i'), strtotime($obs_interna->data)) . ' <br>';

                        //     if($a->thumb == null){
                        //         $thumb = base_url().'assets/img/icon-file.png';
                        //         $link = base_url().'assets/img/icon-file.png';
                        //     }
                        //     else{
                        //         $thumb = base_url().'assets/anexos/thumbs/'.$a->thumb;
                        //         $link = $a->url.$a->anexo;
                        //     }

                        //     if($cont == $flag){
                        //         echo '<div style="margin-left: 0" class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                        //         $flag += 4;
                        //     }
                        //     else{
                        //         echo '<div class="span3"><a href="#modal-anexo" imagem="'.$a->idAnexos.'" link="'.$link.'" role="button" class="btn anexo" data-toggle="modal"><img src="'.$thumb.'" alt=""></a></div>'; 
                        //     }

                        //     $cont++;

                        endforeach; 
                    ?>
                </div>
            </div>
        </div>
        <!--Fim Observações Internas-->
        
    </div>
</div>
.
</div>
</div>
</div>
</div>
<div id="modal-inserir-cliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/clientes/inserirClienteAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Cliente</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="nomeCliente" placeholder="Nome do cliente">
            <input type="text" name="rgCliente" placeholder="RG">
            <input type="text" name="telefoneCliente" placeholder="Telefone">
            <input type="text" name="emailCliente" placeholder="Email">
            <input type="text" name="cepCliente" placeholder="CEP">
            <input type="text" name="ruaCliente" placeholder="Rua">
            <input type="text" name="numeroCliente" placeholder="Número">
            <input type="text" name="bairroCliente" placeholder="Bairro">
            <input type="text" name="cidadeCliente" placeholder="Cidade">
            <input type="text" name="estadoCliente" placeholder="Estado">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnInserirClienteAjax">Inserir</button>
        </div>
    </form>
</div>
<!-- Modal excluir produto -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>
<div id="modal-inserir-equipamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/equipamentos/inserirEquipamentoAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Eqluipamento</h5>
        </div>
        <div class="modal-body">
            <select name="tipoEquipamento">
               <option value="">Equipamento...</option>
               <?php foreach(unserialize($franquia->permissoes_equip) as $e){ ?>
               <option value="<?php echo $e; ?>"><?php echo $e; ?></option>
               <?php } ?>
           </select>
           <input type="text" id="equip_marca" name="marcaEquipamento" placeholder="Marca">
           <input id="equip_marcas_id" type="hidden" class="span12" name="marcas_id" value="363">
           <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value="<?php echo $result->clientes_id ?>"  />
           <input type="text" name="meiEquipamento" placeholder="Numero de Serie / MEI">
           <input type="text" name="modeloEquipamento" placeholder="Modelo">
           <input type="text" name="obsEquipamento" placeholder="Observação">
           <h5 style="text-align: center"></h5>
       </div>
       <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
         <button class="btn btn-primary" id="btnInserirEquipamentoAjax">Inserir</button>
     </div>
 </form>
</div>
<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Visualizar Anexo</h3>
</div>
<div class="modal-body">
    <div class="span12" id="div-visualizar-anexo" style="text-align: center">
        <div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
    <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
    <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
</div>
</div>
<!-- Modal Faturar-->
<div id="modal-faturar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formFaturar" action="<?php echo current_url() ?>" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Faturar Venda</h3>
      </div>
      <div class="modal-body">
        <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>
        <div class="span12" style="margin-left: 0"> 
          <label for="descricao">Descrição</label>
          <input class="span12" id="descricao" type="text" name="descricao" value="Fatura de Venda - #<?php echo $result->idOs; ?> "  />
      </div>  
      <div class="span12" style="margin-left: 0"> 
          <div class="span12" style="margin-left: 0"> 
            <label for="cliente">Cliente*</label>
            <input class="span12" id="cliente" type="text" name="cliente" value="<?php echo $result->nomeCliente ?>" />
            <input type="hidden" name="clientes_id" id="clientes_id" value="<?php echo $result->clientes_id ?>">
            <input type="hidden" name="os_id" id="os_id" value="<?php echo $result->idOs; ?>">
        </div>
    </div>
    <div class="span12" style="margin-left: 0"> 
      <div class="span4" style="margin-left: 0">  
        <label for="valor">Valor*</label>
        <input type="hidden" id="tipo" name="tipo" value="receita" /> 
        <input class="span12 money" id="valor" type="text" name="valor" value="<?php echo number_format($total,2); ?> "  />
    </div>
    <div class="span4" >
        <label for="vencimento">Data Vencimento*</label>
        <input class="span12 datepicker" id="vencimento" type="text" name="vencimento"  />
    </div>
</div>
<div class="span12" style="margin-left: 0"> 
  <div class="span4" style="margin-left: 0">
    <label for="recebido">Recebido?</label>
    &nbsp &nbsp &nbsp &nbsp <input  id="recebido" type="checkbox" name="recebido" value="1" /> 
</div>
<div id="divRecebimento" class="span8" style=" display: none">
    <div class="span6">
      <label for="recebimento">Data Recebimento</label>
      <input class="span12 datepicker" id="recebimento" type="text" name="recebimento" /> 
  </div>
  <div class="span6">
      <label for="formaPgto">Forma Pgto</label>
      <select name="formaPgto" id="formaPgto" class="span12">
        <option value="Dinheiro">Dinheiro</option>
        <option value="Cartão de Crédito">Cartão de Crédito</option>
        <option value="Cheque">Cheque</option>
        <option value="Boleto">Boleto</option>
        <option value="Depósito">Depósito</option>
        <option value="Débito">Débito</option>        
    </select> 
</div>
</div>
</div>
<div class="modal-footer">
  <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
  <button class="btn btn-primary">Faturar</button>
</div>
</form>
</div>


<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">

    $(document).ready(function(){

        $("#marca").autocomplete({
            source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
            minLength: 1,
            select: function( event, ui ) {
                $("#componenteMarcas_id").val(ui.item.id);
            }
        });

        $("#equip_marca").keyup(function(e){
            if($(this).val() != ""){
                $(this).autocomplete({
                    source: "<?php echo site_url(); ?>/equipamentos/autoCompleteMarca",
                    minLength: 1,
                    select: function( event, ui) {
                        if(ui.item.id != undefined){
                            $("#equip_marcas_id").val(ui.item.id);
                        }else{
                            $("#equip_marcas_id").val("");
                        }
                    }
                });
            }else{
                $("#equip_marcas_id").val("");
            }
        });
         
        $('#recebido').click(function(event) {
            var flag = $(this).is(':checked');
            if(flag == true){
                $('#divRecebimento').show();
            }
            else{
                $('#divRecebimento').hide();
            }
        });

        $(document).on('click', '#btn-faturar', function(event) {
            event.preventDefault();
            valor = $('#total-venda').val();
            total_servico = $('#total-servico').val();
            valor = valor.replace(',', '' );
            total_servico = total_servico.replace(',', '' );
            total_servico = parseFloat(total_servico); 
            valor = parseFloat(valor);
            $('#valor').val(valor + total_servico);
        });

        $("#formFaturar").validate({
            rules:{
                descricao: {required:true},
                cliente: {required:true},
                valor: {required:true},
                vencimento: {required:true}
            },
            messages:{
                descricao: {required: 'Campo Requerido.'},
                cliente: {required: 'Campo Requerido.'},
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            },
            submitHandler: function( form ) {

                var dados = $( form ).serialize();
                $('#btn-cancelar-faturar').trigger('click');
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/faturar",
                    data: dados,
                    dataType: 'json',
                    success: function(data)
                    {
                    if(data.result == true){
                        window.location.reload(true);
                    }
                    else{
                        alert('Ocorreu um erro ao tentar faturar OS.');
                        $('#progress-fatura').hide();
                    }
                }

                });
                return false;
            }
        });

        $("#produto").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteProduto",
            minLength: 2,
            select: function( event, ui ) {
               $("#idProduto").val(ui.item.id);
               $("#estoque").val(ui.item.estoque);
               $("#preco").val(ui.item.preco);
               $("#quantidade").focus();
            }
        });

        $("#equipamento").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteEquipamento",
            minLength: 2,
            select: function( event, ui ) {
                $("#idEquipamento").val(ui.item.id);
            }
        });

        $("#servico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteServico",
            minLength: 2,
            select: function( event, ui )
            {
                $("#idServico").val(ui.item.id);
                $("#precoServico").val(ui.item.preco);
                $("#descricaoServico").val(ui.item.descricao);
            }
        });

        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 2,
            select: function( event, ui ) {
               $("#clientes_id").val(ui.item.id);
            }
        });
        
        $("#atendente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteAtendente",
            minLength: 1,
            select: function( event, ui ){
                $("#atendentes_id").val(ui.item.id);
            }
        });

        $("#formOs").validate({
            rules:{
                cliente: {required:true},
                tecnico: {required:true},
                dataInicial: {required:true},
                garantia: {required:true}
            },
            messages:{
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataInicial: {required: 'Campo Requerido.'},
                garantia: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formPagamentos").validate({
            rules:
            {
                formaPg: {required:true},
                valorPg: {required:true}
            },
            messages:{
                formaPg: {required: 'Insira uma forma de pagamento'},
                valorPg: {required: 'Insira o valor do pagamento'}
            },
            submitHandler: function( form )
            {       
                var dados = $( form ).serialize();
                $("#divPagamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/adicionarPagamento",
                    data: dados,
                    dataType: 'json',
                    success: function(data)
                    {
                        if(data.result == true){
                            $( "#divPagamentos" ).load("<?php echo current_url();?> #divPagamentos" );
                            $("#dadosPg").val('');
                            $("#valorPg").val('');
                            $("#formaPg").focus();
                        }
                        else{
                            alert('Ocorreu um erro ao tentar adicionar serviço.');
                        }
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules:{
                quantidade: {required:true} 
            },
            messages:{
                quantidade: {required: 'Insira a quantidade'}
            },
            submitHandler: function( form ){

                var quantidade = parseInt($("#quantidade").val());
                var estoque = parseInt($("#estoque").val());

                if(estoque < quantidade){

                    alert('Você não possui estoque suficiente.');

                } else {

                    var dados = $( form ).serialize();
                    
                    $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/os/adicionarProduto",
                        data: dados,
                        dataType: 'json',
                        success: function(data) {
                            if(data.result == true){
                                $( "#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                                $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                                $("#quantidade").val('');
                                $("#produto").val('').focus();
                            }
                            else{
                                alert('Ocorreu um erro ao tentar adicionar produto.');
                            }
                        }
                    });
                    
                    return false;
                }
            }
        });

        $("#formEquipamentos").validate({
          rules:{
           equipamento: {required:true} 
       },
       messages:{
           equipamento: {required: 'Insira o nome do equipamento'}
       },
       submitHandler: function( form )
       {
          var dados = $( form ).serialize();
          $("#divEquipamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
          $.ajax(
          {
              type: "POST",
              url: "<?php echo base_url();?>index.php/os/adicionarEquipamento",
              data: dados,
              dataType: 'json',
              success: function(data)
              {
                console.log(data);
                if(data.result == true)
                {
                 $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                 $("#equipamento").val('');
                 $("#marca_id").val('');
                 //alert("OS criada com sucesso!");
                 /*window.location = "<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>";*/
                $("#btnAdicionarEquipamento").hide();
                $("#inputsdivequipamentos").hide();
             }
             else
             {
                 alert("Não há mais espaço para adicionar um equipamento, favor liberar para continuar adicionando;");
                 $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                 $("#equipamento").val('');
                 $("#marca_id").val('');
             }
            //alert("OS criada com sucesso!");
            /*
            window.location = "<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>";*/
         },
         error: function(){
             $("#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
             $("#equipamento").val('');
             $("#marca_id").val('');
         }
     });
          return false;
      }
  });
        $("#formServicos").validate({
          rules:{
           servico: {required:true},
           precoServico: {required:true}
       },
       messages:{
           servico: {required: 'Insira um serviço'},
           precoServico: {required: 'Insira o valor do serviço'}
       },
       submitHandler: function( form ){       
           var dados = $( form ).serialize();
           $("#divServicos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
           $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>index.php/os/adicionarServico",
              data: dados,
              dataType: 'json',
              success: function(data)
              {
                if(data.result == true){
                    $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    $( "#divServicos" ).load("<?php echo current_url();?> #divServicos" );
                    $("#precoServico").val('');
                    $("#descricaoServico").val('');
                    $("#servico").val('').focus();
                }
                else{
                    alert('Ocorreu um erro ao tentar adicionar serviço.');
                }
            }
        });
           return false;
       }
   });

        $("#formAnexos").validate({
          submitHandler: function( form ){       
                //var dados = $( form ).serialize();
                var dados = new FormData(form); 
                $("#form-anexos").hide('1000');
                $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/anexar",
                  data: dados,
                  mimeType:"multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData:false,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divAnexos" ).load("<?php echo current_url();?> #divAnexos" );
                        $("#userfile").val('');
                    }
                    else{
                        $("#divAnexos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> '+data.mensagem+'</div>');      
                    }
                },
                error : function() {
                  $("#divAnexos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');      
              }
          });
                $("#form-anexos").show('1000');
                return false;
            }
        });

        $("#formObservacoesInternas").validate({
            submitHandler: function( form ){       
                //var dados = $( form ).serialize();
                var dados = new FormData(form); 
                $("#form-observacoes-internas").hide('1000');
                $("#divObservacoesInternas").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/os/adicionarObservacoesInternas",
                    data: dados,
                    // mimeType:"multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'json',
                    success: function(data) {
                        if(data.result == true){
                            $("#divObservacoesInternas" ).load("<?php echo current_url();?> #divObservacoesInternas" );
                            $("#observacoes_internas").val(null);
                        }
                        else{
                            $("#divObservacoesInternas").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> '+data.mensagem+'</div>');      
                        }
                    },
                    error : function() {
                        $("#divObservacoesInternas").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você informou a observação.</div>');      
                    }
                });
                $("#form-observacoes-internas").show('1000');
                return false;
            }
        });


        $(document).on('click', '#divEquipamentos a', function(event) {
            var idEquipamento = $(this).attr('idacao');
            if((idEquipamento % 1) == 0)
            {
                $("#divEquipamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirEquipamento",
                  data: "idEquipamento="+idEquipamento,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true)
                    {
                        $( "#divEquipamentos" ).load("<?php echo current_url();?> #divEquipamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir equipamento.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '#divProdutos a', function(event) {
            var idProduto = $(this).attr('idAcao');
            var quantidade = $(this).attr('quantAcao');
            var produto = $(this).attr('prodAcao');
            if((idProduto % 1) == 0){
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirProduto",
                  data: "idProduto="+idProduto+"&quantidade="+quantidade+"&produto="+produto,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $( "#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                        
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir produto.');
                    }
                }
            });
                return false;
            }
            
        });
        $(document).on('click', '#divServicos span', function(event)
        {
            var idServico = $(this).attr('idAcao');
            if((idServico % 1) == 0){
                $("#divServicos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirServico",
                  data: "idServico="+idServico,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divServicos").load("<?php echo current_url();?> #divServicos" );
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir serviço.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '#divPagamentos span', function(event)
        {
            var idPagamento = $(this).attr('idAcao');
            if((idPagamento % 1) == 0){
                $("#divPagamentos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/os/excluirPagamento",
                  data: "idPagamento="+idPagamento,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true){
                        $("#divPagamentos").load("<?php echo current_url();?> #divPagamentos" );
                    }
                    else{
                        alert('Ocorreu um erro ao tentar excluir pagamento.');
                    }
                }
            });
                return false;
            }
        });
        $(document).on('click', '.anexo', function(event) {
         event.preventDefault();
         var link = $(this).attr('link');
         var id = $(this).attr('imagem');
         var url = '<?php echo base_url(); ?>os/excluirAnexo/';
         $("#div-visualizar-anexo").html('<img src="'+link+'" alt="">');
         $("#excluir-anexo").attr('link', url+id);
         $("#download").attr('href', "<?php echo base_url(); ?>index.php/os/downloadanexo/"+id);
     });
        $(document).on('click', '#excluir-anexo', function(event) {
         event.preventDefault();
         var link = $(this).attr('link'); 
         $('#modal-anexo').modal('hide');
         $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
         $.ajax({
          type: "POST",
          url: link,
          dataType: 'json',
          success: function(data)
          {
            if(data.result == true){
                $("#divAnexos" ).load("<?php echo current_url();?> #divAnexos" );
            }
            else{
                alert(data.mensagem);
            }
        }
    });
     });
        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
        $(document).on('click', '#btnInserirClienteAjax', function(event) {
            var preenchido = true;
            $(this).closest("form").find("input[type=text]").each(function(){
                if($(this).val() == ""){
                    preenchido = false;
                    return false;
                }
            });
            if(!preenchido){
                alert("Informe todos os campos do cliente pra inserir!");
            }else{
                var dados = $(this).closest("form").serialize();
                console.log(dados); 
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/clientes/inserirClienteAjax",
                    data: dados,
                    dataType: 'json',
                    success: function(data){
                        if(data.result == true){
                            $('#modal-inserir-cliente').modal('hide');
                            $("#cliente").val(data.nomeCliente);
                            $("#clientes_id").val(data.idCliente);
                        }else{
                            alert("Ocorreu um erro ao tentar adicionar um cliente!");
                        }
                    },
                    error: function(){
                        alert("ERRO!");
                    }
                });
            }
            return false;
        });
        $(document).on('click', '#btnInserirEquipamentoAjax', function(event) {
          var preenchido = true;
          $(this).closest("form").find("input[type=text], select").each(function(){
             if($(this).val() == "" && $(this).attr('name') != 'obsEquipamento'){
                preenchido = false;
                return false;
            }
        });
          if(!preenchido){
             alert("Informe todos os campos do equipamento pra inserir!");
         }else{
             var dados = $(this).closest("form").serialize();
             $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/equipamentos/inserirEquipamentoAjax",
                data: dados,
                dataType: 'json',
                success: function(data){
                   if(data.result == true){
                    var tipoEquipamento = $('#modal-inserir-equipamento select[name=tipoEquipamento]').val(),
                    marcaEquipamento    = $('#modal-inserir-equipamento input[name=marcaEquipamento]').val(),
                    meiEquipamento      = $('#modal-inserir-equipamento input[name=meiEquipamento]').val(),
                    modeloEquipamento   = $('#modal-inserir-equipamento input[name=modeloEquipamento]').val();
                    $('#modal-inserir-equipamento').modal('hide');
                    $('#idEquipamento').val(data.idEquipamento);
                    $('#equipamento').val(tipoEquipamento+' | '+marcaEquipamento+', IMEI '+meiEquipamento+' - MODELO: '+modeloEquipamento);
                    window.open("<?php echo base_url() ?>index.php/equipamentos/editar/"+data.idEquipamento+"/true");
                }else{
                  alert("Ocorreu um erro ao tentar adicionar um equipamento!");
              }
          },
          error: function(){
           alert("ERRO!");
       }
   });
         }
         return false;
     });
    $('input[name=queda]').on('change', function(){
        var val = $('textarea[name=observacoes]').val();
        
        if($('input[name=queda]').is(':checked')){
            if(val != ""){
                val += '\n';            
            }
            val += 'ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção destes danos terá custo adicional de acordo com cada equipamento. \n';            
        }else{
            val = val.split('ATENÇÃO: Equipamentos que "sofreram" queda ou pancada estão sujeitos a danos total ou parcial após manutenção corretiva ou preventiva, a manutenção destes danos terá custo adicional de acordo com cada equipamento.').join('').trim();
        }
        $('textarea[name=observacoes]').val(val);
    });
    $('input[name=umidade]').on('change', function(){
        var val = $('textarea[name=observacoes]').val();
        
        if($('input[name=umidade]').is(':checked')){
            if(val != ""){
                val += '\n';            
            }
            val += 'ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo não ligar após este procedimento por questões adversas. \n';            
        }else{
            val = val.split('ATENÇÃO: A garantia do reparo se estende apenas ao item substituído ou serviço realizado, em caso de danos posteriores a desoxidação terá custo adicional após nova avaliação técnica. Em situações onde o equipamento ainda está ligando e precisa passar pelo processo de desoxidação, poderá o mesmo não ligar após este procedimento por questões adversas.').join('').trim();
        }
        $('textarea[name=observacoes]').val(val);
    });

    $('input[name=telaandroid]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=telaandroid]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente. ';

        }else{
            val = val.split('ATENÇÃO: após trocar a tela de aparelho Android por uma tela que não seja a original, o mesmo não deverá ser atualizado para novas versões Android. A partir da atualização do Android Oreo 8 o touch irá parar de funcionar caso seja atualizado, e a correção das funcionalidades do touch será custeada pelo cliente.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });


    $('input[name=paralela1]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela1]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo preço. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo preço.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    $('input[name=paralela2]').on('change', function(){

        var val = $('textarea[name=observacoes]').val();

        if($('input[name=paralela2]').is(':checked')){

            if (val != "") {
                val += '\n';
            }

            val += 'ATENÇÃO: Cliente optou por tela paralela, motivo indisponibilidade no mercado de peças. ';

        }else{
            val = val.split('ATENÇÃO: Cliente optou por tela paralela, motivo indisponibilidade no mercado de peças.').join('').trim();

        }

        $('textarea[name=observacoes]').val(val);

    });

    });

    function modalCodigo() {
        $("#modal-codigo-barras .modal-header h5").text('Escanear código de barras').find('i').remove();
        $("#modal-codigo-barras .modal-body .div-scan").show();
        $("#modal-codigo-barras .modal-body .div-wrapper").addClass('hidden');
    }

</script>

