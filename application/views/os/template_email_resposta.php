<html lang="pt-br">

<head>

<title>ASSISTÊNCIA TÉCNICA ONLINE</title>

<meta charset="UTF-8" />

</head>

<body>

<table width="700px" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6">

<tbody><tr align="center" height="85px">

		<td style="border-bottom:1px solid #e2e2e2">

			<a href="<?php echo site_url(); ?>" alt="Rede Multi Assistência" title="Acessar site">

				<img src="<?php echo base_url();?>assets/img/logo.png" style="width: 150px;">

			</a>

		</td>

	</tr>

	<tr align="center" height="120">

		<td style="padding: 0 15px;">

			<br>

			<br>

			<font face="Arial, Helvetica, sans-serif" color="#616163" size="5"><strong>Você tem uma nova mensagem de nossa equipe!</strong></font>

			<br>

			<br>

			<font face="Arial, Helvetica, sans-serif" color="#959595" size="2">
                
                
				<p>
				<?php if(@$qualMsg == '1'){ ?>
				Olá!<br>
				Valor em <?php echo $vezes; ?>X de R$ <?php echo $parcela; ?> <?php echo $juros; ?> nos cartões ou R$ <?php echo $preco; ?> á vista.<br>
				<?php echo !empty($mensagem) ? $mensagem : ''; ?><br><br>
				Quer comodidade? Pague agora que iremos agendar o dia do conserto, seu aparelho será consertado no mesmo dia. Você pode pagar com o cartão, de forma online, ou fazer uma transferência/depósito para nossa conta no banco e enviar o comprovante.<br>
				
				<a href="<?php echo site_url('sistemaos/cliente_pagamento_orcamento/'.$idOrcamento); ?>" target="_blank" style="color: white;text-decoration: none;background: #0f70b7;padding: 10px 20px;font-size: 18px;margin-top: 30px;display: inline-block;">Pagar com cartão</a> <a href="<?php echo site_url('sistemaos/comprovante_pagamento/'.$idOrcamento); ?>" target="_blank" style="color: white;text-decoration: none;background: #0f70b7;padding: 10px 20px;font-size: 18px;margin-top: 30px;margin-left: 5px;display: inline-block;">Enviar comprovante</a>
				<?php if(!empty($dados_bancarios)){ ?>
				<br><br><br><strong>Dados bancários</strong>
				<?php echo $dados_bancarios; ?>
				<?php } ?>
				<?php if($filial == 'S'){ ?>
                    Para sua região, atendemos com serviço delivery!<br>
                    Nosso pessoal de delivery retira em sua casa ou seu local de trabalho, traz até nossa loja, em seguida consertamos e entregamos novamente em sua casa ou trabalho, tudo isso já incluso no valor do conserto.
				<?php } ?>
				<?php }elseif(@$qualMsg == '2'){ ?>
				Olá!<br>
                No caso deste defeito, para um orçamento correto, nossos
                técnicos verificaram a necessidade de uma análise técnica do equipamento em nosso laboratório técnico.<br>
				<?php echo !empty($mensagem) ? $mensagem : ''; ?>
				<?php }else{ ?>
					<?php echo !empty($mensagem) ? $mensagem : ''; ?><br><br>
				<?php } ?>
				</p>


			</font>

			<br>

			<br>

			<br>

		</td>

	</tr>

	<?php if(!empty($nome) || !empty($tel) || !empty($marca) || !empty($modelo) || !empty($defeito) || !empty($idOs) || !empty($dataInicial) || !empty($dataFinal) || !empty($laudoTecnico) || !empty($descricaoProduto) || !empty($obs)){ ?>
	<tr>

		<td><table width="532" border="0" align="center" cellspacing="0" cellpadding="15" bgcolor="#ffffff" style="background:#ffffff;border-radius:5px;border:1px solid #e2e2e2!important">

			<tbody><tr>

				<td width="500" style="padding: 0;">

				<table cellspacing="0" style="background-color:#fff;border:1px #0088cc solid;border-radius:6px;width:100%;text-align:left;color:#0088cc;font-size:1.05em">

					<tbody>





						<?php if(!empty($nome)){ ?>

						<tr>

							<td style="padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px"><font face="Arial, Helvetica, sans-serif" size="2">Nome:</font></td>

							<td style="padding:10px 15px;font-size:0.9em;text-decoration:none!important"><font face="Arial, Helvetica, sans-serif" size="2"><?php echo $nome; ?></font></td>

						</tr>

						<?php } ?>



						<?php if(!empty($tel)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Celular:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><a href="tel:<?php echo $tel; ?>" target="_blank"><?php echo $tel; ?></a></td>

						</tr>

						<?php } ?>





						<?php if(!empty($marca)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Marca do aparelho:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $marca; ?></td>

						</tr>

						<?php } ?>







						<?php if(!empty($modelo)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Modelo:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $modelo; ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($defeito)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Defeito:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $defeito; ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($idOs)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Nº do serviço:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $idOs; ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($dataInicial)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Início:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo date_usa2br($dataInicial); ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($dataFinal)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Término:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo date_usa2br($dataFinal); ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($laudoTecnico)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Laudo Técnico:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $laudoTecnico; ?></td>

						</tr>

						<?php } ?>



						<?php if(!empty($descricaoProduto)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Descrição:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $descricaoProduto; ?></td>

						</tr>

						<?php } ?>

						

						<?php if(!empty($obs)){ ?>

						<tr>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;border-right:1px #0088cc solid;font-size:0.8em;font-weight:bold;width:140px">Observação:</td>

							<td style="border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important"><?php echo $obs; ?></td>

						</tr>

						<?php } ?>



				</tbody>

			</table></td>

		</tr>

	</tbody></table></td>

</tr>
<?php } ?>

<tr>



	<?php if(empty($idOs) && !empty($nome)){ ?>

	<td>

		<img src="<?php echo base_url(); ?>assets/img/os_validate.png" alt="Validade" style="margin-left: 80px;">

	</td>

	<?php } ?>



</tr>

<?php if(!empty($email_marketing)){ ?>

<tr>



	<td align="center" valign="top" style="padding: 20px 0;">

		<font face="Arial, Helvetica, sans-serif" color="#a7a5a5" size="3" style="display: block;margin-bottom: 20px;"><strong>Promoções e novidades</strong></font>

		<img src="<?php echo $email_marketing; ?>" alt="E-mail Marketing" style="max-width: 700px;">

	</td>



</tr>

<?php } ?>

<tr>

	<td>

		<table width="700" border="0" align="center" cellspacing="15" cellpadding="30" bgcolor="#0088cc">

			<tbody><tr>



				<?php if(!empty($facebook)){ ?>

				<td>

					<a href="<?php echo $facebook; ?>" alt="Facebook" title="Facebook" target="_blank"><img src="<?php echo base_url(); ?>assets/img/email_face.png"></a>

				</td>

				<?php } ?>



				<?php if(!empty($ass)){ ?>

				<td align="right" valign="top" style="padding: 0;">

					<img src="<?php echo $ass; ?>" alt="Assinatura">

				</td>

				<?php } ?>



			</tr>

		</tbody></table>

	</td>

</tr>

</tbody></table></body></html>