<div class="span6" style="margin-left: 0">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-th-list"></i>
        </span>
                <h5>Minha Conta</h5>
            </div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12" style="min-height: 260px">
                        <ul class="site-stats">
                            <li class="bg_ls span12"><strong>Nome: <?php echo $franquia->nome?></strong></li>
                            <li class="bg_lb span12" style="margin-left: 0"><strong>Telefone: <?php echo $franquia->telefone?></strong></li>
                            <li class="bg_lg span12" style="margin-left: 0"><strong>Email: <?php echo $franquia->email?></strong></li>
                            <li class="bg_lo span12" style="margin-left: 0"><strong>Nível: <?php echo $franquia->permissao; ?></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="span6">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-th-list"></i>
        </span>
                <h5>Alterar Minha Senha</h5>
            </div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12" style="min-height: 260px">
                        <form id="formSenha" action="<?php echo base_url();?>index.php/sistemaos/alterarSenha" method="post">
                        
                        <div class="span12" style="margin-left: 0">
                            <label for="">Senha Atual</label>
                            <input type="password" id="oldSenha" name="oldSenha" class="span12" />
                        </div>
                        <div class="span12" style="margin-left: 0">
                            <label for="">Nova Senha</label>
                            <input type="password" id="novaSenha" name="novaSenha" class="span12" />
                        </div>
                        <div class="span12" style="margin-left: 0">
                            <label for="">Confirmar Senha</label>
                            <input type="password" name="confirmarSenha" class="span12" />
                        </div>
                        <div class="span12" style="margin-left: 0; text-align: center">
                            <button class="btn btn-primary">Alterar Senha</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span6" style="margin-left: 0">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-th-list"></i>
        </span>
                <h5>Informações de e-mail</h5>
            </div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div>
                        <form method="post" enctype="multipart/form-data">
                          <div class="form-group" style="margin-bottom: 20px;">
                            <label for="face">Facebook [link]:</label>
                            <input type="text" class="form-control" name="facebook" id="face" <?php echo !empty($franquia->facebook) ? 'value="'.$franquia->facebook.'"' : ''; ?>>
                            <div class="clearfix"></div>
                          </div>
                          <div class="form-group" style="margin-bottom: 20px;">
                            <label for="pwd" style="margin-bottom: 0px;">E-mail Marketing [700x200]:</label>
                            <input type="file" class="form-control" name="email_marketing" id="pwd">
                            <div class="clearfix"></div>
                          </div>
                          <?php if(!empty($franquia->email_marketing)){ ?>
                          <div class="form-group" style="margin-bottom: 20px; max-width: 350px; position: relative;">
                            <img src="<?php echo base_url().'assets/uploads/'.$franquia->email_marketing; ?>" alt="E-mail Marketing">
                            <a href="<?php echo site_url(); ?>/sistemaos/excluir_email_marketing" style="background: #c30a0a;color: white;padding: 2px 5px;position: absolute;right: 10px;top: 10px;">X</a>
                          </div>
                          <?php } ?>
                          <div class="form-group" style="margin-bottom: 20px;">
                            <label for="pwd" style="margin-bottom: 0px;">Asinatura de e-mail [305x90]:</label>
                            <input type="file" class="form-control" name="ass_email" id="pwd">
                            <div class="clearfix"></div>
                          </div>
                          <?php if(!empty($franquia->ass_email)){ ?>
                          <div class="form-group" style="margin-bottom: 20px; max-width: 350px; position: relative;">
                            <img src="<?php echo base_url().'assets/uploads/'.$franquia->ass_email; ?>" alt="Assinatura">
                            <a href="<?php echo site_url(); ?>/sistemaos/excluir_ass_email" style="background: #c30a0a;color: white;padding: 2px 5px;position: absolute;right: 10px;top: 10px;">X</a>
                          </div>
                          <?php } ?>
                          
                          <div class="form-group">
                              <input type="hidden" name="id" value="<?php echo $franquia->idFranquias; ?>">
                              <button class="btn btn-primary">Alterar</button>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="span6">
  <div class="widget-box">
    <div class="widget-title">
      <span class="icon">
        <i class="icon-th-list"></i>
      </span>
      <h5>Dados Bancarios e Gerencia Net</h5>
    </div>
    <div class="widget-content">
      <div class="row-fluid">
        <div class="span12" style="min-height: 260px">
          <form id="formCartao" action="<?php echo base_url();?>index.php/sistemaos/alterarGN" method="post">

                    <div class="span12">
                        <label for="identificador_conta" class="control-label">Identificador de Conta<span class="required">*</span></label>
                        <div class="controls">
                            <input id="identificador_conta" type="text" name="identificador_conta" value="<?php echo $franquia->identificador_conta; ?>">
                        </div>
                    </div>

                    <div class="span12">
                        <label for="client_id" class="control-label">Cliente ID<span class="required">*</span></label>
                        <div class="controls">
                            <input id="client_id" type="text" name="client_id" value="<?php echo $franquia->client_id; ?>">
                        </div>
                    </div>

                    <div class="span12">
                        <label for="client_secret" class="control-label">Client Secret<span class="required">*</span></label>
                        <div class="controls">
                            <input id="client_secret" type="text" name="client_secret" value="<?php echo $franquia->client_secret; ?>">
                        </div>
                    </div>

                    <div class="span12">
                        <label for="dados_bancarios" class="control-label">Dados bancários<span class="required">*</span></label>
                        <div class="controls">
                            <textarea id="dados_bancarios" name="dados_bancarios"><?php echo $franquia->dados_bancarios; ?></textarea>
                        </div>
                    </div>

            <div class="span12" style="margin-left: 0; text-align: center">
              <button class="btn btn-primary">Alterar Dados</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formSenha').validate({
            rules :{
                  oldSenha: {required: true},  
                  novaSenha: { required: true},
                  confirmarSenha: { equalTo: "#novaSenha"}
            },
            messages:{
                  oldSenha: {required: 'Campo Requerido'},  
                  novaSenha: { required: 'Campo Requerido.'},
                  confirmarSenha: {equalTo: 'As senhas não combinam.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
    });
</script>