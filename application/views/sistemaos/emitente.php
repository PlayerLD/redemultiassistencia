
<?php if(!isset($dados) || $dados == null) {?>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Dados do Emitente</h5>
            </div>
            <div class="widget-content ">
                <div class="alert alert-danger">Nenhum dado foi cadastrado até o momento. Essas informações 
                estarão disponíveis na tela de impressão de OS.</div>
                <a href="#modalCadastrar" data-toggle="modal" role="button" class="btn btn-success">Cadastrar Dados</a>
            </div>
        </div>    
         
    </div>
</div>   


<div id="modalCadastrar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url(); ?>index.php/sistemaos/cadastrarEmitente" id="formCadastrar" enctype="multipart/form-data" method="post" class="form-horizontal" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">SistemaOs - Cadastrar Dados do Emitente</h3>
  </div>
  <div class="modal-body">
        
        
                    <div class="control-group">
                        <label for="nome" class="control-label">Razão Social<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cnpj" class="control-label"><span class="required">CNPJ*</span></label>
                        <div class="controls">
                            <input class="" type="text" name="cnpj" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">IE*</span></label>
                        <div class="controls">
                            <input type="text" name="ie" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Logradouro*</span></label>
                        <div class="controls">
                            <input type="text" name="logradouro" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Número*</span></label>
                        <div class="controls">
                            <input type="text" name="numero" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Complemento </span></label>
                        <div class="controls">
                            <input type="text" name="complemento" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Bairro*</span></label>
                        <div class="controls">
                            <input type="text" name="bairro" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Cidade*</span></label>
                        <div class="controls">
                            <input type="text" name="cidade" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">UF*</span></label>
                        <div class="controls">
                            <input type="text" name="uf" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Telefone*</span></label>
                        <div class="controls">
                            <input type="text" name="telefone" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">E-mail*</span></label>
                        <div class="controls">
                            <input type="text" name="email" value="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_hora_inicio" class="control-label"><span class="required">Início do expediente*</span></label>
                        <div class="controls">
                            <input id="field_hora_inicio" type="text" name="hora_inicio" value="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_hora_termino" class="control-label"><span class="required">Término do expediente*</span></label>
                        <div class="controls">
                            <input id="field_hora_termino" type="text" name="hora_termino" value="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_fuso_horario" class="control-label"><span class="required">Fuso horário*</span></label>
                        <div class="controls">
                            <select name="fuso_horario" id="field_fuso_horario">
                              <option value="+1">UTC-2</option>
                              <option value="+0">UTC-3</option>
                              <option value="-1">UTC-4</option>
                              <option value="-2">UTC-5</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
                        <div class="controls">
                            <input type="file" name="userfile" value="" />
                        </div>
                    </div>
               

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
    <button class="btn btn-success">Cadastrar</button>
  </div>
  </form>
</div>

<?php } else { ?>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Dados do Emitente</h5>
            </div>
            <div class="widget-content ">
            <div class="alert alert-info">Os dados abaixo serão utilizados no cabeçalho das telas de impressão.</div>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 25%"><img src=" <?php echo $dados[0]->url_logo; ?> "></td>
                            <td> <span style="font-size: 20px; "> <?php echo $dados[0]->nome; ?> </span> </br><span><?php echo $dados[0]->cnpj; ?> </br> <?php echo $dados[0]->rua.', nº:'.$dados[0]->numero.', '.$dados[0]->complemento.', '.$dados[0]->bairro.' - '.$dados[0]->cidade.' - '.$dados[0]->uf; ?> </span> </br> <span> E-mail: <?php echo $dados[0]->email.' - Fone: '.$dados[0]->telefone; ?></span></td>
                        </tr>
                    </tbody>
                </table>

                <a href="#modalAlterar" data-toggle="modal" role="button" class="btn btn-primary">Alterar Dados</a>
                <a href="#modalLogo" data-toggle="modal" role="button" class="btn btn-inverse">Alterar Logo</a>
            </div>
        </div>
    </div>
</div>




<div id="modalAlterar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url(); ?>index.php/sistemaos/editarEmitente" id="formAlterar" enctype="multipart/form-data" method="post" class="form-horizontal" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="">SistemaOs - Editar Dados do Emitente</h3>
  </div>
  <div class="modal-body">
        
        
                    <div class="control-group">
                        <label for="nome" class="control-label">Razão Social<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo $dados[0]->nome; ?>"  />
                            <input id="nome" type="hidden" name="id" value="<?php echo $dados[0]->id; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cnpj" class="control-label"><span class="required">CNPJ*</span></label>
                        <div class="controls">
                            <input class="" type="text" name="cnpj" value="<?php echo $dados[0]->cnpj; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">IE*</span></label>
                        <div class="controls">
                            <input type="text" name="ie" value="<?php echo $dados[0]->ie; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Logradouro*</span></label>
                        <div class="controls">
                            <input type="text" name="logradouro" value="<?php echo $dados[0]->rua; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Número*</span></label>
                        <div class="controls">
                            <input type="text" name="numero" value="<?php echo $dados[0]->numero; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Complemento</span></label>
                        <div class="controls">
                            <input type="text" name="complemento" value="<?php echo $dados[0]->complemento; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Bairro*</span></label>
                        <div class="controls">
                            <input type="text" name="bairro" value="<?php echo $dados[0]->bairro; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Cidade*</span></label>
                        <div class="controls">
                            <input type="text" name="cidade" value="<?php echo $dados[0]->cidade; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">UF*</span></label>
                        <div class="controls">
                            <input type="text" name="uf" value="<?php echo $dados[0]->uf; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">Telefone*</span></label>
                        <div class="controls">
                            <input type="text" name="telefone" value="<?php echo $dados[0]->telefone; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="descricao" class="control-label"><span class="required">E-mail*</span></label>
                        <div class="controls">
                            <input type="text" name="email" value="<?php echo $dados[0]->email; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_hora_inicio" class="control-label"><span class="required">Início do expediente*</span></label>
                        <div class="controls">
                            <input id="field_hora_inicio" type="text" name="hora_inicio" value="<?php echo $dados[0]->hora_inicio; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_hora_termino" class="control-label"><span class="required">Término do expediente*</span></label>
                        <div class="controls">
                            <input id="field_hora_termino" type="text" name="hora_termino" value="<?php echo $dados[0]->hora_termino; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="field_fuso_horario" class="control-label"><span class="required">Fuso horário*</span></label>
                        <div class="controls">
                            <select name="fuso_horario" id="field_fuso_horario">
                              <option value="+1" <?php echo $dados[0]->fuso_horario == "+1" ? 'selected' : ''; ?>>UTC-2 (+1)</option>
                              <option value="+0" <?php echo $dados[0]->fuso_horario == "+0" ? 'selected' : ''; ?>>UTC-3 (Horário de Brasília)</option>
                              <option value="-1" <?php echo $dados[0]->fuso_horario == "-1" ? 'selected' : ''; ?>>UTC-4 (-1)</option>
                              <option value="-2" <?php echo $dados[0]->fuso_horario == "-2" ? 'selected' : ''; ?>>UTC-5 (-2)</option>
                            </select>
                        </div>
                    </div>

    
               

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
    <button class="btn btn-primary">Alterar</button>
  </div>
  </form>
</div>


<div id="modalLogo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url(); ?>index.php/sistemaos/editarLogo" id="formLogo" enctype="multipart/form-data" method="post" class="form-horizontal" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="">SistemaOs - Alterar Logomarca</h3>
  </div>
  <div class="modal-body">
         <div class="span12 alert alert-info">Selecione uma nova imagem da logomarca. Tamanho indicado (130 X 130).</div>          
         <div class="control-group">
            <label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
            <div class="controls">
                <input type="file" name="userfile" value="" />
                <input id="nome" type="hidden" name="id" value="<?php echo $dados[0]->id; ?>"  />
            </div>
        </div>

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
    <button class="btn btn-primary">Alterar</button>
  </div>
  </form>
</div>



<?php } ?>


<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
    
$(document).ready(function(){

    $("#formLogo").validate({
      rules:{
         userfile: {required:true}
      },
      messages:{
         userfile: {required: 'Campo Requerido.'}
      },

        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
            $(element).parents('.control-group').removeClass('success');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
   });


    $("#formCadastrar").validate({
      rules:{
         userfile: {required:true},
         nome: {required:true},
         cnpj: {required:true},
         ie: {required:true},
         logradouro: {required:true},
         numero: {required:true},
         complemento: {required: false},
         bairro: {required:true},
         cidade: {required:true},
         uf: {required:true},
         telefone: {required:true},
         email: {required:true}
      },
      messages:{
         userfile: {required: 'Campo Requerido.'},
         nome: {required: 'Campo Requerido.'},
         cnpj: {required: 'Campo Requerido.'},
         ie: {required: 'Campo Requerido.'},
         logradouro: {required: 'Campo Requerido.'},
         numero: {required:'Campo Requerido.'},
         complemento: {required: 'Pode deixar em branco.'},
         bairro: {required:'Campo Requerido.'},
         cidade: {required:'Campo Requerido.'},
         uf: {required:'Campo Requerido.'},
         telefone: {required:'Campo Requerido.'},
         email: {required:'Campo Requerido.'}
      },

        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
            $(element).parents('.control-group').removeClass('success');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
   });


    $("#formAlterar").validate({
      rules:{
         userfile: {required:true},
         nome: {required:true},
         cnpj: {required:true},
         ie: {required:true},
         logradouro: {required:true},
         numero: {required:true},
         complemento: {required: false},
         bairro: {required:true},
         cidade: {required:true},
         uf: {required:true},
         telefone: {required:true},
         email: {required:true},
         hora_inicio: {required:true},
         hora_termino: {required:true},
         fuso_horario: {required:true}
      },
      messages:{
         userfile: {required: 'Campo Requerido.'},
         nome: {required: 'Campo Requerido.'},
         cnpj: {required: 'Campo Requerido.'},
         ie: {required: 'Campo Requerido.'},
         logradouro: {required: 'Campo Requerido.'},
         numero: {required:'Campo Requerido.'},
         complemento: {required: 'Pode deixar em branco.'},
         bairro: {required:'Campo Requerido.'},
         cidade: {required:'Campo Requerido.'},
         uf: {required:'Campo Requerido.'},
         telefone: {required:'Campo Requerido.'},
         email: {required:'Campo Requerido.'},
         hora_inicio: {required:'Campo Requerido.'},
         hora_termino: {required:'Campo Requerido.'},
         fuso_horario: {required:'Campo Requerido.'}
      },

        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
            $(element).parents('.control-group').removeClass('success');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
   });

});
    
</script>