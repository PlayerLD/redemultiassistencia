<?php
	$CI =& get_instance();
?>
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="<?php //echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/dist/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/dist/jquery.jqplot.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/newstilo.css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dist/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/dist/plugins/jqplot.donutRenderer.min.js"></script>
<?php if ($orcamentos_pendentes->total > 0):?>
<?php endif;?> 
<!--Action boxes-->
<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="row">
							<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){ ?>	<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#27a9e3;">
														<i class="icon-group"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Clientes</h4>
														<div class="info">
															<strong class="amount"><?= $CI->count_if('clientes'); ?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/clientes/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/clientes" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>
<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){ ?>
																		<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#28b779;">
														<i class="icon-barcode"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Produtos</h4>
														<div class="info">
															<strong class="amount"><?= $CI->count_if('produtos');?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/produtos/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/produtos" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){ ?>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#ffb848;">
														<i class="icon-wrench"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Serviços</h4>
														<div class="info">
															<strong class="amount"><?= $CI->count_if('servicos');?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/servicos/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/servicos" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>
	        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){ ?>
							<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#da542e;">
														<i class="icon-tags"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">OS</h4>
														<div class="info">
															<strong class="amount"><?= $CI->count_if('os', 'idFranquias');?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/os/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/os" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){ ?>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#2255a4;">
														<i class="icon-shopping-cart"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Vendas</h4>
														<div class="info">
															<strong class="amount"><?= $CI->count_if('vendas');?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/vendas/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/vendas" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vFornecedor')){ ?>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary" style="background-color:#da9628;">
														<i class="icon-truck"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Fornecedores</h4>
														<div class="info">
															<strong class="amount"><?= $fornecedores;?></strong>
															<span class="text-primary"><a href="<?php echo base_url()?>index.php/fornecedores/adicionar"> (Adicionar) </a></span>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url()?>index.php/fornecedores" class="text-muted text-uppercase">Ver todos</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
<?php } ?>
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-primary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <?php $teste = $CI->countOrcamentos(); if($teste->total > "0"){ ?>
                                                    	<div class="summary-icon">
                                                        	<img src="<?php echo base_url()?>assets/img/notdoida.gif">
                                                        	
                                                        </div>
                                                    <?php }else{ ?>
                                                    	<div class="summary-icon bg-primary" style="background-color:#939DA8;">
                                                    	    <i class="icon-star"></i>
                                                    	</div>
                                                    <?php } ?>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Orçamentos</h4>
                                                        <div class="info">
                                                            <strong class="amount"><?= $CI->count_if('orcamentos');?></strong>

                                                        </div>
                                                    </div>
                                                    <div class="summary-footer">
                                                        <a href="<?php echo base_url()?>index.php/os/orcamentos" class="text-muted text-uppercase">Ver todos</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
						
							</div>
						</div>  <div class="container-fluid">
    
  </div>  
<!--End-Action boxes-->  
<div class="row-fluid" style="margin-top: 0">
    
    <div class="span12">
        
        <div class="widget-box">

            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Sugestões de pedidos <a  href="#modal-solicitacao-produtos" onclick="$('#modal-solicitacao-produtos').modal('show');" title="Solicitar vários para Central" class="btn btn-warning" style="    margin-top: -15px;position: relative;top: 6px;"> <i class="icon-truck" ></i> </a></h5></div>
            <div class="widget-content">
                <table class="table table-bordered">
                	 <thead>
                          <tr>
                              <th>#</th>
                              <th>Nome</th>
                              <th>Custo</th>
                              <th>Ideal</th>
                              <th>Quantidade</th>
                              <th>Reposição</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          foreach ($produtos as $p) {
                              echo '<tr>';
                              echo '<td>'.$p->codigo_barras.'</td>';
                              echo '<td>' . $p->descricao. '</td>';
                              echo '<td>' . $p->precoCompra . '</td>';
                              echo '<td>' . $p->estoque_ideal . '</td>';
                              echo '<td>' . $p->estoque . '</td>';
                              echo '<td>' . $p->reposicao. '</td>';
                              echo '<td>';
	                            if($this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
	                                echo '<a href="'.base_url().'index.php/produtos/editar/'.$p->idProdutos.'" class="btn btn-info"> <i class="icon-pencil" ></i> </a>  '; 
	                                echo '<a onclick="$(\'#id_produto\').attr(\'value\', \''.$p->idProdutos.'\'); $(\'#modal-solicitacao-produto\').modal(\'show\');" href="#modal-solicitacao-produto" title="Solicitar para Central" class="btn btn-warning"> <i class="icon-truck" ></i> </a>  '; 
	                            }
	                            echo '</td>';
                              echo '</tr>';
                          }
                          ?>
                      </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php if($estatisticas_financeiro != null){ 
      if($estatisticas_financeiro->total_receita != null || $estatisticas_financeiro->total_despesa != null || $estatisticas_financeiro->total_receita_pendente != null || $estatisticas_financeiro->total_despesa_pendente != null){  ?>
<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas financeiras - Realizado</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                      <div id="chart-financeiro" style=""></div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas financeiras - Pendente</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                      <div id="chart-financeiro2" style=""></div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Total em caixa / Previsto</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                      <div id="chart-financeiro-caixa" style=""></div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
<?php } } ?>
<?php if($os != null){ ?>
<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas de OS</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                      <div id="chart-os" style=""></div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<?php if($os != null) {?>
<script type="text/javascript">
    
    $(document).ready(function(){
      var data = [
        <?php foreach ($os as $o) {
            echo "['".$o->status."', ".$o->total."],";
        } ?>
       
      ];
      var plot1 = jQuery.jqplot ('chart-os', [data], 
        { 
          seriesDefaults: {
            // Make this a pie chart.
            renderer: jQuery.jqplot.PieRenderer, 
            rendererOptions: {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              showDataLabels: true
            }
          }, 
          legend: { show:true, location: 'e' }
        }
      );
    });
 
</script>
<?php } ?>
<?php if(isset($estatisticas_financeiro) && $estatisticas_financeiro != null) { 
         if($estatisticas_financeiro->total_receita != null || $estatisticas_financeiro->total_despesa != null || $estatisticas_financeiro->total_receita_pendente != null || $estatisticas_financeiro->total_despesa_pendente != null){
?>
<script type="text/javascript">
    
    $(document).ready(function(){
      var data2 = [['Total Receitas',<?php echo ($estatisticas_financeiro->total_receita != null ) ?  $estatisticas_financeiro->total_receita : '0.00'; ?>],['Total Despesas', <?php echo ($estatisticas_financeiro->total_despesa != null ) ?  $estatisticas_financeiro->total_despesa : '0.00'; ?>]];
      var plot2 = jQuery.jqplot ('chart-financeiro', [data2], 
        {  
          seriesColors: [ "#9ACD32", "#FF8C00", "#EAA228", "#579575", "#839557", "#958c12","#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],   
          seriesDefaults: {
            // Make this a pie chart.
            renderer: jQuery.jqplot.PieRenderer, 
            rendererOptions: {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              dataLabels: 'value',
              showDataLabels: true
            }
          }, 
          legend: { show:true, location: 'e' }
        }
      );
      var data3 = [['Total Receitas',<?php echo ($estatisticas_financeiro->total_receita_pendente != null ) ?  $estatisticas_financeiro->total_receita_pendente : '0.00'; ?>],['Total Despesas', <?php echo ($estatisticas_financeiro->total_despesa_pendente != null ) ?  $estatisticas_financeiro->total_despesa_pendente : '0.00'; ?>]];
      var plot3 = jQuery.jqplot ('chart-financeiro2', [data3], 
        {  
          seriesColors: [ "#90EE90", "#FF0000", "#EAA228", "#579575", "#839557", "#958c12","#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],   
          seriesDefaults: {
            // Make this a pie chart.
            renderer: jQuery.jqplot.PieRenderer, 
            rendererOptions: {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              dataLabels: 'value',
              showDataLabels: true
            }
          }, 
          legend: { show:true, location: 'e' }
        }
      );
      var data4 = [['Total em Caixa',<?php echo ($estatisticas_financeiro->total_receita - $estatisticas_financeiro->total_despesa); ?>],['Total a Entrar', <?php echo ($estatisticas_financeiro->total_receita_pendente - $estatisticas_financeiro->total_despesa_pendente); ?>]];
      var plot4 = jQuery.jqplot ('chart-financeiro-caixa', [data4], 
        {  
          seriesColors: ["#839557","#d8b83f", "#d8b83f", "#ff5800", "#0085cc"],   
          seriesDefaults: {
            // Make this a pie chart.
            renderer: jQuery.jqplot.PieRenderer, 
            rendererOptions: {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              dataLabels: 'value',
              showDataLabels: true
            }
          }, 
          legend: { show:true, location: 'e' }
        }
      );
    });
 
</script>
<?php } } ?>

<div id="modal-solicitacao-produto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/solicitar" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Solicitar Produto</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="qtd" placeholder="Quantidade">
            <input type="hidden" name="id_produto" id="id_produto">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnSolicitarProduto">Solicitar</button>
        </div>
    </form>
</div>

<div id="modal-solicitacao-produtos" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/solicitar" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Solicitar Produtos</h5>
            <small>Os produtos com campos vazios ou zerados não serão incluídos na solicitação</small>
        </div>
        <div class="modal-body">
            <table class="table table-bordered">
                	 <thead>
                          <tr>
                              <th>#</th>
                              <th>Produto</th>
                              <th>Quantidade</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          foreach ($produtos as $p) {
                              echo '<tr>';
                              echo '<td>'.$p->codigo_barras.'</td>';
                              echo '<td>' . $p->descricao. '</td>';
                              echo '<td><input type="text" name="qtd[]" placeholder="Quantidade"><input type="hidden" name="id_produto[]" id="id_produto" value="'.$p->idProdutos.'"></td>';
                              echo '</tr>';
                          }
                          ?>
                      </tbody>
                </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnSolicitarProduto">Inserir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
	$(document).on('click', '#btnSolicitarProduto', function(event) {
        var preenchido = true;
        
        if(!preenchido){
            alert("Informe todos os campos do cliente para fazer a solicitação!");
        }else{
            var dados = $(this).closest("form").serialize();
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/produtos/solicitar",
                data: dados,
                dataType: 'json',
                success: function(data){
                    $('#modal-solicitacao-produto,#modal-solicitacao-produtos').modal('hide');
                },
                error: function(){
                    alert("ERRO!");
                }
            });
        }
        
        return false;
    });
</script>