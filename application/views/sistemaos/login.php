<!DOCTYPE html>
<html lang="pt-br">
    
<head>
        <title>ASSISTÊNCIA TÉCNICA ONLINE</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/matrix-login.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script>
    </head> 
    <body style="background-color: rgb(16, 112, 183);">
        <div id="loginbox">             
            <form  class="form-vertical" id="formLogin" method="post" action="<?php echo base_url()?>index.php/sistemaos/verificarLogin" style="background-color: rgb(16, 112, 183);; border:1px;">
                  <?php if($this->session->flashdata('error') != null){?>
                        <div class="alert alert-danger">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <?php echo $this->session->flashdata('error');?>
                       </div>
                  <?php }?>
                <div class="control-group normal_text" style="background-color: rgb(16, 112, 183);"> <h3><img src="<?php echo base_url()?>assets/img/logoinico.png" alt="Logo"style="max-width: 300px;" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="span5" style="margin-left: 0;float: left;width: 50%;text-align: right;padding-right: 15px;box-sizing: border-box;">
                            <label for="is_franquia" style="color: white;display: inline-block;">Franquia</label>
                            <input id="is_franquia" type="radio" name="tipo_login" value="is_franquia" checked> 
                        </div>
                        <div class="span5" style="margin-left: 0;float: left;width: 50%;padding-left: 15px;box-sizing: border-box;">
                            <label for="is_usuario" style="color: white;display: inline-block;">Usuário</label>
                            <input id="is_usuario" type="radio" name="tipo_login" value="is_usuario"> 
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"></i></span><input id="email" name="email" type="text" placeholder="Email" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input id="senha" name="senha" type="password" placeholder="Senha" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lr"><i class="icon-lock"></i></span><input id="idv" name="idv" type="number" placeholder="1" />
                        </div>
                    </div>
                </div>
                <div class="form-actions" style="text-align: center; border:none;">
                    <button class="btn btn-info btn-large"/> Logar</button>
                </div>
            </form>
       
        </div>
        
        
        
        <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/validate.js"></script>




        <script type="text/javascript">
            $(document).ready(function(){

                $('#email').focus();
                $("#formLogin").validate({
                     rules :{
                          email: { required: true, email: true},
                          senha: { required: true},
                          idv: { required: true}
                    },
                    messages:{
                          email: { required: 'Campo Requerido.', email: 'Insira Email válido'},
                          senha: {required: 'Campo Requerido.'},
                          idv: {required: 'Campo Requerido.'},

                    },
                   submitHandler: function( form ){       
                         var dados = $( form ).serialize();
                         
                    
                        $.ajax({
                          type: "POST",
                          url: "<?php echo base_url();?>index.php/sistemaos/verificarLogin?ajax=true",
                          data: dados,
                          dataType: 'json',
                          success: function(data)
                          {
                            if(data.result == true)
                            {
                                window.location.href = "<?php echo base_url();?>index.php/sistemaos";
                            }
                            else
                            {
                                $('#notification .modal-body h5').html(data.error); 
                                $('#call-modal').trigger('click'); 
                                console.log(typeof data.boleto);
                                if(typeof data.boleto == "string")
                                  $('#btn-boleto').attr('href', data.boleto).show(); 
                            }
                          }
                          });

                          return false;
                    },

                    errorClass: "help-inline",
                    errorElement: "span",
                    highlight:function(element, errorClass, validClass) {
                        $(element).parents('.control-group').addClass('error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).parents('.control-group').removeClass('error');
                        $(element).parents('.control-group').addClass('success');
                    }
                });

            });

        </script>



        <a href="#notification" id="call-modal" role="button" class="btn" data-toggle="modal" style="display: none ">notification</a>

        <div id="notification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel">SistemaOs</h4>
          </div>
          <div class="modal-body">
            <h5 style="text-align: center">Os dados de acesso estão incorretos, por favor tente novamente!</h5>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Fechar</button>
            <a class="btn btn-primary" id="btn-boleto" href="" target="_blank" style="display: none;">Pagar boleto</a>

          </div>
        </div>


    </body>

</html>