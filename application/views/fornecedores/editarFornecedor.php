<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Editar Fornecedor</h5>
            </div>
            <div class="widget-content nopadding">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                    
                    
                }
                 ?>
                <form action="<?php echo current_url(); ?>" id="formFornecedor" method="post" class="form-horizontal" >
                    <?
                    	if (!$result)
                    	{
                    ?>
	                    <div class="control-group">
	                        <label class="control-label" style="width: 100%; text-align: left; color: red; margin-bottom: 175px;">&emsp;&emsp;&emsp;<strong>Error: </strong>Fornecedor não encontrado.</label>
	                    </div>
	                    <div class="form-actions">
	                        <div class="span12">
	                            <div class="span6 offset3">
	                                <a href="<?php echo base_url() ?>index.php/fornecedores" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
	                            </div>
	                        </div>
	                    </div>
                    <?	
                    	}
                    	else
                    	{
                    ?>
                
	                    <div class="control-group">
	                        <?php echo form_hidden('idFornecedores',$result->idFornecedores) ?>
	                        <label for="nomeFornecedor" class="control-label">Nome<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="nomeFornecedor" type="text" name="nomeFornecedor" value="<?php echo $result->nomeFornecedor; ?>"  />
	                        </div>
	                    </div>
	                    <div class="control-group">
	                        <label for="documento" class="control-label">CPF/CNPJ<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="documento" type="text" name="documento" value="<?php echo $result->documento; ?>"  />
	                        </div>
	                    </div>
	                    <div class="control-group">
	                        <label for="responsavel" class="control-label">Responsável<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="responsavel" type="text" name="responsavel" value="<?php echo $result->responsavel; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="telefone" class="control-label">Telefone<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="telefone" type="text" name="telefone" value="<?php echo $result->telefone; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="celular" class="control-label">Celular</label>
	                        <div class="controls">
	                            <input id="celular" type="text" name="celular" value="<?php echo $result->celular; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="email" class="control-label">Email<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="email" type="text" name="email" value="<?php echo $result->email; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="site" class="control-label">Site</label>
	                        <div class="controls">
	                            <input id="site" type="text" name="site" value="<?php echo $result->site; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="cep" class="control-label">CEP<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="cep" type="text" name="cep" value="<?php echo $result->cep; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="rua" class="control-label">Rua<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="rua" type="text" name="rua" value="<?php echo $result->rua; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="numero" class="control-label">Número<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="numero" type="text" name="numero" value="<?php echo $result->numero; ?>"  />
	                        </div>
	                    </div>
	                    <div class="control-group">
	                        <label for="complemento" class="control-label">Complemento</label>
	                        <div class="controls">
	                            <input id="complemento" type="text" name="complemento" value="<?php echo $result->complemento; ?>"  />
	                        </div>
	                    </div>
	
	
	                    <div class="control-group" class="control-label">
	                        <label for="bairro" class="control-label">Bairro<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="bairro" type="text" name="bairro" value="<?php echo $result->bairro; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="cidade" class="control-label">Cidade<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="cidade" type="text" name="cidade" value="<?php echo $result->cidade; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="estado" class="control-label">Estado<span class="required">*</span></label>
	                        <div class="controls">
	                            <input id="estado" type="text" name="estado" value="<?php echo $result->estado; ?>"  />
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="descritivo" class="control-label">Descritivo</label>
	                        <div class="controls">
	                            <textarea id="descritivo" type="text" name="descritivo"><?php echo $result->descritivo; ?></textarea>
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="obs" class="control-label">OBS</label>
	                        <div class="controls">
	                            <textarea id="obs" type="text" name="obs"><?php echo $result->obs; ?></textarea>
	                        </div>
	                    </div>
	
	                    <div class="control-group" class="control-label">
	                        <label for="fixo" class="control-label">Fixo para todas franquias<span class="required">*</span></label>
	                        <div class="controls">
			            <select name="fixo" id="fixo" value="">
			    	         <option <?php if($result->fixo == 's'){echo 'selected';} ?> value="s">Sim</option>
			    	         <option <?php if($result->fixo == 'n'){echo 'selected';} ?> value="n">Não</option>
	                            </select>
	                        </div>
	                    </div>
	
	
	
	                    <div class="form-actions">
	                        <div class="span12">
	                            <div class="span6 offset3">
	                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
	                                <a href="<?php echo base_url() ?>index.php/fornecedores" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
	                            </div>
	                        </div>
	                    </div>
                    <? } ?>
                </form>
            </div>
        </div>
    </div>
</div>



<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
           $('#formFornecedor').validate({
            rules :{
                  nomeFornecedor:{ required: true},
                  documento:{ required: true},
                  responsavel:{ required: true},
                  telefone:{ required: true},
                  email:{ required: true},
                  rua:{ required: true},
                  numero:{ required: true},
                  bairro:{ required: true},
                  cidade:{ required: true},
                  estado:{ required: true},
                  cep:{ required: true},
                  fixo:{ required: true}

            },
            messages:{
                  nomeFornecedor :{ required: 'Campo Requerido.'},
                  documento :{ required: 'Campo Requerido.'},
                  responsavel:{ required: 'Campo Requerido.'},
                  telefone:{ required: 'Campo Requerido.'},
                  email:{ required: 'Campo Requerido.'},
                  rua:{ required: 'Campo Requerido.'},
                  numero:{ required: 'Campo Requerido.'},
                  bairro:{ required: 'Campo Requerido.'},
                  cidade:{ required: 'Campo Requerido.'},
                  estado:{ required: 'Campo Requerido.'},
                  cep:{ required: 'Campo Requerido.'},
                  fixo:{ required: 'Campo Requerido.'}

            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
      });
</script>

