<?php 
	require (reisdev('bootstrap'));
	if($this->permission->checkPermission($this->session->userdata('permissao'),'aFornecedor'))
	{ ?>
    		<a href="<?php echo base_url();?>index.php/fornecedores/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Fornecedor</a>    
<?php   } ?>

<div class="widget-box">
	<div class="widget-title">
		<span class="icon">
        		<i class="icon-truck"></i>
    		</span>
    		<h5>Fornecedores</h5>
	</div>
</div>
<div class="widget-content nopadding">
<?php
	$dt = new DataTable();
	$dt->Columns(array('Cód', 'Nome', 'CPF/CNPJ', 'Site', 'Telefone', ''));
	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'vFornecedor'))
                		$buttons = $buttons.'<a href="'.base_url().'index.php/fornecedores/visualizar/'.$r->idFornecedores.'" style="margin-right: 1%" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';

            		if($this->permission->checkPermission($this->session->userdata('permissao'),'eFornecedor'))
                		$buttons = $buttons.'<a href="'.base_url().'index.php/fornecedores/editar/'.$r->idFornecedores.'" style="margin-right: 1%" class="btn btn-info tip-top" title="Editar Fornecedor"><i class="icon-pencil icon-white"></i></a>';

            		if($this->permission->checkPermission($this->session->userdata('permissao'),'dFornecedor'))
                		$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" fornecedor="'.$r->idFornecedores.'" style="margin-right: 1%" class="btn btn-danger tip-top" title="Excluir Fornecedor"><i class="icon-remove icon-white"></i></a>';

			$dt->add_Item(array
			(
				$r->idFornecedores,
				$r->nomeFornecedor,
				$r->documento,
				"<a  href='http://". $r->site . "' target='_blank'>". $r->site . "</a>",
				Mask::Tel($r->telefone),
				$buttons
			));
		}
	}
	$dt->End();
?>
</div>
 
<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<form action="<?php echo base_url() ?>index.php/fornecedores/excluir" method="post" >
  		<div class="modal-header">
    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    			<h5 id="myModalLabel">Excluir Fornecedor</h5>
  		</div>
  		<div class="modal-body">
    			<input type="hidden" id="idFornecedor" name="id" value="" />
    			<h5 style="text-align: center">Deseja realmente excluir este fornecedor e os dados associados a ele (OS, Vendas, Receitas)?</h5>
 		</div>
  		<div class="modal-footer">
    			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    			<button class="btn btn-danger">Excluir</button>
  		</div>
  	</form>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$(document).on('click', 'a', function(event)
		{
	        	var fornecedor = $(this).attr('fornecedor');
	        	$('#idFornecedor').val(fornecedor);
	    	});
	});
</script>
