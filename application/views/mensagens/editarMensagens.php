<style>
textarea
{
	resize:none
}

.divbox
{
	margin-left: 0 !important;
	padding: 0 !important;
	margin-bottom: 1% !important;
	border: 1px solid #CDCDCD !important;
	background: #fff !important;
}
.divcontent
{
	padding: 1% !important;
	padding-top: 0 !important;
	margin-left: 0 !important;
	margin-bottom: 0  !important;
}
.divtitle
{
	padding: 1% !important;
	padding-top: 0 !important;
	padding-bottom: 0 !important;
	margin-left: 0 !important;
	margin-bottom: 0  !important;
}

</style>
<div class="widget-box" style="margin-bottom: 0">
	<div class="widget-title">
		<span class="icon">
        		<i class="icon-envelope"></i>
    		</span>
    		<h5>Editar Mensagens</h5>
	</div>
</div>
<?php if ($custom_error != '') {
    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
} ?>
<form action="<?php echo current_url(); ?>" method="post" id="formMensagens" class="form-horizontal">
	<div class="divbox span12">
		<input id="idFranquia" name="idFranquia" type="hidden" value="<?php if (isset($results['idFranquia'])) echo $results['idFranquia'] ?>"/>
		<div class="divtitle span12">
			<h5>Usuários</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label> &emsp; <i class="icon-angle-right"></i> E-mail</label>
                       		<label for="emailUser" class="control-label" style="width:70px; padding-top: 10px;"> Usuário </label>
                        	<div class="controls" style="margin-left: 75px; padding: 3px 0; padding-right: 3%;">
					<input id="emailUser" name="emailUser" type="email" style="width: 100%;" autocomplete="off" value="<?php if (isset($results['email']['user'])) echo $results['email']['user'] ?>"/>
				</div>
				<label for="emailName" class="control-label" style="width:70px; padding-top: 10px;"> Nome </label>
                        	<div class="controls" style="margin-left: 75px; padding: 3px 0; padding-right: 3%;">
					<input id="emailName" name="emailName" type="text" style="width: 100%;" autocomplete="off" value="<?php if (isset($results['email']['name'])) echo $results['email']['name'] ?>"/>
				</div>
			</div>
			<div class="span6">
				<label> &emsp; <i class="icon-angle-right"></i> SMS</label>
				             
                        	<label for="smsGateway" class="control-label" style="width:70px; padding-top: 10px;"> Gateway </label>
                        	<div class="controls" style="margin-left: 75px; padding: 3px 0;">
	                             	<select name="smsGateway" id="smsGateway" style="width: 100%;" value="">
	                                	<option value="upsms">UP Soluctions SMS</option>
	                             	</select>
                       		</div>
                       		<label for="smsUser" class="control-label" style="width:70px; padding-top: 10px;"> Usuário </label>
                        	<div class="controls" style="margin-left: 75px; padding: 3px 0; padding-right: 3%;">
					<input id="smsUser" name="smsUser" type="email" style="width: 100%;" autocomplete="off" value="<?php if (isset($results['sms']['user'])) echo $results['sms']['user'] ?>"/>
				</div>
				<label for="smsPsw" class="control-label" style="width:70px; padding-top: 10px;"> Senha </label>
                        	<div class="controls" style="margin-left: 75px; padding: 3px 0; padding-right: 3%;">
					<input id="smsPsw" name="smsPsw" type="password" style="width: 100%;" autocomplete="off" value="<?php if (isset($results['sms']['psw'])) echo $results['sms']['psw'] ?>"/>
				</div>
			</div>
		</div>
	</div>
	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Orçamento</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailOrcamento"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailOrcamento" id="emailOrcamento" cols="30" rows="5"><?php if (isset($results['email']['Orcamento'])) echo $results['email']['Orcamento'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsOrcamento"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsOrcamento" id="smsOrcamento" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Orcamento'])) echo $results['sms']['Orcamento'] ?></textarea>
			</div>
		</div>
	</div>
	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Aguardando Aprovação</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailAguardandoAprovacao"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailAguardandoAprovacao" id="emailAguardandoAprovacao" cols="30" rows="5"><?php if (isset($results['email']['AguardandoAprovacao'])) echo $results['email']['AguardandoAprovacao'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsAguardandoAprovacao"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsAguardandoAprovacao" id="smsAguardandoAprovacao" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['AguardandoAprovacao'])) echo $results['sms']['AguardandoAprovacao'] ?></textarea>
			</div>
		</div>
	</div>

    <div class="divbox span12">
        <div class="divtitle span12">
            <h5>Status: Aguardando Peças</h5>
        </div>
        <div class="divcontent span12">
            <div class="span6">
                <label for="emailAguardandoPecas"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
                <textarea class="span12" name="emailAguardandoPecas" id="emailAguardandoPecas" cols="30" rows="5"><?php if (isset($results['email']['AguardandoPecas'])) echo $results['email']['AguardandoPecas'] ?></textarea>
            </div>
            <div class="span6">
                <label for="smsAguardandoPecas"> &emsp; <i class="icon-angle-right"></i> SMS</label>
                <textarea class="span12" name="smsAguardandoPecas" id="smsAguardandoPecas" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['AguardandoPecas'])) echo $results['sms']['AguardandoPecas'] ?></textarea>
            </div>
        </div>
    </div>

	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Em Andamento</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailEmAndamento"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailEmAndamento" id="emailEmAndamento" cols="30" rows="5"><?php if (isset($results['email']['EmAndamento'])) echo $results['email']['EmAndamento'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsEmAndamento"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsEmAndamento" id="smsEmAndamento" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['EmAndamento'])) echo $results['sms']['EmAndamento'] ?></textarea>
			</div>
		</div>
	</div>

	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Faturando</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailFaturando"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailFaturando" id="emailFaturando" cols="30" rows="5"><?php if (isset($results['email']['Faturando'])) echo $results['email']['Faturando'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsFaturando"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsFaturando" id="smsFaturando" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Faturando'])) echo $results['sms']['Faturando'] ?></textarea>
			</div>
		</div>
	</div>
	
	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Finalizado</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailFinalizado"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailFinalizado" id="emailFinalizado" cols="30" rows="5"><?php if (isset($results['email']['Finalizado'])) echo $results['email']['Finalizado'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsFinalizado"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsFinalizado" id="smsFinalizado" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Finalizado'])) echo $results['sms']['Finalizado'] ?></textarea>
			</div>
		</div>
	</div>

    <div class="divbox span12">
        <div class="divtitle span12">
            <h5>Status: Não Aprovado</h5>
        </div>
        <div class="divcontent span12">
            <div class="span6">
                <label for="emailNaoAprovado"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
                <textarea class="span12" name="emailNaoAprovado" id="emailNaoAprovado" cols="30" rows="5"><?php if (isset($results['email']['NaoAprovado'])) echo $results['email']['NaoAprovado'] ?></textarea>
            </div>
            <div class="span6">
                <label for="smsNaoAprovado"> &emsp; <i class="icon-angle-right"></i> SMS</label>
                <textarea class="span12" name="smsNaoAprovado" id="smsNaoAprovado" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['NaoAprovado'])) echo $results['sms']['NaoAprovado'] ?></textarea>
            </div>
        </div>
    </div>

	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Cancelado</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="emailCancelado"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="emailCancelado" id="emailCancelado" cols="30" rows="5"><?php if (isset($results['email']['Cancelado'])) echo $results['email']['Cancelado'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsCancelado"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsCancelado" id="smsCancelado" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Cancelado'])) echo $results['sms']['Cancelado'] ?></textarea>
			</div>
		</div>
	</div>
    
    
    
    
    
	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Aprovado</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="smsAprovado"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="smsAprovado" id="smsAprovado" cols="30" rows="5"><?php if (isset($results['email']['Aprovado'])) echo $results['email']['Aprovado'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsAprovado"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsAprovado" id="smsAprovado" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Aprovado'])) echo $results['sms']['Aprovado'] ?></textarea>
			</div>
		</div>
	</div>   
    
    
    
    
	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Liberar Slot</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="smsLiberarSlot"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="smsLiberarSlot" id="smsLiberarSlot" cols="30" rows="5"><?php if (isset($results['email']['Liberar Slot'])) echo $results['email']['Liberar Slot'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsLiberarSlot"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsLiberarSlot" id="smsLiberarSlot" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Liberar Slot'])) echo $results['sms']['Liberar Slot'] ?></textarea>
			</div>
		</div>
	</div>        




	<div class="divbox span12">
		<div class="divtitle span12">
			<h5>Status: Guardar Aparelho</h5>
		</div>
		<div class="divcontent span12">
			<div class="span6">
				<label for="smsGuardarAparelho"> &emsp; <i class="icon-angle-right"></i> E-mail</label>
				<textarea class="span12" name="smsGuardarAparelho" id="smsGuardarAparelho" cols="30" rows="5"><?php if (isset($results['email']['Guardar Aparelho'])) echo $results['email']['Guardar Aparelho'] ?></textarea>
			</div>
			<div class="span6">
				<label for="smsGuardarAparelho"> &emsp; <i class="icon-angle-right"></i> SMS</label>
				<textarea class="span12" name="smsGuardarAparelho" id="smsGuardarAparelho" maxlength="160" cols="30" rows="5"><?php if (isset($results['sms']['Guardar Aparelho'])) echo $results['sms']['Guardar Aparelho'] ?></textarea>
			</div>
		</div>
	</div>        








	
	<div class="span12" style="padding: 1%; margin-left: 0">
		<div class="span6 offset3" style="text-align: center">
			<button type="submit" name="save" value="" class="btn btn-primary"><i class="icon-ok icon-white"></i> Salvar</button>
			<a href="<?php echo base_url() ?>index.php" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
		</div>
	</div>
</form>



<script src="<?php echo base_url()?>js/jquery.validate.js"></script>

<script type="text/javascript">
      $(document).ready(function()
      {
           $('#formMensagens').validate(
           {
            	rules :
            	{
                  	idFranquia:{ required: true}
            	},
            	messages:
            	{
                  	idFranquia:{ required: 'Campo Requerido.'}
            	},

           	errorClass: "help-inline",
           	errorElement: "span",
            	highlight:function(element, errorClass, validClass)
            	{
                	$(element).parents('.control-group').addClass('error');
            	},
            	unhighlight: function(element, errorClass, validClass)
            	{
                	$(element).parents('.control-group').removeClass('error');
                	$(element).parents('.control-group').addClass('success');
            	}
           }); 
      });
</script>