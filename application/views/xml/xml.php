
<?php if(!empty($custom_error)){ ?>
  <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;"><?php echo $custom_error; ?></div>
<?php } ?>

 
<div class="widget-box">
	<div class="widget-title">
            	<span class="icon"><i class="icon-hdd"></i></span>
            	<h5>XML</h5>
	</div>
</div>  
<div class="widget-content nopadding">
<?php 
  $CI = &get_instance(); 

  require (reisdev('bootstrap'));
   
  $dt = new DataTable();
  $dt->Columns(array('Os', 'Cód Franquia', 'Numero NFse', 'Cód Cliente', 'Nome Cliente','Ações'));

  //echo "<pre>";
  //var_dump($results);
  //echo "</pre>"; 

 
	if(@$results)
	{
		foreach ($results as $r)
		{
  
      if ($r->numeroNFse == NULL) {
          $msg = "SEM REGISTRO.";
        }else{
          $msg = $r->numeroNFse;
        }
        //echo "<p>".$r->url."</p>";
			$buttons = ' ';
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'vXML'))
                        	//$buttons = $buttons.'<a class="btn btn-inverse tip-top" style="margin-right: 1%" target="_blank" href="'.$r->url.'" class="btn tip-top" title="Imprimir"><i class="icon-print"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'vXML'))
                          //$buttons = $buttons.'<a href="'.base_url().'assets/arquivos/nfse/xml/nfse'.$r->numeroNFse.'.xml" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>';
                        	$buttons = $buttons.'<a href="'.base_url().'index.php/XML/download/'.$r->numeroNFse.'" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'eXML'))
                        	//$buttons = $buttons.'<a href="'.base_url().'index.php/XML/editar/'.$r->numeroNFse.'" class="btn btn-info tip-top" style="margin-right: 1%" title="Editar"><i class="icon-pencil icon-white"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'dXML'))
                         	//$buttons = $buttons.'<a href="#modal-excluir" style="margin-right: 1%" role="button" data-toggle="modal" XML="'.$r->numeroNFse.'" class="btn btn-danger tip-top" title="Excluir XML"><i class="icon-remove icon-white"></i></a>';
                    
			$dt->add_Item(array
			(
				$r->idOs,
				$r->idFranquias,
				//date('d/m/Y',strtotime($r->cadastro)),
				$msg,
        		$r->idClientes,
				$r->nomeCliente,
				$buttons
			));
		}
	}
	$dt->End();
?>
</div>
 
<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/XML/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir XML</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idDocumento" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir este XML?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>

  <br>
  <br>
  <a href="<?php echo base_url('index.php/xml/loteArqPer'); ?>" class="btn btn-warning">Download Por Períodos</a>

 

<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var XML = $(this).attr('XML');
        $('#idDocumento').val(XML);

   });

   //$(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
});

</script>
