<?php 
	require (reisdev('bootstrap'));
	
	if($this->permission->checkPermission($this->session->userdata('permissao'),'aCliente'))
	{ ?>
    		<a href="<?php echo base_url();?>index.php/clientes/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Cliente</a>    
<?php   } ?>

<div class="widget-box">
     <div class="widget-title">
        <span class="icon">
            <i class="icon-user"></i>
         </span>
        <h5>Clientes</h5> 
     </div>
</div>
<div class="widget-content nopadding">
<?php
	$dt = new DataTable();
	$dt->Columns(array('Cód', 'Nome', 'RG', 'CPF/CNPJ', 'Telefone', 'Celular', ''));
	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'vCliente'))
        			$buttons = $buttons.'<a href="'.base_url().'index.php/clientes/visualizar/'.$r->idClientes.'" style="margin-right: 1%" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';

            		if($this->permission->checkPermission($this->session->userdata('permissao'),'eCliente'))
        			$buttons = $buttons.'<a href="'.base_url().'index.php/clientes/editar/'.$r->idClientes.'" style="margin-right: 1%" class="btn btn-info tip-top" title="Editar Cliente"><i class="icon-pencil icon-white"></i></a>';
        			
    			if($this->permission->checkPermission($this->session->userdata('permissao'),'dCliente'))
        			$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" cliente="'.$r->idClientes.'" style="margin-right: 1%" class="btn btn-danger tip-top" title="Excluir Cliente"><i class="icon-remove icon-white"></i></a>';
    
			$dt->add_Item(array
			(
				$r->idClientes,
				$r->nomeCliente,
				$r->rg,
				$r->documento,
				Mask::Tel($r->telefone),
				Mask::Tel($r->celular, $r->whatsapp),
				$buttons
			));
		}
	}
	$dt->End();
?>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form action="<?php echo base_url() ?>index.php/clientes/excluir" method="post" >
		<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    		<h5 id="myModalLabel">Excluir Cliente</h5>
	  	</div>
	  	<div class="modal-body">
	   		<input type="hidden" id="idCliente" name="id" value="" />
	    		<h5 style="text-align: center">Deseja realmente excluir este cliente e os dados associados a ele (OS, Vendas, Receitas)?</h5>
	  	</div>
	  	<div class="modal-footer">
		    	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
	    		<button class="btn btn-danger">Excluir</button>
	  	</div>
  	</form>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$(document).on('click', 'a', function(event)
		{
	        	var cliente = $(this).attr('cliente');
	        	$('#idCliente').val(cliente);
	    	});
	});
</script>
