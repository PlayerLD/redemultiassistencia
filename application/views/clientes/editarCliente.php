<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span> 
                <h5>Editar Cliente</h5>
            </div>
            <div class="widget-content">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
                <div id="conteudo">
                	<div class="formulario">
                		<form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal" >
                  			<?php $idFranquia = $this->session->userdata('id');?>
                       		<input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
							<?php echo form_hidden('idClientes',$result->idClientes) ?>
                            <div class="dados">
                    			<div class="campos">
                        			<div class="campo" style="width: 69%;">
                            			<label for="nomeCliente">Nome <span class="required">*</span></label>
                             			<input id="nomeCliente" type="text" name="nomeCliente" value="<?php echo $result->nomeCliente; ?>"  />
                            		</div>
                            		<div class="campo last" style="width: 30%;">
                            			<label for="rg" >RG <span class="required">*</span></label>
                             			<input id="rg" type="text" name="rg" value="<?php echo $result->rg; ?>"  />
                            		</div>
                                    <div class="campo" style="width: 25%;">
                                        <label for="inscricaoEstadual">Inscrição Estadual</label>
                                        <input id="inscricaoEstadual" type="text" name="inscricaoEstadual" value="<?php echo $result->inscricaoEstadual; ?>"  />
                                    </div>
                                    <div class="campo" style="width: 22%;">
                            			<label for="documento">CPF/CNPJ</label>
                             			<input id="documento" type="text" name="documento" value="<?php echo $result->documento; ?>"  />
                            		</div>
                                    <div class="campo" style="width: 25%;">
                            			<label for="telefone">Telefone <span class="required">*</span></label>
                             			<input id="telefone" type="text" name="telefone" value="<?php echo $result->telefone; ?>"  />
                            		</div>
                                    <div class="campo last" style="width: 25%;">
                            			<label for="celular">Celular</label>
                             			<input id="celular" type="text" name="celular" value="<?php echo $result->celular; ?>"  />
                                        <div style="display: none;">
                                        	<input type="checkbox" value="1" id="sms" name="sms" style="display: none;"> SMS 
                                    		<input type="checkbox" value="1" id="whatsapp" name="whatsapp" style="display: none;"> Whatsapp
                                      	</div>
                            		</div>
                                    <div class="campo" style="width: 69%;">
                            			<label for="email">Email <span class="required">*</span></label>
                             			<input id="email" type="text" name="email" value="<?php echo $result->email; ?>"  />
                            		</div>
                                    <div class="campo last" style="width: 30%;">
                            			<label for="cep">CEP <span class="required">*</span></label>
                             			<input id="cep" type="text" name="cep" value="<?php echo $result->cep; ?>"  />
                            		</div>
                                    <div class="campo" style="width: 59%;">
                            			<label for="rua">Rua<span class="required">*</span></label>
                             			<input id="rua" type="text" name="rua" value="<?php echo $result->rua; ?>"  />
                            		</div>
                                    <div class="campo" style="width: 17%;">
                            			<label for="numero">Número<span class="required">*</span></label>
                             			<input id="numero" type="text" name="numero" value="<?php echo $result->numero; ?>"  />
                            		</div>
                                    <div class="campo last" style="width: 22%;">
                            			<label for="complemento">Complemento</label>
                             			<input id="complemento" type="text" name="complemento" value="<?php echo $result->complemento; ?>"  />
                            		</div>
                                    <div class="campo" style="width: 32%;">
                                    	<label for="bairro">Bairro<span class="required">*</span></label>
                                        <input id="bairro" type="text" name="bairro" value="<?php echo $result->bairro; ?>"  />
                                    </div>
                                    <div class="campo" style="width: 32%;">
                                    	<label for="cidade">Cidade<span class="required">*</span></label>
                                        <input id="cidade" type="text" name="cidade" value="<?php echo $result->cidade; ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 34%;">
                                    	<label for="estado">Estado<span class="required">*</span></label>
                                        <input id="estado" type="text" name="estado" value="<?php echo $result->estado; ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 100%;">
                                    	<label for="obs">Observação</label>
                                        <textarea id="obs" name="obs"><?php echo $result->obs; ?></textarea>
                                    </div>
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                    	<button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Alterar</button>
                                        <a href="<?php echo base_url() ?>index.php/clientes" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                        		</div>
                    		<div>
                        </form>
             		</div>
              	</div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>

<script type="text/javascript" >
	$(document).ready(function() {
		function limpa_formulário_cep() {
        	// Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#estado").val("");
		}
            
		//Quando o campo cep perde o foco.
		$("#cep").blur(function() {
			//Nova variável "cep" somente com dígitos.
			var cep = $(this).val().replace(/\D/g, '');

			//Verifica se campo cep possui valor informado.
			if (cep != "") {
				//Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;
				//Valida o formato do CEP.
                if(validacep.test(cep)) {
					//Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#estado").val("...");

					//Consulta o webservice viacep.com.br/
					$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

						if (!("erro" in dados)) {
							//Atualiza os campos com os valores da consulta.
							$("#rua").val(dados.logradouro);
							$("#bairro").val(dados.bairro);
							$("#cidade").val(dados.localidade);
							$("#estado").val(dados.uf);
						} else {
							//CEP pesquisado não foi encontrado.
							limpa_formulário_cep();
							//alert("CEP não encontrado.");
						}
                 	});
           		} else {
					//cep é inválido.
                    limpa_formulário_cep();
                    //alert("Formato de CEP inválido.");
               	}
			} else {
				//cep sem valor, limpa formulário.
				limpa_formulário_cep();
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#formCliente').validate({
			rules :{
				nomeCliente:{ required: true},
				rg:{ required: true},
				telefone:{ required: true},
				email:{ required: true},
				cep:{ required: true},
				rua:{ required: true},
				numero:{ required: true},
				bairro:{ required: true},
				cidade:{ required: true},
				estado:{ required: true}
				
			},
            messages:{
				nomeCliente :{ required: ''},
				rg:{ required: ''},
				telefone:{ required: ''},
				email:{ required: ''},
				cep:{ required: ''},
				rua:{ required: ''},
				numero:{ required: ''},
				bairro:{ required: ''},
				cidade:{ required: ''},
				estado:{ required: ''}
            },
            highlight:function(element, errorClass, validClass) {
				$(element).parents('.campo').removeClass('success');
                $(element).parents('.campo').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.campo').removeClass('error');
                $(element).parents('.campo').addClass('success');
            }
		});
	});
</script>