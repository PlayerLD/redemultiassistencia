<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<?php $idFranquia = $this->session->userdata('id');?>
<script type="text/javascript">
  function gerarCodigoBarra(){
      var now = new Date(),
          cod = '<?php echo $idFranquia; ?>'+$.datepicker.formatDate('dmy', new Date())+now.getHours()+now.getMinutes()+now.getSeconds();
      $('input#codigoBarras').val(cod);
      window.open('<?php echo site_url('produtos/geraCodigoBarra'); ?>/'+cod, '_blank', 'location=yes,height=1024,width=768,scrollbars=yes,status=yes');
  }
</script>
<div class="row-fluid" style="margin-top:0"> 
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Produto</h5>
            </div>
            <div class="widget-content">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
                <div id="conteudo">
                  <div class="formulario">
                        <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >
                          <input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
                          <div class="dados">
                          <div class="campos">
                                  <div class="campo last" style="width: 100%">
                                      <label for="codigoBarras">Código de barras</label>
                                        <input id="codigoBarras" type="text" name="codigoBarras" value="<?php echo set_value('codigoBarras'); ?>"  />
                                        <a style="position: absolute;top: 45px;right: 0px;height: 25px;line-height: 25px;" href="javascript:;" class="btn btn-primary" title="Gerar código de barras" onclick="gerarCodigoBarra();"><i class="icon-plus icon-white"></i></a>
                                    </div>
                                  <div class="campo last" style="width: 100%">
                                      <label for="codigo_produto">Código do produto</label>
                                        <input id="codigo_produto" type="text" name="codigo_produto" value="<?php echo set_value('codigo_produto'); ?>"  />
                                    </div>
                                  <div class="campo last" style="width: 100%">
                                      <label for="descricao">Nome do produto<span class="required">*</span></label>
                                        <input id="descricao" type="text" name="descricao" value="<?php echo set_value('descricao'); ?>"  />
                                    </div>

                                  <?php if(!empty($tipos)){ ?>
                                  <div class="campo" style="width: 24%">
                                    <label for="tipo">Tipo </label>
                                    <select name="tipo" id="tipo" value="">
                                        <option value="">Selecione</option>
                                    <?php foreach($tipos as $v){ ?>
                                      <option value="<?php echo $v->idTipos; ?>"><?php echo $v->titulo; ?></option>
                                    <?php } ?>
                                    </select>
                                  </div>
                                  <?php } ?>
                                  <div class="campo" style="width: 24%">
                                      <label for="peso">Peso (kg)</label>
                                        <input id="peso" type="text" name="peso" value="<?php echo set_value('peso'); ?>"  />
                                    </div>
                                    <?php if(!empty($taxas)){ ?>
                                    <div class="campo" style="width: 24%">
                                      <label for="impostos">Impostos sobre o produto</label>
                                      <select name="impostos" id="impostos" value="">
                                        <option value="">Selecione</option>
                                      <?php foreach($taxas as $v){ ?>
                                        <option value="<?php echo $v->idTaxas; ?>"><?php echo $v->titulo; ?>@<?php echo $v->valor; ?>%</option>
                                      <?php } ?>
                                      </select>
                                    </div>
                                    <?php } ?>
                                    <div class="campo" style="width: 24%">
                                      <label for="unidade">Unidade <span class="required">*</span></label>
                                        <input id="unidade" type="text" name="unidade" value="UND"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="marca">Marca <span class="required">*</span></label>
                                        <input id="marca" type="text" name="marca" value=""  />
                                        <input id="marcas_id" type="hidden" class="span12" name="marcas_id" value=""  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="fornecedor">Fornecedor</label>
                                        <input id="fornecedor" type="text" name="fornecedor" value=""  />
                                        <input id="fornecedor_id" type="hidden" class="span12" name="fornecedor_id" value=""  />
                                    </div>
                                    <div class="campo last" style="width: 24%">
                                      <label for="ncm">NCM <span class="required">*</span></label>
                                        <input id="ncm" type="text" name="ncm" value=""  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="cfop">CFOP <span class="required">*</span></label>
                                        <input id="cfop" type="text" name="cfop" value=""  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="precoCompra">Preço de Compra <span class="required">*</span></label>
                                        <input id="precoCompra" class="money" type="text" name="precoCompra" value="<?php echo set_value('precoCompra'); ?>"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="precoVenda">Preço de Venda <span class="required">*</span></label>
                                        <input id="precoVenda" class="money" type="text" name="precoVenda" value="<?php echo set_value('precoVenda'); ?>"  />
                                    </div>
                                    
                                    <?php if($this->session->userdata('id') == 4){ ?>
                                    <div class="campo" style="width: 24%">
                                      <label for="preco_sugerido">Preço sugerido</label>
                                        <input id="preco_sugerido" class="money" type="text" name="preco_sugerido" value="<?php echo set_value('preco_sugerido'); ?>"  />
                                    </div>
                                    <?php } ?>

                                    <div class="campo last" style="width: 24%">
                                      <label for="estoque">Estoque <span class="required">*</span></label>
                                        <input id="estoque" type="text" name="estoque" value="<?php echo set_value('estoque'); ?>"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="estoque_ideal">Estoque ideal</label>
                                        <input id="estoque_ideal" type="text" name="estoque_ideal" value="<?php echo set_value('estoque_ideal'); ?>"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="estoqueMinimo">Estoque Mínimo</label>
                                        <input id="estoqueMinimo" type="text" name="estoqueMinimo" value="<?php echo set_value('estoqueMinimo'); ?>"  />
                                  </div>
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                      <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                    <a href="<?php echo base_url() ?>index.php/produtos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>              
<script type="text/javascript">
  $(document).ready(function(){
      $("#marca").autocomplete({
          source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
          minLength: 1,
          select: function( event, ui ) {
              $("#marcas_id").val(ui.item.id);
            }
        });

      $("#fornecedor").autocomplete({
          source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
          minLength: 1,
          select: function( event, ui ) {
              $("#fornecedor_id").val(ui.item.id);
            }
        });

        

        $('#formProduto').validate({
            rules :{
                  ncm: { required: true},
                  cfop: { required: true},
                  descricao: { required: true},
                  unidade: { required: true},
                  marca: { required: true},
                  precoCompra: { required: true},
                  precoVenda: { required: true},
                  estoque: { required: true}
            },
            messages:{
                  cfop: { required: ''},
                  ncm: { required: ''},
                  descricao: { required: ''},
                  unidade: {required: ''},
                  marca: {required: ''},
                  precoCompra: { required: ''},
                  precoVenda: { required: ''},
                  estoque: { required: ''}
            },

            errorClass: "error",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
              $(element).parents('.campo').removeClass('success');
        $(element).parents('.campo').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.campo').removeClass('error');
                $(element).parents('.campo').addClass('success');
            }
    });
  });
</script>