<?php
require (reisdev('bootstrap'));
if($this->permission->checkPermission($this->session->userdata('permissao'),'aProduto'))
	{ ?>
<a href="<?php echo base_url();?>index.php/produtos/adicionarTipo" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Tipo</a>
<?php 	} ?> 
		<script type="text/javascript">
		    function gerarCodigoBarra(cod){
		      	//cod = "<?php foreach ($results as $r) { echo $r->codigo_barras; }?>";
		      	window.open('<?php echo site_url('produtos/geraCodigoBarra'); ?>/'+cod, '_blank', 'location=yes,height=1024,width=768,scrollbars=yes,status=yes');
		      	console.log(cod);
		    }
		</script>
<style type="text/css">
	div.escanear_cod {
		max-width: 280px;
		border: 1px solid #ccc;
		padding: 20px;
		font-size: 80px;
		margin: 0 auto;
		border-radius: 10px;
	}
	div.escanear_cod h2 {
		font-size: 16px;
		line-height: 1;
	}
</style>
<div class="widget-box">
	<div class="widget-title">
		<span class="icon"><i class="icon-barcode"></i></span>
		<h5>Tipos</h5>
	</div>
</div>
<div class="widget-content nopadding">
	<?php
	$dt = new DataTable();
	$dt->Columns(array('Id', 'Título', ''));
	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'eProduto'))
				$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/produtos/editarTipo/'.$r->idTipos.'" class="btn btn-info tip-top" title="Editar Tipo"><i class="icon-pencil icon-white"></i></a>';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'dProduto'))
				$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" produto="'.$r->idTipos.'" class="btn btn-danger tip-top" title="Excluir Tipo"><i class="icon-remove icon-white"></i></a>';
			$dt->add_Item(array
				(
					$r->idTipos,
					$r->titulo,
					$buttons
					));
		?>
		
<?php
		} 
	}
	$dt->End();
	?>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
  <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Excluir Tipo</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idProduto" name="id" value="" />
      <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Excluir</button>
    </div>
  </form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', 'a', function(event) {
			var produto = $(this).attr('produto');
			$('#idProduto').val(produto);
		});
	});
</script>