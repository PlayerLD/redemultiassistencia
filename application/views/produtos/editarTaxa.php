<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<div class="row-fluid" style="margin-top:0">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-title">
        <span class="icon">
          <i class="icon-align-justify"></i>
        </span>
        <h5>Editar Taxa</h5>
      </div>
      <div class="widget-content">
        <?php if ($custom_error != '') {
          echo '<div class="alert alert-danger">' . $custom_error . '</div>';
        } ?>
        <div id="conteudo">
          <div class="formulario">
            <form action="<?php echo current_url(); ?>" id="formTaxa" method="post" class="form-horizontal" >
              <div class="dados">
                <div class="campos">
                  <div class="campo last" style="width: 100%">
                    <label for="titulo">Nome<span class="required">*</span></label>
                    <input id="titulo" type="text" name="titulo" value="<?php echo $result->titulo; ?>"  />
                  </div>
                  <div class="campo last" style="width: 100%">
                    <label for="valor">Valor %<span class="required">*</span></label>
                    <input id="valor" class="money" type="text" name="valor" value="<?php echo $result->valor; ?>"  />
                  </div>
                  <div class="campo last" style="width: 100%; height: 10px"></div>
                  <h3></h3>
                  <!-- FIM API FOTO -->
                  <div class="campo last" style="width: 100%; text-align:center;">
                    <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                    <a href="<?php echo base_url() ?>index.php/produtos/gerenciar_tipos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    window.onload = function (event) {
      updatePreview();
    };

    $("#marca").autocomplete({
      source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
      minLength: 1,
      select: function( event, ui ) {
        $("#marcas_id").val(ui.item.id);
      }
    });

    $("#fornecedor").autocomplete({
      source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
      minLength: 1,
      select: function( event, ui ) {
        $("#fornecedor_id").val(ui.item.id);
      }
    });

    $('#formTaxa').validate({
      rules :{
        titulo: { required: true},
        valor: { required: true}
      },
      messages:{
        titulo: { required: ''},
        valor: { required: ''}
      },
      errorClass: "error",
      errorElement: "span",
      highlight:function(element, errorClass, validClass) {
        $(element).parents('.campo').removeClass('success');
        $(element).parents('.campo').addClass('error');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.campo').removeClass('error');
        $(element).parents('.campo').addClass('success');
      }
    });
  });
</script>