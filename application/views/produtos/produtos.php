<?php
require (reisdev('bootstrap'));
if($this->permission->checkPermission($this->session->userdata('permissao'),'aProduto'))
	{ ?>
<a href="<?php echo base_url();?>index.php/produtos/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Produto</a>
<a href="<?php echo base_url();?>index.php/produtos/produto_variacoes" class="btn btn-warning"><i class="icon-list icon-white"></i> Variações de Produtos</a>
<a href="<?php echo base_url();?>index.php/produtos/gerenciar_taxas" class="btn btn-warning"><i class="icon-list icon-white"></i> Impostos de Produtos</a>
<a href="#modal-solicitacao-produtos" onclick="$('#modal-solicitacao-produtos').modal('show');" title="Solicitar vários para Central" class="btn btn-warning"><i class="icon-truck"></i> Solicitar Produtos</a>
<?php 	} ?> 
		<script type="text/javascript">
		    function gerarCodigoBarra(cod){
		      	//cod = "<?php foreach ($results as $r) { echo $r->codigo_barras; }?>";
		      	window.open('<?php echo site_url('produtos/geraCodigoBarra'); ?>/'+cod, '_blank', 'location=yes,height=1024,width=768,scrollbars=yes,status=yes');
		    }
		</script>
<style type="text/css">
	div.escanear_cod {
		max-width: 280px;
		border: 1px solid #ccc;
		padding: 20px;
		font-size: 80px;
		margin: 0 auto;
		border-radius: 10px;
	}
	div.escanear_cod h2 {
		font-size: 16px;
		line-height: 1;
	}
</style>
<div class="widget-box">
	<div class="widget-title">
		<span class="icon"><i class="icon-barcode"></i></span>
		<h5>Produtos</h5>
	</div>
</div>
<div class="widget-content nopadding">
  <!-- aqui ficava o codigo original -->
    <table id="memListTable" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>  
            <th>Cod</th>
            <th>Nome</th>
            <th>Codigo Barras</th>
            <th>Codigo Produto</th>
            <th>Estoque</th>
            <th>Preço</th>
            <th>Botão de Ação</th>

          </tr>
      </thead>
      
    </table>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
  <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Excluir Produto</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idProduto" name="id" value="" />
      <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Excluir</button>
    </div>
  </form>
</div>

<div id="modal-solicitacao-produto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/solicitar" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Solicitar Produto</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="qtd" placeholder="Quantidade">
            <input type="hidden" name="id_produto" id="id_produto">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnSolicitarProduto">Solicitar</button>
        </div>
    </form>
</div>

<div id="modal-solicitacao-produtos" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/solicitar" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Solicitar Produtos</h5>
            <small>Os produtos com campos vazios ou zerados não serão incluídos na solicitação</small>
        </div>
        <div class="modal-body">
            <table class="table table-bordered">
                	 <thead>
                          <tr>
                              <th>#</th>
                              <th>Produto</th>
                              <th>Quantidade</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          foreach ($results as $p) {
                          	if($p->idFranquia == 4){
                            	echo '<tr>';
                            	echo '<td>'.$p->codigo_barras.'</td>';
                            	echo '<td>' . $p->descricao. '</td>';
                            	echo '<td><input type="text" name="qtd[]" placeholder="Quantidade"><input type="hidden" name="id_produto[]" id="id_produto" value="'.$p->idProdutos.'"></td>';
                            	echo '</tr>';
	                        }
                          }
                          ?>
                      </tbody>
                </table>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnSolicitarProduto">Inserir</button>
        </div>
    </form>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
      $('#memListTable').DataTable({
          "language": {
            "lengthMenu": "Nº Os _MENU_ total por páginas",
            "zeroRecords": "Nada encontrado - Desculpe!",
            "info": "Mostrando _PAGE_ de _PAGES_ Total de _MAX_",
            "infoEmpty": "Sem registo disponível ",
            "infoFiltered": "(Filtrados _MAX_ total de dados)",
            "infoPostFix":    "",
            "thousands":      ",",
            "loadingRecords": "Buscando...",
            "processing":     "Processando...",
            "search":         "Procurar:",
            "zeroRecords":    "Nenhum registro correspondente encontrado",
            "paginate": {
                "first":      "Próximo",
                "last":       "Anterior",
                "next":       "Avançar",
                "previous":   "Retornar"
              }
          },
          "order": [[ 0, "desc" ]],
          "processing": true,
          "serverSide": true,
          "ajax":{
          "url": "<?php echo base_url('index.php/produtos/getlists'); ?>",
          "dataType": "json",
          "type": "POST",
          "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                         },
        "columns": [
          {"data": "idProdutos"},
          {"data": "descricao"},
          {"data": "codigo_barras"},
          {"data": "codigo_produto"},
          {"data": "estoque"},
          {"data": "precoVenda"},
          {"data": "todosB"},
        ]

      });
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', 'a', function(event) {
			var produto = $(this).attr('produto');
			$('#idProduto').val(produto);
		});
	});

	$(document).on('click', '#btnSolicitarProduto', function(event) {
        var preenchido = true;
        
        if(!preenchido){
            alert("Informe todos os campos do cliente para fazer a solicitação!");
        }else{
            var dados = $(this).closest("form").serialize();
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/produtos/solicitar",
                data: dados,
                dataType: 'json',
                success: function(data){
                    $('#modal-solicitacao-produto,#modal-solicitacao-produtos').modal('hide');
                },
                error: function(){
                    alert("ERRO!");
                }
            });
        }
        
        return false;
    });
</script>