<?php require (reisdev('bootstrap')); ?>


<!-- <link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" /> -->
<div class="row-fluid" style="margin-top:0">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-title">
        <span class="icon">
          <i class="icon-align-justify"></i>
        </span>
        <h5>Editar Produto</h5>
      </div>
      <div class="widget-content">
        <?php if ($custom_error != '') {
          echo '<div class="alert alert-danger">' . $custom_error . '</div>';
        } ?>
        <div id="conteudo">
          <div class="formulario">
              <div class="dados">
                <div class="campos">
            <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >
                  <div class="campo last" style="width: 100%">
                    <label for="codigoBarras">Código de barras</label>
                    <input id="codigoBarras" type="text" name="codigoBarras" value="<?php echo $result->codigo_barras; ?>"  />
                    <a style="position: absolute;top: 45px;right: 0px;height: 25px;line-height: 25px;" href="javascript:;" class="btn btn-primary" title="Gerar código de barras" onclick="gerarCodigoBarra();"><i class="icon-plus icon-white"></i></a>
                  </div>
                  <div class="campo last" style="width: 100%">
                    <label for="codigo_produto">Código do produto</label>
                    <input id="codigo_produto" type="text" name="codigo_produto" value="<?php echo $result->codigo_produto; ?>"  />
                  </div>
                  <div class="campo last" style="width: 100%">
                    <label for="descricao">Nome do produto<span class="required">*</span></label>
                    <input id="descricao" type="text" name="descricao" value="<?php echo $result->descricao; ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="peso">Peso (kg)</label>
                    <input id="peso" type="text" name="peso" value="<?php echo $result->peso; ?>"  />
                  </div>
                  <?php if(!empty($taxas)){ ?>
                    <div class="campo" style="width: 24%">
                      <label for="impostos">Impostos sobre o produto</label>
                      <select name="impostos" id="impostos" value="">
                        <option value="">Selecione</option>
                        <?php foreach($taxas as $v){ ?>
                          <option value="<?php echo $v->idTaxas; ?>" <?php echo $result->impostos == $v->idTaxas ? 'selected' : ''; ?>><?php echo $v->titulo; ?>@<?php echo $v->valor; ?>%</option>
                        <?php } ?>
                      </select>
                    </div>
                  <?php } ?>
                  <div class="campo" style="width: 24%">
                    <label for="unidade">Unidade <span class="required">*</span></label>
                    <input id="unidade" type="text" name="unidade" value="<?php echo $result->unidade; ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="marca">Marca <span class="required">*</span></label>
                    <input id="marca" type="text" name="marca" value="<?php echo $result->nome ?>"  />
                    <input id="marcas_id" type="hidden" class="span12" name="marcas_id" value="<?php echo $result->idMarcas ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="fornecedor">Fornecedor</label>
                    <input id="fornecedor" type="text" name="fornecedor" value="<?php echo $result->nomeFornecedor ?>"  />
                    <input id="fornecedor_id" type="hidden" class="span12" name="fornecedor_id" value="<?php echo $result->idFornecedores ?>"  />
                  </div>
                  <div class="campo last" style="width: 24%">
                    <label for="ncm">NCM <span class="required">*</span></label>
                    <input id="ncm" type="text" name="ncm" value="<?php echo $result->ncm ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="cfop">CFOP <span class="required">*</span></label>
                    <input id="cfop" type="text" name="cfop" value="<?php echo $result->cfop ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="precoCompra">Preço de Compra <span class="required">*</span></label>
                    <input id="precoCompra" class="money" type="text" name="precoCompra" value="<?php echo $result->precoCompra; ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="precoVenda">Preço de Venda <span class="required">*</span></label>
                    <input id="precoVenda" class="money" type="text" name="precoVenda" value="<?php echo $result->precoVenda; ?>"  />
                  </div>
                  
                  <?php if($this->session->userdata('id') == 4){ ?>
                  <div class="campo" style="width: 24%">
                    <label for="preco_sugerido">Preço sugerido <span class="required">*</span></label>
                    <input id="preco_sugerido" class="money" type="text" name="preco_sugerido" value="<?php echo $result->preco_sugerido; ?>"  />
                  </div>
                  <?php } ?>

                  <div class="campo last" style="width: 24%">
                    <label for="estoque">Estoque <span class="required">*</span></label>
                    <input id="estoque" type="text" name="estoque" value="<?php echo $result->estoque; ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="estoque_ideal">Estoque ideal</label>
                    <input id="estoque_ideal" type="text" name="estoque_ideal" value="<?php echo $result->estoque_ideal; ?>"  />
                  </div>
                  <div class="campo" style="width: 24%">
                    <label for="estoqueMinimo">Estoque Mínimo</label>
                    <input id="estoqueMinimo" type="text" name="estoqueMinimo" value="<?php echo $result->estoqueMinimo; ?>"  />
                  </div>
                  <div class="campo last" style="width: 100%; height: 10px"></div>

                  <div class="campo last" style="width: 100%; text-align:center;">
                              <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                              <a href="<?php echo base_url() ?>index.php/produtos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                  </form>
                  <h3></h3>
                  <?php if(empty($result->pai_id)){ ?>
                    <div class="span12 well" style="padding: 1%; margin-left: 0">
                      <form id="formVariacao" action="<?php echo base_url() ?>index.php/produtos/adicionarVariacaoAjax" method="post">
                        <div class="span8" style="position:relative;" id="inputsdivvariacao">
                          <input type="hidden" name="idVariacao" id="idVariacao" />
                          <input type="hidden" name="idProdutos" id="idProdutos" value="<?php echo $result->idProdutos?>" />
                          <label for="">Variacao</label>
                          <input type="text" class="span12" name="variacao" id="variacao" placeholder="Digite o nome do variacao" />
                        </div>

                        <div class="span2">
                          <label for="">.</label>
                          <button class="btn btn-success span12" id="btnAdicionarVariacao"><i class="icon-white icon-plus"></i> Adicionar</button>
                        </div>

                        <div class="span12" style="padding: 1%; margin-left: 0">
                          <div class="span6 offset3" style="text-align: center">
                            <a href="<?php echo base_url() ?>index.php/produtos/visualizar/<?php echo $result->idProdutos; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>
                            <a href="<?php echo base_url() ?>index.php/produtos" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                          </div>
                        </div>
                      </form>
                    </div>

                    <div class="span12" id="divVariacao" style="margin-left: 0">
                      <table class="table table-bordered" id="tblVariacao">
                        <thead>
                          <tr>
                            <th>Variacao</th>
                            <th>Valor</th>
                            <th>Quantidade</th>
                            <th>Peso</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach ($variacao as $e){
                            $CI = &get_instance();

                                        //echo '<pre>';
                                        //var_dump($e);
                                        //echo '</pre>';

                            echo '<tr id="form-'.$e->id.'">';
                            echo '<input type="hidden" id="form-'.$e->id.'-id_rel" name="id_rel" value="'.$e->id.'" />';
                            echo '<td>'.$e->titulo.'</td>';                                                                                
                            echo '<td><input id="form-'.$e->id.'-valor" class="money" style="width: 60px;" type="text" name="valor" value="'.$e->valor.'"  /></td>';
                            echo '<td><input id="form-'.$e->id.'-qtd" style="width: 60px;" type="text" name="qtd" value="'.$e->qtd.'"  /></td>';
                            echo '<td><input id="form-'.$e->id.'-peso" style="width: 60px;" type="text" name="peso" value="'.$e->peso.'"  /></td>';
                            echo '<td>';
                            echo '<a href="" equiAcao="'.$e->idVariacao.'"title="Remover Variacao do produto" class="btn btn-danger tip-top variacao-delete" data-select="#form-'.$e->id.'"><i class="icon-remove icon-white"></i></a>';
                            echo '<button type="submit" title="Atualizar Variacao do produto" class="btn btn-success tip-top variacao-update" data-select="#form-'.$e->id.'"><i class="icon-ok icon-white"></i></a>';
                            echo '</td>';
                            echo '</tr>';
                          } ?>
                          <tr>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  <?php } ?>

                  <h3></h3>
                  <div class="widget-title">
                    <!-- INÍCIO API FOTO -->
                    <span class="icon">
                      <i class="icon-picture"></i>
                    </span>
                    <h5>
                      Imagens do produto
                    </h5>
                  </div>
                  <div id="imgPopupBack" class="modal">
                    <span class="imgPopupClose">×</span>
                    <img class="modal-content" id="imgPopupView">
                    <div id="imgPopupText"></div>
                  </div>
                  <br/>
                  <!-- <script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script> -->
                  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script> -->
                  <script type="text/javascript"
                  src="<?php echo base_url() . reisdev('url') ?>cam-api/functions.js"></script>
                  <style type="text/css">@import url("<?php echo base_url().reisdev('url')?>cam-api/styles.css");</style>
                  <style>
                  .pnPrev{
                    width: 100% !important;
                    max-width:2500px !important;
                    min-height: 0 !important;
                    height: 150px !important;
                    box-sizing:border-box;
                  }

                  .pnPrev .pnI{
                    width: 16%;
                    margin: 0.33%;
                    max-width:100%; 
                    display: block;
                    float:left;
                    box-sizing:border-box;
                    position:relative;
                  }

                  .pnPrev .pnI .btn-danger{
                    top: 0;
                    right:-5px;
                  }

                  .pnPrev .pnImg{
                    width: 100%;
                    height: 110px;
                  }

                  .pnPrev .pnImg img{
                    height: auto !important;
                  }
                </style>
                <div id="conteudo">
                  <div class="formulario">
                    <div class="dados">
                      <div class="campos">
                        <div class="campo last" style="width:100%; text-align:center;">
                          <div id="pnPrev" class="pnPrev">
                            <div id="pnImg07" class="pnI">
                              <a onclick="excludeImage(7)" id="delImg07" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg07" class="pnImg"></div>
                            </div>
                            <div id="pnImg08" class="pnI">
                              <a onclick="excludeImage(8)" id="delImg08" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg08" class="pnImg"></div>
                            </div>
                            <div id="pnImg09" class="pnI">
                              <a onclick="excludeImage(9)" id="delImg09" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg09" class="pnImg"></div>
                            </div>
                            <div id="pnImg10" class="pnI">
                              <a onclick="excludeImage(10)" id="delImg10" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg10" class="pnImg"></div>
                            </div>
                            <div id="pnImg11" class="pnI">
                              <a onclick="excludeImage(11)" id="delImg11" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg11" class="pnImg"></div>
                            </div>
                            <div id="pnImg12" class="pnI">
                              <a onclick="excludeImage(12)" id="delImg12" class="btn btn-danger bt btDel btHidden tip-top"
                              title="Excluir"> <i class="icon-remove icon-white"></i></a>
                              <div id="divImg12" class="pnImg"></div>
                            </div>
                          </div>
                        </div>
                        <div class="campo last" style="width:100%; text-align:center;">
                          <div class="panel" style="width:100%;">
                            <div id="pnPreview" class="preview" height="240px" style="left: 50%; transform: translate(-50%,0);"></div>
                            <div id="pnCam" height="240px"></div>
                          </div>
                          <select id="tipoEntrega" class="cbox" name="tipoEntrega" size="1" style="width:50%; margin: 5px; float:none;">
                            <option value="1">Entrega do produto para a loja
                            </select>
                            <select id="cbQuality" class="cbox" name="pages" size="1" onChange="changedQuality(this);" style="width:50%; margin: 5px; float:none;">
                              <option value="25">Muito Baixa(25%)
                                <option value="50">Baixa(50%)
                                  <option value="75" selected>Normal(75%)
                                    <option value="90">Alta(90%)
                                      <option value="100">Muito Alta(100%)
                                      </select>
                                      <div>
                                        <button type="button" id="btCapture" class="button"> Capturar</button>
                                        <button type="button" id="btFiles" class="button" onclick="$('#imgfiles').click();"> Selecionar imagens</button>
                                        <input type="file" name="files" id="imgfiles" style="display: none;" multiple>
                                        <button type="button" id="btSave" hidden="true" class="button"> Manter</button>
                                        <button type="button" id="btDiscard" hidden="true" class="buttonred"> Descartar</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- FIM API FOTO -->
                          </div>
                        </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
            <div id="modal-inserir-variacao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <form action="<?php echo base_url() ?>index.php/produtos/inserirProdutoAjax" method="post" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h5 id="myModalLabel">Inserir Variacao</h5>
                </div>
                <div class="modal-body">
                  <input type="text" name="descricao" placeholder="Nome">
                  <input type="text" name="estoque" placeholder="Qtd">
                  <input id="pai_id" type="hidden" class="span12" name="pai_id" value="<?php echo $result->idProdutos; ?>"  />
                  <input type="hidden" name="codigo_barras" value="<?php echo $result->codigo_barras; ?>"  />
                  <input type="hidden" name="tipo"          value="<?php echo $result->tipo; ?>"  />
                  <input type="hidden" name="ligacao"       value="<?php echo $result->ligacao; ?>"  />
                  <input type="hidden" name="peso"          value="<?php echo $result->peso; ?>"  />
                  <input type="hidden" name="impostos"      value="<?php echo $result->impostos; ?>"  />
                  <input type="hidden" name="fornecedor_id" value="<?php echo $result->fornecedor_id; ?>"  />
                  <input type="hidden" name="estoque_ideal" value="<?php echo $result->estoque_ideal; ?>"  />
                  <input type="hidden" name="idFranquia"    value="<?php echo $result->idFranquia; ?>"  />
                  <input type="hidden" name="unidade"       value="<?php echo $result->unidade; ?>"  />
                  <input type="hidden" name="marcas_id"     value="<?php echo $result->marcas_id; ?>"  />
                  <input type="hidden" name="precoCompra"   value="<?php echo $result->precoCompra; ?>"  />
                  <input type="hidden" name="precoVenda"    value="<?php echo $result->precoVenda; ?>"  />
                  <input type="hidden" name="ncm"           value="<?php echo $result->ncm; ?>"  />
                  <input type="hidden" name="cfop"          value="<?php echo $result->cfop; ?>"  />
                  <input type="hidden" name="estoqueMinimo" value="<?php echo $result->estoqueMinimo; ?>"  />
                  <h5 style="text-align: center"></h5>
                </div>
                <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                  <button class="btn btn-primary" id="btnInserirVariacaoAjax">Inserir</button>
                </div>
              </form>
            </div>
          </div>

          <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
          <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

          <script type="text/javascript">
            $(document).ready(function(){

              window.onload = function (event) {
                updatePreview();
              };

              $("#marca").autocomplete({
                source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
                minLength: 1,
                select: function( event, ui ) {
                  $("#marcas_id").val(ui.item.id);
                }
              });

              $("#variacao").autocomplete({
                  source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteVariacao",
                  minLength: 2,
                  select: function( event, ui ) {
                      $("#idVariacao").val(ui.item.id);
                  }
              });

              $("#fornecedor").autocomplete({
                source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
                minLength: 1,
                select: function( event, ui ) {
                  $("#fornecedor_id").val(ui.item.id);
                }
              });

              $("#formVariacao").validate({
                rules:{
                 variacao: {required:true} 
               },
               messages:{
                 variacao: {required: 'Insira o nome do variacao'}
               },
               submitHandler: function( form )
               {
                var dados = $( form ).serialize();
                $("#divVariacao").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax(
                {
                  type: "POST",
                  url: "<?php echo base_url();?>index.php/produtos/adicionarVariacaoAjax",
                  data: dados,
                  dataType: 'json',
                  success: function(data)
                  {
                    if(data.result == true)
                    {
                     $("#divVariacao" ).load("<?php echo current_url();?> #divVariacao" );
                     $("#variacao").val('');
               }
               else
               {
                 alert("Não há mais espaço para adicionar um variacao, favor liberar para continuar adicionando;");
                 $("#divVariacao" ).load("<?php echo current_url();?> #divVariacao" );
                 $("#variacao").val('');
                 $("#marca_id").val('');
               }
            //alert("OS criada com sucesso!");
            /*
            window.location = "<?php echo base_url() ?>index.php/produtos/visualizar/<?php echo $result->idProdutos; ?>";*/
          },
          error: function(){
           $("#divVariacao" ).load("<?php echo current_url();?> #divVariacao" );
           $("#variacao").val('');
           $("#marca_id").val('');
         }
       });
                return false;
              }
            });
              $(document).on('click', '#divVariacao a', function(event) {
                var idVariacao = $(this).attr('idacao');
                if((idVariacao % 1) == 0)
                {
                  $("#divVariacao").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/excluirVariacao",
                    data: "idVariacao="+idVariacao,
                    dataType: 'json',
                    success: function(data)
                    {
                      if(data.result == true)
                      {
                        $( "#divVariacao" ).load("<?php echo current_url();?> #divVariacao" );
                      }
                      else{
                        alert('Ocorreu um erro ao tentar excluir variacao.');
                      }
                    }
                  });
                  return false;
                }
              });

              $(document).on('click', '#btnInserirVariacaoAjax', function(event) {
                var preenchido = true;
                $(this).closest("form").find("input[type=text]").each(function(){
                  if($(this).val() == ""){
                    preenchido = false;
                    return false;
                  }
                });
                if(!preenchido){
                  alert("Informe todos os campos do cliente pra inserir!");
                }else{
                  var dados = $(this).closest("form").serialize();
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/produtos/inserirProdutoAjax",
                    data: dados,
                    dataType: 'json',
                    success: function(data){
                      if(data.result == true){
                        $('#modal-inserir-variacao').modal('hide');
                        window.location.reload(false);
                      }else{
                        alert("Ocorreu um erro ao tentar adicionar um variacao!");
                      }
                    },
                    error: function(){
                      alert("ERRO!");
                    }
                  });
                }
                return false;
              });

              $(document).on('click', '.variacao-update', function(event) {
                  event.preventDefault();
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/produtos/atualizarVariacao",
                    data: {'id_rel':$($(this).data('select')+'-id_rel').val(), 'valor':$($(this).data('select')+'-valor').val(), 'qtd':$($(this).data('select')+'-qtd').val(), 'peso':$($(this).data('select')+'-peso').val()},
                    dataType: 'json',
                    success: function(data){
                      if(data.result == true){
                        // window.location.reload(false);
                      }else{
                        alert("Ocorreu um erro ao tentar adicionar um variacao!");
                      }
                    },
                    error: function(){
                      alert("ERRO!");
                    }
                  });
              });

              $(document).on('click', '.variacao-delete', function(event) {
                  event.preventDefault();
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/produtos/deletarVariacao",
                    data: {'id_rel':$($(this).data('select')+'-id_rel').val()},
                    dataType: 'json',
                    success: function(data){
                      if(data.result == true){
                        console.log($(this).data('select'));
                        // $($(this).data('select')).remove();
                      }else{
                        alert("Ocorreu um erro ao tentar adicionar um variacao!");
                      }
                    },
                    error: function(){
                      alert("ERRO!");
                    }
                  });
              });

              $(document).on('click', '#btnInserirVariacaoAjax', function(event) {
                  var preenchido = true;
                  $(this).closest("form").find("input[type=text]").each(function(){
                      if($(this).val() == ""){
                          preenchido = false;
                          return false;
                      }
                  });
                  if(!preenchido){
                      alert("Informe todos os campos do cliente pra inserir!");
                  }else{
                      var dados = $(this).closest("form").serialize();
                      $.ajax({
                          type: "POST",
                          url: "<?php echo base_url() ?>index.php/produtos/inserirProdutoAjax",
                          data: dados,
                          dataType: 'json',
                          success: function(data){
                              if(data.result == true){
                                  $('#modal-inserir-variacao').modal('hide');
                                  window.location.reload(false);
                              }else{
                                  alert("Ocorreu um erro ao tentar adicionar um variacao!");
                              }
                          },
                          error: function(){
                              alert("ERRO!");
                          }
                      });
                  }
                  return false;
              });

              $('#formProduto').validate({
                rules :{
                  ncm: { required: true},
                  cfop: { required: true},
                  descricao: { required: true},
                  unidade: { required: true},
                  marca: { required: true},
                  precoCompra: { required: true},
                  precoVenda: { required: true},
                  estoque: { required: true}
                },
                messages:{
                  cfop: { required: ''},
                  ncm: { required: ''},
                  descricao: { required: ''},
                  unidade: {required: ''},
                  marca: {required: ''},
                  precoCompra: { required: ''},
                  precoVenda: { required: ''},
                  estoque: { required: ''}
                },
                errorClass: "error",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                  $(element).parents('.campo').removeClass('success');
                  $(element).parents('.campo').addClass('error');
                },
                unhighlight: function(element, errorClass, validClass) {
                  $(element).parents('.campo').removeClass('error');
                  $(element).parents('.campo').addClass('success');
                }
              });
            });
          </script>