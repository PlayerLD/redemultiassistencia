<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<?php $idFranquia = $this->session->userdata('id');?>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Tipo de Produto</h5>
            </div>
            <div class="widget-content">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
                <div id="conteudo">
                  <div class="formulario">
                        <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >
                          <input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
                          <div class="dados">
                          <div class="campos">
                                  <div class="campo last" style="width: 100%">
                                      <label for="titulo">Nome<span class="required">*</span></label>
                                        <input id="titulo" type="text" name="titulo" value="<?php echo set_value('titulo'); ?>"  />
                                    </div>
                                  <div class="campo last" style="width: 100%">
                                      <label for="valor">Valor %<span class="required">*</span></label>
                                        <input id="valor" class="money" type="text" name="valor" value="<?php echo set_value('valor'); ?>"  />
                                    </div>
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                      <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                    <a href="<?php echo base_url() ?>index.php/produtos/gerenciar_taxas" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>              
<script type="text/javascript">
  $(document).ready(function(){       

        $('#formProduto').validate({
            rules :{
                  titulo: { required: true},
                  valor: { required: true}
            },
            messages:{
                  titulo: { required: ''},
                  valor: { required: ''}
            },

            errorClass: "error",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
              $(element).parents('.campo').removeClass('success');
        $(element).parents('.campo').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.campo').removeClass('error');
                $(element).parents('.campo').addClass('success');
            }
    });
  });
</script>