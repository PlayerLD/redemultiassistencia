<!DOCTYPE html>
<html lang="pt-br"> 
<head>
    <title>ASSISTÊNCIA TÉCNICA ONLINE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
    <style>
        body {
            background-color: #0E6FB6;
        }
        body {
            color: white;
        }
        footer, header {
            color: gray;
            text-align: center;
            padding: 40px;
            background-color: white;
            margin-bottom: 0;
        }
        legend {
            font-size: 20pt;
            color: white;
        }
        @media (max-width: 767px){
            .modal {
                position: fixed;
                top: 20px;
                right: none;
                left: 50%;
                margin: 0;
                padding: 15px;
            }
            div#sticky h1 {
                font-size: 23px !important;
            }
        }
    </style>
</head>
<body>
<br />
<center>
    <a href="http://redemultiassistencia.com.br">
        <img src="<?php echo base_url();?>assets/img/franquia-rede-multi-assistencia.png" alt="Logo" class="img" />
    </a>
</center>
<header>
    <?php if($this->session->flashdata('success')): ?>
        <h2><?php echo $this->session->flashdata('success')?></h2>
    <?php else: ?>
        <h2>Orçamento de Reparos em LOGGI PARCERIA - ADRIANA?></h2>
    <?php endif; ?>
</header>
<div class="container" style="padding: 0 15px;">
    <br />
    <div class="row">
        <div class="span7">
            <?php if($custom_error == true){ ?>
                <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique se os campos estão preenchidos corretamente.</div>
            <?php } ?>
            <form method="post" action="<?php echo current_url();?>?tipo=<?php echo $tipo; ?>" class="form" id="frmOrcamento">
                <fieldset>
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="nome">Nome (obrigatório)</label>
                        <div class="controls">
                            <input id="nome" name="nome" type="text" placeholder="" class="input-xxlarge span7" required="" value="<?php echo set_value('nome')?>">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="email">E-mail</label>
                        <div class="controls">
                            <input id="email" name="email" type="email" placeholder="" class="input-xxlarge span7"value="<?php echo set_value('email')?>">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="telefone">Seu Telefone + DDD (obrigatório)</label>
                        <div class="controls">
                            <input id="telefone" name="telefone" type="text" placeholder="" class="input-xxlarge span7" required="" value="<?php echo set_value('telefone')?>">
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="control-group">
                        <label class="control-label" for="marca">Marca do Aparelho (obrigatório)</label>
                        <div class="controls">
                            <select id="marca" name="marca" class="input-xxlarge span7">
                                <option value="">Escolha a Marca</option>
                                <?php
                                    foreach($marcas as $marca):
                                        $selected = ($marca == set_value('marca') ? 'selected': '');
                                        echo "<option value='$marca' $selected>$marca</option>";
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="modelo">Modelo do Aparelho (obrigatório)</label>
                        <div class="controls">
                            <input id="modelo" name="modelo" type="text" placeholder="" class="input-xxlarge span7" required="" value="<?php echo set_value('modelo')?>">
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="control-group">
                        <label class="control-label" for="defeito">Defeito do Aparelho (obrigatório)</label>
                        <div class="controls">
                            <select id="defeito" name="defeito" class="input-xxlarge span7">
                                <option value="">Defeito do Aparelho</option>
                                <?php
                                foreach($defeitos as $defeito):
                                    $selected = ($defeito == set_value('defeito') ? 'selected': '');
                                    echo "<option value='$defeito' $selected>$defeito</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="control-group">
                        <label class="control-label" for="franquia">Escolha o estado (obrigatório)</label>
                        <div class="controls">
                            <select id="estado" type="text" name="estado" class="input-xxlarge span7" required>
                                <option>Escolha o estado</option>
                                <option value="Acre">                Acre</option>
                                <option value="Alagoas">             Alagoas</option>
                                <option value="Amapá">               Amapá</option>
                                <option value="Amazonas">            Amazonas</option>
                                <option value="Bahia">               Bahia</option>
                                <option value="Ceará">               Ceará</option>
                                <option value="Distrito Federal">    Distrito Federal</option>
                                <option value="Espírito Santo">      Espírito Santo</option>
                                <option value="Goiás">               Goiás</option>
                                <option value="Maranhão">            Maranhão</option>
                                <option value="Mato Grosso">         Mato Grosso</option>
                                <option value="Mato Grosso do Sul">  Mato Grosso do Sul</option>
                                <option value="Minas Gerais">        Minas Gerais</option>
                                <option value="Pará">                Pará</option>
                                <option value="Paraíba">             Paraíba</option>
                                <option value="Paraná">              Paraná</option>
                                <option value="Pernambuco">          Pernambuco</option>
                                <option value="Piauí">               Piauí</option>
                                <option value="Rio de Janeiro">      Rio de Janeiro</option>
                                <option value="Rio Grande do Norte"> Rio Grande do Norte</option>
                                <option value="Rio Grande do Sul">   Rio Grande do Sul</option>
                                <option value="Rondônia">            Rondônia</option>
                                <option value="Roraima">             Roraima</option>
                                <option value="Santa Catarina">      Santa Catarina</option>
                                <option value="São Paulo">           São Paulo</option>
                                <option value="Sergipe">             Sergipe</option>
                                <option value="Tocantins">           Tocantins</option>
                            </select>
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="control-group">
                        <label class="control-label" for="franquia">Escolha a Unidade (obrigatório)</label>
                        <div class="controls">
                            <select id="franquia" name="franquia" class="input-xxlarge span7" disabled>
                                <option value="">Selecione a Unidade</option>
                            </select>
                        </div>
                    </div>
                    <!-- Textarea -->
                    <div class="control-group">
                        <label class="control-label" for="obs">Observação</label>
                        <div class="controls">
                            <textarea id="obs" name="obs" style="min-width: 100%" rows="5" class="span7"><?php echo set_value('obs')?></textarea>
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnEnviar"></label>
                        <div class="controls">
                            <button id="btnEnviar" name="btnEnviar" class="btn btn-success">Enviar</button>
                        <onClick="ga('send', 'event', 'botao', 'clique');"></onClick>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="span5">
            <img src="<?php echo base_url();?>assets/img/garantia-conserto-de-celular-tablets-notebook-video-game.png" alt="Garantia" class="img" />
        </div>
    </div>
</div>
<footer>
    © 2018 Rede Multi Assistência. All Rights Reserved
</footer>
<div id="sticky" class="modal" style="position: fixed; text-align: center; max-width: 800px; margin-left: 0;  transform: translateX(-50%);">
  
  <!-- <a href="#close-modal" rel="modal:close" class="close-modal">X</a> -->
  <h1 style="color: black;">Infelizmente não temos unidadades nesta região :(<br><br> Aproveite e leve nossa franquia para esta região :)</h1>
  <small style="color: black;font-size: 16px;">Aproveite a oportunidade e conheça nossos benefícios!</small><br>
  <a class="btn btn-success" target="_blank" style="margin-top: 20px;border-radius: 6px;font-size: 25px;padding: 13px 30px;" href="https://redemultiassistencia.com.br/franquia-assistencia-tecnica-de-celular/">Conheça agora</a>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate_pt-br.js"></script>
<script>
    franquias = '<?php echo json_encode($franquias); ?>';
    $(function () {
        $("#frmOrcamento").validate({
            rules: {
                nome: 'required',
                email: 'email',
                telefone: 'required',
                marca: 'required',
                modelo: 'required',
                defeito: 'required',
                franquia: 'required'
            }
        });
    });
    $('select[name=estado]').on('change', function(){
        var estado = $(this).val(),
            options = '';
        if(franquias.indexOf(estado) != -1){
            
            franquiasObj = JSON.parse(franquias);
            $('select[name="franquia"]').attr('disabled', false);
            for (var i = franquiasObj.length - 1; i >= 0; i--) {
                if(franquiasObj[i].estado == estado){
                    options += '<option value="'+franquiasObj[i].idFranquias+'">'+franquiasObj[i].nome+'</option>';
                }
            }
            $('select[name="franquia"]').html(options);

            var itensOrdenados = $('select[name="franquia"] option').sort(function (a, b) {
                return a.text < b.text ? -1 : 1;
            });

            $('select[name="franquia"]').html(itensOrdenados);
            $('select[name="franquia"]').prepend('<option value="" selected>Selecione a Unidade</option>');
        }else{
            $('select[name="franquia"]').html(options);
            $("#sticky").modal();
        }
    });
</script>
</body>
</html>