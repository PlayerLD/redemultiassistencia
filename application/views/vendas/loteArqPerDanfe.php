
<?php if(!empty($custom_error)){ ?>
  <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;"><?php echo $custom_error; ?></div>
<?php } ?>


<div class="widget-box">
  <div class="widget-title">
              <span class="icon"><i class="icon-hdd"></i></span>
              <h5>DANFE - PDFs</h5>
  </div>
</div> 
<div class="widget-content nopadding">
<?php 
$CI = &get_instance();

  require (reisdev('bootstrap'));
  $dt = new DataTable();
  $dt->Columns(array('Cód', 'Data da Venda', 'Cliente', 'Faturado', 'PDFs - Chave NF-e', ''));
  if($results)
  {
    //echo "<pre>";
    //var_dump($results);
    //echo "</pre>";

    foreach ($results as $r)
    {

    if ($r->numNFE == NULL) {
          $msg = "SEM REGISTRO.";
        }else{
          $msg = $r->numNFE.' - '.'<a href="'.base_url().'index.php/vendas/downloaddanfeone/'.$r->numNFE.'" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>';
        }

      $dataVenda = date(('d/m/Y'), strtotime($r->dataVenda));
                if($r->faturado == 1)
                  $faturado = '<span class="label label-success">Sim</span>';
                else
                  $faturado = '<span class="label label-important">Não</span>';
                
      $buttons = ' ';
                if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda'))
                    //$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/vendas/visualizar/'.$r->idVendas.'" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
                
                if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda'))
                    //$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/vendas/editar/'.$r->idVendas.'" class="btn btn-info tip-top" title="Editar venda"><i class="icon-pencil icon-white"></i></a>';
                  
          if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda'))
                    //$buttons = $buttons.'<a target="_blank" style="margin-right: 1%" href="'.base_url().'agile/gerar-nfe.php?idVenda='.$r->idVendas.'&idFranquia='.$this->session->userdata('id').'" class="btn  btn-warning" title="Gerar Nfe"><i class="icon-file icon-white"></i></a>';
          
          if($this->permission->checkPermission($this->session->userdata('permissao'),'dVenda'))
                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" venda="'.$r->idVendas.'" class="btn btn-danger tip-top" title="Excluir Venda"><i class="icon-remove icon-white"></i></a>';
            
                $dt->add_Item(array
      (
        $r->idVendas,
        $dataVenda,
        '<a href="'.base_url().'index.php/clientes/visualizar/'.$r->idClientes.'">'.$r->nomeCliente.'</a>',
        $faturado,
        $msg, 
        $buttons
      ));
    }
  }
  $dt->End();
?>
</div>
 

  <br>
  <br>
  <a href="<?php echo base_url('index.php/vendas/loteArqPerDanfeAll'); ?>" class="btn btn-warning">Download Por Períodos</a>

 

<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var XML = $(this).attr('XML');
        $('#idDocumento').val(XML);

   });

   //$(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
});

</script>
