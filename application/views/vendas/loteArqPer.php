<?php if(!empty($custom_error)){ ?>
  <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;"><?php echo $custom_error; ?></div>
<?php } ?>

<div class="widget-box">
	<div class="widget-title">
    	<span class="icon"><i class="icon-hdd"></i></span>
    	<h5>XML - Por Lotes</h5>
	</div>
</div> 

<div class="widget-content nopadding">

<?php  if($this->permission->checkPermission($this->session->userdata('permissao'),'vXML')) { ?>

    <h4>Selecione as datas, para fazer o download dos arquivos que estiverem dentro do período.</h4>
    <form action="<?php echo base_url(); ?>index.php/vendas/loteArqPer" id="formDatasArquivos" method="post" class="form-horizontal">
      <input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $this->session->userdata('id'); ?>" />
      <div class="span3">
          <label for="dataInicial">Data Inicial<span class="required">*</span></label>
          <input type="date" id="dataInicial" class="span12 datepicker" type="text" name="dataInicial"/>
      </div>
      <div class="span3">
          <label for="dataFinal">Data Final<span class="required">*</span></label>
          <input type="date" id="dataFinal" class="span12 datepicker" type="text" name="dataFinal" />
      </div>
      <div class="span3">
          <label for="dataFinal">---</label>
          <button class="btn btn-success" id="btnpesquisar">Verificar e Fazer download</button>
      </div>
    </form>

<?php } ?>    


  <br>
  <br>
  <hr>
  <p>
    <h4>Clique no botão para realizar o donwload de todos os arquivos.</h4>
  	
  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vXML')) { ?>
      <a href="<?php echo base_url('index.php/vendas/loteDownload/'.$this->session->userdata('id')); ?>" target="_blank" class="btn btn-warning">Fazer download em LOTE - TODOS XMLs</a>
  <?php  } ?>
  
  </p>
    

</div>