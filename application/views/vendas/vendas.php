<?php
	require (reisdev('bootstrap'));
	if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda'))
	{ ?>
    		<a href="<?php echo base_url();?>index.php/vendas/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Venda</a>
<?php 	} ?>
 
<div class="widget-box">
     	<div class="widget-title">
        	<span class="icon"><i class="icon-tags"></i></span>
        	<h5>Vendas</h5>
     	</div> 
</div>
<div class="widget-content nopadding">
<!-- aqui ficava o codigo original -->
    <table id="memListTable" class="table table-striped table-bordered" style="width:100%">
      	<thead>
			<tr>  
				<th>Cod</th>
				<th>Data Venda</th>
				<th>Cliente</th>
				<th>Faturado</th>
				<th>Chave Nfe</th>
				<th>Botão de Ação</th>
			</tr>
     	</thead>
      
    </table>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/vendas/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir Venda</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idVenda" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir esta Venda?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>



<br>
  <br>
  <a href="<?php echo base_url('index.php/vendas/loteArqPer'); ?>" class="btn btn-warning">Download Por Períodos - XML </a>
  <a href="<?php echo base_url('index.php/vendas/ArquivosDanfes'); ?>" class="btn btn-warning">Download Por Períodos - DANFE</a>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
      $('#memListTable').DataTable({
          "language": {
            "lengthMenu": "Nº Os _MENU_ total por páginas",
            "zeroRecords": "Nada encontrado - Desculpe!",
            "info": "Mostrando _PAGE_ de _PAGES_ Total de _MAX_",
            "infoEmpty": "Sem registo disponível ",
            "infoFiltered": "(Filtrados _MAX_ total de dados)",
            "infoPostFix":    "",
            "thousands":      ",",
            "loadingRecords": "Buscando...",
            "processing":     "Processando...",
            "search":         "Procurar:",
            "zeroRecords":    "Nenhum registro correspondente encontrado",
            "paginate": {
                "first":      "Próximo",
                "last":       "Anterior",
                "next":       "Avançar",
                "previous":   "Retornar"
              }
          },
          "order": [[ 0, "desc" ]],
          "processing": true,
          "serverSide": true,
          "ajax":{
          "url": "<?php echo base_url('index.php/vendas/getlists'); ?>",
          "dataType": "json",
          "type": "POST",
          "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                         },
        "columns": [
       	    {"data": "idVendas"},
		  	{"data": "dataVenda"},
		  	{"data": "cliente"},
		  	{"data": "faturado"},
		  	{"data": "chaveNfe"},
          	{ "data": "todosB"},
        ]

      });
    });
</script>


<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var venda = $(this).attr('venda');
        $('#idVenda').val(venda);

    });

});

</script>