<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon"> 
                    <i class="icon-tags"></i>
                </span>
                <h5>Cadastro de venda</h5>
            </div>
            <div class="widget-content nopadding">
                

                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da venda</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divCadastrarOs">
                                <?php if($custom_error == true){ ?>
                                <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique os campos com asterisco ou se selecionou corretamente cliente e responsável.</div>
                                <?php } ?>
                                <form action="<?php echo current_url(); ?>" method="post" id="formVendas">

                                    <div class="span12" style="padding: 1%">

                                        <div class="span2">
                                            <label for="dataInicial">Data da Venda<span class="required">*</span></label>
                                            <input id="dataVenda" class="span12 datepicker" type="text" name="dataVenda" value="<?php echo date('d/m/Y'); ?>"  />
                                        </div>
                                        <div class="span5" style="position: relative;">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="span12" type="text" name="cliente" value=""  />
                                            <input id="clientes_id" class="span12" type="hidden" name="clientes_id" value=""  />
                                            <a style="position: absolute;top: 25px;right: 21px;height: 25px;line-height: 25px;" href="#modal-inserir-cliente" role="button" data-toggle="modal" class="btn btn-primary " title="Inserir Novo Cliente"><i class="icon-plus icon-white"></i></a>
                                        </div>
                                        <div class="span5">
                                            <label for="tecnico">Vendedor<span class="required">*</span></label>
                                            <input id="tecnico" class="span12" type="text" name="tecnico" value=""  />
                                            <input id="usuarios_id" class="span12" type="hidden" name="usuarios_id" value=""  />
                                        </div>
                                        
                                    </div>
                              
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                            <a href="<?php echo base_url() ?>index.php/vendas" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>

                
.
             
        </div>
        
    </div>
</div>
</div>

<div id="modal-inserir-cliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/clientes/inserirClienteAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Cliente</h5>
        </div>
        <div class="modal-body">
            <input type="text" name="nomeCliente" placeholder="Nome do cliente">
            <input type="text" name="docCliente" placeholder="CPF/CNPJ">
            <input type="text" name="rgCliente" placeholder="RG">
            <input type="text" name="inscricaoEstadual" placeholder="Inscrição Estadual">
            <input type="text" name="telefoneCliente" placeholder="Telefone">
            <input type="text" name="emailCliente" placeholder="Email">
            <input type="text" name="cepCliente" placeholder="CEP">
            <input type="text" name="ruaCliente" placeholder="Rua">
            <input type="text" name="numeroCliente" placeholder="Número">
            <input type="text" name="bairroCliente" placeholder="Bairro">
            <input type="text" name="cidadeCliente" placeholder="Cidade">
            <input type="text" name="estadoCliente" placeholder="Estado">
            <h5 style="text-align: center"></h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-primary" id="btnInserirClienteAjax">Inserir</button>
        </div>
    </form>
</div>



<script type="text/javascript">
$(document).ready(function(){

      $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/vendas/autoCompleteCliente",
            minLength: 1,
            select: function( event, ui ) {

                 $("#clientes_id").val(ui.item.id);
                

            }
      });

      $("#tecnico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/vendas/autoCompleteUsuario",
            minLength: 1,
            select: function( event, ui ) {

                 $("#usuarios_id").val(ui.item.id);


            }
      });

      
      

      $("#formVendas").validate({
          rules:{
             cliente: {required:true},
             tecnico: {required:true},
             dataVenda: {required:true}
          },
          messages:{
             cliente: {required: 'Campo Requerido.'},
             tecnico: {required: 'Campo Requerido.'},
             dataVenda: {required: 'Campo Requerido.'}
          },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
       });

    $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });

    $(document).on('click', '#btnInserirClienteAjax', function(event) {
        var preenchido = true;
        $(this).closest("form").find("input[type=text]").each(function(){
            if($(this).val() == ""){
                preenchido = false;
                return false;
            }
        });
        
        if(!preenchido){
            alert("Informe todos os campos do cliente pra inserir!");
        }else{
            var dados = $(this).closest("form").serialize();
            
            console.log(dados); 
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/clientes/inserirClienteAjax",
                data: dados,
                dataType: 'json',
                success: function(data){
                    if(data.result == true){
                        $('#modal-inserir-cliente').modal('hide');
                        $("#cliente").val(data.nomeCliente);
                        $("#clientes_id").val(data.idCliente);
                    }else{
                        alert("Ocorreu um erro ao tentar adicionar um cliente!");
                    }
                },
                error: function(){
                    alert("ERRO!");
                }
            });
        }
        
        return false;
    });
   
});

</script>

