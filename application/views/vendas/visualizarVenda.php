<?php $totalProdutos = 0;?>
<style>
	@media print {
		#header, #user-nav, #search, #sidebar, #content-header, .widget-title, body>.row-fluid{
			display: none;
		}
		body{
			margin: 0;
			padding: 0;
		}
		#content{
			margin: 0;
			paddnig:0
		}
	}
</style>
<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Venda</h5>
                <div class="buttons">
	    	<? if ($result) { ?>
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                        echo '<a title="Icon Title" class="btn btn-mini btn-info" href="'.base_url().'index.php/vendas/editar/'.$result->idVendas.'"><i class="icon-pencil icon-white"></i> Editar</a>'; 
                    } ?>
                    <?php echo '<a title="" href="'.base_url().'agile/gerar-nfe.php?idVenda='.$result->idVendas.'&idFranquia='.$this->session->userdata('id').'" class="btn btn-mini btn-warning"><i class="icon-file icon-white"></i> Gerar NF-e</a>'; ?>
                    <?php echo '<a title="" href="#modal-cancelar" role="button" data-toggle="modal" class="btn btn-mini btn-danger"><i class="icon-file icon-white"></i> Cancelar NF-e</a>'; ?>
                    <a id="imprimir" title="Imprimir" class="btn btn-mini btn-inverse" href="#"><i class="icon-print icon-white"></i> Imprimir</a>
                <? } ?>
                </div>
            </div>
            <div class="widget-content" id="printOs">
            	<?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>
	    <?
	    	if (!$result)
	    	{
	    ?>
	            <div class="control-group">
	                <label class="control-label" style="width: 100%; text-align: left; color: red; margin-bottom: 175px;">&emsp;&emsp;&emsp;<strong>Error: </strong>Venda não encontrada.</label>
	            </div>
	    <?	
	    	}
	    	else
	    	{
	    ?>
	                <div class="invoice-content">
	                    <div class="invoice-head">
	                        <table class="table">
	                            <tbody>
	
	                                <?php if($emitente == null) {?>
	                                            
	                                <tr>
	                                    <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/sistemaos/emitente">Configurar</a><<<</td>
	                                </tr>
	                                <?php } else {?>
	
	                                <tr>
	                                    <td style="width: 25%"><img src=" <?php echo $emitente[0]->url_logo; ?> "></td>
	                                    <td> <span style="font-size: 20px; "> <?php echo $emitente[0]->nome; ?></span> </br><span><?php echo $emitente[0]->cnpj; ?> </br> <?php echo $emitente[0]->rua.', nº:'.$emitente[0]->numero.', '.$emitente[0]->bairro.' - '.$emitente[0]->cidade.' - '.$emitente[0]->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente[0]->email.' - Fone: '.$emitente[0]->telefone; ?></span></td>
	                                    <td style="width: 18%; text-align: center">#Venda: <span ><?php echo $result->idVendas?></span></br> </br> <span>Emissão: <?php echo date('d/m/Y');?></span></td>
	                                </tr>
	
	                                <?php } ?>
	                            </tbody>
	                        </table>
	   
	                        <table class="table">
	                            <tbody>
	                                <tr>
	                                    <td style="width: 50%; padding-left: 0">
	                                        <ul>
	                                            <li>
	                                                <span><h5>Cliente</h5>
	                                                <span><?php echo $result->nomeCliente?></span><br/>
	                                                <span><?php echo $result->rua?>, <?php echo $result->numero?>, <?php echo $result->bairro?></span><br/>
	                                                <span><?php echo $result->cidade?> - <?php echo $result->estado?></span>
	                                            </li>
	                                        </ul>
	                                    </td>
	                                    <td style="width: 50%; padding-left: 0">
	                                        <ul>
	                                            <li>
	                                                <span><h5>Vendedor</h5></span>
	                                                <span><?php echo $result->nome?></span> <br/>
	                                                <span>Telefone: <?php echo $result->telefone?></span><br/>
	                                                <span>Email: <?php echo $result->email?></span>
	                                            </li>
	                                        </ul>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table> 
	      
	                    </div>
	
	                    <div style="margin-top: 0; padding-top: 0">
	
	
	                        <?php if($produtos != null){?>
	              
	                        <table class="table table-bordered table-condensed" id="tblProdutos">
	                                    <thead>
	                                        <tr>
	                                            <th style="font-size: 15px">Produto</th>
	                                            <th style="font-size: 15px">Quantidade</th>
	                                            <th style="font-size: 15px">fotos</th>
	                                            <th style="font-size: 15px">Sub-total</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                        <?php
	                                        foreach ($produtos as $p) {
	
	                                            $totalProdutos = $totalProdutos + $p->subTotal;
	                                            echo '<tr>';
	                                            echo '<td>'.$p->descricao.'</td>';
	                                            echo '<td>'.$p->quantidade.'</td>'; ?>
	                                            <td>
                                					<?php if(!empty($p->img07)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img07; ?>" width="200px"><?php } ?>
                                					<?php if(!empty($p->img08)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img08; ?>" width="200px"><?php } ?>
                                					<?php if(!empty($p->img09)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img09; ?>" width="200px"><?php } ?>
                                					<?php if(!empty($p->img10)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img10; ?>" width="200px"><?php } ?>
                                					<?php if(!empty($p->img11)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img11; ?>" width="200px"><?php } ?>
                                					<?php if(!empty($p->img12)){ ?><img src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/<?php echo $p->img12; ?>" width="200px"><?php } ?>
					                            </td>	                                            
	                                            <?php echo '<td>R$ '.number_format($p->subTotal,2,',','.').'</td>';?>
					                        </tr>
	                                        <?php } ?>
	                                        <tr>
	                                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
	                                            <td><strong>R$ <?php echo number_format($totalProdutos,2,',','.');?></strong></td>
	                                            <tr>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                               <?php }?>
	                        
	                
	                        <hr />
	                    
	                        <h4 style="text-align: right">Valor Total: R$ <?php echo number_format($totalProdutos,2,',','.');?></h4>
	
	                    </div>
	            
	
	                    
	                    
	              
	                </div>
	            </div>
            
             <? } ?>
        </div>
    </div>
</div>

<div id="modal-cancelar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>agile/gerar-nfe.php" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Cancelar NF-e</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" name="idVenda" value="<?php echo $result->idVendas; ?>">
            <input type="hidden" name="idFranquia" value="<?php echo $this->session->userdata('id'); ?>">
            <input type="text" name="cancelarNF" placeholder="Motivo do cancelamento">
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Sair</button>
            <button class="btn btn-danger">Cancelar NF-e</button>
        </div>
    </form>
</div>

<script src="<?php echo base_url() . 'assets/js/jQuery.print.js'?>" type="application/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#imprimir").click(function(){         
//            PrintElem('#printOs');
            window.print();
        });

        function PrintElem(elem)
        {
            Popup($(elem).html());
        }

        function Popup(data)
        {
            var mywindow = window.open('', 'MapOs', 'height=600,width=800');
            mywindow.document.write('<html><head><title>Map Os</title>');
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap-responsive.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-style.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-media.css' />");


            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

            return true;
        }

    });
</script>