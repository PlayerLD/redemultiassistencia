<a href="<?php echo base_url()?>index.php/franquias/adicionar<?php echo !empty($id_filial) ? '/'.$id_filial : ''; ?>" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Franquia</a>


<div class="widget-box">
        <div class="widget-title">
            <span class="icon"><i class="icon-user"></i></span>
            <h5>Franquias</h5>
        </div>
</div>
<div class="widget-content nopadding">
<?php
    require (reisdev('bootstrap'));
    
    $dt = new DataTable();
    $dt->Columns(array('C&#243;d', 'Nome', 'CPF/CNPJ', 'Telefone', 'N&#237;vel', ''));
    if($results)
    {
        foreach ($results as $r)
        {
            $buttons = '<a href="'.base_url().'index.php/franquias/editar/'.$r->idFranquias.'" class="btn btn-info tip-top" title="Editar Franquia"><i class="icon-pencil icon-white"></i></a>';

            if(empty($r->id_pai))
                $buttons .= '<a href="'.base_url().'index.php/franquias/filiais/'.$r->idFranquias.'" class="btn btn-info tip-top" title="Filiais"><i class="icon-group icon-white"></i></a>';

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'dFranquia'))
                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" franquia="'.$r->idFranquias.'" class="btn btn-danger tip-top" title="Excluir Franquia"><i class="icon-remove icon-white"></i></a>';

            $dt->add_Item(array
            (
                $r->idFranquias,
                $r->nome,
                $r->cpf,
                Mask::Tel($r->telefone),
                $r->permissao,
                $buttons
            ));
        }
    }
    $dt->End();
?>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/franquias/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Franquia</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idFranquiaExcluir" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir esta Franquia?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script>
    $( document ).ready(function()
    {
          $(document).on('click', 'a', function(event){
                var franquia = $(this).attr('franquia');
                $('#idFranquiaExcluir').val(franquia);
          });
     })
</script>