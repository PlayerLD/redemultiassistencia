<div class="row-fluid" style="margin-top:0">
	<div class="span12">
		<div class="widget-box">
 
			<form action="<?php echo current_url(); ?>" id="formFranquia" method="post" class="form-horizontal" >

				<div class="widget-title">
					<span class="icon"><i class="icon-user"></i></span>
                    <h5>Cadastro de Franquia</h5>
                </div>

                <div class="widget-content nopadding">

                    <?php if ($custom_error != ''): ?>
                        <div class="alert alert-danger"><?php echo $custom_error ?></div>
                    <?php endif; ?>                    

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="rg" class="control-label">RG<span class="required">*</span></label>
                        <div class="controls">
                            <input id="rg" type="text" name="rg" value="<?php echo set_value('rg'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cpf" class="control-label">CNPJ<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cpf" type="text" name="cpf" value="<?php echo set_value('cpf'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cep" class="control-label">CEP<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cep" type="text" name="cep" value="<?php echo set_value('cep'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="rua" class="control-label">Rua<span class="required">*</span></label>
                        <div class="controls">
                            <input id="rua" type="text" name="rua" value="<?php echo set_value('rua'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero" class="control-label">Numero<span class="required">*</span></label>
                        <div class="controls">
                            <input id="numero" type="text" name="numero" value="<?php echo set_value('numero'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="bairro" class="control-label">Bairro<span class="required">*</span></label>
                        <div class="controls">
                            <input id="bairro" type="text" name="bairro" value="<?php echo set_value('bairro'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cidade" class="control-label">Cidade<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cidade" type="text" name="cidade" value="<?php echo set_value('cidade'); ?>"  />
                        </div>
                    </div>

                    <!-- 'AC'=>'Acre', -->
                    <!-- 'AL'=>'Alagoas', -->
                    <!-- 'AP'=>'Amapá', -->
                    <!-- 'AM'=>'Amazonas', -->
                    <!-- 'BA'=>'Bahia', -->
                    <!-- 'CE'=>'Ceará', -->
                    <!-- 'DF'=>'Distrito Federal', -->
                    <!-- 'ES'=>'Espírito Santo', -->
                    <!-- 'GO'=>'Goiás', -->
                    <!-- 'MA'=>'Maranhão', -->
                    <!-- 'MT'=>'Mato Grosso', -->
                    <!-- 'MS'=>'Mato Grosso do Sul', -->
                    <!-- 'MG'=>'Minas Gerais', -->
                    <!-- 'PA'=>'Pará', -->
                    <!-- 'PB'=>'Paraíba', -->
                    <!-- 'PR'=>'Paraná', -->
                    <!-- 'PE'=>'Pernambuco', -->
                    <!-- 'PI'=>'Piauí', -->
                    <!-- 'RJ'=>'Rio de Janeiro', -->
                    <!-- 'RN'=>'Rio Grande do Norte', -->
                    <!-- 'RS'=>'Rio Grande do Sul', -->
                    <!-- 'RO'=>'Rondônia', -->
                    <!-- 'RR'=>'Roraima', -->
                    <!-- 'SC'=>'Santa Catarina', -->
                    <!-- 'SP'=>'São Paulo', -->
                    <!-- 'SE'=>'Sergipe', -->
                    <!-- 'TO'=>'Tocantins' -->

                    <div class="control-group">
                        <label for="estado" class="control-label">Estado<span class="required">*</span></label>
                        <div class="controls">
                            <select id="estado" type="text" name="estado">
                                <option value="Acre"                <?php echo set_value('estado') == 'Acre' ?                'selected' : ''; ?>>Acre</option>
                                <option value="Alagoas"             <?php echo set_value('estado') == 'Alagoas' ?             'selected' : ''; ?>>Alagoas</option>
                                <option value="Amapá"               <?php echo set_value('estado') == 'Amapá' ?               'selected' : ''; ?>>Amapá</option>
                                <option value="Amazonas"            <?php echo set_value('estado') == 'Amazonas' ?            'selected' : ''; ?>>Amazonas</option>
                                <option value="Bahia"               <?php echo set_value('estado') == 'Bahia' ?               'selected' : ''; ?>>Bahia</option>
                                <option value="Ceará"               <?php echo set_value('estado') == 'Ceará' ?               'selected' : ''; ?>>Ceará</option>
                                <option value="Distrito Federal"    <?php echo set_value('estado') == 'Distrito Federal' ?    'selected' : ''; ?>>Distrito Federal</option>
                                <option value="Espírito Santo"      <?php echo set_value('estado') == 'Espírito Santo' ?      'selected' : ''; ?>>Espírito Santo</option>
                                <option value="Goiás"               <?php echo set_value('estado') == 'Goiás' ?               'selected' : ''; ?>>Goiás</option>
                                <option value="Maranhão"            <?php echo set_value('estado') == 'Maranhão' ?            'selected' : ''; ?>>Maranhão</option>
                                <option value="Mato Grosso"         <?php echo set_value('estado') == 'Mato Grosso' ?         'selected' : ''; ?>>Mato Grosso</option>
                                <option value="Mato Grosso do Sul"  <?php echo set_value('estado') == 'Mato Grosso do Sul' ?  'selected' : ''; ?>>Mato Grosso do Sul</option>
                                <option value="Minas Gerais"        <?php echo set_value('estado') == 'Minas Gerais' ?        'selected' : ''; ?>>Minas Gerais</option>
                                <option value="Pará"                <?php echo set_value('estado') == 'Pará' ?                'selected' : ''; ?>>Pará</option>
                                <option value="Paraíba"             <?php echo set_value('estado') == 'Paraíba' ?             'selected' : ''; ?>>Paraíba</option>
                                <option value="Paraná"              <?php echo set_value('estado') == 'Paraná' ?              'selected' : ''; ?>>Paraná</option>
                                <option value="Pernambuco"          <?php echo set_value('estado') == 'Pernambuco' ?          'selected' : ''; ?>>Pernambuco</option>
                                <option value="Piauí"               <?php echo set_value('estado') == 'Piauí' ?               'selected' : ''; ?>>Piauí</option>
                                <option value="Rio de Janeiro"      <?php echo set_value('estado') == 'Rio de Janeiro' ?      'selected' : ''; ?>>Rio de Janeiro</option>
                                <option value="Rio Grande do Norte" <?php echo set_value('estado') == 'Rio Grande do Norte' ? 'selected' : ''; ?>>Rio Grande do Norte</option>
                                <option value="Rio Grande do Sul"   <?php echo set_value('estado') == 'Rio Grande do Sul' ?   'selected' : ''; ?>>Rio Grande do Sul</option>
                                <option value="Rondônia"            <?php echo set_value('estado') == 'Rondônia' ?            'selected' : ''; ?>>Rondônia</option>
                                <option value="Roraima"             <?php echo set_value('estado') == 'Roraima' ?             'selected' : ''; ?>>Roraima</option>
                                <option value="Santa Catarina"      <?php echo set_value('estado') == 'Santa Catarina' ?      'selected' : ''; ?>>Santa Catarina</option>
                                <option value="São Paulo"           <?php echo set_value('estado') == 'São Paulo' ?           'selected' : ''; ?>>São Paulo</option>
                                <option value="Sergipe"             <?php echo set_value('estado') == 'Sergipe' ?             'selected' : ''; ?>>Sergipe</option>
                                <option value="Tocantins"           <?php echo set_value('estado') == 'Tocantins' ?           'selected' : ''; ?>>Tocantins</option>
                            </select>
                        </div> 
                    </div>

                    <div class="control-group">
                        <label for="email" class="control-label">Email<span class="required">*</span></label>
                        <div class="controls">
                            <input id="email" type="text" name="email" value="<?php echo set_value('email'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="senha" class="control-label">Senha<span class="required">*</span></label>
                        <div class="controls">
                            <input id="senha" type="password" name="senha" value="<?php echo set_value('senha'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="telefone" class="control-label">Telefone<span class="required">*</span></label>
                        <div class="controls">
                            <input id="telefone" type="text" name="telefone" value="<?php echo set_value('telefone'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="celular" class="control-label">Celular</label>
                        <div class="controls">
                            <input id="celular" type="text" name="celular" value="<?php echo set_value('celular'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">

                        <label for="numslots" class="control-label" style="line-height: 325px;">
                            Numero de Slots<span class="required" >*</span>
                        </label>

                        <div class="controls">
                            <div class="span12" style="padding-bottom: 10px; margin-left: 0">
                                <div class="span4" >

                                    <!-- <input id="slotsp" type="hidden" name="slotsp"  value="0"  /> -->
                                    <label for="slotsm">Pequenos <small>(para celulares)</small><span class="required">*</span></label>
                                    <input id="slotsp" type="number" min="0" name="slotsp"  value="<?php echo set_value('slotsp'); ?>"  />
                                    <input id="nivel" type="hidden" name="nivel"  value="0"  />
                                    
                                    <label for="slotsm">Médios <small>(para Notebooks, Tablets e Video Games)</small><span class="required">*</span></label>
                                    <input id="slotsm" type="number" min="0" name="slotsm"  value="<?php echo set_value('slotsm'); ?>"  />
                            
                                    <label for="slotsg">Grandes <small>(para Tvs, Computadores (PCs))</small><span class="required">*</span></label>
                                    <input id="slotsg" type="number" min="0" name="slotsg"  value="<?php echo set_value('slotsg'); ?>"  />
                                    
                                    <!-- <label for="fileiras_slots">Qtd. por Fileira<span class="required">*</span></label> -->
                                    <!-- <input id="fileiras_slots" type="number" min="0" name="fileiras_slots" value="<?php //echo set_value('fileiras_slots'); ?>" /> -->
                                    <input id="fileiras_slots" type="hidden" min="0" name="fileiras_slots" value="0" /> -->

                                    <label for="qtd_fileiras_slots_p">Qtd. de Fileiras para os Slots Pequenos <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_p" type="number" min="0" name="qtd_fileiras_slots_p"  value="<?php echo set_value('qtd_fileiras_slots_p'); ?>" />

                                    <label for="qtd_fileiras_slots_m">Qtd. de Fileiras para os Slots Médios <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_m" type="number" min="0" name="qtd_fileiras_slots_m"  value="<?php echo set_value('qtd_fileiras_slots_m'); ?>" />

                                    <label for="qtd_fileiras_slots_g">Qtd. de Fileiras para os Slots Grandes <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_g" type="number" min="0" name="qtd_fileiras_slots_g"  value="<?php echo set_value('qtd_fileiras_slots_g'); ?>" />

                                </div>
                            </div>
                    	</div>
                    </div>

                    <div class="control-group">
                        <label  class="control-label">Situação<span class="required">*</span></label>
                        <div class="controls">
                            <select name="situacao" id="situacao">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label  class="control-label">Orçamentos Site:</label>
                        <div class="controls">
                            <select name="statusOrcamento" id="statusOrcamento">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label  class="control-label">Informar link de Avaliação do Gooogle:</label>
                        <div class="controls">
                             <input id="linkgoogle" type="text" name="linkgoogle" value="<?php echo set_value('linkgoogle'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group"  style="margin-bottom: 10px;">
                        <label  class="control-label">Permissões<span class="required">*</span></label>
                        <div class="controls">
                            <select name="permissoes_id" id="permissoes_id">
                                <?php foreach ($permissoes as $p): ?>
                                    <option value="<?= $p->idPermissao ?>"><?= $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
             	</div>       

				<div class="widget-title">
                	<span class="icon">
                    	<i class="icon-user"></i>
                	</span>
                	<h5>Permissões de Equipamento - <font style="color: #F66;"> Marque os equipamentos que a franquia poderá usar. </font></h5>
            	</div>

            	<div class="widget-content nopadding">
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Celular </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkCelular" id="checkCelular" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tablet </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkTablet" id="checkTablet" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Videogame </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkVideogame" id="checkVideogame" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Notebook </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkNotebook" id="checkNotebook" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> TV </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkTv" id="checkTv" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Computador </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        	<input type="checkbox" name="checkComputador" id="checkComputador" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> IMAC </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkimac" id="checkimac" value="1"><br>
                        </div>
                    </div>

                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> MAC </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkmac" id="checkmac" value="1"><br>
                        </div>
                    </div>

                </div>

                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-user"></i>
                    </span>
                    <h5>Plano/Pagamento</h5>
                </div>

                <div class="widget-content nopadding">

                    <div class="control-group">
                        <label for="plano" class="control-label">Plano 1<span class="required">*</span></label>
                        <div class="controls">
                            <select id="plano" name="plano">
                                <option value="">Selecione</option>
                                
                                <?php if(!empty($planos)) { ?>
                                    <?php foreach ($planos as $p) { ?>
                                    <option value="<?php echo $p->idPlanos; ?>"><?php echo $p->nome; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="plano2" class="control-label">Plano 2<span class="required">*</span></label>
                        <div class="controls">
                            <select id="plano2" name="plano2">
                                <option value="">Selecione</option>
                                
                                <?php if(!empty($planos)) { ?>
                                    <?php foreach ($planos as $p) { ?>
                                    <option value="<?php echo $p->idPlanos; ?>"><?php echo $p->nome; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome_cartao" class="control-label">Nome do titular<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome_cartao" type="text" name="nome_cartao">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cpf_cartao" class="control-label">CPF do titular<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cpf_cartao" type="text" name="cpf_cartao">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="num_cartao" class="control-label">Número do cartão<span class="required">*</span></label>
                        <div class="controls">
                            <input id="num_cartao" type="text" name="num_cartao">
                            <input type="hidden" name="cartao_bandeira">
                            <input type="hidden" name="token_pagamento">
                            <ul class="col-sm-12 form-group card-list">
                                <li class="visa"><img src="<?php echo base_url();?>assets/img/cards/visa.jpg" alt="visa"></li>
                                <li class="mastercard"><img src="<?php echo base_url();?>assets/img/cards/mastercard.jpg" alt="mastercard"></li>
                                <li class="diners"><img src="<?php echo base_url();?>assets/img/cards/diners.jpg" alt="diners"></li>
                                <li class="AMERICAN_EXPRESS"><img src="<?php echo base_url();?>assets/img/cards/american.jpg" alt="american"></li>
                                <li class="elo"><img src="<?php echo base_url();?>assets/img/cards/elo.jpg" alt="elo"></li>
                                <li class="aura"><img src="<?php echo base_url();?>assets/img/cards/aura.jpg" alt="aura"></li>
                                <li class="hipercard"><img src="<?php echo base_url();?>assets/img/cards/hipercard.jpg" alt="hipercard"></li>
                            </ul>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="mes_cartao" class="control-label">Mês<span class="required">*</span></label>
                        <div class="controls">
                            <input id="mes_cartao" type="number" name="mes_cartao">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="ano_cartao" class="control-label">Ano<span class="required">*</span></label>
                        <div class="controls">
                            <input id="ano_cartao" type="number" name="ano_cartao">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cvv_cartao" class="control-label">CVV - Código validador<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cvv_cartao" type="number" name="cvv_cartao">
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/franquias" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </form>
        </div>
    </div>
</div>
<!-- script do gerencianet.com -->
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://sandbox.gerencianet.com.br/v1/cdn/1a02e21dd07c5eae79ac5863a0ff22da/'+v;s.async=false;s.id='1a02e21dd07c5eae79ac5863a0ff22da';if(!document.getElementById('1a02e21dd07c5eae79ac5863a0ff22da')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<!-- ------------------------ -->

<script  src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
            $('input[name="num_cartao"]').on('blur', function(){
                var card = detect_card_type($(this).val());
                $('input[name="cartao_bandeira"]').val(card);
                $('.card-list').find('li').addClass('unselected').removeClass('active');
                if(card != undefined) 
                    $('.card-list').find('li.'+card).removeClass('unselected').addClass('active');
            });

           $('#formFranquia').validate({
            rules : {
                  nome:{ required: true},
                  rg:{ required: true},
                  cpf:{ required: true},
                  cep:{ required: true},
                  telefone:{ required: true},
                  email:{ required: true},
                  senha:{ required: true},
                  rua:{ required: true},
                  numero:{ required: true},
                  bairro:{ required: true},
                  cidade:{ required: true},
                  estado:{ required: true},
                  //cep:{ required: true}
                  //slotsm:{ required: true},
                  //slotsg:{ required: true}
            },
            messages: {
                  nome :{ required: 'Campo Requerido.'},
                  rg:{ required: 'Campo Requerido.'},
                  cpf:{ required: 'Campo Requerido.'},
                  cep:{ required: 'Campo Requerido.'},
                  telefone:{ required: 'Campo Requerido.'},
                  email:{ required: 'Campo Requerido.'},
                  senha:{ required: 'Campo Requerido.'},
                  rua:{ required: 'Campo Requerido.'},
                  numero:{ required: 'Campo Requerido.'},
                  bairro:{ required: 'Campo Requerido.'},
                  cidade:{ required: 'Campo Requerido.'},
                  estado:{ required: 'Campo Requerido.'},
                  //cep:{ required: 'Campo Requerido.'}
                  //slotsm:{ required: 'Campo Requerido.'},
                  //slotsg:{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
           
        $gn.ready(function(checkout) {
            verify_cartao(checkout);
            
            $('input[name="num_cartao"], input[name="mes_cartao"], input[name="ano_cartao"], input[name="cvv_cartao"]').on('blur', function(){
                verify_cartao(checkout);
            });
        });

        function verify_cartao(checkout) {
            var num_cartao = $('input[name="num_cartao"]').val();
            var mes_cartao = $('input[name="mes_cartao"]').val();
            var ano_cartao = $('input[name="ano_cartao"]').val();
            var cvv_cartao = $('input[name="cvv_cartao"]').val();
            var bandeira = detect_card_type(num_cartao);

            if(num_cartao != '' && mes_cartao != '' && ano_cartao != '' && cvv_cartao != ''){


              var callback = function(error, response) {
                if(error) {
                          // Trata o erro ocorrido
                          console.error(error);
                      } else {
                          // Trata a resposta
                          token = response.data.payment_token;
                          $('input[name="token_pagamento"]').val(token);
                      }
                  };

                  checkout.getPaymentToken({
                        brand: bandeira, // bandeira do cartão
                        number: num_cartao, // número do cartão
                        cvv: cvv_cartao, // código de segurança
                        expiration_month: mes_cartao, // mês de vencimento
                        expiration_year: ano_cartao // ano de vencimento
                    }, callback);

              }
        }

        function detect_card_type(number) {
            var re = {
                visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                mastercard: /^5[1-5][0-9]{14}$/,
                amex: /^3[47][0-9]{13}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                elo: /^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/,
                jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
                hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})$/,
                aura: /^50[0-9]{17}$/
            };
            if (re.visa.test(number)) {
                return 'visa';
            } else if (re.mastercard.test(number)) {
                return 'mastercard';
            } else if (re.amex.test(number)) {
                return 'amex';
            } else if (re.diners.test(number)) {
                return 'diners';
            } else if (re.elo.test(number)) {
                return 'elo';
            } else if (re.hipercard.test(number)) {
                return 'hipercard';
            } else if (re.aura.test(number)) {
                return 'aura';
            } else if (re.discover.test(number)) {
                return 'discover';
            }else if (re.jcb.test(number)) {
                return 'jcb';
            } else {
                return undefined;
            }
        }
      });
</script>
