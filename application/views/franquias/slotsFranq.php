<?php 
//echo "Area de Slots franquia"; 
//$CI = &get_instance(); 
//$CI->load->model('os_model');
//    echo "ID franquia Logada: ".$this->session->userdata('id');
//    //echo "Get dados da franquia URL: ".$_GET['id'];
//        $ddfranq = $CI->franquias_model->editarSlots();
//        echo $ddfranq['msg'];

?>
<div class="row-fluid" style="margin-top:0">

    <div class="span12">

        <?php if ($custom_error != ''): ?>
            <div class="alert alert-danger"><?php echo $custom_error ?></div>
        <?php endif; ?>

        <div class="widget-box">

            <div class="widget-title">
            
                <span class="icon">
                    <i class="icon-user"></i>
                </span>

                <h5>Editar Franquia</h5>
 
            </div>

            <div class="widget-content nopadding">
                
                <form action="<?php echo current_url(); ?>" id="formSlots" method="post" class="form-horizontal" >
                  
                    <div class="control-group" style="">
                        <?php echo $dados = form_hidden('idFranquias',$result->idFranquias) ?>

                        <label for="numslots" class="control-label" style="line-height: 325px;">
                                Número de Slots<span class="required" >*</span>
                        </label>
                        
                        <div class="controls">
                            <div class="span12" style="padding-bottom: 10px; margin-left: 0">
                                <div class="span4"> 
                                    <p class="alert alert-warning">Desculpe! No momento você não poderá realizar edição dos dados abaixo, favor contate o suporte.</p>
                                    <!-- <input id="slotsp" type="number" name="slotsp"  value="0" /> -->
                                    <label for="slotsm">Pequenos <small>(para celulares)</small><span class="required">*</span></label>
                                    <input id="slotsp" type="number" min="0" name="slotsp"  value="<?php echo $result->slotsp; ?>"  disabled/>
                                                                                                        
                                    <label for="slotsm">Médios <small>(para Notebooks, Tablets e Video Games)</small><span class="required">*</span></label>
                                    <input id="slotsm" type="number" min="0" name="slotsm"  value="<?php echo $result->slotsm; ?>"  disabled/>
                        
                                    <label for="slotsg">Grandes <small>(para Tvs, Computadores (PCs))</small><span class="required">*</span></label>
                                    <input id="slotsg" type="number" min="0" name="slotsg"  value="<?php echo $result->slotsg; ?>"  disabled/>

                                    <!-- <label for="fileiras_slots">Qtd. por Fileira<span class="required">*</span></label>-->
                                    <!-- <input id="fileiras_slots" type="number" min="0" name="fileiras_slots"  value="<?php //echo $result->fileiras_slots; ?>"  />-->

                                    <label for="qtd_fileiras_slots_p">Qtd. de Fileiras para os Slots Pequenos <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_p" type="number" min="0" name="qtd_fileiras_slots_p"  value="<?php echo $result->qtd_fileiras_slots_p; ?>"  disabled/>

                                    <label for="qtd_fileiras_slots_m">Qtd. de Fileiras para os Slots Médios <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_m" type="number" min="0" name="qtd_fileiras_slots_m"  value="<?php echo $result->qtd_fileiras_slots_m; ?>"  disabled/>

                                    <label for="qtd_fileiras_slots_g">Qtd. de Fileiras para os Slots Grandes <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_g" type="number" min="0" name="qtd_fileiras_slots_g"  value="<?php echo $result->qtd_fileiras_slots_g; ?>"  disabled/>

                                </div>
                            </div>
                        </div>
                    </div>

                   
                    
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <!-- <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Salvar</button> -->
                                <a href="<?php echo base_url() ?>index.php/franquias" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

