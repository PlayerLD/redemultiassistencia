<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">

            <div class="widget-title">
            
                <span class="icon">
                    <i class="icon-user"></i>
                </span>

                <h5>Editar Franquia</h5>

            </div> 

            <div class="widget-content nopadding">

                <?php //if ($custom_error != ''): ?>
                <!-- <div class="alert alert-danger"><?php //echo $custom_error ?></div> -->
                <?php //endif; ?>

                <form action="<?php echo current_url(); ?>" id="formFranquia" method="post" class="form-horizontal" >
                    <div class="control-group">
                        <?php echo form_hidden('idFranquias',$result->idFranquias) ?>
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo $result->nome; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="rg" class="control-label">RG<span class="required">*</span></label>
                        <div class="controls">
                            <input id="rg" type="text" name="rg" value="<?php echo $result->rg; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cpf" class="control-label">CNPJ<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cpf" type="text" name="cpf" value="<?php echo $result->cpf; ?>"  />
                        </div>
                    </div>

                    <div class="control-group"> 
                        <label for="cep" class="control-label">CEP<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cep" type="text" name="cep" value="<?php echo $result->cep; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="rua" class="control-label">Rua<span class="required">*</span></label>
                        <div class="controls">
                            <input id="rua" type="text" name="rua" value="<?php echo $result->rua; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero" class="control-label">Numero<span class="required">*</span></label>
                        <div class="controls">
                            <input id="numero" type="text" name="numero" value="<?php echo $result->numero; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="bairro" class="control-label">Bairro<span class="required">*</span></label>
                        <div class="controls">
                            <input id="bairro" type="text" name="bairro" value="<?php echo $result->bairro; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cidade" class="control-label">Cidade<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cidade" type="text" name="cidade" value="<?php echo $result->cidade; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="estado" class="control-label">Estado<span class="required">*</span></label>
                        <div class="controls">
                            <select id="estado" type="text" name="estado">
                                <option value="Acre"                <?php echo $result->estado == 'Acre' ?                'selected' : ''; ?>>Acre</option>
                                <option value="Alagoas"             <?php echo $result->estado == 'Alagoas' ?             'selected' : ''; ?>>Alagoas</option>
                                <option value="Amapá"               <?php echo $result->estado == 'Amapá' ?               'selected' : ''; ?>>Amapá</option>
                                <option value="Amazonas"            <?php echo $result->estado == 'Amazonas' ?            'selected' : ''; ?>>Amazonas</option>
                                <option value="Bahia"               <?php echo $result->estado == 'Bahia' ?               'selected' : ''; ?>>Bahia</option>
                                <option value="Ceará"               <?php echo $result->estado == 'Ceará' ?               'selected' : ''; ?>>Ceará</option>
                                <option value="Distrito Federal"    <?php echo $result->estado == 'Distrito Federal' ?    'selected' : ''; ?>>Distrito Federal</option>
                                <option value="Espírito Santo"      <?php echo $result->estado == 'Espírito Santo' ?      'selected' : ''; ?>>Espírito Santo</option>
                                <option value="Goiás"               <?php echo $result->estado == 'Goiás' ?               'selected' : ''; ?>>Goiás</option>
                                <option value="Maranhão"            <?php echo $result->estado == 'Maranhão' ?            'selected' : ''; ?>>Maranhão</option>
                                <option value="Mato Grosso"         <?php echo $result->estado == 'Mato Grosso' ?         'selected' : ''; ?>>Mato Grosso</option>
                                <option value="Mato Grosso do Sul"  <?php echo $result->estado == 'Mato Grosso do Sul' ?  'selected' : ''; ?>>Mato Grosso do Sul</option>
                                <option value="Minas Gerais"        <?php echo $result->estado == 'Minas Gerais' ?        'selected' : ''; ?>>Minas Gerais</option>
                                <option value="Pará"                <?php echo $result->estado == 'Pará' ?                'selected' : ''; ?>>Pará</option>
                                <option value="Paraíba"             <?php echo $result->estado == 'Paraíba' ?             'selected' : ''; ?>>Paraíba</option>
                                <option value="Paraná"              <?php echo $result->estado == 'Paraná' ?              'selected' : ''; ?>>Paraná</option>
                                <option value="Pernambuco"          <?php echo $result->estado == 'Pernambuco' ?          'selected' : ''; ?>>Pernambuco</option>
                                <option value="Piauí"               <?php echo $result->estado == 'Piauí' ?               'selected' : ''; ?>>Piauí</option>
                                <option value="Rio de Janeiro"      <?php echo $result->estado == 'Rio de Janeiro' ?      'selected' : ''; ?>>Rio de Janeiro</option>
                                <option value="Rio Grande do Norte" <?php echo $result->estado == 'Rio Grande do Norte' ? 'selected' : ''; ?>>Rio Grande do Norte</option>
                                <option value="Rio Grande do Sul"   <?php echo $result->estado == 'Rio Grande do Sul' ?   'selected' : ''; ?>>Rio Grande do Sul</option>
                                <option value="Rondônia"            <?php echo $result->estado == 'Rondônia' ?            'selected' : ''; ?>>Rondônia</option>
                                <option value="Roraima"             <?php echo $result->estado == 'Roraima' ?             'selected' : ''; ?>>Roraima</option>
                                <option value="Santa Catarina"      <?php echo $result->estado == 'Santa Catarina' ?      'selected' : ''; ?>>Santa Catarina</option>
                                <option value="São Paulo"           <?php echo $result->estado == 'São Paulo' ?           'selected' : ''; ?>>São Paulo</option>
                                <option value="Sergipe"             <?php echo $result->estado == 'Sergipe' ?             'selected' : ''; ?>>Sergipe</option>
                                <option value="Tocantins"           <?php echo $result->estado == 'Tocantins' ?           'selected' : ''; ?>>Tocantins</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="email" class="control-label">Email<span class="required">*</span></label>
                        <div class="controls">
                            <input id="email" type="text" name="email" value="<?php echo $result->email; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="senha" class="control-label">Senha</label>
                        <div class="controls">
                            <input id="senha" type="password" name="senha" value=""  placeholder="Não preencha se não quiser alterar."  />
                            <i class="icon-exclamation-sign tip-top" title="Se não quiser alterar a senha, não preencha esse campo."></i>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="telefone" class="control-label">Telefone<span class="required">*</span></label>
                        <div class="controls">
                            <input id="telefone" type="text" name="telefone" value="<?php echo $result->telefone; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="celular" class="control-label">Celular</label>
                        <div class="controls">
                            <input id="celular" type="text" name="celular" value="<?php echo $result->celular; ?>"  />
                        </div>
                    </div>

                    <!-- <div class="control-group" style="">

                        <label for="numslots" class="control-label" style="line-height: 325px;">
                                Número de Slots<span class="required" >*</span>
                        </label>
                        
                        <div class="controls">
                            <div class="span12" style="padding-bottom: 10px; margin-left: 0">
                                <div class="span4"> 

                                   
                                    <label for="slotsm">Pequenos <small>(para celulares)</small><span class="required">*</span></label>
                                    <input id="slotsp" type="number" min="0" name="slotsp"  value="<?php //echo $result->slotsp; ?>"  />
                                                                    
                                    <label for="slotsm">Médios <small>(para Notebooks, Tablets e Video Games)</small><span class="required">*</span></label>
                                    <input id="slotsm" type="number" min="0" name="slotsm"  value="<?php //echo $result->slotsm; ?>"  />
                        
                                    <label for="slotsg">Grandes <small>(para Tvs, Computadores (PCs))</small><span class="required">*</span></label>
                                    <input id="slotsg" type="number" min="0" name="slotsg"  value="<?php //echo $result->slotsg; ?>"  />

                                    <label for="fileiras_slots">Qtd. por Fileira<span class="required">*</span></label>
                                    <input id="fileiras_slots" type="number" min="0" name="fileiras_slots"  value="<?php //echo $result->fileiras_slots; ?>"  />

                                    <label for="qtd_fileiras_slots_p">Qtd. de Fileiras para os Slots Pequenos <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_p" type="number" min="0" name="qtd_fileiras_slots_p"  value="<?php //echo $result->qtd_fileiras_slots_p; ?>"  />

                                    <label for="qtd_fileiras_slots_m">Qtd. de Fileiras para os Slots Médios <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_m" type="number" min="0" name="qtd_fileiras_slots_m"  value="<?php //echo $result->qtd_fileiras_slots_m; ?>"  />

                                    <label for="qtd_fileiras_slots_g">Qtd. de Fileiras para os Slots Grandes <span class="required">*</span></label>
                                    <input id="qtd_fileiras_slots_g" type="number" min="0" name="qtd_fileiras_slots_g"  value="<?php //echo $result->qtd_fileiras_slots_g; ?>"  />

                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="control-group">
                        <label  class="control-label">Situação*</label>
                        <div class="controls">
                            <select name="situacao" id="situacao">
                                <?php if($result->situacao == 1){$ativo = 'selected'; $inativo = '';} else{$ativo = ''; $inativo = 'selected';} ?>
                                <option value="1" <?php echo $ativo; ?>>Ativo</option>
                                <option value="0" <?php echo $inativo; ?>>Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Orçamentos Site:</label>
                        <div class="controls">
                            <select name="statusOrcamento" id="statusOrcamento">
                                <?php if($result->statusOrcamento == 1){$ativo = 'selected'; $inativo = '';} else{$ativo = ''; $inativo = 'selected';} ?>
                                <option value="1" <?php echo $ativo; ?>>Ativo</option>
                                <option value="0" <?php echo $inativo; ?>>Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Informar link de Avaliação do Gooogle:</label>
                        <div class="controls">
                             <input id="linkgoogle" type="text" name="linkgoogle" value="<?php echo $result->linkgoogle; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Permissões<span class="required">*</span></label>
                        <div class="controls">
                            <select name="permissoes_id" id="permissoes_id">
                                  <?php foreach ($permissoes as $p) {
                                     if($p->idPermissao == $result->permissoes_id){ $selected = 'selected';}else{$selected = '';}
                                      echo '<option value="'.$p->idPermissao.'"'.$selected.'>'.$p->nome.'</option>';
                                  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="widget-title">
                    <span class="icon">
                        <i class="icon-user"></i>
                    </span>
                    <h5>Permissões de Equipaento - <font style="color: #F66;"> Marque os equipamentos que a franquia poderá usar. </font></h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Celular </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkCelular" <?php echo in_array('Celular', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkCelular" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Tablet </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkTablet" <?php echo in_array('Tablet', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkTablet" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Videogame </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkVideogame" <?php echo in_array('Videogame', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkVideogame" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Notebook </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkNotebook" <?php echo in_array('Notebook', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkNotebook" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> TV </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkTv" <?php echo in_array('Tv', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkTv" value="1"><br>
                        </div>
                    </div>
                    
                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Computador </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkComputador" <?php echo in_array('Computador', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkComputador" value="1"><br>
                        </div>
                    </div>

                     <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> iMac </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkimac" <?php echo in_array('iMac', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkimac" value="1"><br>
                        </div>
                    </div>

                    <div class="control-group" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                        <label class="control-label" style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-size: 12px"> Mac </label>
                        <div class="controls" style="margin-top: 0px; margin-bottom: 0px; padding: 0px;">
                            <input type="checkbox" name="checkmac" <?php echo in_array('Mac', (array)@unserialize($result->permissoes_equip)) ? 'checked' : ''; ?> id="checkmac" value="1"><br>
                        </div>
                    </div>

                    <div class="widget-title">
                    <span class="icon">
                        <i class="icon-user"></i>
                    </span>
                    <h5>Plano/Pagamento</h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="control-group">
                        <label for="plano" class="control-label">Plano 1<span class="required">*</span></label>
                        <div class="controls">
                            <select id="plano" name="plano">
                                <option value="">Selecione</option>
                                
                                <?php if(!empty($planos)) { ?>
                                    <?php foreach ($planos as $p) { ?>
                                    <option value="<?php echo $p->idPlanos; ?>" <?php echo $result->id_plano == $p->idPlanos ? 'selected' : ''; ?>><?php echo $p->nome; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="plano2" class="control-label">Plano 2<span class="required">*</span></label>
                        <div class="controls">
                            <select id="plano2" name="plano2">
                                <option value="">Selecione</option>
                                
                                <?php if(!empty($planos)) { ?>
                                    <?php foreach ($planos as $p) { ?>
                                    <option value="<?php echo $p->idPlanos; ?>" <?php echo $result->id_plano2 == $p->idPlanos ? 'selected' : ''; ?>><?php echo $p->nome; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nome_cartao" class="control-label">Nome do titular<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome_cartao" type="text" name="nome_cartao" value="<?php echo $result->nome_cartao; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cpf_cartao" class="control-label">CPF do titular<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cpf_cartao" type="text" name="cpf_cartao" value="<?php echo $result->cpf_cartao; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="num_cartao" class="control-label">Número do cartão<span class="required">*</span></label>
                        <div class="controls">
                            <input id="num_cartao" type="text" name="num_cartao" value="<?php echo $result->num_cartao; ?>">
                            <input type="hidden" name="cartao_bandeira">
                            <input type="hidden" name="token_pagamento">
                            <ul class="col-sm-12 form-group card-list">
                                <li class="visa"><img src="<?php echo base_url();?>assets/img/cards/visa.jpg" alt="visa"></li>
                                <li class="mastercard"><img src="<?php echo base_url();?>assets/img/cards/mastercard.jpg" alt="mastercard"></li>
                                <li class="diners"><img src="<?php echo base_url();?>assets/img/cards/diners.jpg" alt="diners"></li>
                                <li class="AMERICAN_EXPRESS"><img src="<?php echo base_url();?>assets/img/cards/american.jpg" alt="american"></li>
                                <li class="elo"><img src="<?php echo base_url();?>assets/img/cards/elo.jpg" alt="elo"></li>
                                <li class="aura"><img src="<?php echo base_url();?>assets/img/cards/aura.jpg" alt="aura"></li>
                                <li class="hipercard"><img src="<?php echo base_url();?>assets/img/cards/hipercard.jpg" alt="hipercard"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="mes_cartao" class="control-label">Mês<span class="required">*</span></label>
                        <div class="controls">
                            <input id="mes_cartao" type="number" max="12" min="1" name="mes_cartao" value="<?php echo $result->mes_cartao; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="ano_cartao" class="control-label">Ano<span class="required">*</span></label>
                        <div class="controls">
                            <input id="ano_cartao" type="number" min="<?php echo date('y'); ?>" max="50" name="ano_cartao" value="<?php echo $result->ano_cartao; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cvv_cartao" class="control-label">CVV - Código validador<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cvv_cartao" type="number" name="cvv_cartao" value="<?php echo $result->cvv_cartao; ?>">
                        </div>
                    </div>
                </div>
                    
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Salvar</button>
                                <a href="<?php echo base_url() ?>index.php/franquias" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>

                <p class="widget-content nopadding text-right">
                    <div class="control-group">
                        <label for="celular" class="control-label">Área Slots</label>
                        <div class="controls">
                            <a href="<?php echo site_url('franquias/areaSlots/'.$result->idFranquias);?>" class="btn btn-warning" target="_blank">Editar Slots</a>
                            <small>Você será redirecinado para área de edição dos slotes.</small>
                        </div>
                    </div>
                </p>
                
            </div>
        </div>
    </div>
</div>
<!-- script do gerencianet.com -->
<script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://sandbox.gerencianet.com.br/v1/cdn/1a02e21dd07c5eae79ac5863a0ff22da/'+v;s.async=false;s.id='1a02e21dd07c5eae79ac5863a0ff22da';if(!document.getElementById('1a02e21dd07c5eae79ac5863a0ff22da')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
<!-- ------------------------ -->
<script  src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
            $('input[name="num_cartao"]').on('blur', function(){
                var card = detect_card_type($(this).val());
                $('input[name="cartao_bandeira"]').val(card);
                $('.card-list').find('li').addClass('unselected').removeClass('active');
                if(card != undefined) 
                    $('.card-list').find('li.'+card).removeClass('unselected').addClass('active');
            });
           $('#formFranquia').validate({
            rules : {
                  nome:{ required: true},
                  rg:{ required: true},
                  cpf:{ required: true},
                  cep:{ required: true},
                  telefone:{ required: true},
                  email:{ required: true},
                  rua:{ required: true},
                  numero:{ required: true},
                  bairro:{ required: true},
                  cidade:{ required: true},
                  estado:{ required: true},
                  cep:{ required: true}
            },
            messages: {
                  nome :{ required: 'Campo Requerido.'},
                  rg:{ required: 'Campo Requerido.'},
                  cpf:{ required: 'Campo Requerido.'},
                  cep:{ required: 'Campo Requerido.'},
                  telefone:{ required: 'Campo Requerido.'},
                  email:{ required: 'Campo Requerido.'},
                  rua:{ required: 'Campo Requerido.'},
                  numero:{ required: 'Campo Requerido.'},
                  bairro:{ required: 'Campo Requerido.'},
                  cidade:{ required: 'Campo Requerido.'},
                  estado:{ required: 'Campo Requerido.'},
                  cep:{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });

           $gn.ready(function(checkout) {
            verify_cartao(checkout);
            
            $('input[name="num_cartao"], input[name="mes_cartao"], input[name="ano_cartao"], input[name="cvv_cartao"]').on('blur', function(){
                verify_cartao(checkout);
            });
        });

        function verify_cartao(checkout) {
            var num_cartao = $('input[name="num_cartao"]').val();
            var mes_cartao = $('input[name="mes_cartao"]').val();
            var ano_cartao = $('input[name="ano_cartao"]').val();
            var cvv_cartao = $('input[name="cvv_cartao"]').val();
            var bandeira = detect_card_type(num_cartao);

            if(num_cartao != '' && mes_cartao != '' && ano_cartao != '' && cvv_cartao != ''){


              var callback = function(error, response) {
                if(error) {
                          // Trata o erro ocorrido
                          console.error(error);
                      } else {
                          // Trata a resposta
                          token = response.data.payment_token;
                          $('input[name="token_pagamento"]').val(token);
                      }
                  };

                  checkout.getPaymentToken({
                        brand: bandeira, // bandeira do cartão
                        number: num_cartao, // número do cartão
                        cvv: cvv_cartao, // código de segurança
                        expiration_month: mes_cartao, // mês de vencimento
                        expiration_year: ano_cartao // ano de vencimento
                    }, callback);

              }
        }

        function detect_card_type(number) {
            var re = {
                visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                mastercard: /^5[1-5][0-9]{14}$/,
                amex: /^3[47][0-9]{13}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                elo: /^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/,
                jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
                hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})$/,
                aura: /^50[0-9]{17}$/
            };
            if (re.visa.test(number)) {
                return 'visa';
            } else if (re.mastercard.test(number)) {
                return 'mastercard';
            } else if (re.amex.test(number)) {
                return 'amex';
            } else if (re.diners.test(number)) {
                return 'diners';
            } else if (re.elo.test(number)) {
                return 'elo';
            } else if (re.hipercard.test(number)) {
                return 'hipercard';
            } else if (re.aura.test(number)) {
                return 'aura';
            } else if (re.discover.test(number)) {
                return 'discover';
            }else if (re.jcb.test(number)) {
                return 'jcb';
            } else {
                return undefined;
            }
        }

      });
</script>
