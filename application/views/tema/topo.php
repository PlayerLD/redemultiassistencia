<?php $CI = &get_instance(); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head> 
<title>ASSISTÊNCIA TÉCNICA ONLINE</title>
<meta charset="UTF-8" /> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="<?php echo base_url();?>assets/img/logo-small.png" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/matrix-media.css" />
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/style_201808271232.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.css" />

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>

<style type="text/css">
    .popover-modal .popover-body{
        overflow: hidden;
        padding: 10px;
        text-align: left;
        line-height: 1.5;
        font-size: 12px;
    }
    .popover-question{
        padding: 0px 5px;
        background: #0088cc;
        color: white;
        border-radius: 50%;
        font-size: 11px;
        margin-left: 5px;
    }
</style>
 
</head>
<body>
 
<!--Header-part-->
<div id="header" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>">
  
  <h1>
    <a href="">Map OS</a>
  </h1>
</div>
<!--close-Header-part--> 
 
<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>">
  <ul class="nav">
    <li class="alert alert-warning alerta-orcamento" style="margin-right: 10px;display: none;"><div style="margin: 8px 0 0 0;color: #000;height: 30px;text-align: center;line-height: 30px;font-size: 15px;padding: 0 5px;font-weight: bold;"><a href="<?php echo base_url()?>index.php/os/orcamentos">Responder novos orçamentos</a></div></li>
    <?php $CI->load->model('os_model'); ?>
    <?php echo $CI->os_model->getNotaRespostas(); ?>
    <li class="alert alert-success">
      <div style="
      margin: 8px 0 0 0;
      color: #000;
      height: 30px;
      text-align: center;
      line-height: 30px;
      font-size: 15px;
      padding: 0 5px;"><i class="icon icon-info"></i>
        <?php echo 'Código: '.$this->session->userdata('id').' / N. Franquia: '.$this->session->userdata('nome'); ?>
      </div>
    </li>
   <li class=""><div style="margin: 4px 0 0 0;
    background-color: #0088cc;
    color: #FFF;
    height: 30px;
    width: 94%;
    text-align: center;
    line-height: 30px;
    font-size: 10px;
    border-radius: 10px;
    padding: 0 5px;
    font-weight: bold;">Versão 1.5.2</div></li>
    <li class=""><a title="" href="<?php echo base_url();?>index.php/sistemaos/minhaConta"><i class="icon icon-star"></i> <span class="text">Minha Conta</span></a></li>
    <li class=""><a title="" href="<?php echo base_url();?>index.php/sistemaos/sair"><i class="icon icon-share-alt"></i> <span class="text">Sair do Sistema</span></a></li>
  </ul>
</div>

<!--start-top-serch-->
<div id="search" style="display: none;">
  <form action="<?php echo base_url()?>index.php/sistemaos/pesquisar">
    <input type="text" name="termo" placeholder="Pesquisar..." style="height: 20px; float: none;"/>
    <button type="submit"  class="tip-bottom" title="Pesquisar"><i class="icon-search icon-white"></i></button>
  </form>
</div>
<!--close-top-serch--> 

<!--sidebar-menu-->

<div id="sidebar" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>"> <a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
  <ul>
  <?php //buscar todos os dados do user logado //print_r($this->session->all_userdata()); ?>

  <!-- Dashboard -->
        <li class="<?php if(isset($menuPainel)){echo 'active';};?>"><a href="<?php echo base_url()?>"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
    
  <!-- Clientes -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){ ?>
          <li class="<?php if(isset($menuClientes)){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/clientes"><i class="icon icon-group"></i> <span>Clientes</span></a></li><?php } ?>
          
  <!-- Avaliações -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vFeedback')){ ?>
          <li class="<?php if(isset($menuFeedbacks)){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/avaliacoes"><i class="icon icon-star"></i> <span>Avaliações</span></a></li><?php } ?>
    
  <!-- Filiais -->
        <?php //if($this->permission->checkFiliais($this->session->userdata('id'))){ ?>
          <!-- <li class="<?php if(isset($menuFornecedores)){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/filiais"><i class="icon-book"></i> <span>Filiais</span></a></li> -->
        <?php //} ?>

  <!-- Fornecedores -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vFornecedor')){ ?>
          <li class="<?php if(isset($menuFornecedores)){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/fornecedores"><i class="icon-truck"></i> <span>Fornecedores</span></a></li><?php } ?>
    
  <!-- Catálogo -->
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamento') || $this->permission->checkPermission($this->session->userdata('permissao'),'vProduto') || $this->permission->checkPermission($this->session->userdata('permissao'),'vServico') || $this->permission->checkPermission($this->session->userdata('permissao'),'vMarca')){ ?>
    <li class="submenu <?php if(isset($menuCatalogo)) { echo 'active open'; } ?>">
              <a href="#"><i class="icon icon-list"></i> <span>Catalogo</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
            <ul>
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){ ?>
                    <li class="<?php if(isset($menuCatalogo)) if($menuCatalogo == 'produtos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/produtos"><i class="icon icon-barcode"></i> <span>Produtos/Componentes</span></a></li><?php } ?>
                               
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){ ?>
                  <li class="<?php if(isset($menuCatalogo)) if($menuCatalogo == 'servicos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/servicos"><i class="icon icon-wrench"></i> <span>Serviços</span></a></li><?php } ?>
              
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamento')){ ?>
                  <li class="<?php if(isset($menuCatalogo)) if($menuCatalogo == 'equipamentos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/equipamentos"><i class="icon icon-mobile-phone"></i><span>Equipamentos</span></a></li><?php } ?>
              
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vMarca')){ ?>
                  <li class="<?php if(isset($menuCatalogo)) if($menuCatalogo == 'marcas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/marcas"><i class="icon icon-circle"></i> <span>Marcas</span></a></li><?php } ?>
  
                  
                  
              </ul>
          </li><?php } ?>
          
  <!-- Ordens de Serviço -->
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){ ?>
    <li class="submenu <?php if(isset($menuOs)){echo 'active open';};?>">
              <a href="#"><i class="icon icon-tags"></i> <span>Ordens de Serviço</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
              <ul>
                  <li class="<?php if(isset($menuOs)) if($menuOs == 'adicionar'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/adicionar"><i class="icon icon-plus"></i> Adicionar O.S</a></li>
                  <li class="<?php if(isset($menuOs)) if($menuOs == 'os'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os"><i class="icon icon-bookmark"></i> Controle de O.S</a></li>
                  <!--<li class="<?php if(isset($menuOs)) if($menuOs == 'pendentes'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/pendentes"><i class="icon icon-bookmark"></i> O.S Pendentes</a></li>
                  <li class="<?php if(isset($menuOs)) if($menuOs == 'finalizadas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/finalizadas"><i class="icon icon-suitcase"></i> O.S Finalizadas</a></li>-->
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'aberto'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/aberto"><i class="icon icon-suitcase"></i> O.S Abertas</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'orcamento'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/orcamento"><i class="icon icon-suitcase"></i> O.S de Orçamentos</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'emAndamento'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/emAndamento"><i class="icon icon-suitcase"></i> O.S Em Andamento</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'aprovado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/osAprovados"><i class="icon icon-suitcase"></i> O.S Aprovado</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'EntregueParaCliente'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/osEntregueParaCliente"><i class="icon icon-suitcase"></i> O.S Entregue para o Cliente</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'consertoNaoRealizado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/osConsertoNaoRealizado"><i class="icon icon-suitcase"></i> O.S Conserto Não Realizado</a></li>
                        
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'finalizado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/finalizado"><i class="icon icon-suitcase"></i> O.S Finalizadas</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'aguardandoAprovacao'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/aguardandoAprovacao"><i class="icon icon-suitcase"></i> O.S Aguardando Aprovação</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'cancelado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/cancelado"><i class="icon icon-suitcase"></i> O.S Canceladas</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'faturado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/faturado"><i class="icon icon-suitcase"></i> O.S Faturadas</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'aguardandoPecas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/aguardandoPecas"><i class="icon icon-suitcase"></i> O.S Aguardando Peças</a></li>
                        <li class="<?php if(isset($menuOs)) if($menuOs == 'naoAprovado'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/naoAprovado"><i class="icon icon-suitcase"></i> O.S Não Aprovadas</a></li>
                <li class="<?php if(isset($menuOs)) if($menuOs == 'slots'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/slots"><i class="icon icon-sitemap"></i> Localização</a></li>
                  <li class="<?php if(isset($menuOs)) if($menuOs == 'orcamento'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/os/orcamentos"><i class="icon icon-fire"></i> Orçamentos Site</a></li>
              
                  
                  
                  
                  
                  <li class=""><a href="<?php echo base_url()?>index.php/xml"><i class="icon icon-fire"></i> Arquivos XML / NFse</a></li>
                  <li class=""><a href="<?php echo base_url()?>index.php/danfe"><i class="icon icon-fire"></i> Arquivos PDF / Danfe</a></li>
              </ul>
          </li><?php } ?>

  <!-- Vendas -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){ ?>
         <li class="submenu <?php if(isset($menuVendas)){echo 'active open';};?>">
          <a href="#"><i class="icon icon-tags"></i> <span>Vendas</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
          <ul>
            <li class="<?php if(isset($menuVendas)){echo 'active';}?>"><a href="<?php echo base_url()?>index.php/vendas"><i class="icon icon-shopping-cart"></i> <span>Vendas</span></a></li>
            
          </ul>
        </li>
        <?php } ?>
    
  <!-- Arquivos -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vArquivo')){ ?>
          <li class="<?php if(isset($menuArquivos)){echo 'active';}?>"><a href="<?php echo base_url()?>index.php/arquivos"><i class="icon icon-hdd"></i> <span>Arquivos</span></a></li><?php } ?>                   
    
  <!-- Financeiro -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vDespesa')){ ?>
          <li class="submenu <?php if(isset($menuFinanceiro)){echo 'active open';};?>">
              <a href="#"><i class="icon icon-money"></i> <span>Financeiro</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
              <ul>
                  <li class="<?php if(isset($menuFinanceiro)) if($menuFinanceiro == 'despesas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/despesas">Lançamentos</a></li>
              </ul>
          </li><?php } ?>
    
  <!-- Fornecedores -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vPlanos')){ ?>
          <li class="<?php if(isset($menuPlanos)){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/planos"><i class="icon-credit-card"></i> <span>Planos</span></a></li>
        <?php } ?>

  <!-- Assistência Técnica -->
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vFranquia')){ ?>
          <li class="submenu <?php if(isset($menuFranquias)){echo 'active open';};?>">
              <a href="#"><i class="icon icon-wrench" aria-hidden="true"></i> <span>Franquias</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
              <ul>
                  <li class="<?php if(isset($menuFranquias)) if($menuFranquias == 'adicionar'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/franquias/adicionar"><i class="icon icon-plus"></i> Adicionar Franquia</a></li>
                  <li class="<?php if(isset($menuFranquias)) if($menuFranquias == 'franquias'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/franquias"><i class="icon icon-bookmark"></i> Controle de Franquia</a></li>
              </ul>
          </li><?php } ?>

  <!-- Relatórios -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor') || $this->permission->checkPermission($this->session->userdata('permissao'),'rCliente') || $this->permission->checkPermission($this->session->userdata('permissao'),'rProduto') || $this->permission->checkPermission($this->session->userdata('permissao'),'rServico') || $this->permission->checkPermission($this->session->userdata('permissao'),'rMarca') || $this->permission->checkPermission($this->session->userdata('permissao'),'rOs') || $this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro') || $this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){ ?>
          <li class="submenu <?php if(isset($menuRelatorios)){echo 'active open';};?>" >
              <a href="#"><i class="icon icon-list-alt"></i> <span>Relatórios</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
              <ul>
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'fornecedores'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/fornecedores">Fornecedores</a></li><?php } ?>

                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rCliente')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'clientes'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/clientes">Clientes</a></li><?php } ?>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'despesas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/despesas">Lançamentos</a></li>
                  <?php } ?>

                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'produtos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/produtos">Produtos</a></li><?php } ?>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rServico')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'servicos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/servicos">Serviços</a></li><?php } ?>
                    
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'marcas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/marcas">Marcas</a></li>
                  
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rOs')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'os'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/os">Ordens de Serviço</a></li><?php } ?>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'vendas'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/vendas">Vendas</a></li><?php } ?>
                    
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'orcamentos'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/orcamentos">Orçamentos</a></li>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro')){ ?>
                    <li class="<?php if(isset($menuRelatorios)) if($menuRelatorios == 'financeiro'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/relatorios/financeiro">Financeiro</a></li><?php } ?>
            
              </ul>
          </li><?php } ?>

      <!-- Configurações -->
        <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cUsuario')  || $this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente') || $this->permission->checkPermission($this->session->userdata('permissao'),'cPermissao') || $this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){ ?>
          <li class="submenu <?php if(isset($menuConfiguracoes)){echo 'active open';};?>">
              <a href="#"><i class="icon icon-cog"></i> <span>Configurações</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
              <ul>
                  <?php //if($this->permission->checkPermission($this->session->userdata('permissao'),'cAtendente')){ ?>
                    <li class="<?php if(isset($menuConfiguracoes)) if($menuConfiguracoes == 'atendentes'){echo 'active';};?>">
                      <a href="<?php echo base_url()?>index.php/atendentes">Atendente</a>
                    </li>
                  <?php //} ?>

                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cUsuario')){ ?>
                    <li class="<?php if(isset($menuConfiguracoes)) if($menuConfiguracoes == 'usuarios'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/usuarios">Técnico/Funcionário</a></li><?php } ?>
                    
                <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){ ?>
                    <li class="<?php if(isset($menuConfiguracoes)) if($menuConfiguracoes == 'emitente'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/sistemaos/emitente">Emitente</a></li><?php } ?>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cPermissao')){ ?>
                    <li class="<?php if(isset($menuConfiguracoes)) if($menuConfiguracoes == 'permissoes'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/permissoes">Permissões</a></li><?php } ?>
                    
                  <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){ ?>
                    <li class="<?php if(isset($menuConfiguracoes)) if($menuConfiguracoes == 'backup'){echo 'active';};?>"><a href="<?php echo base_url()?>index.php/sistemaos/backup">Backup</a></li><?php } ?>
              </ul>
          </li><?php } ?>
    </ul>
</div>
<div id="content" style="<?php echo @$iframe ? 'margin-left: 0;' : '' ; ?>">
  <div id="content-header">
    <div id="breadcrumb" style="<?php echo @$iframe ? 'display: none;' : '' ; ?>"> <a href="<?php echo base_url()?>" title="Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> <?php if($this->uri->segment(1) != null){?><a href="<?php echo base_url().'index.php/'.$this->uri->segment(1)?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1));?>"><?php echo ucfirst($this->uri->segment(1));?></a> <?php if($this->uri->segment(2) != null){?><a href="<?php echo base_url().'index.php/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3) ?>" class="current tip-bottom" title="<?php echo ucfirst($this->uri->segment(2)); ?>"><?php echo ucfirst($this->uri->segment(2));} ?></a> <?php }?></div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
          <?php if($this->session->flashdata('error') != null && !is_array($this->session->flashdata('error'))){?>
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <?php echo $this->session->flashdata('error');?>
               </div>
          <?php }?>

          <?php if(is_array($this->session->flashdata('error'))){?>
            <?php foreach($this->session->flashdata('error') as $v){?>
                  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $v;?>
                 </div>
            <?php }?>
          <?php }?>

                      <?php if($this->session->flashdata('success') != null && !is_array($this->session->flashdata('success'))){?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <?php echo $this->session->flashdata('success');?>
                           </div>
                      <?php }?>

                      <?php if(is_array($this->session->flashdata('success'))){?>
                        <?php foreach($this->session->flashdata('success') as $v){?>
                              <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <?php echo $v;?>
                             </div>
                        <?php }?>
                      <?php }?>
                          
                      <?php if(isset($view)){echo $this->load->view($view);}?>


      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> <?php echo date('Y'); ?> &copy; ASSISTÊNCIA TÉCNICA ONLINE</div>
</div>
<!--end-Footer-part-->

<style type="text/css">
  div.escanear_cod {
    max-width: 280px;
    border: 1px solid #ccc;
    padding: 20px;
    font-size: 80px;
    margin: 0 auto;
    border-radius: 10px;
  }

  div#modal-codigo-barras td{padding: 4px 10px;}
  div#modal-codigo-barras input, div#modal-codigo-barras table{margin: 0;}
  div#modal-codigo-barras .modal-body{padding-bottom: 0;}

  div.escanear_cod h2 {
    font-size: 16px;
    line-height: 1;
  }
</style>
<div id="modal-codigo-barras" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1001 !important;">
  <form action="<?php echo base_url() ?>index.php/codigo_barras/index" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Escanear código de barras</h5>
    </div>
    <div class="modal-body">
      <div class="div-scan">
        <input type="hidden" name="codigo_barras" id="codigo_barras">
        <div class="text-center escanear_cod">
          <i class="icon-barcode"></i><br>
          <h2>Passe o leitor sobre o código de barras</h2>
        </div>
      </div>
      <div class="div-wrapper hidden"></div>
    </div>
  </form>
</div>

<div id="modal-inserir-componente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/inserirProdutoAjax" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inserir Produto</h5>
        </div>
        <div class="modal-body">
            <?php $idFranquia = $this->session->userdata('id');?>
            <input id="idFranquia" type="hidden" class="span12" name="idFranquia" value="<?php echo $idFranquia; ?>" />
            <input id="campo_codigo_barras" type="hidden" name="campo_codigo_barras"/>
            <input id="codigo_produto" type="text" name="codigo_produto"  placeholder="Código do produto"/>
            <input id="descricao" type="text" name="descricao"  placeholder="Descrição"/>
            <input id="peso" type="text" name="peso" value=""  placeholder="Peso" />
            <?php if(!empty($taxas)){ ?>
            <select name="impostos" id="impostos" value="">
              <option value="">Selecione</option>
              <?php foreach($taxas as $v){ ?>
              <option value="<?php echo $v->idTaxas ?>"><?php echo $v->titulo; ?>@<?php echo $v->valor; ?>%</option>
              <?php } ?>
            </select>
            <?php } ?>
            <input id="unidade" type="text" name="unidade"  value="UND" placeholder="Unidade"/>
            <input id="componenteMarca" type="text" name="marca" value=""  placeholder="Marca"/>
            <input id="componenteMarcas_id" type="hidden" class="span12" name="marcas_id" value=""/>
            <input id="componenteFornecedor" type="text" name="fornecedor" value=""  placeholder="Fornecedor"/>
            <input id="componenteFornecedor_id" type="hidden" class="span12" name="fornecedor_id" value="" />
            <input id="ncm" type="text" name="ncm" value=""  placeholder="NCM"/>
            <input id="cfop" type="text" name="cfop" value=""  placeholder="CFOP"/>
            <input id="precoCompra" class="money" type="text" name="precoCompra"  placeholder="Preço de Compra"/>
            <input id="precoVenda" class="money" type="text" name="precoVenda"  placeholder="Preço de Venda"/>
            <?php if($idFranquia == 4){ ?>
            <input id="preco_sugerido" class="money" type="text" name="preco_sugerido"  placeholder="Preço sugerido"/>
            <?php } ?>
            <input id="estoque" type="text" name="estoque"  placeholder="Estoque"/>
            <input id="estoque_ideal" type="text" name="estoque_ideal"  placeholder="Estoque Ideal"/>
            <input id="estoqueMinimo" type="text" name="estoqueMinimo"  placeholder="Estoque Mínimo"/>
        </div>
        <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
         <button class="btn btn-primary" id="btnInserirProdutoAjax">Inserir</button>
     </div>
 </form>
</div>

<!-- Modal -->
<div id="modal-excluir-produto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
  <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Excluir Produto</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idProduto" name="id" value="" />
      <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Excluir</button>
    </div>
  </form>
</div>

<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-popover/dist/jquery-popover-0.0.3.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script src="<?php echo base_url();?>js/mask.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/matrix.js"></script> 

<script>
  $(document).ready(function(){
    $('[data-role="popover"]').popover();
    $(".money").maskMoney(); 
    $('input[name*=cep]').mask('00000-000');
    $('input[name=peso]').mask("##0.00", {reverse: true});
    $('input[name*=cpf]').mask('000.000.000-00');
    $('input[name*=tel]').mask('(00) 0000-0000');
    $('input[name*=cel]').mask('(00) 00000-0000');
    $('input[name*=hora]').mask('00:00');

    scanCodigo('input#codigo_barras');
    verificarOrcamentos();

    $("#componenteMarca").autocomplete({
            source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
            minLength: 1,
            select: function( event, ui ) {
                $("#componenteMarcas_id").val(ui.item.id);
            }
        });

    $("#componenteFornecedor").autocomplete({
            source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
            minLength: 1,
            select: function( event, ui ) {
                $("#componenteFornecedor_id").val(ui.item.id);
            }
        });

    $(document).on('click', '#btnInserirProdutoAjax', function(event) {
          var preenchido = true;
          $(this).closest("form").find("input[type=text]").each(function(){
             if($(this).val() == ""){
                switch($(this).attr('name')){
                  case "descricao":
                  case "impostos":
                  case "unidade":
                  case "componenteMarca":
                  case "componenteMarcas_id":
                  case "ncm":
                  case "cfop":
                  case "precoCompra":
                  case "precoVenda":
                  case "estoque":
                  case "estoque_ideal":
                  case "estoqueMinimo":
                  preenchido = false;
                  return false;
                  break;
                }
            }
        });
          if(!preenchido){
             alert("Informe todos os campos do produto pra inserir!");
         }else{
             var dados = $(this).closest("form").serialize();
             $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/produtos/inserirProdutoAjax",
                data: dados,
                dataType: 'json',
                success: function(data){
                   if(data.result == true){
                      $('#modal-inserir-componente').modal('hide');
                      $("#produto").val(data.nomeProduto);
                      $("#idProduto").val(data.idProduto);
                      $("#preco").val(data.preco);
                      $("#quantidade").focus();

                      if(window.location.href.indexOf('produtos') != -1){
                        window.location.href = "<?php echo base_url() ?>index.php/produtos";
                      }
                  }else{
                      alert("Ocorreu um erro ao tentar adicionar um produto!");
                  }
              },
              error: function(){
               alert("ERRO!");
           }
       });
         }
         return false;
     });
  });

  $("#modal-codigo-barras form").on('submit', function (event) {
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: $('#modal-codigo-barras form a[href*="produtos/editar"]').attr('href'),
      data:  $("#modal-codigo-barras form").serialize(),
      dataType: 'json',
      success: function(data){
        if(data.result == true){
          $('#modal-codigo-barras button[type=submit]').css('background', 'green'); 
        }else if(data.result == false){
          $('#modal-codigo-barras button[type=submit]').css('background', '#da4f49');

        }
      }
    });
  });
  function scanCodigo(input){
    // Apaga os dados do input primeiro, caso tenha
    $(input).val('');
    var _time = '';
    $(document).off('keypress');
    $("#modal-codigo-barras").on('hide.bs.modal', function () {
      $(input).val('');
    });
    
    $(document).on('click', '#btn-voltar', function(e){
      $("#modal-codigo-barras .modal-header h5").text('Escanear código de barras').find('i').remove();
      $("#modal-codigo-barras .modal-body .div-scan").show();
      $("#modal-codigo-barras .modal-body .div-wrapper").addClass('hidden');
    });
    $(document).on('keypress', function(e){
      clearTimeout(_time);
      if(e.wich != 13 && e.keyCode != 13){
        var val = $(input).val();
        $(input).val(val+e.key);
        _time = setTimeout(function(){ 
          if($(input).val().length < 13){
            $(input).val('');           
          }
        }, 50);
        if($(input).val().length > 12){
          var codigo = $(input).val();
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/produtos/buscar_codigo",
            data:  $("#modal-codigo-barras form").serialize(),
            dataType: 'json',
            success: function(data){
              $("#modal-codigo-barras .modal-header h5").text('Leitura efetuada').prepend('<i id="btn-voltar" class="icon-arrow-left" style="margin-right: 10px; cursor: pointer;">');
              $("#modal-codigo-barras .modal-body .div-scan").hide();
              if(data.result == true){
                $("#modal-codigo-barras .modal-body .div-wrapper").html('<table class="table table-bordered"><tbody><tr><td style="text-align: right; width: 30%"><strong>Descrição</strong></td><td><input id="descricao" type="text" name="descricao" value="'+(data.produto.descricao == null ? '' : data.produto.descricao)+'"  /><input type="hidden" name="modal_ajax" value="true" /></td></tr><tr><td style="text-align: right"><strong>Código do produto</strong></td><td><input id="codigo_produto"  type="text" name="codigo_produto" value="'+(data.produto.codigo_produto == null ? '' : data.produto.codigo_produto)+'"  /></td></tr><tr><td style="text-align: right"><strong>Peso</strong></td><td><input id="peso"  type="text" name="peso" value="'+(data.produto.peso == null ? '' : data.produto.peso)+'"  /></td></tr><tr><td style="text-align: right"><strong>Impostos</strong></td><td><?php if(!empty($taxas)){ ?><select name="impostos" id="impostos" value=""><option value="">Selecione</option><?php foreach($taxas as $v){ ?><option value="<?php echo $v->idTaxas ?>"><?php echo $v->titulo; ?>@<?php echo $v->valor; ?>%</option><?php } ?></select><?php } ?></td></tr><tr><td style="text-align: right"><strong>Unidade</strong></td><td><input id="unidade" type="text" name="unidade" value="'+(data.produto.unidade == null ? '' : data.produto.unidade)+'"  /></td></tr><tr><td style="text-align: right"><strong>Marca</strong></td><td><input id="produtoMarca"  type="text" name="marca" value="'+(data.produto.nome == null ? '' : data.produto.nome)+'"  /><input id="produtoMarcas_id" type="hidden" class="span12" name="marcas_id" value="'+(data.produto.marcas_id == null ? '' : data.produto.marcas_id)+'"  /></td></tr><tr><td style="text-align: right"><strong>Fornecedor</strong></td><td><input id="produtoFornecedor"  type="text" name="fornecedor" value="'+(data.produto.nomeFornecedor == null ? '' : data.produto.nomeFornecedor)+'"  /><input id="produtoFornecedors_id" type="hidden" class="span12" name="marcas_id" value="'+(data.produto.fornecedor_id == null ? '' : data.produto.fornecedor_id)+'"  /></td></tr><tr><td style="text-align: right"><strong>NCM</strong></td><td><input id="ncm" type="text" name="ncm" value="'+(data.produto.ncm == null ? '' : data.produto.ncm)+'"  /></td></tr><tr><td style="text-align: right"><strong>CFOP</strong></td><td><input id="cfop" type="text" name="cfop" value="'+(data.produto.cfop == null ? '' : data.produto.cfop)+'"  /></td></tr><tr><td style="text-align: right"><strong>Preço de compra</strong></td><td>R$ <input id="precoCompra" class="money" type="text" name="precoCompra" value="'+(data.produto.precoCompra == null ? '' : data.produto.precoCompra)+'"  /></td></tr><tr><td style="text-align: right"><strong>Preço de Venda</strong></td><td>R$ <input id="precoVenda" class="money" type="text" name="precoVenda" value="'+(data.produto.precoVenda == null ? '' : data.produto.precoVenda)+'"  /></td></tr><tr><td style="text-align: right"><strong>Preço sugerido</strong></td><td>R$ <input id="preco_sugerido" class="money" type="text" name="preco_sugerido" value="'+(data.produto.preco_sugerido == null ? '' : data.produto.preco_sugerido)+'" <?php echo $this->session->userdata('id') != 4 ? "disabled" : ""; ?> /></td></tr><tr><td style="text-align: right"><strong>Estoque</strong></td><td><input id="estoque" type="text" name="estoque" value="'+(data.produto.estoque == null ? '' : data.produto.estoque)+'"  /></td></tr><tr><td style="text-align: right"><strong>Estoque Ideal</strong></td><td><input id="estoque_ideal" type="text" name="estoque_ideal" value="'+(data.produto.estoque_ideal == null ? '' : data.produto.estoque_ideal)+'"  /></td></tr><tr><td style="text-align: right"><strong>Estoque Mínimo</strong></td><td><input id="estoqueMinimo" type="text" name="estoqueMinimo" value="'+(data.produto.estoqueMinimo == null ? '' : data.produto.estoqueMinimo)+'"  /></td></tr><tr><td style="text-align: right"><strong>Fotos</strong></td><td><img '+(data.produto.img07 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img07+'" width="200px"><img '+(data.produto.img08 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img08+'" width="200px"><img '+(data.produto.img09 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img09+'" width="200px"><img '+(data.produto.img10 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img10+'" width="200px"><img '+(data.produto.img11 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img11+'" width="200px"><img '+(data.produto.img12 == null ? 'style="display: none;' : '')+'" src="<?php echo base_url().reisdev('url')?>cam-api/saved_img/'+data.produto.img12+'" width="200px"></td></tr><tr><td style="text-align: right"><strong>Ação</strong></td><td><button style="margin-right: 1%" type="submit" class="btn btn-info tip-top" title="Salvar informações"><i class="icon-ok icon-white"></i></button><a style="margin-right: 1%" href="#modal-excluir-produto" role="button" data-toggle="modal" produto="'+data.produto.idProdutos+'" class="btn btn-danger tip-top" title="Excluir Produto"><i class="icon-remove icon-white"></i></a><a href="<?php echo base_url(); ?>index.php/produtos/editar/'+data.produto.idProdutos+'" class="btn btn-info tip-top" target="_blank" title="Ir para página de edição"><i class="icon-pencil icon-white"></i></a></td></tr><tr><td style="text-align: right"></td><td><label style="margin-bottom: -12px;font-size: 12px;">Qtd</label><input type="number" data-href="<?php echo base_url(); ?>index.php/vendas/adicionar/'+data.produto.idProdutos+'"href="<?php echo base_url(); ?>index.php/vendas/adicionar/'+data.produto.idProdutos+'/" onchange="$(\'#efetuarVenda\').attr(\'href\', $(this).data(\'href\')+\'/\'+$(this).val());" name="qty" value="1" style="width: 50px;margin-top: 11px;margin-right: 5px;"><a href="<?php echo base_url(); ?>index.php/vendas/adicionar/'+data.produto.idProdutos+'" id="efetuarVenda" class="btn btn-info tip-top" title="Ir para página de venda"><i class="icon-shopping-cart icon-white"></i> Efetuar venda</a></td></tr></tbody></table>').removeClass('hidden');
                $("#produtoMarca").autocomplete({
                        source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
                        minLength: 1,
                        select: function( event, ui ) {
                            $("#produtoMarcas_id").val(ui.item.id);
                        }
                    });
              }
              else{
                $("#modal-codigo-barras .modal-body .div-wrapper").html('<div class="escanear_cod text-center"><button style="font-size: 80px;background: none;border: none;color: #666666;" title="Inserir" onclick="$(\'#modal-codigo-barras\').modal(\'hide\');$(\'#campo_codigo_barras\').val(\''+codigo+'\');$(\'#modal-inserir-componente .modal-header h5\').text(\'Inserir Produto (Código '+codigo+')\');$(\'#modal-inserir-componente\').modal(\'show\');"><i class="icon-plus-sign"></button></i><br><h2>Este produto não consta no sistema, clique para cadastrar</h2></div>').removeClass('hidden');
              }
              $("#modal-codigo-barras").modal('show');
            }
          });
          $(input).val('');           
        }
        
      }
    });
  }


  setInterval(function(){
    verificarOrcamentos();
  }, 300000)

function verificarOrcamentos(){
   $.ajax({
    url: "<?php echo base_url()?>index.php/sistemaos/verificarOrcamentos",
    dataType: "json",
    success: function(data){
      if(data.total > 0){
          $(".alerta-orcamento").show();
        var audio = new Audio("<?php echo base_url();?>assets/audio/alert.mp3");
          audio.play();
      }else{
        $(".alerta-orcamento").hide();
      }
    },
    error:function(){
    }
  });
}  

</script>




</body>
</html>
