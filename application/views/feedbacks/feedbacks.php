<?php
	require (reisdev('bootstrap'));
	$idFranquia = $this->session->userdata('id');
?>

<div class="widget-box">
	<div class="widget-title">
        	<span class="icon">
            		<i class="icon icon-star"></i>
         	</span>
        	<h5>Avaliações</h5>
     	</div>
</div>
<div class="widget-content nopadding">
<?php
	$dt = new DataTable();
	if ($idFranquia == '1')
		$dt->Columns(array('Avaliação', 'Data', 'Comentário', ''));
	else
		$dt->Columns(array('Avaliação', 'Data', 'Comentário'));
		
	if($results)
	{
		foreach ($results as $r)
		{
			if ($r->status != 1)
				continue;
			
			$star = ' ';
			if ($r->star > 0)
			{
				for ($i = 0; $i < $r->star; $i++)
					$star = $star.'<i class="icon icon-star"></i>';
					
				if ($r->star > 1)
					$star = $star.' ('.$r->star.' estrelas)';
				else
					$star = $star.' (1 estrela)';
			}
			else
				$star = 'Nenhuma estrela.';
				
			$data = date('d/m/Y',  strtotime($r->dataFeedback));
			
			$buttons = ' ';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'dFeedback'))
        			$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" feedback="'.$r->idFeedbacks.'" class="btn btn-danger tip-top" title="Excluir Avaliação"><i class="icon-remove icon-white"></i></a>';

			if ($idFranquia == "1")
				$dt->add_Item(array
				(
					$star,
					$data,
					$r->valor,
					$buttons
				));
			else
				$dt->add_Item(array
				(
					$star,
					$data,
					$r->valor
				));
			
		}
	}
	$dt->End();
?>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/feedbacks/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir Avaliação</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idFeedback" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir esta avaliação?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>






<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var feedback = $(this).attr('feedback');
        $('#idFeedback').val(feedback);

    });

});

</script>