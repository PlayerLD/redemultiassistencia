<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<div class="row-fluid" style="margin-top:0">
  <div class="span12">
    <div class="widget-box">
      <div class="widget-title">
        <span class="icon">
          <i class="icon-align-justify"></i>
        </span>
        <h5>Editar Despesa e Receita</h5>
      </div>
      <div class="widget-content">
        <?php if ($custom_error != '') {
          echo '<div class="alert alert-danger">' . $custom_error . '</div>';
        } ?>
        <div id="conteudo">
          <div class="formulario">
            <form action="<?php echo current_url(); ?>" id="formDespesa" method="post" class="form-horizontal" >
              <div class="dados">
                <div class="campos">

                                    <div class="campo" style="width: 24%">
                                      <label for="status">Status <span class="required">*</span></label>
                                        <select name="status" id="status">
                                          <option value="A" <?php echo $result->status == 'A' ? 'selected' : ''; ?>>Agendado</option>
                                          <option value="P" <?php echo $result->status == 'P' ? 'selected' : ''; ?>>Pago</option>
                                          <option value="AT" <?php echo $result->status == 'AT' ? 'selected' : ''; ?>>Atrasado</option>
                                        </select>
                                    </div>

                                    <div class="campo" style="width: 24%">
                                      <label for="tipo">Tipo <span class="required">*</span></label>
                                        <select name="tipo" id="tipo">
                                          <option value="1" <?php echo $result->tipo == 1 ? 'selected' : ''; ?>>Despesa</option>
                                          <option value="2" <?php echo $result->tipo == 2 ? 'selected' : ''; ?>>Receita</option>
                                        </select>
                                    </div>
                                  <div class="campo last" style="width: 100%">
                                      <label for="nome">Nome da Despesa<span class="required">*</span></label>
                                        <input id="nome" type="text" name="nome" value="<?php echo $result->nome; ?>" />
                                    </div>
                                  <div class="campo" style="width: 24%">
                                      <label for="data_vencimento">Data de vencimento<span class="required">*</span></label>
                                            <input id="data_vencimento" class="span12 datepicker" type="text" name="data_vencimento" value="<?php echo date('d/m/Y', strtotime($result->data_vencimento)); ?>" />
                                    </div>
                                  <div class="campo" style="width: 24%">
                                      <label for="valor">Valor<span class="required">*</span></label>
                                        <input id="valor" name="valor" value="<?php echo $result->valor; ?>" class="money" class="span2" type="text" placeholder="0,00"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="forma_pagamento">Forma de pagamento <span class="required">*</span></label>
                                        <select name="forma_pagamento" value="<?php echo $result->forma_pagamento; ?>" id="forma_pagamento">
                                          <option value="1" <?php echo $result->status == '1' ? 'selected' : ''; ?>>A vista</option>
                                          <option value="2" <?php echo $result->status == '2' ? 'selected' : ''; ?>>Em lotes</option>
                                        </select>
                                    </div>
                                    <div class="campo last" style="width: 24%">
                                      <label for="codigo">Código <span class="required">*</span></label>
                                        <input id="codigo" type="text" name="codigo" value="<?php echo $result->codigo; ?>" />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="fornecedor">Fornecedor</label>
                                        <input id="fornecedor" type="text" name="fornecedor" value="<?php echo $result->fornecedor; ?>"  />
                                        <input id="fornecedor_id" type="hidden" class="span12" name="fornecedor_id" value="<?php echo $result->fornecedor_id; ?>" />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="data_emissao">Data de emissão<span class="required">*</span></label>
                                            <input id="data_emissao" class="span12 datepicker" type="text" name="data_emissao" value="<?php echo date('d/m/Y', strtotime($result->data_emissao)); ?>" />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="data_pagamento">Data de pagamento<span class="required">*</span></label>
                                            <input id="data_pagamento" class="span12 datepicker" type="text" name="data_pagamento" value="<?php echo date('d/m/Y', strtotime($result->data_pagamento)); ?>" />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="valor_pago">Valor Pago</label>
                                        <input id="valor_pago" name="valor_pago" value="<?php echo $result->valor_pago; ?>" class="money" class="span2" type="text" placeholder="0,00"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="juros">Juros</label>
                                        <input id="juros" name="juros" value="<?php echo $result->juros; ?>" class="money" class="span2" type="text" placeholder="0,00"  />
                                    </div>
                                    <div class="campo" style="width: 24%">
                                      <label for="desconto">Desconto</label>
                                        <input id="desconto" name="desconto" value="<?php echo $result->desconto; ?>" class="money" class="span2" type="text" placeholder="0,00"  />
                                    </div>
                                    <div class="campo last" style="width: 100%">
                                      <label for="obs">Observação</label>
                                        <textarea id="obs" rows="5" type="text" name="obs"><?php echo $result->obs; ?></textarea>
                                    </div>
                                    
                                    <div class="campo last" style="width: 100%; height: 10px"></div>
                                    <h3></h3>
                                    <div class="campo last" style="width: 100%; text-align:center;">
                                      <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Salvar</button>
                                    <a href="<?php echo base_url() ?>index.php/despesas" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </div>
          <script type="text/javascript">
            $(document).ready(function(){

              window.onload = function (event) {
                updatePreview();
              };

              $("#marca").autocomplete({
                source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteMarca",
                minLength: 1,
                select: function( event, ui ) {
                  $("#marcas_id").val(ui.item.id);
                }
              });

              $("#fornecedor").autocomplete({
          source: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedores",
          minLength: 1,
          select: function( event, ui ) {
              $("#fornecedor_id").val(ui.item.id);
            }
        });

              $('#formDespesa').validate({
                rules :{
                  tipo: { required: true},
                  status: { required: true},
                  nome: { required: true},
                  data_vencimento: { required: true},
                  valor: { required: true},
                  forma_pagamento: { required: true},
                  codigo: { required: true},
                  data_emissao: { required: true},
                  data_pagamento: { required: true},
            },
            messages:{
                  tipo: { required: ''},
                  status: { required: ''},
                  nome: { required: ''},
                  data_vencimento: { required: ''},
                  valor: { required: ''},
                  forma_pagamento: { required: ''},
                  codigo: {required: ''},
                  data_emissao: {required: ''},
                  data_pagamento: {required: ''},
            },
                errorClass: "error",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                  $(element).parents('.campo').removeClass('success');
                  $(element).parents('.campo').addClass('error');
                },
                unhighlight: function(element, errorClass, validClass) {
                  $(element).parents('.campo').removeClass('error');
                  $(element).parents('.campo').addClass('success');
                }
              });
            });
          </script>