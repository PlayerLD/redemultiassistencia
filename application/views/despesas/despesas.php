<?php
require (reisdev('bootstrap'));
if($this->permission->checkPermission($this->session->userdata('permissao'),'aDespesa'))
	{ ?>
<a href="<?php echo base_url();?>index.php/despesas/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Receitas e Despesas</a>
<?php 	} ?> 
		<script type="text/javascript">
		    function gerarCodigoBarra(cod){
		      	window.open('<?php echo site_url('despesas/geraCodigoBarra'); ?>/'+cod, '_blank', 'location=yes,height=1024,width=768,scrollbars=yes,status=yes');
		      	console.log(cod);
		    }
		</script>
<style type="text/css">
	div.escanear_cod {
		max-width: 280px;
		border: 1px solid #ccc;
		padding: 20px;
		font-size: 80px;
		margin: 0 auto;
		border-radius: 10px;
	}
	div.escanear_cod h2 {
		font-size: 16px;
		line-height: 1;
	}
</style>
<div class="widget-box">
	<div class="widget-title">
		<span class="icon"><i class="icon-barcode"></i></span>
		<h5>Receitas e Despesas</h5>
	</div>
</div>
<div class="widget-content nopadding">
	<?php
	$dt = new DataTable();
	$dt->Columns(array('Id', 'Código', 'Nome', 'Valor', 'Data Vencimento', 'Data Emissão', 'Data Pagamento', 'Tipo', 'Status', ''));
	$tipo = array(1 => '<span class="alert alert-danger" style="display: inherit;padding: 5px 10px;"><i class="icon icon-minus"></i> Despesa</span>', 2 => '<span class="alert alert-success" style="display: inherit;padding: 5px 10px;"><i class="icon icon-plus"></i> Receita</span>');
	$status = array('P' => '<span class="alert alert-success" style="display: inherit;padding: 5px 10px;">Pago</span>', 'A' => '<span class="alert alert-success" style="display: inherit;padding: 5px 10px;">Agendado</span>', 'AT' => '<span class="alert alert-success" style="display: inherit;padding: 5px 10px;">Atrasado</span>');	
	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
			// if($this->permission->checkPermission($this->session->userdata('permissao'),'vDespesa'))
			// 	$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/despesas/visualizar/'.$r->idDespesas.'" class="btn tip-top" title="Visualizar Despesa"><i class="icon-eye-open"></i></a>  ';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'eDespesa'))
				$buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/despesas/editar/'.$r->idDespesas.'" class="btn btn-info tip-top" title="Editar Lançamento"><i class="icon-pencil icon-white"></i></a>';
			if($this->permission->checkPermission($this->session->userdata('permissao'),'dDespesa'))
				$buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" despesa="'.$r->idDespesas.'" class="btn btn-danger tip-top" title="Excluir Lançamento"><i class="icon-remove icon-white"></i></a>';
			$dt->add_Item(array
				(
					$r->idDespesas,
					$r->codigo,
					$r->nome,
					'R$'.number_format($r->valor, 2, ',', '.'),
					date('d/m/Y', strtotime($r->data_vencimento)),
					date('d/m/Y', strtotime($r->data_emissao)),
					date('d/m/Y', strtotime($r->data_pagamento)),
					$tipo[$r->tipo],
					$status[$r->status],
					$buttons
					));
		?>
		
<?php
		} 
	}
	$dt->End();
	?>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1002 !important;">
  <form action="<?php echo base_url() ?>index.php/despesas/excluir" method="post" >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h5 id="myModalLabel">Excluir Despesa</h5>
    </div>
    <div class="modal-body">
      <input type="hidden" id="idDespesa" name="id" value="" />
      <h5 style="text-align: center">Deseja realmente excluir este despesa?</h5>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
      <button class="btn btn-danger">Excluir</button>
    </div>
  </form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', 'a', function(event) {
			var despesa = $(this).attr('despesa');
			$('#idDespesa').val(despesa);
		});
	});
</script>