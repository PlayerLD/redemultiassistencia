<div class="widget-box">
	<div class="widget-title">
            	<span class="icon"><i class="icon-hdd"></i></span>
            	<h5>Arquivos</h5>
	</div>
</div>
<div class="widget-content nopadding">
<?php
	require (reisdev('bootstrap'));
	
	$dt = new DataTable();
	$dt->Columns(array('Cód', 'Documento', 'Data', 'Tamanho', 'Extensão', ''));
	if($results)
	{
		foreach ($results as $r)
		{
			$buttons = ' ';
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'vArquivo'))
                        	$buttons = $buttons.'<a class="btn btn-inverse tip-top" style="margin-right: 1%" target="_blank" href="'.$r->url.'" class="btn tip-top" title="Imprimir"><i class="icon-print"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'vArquivo'))
                        	$buttons = $buttons.'<a href="'.base_url().'index.php/arquivos/download/'.$r->idDocumentos.'" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'eArquivo'))
                        	$buttons = $buttons.'<a href="'.base_url().'index.php/arquivos/editar/'.$r->idDocumentos.'" class="btn btn-info tip-top" style="margin-right: 1%" title="Editar"><i class="icon-pencil icon-white"></i></a>';
                        	
                    	if($this->permission->checkPermission($this->session->userdata('permissao'),'dArquivo'))
                         	$buttons = $buttons.'<a href="#modal-excluir" style="margin-right: 1%" role="button" data-toggle="modal" arquivo="'.$r->idDocumentos.'" class="btn btn-danger tip-top" title="Excluir Arquivo"><i class="icon-remove icon-white"></i></a>';
                    
			$dt->add_Item(array
			(
				$r->idDocumentos,
				$r->documento,
				date('d/m/Y',strtotime($r->cadastro)),
				$r->tamanho,
				$r->tipo,
				$buttons
			));
		}
	}
	$dt->End();
?>
</div>

 
<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/arquivos/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir Arquivo</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idDocumento" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir este arquivo?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>



<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var arquivo = $(this).attr('arquivo');
        $('#idDocumento').val(arquivo);

   });

   //$(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
});

</script>
