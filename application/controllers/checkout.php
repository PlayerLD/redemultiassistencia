<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Bcash\Exception\ValidationException;
use Bcash\Exception\ConnectionException;
use Bcash\Test\NotificationSimulator;
use Bcash\Domain\PaymentMethodEnum;
class Checkout extends CI_Controller {
    public $data = array();
    
    function  __construct() {
        parent::__construct();
        $this->load->model('pagamentos_model', 'pagamentos');
        $this->data['cliente'] = $this->cliente = $this->session->userdata('ep4_login');
        $this->data['estados'] = array(''=>'Selecione','AC'=>'Acre', 'AL'=>'Alagoas', 'AM'=>'Amazonas', 'AP'=>'Amapá','BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal','ES'=>'Espírito Santo','GO'=>'Goiás','MA'=>'Maranhão','MT'=>'Mato Grosso','MS'=>'Mato Grosso do Sul','MG'=>'Minas Gerais','PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná','PE'=>'Pernambuco','PI'=>'Piauí','RJ'=>'Rio de Janeiro','RN'=>'Rio Grande do Norte','RO'=>'Rondônia','RS'=>'Rio Grande do Sul','RR'=>'Roraima','SC'=>'Santa Catarina','SE'=>'Sergipe','SP'=>'São Paulo','TO'=>'Tocantins');
        $this->qual = array('A'=>'Apostila', 'C'=>'Curso', 'E'=>'Eventos');
    }
    
    function pagamento($dados = array(), $item = array()) {
        // $login = $this->session->userdata('ep4_login');
        // $cupom = $this->session->userdata('ep4_cupom');
        // $cart = $this->cart->contents();
        // if(empty($login))
        //     redirect('checkout/login');
        
        // if(empty($cart))
        //     redirect('checkout/carrinho');
        // Complementando cadastro
        // $dados = $this->clientes->obter(array('id'=>$login['id']));
        // if((empty($dados->cpf) && empty($dados->cnpj)) || empty($dados->cep))
        //     redirect('checkout/cadastro');
        // $this->data['curso'] = $curso = $this->cursos->obter(array('uri'=>$curso));
        // if(!empty($_POST)) {
        //     $json = array();
            $pagamento = 'cartao';
            // $this->form_validation->set_rules('pagamento', 'Forma de pagamento', 'required');
            // if($pagamento == 'cartao') {
            //     $this->form_validation->set_rules('cartao_numero', 'Número do cartão', 'required');
            //     $this->form_validation->set_rules('cartao_nome', 'Seu nome (Igual do cartão)', 'required');
            //     $this->form_validation->set_rules('cartao_validade', 'Data de validade', 'required');
            //     $this->form_validation->set_rules('cartao_codigo', 'Código de segurança', 'required|integer');
            //     $this->form_validation->set_rules('cartao_bandeira', 'Cartão',    'callback_validar_bandeira');
            // }
            
            // if($this->form_validation->run()) {
                $dados  = (object) array(
                    'id'          => "313",
                    'cartao_bandeira' => 'MASTERCARD',
                    'nome'        => "Lucas Daniel",
                    'cpf'         => "139.435.596-37",
                    'empresa'     => "",
                    'cnpj'        => "",
                    'telefone'    => "(62) 92510755",
                    'celular'     => "",
                    'logradouro'  => "Rua das Castanheiras",
                    'numero'      => "S/N",
                    'complemento' => "Q4 L1",
                    'bairro'      => "Residencial Por do Sol",
                    'cidade'      => "Aparecida de Goiânia",
                    'estado'      => "GO",
                    'cep'         => "74946572",
                    'email'       => "lucas@agenciaweek.com.br",
                    'senha'       => "OLCAsbq7g6ed+yZzLhqshOo4DsaDPDTA1d/pB8T/yQHqSYf1loA3SsOIU+c0dZJyEbmc/l6AOTFhowW5zcYujw==",
                    'hash'        => "oQXiCbUjGgEtr5rlNPgP",
                    'data'        => "2017-06-21 10:30:41",
                    
                    'pagamento'   => "cartao",
                    'cartao_validade' => '11/25',
                    'cartao_nome' => 'Lucas D O da Silva',
                    'cartao_numero' => '5268630602818338',
                    'cartao_codigo' => '551',
                    'parcela' => 1
                );
                
                $item    = (object) array(
                        'id' => "52",
                        'produto_id' => "16",
                        'tabela' => "C",
                        'titulo' => "Intensivo ENEM",
                        'uri' => "intensivo-enem",
                        'valor' => "1000.00",
                        // 'desconto' => "980.00",
                        'parcela_valor' => "250",
                        'parcela_quantidade' => "3",
                        'imagem' => "ed4017cf536adb6b2edd4e131a248116.png",
                        'data' => "2017-07-03 11:02:58",
                        'qty' => "1",
                        'status' => "A",
                    );
                $bandeira = $this->bcash->get_payment_method($dados->cartao_bandeira);
                // Pedidos
                $pagamentos_id = $this->pagamentos->criar(array(
                    'clientes_id'   => $dados->id,
                    'total'         => $item->valor,
                    ));
                // Endereço
                $address = $this->bcash->address();
                $address->setAddress($dados->logradouro);
                $address->setNumber($dados->numero);
                $address->setComplement($dados->complemento);
                $address->setNeighborhood($dados->bairro);
                $address->setCity($dados->cidade);
                $address->setState($dados->estado);
                $address->setZipCode($dados->cep);
                // Cliente
                $customer = $this->bcash->customer();
                $customer->setMail($dados->email);
                $customer->setName($dados->nome);
                $customer->setCpf($dados->cpf);
                if(!empty($dados->cnpj)){
                    $customer->setCompanyName($dados->empresa);
                    $customer->setCnpj($dados->cnpj);
                }
                $customer->setPhone($dados->telefone);
                $customer->setCellPhone($dados->celular);
                $customer->setAddress($address);
                // // Produtos
                $products = array();
                // foreach($cart as $rowid => $row) {
                    // Adiciona o produto ao pedido
                    $this->pagamentos->rel($pagamentos_id, $item->id, $item->qty);
                    $products[0] = $this->bcash->product();
                    $products[0]->setCode($item->id);
                    $products[0]->setDescription("Teo Cursos - {$item->titulo}");
                    $products[0]->setAmount(1);
                    $products[0]->setValue($item->valor);
                // }
                $products = array_values($products);
                // echo '<pre>'; var_dump($products); 
                // exit;
                // Transação
                $transaction = $this->bcash->transaction();
                $transaction->setOrderId($pagamentos_id);
                $transaction->setBuyer($customer);
                // Cartão de crédito
                if($pagamento == 'cartao') {
                    $transaction->setPaymentMethod($bandeira);
                    $validade = $dados->cartao_validade;
                    $credit_card = $this->bcash->credit_card();
                    $credit_card->setHolder($dados->cartao_nome);
                    $credit_card->setNumber($dados->cartao_numero);
                    $credit_card->setSecurityCode($dados->cartao_codigo);
                    $credit_card->setMaturityMonth(substr($validade, 0, 2));
                    $credit_card->setMaturityYear('20'.substr($validade, 3));
                    $transaction->setCreditCard($credit_card);
                    $transaction->setInstallments($dados->parcela);
                } else {
                    $transaction->setPaymentMethod(PaymentMethodEnum::BANK_SLIP);
                }
                $transaction->setUrlNotification(site_url("checkout/notificacao/{$pagamentos_id}"));
                $transaction->setProducts($products);
                $transaction->setAcceptedContract('S');
                $transaction->setViewedContract('S');
                // Pagamento
                restore_exception_handler();
                $payment = $this->bcash->payment();
                try {
                    $response = $payment->create($transaction);
                    
                    $this->pagamentos->editar($pagamentos_id, array(
                        'bcash_id'      => $response->transactionId,
                        'bcash_token'   => isset($response->permanentToken) ? $response->permanentToken : '',
                        'boleto'        => $dados->pagamento == 'boleto' ? $response->paymentLink : null
                        ));
                    // $this->email_pedido($pedidos_id, false);
                    // Utilizar cupom
                    // if(!empty($cupom)) {
                    //     $this->cupons->utilizar($cupom['id']);
                    // }
                    $json['success'] = 'Pagamento realizado com sucesso! Aguarde enquanto você é redirecionado.';
                    $json['redirect'] = site_url("checkout/confirmacao/{$pagamentos_id}");
                    log_message('error', json_encode($response));
                } catch (ValidationException $e) {
                    $errors = $e->getErrors();
                    $error = array();
                    foreach ($errors->list as $e) {
                        $error[] = $e->description;
                    }
                    $json['error1'] = implode('<br>', $error);
                } catch (ConnectionException $e) {
                    $json['error2'] = $e->getMessage();
                }
            // } else {
            //     $json['error3'] = implode('<br>', $this->form_validation->_error_array);
            // }
            echo json_encode($json);
            exit;
        }
    }