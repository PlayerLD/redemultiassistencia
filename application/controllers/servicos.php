<?php

class Servicos extends CI_Controller {
    

    /** 
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('os_model');
        $this->data['menuCatalogo'] = 'servicos';
    }
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar serviços.');
           redirect(base_url());
        }

	        $countrows = $this->servicos_model->count('servicos', $this->session->userdata('id'));        
		$this->data['results'] = $this->servicos_model->get('servicos','idServicos,idFranquia,nome,descricao,preco','',$countrows,$this->uri->segment(3));
       
	    $this->data['view'] = 'servicos/servicos';
       	$this->load->view('tema/topo',$this->data);
	}

    // server side

    function getlists(){

        $columns = array(
                0 => 'idServicos',
                1 => 'nome',
                2 => 'preco',
                3 => 'descricao',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->servicos_model->count_totalServicos();


        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->servicos_model->allServicos($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->servicos_model->idServicos_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->servicos_model->servicos_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['idServicos'] = $post->idServicos;
                $nestedData['nome'] = $post->nome;
                $nestedData['preco'] = number_format($post->preco,2,',','.');
                $nestedData['descricao'] = $post->descricao;
               
                // adicionando as funções do botões de ações
                $buttons = ' ';

            if($this->permission->checkPermission($this->session->userdata('permissao'),'eServico')){
                $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/servicos/editar/'.$post->idServicos.'" class="btn btn-info tip-top" title="Editar Serviço"><i class="icon-pencil icon-white"></i></a>';      
            }
            if($this->permission->checkPermission($this->session->userdata('permissao'),'dServico')){
                $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" servico="'.$post->idServicos.'" class="btn btn-danger tip-top" title="Excluir Serviço"><i class="icon-remove icon-white"></i></a>  ';
            }

            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }

    // end server side

	
    function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aServico')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar serviços.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('servicos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);

            $data = array(
                'nome' => set_value('nome'),
                'itemListaServico' => set_value('itemListaServico'),
                'idFranquia' => set_value('idFranquia'),
                'descricao' => set_value('descricao'),
                'preco' => $preco
            );

            if ($this->servicos_model->add('servicos', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Serviço adicionado com sucesso!');
                redirect(base_url() . 'index.php/servicos/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'servicos/adicionarServico';
        $this->load->view('tema/topo', $this->data);

    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eServico')){
           $this->session->set_flashdata('error','Você não tem permissão para editar serviços.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('servicos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);
            $data = array(
                'nome' => $this->input->post('nome'),
                'itemListaServico' => set_value('itemListaServico'),
                'descricao' => $this->input->post('descricao'),
                'preco' => $preco
            );

            if ($this->servicos_model->edit('servicos', $data, 'idServicos', $this->input->post('idServicos')) == TRUE) {
                $this->session->set_flashdata('success', 'Serviço editado com sucesso!');
                redirect(base_url() . 'index.php/servicos/editar/'.$this->input->post('idServicos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->servicos_model->getById($this->uri->segment(3));

        $this->data['view'] = 'servicos/editarServico';
        $this->load->view('tema/topo', $this->data);

    }
	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dServico')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir serviços.');
           redirect(base_url());
        }
       
        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir serviço.');            
            redirect(base_url().'index.php/servicos/gerenciar/');
        }

        $this->db->where('servicos_id', $id);
        $this->db->delete('servicos_os');

        $this->servicos_model->delete('servicos','idServicos',$id);             
        

        $this->session->set_flashdata('success','Serviço excluido com sucesso!');            
        redirect(base_url().'index.php/servicos/gerenciar/');
    }
}

