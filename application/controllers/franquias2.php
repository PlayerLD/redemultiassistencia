<?php

class Franquias extends CI_Controller {
    

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {

        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cFranquia')){
          $this->session->set_flashdata('error','Você não tem permissão para configurar os usuários.');
          redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('franquias_model', '', TRUE);
        $this->data['menuFranquias'] = 'franquias';
    }

    function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
	$countrows = $this->franquias_model->count('franquias', $this->session->userdata('id'));        
        $this->data['results'] = $this->franquias_model->get($countrows,$this->uri->segment(3));
       
	$this->data['view'] = 'franquias/franquias';
       	$this->load->view('tema/topo',$this->data);

       
		
    }
	
    function adicionar(){  
          
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('franquias') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="alert alert-danger">'.validation_errors().'</div>' : false);

        } else
        {     

            $this->load->library('encrypt');                       
            $data = array(
                    'nome' => set_value('nome'),
					'rg' => set_value('rg'),
					'cpf' => set_value('cpf'),
					'rua' => set_value('rua'),
					'numero' => set_value('numero'),
					'bairro' => set_value('bairro'),
					'cidade' => set_value('cidade'),
					'estado' => set_value('estado'),
					'email' => set_value('email'),
					'senha' => $this->encrypt->sha1($this->input->post('senha')),
					'telefone' => set_value('telefone'),
					'celular' => set_value('celular'),
					'slotsp' => set_value('slotsp'),
					'slotsm' => set_value('slotsm'),
					'slotsg' => set_value('slotsg'),
					'situacao' => set_value('situacao'),
                    'permissoes_id' => $this->input->post('permissoes_id'),
					'dataCadastro' => date('Y-m-d')
            );
           
			if ($this->franquias_model->add('franquias',$data) == TRUE)
			{
                                $this->session->set_flashdata('success','Usuário cadastrado com sucesso!');
				redirect(base_url().'index.php/franquias/adicionar/');
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';

			}
		}
        
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome');   
        $this->data['menuFranquias'] = 'adicionar';
	$this->data['view'] = 'franquias/adicionarFranquia';
        $this->load->view('tema/topo',$this->data);
   
       
    }	
    
    function editar(){  
        
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rg', 'RG', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rua', 'Rua', 'trim|required|xss_clean');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slotsp', 'Slot Pequeno', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slotsm', 'Slot Médio', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slotsg', 'Slot Grande', 'trim|required|xss_clean');
        $this->form_validation->set_rules('situacao', 'Situação', 'trim|required|xss_clean');
        $this->form_validation->set_rules('permissoes_id', 'Permissão', 'trim|required|xss_clean');

        if ($this->form_validation->run() == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        } else
        { 

            if ($this->input->post('idFranquias') == 1 && $this->input->post('situacao') == 0)
            {
                $this->session->set_flashdata('error','O usuário super admin não pode ser desativado!');
                redirect(base_url().'index.php/franquias/editar/'.$this->input->post('idFranquias'));
            }

            $senha = $this->input->post('senha'); 
            if($senha != null){
                $this->load->library('encrypt');   
                $senha = $this->encrypt->sha1($senha);

                $data = array(
                        'nome' => $this->input->post('nome'),
                        'rg' => $this->input->post('rg'),
                        'cpf' => $this->input->post('cpf'),
                        'rua' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'bairro' => $this->input->post('bairro'),
                        'cidade' => $this->input->post('cidade'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'senha' => $senha,
                        'telefone' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'slotsp' => $this->input->post('slotsp'),
                        'slotsm' => $this->input->post('slotsm'),
                        'slotsg' => $this->input->post('slotsg'),
                        'situacao' => $this->input->post('situacao'),
                        'permissoes_id' => $this->input->post('permissoes_id')
                );
            }  

            else{

                $data = array(
                        'nome' => $this->input->post('nome'),
                        'rg' => $this->input->post('rg'),
                        'cpf' => $this->input->post('cpf'),
                        'rua' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'bairro' => $this->input->post('bairro'),
                        'cidade' => $this->input->post('cidade'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'telefone' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'slotsp' => $this->input->post('slotsp'),
                        'slotsm' => $this->input->post('slotsm'),
                        'slotsg' => $this->input->post('slotsg'),
                        'situacao' => $this->input->post('situacao'),
                        'permissoes_id' => $this->input->post('permissoes_id')
                );

            }  

           
			if ($this->franquias_model->edit('franquias',$data,'idFranquias',$this->input->post('idFranquias')) == TRUE)
			{
                $this->session->set_flashdata('success','Usuário editado com sucesso!');
				redirect(base_url().'index.php/franquias/editar/'.$this->input->post('idFranquias'));
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';

			}
		}

		$this->data['result'] = $this->franquias_model->getById($this->uri->segment(3));
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome'); 

	$this->data['view'] = 'franquias/editarFranquia';
        $this->load->view('tema/topo',$this->data);
			
      
    }
	
    public function excluir(){

            $ID =  $this->uri->segment(3);
            $this->franquias_model->delete('franquias','idFranquias',$ID);             
            redirect(base_url().'index.php/franquias/gerenciar/');
    }
}

