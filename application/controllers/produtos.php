<?php
class Produtos extends CI_Controller {
    
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     *  
     */ 
    
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('os_model');
        $this->data['menuCatalogo'] = 'produtos';
    }
    function index(){
       $this->gerenciar();
    }
    function gerenciar(){
        $idFranquia=$this->session->userdata('id');
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }
        $countrows = $this->produtos_model->count('produtos', $this->session->userdata('id'));        
        $this->data['results'] = $this->produtos_model->get('produtos','codigo_barras,codigo_produto,idProdutos,idFranquia,descricao,unidade,marcas_id,precoCompra,precoVenda,estoque,estoqueMinimo','',$countrows,$this->uri->segment(3));
        $this->data['view'] = 'produtos/produtos';
        $this->load->view('tema/topo',$this->data);
    }

    // server side
    function getlists(){

        $columns = array(
                0 => 'idProdutos',
                1 => 'descricao',
                2 => 'codigo_barras',
                3 => 'codigo_produto',
                4 => 'estoque',
                5 => 'precoVenda',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->produtos_model->count_totalProdutos();


        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->produtos_model->allProdutos($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->produtos_model->idProdutos_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->produtos_model->produtos_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['idProdutos'] = $post->idProdutos;
                $nestedData['descricao'] = ($post->idFranquia == 4 ? "<strong style=\"color: #04c;\">{$post->descricao}</strong>": $post->descricao);
                $nestedData['codigo_barras'] = (!empty($post->codigo_barras) ? $post->codigo_barras.'<a href="javascript:;" class="pull-right btn btn-primary" title="Imprimi Código de Barras" onclick="gerarCodigoBarra('.$post->codigo_barras.');">IMPRIMIR</a>' : 'Não consta');
                $nestedData['codigo_produto'] = (!empty($post->codigo_produto) ? $post->codigo_produto : 'Não consta');
                $nestedData['estoque'] = $post->estoque;
                $nestedData['precoVenda'] = number_format($post->precoVenda,2,',','.');        
               
                // adicionando as funções do botões de ações
                $buttons = ' ';

            if($this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
                $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/produtos/visualizar/'.$post->idProdutos.'" class="btn tip-top" title="Visualizar Produto"><i class="icon-eye-open"></i></a>  ';
            }
            if($this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
                $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/produtos/editar/'.$post->idProdutos.'" class="btn btn-info tip-top" title="Editar Produto"><i class="icon-pencil icon-white"></i></a>';
            }
            if($this->permission->checkPermission($this->session->userdata('permissao'),'dProduto')){
                if($post->idFranquia != 4){
                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" produto="'.$post->idProdutos.'" class="btn btn-danger tip-top" title="Excluir Produto"><i class="icon-remove icon-white"></i></a>';
                }
                if($post->idFranquia == 4){
                    $buttons = $buttons.'<a onclick="$(\'#id_produto\').attr(\'value\', \''.$post->idProdutos.'\'); $(\'#modal-solicitacao-produto\').modal(\'show\');" href="#modal-solicitacao-produto" title="Solicitar para Central" class="btn btn-warning"> <i class="icon-truck" ></i> </a>  ';
                }
            }

            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }
    // end server side

    function produto_variacoes(){
 $idFranquia=$this->session->userdata('id');
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }
            $countrows = $this->produtos_model->count('produto_variacoes', $this->session->userdata('id'), false);        
        $this->data['results'] = $this->produtos_model->getVariacao('produto_variacoes','produto_variacoes.idVariacao, produto_variacoes.titulo, produto_variacoes.valor',['pai_id' => null],$countrows,$this->uri->segment(3));
       
        $this->data['view'] = 'produtos/produto_variacoes';
        $this->load->view('tema/topo',$this->data);      
        
    }

    function produto_variacoes_filho($pai_id){
 $idFranquia=$this->session->userdata('id');
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }
            $countrows = $this->produtos_model->count('produto_variacoes', $this->session->userdata('id'), false);        
        $this->data['results'] = $this->produtos_model->getVariacao('produto_variacoes','produto_variacoes.idVariacao, produto_variacoes.titulo, produto_variacoes.valor',['pai_id' => $pai_id],$countrows,$this->uri->segment(4));
       
        $this->data['pai_id'] = $pai_id;
        $this->data['view'] = 'produtos/produto_variacoes_filho';
        $this->load->view('tema/topo',$this->data);      
        
    }

    function gerenciar_taxas(){
 $idFranquia=$this->session->userdata('id');
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar taxas.');
           redirect(base_url());
        }
            $countrows = $this->produtos_model->count('produto_taxas', $this->session->userdata('id'));        
        $this->data['results'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$countrows,$this->uri->segment(3));
       
        $this->data['view'] = 'produtos/produto_taxas';
        $this->load->view('tema/topo',$this->data);      
        
    }
    
    function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produtos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);
            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);
            $preco_sugerido = $this->input->post('preco_sugerido');
            $preco_sugerido = str_replace(",", "", $preco_sugerido);
            $data = array(
                'codigo_barras' => $this->input->post('codigoBarras') ? $this->input->post('codigoBarras') : null,
                'descricao' => $this->input->post('descricao'),
                'codigo_produto' => $this->input->post('codigo_produto'),
                'tipo'    => (int)$this->input->post('tipo'),
                'ligacao'    => $this->input->post('ligacao'),
                'peso'    => $this->input->post('peso'),
                'impostos'    => (int)$this->input->post('impostos'),
                'fornecedor_id'    => (int)$this->input->post('fornecedor_id'),
                'estoque_ideal'    => $this->input->post('estoque_ideal'),
                'idFranquia' => $this->input->post('idFranquia'),
                'unidade' => $this->input->post('unidade'),
                'marcas_id' => $this->input->post('marcas_id'),
                'precoCompra' => $precoCompra,
                'precoVenda' => $precoVenda,
                'estoque' => $this->input->post('estoque'),
                'ncm' => $this->input->post('ncm'),
                'cfop' => $this->input->post('cfop'),
                'estoqueMinimo' => $this->input->post('estoqueMinimo')
            );
            if($this->session->userdata('id') == 4)
                $data['preco_sugerido'] = $preco_sugerido;
            
            
            if ($this->produtos_model->add('produtos', $data) == TRUE) {
                $this->session->set_flashdata('success','Produto adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['taxas'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$this->produtos_model->count('produto_taxas', $this->session->userdata('id')),$this->uri->segment(3));
        $this->data['tipos'] = $this->produtos_model->getTipos('produto_tipos','idTipos, titulo, valor','',$this->produtos_model->count('produto_tipos', $this->session->userdata('id')),$this->uri->segment(3));
        $this->data['view'] = 'produtos/adicionarProduto';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function adicionarTipo() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_tipos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $idFranquia = $this->session->userdata('id');

            $data = array(
                'idFranquia' => $idFranquia,
                'titulo'     => $this->input->post('titulo')
            );
            
            if ($this->produtos_model->add('produto_tipos', $data) == TRUE) {
                $this->session->set_flashdata('success','Tipo adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/adicionarTipo/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['view'] = 'produtos/adicionarTipo';
        $this->load->view('tema/topo', $this->data);
     
    }

    public function adicionarVariacaoAjax()
    {
        $variacao_id = $this->input->post('idVariacao');
        $produto_id = $this->input->post('idProdutos');
        $variacoes = $this->produtos_model->getVariacao('produto_variacoes','produto_variacoes.idVariacao, produto_variacoes.titulo, produto_variacoes.valor',['produto_variacoes.pai_id' => $variacao_id]);

        if (!empty($variacoes)) {

        foreach ($variacoes as $v) {
           $data = array(
                'id_produto' => $produto_id,
                'id_variacao' => $v->idVariacao
            );
           $this->produtos_model->add('produto_variacoes_rel', $data);
        }

            echo json_encode(array('result' => true));
        } else
            echo json_encode(array('result' => false));
    }

    public function atualizarVariacao()
    {
       $id_rel = $this->input->post('id_rel');

       $data = array();

       if($this->input->post('valor'))
       $data['valor'] = $this->input->post('valor');

       if($this->input->post('qtd'))   
       $data['qtd'] = $this->input->post('qtd');

       if($this->input->post('peso'))       
       $data['peso'] = $this->input->post('peso');

        if(!empty($data))
       $this->produtos_model->edit('produto_variacoes_rel', $data, 'id', $id_rel);

        echo json_encode(array('result' => true));
    }


    public function deletarVariacao()
    {
        $id_rel = $this->input->post('id_rel');

           $this->produtos_model->delete('produto_variacoes_rel', 'id', $id_rel);

            echo json_encode(array('result' => true));
    }

    public function autoCompleteVariacao()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->produtos_model->autoCompleteVariacao($q);
        }
    }

    function excluirVariacaoAjax()
    {
        $ID = $this->input->post('idVariacao');
        if ($this->produtos_model->deleteVariacao($ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
    
    function adicionarVariacao() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_variacoes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $idFranquia = $this->session->userdata('id');

            $data = array(
                'titulo'     => $this->input->post('titulo')
            );
            
            if ($this->produtos_model->add('produto_variacoes', $data) == TRUE) {
                $this->session->set_flashdata('success','Variacao adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/adicionarVariacao/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['view'] = 'produtos/adicionarVariacao';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function adicionarVariacaoFilho($pai_id) {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_variacoes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $idFranquia = $this->session->userdata('id');

            $data = array(
                'pai_id'     => $pai_id,
                'titulo'     => $this->input->post('titulo')
            );
            
            if ($this->produtos_model->add('produto_variacoes', $data) == TRUE) {
                $this->session->set_flashdata('success','Variacao adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/adicionarVariacaoFilho/'.$pai_id);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['view'] = 'produtos/adicionarVariacao';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function adicionarTaxa() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_taxas') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $idFranquia = $this->session->userdata('id');

            $data = array(
                'idFranquia' => $idFranquia,
                'titulo'     => $this->input->post('titulo'),
                'valor'     => $this->input->post('valor')
            );
            
            if ($this->produtos_model->add('produto_taxas', $data) == TRUE) {
                $this->session->set_flashdata('success','Taxa adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/adicionarTaxa/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['view'] = 'produtos/adicionarTaxa';
        $this->load->view('tema/topo', $this->data);
     
    }

    function geraCodigoBarra($numero){
        $fino = 1;
        $largo = 3;
        $altura = 50;
        
        $barcodes[0] = '00110';
        $barcodes[1] = '10001';
        $barcodes[2] = '01001';
        $barcodes[3] = '11000';
        $barcodes[4] = '00101';
        $barcodes[5] = '10100';
        $barcodes[6] = '01100';
        $barcodes[7] = '00011';
        $barcodes[8] = '10010';
        $barcodes[9] = '01010';
        
        for($f1 = 9; $f1 >= 0; $f1--){
            for($f2 = 9; $f2 >= 0; $f2--){
                $f = ($f1*10)+$f2;
                $texto = '';
                for($i = 1; $i < 6; $i++){
                    $texto .= substr($barcodes[$f1], ($i-1), 1).substr($barcodes[$f2] ,($i-1), 1);
                }
                $barcodes[$f] = $texto;
            }
        }
        
        echo '<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>';
        echo '<script type="text/javascript">
              function printDiv(divName) {
                   var printContents = document.getElementById(divName).innerHTML;
                   var originalContents = document.body.innerHTML;

                   document.body.innerHTML = printContents;

                   window.print();

                   document.body.innerHTML = originalContents;
              }
              </script>';
        echo '<div id="codigo_barras" style="display:inline-block; text-align:center;">';
        echo '<img src="'.base_url().'assets/img/codigoBarra_p.gif" width="'.$fino.'" height="'.$altura.'" border="0" />';
        echo '<img src="'.base_url().'assets/img/codigoBarra_b.gif" width="'.$fino.'" height="'.$altura.'" border="0" />';
        echo '<img src="'.base_url().'assets/img/codigoBarra_p.gif" width="'.$fino.'" height="'.$altura.'" border="0" />';
        echo '<img src="'.base_url().'assets/img/codigoBarra_b.gif" width="'.$fino.'" height="'.$altura.'" border="0" />';
        
        echo '<img ';
        
        $texto = $numero;
        
        if((strlen($texto) % 2) <> 0){
            $texto = '0'.$texto;
        }
        
        while(strlen($texto) > 0){
            $i = round(substr($texto, 0, 2));
            $texto = substr($texto, strlen($texto)-(strlen($texto)-2), (strlen($texto)-2));
            
            if(isset($barcodes[$i])){
                $f = $barcodes[$i];
            }
            
            for($i = 1; $i < 11; $i+=2){
                if(substr($f, ($i-1), 1) == '0'){
                    $f1 = $fino ;
                }else{
                    $f1 = $largo ;
                }
                
                echo 'src="'.base_url().'assets/img/codigoBarra_p.gif" width="'.$f1.'" height="'.$altura.'" border="0">';
                echo '<img ';
                
                if(substr($f, $i, 1) == '0'){
                    $f2 = $fino ;
                }else{
                    $f2 = $largo ;
                }
                
                echo 'src="'.base_url().'assets/img/codigoBarra_b.gif" width="'.$f2.'" height="'.$altura.'" border="0">';
                echo '<img ';
            }
        }
        echo 'src="'.base_url().'assets/img/codigoBarra_p.gif" width="'.$largo.'" height="'.$altura.'" border="0" />';
        echo '<img src="'.base_url().'assets/img/codigoBarra_b.gif" width="'.$fino.'" height="'.$altura.'" border="0" />';
        echo '<img src="'.base_url().'assets/img/codigoBarra_p.gif" width="1" height="'.$altura.'" border="0" />';
        echo '</br>';
        echo '<span style="font-family: monospace;font-size: 14px;letter-spacing: 2px;">'.$numero.'</span>';
        echo '</div>';
        echo '<button onclick="printDiv(\'codigo_barras\');">Imprimir</button>';
        echo '</br>';
        echo '<div id="output"></div>';
        echo '<script type="text/javascript">
                    window.onload = function () {
                        function convert(target) {      
                            html2canvas(target, {
                                "proxy": "../html2canvasproxy.php",
                                "logging": true, //Enable log (use Web Console for get Errors and Warnings)
                                "onrendered": function(canvas) {
                                    var img = new Image();
                                        img.onload = function () {
                                            img.onload = null;
                                            document.getElementById("output").appendChild(img);
                                        };
                                        img.src = canvas.toDataURL("image/png");
                                        document.getElementById("codigo_barras").style.display = \'none\';
                                }
                            });
                        }
                        convert(document.getElementById("codigo_barras"));
                    };
              </script>';
    }

    function inserirProdutoAjax(){
        $json = array();
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
            exit;
        }
        $idFranquia=$this->session->userdata('id');
        
        $precoCompra = $this->input->post('precoCompra');
        $precoCompra = str_replace(",","", $precoCompra);
        $precoVenda = $this->input->post('precoVenda');
        $precoVenda = str_replace(",", "", $precoVenda);
        $preco_sugerido = $this->input->post('preco_sugerido');
        $preco_sugerido = str_replace(",", "", $preco_sugerido);
        $data = array(
            'idFranquia' => $idFranquia,
            'pai_id' => !empty($this->input->post('pai_id')) ? (int)$this->input->post('pai_id') : NULL,
            'codigo_barras' => $this->input->post('campo_codigo_barras'),
            'descricao' => $this->input->post('descricao'),
            'codigo_produto' => $this->input->post('codigo_produto'),
            'tipo'    => (int)$this->input->post('tipo'),
            'ligacao'    => $this->input->post('ligacao'),
            'peso'    => $this->input->post('peso'),
            'impostos'    => (int)$this->input->post('impostos'),
            'fornecedor_id'    => (int)$this->input->post('fornecedor_id'),
            'estoque_ideal'    => $this->input->post('estoque_ideal'),
            'unidade' => $this->input->post('unidade'),
            'marcas_id' => $this->input->post('marcas_id'),
            'precoCompra' => $precoCompra,
            'precoVenda' => $precoVenda,
            'estoque' => $this->input->post('estoque'),
            'ncm' => $this->input->post('ncm'),
            'cfop' => $this->input->post('cfop'),
            'estoqueMinimo' => $this->input->post('estoqueMinimo')
        );
        if($this->session->userdata('id') == 4)
            $data['preco_sugerido'] = $preco_sugerido;
        
        $idProduto = $this->produtos_model->addReturnId('produtos', $data);
        if ($idProduto != FALSE) {
            $json = array("result" => true, "idProduto" => $idProduto, "nomeProduto" => $data["descricao"], "preco" => $precoVenda);
            // $json = array("result" => true);
        } else {
            $json = array("result" => false);
        }
        echo json_encode($json);    
    }

    function buscar_codigo(){
        $json = array();
        
        $codigo = $this->input->post('codigo_barras');
        $data = array(
            'codigo_barras' => $codigo
        );
        $produto = $this->produtos_model->buscar_codigo('produtos', $data);
        if ($produto != FALSE) {
            $json = array("result" => true, "produto" => $produto);
        } else {
            $json = array("result" => false);
        }
        echo json_encode($json);    
    }
    
    function editar() {
        $id = (int)$this->uri->segment(3);
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produtos') == false && empty($_POST['modal_ajax'])) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);
            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);
            $preco_sugerido = $this->input->post('preco_sugerido');
            $preco_sugerido = str_replace(",", "", $preco_sugerido);

            $produto = $this->produtos_model->buscar_codigo('produtos', ['idProdutos' => $id]);
            if($produto->idFranquia == 4 && $this->session->userdata('id') != 4){
                $data = array(
                    'idFranquia' => $this->session->userdata('id'),
                    'codigo_barras' => $produto->codigo_barras,
                    'descricao' => $produto->descricao,
                    'tipo'    => (int)$produto->tipo,
                    'ligacao'    => $produto->ligacao,
                    'peso'    => $produto->peso,
                    'impostos'    => (int)$produto->impostos,
                    'fornecedor_id'    => (int)$produto->fornecedor_id,
                    'estoque_ideal'    => $this->input->post('estoque_ideal'),
                    'unidade' => $produto->unidade,
                    'marcas_id' => $produto->marcas_id,
                    'precoCompra' => $precoCompra,
                    'precoVenda' => $precoVenda,
                    'ncm' => $produto->ncm,
                    'cfop' => $this->input->post('cfop'),
                    'estoque' => $this->input->post('estoque'),
                    'estoqueMinimo' => $this->input->post('estoqueMinimo')
                );

                $idProduto = $this->produtos_model->addReturnId('produtos', $data);
               if(!empty($_POST['modal_ajax'])){
                        $json = array("result" => true);
                        echo json_encode($json);
                        exit;
                    }else{
                        $this->session->set_flashdata('success','Produto editado com sucesso!');
                        redirect(base_url() . 'index.php/produtos/editar/'.$id);
                    }
                echo json_encode($json);
            }else{
                $data = array(
                    // 'codigo_barras' => $this->input->post('codigoBarras') ? $this->input->post('codigoBarras') : null,
                    'descricao' => $this->input->post('descricao'),
                    'codigo_produto' => $this->input->post('codigo_produto'),
                    'tipo'    => (int)$this->input->post('tipo'),
                    'ligacao'    => $this->input->post('ligacao'),
                    'peso'    => $this->input->post('peso'),
                    'impostos'    => (int)$this->input->post('impostos'),
                    'fornecedor_id'    => (int)$this->input->post('fornecedor_id'),
                    'estoque_ideal'    => $this->input->post('estoque_ideal'),
                    'unidade' => $this->input->post('unidade'),
                    'marcas_id' => $this->input->post('marcas_id'),
                    'precoCompra' => $precoCompra,
                    'precoVenda' => $precoVenda,
                    'ncm' => $this->input->post('ncm'),
                    'cfop' => $this->input->post('cfop'),
                    'estoque' => $this->input->post('estoque'),
                    'estoqueMinimo' => $this->input->post('estoqueMinimo')
                );
                if($this->session->userdata('id') == 4)
                    $data['preco_sugerido'] = $preco_sugerido;
                
                if ($this->produtos_model->edit('produtos', $data, 'idProdutos', $id) == TRUE) {
                    if(!empty($_POST['modal_ajax'])){
                        $json = array("result" => true);
                        echo json_encode($json);
                        exit;
                    }else{
                        $this->session->set_flashdata('success','Produto editado com sucesso!');
                        redirect(base_url() . 'index.php/produtos/editar/'.$id);
                    }
                } else {
                    if(!empty($_POST['modal_ajax'])){
                        $json = array("result" => false);
                        echo json_encode($json);
                        exit;
                    }else{
                        $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
                    }
                }
            }
        }

        $this->data['variacoes'] = $this->produtos_model->get('produtos','codigo_barras,idProdutos,idFranquia,descricao,unidade,marcas_id,
precoCompra,precoVenda,estoque,estoqueMinimo',['pai_id' => $id],15,0);

        $this->data['taxas'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$this->produtos_model->count('produto_taxas', $this->session->userdata('id')),0);
        $this->data['tipos'] = $this->produtos_model->getTipos('produto_tipos','idTipos, titulo, valor','',$this->produtos_model->count('produto_tipos', $this->session->userdata('id')),0);
        $this->data['variacao'] = $this->produtos_model->getVariacao('produto_variacoes','produto_variacoes.idVariacao, produto_variacoes.titulo, produto_variacoes_rel.id, produto_variacoes_rel.valor, produto_variacoes_rel.qtd, produto_variacoes_rel.peso',['produto_variacoes_rel.id_produto' => $id]);
        $this->data['result'] = $this->produtos_model->getById($id);
        $this->data['view'] = 'produtos/editarProduto';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function editarVariacao() {
        $id = (int)$this->uri->segment(3);
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar tipo.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_variacoes') == false && empty($_POST['modal_ajax'])) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'titulo' => $this->input->post('titulo')
            );
            if ($this->produtos_model->edit('produto_variacoes', $data, 'idVariacao', $id) == TRUE) {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => true);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->session->set_flashdata('success','Produto editado com sucesso!');
                    redirect(base_url() . 'index.php/produtos/editarVariacao/'.$id);
                }
            } else {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => false);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
                }
            }
        }
        $this->data['result'] = $this->produtos_model->getVariacaoById($id);
        $this->data['view'] = 'produtos/editarVariacao';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function editarVariacaoFilho() {
        $id = (int)$this->uri->segment(3);
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar tipo.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_variacoes') == false && empty($_POST['modal_ajax'])) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'titulo' => $this->input->post('titulo')
            );
            if ($this->produtos_model->edit('produto_variacoes', $data, 'idVariacao', $id) == TRUE) {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => true);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->session->set_flashdata('success','Produto editado com sucesso!');
                    redirect(base_url() . 'index.php/produtos/editarVariacaoFilho/'.$id);
                }
            } else {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => false);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
                }
            }
        }
        $this->data['result'] = $this->produtos_model->getVariacaoById($id);
        $this->data['view'] = 'produtos/editarVariacao';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function editarTaxa() {
        $id = (int)$this->uri->segment(3);
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar taxas.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_taxas') == false && empty($_POST['modal_ajax'])) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'titulo' => $this->input->post('titulo'),
                'valor' => $this->input->post('valor')
            );
            if ($this->produtos_model->edit('produto_taxas', $data, 'idTaxas', $id) == TRUE) {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => true);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->session->set_flashdata('success','Taxa editada com sucesso!');
                    redirect(base_url() . 'index.php/produtos/editarTaxa/'.$id);
                }
            } else {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => false);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
                }
            }
        }
        $this->data['result'] = $this->produtos_model->getTaxaById($id);
        $this->data['view'] = 'produtos/editarTaxa';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function editarTipo() {
        $id = (int)$this->uri->segment(3);
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar tipo.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('produto_tipos') == false && empty($_POST['modal_ajax'])) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'titulo' => $this->input->post('titulo')
            );
            if ($this->produtos_model->edit('produto_tipos', $data, 'idTipos', $id) == TRUE) {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => true);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->session->set_flashdata('success','Produto editado com sucesso!');
                    redirect(base_url() . 'index.php/produtos/editarTipo/'.$id);
                }
            } else {
                if(!empty($_POST['modal_ajax'])){
                    $json = array("result" => false);
                    echo json_encode($json);
                    exit;
                }else{
                    $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
                }
            }
        }
        $this->data['result'] = $this->produtos_model->getTipoById($id);
        $this->data['view'] = 'produtos/editarTipo';
        $this->load->view('tema/topo', $this->data);
     
    }
    

    
    function solicitar() {
        $id =  $this->input->post('id_produto');
        $qtd = $this->input->post('qtd');
        $idFranquia=$this->session->userdata('id');

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar produtos.');
           redirect(base_url());
        }

        if(is_array($id) && is_array($qtd)){
            $produtos = '';
            foreach($id as $k => $v){
                $produto = $this->produtos_model->getById($v);
                if(!empty($produto) && !empty($qtd[$k]) && $qtd[$k] != 0){
                    $add_solicitacao = $this->produtos_model->add('produtos_solicitacoes',  array(
                    'franquia_id'          => $idFranquia,
                    'produto_id'           => (int)$v,
                    'qtd'                  => (int)$qtd[$k]));
                    $produtos .= "<tr>
                                    <td style=\"border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important\"><font face=\"Arial, Helvetica, sans-serif\" size=\"2\">".$produto->descricao."</font></td>
                                    <td style=\"border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important\">".$produto->codigo_barras."</td>
                                    <td style=\"border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important\">".$produto->peso."</td>
                                    <td style=\"border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important\">R$".number_format($produto->precoCompra, 2, ',', '.')."</td>
                                    <td style=\"border-top:1px #0088cc solid;padding:10px 15px;font-size:0.9em;text-decoration:none!important\">".$qtd[$k]."</td>
                                </tr>";
                }
            }

            if(!empty($produtos)){
                $this->load->model('franquias_model');
                $this->load->model('sistemaos_model');

                $franquia = $this->franquias_model->getById($idFranquia);
                $central = $this->franquias_model->getById(4);
                $message = $this->load->view('os/template_email_solicitacao_produtos', array(
                'franquia'            => "{$idFranquia} ({$franquia->nome})",
                'emitente'            => $this->sistemaos_model->getEmitente($this->session->userdata('id')),
                'produtos'            => $produtos,
                'facebook'            => $franquia->facebook,
                'ass'                 => $this->franquias_model->obter_ass_email($this->session->userdata('id'))
                ), true);

                $subject = 'Solicitação de estoque';
            }
        }elseif(!is_array($id) && !is_array($qtd)){

            $add_solicitacao = $this->produtos_model->add('produtos_solicitacoes',  array(
            'franquia_id'          => (int)$idFranquia,
            'produto_id'           => (int)$id,
            'qtd'                  => $qtd));

            if($add_solicitacao){
                $this->load->model('franquias_model');
                $this->load->model('sistemaos_model');

                $produto = $this->produtos_model->getById($id);
                $franquia = $this->franquias_model->getById($idFranquia);
                $central = $this->franquias_model->getById(4);
                $message = $this->load->view('os/template_email_solicitacao', array(
                'franquia'            => "{$idFranquia} ({$franquia->nome})",
                'emitente'            => $this->sistemaos_model->getEmitente($this->session->userdata('id')),
                'descricao'           => $produto->descricao,
                'codigo_barras'       => $produto->codigo_barras,
                'peso'       => $produto->peso,
                'preco'               => $produto->precoCompra,
                'qtd'                 => $qtd,
                'facebook'            => $franquia->facebook,
                'ass'                 => $this->franquias_model->obter_ass_email($this->session->userdata('id'))
                ), true);

                $subject = 'Solicitação de estoque: #' . $produto->codigo_barras;
            }

        }
        
        $from = $franquia->email;
        $fromname = $franquia->nome;
        $to = $central->email;
        $toname = $central->nome;
        $this->load->library('email');
        $this->email->from($from, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
        
        echo json_encode(array('success' => 'true'));     
    }

    function visualizar() {
        
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }
        $this->data['result'] = $this->produtos_model->getById($this->uri->segment(3));
        if($this->data['result'] == null){
            $this->session->set_flashdata('error','Produto não encontrado.');
            redirect(base_url() . 'index.php/produtos/editar/'.$this->input->post('idProdutos'));
        }
        $this->data['view'] = 'produtos/visualizarProduto';
        $this->load->view('tema/topo', $this->data);
     
    }
    
    function excluir(){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir produtos.');
           redirect(base_url());
        }
        
        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir produto.');            
            redirect(base_url().'index.php/produtos/gerenciar/');
        }

        $result = $this->produtos_model->getById($id);

        if($result->id_franquia != 4 || $this->session->userdata('id') == 4){
            $this->db->where('produtos_id', $id);
            $this->db->delete('produtos_os');
            $this->db->where('produtos_id', $id);
            $this->db->delete('itens_de_vendas');
            
            $this->produtos_model->delete('produtos','idProdutos',$id);             
            
            $this->session->set_flashdata('success','Produto excluido com sucesso!');            
        }else
            $this->session->set_flashdata('danger','Não é possível excluir um produto padrão.');            

        redirect(base_url().'index.php/produtos/gerenciar/');
    }
    public function autoCompleteMarca(){
        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->produtos_model->autoCompleteMarca($q);
        }
    }
    public function autoCompleteFornecedores(){
        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->produtos_model->autoCompleteFornecedores($q);
        }
    }
}
