<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orcamento extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model(array('franquias_model', 'orcamentos_model'));
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->library('form_validation');
    }  
    public function index() {
        $tipo = $this->input->get('tipo'); 
        $this->data['custom_error'] = '';
        if($this->form_validation->run('orcamento') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {  
            $data = array(
                'nome'       => $this->input->post('nome'),
                'email'      => $this->input->post('email'),
                'telefone'   => $this->limparNumeros($this->input->post('telefone2')),
                'telwhats'   => $this->limparNumeros($this->input->post('telwhats')),
                'marca'      => $this->input->post('marca'),
                'modelo'     => $this->input->post('modelo'),
                'defeito'    => $this->input->post('defeito'),
                'idFranquia' => $this->input->post('franquia'),
                'obs'        => $this->input->post('obs'),
                'tipo'       => $tipo,
                'status'     => 0 // Não Visualizado
            );
            $franquia = $this->franquias_model->getById($data['idFranquia']);
            if(!empty($franquia->id_pai)){
                $data['idFranquia'] = $franquia->id_pai;
                $data['idFilial']   = $franquia->idFranquias;
                // $franquia_pai = $this->franquias_model->getById($franquia->id_pai);
            }
            // $franquias = $this->franquias_model->listar('id_pai = "'.$franquia_pai->idFranquias.'" OR idFranquias = "'.$franquia_pai->idFranquias.'"');
            $this->orcamentos_model->add('orcamentos', $data);
            
            $data['mensagem'] = 'Obrigado por fazer um orçamento conosco! Em breve nossa equipe retornará com mais informações.';
            
            // Dados para o template de e-mail
            $data['facebook']        = $franquia->facebook;
            $data['ass']             = $this->franquias_model->obter_ass_email(!empty($franquia->id_pai)       ? $franquia->id_pai : $franquia->idFranquias);
            $data['email_marketing'] = $this->franquias_model->obter_email_marketing(!empty($franquia->id_pai) ? $franquia->id_pai : $franquia->idFranquias);

            $subject = 'Rede Multi Assistência "[Recebemos o seu pedido]"';

            $this->load->library('email');
            $this->email->from($franquia->email, $franquia->nome);
            $this->email->to($data['email']);
            $this->email->subject($subject);
            $this->email->message($this->load->view('os/template_email_resposta', $data,true));
            $this->email->send();
      
            $this->session->set_flashdata('success', 'Orçamento Realizado com sucesso!<br>Em breve você receberá um e-mail de confirmação.');
            redirect(base_url() . 'index.php/orcamento?tipo=' . $tipo);
        }
    
    
        $marcas = '';
        $defeitos = '';
        switch(strtolower($tipo)) {
            case 'tablet':
            case 'tablets':
                $marcas = 'Apple(iPad),Samsung,Motorola,LG,Sony,Microsoft,Nokia,Asus,Positivo,Genesis,Multilaser,CCE,DL,Outros';
                $defeitos = 'Não liga,Tela Quebrada,Não aparece imagem na tela,Tela com Manchas,Touch Screen Quebrado,Tablet Molhou,Não funciona Wifi,Não Funciona Câmera Frontal,Não Funciona Câmera Traseira,Botão Home,Botão de Ligar,Botões de Volume,Não tem Sinal de Operadora,Não Carrega,Desligando sozinho,Travado,Com erros de aplicativo,Não reconhece Chip,Descarregando rápido,Eu não escuto em ligações,Não me escutam em ligações,Não toca músicas,Não Funciona Viva Voz,Outros';
                break;
            case 'celular':
            case 'celulares':
                $marcas = 'Apple Iphone,Asus,Alcatel,Blu,Cce,LG,Motorola Lenovo,Nokia,Microsoft,Sony,Samsung,Windows Phone,Xiaomi,ZTE,Outros';
                $defeitos = 'Não liga,Tela Quebrada,Não aparece imagem na tela,Tela com Manchas,Touch Screen Quebrado,Celular Molhou,Não funciona Wifi,Não Funciona Câmera Frontal,Não Funciona Câmera Traseira,Botão Home,Botão de Ligar,Botões de Volume,Não tem Sinal de Operadora,Não Carrega,Desligando sozinho,Travado,Com erros de aplicativo,Não reconhece Chip,Descarregando rápido,Eu não escuto em ligações,Não me escutam em ligações,Não toca músicas,Não Funciona Viva Voz,Outros';
                break;
            case 'Iphone':
            case 'iphone':
            case 'IPHONE':
                $marcas = 'Apple Iphone';
                $defeitos = 'Não liga,Tela Quebrada,Não aparece imagem na tela,Tela com Manchas,Touch Screen Quebrado,Celular Molhou,Não funciona Wifi,Não Funciona Câmera Frontal,Não Funciona Câmera Traseira,Botão Home,Botão de Ligar,Botões de Volume,Não tem Sinal de Operadora,Não Carrega,Desligando sozinho,Travado,Com erros de aplicativo,Não reconhece Chip,Descarregando rápido,Eu não escuto em ligações,Não me escutam em ligações,Não toca músicas,Não Funciona Viva Voz,Outros';
                break;
            case 'notebook':
            case 'notebooks':
                $marcas = 'Apple(Mac),Sony,Samsung,Itautec,DELL,HP,Asus,Positivo,Acer,Lenovo,LG,CCE,STI,Outros';
                $defeitos = 'Tela quebrada,Não aparece imagem na tela,Formatação / Atualização,Travado,Não Inicia,Tela Preta / Azul,Reparo de placa mãe / BGA,Muito Lento,Remoção de Vírus,Reiniciando Sozinho,Esquentando Demais,Problemas na Bateria,Problemas no Carregador,Problemas no Teclado,Carcaça quebrada,Outros';
                break;
            case 'mac':
            case 'Mac':
            case 'iMac':
            case 'imac':
                $marcas = 'Apple(Mac/iMac)';
                $defeitos = 'Tela quebrada,Não aparece imagem na tela,Formatação / Atualização,Travado,Não Inicia,Tela Preta / Azul,Reparo de placa mãe / BGA,Muito Lento,Remoção de Vírus,Reiniciando Sozinho,Esquentando Demais,Problemas na Bateria,Problemas no Carregador,Problemas no Teclado,Carcaça quebrada,Outros';
                break;
            case 'videogame':
            case 'videogames':
                $marcas = 'Sony(Playstation),Microsoft(Xbox),Nintendo (Wii),Sony(Portátil),Nintendo(Portátil)';
                $defeitos = 'Não Liga,Tela Quebrada,Desbloqueio,Atualização,Fonte de Energia com Problema,Travando ou Lento,Aquecendo demais,Não reconhece jogos,Luz Vermelha / Verde,Desligando sozinho,Não reconhece Controle,Outros';
                break;
            case 'tv':
            case 'TV':
            case 'tvs':
                $marcas = 'LG,Samsung,Philips,Sony,Panasonic,Sharp,Buster,Semp Toshiba,Toshiba,Hitachi,Philco,Aiwa,AOC,Pioneer,Fujitsu,Philips,RCA,JVC,Bosch,Sanyo,TCL,Outras';
                $defeitos = 'Tela manchada,Aparelho molhou,Não funciona WI-FI,Imagem travada,C. Remoto não funciona,Não acessa Internet,Não funciona função smart,Touch não funciona,Outros';
                break;
            case 'loggi':
                $marcas   = 'Apple Iphone,Asus,Alcatel,Blu,Cce,LG,Motorola Lenovo,Nokia,Microsoft,Sony,Samsung,Windows Phone,Xiaomi,ZTE,Outros';
                //$marcas   .= 'Apple Iphone';
                $defeitos = 'Não liga,Tela Quebrada,Não aparece imagem na tela,Tela com Manchas,Touch Screen Quebrado,Celular Molhou,Não funciona Wifi,Não Funciona Câmera Frontal,Não Funciona Câmera Traseira,Botão Home,Botão de Ligar,Botões de Volume,Não tem Sinal de Operadora,Não Carrega,Desligando sozinho,Travado,Com erros de aplicativo,Não reconhece Chip,Descarregando rápido,Eu não escuto em ligações,Não me escutam em ligações,Não toca músicas,Não Funciona Viva Voz,Outros';
                //$defeitos = 'Não liga,Tela Quebrada,Não aparece imagem na tela,Tela com Manchas,Touch Screen Quebrado,Celular Molhou,Não funciona Wifi,Não Funciona Câmera Frontal,Não Funciona Câmera Traseira,Botão Home,Botão de Ligar,Botões de Volume,Não tem Sinal de Operadora,Não Carrega,Desligando sozinho,Travado,Com erros de aplicativo,Não reconhece Chip,Descarregando rápido,Eu não escuto em ligações,Não me escutam em ligações,Não toca músicas,Não Funciona Viva Voz,Outros';
                break;
            default:
                redirect("http://redemultiassistencia.com.br/orcamento-$tipo/");
        }
        $this->data['tipo'] = $tipo;
        $this->data['marcas'] = explode(',', $marcas);
        $this->data['defeitos'] = explode(',', $defeitos);
        $this->data['franquias'] = $this->franquias_model->getByStatusOrcamento(1, $tipo);
        $this->load->view('orcamento/orcamento', $this->data);
    }


    function limparNumeros($num){
        $num1 = str_replace("(", "", $num);
        $num2 = str_replace(")", "", $num1);
        $num3 = str_replace("-", "", $num2);
        $num4 = str_replace(" ", "", $num3);
        return $num4;
    }
    

    function GetStatusLabelOrcamento($status, $str = "")
    {
        switch($status) {
            case 0: // Não Visualizado
                return "<span class=\"label label-warning\">Não Visualizado</span>{$str}";
            case 1: // Visualizado
                return "<span class=\"label label-info\">Visualizado</span>{$str}";
            case 2: // Respondido
                return "<span class=\"label label-success\">Respondido</span>{$str}";
            case 3: // Comprovante
                return "<span class=\"label label-info\">Comprovante recebido</span>{$str}";
            case 4: // Cartão de crédito
                return "<span class=\"label label-info\">Novo pagamento</span>{$str}";
            case 5: // Cartão de crédito
                return "<span class=\"label label-danger\">Respondido fora do tempo</span>{$str}";
            default:
                return "<span class=\"label label-reverse\">' . $status . '</span>{$str}";
        }
    }

    function getlists(){

        $columns = array(
                0 => 'idOrcamento',
                1 => 'nome',
                2 => 'telefone',
                3 => 'tipo',
                4 => 'data',
                5 => 'status',
                6 => 'dataResposta',
                7 => 'filial',
                8 => 'unidade',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->orcamentos_model->count_totalOrcamentos();


        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->orcamentos_model->allOrcamento($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->orcamentos_model->idOrcamento_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->orcamentos_model->orcamento_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

            // calculo para tempo de reposta de status, igual ao anterior
            
            $where = array(
                'orcamento_id' => $post->idOrcamento,
                'status !='    => '0',
                'status !='    => '1',
                'status !='    => '2'
            );


            $this->db->where($where);
            $this->db->from('pagamentos');
            $this->db->limit(1);
            $pagamentos = $this->db->get('franquias')->result();

            $date1= strtotime($post->data);
            $date2= strtotime($post->dataResposta);
            $tempo  = abs( $date2 - $date1 ) / 60; 

            $str = "";
            if($tempo > 30 && $post->status == 2){
                $post->status = 5;
                if($tempo > 120 && $post->status == 2){
                    $str = "<br>Mais de 2 horas de atraso";
                }else
                    $str = "<br>".floor($tempo-30)." minutos de atraso";
            }

           // $status = $CI->GetStatusLabelOrcamento($r->status, $str);
            $status = $this->GetStatusLabelOrcamento($post->status, $str);
 
            if(!empty($pagamentos)){
                $pag_status = array(
                    3 => '<span class="label label-success">Pagamento aprovado</span>',
                    4 => '<span class="label label-danger">Pagamento recusado</span>',
                    5 => '<span class="label label-info">Pagamento Devolvido</span>',
                    6 => '<span class="label label-warning" style="background: red;">Pagamento Contestado</span>',
                    7 => '<span class="label label-danger" style="background: red;">Pagamento Cancelado</span>',
                    9 => '<span class="label label-info">Pagamento Expirado</span>'
                );

                if(isset($pag_status[$pagamentos[0]->status])){
                    if($r->status == 4)
                        $status = '<span class="label label-info">Novo pagamento</span>';

                    $status .= $pag_status[$pagamentos[0]->status];
                }

            }

            if(!empty($post->comprovante_pagamento)){
                $status = str_replace('<span class="label label-info">Comprovante recebido</span>', '', $status);
                $status .= '<span class="label label-info">Comprovante recebido</span>';
            }
            // END calculo para tempo de reposta de status, igual ao anterior

                $nestedData['idOrcamento'] = $post->idOrcamento;
                $nestedData['nome'] = ucfirst($post->nome).($post->telwhats != null ? '<br/><label class="label label-success">WhatsApp Informado.</label>' : '' );
                $nestedData['telefone'] = ($post->telefone == null ? $post->telwhats : $post->telefone);
                $nestedData['tipo'] = $post->tipo;
                $nestedData['data'] = date('d/m/Y - h:i:s',strtotime($post->data)); 
                $nestedData['status'] = $status;
                $nestedData['dataResposta'] = !empty($post->dataResposta) ? date('d/m/Y - h:i:s',strtotime($post->dataResposta)) : ''; 
                $nestedData['filial'] = $post->as_filial;
                $nestedData['unidade'] =$post->franquianome;
               
                // adicionando as funções do botões de ações
                $buttons = ' ';

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'vOrcamento')){
                $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/os/visualizarOrcamento/'.$post->idOrcamento.'" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
            }

            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }
    


    
}