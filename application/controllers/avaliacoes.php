<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avaliacoes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("avaliacoes_model");
        $this->load->model("os_model");
        $this->data['menuFeedbacks'] = 'Vendas';
    }

    public function index() {
        $countrows = $this->avaliacoes_model->count('avaliacoes', $this->session->userdata('id'));
        $this->data['results'] = $this->avaliacoes_model->get('avaliacoes', 'os.idOs, c.nomeCliente, idAvaliacao, avaliacoes.nota, avaliacoes.obs, avaliacoes.data', '', $countrows, $this->uri->segment(3));
        $this->data['view'] = 'avaliacoes/avaliacoes';
        $this->load->view('tema/topo', $this->data);
    }

    public function avaliar() {

        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $key = $this->input->get_post('key');

        if (empty($key)) {
            show_404();
        }

        $idOs = $this->encrypt->decode(base64_decode($key));

        $this->data['custom_error'] = '';
        if($this->form_validation->run('avaliacao') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $idAvaliacao = $this->input->post('idAvaliacao');
            if (empty($idAvaliacao)) {
                $data = array(
                    'nota' => $this->input->post('nota'),
                    'obs' => $this->input->post('obs'),
                    'os' => $idOs
                );
                $this->avaliacoes_model->add('avaliacoes', $data);
            } else {
                $data = array(
                    'nota' => $this->input->post('nota'),
                    'obs' => $this->input->post('obs')
                );
                $this->avaliacoes_model->edit('avaliacoes', $data, "idAvaliacao", $idAvaliacao);
            }

            $this->session->set_flashdata('success', 'Obrigado por nos avaliar. A sua opinião é muito importante para nós!');
            redirect(base_url() . 'index.php/avaliacoes/avaliar?key=' . $key);
        }

        $result = $this->avaliacoes_model->getOsById($idOs);

        if (empty($result)) {
            show_404();
        }

        $this->data['key'] = $key;
        $this->data['result'] = $result;
        $this->load->view("avaliacoes/avaliar", $this->data);
    }

//    public function teste($idos) {
//        $this->load->library('encrypt');
//
//        echo base64_encode($this->encrypt->encode($idos));
//    }

}
