<?php

class Fornecedores extends CI_Controller {
    
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
            if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('sistemaos/login');
            }
            $this->load->helper(array('codegen_helper'));
            $this->load->model('fornecedores_model','',TRUE);
            $this->data['menuFornecedores'] = 'fornecedores';
	}	
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vFornecedor')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar fornecedores.');
           redirect(base_url());
        }
	        $countrows = $this->fornecedores_model->count('fornecedores');
	    $this->data['results'] = $this->fornecedores_model->get('fornecedores',
	    							    'idFornecedores,idFranquia,nomeFornecedor,documento,responsavel,telefone,celular,email,site,rua,numero,complemento,bairro,cidade,estado,cep,descritivo,obs,fixo',
	    							    '',
	    							    $countrows,$this->uri->segment(3));
       	
       	$this->data['view'] = 'fornecedores/fornecedores';
       	$this->load->view('tema/topo',$this->data);
	  
       
		
    }
	
    function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aFornecedor')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar fornecedores.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('fornecedores') == false)
        {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        }
        else
        {
            $data = array(
                'idFranquia' => $this->session->userdata('id'),
                'nomeFornecedor' => set_value('nomeFornecedor'),
                'documento' => set_value('documento'),
                'responsavel' => set_value('responsavel'),
                'telefone' => set_value('telefone'),
                'celular' => $this->input->post('celular'),
                'email' => set_value('email'),
                'site' => set_value('site'),
                'rua' => set_value('rua'),
                'numero' => set_value('numero'),
                'complemento' => set_value('complemento'),
                'bairro' => set_value('bairro'),
                'cidade' => set_value('cidade'),
                'estado' => set_value('estado'),
                'cep' => set_value('cep'),
                'descritivo' => set_value('descritivo'),
                'obs' => set_value('obs'),
                'fixo' => set_value('fixo'),
                'dataCadastro' => date('Y-m-d')
            );

            if ($this->fornecedores_model->add('fornecedores', $data) == TRUE)
            {
                $this->session->set_flashdata('success','Fornecedor adicionado com sucesso!');
                redirect(base_url() . 'index.php/fornecedores/adicionar/');
            }
            else
            {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'fornecedores/adicionarFornecedor';
        $this->load->view('tema/topo', $this->data);

    }


    function editar() {

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }


        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eFornecedor')){
           $this->session->set_flashdata('error','Você não tem permissão para editar fornecedores.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('fornecedores') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'idFranquia' => $this->session->userdata('id'),
                'nomeFornecedor' => $this->input->post('nomeFornecedor'),
                'documento' => $this->input->post('documento'),
                'responsavel' => $this->input->post('responsavel'),
                'telefone' => $this->input->post('telefone'),
                'celular' => $this->input->post('celular'),
                'email' => $this->input->post('email'),
                'site' => $this->input->post('site'),
                'rua' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'complemento' => $this->input->post('complemento'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
                'cep' => $this->input->post('cep'),
                'descritivo' => $this->input->post('descritivo'),
                'obs' => $this->input->post('obs'),
                'fixo' => $this->input->post('fixo')
            );

            if ($this->fornecedores_model->edit('fornecedores', $data, 'idFornecedores', $this->input->post('idFornecedores')) == TRUE) {
                $this->session->set_flashdata('success','Fornecedor editado com sucesso!');
                redirect(base_url() . 'index.php/fornecedores/editar/'.$this->input->post('idFornecedores'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }


        $this->data['result'] = $this->fornecedores_model->getById($this->uri->segment(3));
        $this->data['view'] = 'fornecedores/editarFornecedor';
        $this->load->view('tema/topo', $this->data);

    }

    public function visualizar(){

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vFornecedor')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar fornecedores.');
           redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->data['result'] = $this->fornecedores_model->getById($this->uri->segment(3));
        $this->data['results'] = $this->fornecedores_model->getOsByFornecedor($this->uri->segment(3));
        $this->data['view'] = 'fornecedores/visualizar';
        $this->load->view('tema/topo', $this->data);
    }

	
    public function excluir(){

            
            if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dFornecedor')){
               $this->session->set_flashdata('error','Você não tem permissão para excluir fornecedores.');
               redirect(base_url());
            }

            
            $id =  $this->input->post('id');
            if ($id == null){

                $this->session->set_flashdata('error','Erro ao tentar excluir fornecedor.');            
                redirect(base_url().'index.php/fornecedores/gerenciar/');
            }

            //$id = 2;
            // excluindo OSs vinculadas ao fornecedor
            $this->db->where('fornecedores_id', $id);
            $os = $this->db->get('os')->result();

            if($os != null){

                foreach ($os as $o) {
                    $this->db->where('os_id', $o->idOs);
                    $this->db->delete('servicos_os');

                    $this->db->where('os_id', $o->idOs);
                    $this->db->delete('produtos_os');


                    $this->db->where('idOs', $o->idOs);
                    $this->db->delete('os');
                }
            }

            // excluindo Vendas vinculadas ao fornecedor
            $this->db->where('fornecedores_id', $id);
            $vendas = $this->db->get('vendas')->result();

            if($vendas != null){

                foreach ($vendas as $v) {
                    $this->db->where('vendas_id', $v->idVendas);
                    $this->db->delete('itens_de_vendas');


                    $this->db->where('idVendas', $v->idVendas);
                    $this->db->delete('vendas');
                }
            }

            //excluindo receitas vinculadas ao fornecedor
            $this->db->where('fornecedores_id', $id);
            $this->db->delete('lancamentos');



            $this->fornecedores_model->delete('fornecedores','idFornecedores',$id); 

            $this->session->set_flashdata('success','Fornecedor excluido com sucesso!');            
            redirect(base_url().'index.php/fornecedores/gerenciar/');
    }
}

