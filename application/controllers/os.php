<?php
class Os extends CI_Controller
{
    /**  
     * author: Ramon Silva
     * email: silva018-mg@yahoo.com.br
     *  
     */   
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
         
        // echo "<pre>";
        // print_r($this->session->all_userdata());
        // echo "</pre>";
        // exit; 

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('os_model', '', TRUE);
        $this->load->model('franquias_model', '', TRUE);
        $this->load->model('pagamentos_model','',TRUE);
        $this->load->model('orcamentos_model', '', TRUE);

        $redirect = false;
        if(strpos(@current_url(), '/os/orcamento'))
            $redirect = true;

        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $franquia = $this->db->get('franquias')->row();
        $this->pagamentos_model->verify($franquia, false, $redirect);
    
    }
    function index()
    {
        $this->gerenciar();
    }
    
    function gerenciar()
    {
        $countrows = $this->os_model->count('os', $this->session->userdata('id'));
        $this->data['results'] = $this->os_model->get('os', 'idOs,idFranquias,dataInicial,dataFinal,garantia,solicitou_garantia,descricaoProduto,visita,backup,os.status,observacoes,laudoTecnico', '', $countrows, $this->uri->segment(3));
        $this->data['menuOs'] = 'os';
        $this->data['view'] = 'os/os';
        $this->load->view('tema/topo', $this->data); 
    }
    
    function obs_addFoto()
    {
        $this->data['iframe'] = true;
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['view']   = 'os/fotosObs';
        $this->load->view('tema/topo', $this->data); 
    }

    function getlists(){
        $columns = array(
                0 => 'idOs',
                1 => 'nomeCliente',
                2 => 'nomeTecnico',
                3 => 'nomeAtendente',
                3 => 'equipamento',
                4 => 'dataInicial',
                5 => 'dataFinal',
                6 => 'status',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->os_model->count_totalOS();

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->os_model->allOs($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->os_model->idOs_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->os_model->os_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['idOs'] = $post->idOs;
                $nestedData['nomeCliente'] = $post->nomeCliente.($post->solicitou_garantia == 1 ? ' <span class="label label-info">Solicitou garantia</span>' : ''); 
                $nestedData['nomeTecnico'] = $post->nomeTecnico; 
                $nestedData['nomeAtendente'] = $post->nomeAtendente; 
                $nestedData['equipamento'] = $this->os_model->tipoEquipamentoOs($post->idOs); 
                $nestedData['dataInicial'] = date('d/m/Y',strtotime($post->dataInicial)); 
                $nestedData['dataFinal'] = date('d/m/Y',strtotime($post->dataFinal)); 
                //$nestedData['diafinal'] = date('d',strtotime($post->dataFinal)); 
                //$nestedData['mesfinal'] = date('m',strtotime($post->dataFinal)); 
                //$nestedData['anofinal'] = date('Y',strtotime($post->dataFinal)); 
                $nestedData['btnwhats'] = $this->btnwhatsresp($post->idClientes,$post->status); 
                $arr_status   = $this->os_model->getBeforeStatus($post->idOs);
                $status       = $this->GetStatusLabel($post->status);
                if(!empty($arr_status)):
                    $status = array();
                    foreach ($arr_status as $s):
                        $status[] = $this->GetStatusLabel($s->status);
                    endforeach;
                    $status = implode('<i class="icon-arrow-left" style="padding: 0 6px;margin-left: -4px;"></i>', $status);
                endif;
                $nestedData['status'] = $status; 
                // adicionando as funções do botões de ações
                $buttons = ' ';

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){
                $nestedData['butaoV'] = $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/os/visualizar/'.$post->idOs.'" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
            }

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                $nestedData['butaoE'] = $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/os/editar/'.$post->idOs.'" class="btn btn-info tip-top" title="Editar OS"><i class="icon-pencil icon-white"></i></a>';
            }

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                $nestedData['butaoStatus'] = $buttons = $buttons.'<a style="margin-right: 1%" href="#modal-alterar-status" role="button" data-toggle="modal" os="'.$post->idOs.'" data-status="'.$post->status.'" class="btn btn-warning tip-top" title="Alterar Status"><i class="icon-list icon-white"></i></a> <input type="hidden" id="clienteid" name="id2" value="'.$post->clientes_id.'" />';
            }

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'dOs')){
                $nestedData['butaoD'] = $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" os="'.$post->idOs.'" class="btn btn-danger tip-top" title="Excluir OS"><i class="icon-remove icon-white"></i></a>';
            }

            if ($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                $nestedData['butaoG'] = $buttons = $buttons.'<a style="margin-right: 1%" href="'.($post->solicitou_garantia == 1 ? "javascript:;" : base_url().'index.php/os/solicitar_garantia/'.$post->idOs).'" class="btn btn-info tip-top" title="Solicitar garantia" '.($post->solicitou_garantia == 1 ? "disabled" : "").'><i class="icon-star icon-white"></i></a>';
            }

            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    // verificando dados do cleinte para habilitar o botão de resposta whatsapp
    function btnwhatsresp($idcliente, $status){
        //return $result = $idcliente.' / '.$status;
        $result = $this->os_model->btndadosClientes($idcliente);
        foreach ($result as $value) {
        
            switch ($status) {
                case 'Orçamento':
                    $emailMsg = "Obrigado, por escolher nossa Assistência Técnica!\nSeu aparelho será analisado por um técnico e logo receberá um orçamento em seu email ou por telefone.\nSe precisar entre em contato conosco, nossos dados de contato estão na ordem de serviço que recebeu em nossa loja.\n\nObrigado pela confiança!";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                break;

                case 'Aguardando Aprovacao':
                    $emailMsg = "<strong>Olá, temos boas noticias</strong>!\nLigue diretamente para o número informado em sua ordem de serviço.\nQuanto mais breve aprovar seu orçamento, mais rápido receberá ele devidamente consertado.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                 case 'Aguardando pagamento online':
                    $emailMsg = "Olá,\nVocê solicitou um orçamento com pagamento online, faça o pagamento para que nós possamos dar início ao serviço.\nQuanto mais breve efetuar o pagamento, mais rápido receberá ele devidamente consertado.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;
                case 'Aguardando Pecas':
                    $emailMsg = "Olá,\nEsta tudo caminhando conforme combinamos!\nEstamos aguardando a peça de reparo, logo será efetuado o serviço e iremos enviar um email informando.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Em Andamento':
                    $emailMsg = "Olá, começamos a efetuar os consertos necessários em seu aparelho!\nO prazo de entrega está descriminado em sua ordem de serviço. Se acontecer algum imprevisto ou se ficar pronto antes, iremos avisar por e-mail ou ligação.\nVolto a lembrar que, qualquer dúvida pode ligar diretamente no contato telefônico informado em sua ordem de serviço.";
                    $smsMsg = $sms['EmAndamento'];
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;
                case 'Faturando':
                    $emailMsg = "Faturando";
                    $smsMsg = $sms['Faturando'];
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Finalizado':
                    $emailMsg = "Conseguimos! \o/ ;) \nSeu equipamento está pronto, o conserto foi realizado com sucesso. \nJá está disponível para retirada, estamos te esperando!\nQualquer dúvida ligue para nossa loja ou responda este email.\n";
                    $emailMsg .= '<a href="'.base_url() . 'index.php/sistemaos/cliente_pagamento/'.$os.'" class="btn btn-success">Efetuar pagamento</a>';
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;
                
                case 'Conserto Nao Realizado':
                    $emailMsg = "Olá, infelizmente o conserto do seu equipamento não foi possível de ser realizado por nossa equipe.\nPor favor, entre em contato com nossa loja e iremos explicar o que aconteceu."; 
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Saiu para a entrega':
                    $emailMsg = "Seu equipamento saiu para ser entregue, conforme combinado."; 
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Nao Aprovado':
                    $emailMsg = "<strong>Seu serviço não foi aprovado!</strong>\nSua opinião é muito importante, por favor informe o motivo da não aprovação. Talvez possamos ajudar em algo mais.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Cancelado':
                    $emailMsg = "Seu serviço foi cancelado!\nSua opinião é muito importante, por favor informe o motivo do seu cancelamento. Talvez possamos ajudar em algo mais.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Aprovado':
                    $emailMsg = "Agradecemos por aprovar o serviço solicitado para o reparo do seu equipamento. Qualquer dúvida ligue para os números de contato que constam em sua ordem de serviços.";           
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Guardar Aparelho':
                    $emailMsg = "Seu aparelho por medida de espaço e por segurança foi guardado em local separado dos demais. Lembramos que o aparelho não retirado no prazo de até 90 dias após ser entregue na loja, será cobrado um taxa de R$ 25,00(vinte e cinco reais) ao mês a título de seguro. ";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

                case 'Entregue Para o Cliente':
                       
                    //// GERA NÚMERO ALEATÓRIO PARA FEEDBACK ID
                    //$feedbackid = strtoupper(dechex(rand(1000, 9999) . time() . rand(1000, 9999)));
                    //// ADICIONA NO BANCO DE DADOS O ID
                    //if (DAO::Add('feedbacks', "`idFeedbacks`, `status`, `cliente_id`, `idFranquia`, `os`", "'" . $feedbackid . "', '0','" . $clienteid . "', '" . $idFranquia . "', '" . $os . "'") == null){
                    //    return; // EM CASO DE FALHA AO ADD NO BANCO DE DADOS DO FEEDBACK TERMINA AQUI
                    //}
                    //$this->load->library("encrypt");
                    //
                    //$key = base64_encode($this->encrypt->encode($os));
                    //// GERA URL DE RESPOSTA ID
                    //$feedbacklink = base_url() . 'index.php/avaliacoes/avaliar?key=' . $key;
                    //$emailMsg = "O seu equipamento foi entregue... \nQualquer dúvida ligue para nossa loja ou responda este email.\n\nPor favor, avalie o nosso serviço: <a href='".$feedbacklink."'>click aqui!</a>";
                    
                    $emailMsg = "O seu equipamento foi entregue... \nQualquer dúvida ligue para nossa loja ou responda este email.";
                    if ($value->celular) {
                        return $btnresp = '<a href="https://api.whatsapp.com/send?phone=55'.$value->celular.'&amp;text='.$emailMsg.'" target="_blank">
                                        <label class="alert alert-success">Whatsapp</label>
                                    </a>';
                    }else{
                        return $btnresp = '<label class="alert alert-danger">Nº não informado.</label>';
                    }
                    break;

            } // end switch


        }// end foreach

    }
    
    function pendentes()
    {
//        $this->data['results'] = $this->os_model->getByStatusDif('os', $this->session->userdata('id'), 'status != ', 'Finalizado', 'status != ', 'Cancelado');
        $this->data['results'] = $this->os_model->getStatusNotIn(array('Finalizado', 'Cancelado', 'Faturado', 'Nao Aprovado'));
        $this->data['menuOs'] = 'pendentes';
        $this->data['view'] = 'os/pendentes';
        $this->load->view('tema/topo', $this->data);
    }
    function finalizadas()
    {
//        $this->data['results'] = $this->os_model->getByStatusOr('os', $this->session->userdata('id'), 'status', 'Finalizado', 'status', 'Cancelado');
        $this->data['results'] = $this->os_model->getStatusIn(array('Finalizado', 'Cancelado', 'Faturado', 'Nao Aprovado'));
        $this->data['menuOs'] = 'finalizadas';
        $this->data['view'] = 'os/finalizadas';
        $this->load->view('tema/topo', $this->data);
    }
    
    function aberto()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Aberto'));
        $this->data['menuOs'] = 'aberto';
        $this->data['view'] = 'os/aberto';
        $this->load->view('tema/topo', $this->data);
    }
    
    function orcamento()
    {

        $this->data['results'] = $this->os_model->getStatusIn(array('Orçamento'));
        $this->data['menuOs'] = 'orcamento';
        $this->data['view'] = 'os/orcamento';
        $this->load->view('tema/topo', $this->data);
    }
    
    function emAndamento()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Em Andamento'));
        $this->data['menuOs'] = 'emAndamento';
        $this->data['view'] = 'os/emAndamento';
        $this->load->view('tema/topo', $this->data);
    }

    function osAprovados(){
        $this->data['results'] = $this->os_model->getStatusIn(array('Aprovado'));
        $this->data['menuOs'] = 'aprovado';
        $this->data['view'] = 'os/aprovado';
        $this->load->view('tema/topo', $this->data);
    }
    
    function osEntregueParaCliente(){
        $this->data['results'] = $this->os_model->getStatusIn(array('Entregue Para o Cliente'));
        $this->data['menuOs'] = 'entregueParaCliente';
        $this->data['view'] = 'os/entregueParaCliente';
        $this->load->view('tema/topo', $this->data);
    }

    function osConsertoNaoRealizado(){
        $this->data['results'] = $this->os_model->getStatusIn(array('Conserto Nao Realizado'));
        $this->data['menuOs'] = 'consertoNaoRealizado';
        $this->data['view'] = 'os/consertoNaoRealizado';
        $this->load->view('tema/topo', $this->data);
    }

    function finalizado()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Finalizado'));
        $this->data['menuOs'] = 'finalizado';
        $this->data['view'] = 'os/finalizado';
        $this->load->view('tema/topo', $this->data);
    }
    
    function aguardandoAprovacao()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Aguardando Aprovacao'));
        $this->data['menuOs'] = 'aguardandoAprovacao';
        $this->data['view'] = 'os/aguardandoAprovacao';
        $this->load->view('tema/topo', $this->data);
    }
    
    function cancelado()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Cancelado'));
        $this->data['menuOs'] = 'cancelado';
        $this->data['view'] = 'os/cancelado';
        $this->load->view('tema/topo', $this->data);
    }
    
    function faturado()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Faturado'));
        $this->data['menuOs'] = 'faturado';
        $this->data['view'] = 'os/faturado';
        $this->load->view('tema/topo', $this->data);
    }
    
    function aguardandoPecas()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Aguardando Pecas'));
        $this->data['menuOs'] = 'aguardandoPecas';
        $this->data['view'] = 'os/aguardandoPecas';
        $this->load->view('tema/topo', $this->data);
    }
    
    function naoAprovado()
    {
        $this->data['results'] = $this->os_model->getStatusIn(array('Nao Aprovado'));
        $this->data['menuOs'] = 'naoAprovado';
        $this->data['view'] = 'os/naoAprovado';
        $this->load->view('tema/topo', $this->data);
    }
    
    function orcamentos()
    {
        $this->data['results'] = $this->os_model->getOrcamentos();
        $this->data['menuOs'] = 'orcamento';
        $this->data['view'] = 'os/orcamentos';
        $this->load->view('tema/topo', $this->data);
    }

    function slotscofre(){
         $this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $this->data['menuOs'] = 'slots';
        $this->data['view'] = 'os/slotscofre';
        $this->load->view('tema/topo', $this->data); 
    }




    function slots()
    {
        $this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $this->data['menuOs'] = 'slots';
        $this->data['view'] = 'os/slots';

        require_once('./custom_classes/slotter/Slotter.php');
        require_once('./custom_functions/functions.php');

        $qtd_pequeno = $franquia->qtd_fileiras_slots_p; // quantidade de linhas de slots pequenos
        $num_slots_p = $franquia->slotsp;

        $qtd_medio   = $franquia->qtd_fileiras_slots_m; // quantidade de linhas de slots médios
        $num_slots_m = $franquia->slotsm;

        $qtd_grande  = $franquia->qtd_fileiras_slots_g; // quantidade de linhas de slots grandes
        $num_slots_g = $franquia->slotsg;

        // programador aqui
        $limite = $franquia->fileiras_slots;

       // $slot = new Slotter($qtd_pequeno, $qtd_medio, $qtd_grande, $num_slots_p, $num_slots_m, $num_slots_g);

        // $slot->reserva_slots_nicho($dimensao = 'pequeno', $qtos_slots = 1, $tipo_de_equip = 'celular', $id_equip = 123);

        $this->load->model('novos_slots_model');
        // echo '<pre>';
        // var_dump($this->novos_slots_model->obterSlots($franquia->idFranquias));
        // $novos_slots_arr = $this->novos_slots_model->obterSlots($franquia->idFranquias);

        // foreach($novos_slots_arr as $key => $novos_slots):
        //     // foreach($novos_slots as $key2 => $novos_slots_x):

        //         var_dump($novos_slots['id']);

        //         if($novos_slots['dimensao'] == 'p'):
        //             // var_dump('pequenooooooooo');
        //             $slot->reserva_slots_nicho($dimensao = 'pequeno', $qtos_slots = 1, $tipo_de_equip = 'celular', $id_equip = 123);
        //         endif;

        //     // endforeach;
        // endforeach;

        //$slot->reserva_slots_nicho($dimensao = 'pequeno', $qtos_slots = 1, $tipo_de_equip = 'celular', $id_equip = 123);

        //$slot->reserva_slots_nicho($dimensao = 'medio', $qtos_slots = 1, $tipo_de_equip = 'tablet', $id_equip = 896);
        //$slot->reserva_slots_nicho($dimensao = 'medio', $qtos_slots = 1, $tipo_de_equip = 'tablet', $id_equip = 897);
        //$slot->reserva_slots_nicho($dimensao = 'medio', $qtos_slots = 1, $tipo_de_equip = 'tablet', $id_equip = 898);

        //$slot->reserva_slots_nicho($dimensao = 'grande', $qtos_slots = 1, $tipo_de_equip = 'tv', $id_equip = 410);
        //$slot->reserva_slots_nicho($dimensao = 'grande', $qtos_slots = 1, $tipo_de_equip = 'computador', $id_equip = 425);

        //$this->data['num_slots_p'] = $num_slots_p;
        //$this->data['num_slots_m'] = $num_slots_m;
        //$this->data['num_slots_g'] = $num_slots_g;
        //$this->data['slot'] = $slot;
        //$this->data['limite'] = $limite;

        // $novos_slots = $this->novos_slots_model->obterSlots($franquia->idFranquias);
        
        // $qtd_pequeno = $franquia->qtd_fileiras_slots_p; // quantidade de linhas de slots pequenos
        // $num_slots_p = $franquia->slotsp;

        // $qtd_medio   = $franquia->qtd_fileiras_slots_m; // quantidade de linhas de slots médios
        // $num_slots_m = $franquia->slotsm;

        // $qtd_grande  = $franquia->qtd_fileiras_slots_g; // quantidade de linhas de slots grandes
        // $num_slots_g = $franquia->slotsg;

        $this->load->view('tema/topo', $this->data); 
    } 

    
    
    public function adicionar()
    {
        if (isset($_GET['eq'])){
            $resultadoeq = $this->data['results'] = $this->os_model->getEquipamentoById($_GET['eq']);
            
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar O.S.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if(!empty($_POST) && $this->form_validation->run('os') == false):

            $this->data['custom_error'] = 'Dados incompletos, verifique os campos com asterisco ou se selecionou corretamente cliente e responsável.';
            // redirect('os/adicionar/?eq='.$_GET['eq']);

        elseif($this->form_validation->run('os')):

            $dataInicial = $this->input->post('dataInicial');
            $dataFinal = $this->input->post('dataFinal');

            try {
                $dataInicial = explode('/', $dataInicial);
                $dataInicial = $dataInicial[2] . '-' . $dataInicial[1] . '-' . $dataInicial[0];
                if ($dataFinal) {
                    $dataFinal = explode('/', $dataFinal);
                    $dataFinal = $dataFinal[2] . '-' . $dataFinal[1] . '-' . $dataFinal[0];
                } else
                    $dataFinal = date('Y/m/d');
            } catch (Exception $e) {
                $dataInicial = date('Y/m/d');
                $dataFinal = date('Y/m/d');
            }

            $idFranquia = $this->session->userdata('id');

            $data = array(
                'dataInicial' => $dataInicial.' '.date('H:i'),
                'idFranquias' => $idFranquia,
                'clientes_id' => $this->input->post('clientes_id'),//set_value('idCliente'),
                'usuarios_id' => $this->input->post('usuarios_id'),//set_value('idUsuario'),
                'atendentes_id' => (int)$this->input->post('atendentes_id'),//set_value('idAtendente'),
                'dataFinal' => $dataFinal,
                'garantia' => $this->input->post('garantia'),
                'descricaoProduto' => $this->input->post('descricaoProduto'),
                'visita_data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('visita_data')))),
                'visita_hora1' => (int)$this->input->post('visita_hora1'),
                'visita_hora2' => (int)$this->input->post('visita_hora2'),
                'visita_valor' => $this->input->post('visita_valor'),
                'backup_modelo' => $this->input->post('backup_modelo'),
                'backup_imei' => $this->input->post('backup_imei'),
                'backup_valor' => $this->input->post('backup_valor'),
                'status' => $this->input->post('status'),
                'observacoes' => $this->input->post('observacoes'),
                'laudoTecnico' => $this->input->post('laudoTecnico'),
                'faturado' => 0
            );

            if (is_numeric($id = $this->os_model->add('os', $data, true))) {

                $eq = $this->input->post('idEquipamentos');
                //$idos = $this->input->post('idOsEquipamento');
                //$eq = $this->GetdadosEspacosSlots($prefix, $idFranquia, $qtd);
                //$eq = $this->os_model->getSlot('p', $idFranquia);

                //var_dump($eq);
                //echo "<p>".$eq."</p>";

                if ($eq != -1) {
                    // verificar porque ainda aqui da erros.
                    //$empty = $this->os_model->getSlotEmpty($eq);
                    $empty = $this->os_model->getDadosSlotEmpty($eq);

                    if ($empty != -1) {

                        $data = array(
                            'equipamentos_id' => $eq,
                            'os_id' => $id,
                            'slot' => $empty['posicao']
                        );
                        // criar no lugar de addInAll outra function para atualizar equipamentos
                        //if ($this->os_model->addInAll('equipamentos_os', $data) == true){
                        if ($this->os_model->addSlotsUpdt('equipamentos_os', $data, $empty) == true){

                            if ($resultadoeq) {
                                //redirect('os/visualizar/' . $id);
                                redirect('os/editar/' . $id . '?tab=2');
                            }
                            
                            $this->ChangeStatus($id, $this->input->post('clientes_id'), $this->input->post('status'));
                            redirect('os/editar/' . $id . '?tab=2');
                           

                        } else {
                            $this->data['custom_error'] = 'Ocorreu um erro. #1';
                            //$this->session->set_flashdata('error', '');
                        }

                    } else {
                        $this->data['custom_error'] = 'Sem espaço para adicionar um equipamento a OS, favor liberar para adicionar.';
                        // $this->session->set_flashdata('error', '');
                    }
                } else {
                    redirect('os/editar/' . $id . '?tab=2');
                    //redirect('os/slots/');
                    //echo "<script>alert('OS criada com sucesso!');</script>";
                    
                    //redirect('os/visualizar/' . $id);
                }
            } else {
                $this->data['custom_error'] = 'Erro na passagem de parâmetros, entre em contato com o seu desenvolvedor.';
                // $this->session->set_flashdata('error', '');
            }
        endif;

        $this->data['menuOs'] = 'adicionar';
        $this->data['view']   = 'os/adicionarOs';
        $this->load->view('tema/topo', $this->data);
    }

    public function solicitar_garantia()
    {
        if (!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))) {
            $this->session->set_flashdata('error', 'Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para solicitar garantia.');
            redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->load->model('sistemaos_model');
        $franquia     = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $result       = $this->os_model->getById($this->uri->segment(3));
        $equipamentos = $this->os_model->getEquipamentos($this->uri->segment(3));
        $produtos     = $this->os_model->getProdutos($this->uri->segment(3));
        $servicos     = $this->os_model->getServicos($this->uri->segment(3));
        $pagamentos   = $this->os_model->getPagamentos($this->uri->segment(3));
        $emitente     = $this->sistemaos_model->getEmitente($this->session->userdata('id'));

        $dataInicial = $this->input->post('dataInicial');
        $dataFinal = $this->input->post('dataFinal');

        $data = array(
            'solicitou_garantia' => 1,
            'solicitou_garantia_os' => $this->uri->segment(3),
            'dataInicial' => $result->dataInicial,
            'idFranquias' => $result->idFranquias,
            'clientes_id' => $result->clientes_id,
            'usuarios_id' => $result->usuarios_id,
            'atendentes_id' => $result->atendentes_id,
            'dataFinal' => $result->dataFinal,
            'garantia' => "90 dias",
            'descricaoProduto' => $result->descricaoProduto,
            'visita_data' => $result->visita_data,
            'visita_hora1' => $result->visita_hora1,
            'visita_hora2' => $result->visita_hora2,
            'visita_valor' => $result->visita_valor,
            'backup_modelo' => $result->backup_modelo,
            'backup_imei' => $result->backup_imei,
            'backup_valor' => $result->backup_valor,
            'status' => $result->status,
            'observacoes' => $result->observacoes,
            'laudoTecnico' => $result->laudoTecnico,
            'faturado' => $result->faturado
        );

        if (is_numeric($id = $this->os_model->add('os', $data, true))) {
            redirect(base_url() . 'index.php/os/editar/'.$id);
        }
    }


    public function editar()
    {
        $express = false; // MODO EXPRESS ADICIONANDO TUDO RAPIDAMENTE VINDO DADOS AO CADASTRAR EQUIPAMENTO
        if (isset($_GET['eq'])) {
            $eq = $_GET['eq'];
            $express = true;
        }

        if (!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))) {
            $this->session->set_flashdata('error', 'Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar O.S.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('os') == false) {

            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);

        } else {

            $dataInicial = $this->input->post('dataInicial');
            $dataFinal = $this->input->post('dataFinal');

            try {
                $dataInicial = explode('/', $dataInicial);
                $dataInicial = $dataInicial[2] . '-' . $dataInicial[1] . '-' . $dataInicial[0];
                $dataFinal = explode('/', $dataFinal);
                $dataFinal = $dataFinal[2] . '-' . $dataFinal[1] . '-' . $dataFinal[0];
            } catch (Exception $e) {
                $dataInicial = date('Y/m/d');
            }

            $data = array(
                'dataInicial' => $dataInicial.' '.date('H:i'),
                'dataFinal' => $dataFinal,
                'garantia' => $this->input->post('garantia'),
                'descricaoProduto' => $this->input->post('descricaoProduto'),
                'visita_data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('visita_data')))),
                'visita_hora1' => (int)$this->input->post('visita_hora1'),
                'visita_hora2' => (int)$this->input->post('visita_hora2'),
                'visita_valor' => $this->input->post('visita_valor'),
                'backup_modelo' => $this->input->post('backup_modelo'),
                'backup_imei' => $this->input->post('backup_imei'),
                'backup_valor' => $this->input->post('backup_valor'),
                'status' => $this->input->post('status'),
                'observacoes' => $this->input->post('observacoes'),
                'laudoTecnico' => $this->input->post('laudoTecnico'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'clientes_id' => $this->input->post('clientes_id'),
                'atendentes_id' => (int)$this->input->post('atendentes_id'),
            );

            if ($this->os_model->edit('os', $data, 'idOs', $this->input->post('idOs')) == TRUE) {

                $this->session->set_flashdata('success', 'Os editada com sucesso!');
                $this->ChangeStatus($this->input->post('idOs'), $data['clientes_id'], $data['status']);
                redirect(base_url() . 'index.php/os/editar/' . $this->input->post('idOs'));

            } else {

                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';

            }

        }

        $this->data['franquia']            = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $this->data['status']              = $this->os_model->getBeforeStatus($this->uri->segment(3), 99);
        $this->data['result']              = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos']            = $this->os_model->getProdutos($this->uri->segment(3));
        $this->data['equipamentos']        = $this->os_model->getEquipamentos($this->uri->segment(3));
        $this->data['servicos']            = $this->os_model->getServicos($this->uri->segment(3));
        $this->data['pagamentos']          = $this->os_model->getPagamentos($this->uri->segment(3));
        $this->data['anexos']              = $this->os_model->getAnexos($this->uri->segment(3));
        $this->data['observacoesInternas'] = $this->os_model->getObservacoesInternas($this->uri->segment(3));
        $this->data['view']                = 'os/editarOs';
        $this->load->view('tema/topo', $this->data);
    }

    function GetSlotMax($prefix)
    {
        return $this->os_model->getSlotMax($prefix);
    }
    function GetSlotData($prefix, $slot)
    {
        return $this->os_model->getSlotData($prefix, $slot);
    }
    function GetStatusLabel($status)
    {
        switch ($status) {
            case 'Aguardando Aprovacao':
            case 'Aguardando Pecas':
                return '<span class="label label-inverse">' . $status . '</span>';
            case 'Em Andamento':
                return '<span class="label label-warning">' . $status . '</span>';
            case 'Faturado':
                return '<span class="label label-info">' . $status . '</span>';
            case 'Finalizado':
                return '<span class="label label-success">' . $status . '</span>';
            case 'Cancelado':
            case 'Nao Aprovado':
                return '<span class="label label-important">' . $status . '</span>';
            // Orçamento
            default:
                return '<span class="label label-reverse">' . $status . '</span>';
        }
    }
    function GetStatusLabelOrcamento($status, $str = "")
    {
        switch($status) {
            case 0: // Não Visualizado
                return "<span class=\"label label-warning\">Não Visualizado</span>{$str}";
            case 1: // Visualizado
                return "<span class=\"label label-info\">Visualizado</span>{$str}";
            case 2: // Respondido
                return "<span class=\"label label-success\">Respondido</span>{$str}";
            case 3: // Comprovante
                return "<span class=\"label label-info\">Comprovante recebido</span>{$str}";
            case 4: // Cartão de crédito
                return "<span class=\"label label-info\">Novo pagamento</span>{$str}";
            case 5: // Cartão de crédito
                return "<span class=\"label label-danger\">Respondido fora do tempo</span>{$str}";
            default:
                return "<span class=\"label label-reverse\">' . $status . '</span>{$str}";
        }
    }
    public function adicionarAjax()
    {
        $this->load->library('form_validation');
        if ($this->form_validation->run('os') == false) {
            $json = array("result" => false);
            echo json_encode($json);
        } else {
            $data = array(
                'dataInicial' => set_value('dataInicial'),
                'clientes_id' => $this->input->post('clientes_id'),//set_value('idCliente'),
                'usuarios_id' => $this->input->post('usuarios_id'),//set_value('idUsuario'),
                'equipamentos_id' => $this->input->post('equipamentos_id'),//set_value('idEquipamento'),
                'dataFinal' => set_value('dataFinal'),
                'garantia' => set_value('garantia'),
                'descricaoProduto' => set_value('descricaoProduto'),
                'visita' => set_value('visita'),
                'backup' => set_value('backup'),
                'status' => set_value('status'),
                'observacoes' => set_value('observacoes'),
                'laudoTecnico' => set_value('laudoTecnico')
            );
            if (is_numeric($id = $this->os_model->add('os', $data, true))) {
                $json = array("result" => true, "id" => $id);
                echo json_encode($json);
            } else {
                $json = array("result" => false);
                echo json_encode($json);
            }
        }
    }
    function getDefeito($arrdef)
    {
        if (strtolower($arrdef[0]) == 'outros' && $arrdef[1])
            return $arrdef[0] . ': ' . $arrdef[1];
        return $arrdef[0];
    }
    public function ChangeStatus($os, $clienteid, $status, $avaliacao)
    {
        switch ($status) {
            //case 'Cancelado':
            case 'Entregue Para o Cliente':
            case 'Liberar Slot':            
                //$this->os_model->deleteListaEquipamentos($os);
                $this->os_model->deleteEquiLiberaSlots($os);
                break;
        }
        $this->StatusUpdate($os, $clienteid, $status, $avaliacao);
    }
    public function StatusUpdate($os, $clienteid, $status, $avaliacao)
    {
        
        $this->os_model->updateStatus($os, $status);
        $idFranquia = $this->session->userdata('id');

        // Conexão com o banco de dados
        {
            include(reisdev('bootstrap'));
            include(reisdev('sms'));
            $array = DAO::Get("mensagens", "`idFranquia`='" . 4 . "'");
                        
            $email = json_decode(utf8_encode($array['email']),true);
                        
            $sms = (array)json_decode($array['sms']);
            $array = DAO::Get("clientes", "`idClientes`='" . $clienteid . "'");
            if ($array == null)
                return;
        }
        // Obtém dados de mensagens
        $emailMsg = '';
        $smsMsg = '';
        {
            switch ($status) {
                case 'Orçamento':
                    $emailMsg = "<strong>Obrigado</strong> por escolher nossa Assistência Técnica!\nSeu aparelho será analisado por um técnico e logo receberá um orçamento em seu email ou por telefone.\nSe precisar entre em contato conosco, nossos dados de contato estão na ordem de serviço que recebeu em nossa loja.\n\nObrigado pela confiança!";
                    break;
                case 'Aguardando Aprovacao':
                    $emailMsg = "<strong>Olá, temos boas noticias</strong>!\nLigue diretamente para o número informado em sua ordem de serviço.\nQuanto mais breve aprovar seu orçamento, mais rápido receberá ele devidamente consertado.";
                    break;

                 case 'Aguardando pagamento online':
                    $emailMsg = "Olá,\nVocê solicitou um orçamento com pagamento online, faça o pagamento para que nós possamos dar início ao serviço.\nQuanto mais breve efetuar o pagamento, mais rápido receberá ele devidamente consertado.";
                    break;
                case 'Aguardando Pecas':
                    $emailMsg = "Olá,\nEsta tudo caminhando conforme combinamos!\nEstamos aguardando a peça de reparo, logo será efetuado o serviço e iremos enviar um email informando.";
                    break;
                case 'Em Andamento':
                    $emailMsg = "Olá, começamos a efetuar os consertos necessários em seu aparelho!\nO prazo de entrega está descriminado em sua ordem de serviço. Se acontecer algum imprevisto ou se ficar pronto antes, iremos avisar por e-mail ou ligação.\nVolto a lembrar que, qualquer dúvida pode ligar diretamente no contato telefônico informado em sua ordem de serviço.";
                    $smsMsg = $sms['EmAndamento'];
                    break;
                case 'Faturando':
                    $emailMsg = "";
                    $smsMsg = $sms['Faturando'];
                    break;
                case 'Finalizado':
                    // GERA NÚMERO ALEATÓRIO PARA FEEDBACK ID
                    //$feedbackid = strtoupper(dechex(rand(1000, 9999) . time() . rand(1000, 9999)));
                    // ADICIONA NO BANCO DE DADOS O ID
                    //if (DAO::Add('feedbacks', "`idFeedbacks`, `status`, `cliente_id`, `idFranquia`, `os`", "'" . $feedbackid . "', '0','" . $clienteid . "', '" . $idFranquia . "', '" . $os . "'") == null)
                    //    return; // EM CASO DE FALHA AO ADD NO BANCO DE DADOS DO FEEDBACK TERMINA AQUI
                    //$this->load->library("encrypt");
                    
                    //$key = base64_encode($this->encrypt->encode($os));
                    // GERA URL DE RESPOSTA ID
                    //$feedbacklink = base_url() . 'index.php/avaliacoes/avaliar?key=' . $key;
                    $emailMsg = "Conseguimos! \o/ ;) \nSeu equipamento está pronto, o conserto foi realizado com sucesso. \nJá está disponível para retirada, estamos te esperando!\nQualquer dúvida ligue para nossa loja ou responda este email.\n";
                    $emailMsg .= '<a href="'.base_url() . 'index.php/sistemaos/cliente_pagamento/'.$os.'" class="btn btn-success">Efetuar pagamento</a>';
                    break;
                
                case 'Conserto Nao Realizado':
                    $emailMsg = "Olá, infelizmente o conserto do seu equipamento não foi possível de ser realizado por nossa equipe.\nPor favor, entre em contato com nossa loja e iremos explicar o que aconteceu."; 
                    break;
                case 'Saiu para a entrega':
                    $emailMsg = "Seu equipamento saiu para ser entregue, conforme combinado."; 
                    break;
                case 'Nao Aprovado':
                    $emailMsg = "<strong>Seu serviço não foi aprovado!</strong>\nSua opinião é muito importante, por favor informe o motivo da não aprovação. Talvez possamos ajudar em algo mais.";
                    break;
                case 'Cancelado':
                    $emailMsg = "Seu serviço foi cancelado!\nSua opinião é muito importante, por favor informe o motivo do seu cancelamento. Talvez possamos ajudar em algo mais.";
                    break;
                case 'Aprovado':
                    $emailMsg = "Agradecemos por aprovar o serviço solicitado para o reparo do seu equipamento. Qualquer dúvida ligue para os números de contato que constam em sua ordem de serviços.";           
                    break;
                case 'Guardar Aparelho':
                    $emailMsg = "Seu aparelho por medida de espaço e por segurança foi guardado em local separado dos demais. Lembramos que o aparelho não retirado no prazo de até 90 dias após ser entregue na loja, será cobrado um taxa de R$ 25,00(vinte e cinco reais) ao mês a título de seguro. ";
                    break;


                case 'Entregue Para o Cliente':
                    if ($avaliacao == "interno") {
                        
                        // GERA NÚMERO ALEATÓRIO PARA FEEDBACK ID
                        $feedbackid = strtoupper(dechex(rand(1000, 9999) . time() . rand(1000, 9999)));
                        // ADICIONA NO BANCO DE DADOS O ID
                        if (DAO::Add('feedbacks', "`idFeedbacks`, `status`, `cliente_id`, `idFranquia`, `os`", "'" . $feedbackid . "', '0','" . $clienteid . "', '" . $idFranquia . "', '" . $os . "'") == null){
                            return; // EM CASO DE FALHA AO ADD NO BANCO DE DADOS DO FEEDBACK TERMINA AQUI
                        }
                        $this->load->library("encrypt");
                        
                        $key = base64_encode($this->encrypt->encode($os));
                        // GERA URL DE RESPOSTA ID
                        $feedbacklink = base_url() . 'index.php/avaliacoes/avaliar?key=' . $key;
                        
                        $emailMsg = "O seu equipamento foi entregue... \nQualquer dúvida ligue para nossa loja ou responda este email.\n\nPor favor, avalie o nosso serviço: <a href='{$feedbacklink}'>click aqui!</a>";
                        
                        break;

                    }else if($avaliacao == "google"){
                        // GERA NÚMERO ALEATÓRIO PARA FEEDBACK ID
                        $feedbackid = strtoupper(dechex(rand(1000, 9999) . time() . rand(1000, 9999)));
                        // ADICIONA NO BANCO DE DADOS O ID
                        if (DAO::Add('feedbacks', "`idFeedbacks`, `status`, `cliente_id`, `idFranquia`, `os`", "'" . $feedbackid . "', '0','" . $clienteid . "', '" . $idFranquia . "', '" . $os . "'") == null){
                            return; // EM CASO DE FALHA AO ADD NO BANCO DE DADOS DO FEEDBACK TERMINA AQUI
                        }
                        // GERA URL DE RESPOSTA ID
                        $feedbacklink = $this->os_model->linkResposta();
                        
                        $emailMsg = "O seu equipamento foi entregue... \nQualquer dúvida ligue para nossa loja ou responda este email.\n\nPor favor, avalie o nosso serviço: <a href='{$feedbacklink}'>click aqui!</a>";
                        
                        break;
                    }
                      
            }
        }
        // Estrutura e envia email
        if ($emailMsg != "") {
            $franquia = $this->franquias_model->getById($idFranquia);
            $OS = $this->os_model->getById($os);
            $message = $this->load->view('os/template_email_resposta', array(
            'mensagem'            => $emailMsg,
            'idOs' => $os,
            'nome'                => $OS->nomeCliente,
            'dataInicial'         => $OS->dataInicial,
            'dataFinal'           => $OS->dataFinal,
            'laudoTecnico'        => $OS->laudoTecnico,
            'descricaoProduto'    => $OS->descricaoProduto,
            'obs'                 => $OS->obs,
            'facebook'            => $franquia->facebook,
            'ass'                 => $this->franquias_model->obter_ass_email($this->session->userdata('id')),
            'email_marketing'     => $this->franquias_model->obter_email_marketing($this->session->userdata('id'))
            ), true);
            $from = $franquia->email;
            $fromname = "Rede Multi Assistência";
            $to = $array['email'];
            $toname = $array['nomeCliente'];
            $subject = 'Atualização de status OS: ' . $os." - ".$status;
            $this->load->library('email');
            $this->email->from($from, $fromname);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }
        // Estrutura e envia sms
//        $tel = '';
//        $type = $array['sms'];
//        if (isset($smsMsg) && !empty($smsMsg) && $type != 0) {
//
//            $subject = 'OS ' . $os . ' ';
//            if ($type == 1 || $type == 3) {
//                $tel = $array['telefone'];
//                if (strlen($tel) < 13)
//                    if (substr($tel, 0, 2) != 55)
//                        $tel = '55' . $tel;
//            }
//
//            if ($type == 2 || $type == 3) {
//                $tel = $array['celular'];
//                if (strlen($tel) < 13)
//                    if (substr($tel, 0, 2) != 55)
//                        $tel = '55' . $tel;
//            }
//
//            if (UpSMS::Enviar($idFranquia, $subject, $tel, $smsMsg) < 1) {
//                error_log("nosms to=" . $tel . ', type=' . $type . ', msg=' . $smsMsg);
//                return;
//            }
//            //error_log("sms to=".$tel.', type='.$type.', msg='.$smsMsg);
//            error_log("sms to=" . $tel . ', type=' . $type);
//        }
    }
    public function visualizar()
    {
        if (!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))) {
            $this->session->set_flashdata('error', 'Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar O.S.');
            redirect(base_url());
        }
        $this->data['custom_error'] = '';
        $this->load->model('sistemaos_model');
        $this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['equipamentos'] = $this->os_model->getEquipamentos($this->uri->segment(3));
        $this->data['produtos'] = $this->os_model->getProdutos($this->uri->segment(3));
        $this->data['servicos'] = $this->os_model->getServicos($this->uri->segment(3));
        $this->data['pagamentos'] = $this->os_model->getPagamentos($this->uri->segment(3));
        $this->data['emitente'] = $this->sistemaos_model->getEmitente($this->session->userdata('id'));
        $this->data['observacoesInternas'] = $this->os_model->getObservacoesInternas($this->uri->segment(3));
        $this->data['view'] = 'os/visualizarOs';
        $this->load->view('tema/topo', $this->data);
    }
    function visualizarOrcamento()
    {
        $id = $this->uri->segment(3);
        if (!$id || !is_numeric($id)) {
            $this->session->set_flashdata('error', 'Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar O.S.');
            redirect(base_url());
        }

        $resultado = $this->orcamentos_model->getById($id);
        if(!empty($resultado->idFilial)){
            $filial = $this->franquias_model->getById($resultado->idFilial);
            $this->data['filial'] = $filial;
        }

        if($resultado->status == 3 || $resultado->status == 4){
            $this->orcamentos_model->updateStatus($id, 2); // Visualizado
        }elseif($resultado->status != 2){
            $this->orcamentos_model->updateStatus($id, 1); // Visualizado
            
            $resultado->status = 2;
        }

        $where = array(
            'orcamento_id' => $id,
            'status !='    => '0',
            'status !='    => '1'
        );


        $pagamentos = $this->planos_model->pagamentos($where);
        if(!empty($pagamentos)){
            $pag_status = array(
                2 => 'Aguardando Aprovação',
                3 => 'Pagamento Aprovado',
                4 => 'Pagamento Recusado',
                5 => 'Pagamento Devolvido',
                6 => 'Pagamento Contestado',
                7 => 'Pagamento Cancelado',
                9 => 'Pagamento Expirado',
                10 => 'Aprovado',
                11 => 'Liberar Slot',
                12 => 'Guardar Aparelho'
                
            );

            if(isset($pag_status[$pagamentos[0]->status])){
                $resultado->pagamento_status = $pag_status[$pagamentos[0]->status];
                $resultado->pagamento_data   = $pagamentos[0]->data;
                $resultado->pagamento_id   = $pagamentos[0]->bcash_id;
                $resultado->pagamento_total   = number_format($pagamentos[0]->total, 2, ',', '.');
            }
        }


        $this->data['result'] = $resultado;
        $this->data['view'] = 'os/visualizarOrcamento';
        $this->load->view('tema/topo', $this->data);
    }
    function excluir()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir O.S.');
            redirect(base_url());
        }
        $id = $this->input->post('id');
        if ($id == null) {
            $this->session->set_flashdata('error', 'Erro ao tentar excluir OS.');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }
        $this->db->where('os_id', $id);
        $this->db->delete('servicos_os');
        $this->db->where('os_id', $id);
        $this->db->delete('produtos_os');
        $this->db->where('os_id', $id);
        $this->db->delete('anexos');
        $this->os_model->delete('os', 'idOs', $id);
        $this->session->set_flashdata('success', 'OS excluída com sucesso!');
        redirect(base_url() . 'index.php/os/gerenciar/');
    }
    
    function alterarStatus(){
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar O.S.');
            redirect(base_url());
        }
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        
        if ($id == null) {
            $this->session->set_flashdata('error', 'Erro ao tentar alterar o status OS.');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }
        
        if($status == null){
            $this->session->set_flashdata('error', 'Selecione um status para alterar a OS.');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }
        if($this->os_model->alterarStatus($status, $id)){
            $os = $this->os_model->getById($id);
            $this->ChangeStatus($id, $os->clientes_id, $os->status, $avaliacao);
            
            $this->session->set_flashdata('success', 'Status da OS alterada com sucesso!');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }else{
            $this->session->set_flashdata('error', 'Erro ao tentar alterar o status OS.');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }
    }
    function excluirOrcamento()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir orçamentos');
            redirect(base_url());
        }
        $id = $this->input->post('id');
        if ($id == null) {
            $this->session->set_flashdata('error', 'Erro ao tentar excluir orcaçmento');
            redirect(base_url() . 'index.php/os/orcamentos/');
        }
        $this->orcamentos_model->delete('orcamentos', 'idOrcamento', $id);
        $this->session->set_flashdata('success', 'Orçamento excluido com sucesso!');
        redirect(base_url() . 'index.php/os/orcamentos/');
    }
    public function autoCompleteProduto()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteProduto($q);
        }
    }
    public function autoCompleteEquipamento()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteEquipamento($q);
        }
    }
    public function autoCompleteCliente()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteCliente($q);
        }
    }
    public function autoCompleteAtendente()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteAtendente($q);
        }
    }
    public function autoCompleteUsuario()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteUsuario($q);
        }
    }
    public function autoCompleteFranquia()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteFranquia($q);
        }
    }
    public function autoCompleteServico()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteServico($q);
        }
    }
    public function adicionarProduto()
    {
        $preco = $this->input->post('preco');
        $quantidade = $this->input->post('quantidade');
        $subtotal = $preco * $quantidade;
        $produto = $this->input->post('idProduto');
        $data = array(
            'quantidade' => $quantidade,
            'subTotal' => $subtotal,
            'produtos_id' => $produto,
            'os_id' => $this->input->post('idOsProduto'),
        );
        if ($this->os_model->add('produtos_os', $data) == true) {
            $sql = "UPDATE produtos set estoque = estoque - ? WHERE idProdutos = ?";
            $this->db->query($sql, array($quantidade, $produto));
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
    function excluirProduto()
    {
        $ID = $this->input->post('idProduto');
        if ($this->os_model->delete('produtos_os', 'idProdutos_os', $ID) == true) {
            $quantidade = $this->input->post('quantidade');
            $produto = $this->input->post('produto');
            $sql = "UPDATE produtos set estoque = estoque + ? WHERE idProdutos = ?";
            $this->db->query($sql, array($quantidade, $produto));
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
    public function adicionarServico()
    {
        $data = array
        (
            'os_id' => $this->input->post('idOsServico'),
            'nomeServico' => $this->input->post('servico'),
            
            'idServico' => (int)$this->input->post('idServico'),
            'precoServico' => $this->input->post('precoServico'),
            'descricaoServico' => $this->input->post('descricaoServico')
        );
        if ($this->os_model->add('servicos_os', $data) == true)
            echo json_encode(array('result' => true));
        else
            echo json_encode(array('result' => false));
    }
    function excluirServico()
    {
        $ID = $this->input->post('idServico');
        if ($this->os_model->delete('servicos_os', 'idServicos_os', $ID) == true)
            echo json_encode(array('result' => true));
        else
            echo json_encode(array('result' => false));
    }
    public function adicionarPagamento()
    {
        $data = array
        (
            'os_id'     => $this->input->post('idOsPagamento'),
            'formaPg'   => $this->input->post('formaPg'),
            'valorPg'   => $this->input->post('valorPg'),
            'valorDesc' => $this->input->post('valorDesc'),
            'data'      => date('Y-m-d', strtotime($this->input->post('data'))).' 00:00:00',
            'dadosPg'   => $this->input->post('dadosPg')
        );
        if ($this->os_model->add('pagamento_os', $data) == true)
            echo json_encode(array('result' => true));
        else
            echo json_encode(array('result' => false));
    }
    function excluirPagamento()
    {
        $ID = $this->input->post('idPagamento');
        if ($this->os_model->delete('pagamento_os', 'idPagamento_os', $ID) == true)
            echo json_encode(array('result' => true));
        else
            echo json_encode(array('result' => false));
    }
    public function adicionarEquipamento()
    {
        $equip_id = $this->input->post('idEquipamento');
        //$empty = $this->os_model->getSlotEmpty($equip_id);
        $empty = $this->os_model->getDadosSlotEmpty($equip_id);
        
        if ($empty != -1) {

           $data = array(
                'equipamentos_id' => $equip_id,
                'os_id' => $this->input->post('idOsEquipamento'),
                'slot' => $empty['posicao']
            );
            if ($this->os_model->addSlotsUpdt('equipamentos_os', $data, $empty) == true)
                echo json_encode(array('result' => true));
            else
                echo json_encode(array('result' => false));
        } else
            echo json_encode(array('result' => false));
    }

    function excluirEquipamento()
    {
        $ID = $this->input->post('idEquipamento');
        if ($this->os_model->deleteEquipamento($ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
    public function anexar()
    {
        $this->load->library('upload');
        $this->load->library('image_lib');
        $upload_conf = array(
            'upload_path' => realpath('./assets/anexos'),
            'allowed_types' => 'jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG|pdf|PDF|cdr|CDR|docx|DOCX|txt', // formatos permitidos para anexos de os
            'max_size' => 0,
        );
        $this->upload->initialize($upload_conf);
        foreach ($_FILES['userfile'] as $key => $val) {
            $i = 1;
            foreach ($val as $v) {
                $field_name = "file_" . $i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }
        unset($_FILES['userfile']);
        $error = array();
        $success = array();
        foreach ($_FILES as $field_name => $file) {
            if (!$this->upload->do_upload($field_name)) {
                $error['upload'][] = $this->upload->display_errors();
            } else {
                $upload_data = $this->upload->data();
                if ($upload_data['is_image'] == 1) {
                    // set the resize config
                    $resize_conf = array(
                        'source_image' => $upload_data['full_path'],
                        'new_image' => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                        'width' => 200,
                        'height' => 125
                    );
                    $this->image_lib->initialize($resize_conf);
                    if (!$this->image_lib->resize()) {
                        $error['resize'][] = $this->image_lib->display_errors();
                    } else {
                        $success[] = $upload_data;
                        $this->load->model('Os_model');
                        $this->Os_model->anexar($this->input->post('idOsServico'), $upload_data['file_name'], base_url() . 'assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./assets/anexos/'));
                    }
                } else {
                    $success[] = $upload_data;
                    $this->load->model('Os_model');
                    $this->Os_model->anexar($this->input->post('idOsServico'), $upload_data['file_name'], base_url() . 'assets/anexos/', '', realpath('./assets/anexos/'));
                }
            }
        }
        if (count($error) > 0) {
            echo json_encode(array('result' => false, 'mensagem' => 'Nenhum arquivo foi anexado.'));
        } else {
            echo json_encode(array('result' => true, 'mensagem' => 'Arquivo(s) anexado(s) com sucesso.'));
        }
    }
    
    public function adicionarObservacoesInternas()
    {
        if( !empty($this->input->post('observacoes_internas')) ) {

            $this->load->model('Os_model');

            $id_pai = $this->Os_model->adicionarObservacaoInterna(
                $this->input->post('idOsServico'), 
                $this->input->post('observacoes_internas'),
                $this->input->post('img01'),
                $this->input->post('img02'),
                $this->input->post('img03'),
                $this->input->post('img04'),
                $this->input->post('img05'),
                $this->input->post('img06')
            );

            echo json_encode(array('result' => true, 'mensagem' => 'Observação adicionada com sucesso.'));

        } else {

            echo json_encode(array('result' => false, 'mensagem' => 'Nenhuma observação foi informada.'));

        }
    }
    
    public function excluirAnexo($id = null)
    {
        if ($id == null || !is_numeric($id)) {
            echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
        } else {
            $this->db->where('idAnexos', $id);
            $file = $this->db->get('anexos', 1)->row();
            unlink($file->path . '/' . $file->anexo);
            if ($file->thumb != null) {
                unlink($file->path . '/thumbs/' . $file->thumb);
            }
            if ($this->os_model->delete('anexos', 'idAnexos', $id) == true) {
                echo json_encode(array('result' => true, 'mensagem' => 'Anexo excluído com sucesso.'));
            } else {
                echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
            }
        }
    }

    public function downloadanexo($id = null)
    {
        if ($id != null && is_numeric($id)) {
            $this->db->where('idAnexos', $id);
            $file = $this->db->get('anexos', 1)->row();
            $this->load->library('zip');
            $path = $file->path;
            $this->zip->read_file($path . '/' . $file->anexo);
            $this->zip->download('file' . date('d-m-Y-H.i.s') . '.zip');
        }
    }
    public function faturar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('receita') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $vencimento = $this->input->post('vencimento');
            $recebimento = $this->input->post('recebimento');
            try {
                $vencimento = explode('/', $vencimento);
                $vencimento = $vencimento[2] . '-' . $vencimento[1] . '-' . $vencimento[0];
                if ($recebimento != null) {
                    $recebimento = explode('/', $recebimento);
                    $recebimento = $recebimento[2] . '-' . $recebimento[1] . '-' . $recebimento[0];
                }
            } catch (Exception $e) {
                $vencimento = date('Y/m/d');
            }
            $data = array(
                'descricao' => set_value('descricao'),
                'valor' => $this->input->post('valor'),
                'clientes_id' => $this->input->post('clientes_id'),
                'data_vencimento' => $vencimento,
                'data_pagamento' => $recebimento,
                'baixado' => $this->input->post('recebido'),
                'cliente_fornecedor' => set_value('cliente'),
                'forma_pgto' => $this->input->post('formaPgto'),
                'tipo' => $this->input->post('tipo')
            );
            if ($this->os_model->add('lancamentos', $data) == TRUE) {
                $os = $this->input->post('os_id');
                $this->db->set('faturado', 1);
                $this->db->set('valorTotal', $this->input->post('valor'));
                $this->db->where('idOs', $os);
                $this->db->update('os');
                $this->os_model->updateEstoque($os);
                $this->session->set_flashdata('success', 'OS faturada com sucesso!');
                $json = array('result' => true);
                echo json_encode($json);
                die();
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar faturar OS.');
                $json = array('result' => false);
                echo json_encode($json);
                die(); 
            }
        }
        $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar faturar OS.');
        $json = array('result' => false);
        echo json_encode($json);
    }
    function enviarEmailOrcamento()
    {
        try {
            $idOrcamento = $this->input->post('idOrcamento');
            $resposta = $this->input->post('txtEmail');
            $orcamento = $this->orcamentos_model->getById($idOrcamento);
            $franquia = $this->franquias_model->getById($this->session->userdata('id'));
            $message = $this->load->view('os/template_email_resposta', array(
            'qualMsg'    => @$this->input->post('qualMsg'),
            
            'idOrcamento'    => $idOrcamento,
            'filial'    => @$this->input->post('isFilial'),
            'vezes'    => @$this->input->post('vezes'),
            'parcela'    => @$this->input->post('parcela'),
            'juros'    => @$this->input->post('juros'),
            'preco'    => @$this->input->post('preco'),
            'mensagem'    => $resposta,
            'dados_bancarios' => $franquia->dados_bancarios,
            'nome'        => $orcamento->nome,
            'tel'         => $orcamento->telefone,
            'marca'       => $orcamento->marca,
            'modelo'      => $orcamento->modelo,
            'defeito'     => $orcamento->defeito,
            'obs'         => $orcamento->obs,
            'facebook'    => $franquia->facebook,
            'ass'         => $this->franquias_model->obter_ass_email($this->session->userdata('id')),
            'email_marketing' => $this->franquias_model->obter_email_marketing($this->session->userdata('id'))
            ), true); 
            $this->load->library(array('typography','email'));
            $this->email->from($franquia->email, $franquia->nome);
            $this->email->to($orcamento->email);
            $this->email->subject('Rede Multi Assistência "[Orçamento]"');
            $this->email->message($message);
            $this->email->send();
            $this->orcamentos_model->updateStatus($idOrcamento, 2); // Respondido
            
            $qualMsg = $this->input->post('qualMsg');
            if($qualMsg == '1'){
                $res_auto = "Olá! Valor em ".$this->input->post('vezes')."X de R$ ".$this->input->post('parcela')." ".$this->input->post('juros')." nos cartões ou R$ ".$this->input->post('preco')." á vista.\n";
                
                $pag_data = array(
                    'vezes'    => @$this->input->post('vezes'),
                    'parcela'  => @$this->input->post('parcela'),
                    'preco'    => @$this->input->post('preco')
                );

                $this->orcamentos_model->edit('orcamentos', $pag_data, 'idOrcamento', $idOrcamento);
                if($this->input->post('isFilial') == 'S'){
                    $res_auto .= " Para sua região contamos com serviço delivery! \n Nosso pessoal de delivery retira em sua casa ou seu local de trabalho, traz até nossa loja, em seguida consertamos e entregamos de volta em sua casa ou trabalho, tudo isso já incluso no valor do conserto.\n";
                    
                    //$res_auto .= " Para sua região contamos com serviço delivery! \n Nosso pessoal de delivery retira em sua casa ou seu local de trabalho, traz até nossa loja, em seguida consertamos e entregamos de volta em sua casa ou trabalho devidamente consertado, tudo isso já incluso no valor do conserto.\n";
                }
            }elseif($qualMsg == '2'){
                $res_auto = "Olá! No caso deste defeito, para um orçamento correto, nossos técnicos verificaram a necessidade de uma análise técnica do equipamentos em nosso laboratório antes.\n";
            }
            
            @$this->orcamentos_model->updateRespostaEmail($idOrcamento, $res_auto.$resposta); // Armazena a resposta na tabela
            $this->session->set_flashdata('success', 'E-mail enviado com sucesso!');
            redirect(base_url() . 'index.php/os/visualizarOrcamento/'.$idOrcamento);
            //redirect(base_url() . 'index.php/os/orcamentos/');
            
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Erro ao enviar email: ' . $e->getMessage());
            //redirect(base_url() . 'index.php/os/visualizarOrcamento/'.$idOrcamento);
            redirect(base_url() . 'index.php/os/orcamentos');
        }
    }

    
    function GetFranquiasSlots($prefix){
        return $this->os_model->franquiasSlots($prefix);
    }

    function GetdadosOS($idfranquia,$dtinicial){
        return $this->os_model->dadosOS($idfranquia,$dtinicial);
    }

    function GetdadosEquipamentos($idos,$idfranquia,$clientes_id){
        return $this->os_model->dadosEquipamentos($idos,$idfranquia,$clientes_id);
    }

    function GetdadosSlots($idFranquia,$prefix){
        return $this->os_model->slotsDados($idFranquia,$prefix);
    }

    function GetdadosEspacosSlots($prefix, $idFranquia ,$qtd){
        return $this->os_model->getSlotParaOS($prefix, $idFranquia, $qtd);
    }

    // criar um slot para cada tipo, chamar a functicon numeros de slots aqui também
    // 
  
    function osSlots($prefixo){

        $this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        $num_slots_p = $franquia->slotsp;
        $num_slots_m = $franquia->slotsm;
        $num_slots_g = $franquia->slotsg;

        $qtdp = $franquia->qtd_fileiras_slots_p;
        $qtdm = $franquia->qtd_fileiras_slots_m;
        $qtdg = $franquia->qtd_fileiras_slots_g;

        $limite = $franquia->fileiras_slots;

        $arraysql = array();
        $arrayequi = array();

        $arrayletras = array('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        $ds = $this->GetdadosSlots($this->session->userdata('id'),$prefixo);
         
        switch ($prefixo) {
            case 'p':
            case 'P':
                foreach ($ds as $key => $value) {
                    echo "<tr>";
                        echo "<td class='number-column'>".$key."</td>";

                        for ($j=1; $j <= $qtdp; $j++) { 
                            $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                            echo "<td class='p'>".$os."</td>";
                            //echo "<td class='p'>"."<a href='".base_url()."index.php/os/visualizar/".$value[$j-1]."'>".$value[$j-1]."</a></td>";
                        }

                    echo "</tr>";
                }
                break;
            case 'm':
            case 'M':
                foreach ($ds as $key => $value) {
                    echo "<tr>";
                        echo "<td class='number-column'>".$key."</td>";

                        for ($j=1; $j <= $qtdm; $j++) { 
                            $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                            echo "<td class='m'>".$os."</td>";
                            //echo "<td class='m'>"."<a href='".base_url()."index.php/os/visualizar/".$value[$j-1]."'>".$value[$j-1]."</a></td>";
                        }

                    echo "</tr>";
                }
                break;
            case 'g':
            case 'G':
                foreach ($ds as $key => $value) {
                    echo "<tr>";
                        echo "<td class='number-column'>".$key."</td>";
                        
                        for ($j=1; $j <= $qtdg; $j++) { 
                            $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                            echo "<td class='g'>".$os."</td>";
                            //echo "<td class='g'>"."<a href='".base_url()."index.php/os/visualizar/".$value[$j-1]."'>".$value[$j-1]."</a></td>";
                        }

                    echo "</tr>";
                }
                break;
        }
        
        
        return;
    }

    // criando visibilidade aos dados do slot-cofre
    function osSlotsCofre($idfranquia){

        //$this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        
        $cofre = 25;
        $qtd_cofre_fl = 26;

        $ds = $this->os_model->slotsDadoscofre($idfranquia);

        foreach ($ds as $key => $value) {
            echo "<tr>";
                echo "<td class='number-column'>".$key."</td>";

                for ($j=1; $j <= $qtd_cofre_fl; $j++) { 
                    $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                    if ($os != null) {
                        echo "<td class='p'>".$os."</td>";
                    }else if($value[$j-1] !=0){
                        echo "<td class='p'>".'<a href='.base_url()."index.php/os/visualizar/".$value[$j-1].'> S/R: </br> '.$value[$j-1]."</a></td>";
                    }else{
                        echo "<td class='p'> </td>";
                    }
                    //echo "<td class='p'>"."<a href='".base_url()."index.php/os/visualizar/".$value[$j-1]."'>".$value[$j-1]."</a></td>";
                }

            echo "</tr>";
        }

        return;
    }

    // function criada para colocar o número de slotes disponibilizados para cada um, conforme configuração
    // na area administrativa.
    // Números de SLOTS que ficam acima da tabela para indentificação
    // Não mexe nessa function aqui
    // alterando configuração para montargem da area de slotes conforme gravadas na table slots.
    function numSlotsOs($prefixo){
        //$this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));
        //$qtdp = $franquia->qtd_fileiras_slots_p;
        //$qtdm = $franquia->qtd_fileiras_slots_m;
        //$qtdg = $franquia->qtd_fileiras_slots_g;

        $ff = $this->franquias_model->getSlotsFranq($this->session->userdata('id'));
        
        $slp = json_decode($ff->p, true);
        $slm = json_decode($ff->m, true);
        $slg = json_decode($ff->g, true);

        // montando tola a table apartir de agora, para mostrar nos slots        
        switch ($prefixo) {
            case 'P':
            case 'p':
                //for($i = 1; $i <= $qtdp; $i++){
                //    echo "<td class='letter-column'>";
                //        echo $i;
                //    echo "</td>";
                //}
                foreach ($slp as $key => $value) {
                    echo "<table>
                            <tbody>
                                <tr>";
                                    echo "<td></td> ";
                                for($i = 1; $i <= count($value); $i++){
                                    echo "<td class='letter-column'>";
                                        echo $i;
                                    echo "</td>"; 
                                }
                           echo "</tr>";

                           echo "<tr>";

                        echo "<td class='number-column'>".$key."</td>";
                            for ($j=1; $j <= count($value); $j++) { 
                                //echo "<td class='p'>".$value[$j-1]."</td>";
                                $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                                echo "<td class='p'>".$os."</td>";
                            }    

                        echo "</tr>";
                    echo "</table>";
                }

            break;
            case 'M':
            case 'm':
                //for($i = 1; $i <= $qtdm; $i++){
                //    echo "<td class='letter-column'>";
                //        echo $i;
                //    echo "</td>";
                //}
                foreach ($slm as $key => $value) {
                    echo "<table>
                            <tbody>
                                <tr>";
                                    echo "<td></td> ";
                                for($i = 1; $i <= count($value); $i++){
                                    echo "<td class='letter-column'>";
                                        echo $i;
                                    echo "</td>"; 
                                }
                           echo "</tr>";

                           echo "<tr>";

                        echo "<td class='number-column'>".$key."</td>";
                            for ($j=1; $j <= count($value); $j++) { 
                                //echo "<td class='p'>".$value[$j-1]."</td>";
                                $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                                echo "<td class='m'>".$os."</td>";
                            }    

                        echo "</tr>";
                    echo "</table>";
                }
            break;
            case 'G':
            case 'g':
                //for($i = 1; $i <= $qtdg; $i++){
                //    echo "<td class='letter-column'>";
                //        echo $i;
                //    echo "</td>";
                //}
                foreach ($slg as $key => $value) {
                    echo "<table>
                            <tbody>
                                <tr>";
                                    echo "<td></td> ";
                                for($i = 1; $i <= count($value); $i++){
                                    echo "<td class='letter-column'>";
                                        echo $i;
                                    echo "</td>"; 
                                }
                           echo "</tr>";

                           echo "<tr>";

                            echo "<td class='number-column'>".$key."</td>";
                            for ($j=1; $j <= count($value); $j++) { 
                                //echo "<td class='p'>".$value[$j-1]."</td>";
                                $os = $this->os_model->iconesEquiSlots($value[$j-1]);
                                echo "<td class='g'>".$os."</td>";
                            } 
                        echo "</tr>";
                    echo "</table>";
                }
            break;
            
        }
        
        return;
    }   

    // pega a primeira posição livre para cada slot
    function posicaoSlots($prefix){
        $this->data['franquia'] = $franquia = $this->franquias_model->getById($this->session->userdata('id'));

        $qtdp = $franquia->qtd_fileiras_slots_p;
        $qtdm = $franquia->qtd_fileiras_slots_m;
        $qtdg = $franquia->qtd_fileiras_slots_g;

        switch ($prefix) {
            case 'p':
            case 'P':
                
                return $slot = $this->GetdadosEspacosSlots($prefix, $this->session->userdata('id'), $qtdp);
                
                break;
            case 'M':
            case 'm':

                return $slot = $this->GetdadosEspacosSlots($prefix, $this->session->userdata('id'), $qtdm);
            
                break;
            case 'G':
            case 'g':
                
                return $slot = $this->GetdadosEspacosSlots($prefix, $this->session->userdata('id'), $qtdg);
                
                break;
        }
    }

    
}
