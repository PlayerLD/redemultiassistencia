<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class XML extends CI_Controller {

    /**
     * author: Ramon Silva  
     * email: silva018-mg@yahoo.com.br
     *  
     */ 

	public function __construct(){
		parent::__construct();

		if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        	redirect('sistemaos/login');
        }

        $this->load->helper(array('codegen_helper'));
        $this->load->model('Xml_model','',TRUE);
        $this->data['menuXML'] = 'xml';
	}

    public function index(){
        $this->gerenciar(); 
    }
	public function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
           redirect(base_url());
        }
 
		$this->load->library('pagination');

        $pesquisa = $this->input->get('pesquisa');
        $de = $this->input->get('data');
        $ate = $this->input->get('data2');

        if($pesquisa == null && $de == null && $ate == null){

            $countresults = $this->Xml_model->countNotas('os');
                   
            $config['base_url'] = base_url().'index.php/xml/gerenciar';
            $config['total_rows'] = $countresults;
            $config['per_page'] = 10;
            $config['next_link'] = 'Próxima';
            $config['prev_link'] = 'Anterior';
            $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
            $config['full_tag_close'] = '</ul></div>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
            $config['cur_tag_close'] = '</b></a></li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['first_link'] = 'Primeira';
            $config['last_link'] = 'Última';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            
            $this->pagination->initialize($config);     
            
            $this->data['results'] = $this->Xml_model->get('os','os.idOs, os.idFranquias, os.numeroNFse, clientes.idClientes, clientes.nomeCliente','',$config['per_page'],$this->uri->segment(3));
        
        }
        else{

            if($de != null){

                $de = explode('/', $de);
                $de = $de[2].'-'.$de[1].'-'.$de[0];

                if($ate != null){
                    $ate = explode('/', $ate);
                    $ate = $ate[2].'-'.$ate[1].'-'.$ate[0]; 
                }
                else{
                    $ate = $de;
                }
            }

            $this->data['results'] = $this->Xml_model->search($pesquisa, $de, $ate);
        }

       	$this->data['view'] = 'xml/xml';
		$this->load->view('tema/topo',$this->data);
	}

    public function download($id = null){
    	
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

    	if($id == null || !is_numeric($id)){
    		$this->session->set_flashdata('error','Erro! O XML não pode ser localizado.');
            redirect(base_url() . 'index.php/XML/');
    	}

    	$file = $this->Xml_model->getByIdNumNFSE($id);
    	
        // Define o tempo máximo de execução em 0 para as conexões lentas
        set_time_limit(0);
        // Arqui você faz as validações e/ou pega os dados do banco de dados
        $aquivoNome = $file->numNFSE.'.xml'; // nome do arquivo que será enviado p/ download
        $arquivoLocal = base_url('assets/arquivos/nfse/xml/nfse'.$file->numNFSE.'.xml'); // caminho absoluto do arquivo
        // Definimos o novo nome do arquivo
        $novoNome = 'nfse'.$file->numNFSE.'.xml';
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$novoNome.'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        //header('Content-Length: '.filesize($aquivoNome));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        // Envia o arquivo para o cliente
        readfile($arquivoLocal);
        exit;
        
    }

    public function loteArqPer(){
        $this->PesquisLoteDataArquivo();
    }

    public function PesquisLoteDataArquivo(){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        $idFranquia = $this->session->userdata('id');
        // recebendo e transformando datas
        $dataInicial  = $this->input->post('dataInicial');
        //$datainicial = explode('/', $dataInicial);
        //$dtInicial = $dataInicial[2] . '-' . $dataInicial[1] . '-' . $dataInicial[0];
        
        $dataFinal    = $this->input->post('dataFinal');
        //$datafinal = explode('/', $dataFinal);
        //$dtFinal = $dataFinal[2] . '-' . $dataFinal[1] . '-' . $dataFinal[0];

        $arq = $this->Xml_model->LoteDataArquivos($idFranquia,$dataInicial,$dataFinal);
        
        //var_dump($arq);

        if($arq){
           
            $ext1  = 'nfse';
            $ext2  = '.xml';
            $this->load->library('zip');
            $this->load->helper('file');
        
            $path = 'assets/arquivos/nfse/xml/';
        
            $files = get_filenames($path);
        
            //var_dump($files);
            foreach ($files as $f) {
                foreach ($arq as $a) {
                    $arquivo = $ext1.$a->numNFSE.$ext2;
                    if ($f == $arquivo) {
                        $this->zip->read_file($path.$f, false);
        
                    }
                }
            }
            //echo "<pre>";
            //var_dump($a);
            //echo "</pre>";
            $this->zip->download('XMLarquivos.zip');
            $this->data['view'] = 'xml/loteArqPer';
            $this->load->view('tema/topo',$this->data);

        }else if($arq == false){

            if($dataInicial && $dataFinal){
                $this->data['custom_error'] = 'Verifique o período informado, sem registros encontrados.';
            }
            

            $this->data['menuXML'] = 'xml';
            $this->data['view'] = 'xml/loteArqPer';   
            $this->load->view('tema/topo',$this->data);
            //redirect(base_url() . 'index.php/xml/');
        }



    }

    public function loteDownload($idfranq = null){
        /*
        sql para download por datas
        SELECT `os`.`numeroNfse` as numNFSE, os.dataInicial, os.dataFinal
FROM (`os`) 
JOIN `clientes` ON `clientes`.`idClientes` = `os`.`clientes_id` 
JOIN `usuarios` ON `usuarios`.`idUsuarios` = `os`.`usuarios_id` 
WHERE os.dataInicial BETWEEN '2018-03-23' AND '2018-04-28' AND
`os`.`numeroNfse` != '(NULL)' AND `os`.`idFranquias` = '49' 
ORDER BY os.numeroNfse ASC

        //$this->load->helper('file');
        $this->load->library('zip');
        $arq = $this->Xml_model->loteIdNumNFSE($idfranq);
        //echo "<pre>";
        //var_dump($arq);
        //echo "</pre>";
        $local = get_filenames(base_url('assets/arquivos/nfse/xml/'));
        //$local = base_url('assets/arquivos/nfse/xml/');
        $ext1  = 'nfse';
        $ext2  = '.xml';
     
        foreach ($arq as $f) {
            $arquivos = $local.$ext1.$f->numNFSE.$ext2;
            //$this->zip->add_data($arquivos);
        }
        $this->zip->read_file($arquivos, true);

        //if ($this->zip->read_dir($local)) {
            //foreach ($arq as $f) {
            //    $this->zip->read_dir(base_url('assets/arquivos/nfse/xml/'));
            //    $arquivos = $local.$ext1.$f->numNFSE.$ext2;
            //}
            //    $this->zip->download('ArquivosXML.zip');
        //}
        $this->zip->download('ArquivosXML.zip');
        */
        /*
        $fileName = 'arquivoXML.zip';
        //$path = __DIR__.base_url('assets/arquivos/nfse/xml/');
        $path = base_url('assets/arquivos/nfse/xml/');
        $fullPath = $path.'/'.$fileName;

        $scanDir = scandir($path);
        array_shift($scanDir);
        array_shift($scanDir);

        $zip = new \ZipArchive();

        if($zip->open($fullPath, \ZipArchive::CREATE)){
            foreach ($scanDir as $file) {
                $zip->addFile($path.'/'.$file, $file);
            }
            $zip->close();
        }
         
        if (file_exists($fullPath)) {
            // Forçamos o donwload do arquivo.
            header('Content-Type: application/zip');
            header('Content-Disposition: attachment; filename="'.$fileName.'"');
            readfile($fullPath);
            //removemos o arquivo zip após download
            //unlink($fullPath);
        }
        */

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        $arq = $this->Xml_model->loteIdNumNFSE($idfranq);
        $ext1  = 'nfse';
        $ext2  = '.xml';
        $this->load->library('zip');
        $this->load->helper('file');

        $path = 'assets/arquivos/nfse/xml/';

        $files = get_filenames($path);

        //var_dump($files);
        foreach ($files as $f) {
            foreach ($arq as $a) {
                $arquivo = $ext1.$a->numNFSE.$ext2;
                if ($f == $arquivo) {
                    $this->zip->read_file($path.$f, false);

                }
            }
        }
        //echo "<pre>";
        //var_dump($a);
        //echo "</pre>";
        $this->zip->download('XMLarquivos.zip');
    }  

    /*
    public function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aXML')){
          $this->session->set_flashdata('error','Você não tem permissão para adicionar XML.');
          redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        $this->form_validation->set_rules('nome', '', 'trim|required|xss_clean');


        if ($this->form_validation->run() == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $XML = $this->do_upload();

            $file = $XML['file_name'];
            $path = $XML['full_path'];
            $url = base_url().'assets/XML/'.date('d-m-Y').'/'.$file;
            $tamanho = $XML['file_size'];
            $tipo = $XML['file_ext'];

            $data = $this->input->post('data');

            if($data == null){
                $data = date('Y-m-d');
            }
            else{
                $data = explode('/',$data);
                $data = $data[2].'-'.$data[1].'-'.$data[0];
            }

            $data = array(
                'documento' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao'),
                'file' => $file,
                'path' => $path,
                'url' => $url,
                'cadastro' => $data,
                'tamanho' => $tamanho,
                'tipo' => $tipo
            );

            if ($this->Xml_model->add('documentos', $data) == TRUE) {
                $this->session->set_flashdata('success','XML adicionado com sucesso!');
                redirect(base_url() . 'index.php/XML/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['view'] = 'xml/adicionarXML';
        $this->load->view('tema/topo', $this->data);

    }

    public function editar() {

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eXML')){
          $this->session->set_flashdata('error','Você não tem permissão para editar XML.');
          redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        $this->form_validation->set_rules('nome', '', 'trim|required|xss_clean');
        if ($this->form_validation->run() == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = $this->input->post('data');
            if($data == null){
                $data = date('Y-m-d');
            }
            else{
                $data = explode('/',$data);
                $data = $data[2].'-'.$data[1].'-'.$data[0];
            }

            $data = array(
                'documento' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao'),
                'cadastro' => $data,           
            );

            if ($this->Xml_model->edit('documentos', $data, 'idDocumentos', $this->input->post('idDocumentos')) == TRUE) {
                $this->session->set_flashdata('success','Alterações efetuadas com sucesso!');
                redirect(base_url() . 'index.php/XML/editar/'.$this->input->post('idDocumentos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }


        $this->data['result'] = $this->Xml_model->getById($this->uri->segment(3));
        $this->data['view'] = 'xml/editarXML';
        $this->load->view('tema/topo', $this->data);

    }
    */ 

    /*
    public function excluir(){
    	if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dXML')){
          $this->session->set_flashdata('error','Você não tem permissão para excluir XML.');
          redirect(base_url());
        }

    	$id = $this->input->post('id');
    	if($id == null || !is_numeric($id)){
    		$this->session->set_flashdata('error','Erro! O XML não pode ser localizado.');
            redirect(base_url() . 'index.php/XML/');
    	}

    	$file = $this->Xml_model->getById($id);

    	$this->db->where('idDocumentos', $id);
        
        if($this->db->delete('documentos')){

        	$path = $file->path;
	    	unlink($path);

	    	$this->session->set_flashdata('success','XML excluido com sucesso!');
	        redirect(base_url() . 'index.php/XML/');
        }
        else{

        	$this->session->set_flashdata('error','Ocorreu um erro ao tentar excluir o XML.');
            redirect(base_url() . 'index.php/XML/');
        }


    }

    public function do_upload(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aXML')){
          $this->session->set_flashdata('error','Você não tem permissão para adicionar XML.');
          redirect(base_url());
        }
	
    	$date = date('d-m-Y');

		$config['upload_path'] = './assets/XML/'.$date;
	    $config['allowed_types'] = 'xml|xmlx|xmls';
	    $config['max_size']     = 0;
	    $config['max_width']  = '';
	    $config['max_height']  = '';
	    $config['encrypt_name'] = true;


		if (!is_dir('./assets/XML/'.$date)) {

			mkdir('./assets/XML/' . $date, 0777, TRUE);

		}

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->session->set_flashdata('error','Erro ao fazer upload do XML, verifique se a extensão do XML é permitida.');
            redirect(base_url() . 'index.php/XML/adicionar/');
		}
		else
		{
			//$data = array('upload_data' => $this->upload->data());
			return $this->upload->data();
		}
	} */

}

/* End of file XML.php */
/* Location: ./application/controllers/XML.php */