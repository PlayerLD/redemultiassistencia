<?php
class Equipamentos extends CI_Controller {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     *  
     */   
     
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('equipamentos_model', '', TRUE);
        $this->load->model('franquias_model', '', TRUE);
    $this->load->model('clientes_model', '', TRUE);
        $this->load->model('os_model', '', TRUE);
        $this->data['menuCatalogo'] = 'equipamentos';
    }
  
  function index(){
    $this->gerenciar();
                $this->load->helper('url');
                //$this->load->view("equipamentos/editarEquipamento");
  }
        
        public function saveImage(){
            /*write your own code to save to your database*/        
            $newname = "some_name.jpg"; 
            file_put_contents( $newname, file_get_contents('php://input') );
        } 
  function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamento')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar serviços.');
           redirect(base_url());
        }
          $countrows = $this->equipamentos_model->count('equipamentos', $this->session->userdata('id'));  
    $this->data['results'] = $this->equipamentos_model->get('equipamentos','idEquipamentos,equipamento,marcas_id,mei,modelo,clientes_id,obser,tipo,status,defeito','',$countrows,$this->uri->segment(3));
       
      $this->data['view'] = 'equipamentos/equipamentos';
        $this->load->view('tema/topo',$this->data);
    
    }

    // server side
    function getlists(){

        $columns = array(
                0 => 'idEquipamentos',
                1 => 'tipo',
                2 => 'equipamento',
                3 => 'marca',
                4 => 'mei',
                5 => 'modelo',
                6 => 'defeito',
                7 => 'cliente',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->equipamentos_model->count_totalEquipamentos();


        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->equipamentos_model->allEquipamentos($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->equipamentos_model->idEquipamentos_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->equipamentos_model->equipamentos_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['idEquipamentos'] = $post->idEquipamentos;
                $nestedData['tipo'] = $post->tipo;
                $nestedData['equipamento'] = $post->equipamento;
                $nestedData['marca'] = $post->marca;
                $nestedData['mei'] = $post->mei;
                $nestedData['modelo'] = $post->modelo;
                $nestedData['defeito'] = $this->equipamentos_model->tranformDefeito($post->defeito);
                $nestedData['cliente'] = $post->cliente;
                // adicionando as funções do botões de ações
                $buttons = ' ';

                if($this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamento')){
                    $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/equipamentos/visualizar/'.$post->idEquipamentos.'" class="btn tip-top" title="Visualizar Equipamento"><i class="icon-eye-open"></i></a>';
                }
                if($this->permission->checkPermission($this->session->userdata('permissao'),'eEquipamento')){
                    $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/equipamentos/editar/'.$post->idEquipamentos.'" class="btn btn-info tip-top" title="Editar Equipamento"><i class="icon-pencil icon-white"></i></a>';
                }                                
                if($this->permission->checkPermission($this->session->userdata('permissao'),'dEquipamento')){
                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" equipamento="'.$post->idEquipamentos.'" class="btn btn-danger tip-top" title="Excluir Equipamento"><i class="icon-remove icon-white"></i></a>';
                }          

            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }
    // end server side


    function inserirEquipamentoAjax(){
        $json = array();
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
            exit;
        }
        $idFranquia=$this->session->userdata('id');
            $data = array(
                'equipamento' => set_value('tipoEquipamento'),
                'idFranquia' => $idFranquia,
                'marcas_id' => $this->input->post('marcas_id'),
                'mei' => set_value('meiEquipamento'),
                'modelo' => set_value('modeloEquipamento'),
                'clientes_id' => $this->input->post('clientes_id'),
                'obser' => set_value('obsEquipamento'),
                'tipo' => set_value('tipoEquipamento'),
                'status' => set_value('status')
            );
            if ( is_numeric($id = $this->equipamentos_model->add('equipamentos', $data, true)) ) {
                $json = array("result" => true, "idEquipamento" => $id);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
                $json = array("result" => false);
            }
        echo json_encode($json);    
    }
    function adicionar($iframe = false) {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aEquipamento')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar serviços.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
    
    $idCliente = $this->session->flashdata("idCliente");
      
        if ($this->form_validation->run('equipamentos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $idFranquia=$this->session->userdata('id');
            $data = array(
                'equipamento' => set_value('equipamento'),
                'idFranquia'  => $idFranquia,
                'marcas_id'   => $this->input->post('marcas_id'),
                'mei'         => set_value('mei'),
                'modelo'      => set_value('modelo'),
                'clientes_id' => $this->input->post('clientes_id'),
                'obser'       => set_value('obser'),
                'tipo'        => set_value('tipo'),
                'status'      => set_value('status')
            );
 
            if ( is_numeric($id = $this->equipamentos_model->add('equipamentos', $data, true)) ) {
                $this->session->set_flashdata('success', 'Equipamento adicionado com sucesso!');
                if($iframe)
                    redirect('equipamentos/editar/'.$id.'/1');            
                else
                    redirect('equipamentos/editar/'.$id);            
      } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
    
    $this->data['cliente'] = $this->clientes_model->getById($idCliente);
    
        $idFranquia=$this->session->userdata('id');
        $this->data['franquia'] = $this->franquias_model->getById($idFranquia);
        $this->data['view'] = 'equipamentos/adicionarEquipamento';
        $this->data['iframe'] = $iframe;
        $this->load->view('tema/topo', $this->data);
    }
    function getDefeito()
    {
      $list; $selected; $defoutros = $this->input->post('defoutros');
        switch ($this->input->post('equipamento'))
      {
            case "celular":
        case "Celular":
          $selected = $this->input->post('celular');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1celular'),
            $this->input->post('check2celular'),
            $this->input->post('check3celular'),
            $this->input->post('check4celular'),
            $this->input->post('check5celular'),
            $this->input->post('check6celular'),
            $this->input->post('check7celular'),
            $this->input->post('check8celular'),
            $this->input->post('check9celular'),
            $this->input->post('check10celular'),
            $this->input->post('check11celular'),
            $this->input->post('check12celular'),
            $this->input->post('check13celular'),
            $this->input->post('check14celular'),
            $this->input->post('check15celular'),
            $this->input->post('check16celular'),
            $this->input->post('check17celular'),
            $this->input->post('check18celular'),
            $this->input->post('check19celular'),
            $this->input->post('check20celular'),
            $this->input->post('check21celular'),
            $this->input->post('check22celular'),
            $this->input->post('check23celular'),
            $this->input->post('check24celular')
          ); break;
        
            case "Tablet":
        case "tablet":
          $selected = $this->input->post('tablet');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1tablet'),
            $this->input->post('check2tablet'),
            $this->input->post('check3tablet'),
            $this->input->post('check4tablet'),
            $this->input->post('check5tablet'),
            $this->input->post('check6tablet'),
            $this->input->post('check7tablet'),
            $this->input->post('check8tablet'),
            $this->input->post('check9tablet'),
            $this->input->post('check10tablet'),
            $this->input->post('check11tablet'),
            $this->input->post('check12tablet'),
            $this->input->post('check13tablet'),
            $this->input->post('check14tablet')
          ); break;
          
            case "Notebook":
        case "notebook":
          $selected = $this->input->post('notebook');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1notebook'),
            $this->input->post('check2notebook'),
            $this->input->post('check3notebook'),
            $this->input->post('check4notebook'),
            $this->input->post('check5notebook'),
            $this->input->post('check6notebook'),
            $this->input->post('check7notebook'),
            $this->input->post('check8notebook')
          ); break;
                
            case "Videogame":
            case "videogame":
                $selected = $this->input->post('videogame');
                $list = array
                (
                    "DEF",
                    "OUT",
                    $this->input->post('check1videogame'),
                    $this->input->post('check1v2deogame'),
                    $this->input->post('check3videogame'),
                    $this->input->post('check4videogame'),
                    $this->input->post('check5videogame'),
                    $this->input->post('check6videogame'),
                    $this->input->post('check7videogame'),
                    $this->input->post('check8videogame'),
                    $this->input->post('check9videogame')
                ); break;
          
        case "TV":
            case "tv":
          $selected = $this->input->post('tv');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1tv'),
            $this->input->post('check2tv'),
            $this->input->post('check3tv'),
            $this->input->post('check4tv'),
            $this->input->post('check5tv'),
            $this->input->post('check6tv'),
            $this->input->post('check7tv'),
            $this->input->post('check9tv'),
            $this->input->post('check10tv'),
            $this->input->post('check11tv'),
            $this->input->post('check12tv')
          ); break;
          
        case "Computador":
            case "computador":
          $selected = $this->input->post('computador');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1computador'),
            $this->input->post('check2computador'),
            $this->input->post('check3computador'),
            $this->input->post('check4computador'),
            $this->input->post('check5computador'),
            $this->input->post('check6computador'),
            $this->input->post('check7computador'),
            $this->input->post('check9computador')
          ); break;
          case "iMac":
        case "imac":
        case "mac":
        case "Mac":
          $selected = $this->input->post('iMac');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1iMac'),
            $this->input->post('check2iMac'),
            $this->input->post('check3iMac'),
            $this->input->post('check4iMac'),
            $this->input->post('check5iMac'),
            $this->input->post('check6iMac'),
            $this->input->post('check7iMac'),
            $this->input->post('check8iMac'),
            $this->input->post('check9iMac'),
            $this->input->post('check10iMac'),
            $this->input->post('check11iMac'),
            $this->input->post('check12iMac'),
            $this->input->post('check13iMac')
          ); break;
          case "drone":
          $selected = $this->input->post('computador');
          $list = array
          (
            "DEF",
            "OUT",
            $this->input->post('check1drone'),
            $this->input->post('check2drone'),
            $this->input->post('check3drone'),
            $this->input->post('check4drone'),
            $this->input->post('check5drone'),
            $this->input->post('check6drone'),
            $this->input->post('check7drone'),
            $this->input->post('check8drone'),
            $this->input->post('check9drone'),
            $this->input->post('check10drone'),
            $this->input->post('check11drone'),
            $this->input->post('check12drone'),
            $this->input->post('check13drone'),
            $this->input->post('check14drone'),
            $this->input->post('check15drone'),
            $this->input->post('check16drone'),
            $this->input->post('check17drone'),
            $this->input->post('check18drone'),
            $this->input->post('check19drone'),
            $this->input->post('check20drone'),
          ); break;
      } 
      
      $result = str_replace("\"", "", str_replace("true", "1", str_replace("false", "0", json_encode($list))));
      return str_replace("DEF", "\"".$selected."\"", str_replace("OUT", "\"".$defoutros."\"", str_replace("0", "\"0\"", str_replace("1", "\"1\"", $result))));
    }
    function get_saida_Defeito()
    {
      $list; $selected; $defoutros = "";
        switch ($this->input->post('equipamento'))
      {
            case "celular":
        case "Celular":
            if(!empty($this->input->post('saida_check1celular')) || !empty($this->input->post('saida_check2celular')) || !empty($this->input->post('saida_check3celular')) || !empty($this->input->post('saida_check4celular')) || !empty($this->input->post('saida_check5celular')) || !empty($this->input->post('saida_check6celular')) || !empty($this->input->post('saida_check7celular')) || !empty($this->input->post('saida_check8celular')) || !empty($this->input->post('saida_check9celular')) || !empty($this->input->post('saida_check10celular')) || !empty($this->input->post('saida_check11celular')) || !empty($this->input->post('saida_check12celular')) || !empty($this->input->post('saida_check13celular')) || !empty($this->input->post('saida_check14celular')) || !empty($this->input->post('saida_check15celular')) || !empty($this->input->post('saida_check16celular')) || !empty($this->input->post('saida_check17celular')) || !empty($this->input->post('saida_check18celular')) || !empty($this->input->post('saida_check19celular')) || !empty($this->input->post('saida_check20celular')) || !empty($this->input->post('saida_check21celular')) || !empty($this->input->post('saida_check22celular')) || !empty($this->input->post('saida_check23celular')) || !empty($this->input->post('saida_check24celular'))){
                $selected = $this->input->post('celular');
                $list = array
                (
                "DEF",
                "OUT",
                $this->input->post('saida_check1celular'),
                $this->input->post('saida_check2celular'),
                $this->input->post('saida_check3celular'),
                $this->input->post('saida_check4celular'),
                $this->input->post('saida_check5celular'),
                $this->input->post('saida_check6celular'),
                $this->input->post('saida_check7celular'),
                $this->input->post('saida_check8celular'),
                $this->input->post('saida_check9celular'),
                $this->input->post('saida_check10celular'),
                $this->input->post('saida_check11celular'),
                $this->input->post('saida_check12celular'),
                $this->input->post('saida_check13celular'),
                $this->input->post('saida_check14celular'),
                $this->input->post('saida_check15celular'),
                $this->input->post('saida_check16celular'),
                $this->input->post('saida_check17celular'),
                $this->input->post('saida_check18celular'),
                $this->input->post('saida_check19celular'),
                $this->input->post('saida_check20celular'),
                $this->input->post('saida_check21celular'),
                $this->input->post('saida_check22celular'),
                $this->input->post('saida_check23celular'),
                $this->input->post('saida_check24celular')
                ); 
            }else
                return null;
        break;
        
            case "Tablet":
        case "tablet":
            if(!empty($this->input->post('saida_check1tablet')) || !empty($this->input->post('saida_check2tablet')) || !empty($this->input->post('saida_check3tablet')) || !empty($this->input->post('saida_check4tablet')) || !empty($this->input->post('saida_check5tablet')) || !empty($this->input->post('saida_check6tablet')) || !empty($this->input->post('saida_check7tablet')) || !empty($this->input->post('saida_check8tablet')) || !empty($this->input->post('saida_check9tablet')) || !empty($this->input->post('saida_check10tablet')) || !empty($this->input->post('saida_check11tablet')) || !empty($this->input->post('saida_check12tablet')) || !empty($this->input->post('saida_check13tablet')) || !empty($this->input->post('saida_check14tablet'))){
                $selected = $this->input->post('tablet');
                $list = array
                (
                "DEF",
                "OUT",
                $this->input->post('saida_check1tablet'),
                $this->input->post('saida_check2tablet'),
                $this->input->post('saida_check3tablet'),
                $this->input->post('saida_check4tablet'),
                $this->input->post('saida_check5tablet'),
                $this->input->post('saida_check6tablet'),
                $this->input->post('saida_check7tablet'),
                $this->input->post('saida_check8tablet'),
                $this->input->post('saida_check9tablet'),
                $this->input->post('saida_check10tablet'),
                $this->input->post('saida_check11tablet'),
                $this->input->post('saida_check12tablet'),
                $this->input->post('saida_check13tablet'),
                $this->input->post('saida_check14tablet')
                );
            }else
                return null;
            break;
          
            case "Notebook":
        case "notebook":
        if(!empty($this->input->post('saida_check1notebook')) || !empty($this->input->post('saida_check2notebook')) || !empty($this->input->post('saida_check3notebook')) || !empty($this->input->post('saida_check4notebook')) || !empty($this->input->post('saida_check5notebook')) || !empty($this->input->post('saida_check6notebook')) || !empty($this->input->post('saida_check7notebook')) || !empty($this->input->post('saida_check8notebook'))){
              $selected = $this->input->post('notebook');
              $list = array
              (
                "DEF",
                "OUT",
                $this->input->post('saida_check1notebook'),
                $this->input->post('saida_check2notebook'),
                $this->input->post('saida_check3notebook'),
                $this->input->post('saida_check4notebook'),
                $this->input->post('saida_check5notebook'),
                $this->input->post('saida_check6notebook'),
                $this->input->post('saida_check7notebook'),
                $this->input->post('saida_check8notebook')
              ); 
          }else
            return null;
          break;
                
            case "Videogame":
            case "videogame":
            if(!empty($this->input->post('saida_check1videogame')) || !empty($this->input->post('saida_check1v2deogame')) || !empty($this->input->post('saida_check3videogame')) || !empty($this->input->post('saida_check4videogame')) || !empty($this->input->post('saida_check5videogame')) || !empty($this->input->post('saida_check6videogame')) || !empty($this->input->post('saida_check7videogame')) || !empty($this->input->post('saida_check8videogame')) || !empty($this->input->post('saida_check9videogame'))){
                $selected = $this->input->post('videogame');
                $list = array
                (
                    "DEF",
                    "OUT",
                    $this->input->post('saida_check1videogame'),
                    $this->input->post('saida_check1v2deogame'),
                    $this->input->post('saida_check3videogame'),
                    $this->input->post('saida_check4videogame'),
                    $this->input->post('saida_check5videogame'),
                    $this->input->post('saida_check6videogame'),
                    $this->input->post('saida_check7videogame'),
                    $this->input->post('saida_check8videogame'),
                    $this->input->post('saida_check9videogame')
                ); 
            }else
                return null;
            break;
          
        case "TV":
            case "tv":
            if(!empty($this->input->post('saida_check1tv')) || !empty($this->input->post('saida_check2tv')) || !empty($this->input->post('saida_check3tv')) || !empty($this->input->post('saida_check4tv')) || !empty($this->input->post('saida_check5tv')) || !empty($this->input->post('saida_check6tv')) || !empty($this->input->post('saida_check7tv')) || !empty($this->input->post('saida_check9tv')) || !empty($this->input->post('saida_check10tv')) || !empty($this->input->post('saida_check11tv')) || !empty($this->input->post('saida_check12tv'))){
              $selected = $this->input->post('tv');
              $list = array
              (
                "DEF",
                "OUT",
                $this->input->post('saida_check1tv'),
                $this->input->post('saida_check2tv'),
                $this->input->post('saida_check3tv'),
                $this->input->post('saida_check4tv'),
                $this->input->post('saida_check5tv'),
                $this->input->post('saida_check6tv'),
                $this->input->post('saida_check7tv'),
                $this->input->post('saida_check9tv'),
                $this->input->post('saida_check10tv'),
                $this->input->post('saida_check11tv'),
                $this->input->post('saida_check12tv')
              ); 
            }else
                return null;
          break;
          
        case "Computador":
            case "computador":
            if(!empty($this->input->post('saida_check1computador')) || !empty($this->input->post('saida_check2computador')) || !empty($this->input->post('saida_check3computador')) || !empty($this->input->post('saida_check4computador')) || !empty($this->input->post('saida_check5computador')) || !empty($this->input->post('saida_check6computador')) || !empty($this->input->post('saida_check7computador')) || !empty($this->input->post('saida_check9computador'))){
              $selected = $this->input->post('computador');
              $list = array
              (
                "DEF",
                "OUT",
                $this->input->post('saida_check1computador'),
                $this->input->post('saida_check2computador'),
                $this->input->post('saida_check3computador'),
                $this->input->post('saida_check4computador'),
                $this->input->post('saida_check5computador'),
                $this->input->post('saida_check6computador'),
                $this->input->post('saida_check7computador'),
                $this->input->post('saida_check9computador')
              );
            }else
                return null;
            break;
          case "iMac":
        case "imac":
        case "mac":
        case "Mac":
            if(!empty($this->input->post('saida_check1iMac')) || !empty($this->input->post('saida_check2iMac')) || !empty($this->input->post('saida_check3iMac')) || !empty($this->input->post('saida_check4iMac')) || !empty($this->input->post('saida_check5iMac')) || !empty($this->input->post('saida_check6iMac')) || !empty($this->input->post('saida_check7iMac')) || !empty($this->input->post('saida_check8iMac')) || !empty($this->input->post('saida_check9iMac')) || !empty($this->input->post('saida_check10iMac')) || !empty($this->input->post('saida_check11iMac')) || !empty($this->input->post('saida_check12iMac')) || !empty($this->input->post('saida_check13iMac'))){
              $selected = $this->input->post('iMac');
              $list = array
              (
                "DEF",
                "OUT",
                $this->input->post('saida_check1iMac'),
                $this->input->post('saida_check2iMac'),
                $this->input->post('saida_check3iMac'),
                $this->input->post('saida_check4iMac'),
                $this->input->post('saida_check5iMac'),
                $this->input->post('saida_check6iMac'),
                $this->input->post('saida_check7iMac'),
                $this->input->post('saida_check8iMac'),
                $this->input->post('saida_check9iMac'),
                $this->input->post('saida_check10iMac'),
                $this->input->post('saida_check11iMac'),
                $this->input->post('saida_check12iMac'),
                $this->input->post('saida_check13iMac')
              );
            }else
                return null;
            break;
          case "drone":
          if(!empty($this->input->post('saida_check1drone')) || !empty($this->input->post('saida_check2drone')) || !empty($this->input->post('saida_check3drone')) || !empty($this->input->post('saida_check4drone')) || !empty($this->input->post('saida_check5drone')) || !empty($this->input->post('saida_check6drone')) || !empty($this->input->post('saida_check7drone')) || !empty($this->input->post('saida_check8drone')) || !empty($this->input->post('saida_check9drone')) || !empty($this->input->post('saida_check10drone')) || !empty($this->input->post('saida_check11drone')) || !empty($this->input->post('saida_check12drone')) || !empty($this->input->post('saida_check13drone')) || !empty($this->input->post('saida_check14drone')) || !empty($this->input->post('saida_check15drone')) || !empty($this->input->post('saida_check16drone')) || !empty($this->input->post('saida_check17drone')) || !empty($this->input->post('saida_check18drone')) || !empty($this->input->post('saida_check19drone')) || !empty($this->input->post('saida_check20drone'))){
              $selected = $this->input->post('computador');
              $list = array
              (
                "DEF",
                "OUT",
                $this->input->post('saida_check1drone'),
                $this->input->post('saida_check2drone'),
                $this->input->post('saida_check3drone'),
                $this->input->post('saida_check4drone'),
                $this->input->post('saida_check5drone'),
                $this->input->post('saida_check6drone'),
                $this->input->post('saida_check7drone'),
                $this->input->post('saida_check8drone'),
                $this->input->post('saida_check9drone'),
                $this->input->post('saida_check10drone'),
                $this->input->post('saida_check11drone'),
                $this->input->post('saida_check12drone'),
                $this->input->post('saida_check13drone'),
                $this->input->post('saida_check14drone'),
                $this->input->post('saida_check15drone'),
                $this->input->post('saida_check16drone'),
                $this->input->post('saida_check17drone'),
                $this->input->post('saida_check18drone'),
                $this->input->post('saida_check19drone'),
                $this->input->post('saida_check20drone'),
              ); 
            }else
                return null;
            break;
      } 
      
      $result = str_replace("\"", "", str_replace("true", "1", str_replace("false", "0", json_encode($list))));
      return str_replace("DEF", "\"".$selected."\"", str_replace("OUT", "\"".$defoutros."\"", str_replace("0", "\"0\"", str_replace("1", "\"1\"", $result))));
    }
    function editar($id, $iframe = false){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eEquipamento')){
           $this->session->set_flashdata('error','Você não tem permissão para editar serviços.');
           redirect(base_url());
        }

        $this->data['result'] = $this->equipamentos_model->getById($this->uri->segment(3));
        
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        if ($this->form_validation->run('equipamentos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        }else{
            date_default_timezone_set('America/Sao_Paulo');
            $entrega = $this->get_saida_Defeito();
            $data_entrega = !empty($entrega) ? date('Y-m-d H:i:s', time()) : NULL;
            $data = array
            (
                'equipamento' => $this->input->post('equipamento'),
                'marcas_id' => $this->input->post('marcas_id'),
                'mei' => $this->input->post('mei'),
                'modelo' => $this->input->post('modelo'),
                'clientes_id' => $this->input->post('clientes_id'),
                'obser' => $this->input->post('obser'),
                'tipo' => $this->input->post('equipamento'),
                'status' => $this->input->post('status'),
                'defeito' => $this->getDefeito(),
                'entrega' => $entrega,
            );

            if(empty($this->data['result']->data_entrega))
                $data['data_entrega'] = $data_entrega;
            
            if ($this->equipamentos_model->edit('equipamentos', $data, 'idEquipamentos', $this->input->post('idEquipamentos')) == TRUE){
                $this->session->set_flashdata('success', 'Equipamento editado com sucesso!');
                if ($this->input->post('save') == 'os')
                  redirect(base_url() . 'index.php/os/adicionar/?eq='.$this->input->post('idEquipamentos'));
                elseif ($this->input->post('save') == 'garantia')
                  redirect(base_url() . 'index.php/equipamentos/editar/'.$this->input->post('idEquipamentos').'/fecharGuia');
                elseif ($iframe)
                  redirect(base_url() . 'index.php/equipamentos/editar/'.$this->input->post('idEquipamentos').'/1');
                else
                  redirect(base_url() . 'index.php/equipamentos/editar/'.$this->input->post('idEquipamentos'));
            }
            else
            {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $garantiaGuia = $this->uri->segment(5);
        if(!empty($garantiaGuia))
            $this->data['garantiaGuia'] = true;
            
        $fecharGuia = $this->uri->segment(4);
        if(!empty($fecharGuia) && $fecharGuia == 'fecharGuia')
            $this->data['fecharGuia'] = true;

        $idFranquia=$this->session->userdata('id');
        $this->data['franquia'] = $this->franquias_model->getById($idFranquia);
        $this->data['view'] = 'equipamentos/editarEquipamento';
        $this->data['iframe'] = $iframe;
        $this->load->view('tema/topo', $this->data);
    }
    function visualizar() {
        
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamento')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar equipamentos.');
           redirect(base_url());
        }
        $this->data['result'] = $this->equipamentos_model->getById($this->uri->segment(3));
        
        $this->data['view'] = 'equipamentos/visualizarEquipamento';
        $this->load->view('tema/topo', $this->data);
     
    }
    function excluir(){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dEquipamento')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir serviços.');
           redirect(base_url());
        }
       
        
        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir serviço.');            
            redirect(base_url().'index.php/equipamentos/gerenciar/');
        }
        $this->db->where('equipamentos_id', $id);
        $this->db->delete('equipamentos_os');
        $this->equipamentos_model->delete('equipamentos','idEquipamentos',$id);             
        
        $this->session->set_flashdata('success','Equipamento excluido com sucesso!');            
        redirect(base_url().'index.php/equipamentos/gerenciar/');
    }
    public function autoCompleteCliente(){
        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->equipamentos_model->autoCompleteCliente($q);
        }
    }
    public function autoCompleteMarca(){
        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->equipamentos_model->autoCompleteMarca($q);
        }
    }
}
