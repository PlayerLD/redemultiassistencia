<?php

class Feedbacks extends CI_Controller {
    

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');

        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('feedbacks_model', '', TRUE);
        $this->data['menuFeedbacks'] = 'Avaliações';
    }
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vFeedback')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar avaliações.');
           redirect(base_url());
        }

        $this->load->library('pagination');
        
        
        $config['base_url'] = base_url().'index.php/feedbacks/gerenciar/';
        $config['total_rows'] = $this->feedbacks_model->count('feedbacks', $this->session->userdata('id'));
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->feedbacks_model->get('feedbacks','idFeedbacks,status,cliente_id,idFranquia,os,star,valor,dataFeedback','',$config['per_page'],$this->uri->segment(3));
       
	    $this->data['view'] = 'feedbacks/feedbacks';
       	$this->load->view('tema/topo',$this->data);

       
		
    }
/**	
   function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aFeedback')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar avaliações.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('feedbacks') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);

            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao'),
                'preco' => $preco
            );

            if ($this->feedbacks_model->add('feedbacks', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Serviço adicionado com sucesso!');
                redirect(base_url() . 'index.php/feedbacks/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'feedbacks/adicionarFeedback';
        $this->load->view('tema/topo', $this->data);

    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eFeedback')){
           $this->session->set_flashdata('error','Você não tem permissão para editar avaliações.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('feedbacks') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao'),
                'preco' => $preco
            );

            if ($this->feedbacks_model->edit('feedbacks', $data, 'idFeedbacks', $this->input->post('idFeedbacks')) == TRUE) {
                $this->session->set_flashdata('success', 'Serviço editado com sucesso!');
                redirect(base_url() . 'index.php/feedbacks/editar/'.$this->input->post('idFeedbacks'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->feedbacks_model->getById($this->uri->segment(3));

        $this->data['view'] = 'feedbacks/editarFeedback';
        $this->load->view('tema/topo', $this->data);

    }
*/	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dFeedback')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir avaliações.');
           redirect(base_url());
        }
       
        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir avaliação.');            
            redirect(base_url().'index.php/feedbacks/gerenciar/');
        }

               $this->feedbacks_model->delete('feedbacks','idFeedbacks',$id);             
        

        $this->session->set_flashdata('success','Avaliação excluido com sucesso!');            
        redirect(base_url().'index.php/feedbacks/gerenciar/');
    }
}

