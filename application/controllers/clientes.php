<?php
 
class Clientes extends CI_Controller {
    
    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br 
     * 
     */
    
    function __construct() {
        parent::__construct();
            if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('sistemaos/login');
            }
            $this->load->helper(array('codegen_helper'));
            $this->load->model('clientes_model','',TRUE);
	$this->load->model('os_model','',TRUE);
            $this->data['menuClientes'] = 'clientes';
	}	
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar clientes.');
           redirect(base_url());
        }
	        $countrows = $this->clientes_model->count('clientes', $this->session->userdata('id'));        
	    $this->data['results'] = $this->clientes_model->get('clientes','idClientes,idFranquia,nomeCliente,rg,documento,inscricaoEstadual,telefone,celular,sms,whatsapp,email,rua,numero,complemento,bairro,cidade,estado,cep,obs','',$countrows,$this->uri->segment(3));

       	
       	$this->data['view'] = 'clientes/clientes';
       	$this->load->view('tema/topo',$this->data);
	  
       
		
    }
	
    function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aCliente')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar clientes.');
           redirect(base_url());
        }
       $idFranquia=$this->session->userdata('id');

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
		
        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array( 
                'nomeCliente' => set_value('nomeCliente'),
                'idFranquia' => $idFranquia,
                'rg' => set_value('rg'),
                'documento' => set_value('documento'),
                'inscricaoEstadual' => $this->input->post('inscricaoEstadual'),
                'telefone' => set_value('telefone'),
                'celular' => $this->input->post('celular'),
                'sms' => $this->input->post('sms'),
                'whatsapp' => set_value('whatsapp'),
                'email' => set_value('email'),
                'rua' => set_value('rua'),
                'numero' => set_value('numero'),
                'complemento' => set_value('complemento'),
                'bairro' => set_value('bairro'),
                'cidade' => set_value('cidade'),
                'estado' => set_value('estado'),
                'cep' => set_value('cep'),
                'obs' => set_value('obs'),
                'dataCadastro' => date('Y-m-d')
            );
            //echo "<pre>";
            //var_dump($data);
            //echo "</pre>";

			$idCliente = $this->clientes_model->addReturnId('clientes', $data);
			
            if ($idCliente != FALSE) {
                $this->session->set_flashdata('success','Cliente adicionado com sucesso!');
				$this->session->set_flashdata('idCliente', $idCliente);
				 
                redirect(base_url() . 'index.php/equipamentos/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
		
        $this->data['view'] = 'clientes/adicionarCliente';
        $this->load->view('tema/topo', $this->data);
    }
	
	function inserirClienteAjax(){
		$idFranquia=$this->session->userdata('id');
        
		$data = array(
			'nomeCliente'=> $this->input->post('nomeCliente'),
			'idFranquia' => $idFranquia,
            'rg'         => $this->input->post('rgCliente'),
            'documento'  => $this->input->post('docCliente'),
			'inscricaoEstadual' => $this->input->post('inscricaoEstadual'),
			'telefone'   => $this->input->post('telefoneCliente'),
            'celular'    => $this->input->post('celularCliente'),
            'sms'        => $this->input->post('smsCliente'),
            'whatsapp'   => $this->input->post('whatsappCliente'),
			'email'      => $this->input->post('emailCliente'),
			'rua'        => $this->input->post('ruaCliente'),
			'numero'     => $this->input->post('numeroCliente'),
			'bairro'     => $this->input->post('bairroCliente'),
			'cidade'     => $this->input->post('cidadeCliente'),
			'estado'     => $this->input->post('estadoCliente'),
			'cep'        => $this->input->post('cepCliente'),
			'dataCadastro' => date('Y-m-d')
		);

		$idCliente = $this->clientes_model->addReturnId('clientes', $data);
			
        if ($idCliente != FALSE) {
        	$json = array("result" => true, "idCliente" => $idCliente, "nomeCliente" => $data["nomeCliente"]);
       	} else {
        	$json = array("result" => false);
      	}
		
		echo json_encode($json);	
	}
	
    function editar() {

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }


        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eCliente')){
           $this->session->set_flashdata('error','Você não tem permissão para editar clientes.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nomeCliente' => $this->input->post('nomeCliente'),
                'documento' => $this->input->post('documento'),
                'rg' => $this->input->post('rg'),
                'inscricaoEstadual' => $this->input->post('inscricaoEstadual'),
                'telefone' => $this->input->post('telefone'),
                'celular' => $this->input->post('celular'),
                'sms' => $this->input->post('sms'),
                'whatsapp' => $this->input->post('whatsapp'),
                'email' => $this->input->post('email'),
                'rua' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'complemento' => $this->input->post('complemento'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
                'cep' => $this->input->post('cep'),
                'obs' => $this->input->post('obs')

            );

            if ($this->clientes_model->edit('clientes', $data, 'idClientes', $this->input->post('idClientes')) == TRUE) {
                $this->session->set_flashdata('success','Cliente editado com sucesso!');
                redirect(base_url() . 'index.php/clientes/editar/'.$this->input->post('idClientes'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }


        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['view'] = 'clientes/editarCliente';
        $this->load->view('tema/topo', $this->data);

    }

    public function visualizar(){

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar clientes.');
           redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['results'] = $this->clientes_model->getOsByCliente($this->uri->segment(3));
        $this->data['results2'] = $this->clientes_model->getEquipamentoByCliente($this->uri->segment(3));
        $this->data['view'] = 'clientes/visualizar';
        $this->load->view('tema/topo', $this->data);

        
    }
	
    public function excluir(){

            
            if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dCliente')){
               $this->session->set_flashdata('error','Você não tem permissão para excluir clientes.');
               redirect(base_url());
            }

            
            $id =  $this->input->post('id');
            if ($id == null){

                $this->session->set_flashdata('error','Erro ao tentar excluir cliente.');            
                redirect(base_url().'index.php/clientes/gerenciar/');
            }

            //$id = 2;
            // excluindo OSs vinculadas ao cliente
            $this->db->where('clientes_id', $id);
            $os = $this->db->get('os')->result();

            if($os != null){

                foreach ($os as $o) {
                    $this->db->where('os_id', $o->idOs);
                    $this->db->delete('servicos_os');

                    $this->db->where('os_id', $o->idOs);
                    $this->db->delete('produtos_os');

                    $this->db->where('os_id', $o->idOs);
                    $this->db->delete('equipamentos_os');

                    $this->db->where('idOs', $o->idOs);
                    $this->db->delete('os');
                }
            }

           // excluindo Vendas vinculadas ao cliente
            $this->db->where('clientes_id', $id);
            $vendas = $this->db->get('vendas')->result();

            if($vendas != null){

                foreach ($vendas as $v) {
                    $this->db->where('vendas_id', $v->idVendas);
                    $this->db->delete('itens_de_vendas');


                    $this->db->where('idVendas', $v->idVendas);
                    $this->db->delete('vendas');
                }
            }

            //excluindo receitas vinculadas ao cliente
            $this->db->where('clientes_id', $id);
            $this->db->delete('lancamentos');



            $this->clientes_model->delete('clientes','idClientes',$id); 

            $this->session->set_flashdata('success','Cliente excluido com sucesso!');            
            redirect(base_url().'index.php/clientes/gerenciar/');
    }
    
    function getDefeito($arrdef)
    {
    	if (strtolower($arrdef[0]) == 'outros' && $arrdef[1])
    		return $arrdef[0].': '.$arrdef[1];
    	return $arrdef[0];
    }
}

