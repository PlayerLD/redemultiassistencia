<?php

class Vendas extends CI_Controller {
    
  
    /** 
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     *    
     */ 
    
    function __construct() {
        parent::__construct();
        
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
		
		$this->load->helper(array('form','codegen_helper'));
        $this->load->model('vendas_model','',TRUE);
        $this->load->model('os_model');
		$this->data['menuVendas'] = 'Vendas';
	}	 
	
	function index(){ 
		$this->gerenciar();
	}

	function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar vendas.');
           redirect(base_url());
        }

	    $countrows = $this->vendas_model->count('vendas', $this->session->userdata('id'));        
		$this->data['results'] = $this->vendas_model->get('vendas','*','',$countrows,$this->uri->segment(3));
       
	    $this->data['view'] = 'vendas/vendas';
       	$this->load->view('tema/topo',$this->data);
		
    }

    // server side

    function getlists(){

        $columns = array(
                0 => 'idVendas',
                1 => 'dataVenda',
                2 => 'cliente',
                3 => 'faturado',
                4 => 'chaveNfe',
        );
                
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->vendas_model->count_totalVendas();


        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value'])){
            $posts = $this->vendas_model->allVendas($limit,$start,$order,$dir);
        }else{
            $search = $this->input->post('search')['value'];

            $posts  = $this->vendas_model->idVendas_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->vendas_model->vendas_search_count($search);
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['idVendas'] = $post->idVendas;
                $nestedData['dataVenda'] = date('d/m/Y', strtotime($post->dataVenda));
                $nestedData['cliente'] = '<a href="'.base_url().'index.php/clientes/visualizar/'.$post->clientes_id.'">'.$post->cliente.'</a>';
                $nestedData['faturado'] = ($post->faturado == 1 ? '<span class="label label-success">Sim</span>' : '<span class="label label-important">Não</span>');
                $nestedData['chaveNfe'] = ($post->chaveNfe == null ? "SEM REGISTRO" : $post->chaveNfe.' - '.'<a href="'.base_url().'index.php/vendas/download/'.$post->chaveNfe.'" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>');
               
                // adicionando as funções do botões de ações
                $buttons = ' ';
                if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
                    $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/vendas/visualizar/'.$post->idVendas.'" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>';
                }
                if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                    $buttons = $buttons.'<a style="margin-right: 1%" href="'.base_url().'index.php/vendas/editar/'.$post->idVendas.'" class="btn btn-info tip-top" title="Editar venda"><i class="icon-pencil icon-white"></i></a>';
                }
                if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                    $buttons = $buttons.'<a target="_blank" style="margin-right: 1%" href="'.base_url().'agile/gerar-nfe.php?idVenda='.$post->idVendas.'&idFranquia='.$this->session->userdata('id').'" class="btn  btn-warning" title="Gerar Nfe"><i class="icon-file icon-white"></i></a>';
                }
                if($this->permission->checkPermission($this->session->userdata('permissao'),'dVenda')){
                    $buttons = $buttons.'<a href="#modal-excluir" role="button" data-toggle="modal" venda="'.$post->idVendas.'" class="btn btn-danger tip-top" title="Excluir Venda"><i class="icon-remove icon-white"></i></a>';
                }                    
            
            // coluna contendo todos os botões de ações
            $nestedData['todosB'] = $buttons;

                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }

    // end server side


    function ArquivosDanfes(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar vendas.');
           redirect(base_url());
        }

        $countrows = $this->vendas_model->count('vendas', $this->session->userdata('id'));        
        $this->data['results'] = $this->vendas_model->get('vendas','*','',$countrows,$this->uri->segment(3));
       
        $this->data['view'] = 'vendas/loteArqPerDanfe';
        $this->load->view('tema/topo',$this->data);
    }
	
    function adicionar($id_produto = '', $qty = 1){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){
          $this->session->set_flashdata('error','Você não tem permissão para adicionar Vendas.');
          redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('vendas') == false) {
           $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $dataVenda = $this->input->post('dataVenda');

            try {
                
                $dataVenda = explode('/', $dataVenda);
                $dataVenda = $dataVenda[2].'-'.$dataVenda[1].'-'.$dataVenda[0];


            } catch (Exception $e) {
               $dataVenda = date('Y/m/d'); 
            }

            $idFranquia = $this->session->userdata('id');
            $data = array(
                'idFranquia' => $idFranquia,
                'dataVenda' => $dataVenda,
                'clientes_id' => $this->input->post('clientes_id'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'faturado' => 0
            );

            if (is_numeric($id = $this->vendas_model->add('vendas', $data, true)) ) {

                if(!empty($id_produto)){
                    $produto = $this->vendas_model->getProduto($id_produto);
                    $qty = !empty($this->input->get('qty')) ? $this->input->get('qty') : $qty;
                    if(!empty($produto)){
                        $preco = $produto->precoVenda;
                        $quantidade = $qty;
                        $subtotal = $preco * $quantidade;
                        $produto = $id_produto;
                        $data = array(
                            'quantidade'=> $quantidade,
                            'subTotal'=> $subtotal,
                            'produtos_id'=> $produto,
                            'vendas_id'=> $id,
                        );

                        if($this->vendas_model->add('itens_de_vendas', $data) == true){
                            $sql = "UPDATE produtos set estoque = estoque - ? WHERE idProdutos = ?";
                            $this->db->query($sql, array($quantidade, $produto));
                        }
                    }
                }

                $this->session->set_flashdata('success','Venda iniciada com sucesso, adicione os produtos.');
                redirect('vendas/editar/'.$id);

            } else {
                
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
         
        $this->data['view'] = 'vendas/adicionarVenda';
        $this->load->view('tema/topo', $this->data);
    }
    

    
    function editar() {

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
          $this->session->set_flashdata('error','Você não tem permissão para editar vendas');
          redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('vendas') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $dataVenda = $this->input->post('dataVenda');

            try {
                
                $dataVenda = explode('/', $dataVenda);
                $dataVenda = $dataVenda[2].'-'.$dataVenda[1].'-'.$dataVenda[0];


            } catch (Exception $e) {
               $dataVenda = date('Y/m/d'); 
            }

            $data = array(
                'dataVenda' => $dataVenda,
                'usuarios_id' => $this->input->post('usuarios_id'),
                'clientes_id' => $this->input->post('clientes_id')
            );

            if ($this->vendas_model->edit('vendas', $data, 'idVendas', $this->input->post('idVendas')) == TRUE) {
                $this->session->set_flashdata('success','Venda editada com sucesso!');
                redirect(base_url() . 'index.php/vendas/editar/'.$this->input->post('idVendas'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['result'] = $this->vendas_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->vendas_model->getProdutos($this->uri->segment(3));
        $this->data['view'] = 'vendas/editarVenda';
        $this->load->view('tema/topo', $this->data);
   
    }

    public function visualizar(){

        $idFranquia = $this->session->userdata('id');

        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar vendas.');
          redirect(base_url());
        }
		
		if($this->input->get('nfe-sempermissao') == "true"){
			$this->data['custom_error'] = 'Você não em autorização par Emissão de Notas Fiscais, por favor entre em contato com o administrador';
		}else{
			$this->data['custom_error'] = '';	
		}
		
        $this->load->model('sistemaos_model');
        $this->data['result'] = $this->vendas_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->vendas_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->sistemaos_model->getEmitente($idFranquia);
        
        $this->data['view'] = 'vendas/visualizarVenda';
        $this->load->view('tema/topo', $this->data);
       
    }
	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dVenda')){
          $this->session->set_flashdata('error','Você não tem permissão para excluir vendas');
          redirect(base_url());
        }
        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir venda.');            
            redirect(base_url().'index.php/vendas/gerenciar/');
        }

        $this->db->where('vendas_id', $id);
        $this->db->delete('itens_de_vendas');

        $this->db->where('idVendas', $id);
        $this->db->delete('vendas');           

        $this->session->set_flashdata('success','Venda excluída com sucesso!');            
        redirect(base_url().'index.php/vendas/gerenciar/');

    }

    public function autoCompleteProduto(){
        
        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->vendas_model->autoCompleteProduto($q);
        }

    }

    public function autoCompleteCliente(){

        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->vendas_model->autoCompleteCliente($q);
        }

    }

    public function autoCompleteUsuario(){

        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->vendas_model->autoCompleteUsuario($q);
        }

    }



    public function adicionarProduto(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
          $this->session->set_flashdata('error','Você não tem permissão para editar vendas.');
          redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idProduto', 'Produto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idVendasProduto', 'Vendas', 'trim|required|xss_clean');
        
        if($this->form_validation->run() == false){
           echo json_encode(array('result'=> false)); 
        }
        else{

            $preco = $this->input->post('preco');
            $quantidade = $this->input->post('quantidade');
            $subtotal = $preco * $quantidade;
            $produto = $this->input->post('idProduto');
            $data = array(
                'quantidade'=> $quantidade,
                'subTotal'=> $subtotal,
                'produtos_id'=> $produto,
                'vendas_id'=> $this->input->post('idVendasProduto'),
            );

            if($this->vendas_model->add('itens_de_vendas', $data) == true){
                $sql = "UPDATE produtos set estoque = estoque - ? WHERE idProdutos = ?";
                $this->db->query($sql, array($quantidade, $produto));
                
                echo json_encode(array('result'=> true));
            }else{
                echo json_encode(array('result'=> false));
            }

        }
        
      
    }

    function excluirProduto(){

            if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
              $this->session->set_flashdata('error','Você não tem permissão para editar Vendas');
              redirect(base_url());
            }

            $ID = $this->input->post('idProduto');
            if($this->vendas_model->delete('itens_de_vendas','idItens',$ID) == true){
                
                $quantidade = $this->input->post('quantidade');
                $produto = $this->input->post('produto');


                $sql = "UPDATE produtos set estoque = estoque + ? WHERE idProdutos = ?";

                $this->db->query($sql, array($quantidade, $produto));
                
                echo json_encode(array('result'=> true));
            }
            else{
                echo json_encode(array('result'=> false));
            }           
    }



    public function faturar() {

        $idFranquia = $this->session->userdata('id');

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
              $this->session->set_flashdata('error','Você não tem permissão para editar Vendas');
              redirect(base_url());
            }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
 

        if ($this->form_validation->run('receita') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {


            $vencimento = $this->input->post('vencimento');
            $recebimento = $this->input->post('recebimento');

            try {
                
                $vencimento = explode('/', $vencimento);
                $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0];

                if($recebimento != null){
                    $recebimento = explode('/', $recebimento);
                    $recebimento = $recebimento[2].'-'.$recebimento[1].'-'.$recebimento[0];

                }
            } catch (Exception $e) {
               $vencimento = date('Y/m/d'); 
            }


            $data = array(
                'idFranquia' => $idFranquia,
                'descricao' => set_value('descricao'),
                'valor' => $this->input->post('valor'),
                'clientes_id' => $this->input->post('clientes_id'),
                'data_vencimento' => $vencimento,
                'data_pagamento' => $recebimento,
                'baixado' => $this->input->post('recebido'),
                'cliente_fornecedor' => set_value('cliente'),
                'forma_pgto' => $this->input->post('formaPgto'),
                'tipo' => $this->input->post('tipo')
            );

            if ($this->vendas_model->add('lancamentos',$data) == TRUE) {
                
                $venda = $this->input->post('vendas_id');

                $this->db->set('faturado',1);
                $this->db->set('valorTotal',$this->input->post('valor'));
                $this->db->where('idVendas', $venda);
                $this->db->update('vendas');

                $this->session->set_flashdata('success','Venda faturada com sucesso!');
                $json = array('result'=>  true);
                echo json_encode($json);
                die();
            } else {
                $this->session->set_flashdata('error','Ocorreu um erro ao tentar faturar venda.');
                $json = array('result'=>  false);
                echo json_encode($json);
                die();
            }
        }

        $this->session->set_flashdata('error','Ocorreu um erro ao tentar faturar venda.');
        $json = array('result'=>  false);
        echo json_encode($json);
        
    }
    
    // implementaçãoes do XML's e DANFE's

    public function download($id = null){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        if($id == null || !is_numeric($id)){
            $this->session->set_flashdata('error','Erro! O XML não pode ser localizado.');
            redirect(base_url() . 'index.php/vendas/');
        }

        $file = $this->vendas_model->getByIdNumNFE($id);
        
        // Define o tempo máximo de execução em 0 para as conexões lentas
        set_time_limit(0);
        // Arqui você faz as validações e/ou pega os dados do banco de dados
        $aquivoNome = $id.$file->numNFE.'.xml'; // nome do arquivo que será enviado p/ download
        $arquivoLocal = base_url('assets/arquivos/xml/nfe'.$id.$file->numNFE.'.xml'); // caminho absoluto do arquivo

        // Definimos o novo nome do arquivo
        $novoNome = 'nfe'.$id.$file->numNFE.'.xml';
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$novoNome.'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        //header('Content-Length: '.filesize($aquivoNome));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        // Envia o arquivo para o cliente
        readfile($arquivoLocal);
        exit;
        
    }

    public function downloaddanfeone($id = null){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        if($id == null || !is_numeric($id)){
            $this->session->set_flashdata('error','Erro! O XML não pode ser localizado.');
            redirect(base_url() . 'index.php/vendas/');
        }

        $file = $this->vendas_model->getByIdNumNFE($id);
        
        // Define o tempo máximo de execução em 0 para as conexões lentas
        set_time_limit(0);
        // Arqui você faz as validações e/ou pega os dados do banco de dados
        $aquivoNome = $id.$file->numNFE.'.pdf'; // nome do arquivo que será enviado p/ download
        $arquivoLocal = base_url('assets/arquivos/danfe/nfe'.$id.$file->numNFE.'.pdf'); // caminho absoluto do arquivo

        // Definimos o novo nome do arquivo
        $novoNome = 'nfe'.$id.$file->numNFE.'.pdf';
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$novoNome.'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        //header('Content-Length: '.filesize($aquivoNome));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        // Envia o arquivo para o cliente
        readfile($arquivoLocal);
        exit;
        
    }

    public function loteArqPer(){
        $this->PesquisLoteDataArquivo();
    }

    public function PesquisLoteDataArquivo(){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        $idFranquia = $this->session->userdata('id');
        // recebendo e transformando datas
        $dataInicial  = $this->input->post('dataInicial');
        //$datainicial = explode('/', $dataInicial);
        //$dtInicial = $dataInicial[2] . '-' . $dataInicial[1] . '-' . $dataInicial[0];
        
        $dataFinal    = $this->input->post('dataFinal');
        //$datafinal = explode('/', $dataFinal);
        //$dtFinal = $dataFinal[2] . '-' . $dataFinal[1] . '-' . $dataFinal[0];

        $arq = $this->vendas_model->LoteDataArquivos($idFranquia,$dataInicial,$dataFinal);
        
        //var_dump($arq);

        if($arq){
           
            $ext1  = 'nfe'.$idFranquia;
            $ext2  = '.xml';
            $this->load->library('zip');
            $this->load->helper('file');
        
            $path = 'assets/arquivos/xml/';
        
            $files = get_filenames($path);
        
            //var_dump($files);
            foreach ($files as $f) {
                foreach ($arq as $a) {
                    $arquivo = $ext1.$a->numNFE.$ext2;
                    if ($f == $arquivo) {
                        $this->zip->read_file($path.$f, false);
        
                    }
                }
            }
            //echo "<pre>";
            //var_dump($a);
            //echo "</pre>";
            $this->zip->download('XMLarquivos.zip');
            $this->data['view'] = 'vendas/loteArqPer';
            $this->load->view('tema/topo',$this->data);

        }else if($arq == false){

            if($dataInicial && $dataFinal){
                $this->data['custom_error'] = 'Verifique o período informado, sem registros encontrados.';
            }
            $this->data['menuVendas'] = 'vendas';
            $this->data['view'] = 'vendas/loteArqPer';   
            $this->load->view('tema/topo',$this->data);
        }



    }

    public function loteArqPerDanfeAll(){
        $this->PesquisLoteDataArquivodanfeall();
    }

    public function PesquisLoteDataArquivodanfeall(){
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vXML')){
          $this->session->set_flashdata('error','Você não tem permissão para visualizar XML.');
          redirect(base_url());
        }

        $idFranquia = $this->session->userdata('id');
        // recebendo e transformando datas
        $dataInicial  = $this->input->post('dataInicial');
        //$datainicial = explode('/', $dataInicial);
        //$dtInicial = $dataInicial[2] . '-' . $dataInicial[1] . '-' . $dataInicial[0];
        
        $dataFinal    = $this->input->post('dataFinal');
        //$datafinal = explode('/', $dataFinal);
        //$dtFinal = $dataFinal[2] . '-' . $dataFinal[1] . '-' . $dataFinal[0];

        $arq = $this->vendas_model->LoteDataArquivosDanfe($idFranquia,$dataInicial,$dataFinal);
        
        //var_dump($arq);

        if($arq){
           
            $ext1  = 'nfe'.$idFranquia;
            $ext2  = '.pdf';
            $this->load->library('zip');
            $this->load->helper('file');
        
            $path = 'assets/arquivos/danfe/';
        
            $files = get_filenames($path);
        
            //var_dump($files);
            foreach ($files as $f) {
                foreach ($arq as $a) {
                    $arquivo = $ext1.$a->numNFE.$ext2;
                    if ($f == $arquivo) {
                        $this->zip->read_file($path.$f, false);
        
                    }
                }
            }
            //echo "<pre>";
            //var_dump($a);
            //echo "</pre>";
            $this->zip->download('DANFEarquivos.zip');
            $this->data['view'] = 'vendas/loteArqPerDanfeAll';
            $this->load->view('tema/topo',$this->data);

        }else if($arq == false){

            if($dataInicial && $dataFinal){
                $this->data['custom_error'] = 'Verifique o período informado, sem registros encontrados.';
            }
            

            $this->data['menuVendas'] = 'loteArqPerDanfeAll';
            $this->data['view'] = 'vendas/loteArqPerDanfeAll';   
            $this->load->view('tema/topo',$this->data);
            //redirect(base_url() . 'index.php/xml/');
        }



    }
}

