<?php

class Atendentes extends CI_Controller {
    

    /**
     * author: Raphael Paulino
     * email: raphael.paulino.web@gmail.com
     * 
     */
    
    public function __construct() 
    {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cAtendente')){
          $this->session->set_flashdata('error','Você não tem permissão para configurar os atendentes.');
          redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('atendentes_model', '', TRUE);
        $this->load->model('os_model');
        $this->data['menuConfiguracoes'] = 'atendentes';
    }

    function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/atendentes/gerenciar/';
        $config['total_rows'] = $this->atendentes_model->count('atendentes');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';	
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->atendentes_model->get($config['per_page'],$this->uri->segment(3));
	    $this->data['view']    = 'atendentes/atendentes';
       	$this->load->view('tema/topo',$this->data);
    }
	
    public function adicionar(){  
          
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('atendentes') == false) {

             $this->data['custom_error'] = (validation_errors() ? '<div class="alert alert-danger">'.validation_errors().'</div>' : false);

        } else {     

            $this->load->library('encrypt');

            $idFranquia = $this->session->userdata('id');
                  
            $data = array(
                'idFranquia' => $idFranquia,
                'nomeAtendente' => set_value('nomeAtendente'),
                'rg' => set_value('rg'),
                'documento' => set_value('documento'),
                'telefone' => set_value('telefone'),
                'celular' => set_value('celular'),
                'email' => set_value('email'),
                'cep' => set_value('cep'),
                'rua' => set_value('rua'),
                'numero' => set_value('numero'),
                'complemento' => set_value('complemento'),
                'bairro' => set_value('bairro'),
                'cidade' => set_value('cidade'),
                'estado' => set_value('estado'),
                // 'senha' => $this->encrypt->sha1($this->input->post('senha')),
                // 'situacao' => set_value('situacao'),
                // 'permissoes_id' => $this->input->post('permissoes_id'),
                'obs' => set_value('obs'),
                'dataCadastro' => date('Y-m-d')
            );
           
			if ($this->atendentes_model->add('atendentes', $data)) {

                $this->session->set_flashdata('success', 'Atendente cadastrado com sucesso!');
                // $this->data['custom_error'] = 'Atendente cadastrado com sucesso!';
                redirect(base_url() . 'index.php/atendentes/adicionar/');

			} else {

                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
                
            }
            
		}
        
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome');   
		$this->data['view'] = 'atendentes/adicionarAtendente';
        $this->load->view('tema/topo',$this->data);
       
    }	
    
    public function editar() 
    {
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pôde ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eAtendente')){
           $this->session->set_flashdata('error','Você não tem permissão para editar atendentes.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->load->model('permissoes_model');
        $this->data['custom_error'] = '';
        
        $this->form_validation->set_rules('nomeAtendente', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rg', 'RG', 'trim|xss_clean');
        $this->form_validation->set_rules('documento', 'CPF/CNPJ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rua', 'Rua', 'trim|xss_clean');
        $this->form_validation->set_rules('numero', 'Número', 'trim|xss_clean');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|xss_clean');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|xss_clean');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|xss_clean');
        $this->form_validation->set_rules('situacao', 'Situação', 'trim|xss_clean');
        $this->form_validation->set_rules('permissoes_id', 'Permissão', 'trim|xss_clean');

        if($this->form_validation->run() == false):

             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        else:

            // if($this->input->post('idAtendentes') == 1 && $this->input->post('situacao') == 0):

            //     $this->session->set_flashdata('error','O usuário super admin não pode ser desativado!');
            //     redirect(base_url().'index.php/atendentes/editar/'.$this->input->post('idAtendentes'));

            // endif;

            // $senha = $this->input->post('senha');

            $data = array(
                'nomeAtendente' => $this->input->post('nomeAtendente'),
                'rg' => $this->input->post('rg'),
                'documento' => $this->input->post('documento'),
                'rua' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
                'email' => $this->input->post('email'),
                'telefone' => $this->input->post('telefone'),
                'celular' => $this->input->post('celular'),
                // 'situacao' => $this->input->post('situacao'),
                // 'permissoes_id' => $this->input->post('permissoes_id')
            );

            // if(isset($senha) && !is_null($senha) && !empty($senha)):

            //     $this->load->library('encrypt');
            //     $data['senha'] = $this->encrypt->sha1($senha);

            // endif;
           
			if($this->atendentes_model->edit('atendentes', $data, 'idAtendentes', $this->input->post('idAtendentes')) == TRUE):

                $this->session->set_flashdata('success','Atendente editado com sucesso!');
                redirect(base_url().'index.php/atendentes/editar/'.$this->input->post('idAtendentes'));
                
			else:

                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
                
            endif;
            
        endif;

        $this->data['result']     = $this->atendentes_model->getById($this->uri->segment(3));
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome'); 
		$this->data['view']       = 'atendentes/editarAtendente';
        $this->load->view('tema/topo',$this->data);
    }
	
    public function excluir(){

            $ID =  $this->uri->segment(3);
            $this->atendentes_model->delete('atendentes','idAtendentes',$ID);             
            redirect(base_url().'index.php/atendentes/gerenciar/');
    }
}

