<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Franquias extends CI_Controller {
    

    /**
     * author: Ramon Silva  
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {

        parent::__construct();

        // echo "<pre>";
        // print_r($this->session->all_userdata());
        // echo "</pre>";
        // exit;

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cFranquia')){
          $this->session->set_flashdata('error','Você não tem permissão para configurar os usuários.');
          redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('franquias_model', '', TRUE);
        $this->load->model('planos_model', '', TRUE);
		$this->load->model('marcas_model', '', TRUE);
        $this->data['menuFranquias'] = 'franquias';
    }

    function index(){
		$this->gerenciar();       
	}

    function gerenciar(){
        
    $countrows = $this->franquias_model->count('franquias');
        $this->data['results'] = $this->franquias_model->get($countrows,$this->uri->segment(3));
       
    $this->data['view'] = 'franquias/franquias';
        $this->load->view('tema/topo',$this->data);

       
        
    }

	function filiais($id){
	$countrows = $this->franquias_model->count('franquias', $id);        

        $this->data['results'] = $this->franquias_model->listar(array('id_pai' => $id));
       
    $this->data['id_filial'] = $id;
	$this->data['view'] = 'franquias/franquias';
       	$this->load->view('tema/topo',$this->data);

       
		
    }
	
    public function adicionar($id_filial = NULL){
          
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('franquias') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="alert alert-danger">'.validation_errors().'</div>' : false);

        } else
        {
            $equips = [];

            if(!empty($_POST['checkCelular']))
                $equips[] = 'Celular';

            if(!empty($_POST['checkTablet']))
                $equips[] = 'Tablet';

            if(!empty($_POST['checkVideogame']))
                $equips[] = 'Videogame';

            if(!empty($_POST['checkNotebook']))
                $equips[] = 'Notebook';

            if(!empty($_POST['checkTv']))
                $equips[] = 'Tv';

            if(!empty($_POST['checkComputador']))
                $equips[] = 'Computador';

            if(!empty($_POST['checkimac']))
                $equips[] = 'iMac';

             if(!empty($_POST['checkmac']))
                $equips[] = 'Mac';
 
            $this->load->library('encrypt');                       
            $data = array(
                    'id_pai'               => $id_filial,
                    'nome'                 => set_value('nome'),
                    'rg'                   => set_value('rg'),
                    'cpf'                  => set_value('cpf'),
                    'cep'                  => set_value('cep'),
                    'rua'                  => set_value('rua'),
                    'id_plano'             => $this->input->post('plano'),
                    'id_plano2'            => $this->input->post('plano2'),
                    'numero'               => set_value('numero'),
                    'bairro'               => set_value('bairro'),
                    'cidade'               => set_value('cidade'),
                    'estado'               => set_value('estado'),
                    'email'                => set_value('email'),
                    'senha'                => $this->encrypt->sha1($this->input->post('senha')),
                    'telefone'             => set_value('telefone'),
                    'celular'              => $this->input->post('celular'),
                    'slotsp'               => $this->input->post('slotsp'),
                    'slotsm'               => set_value('slotsm'),
                    'slotsg'               => set_value('slotsg'),
                    'fileiras_slots'       => set_value('fileiras_slots'),
                    'qtd_fileiras_slots_p' => $this->input->post('qtd_fileiras_slots_p'),
                    'qtd_fileiras_slots_m' => $this->input->post('qtd_fileiras_slots_m'),
                    'qtd_fileiras_slots_g' => $this->input->post('qtd_fileiras_slots_g'),
                    'situacao'             => set_value('situacao'),
                    'nivel'                => $this->input->post('nivel'),
                    'permissoes_id'        => $this->input->post('permissoes_id'),
                    'statusOrcamento'      => $this->input->post('statusOrcamento'),
                    'linkgoogle'           => $this->input->post('linkgoogle'),
                    'dataCadastro'         => date('Y-m-d'),
                    'permissoes_equip'     => serialize($equips)
            );
            
            $idFranquia = $this->franquias_model->add('franquias',$data);
            // quando chegar aqui os dados dos slotes, eles são redirecionados para functions add que fica em os_model.php, lá é feito todo o processamento dos dados e contagem dos slotes para adcionar os slotes.
            
            if ($idFranquia !== FALSE):
                
                if (!empty($_POST['nome_cartao']) && !empty($_POST['num_cartao']) && 
                        !empty($_POST['mes_cartao']) && !empty($_POST['ano_cartao']) && 
                            !empty($_POST['cvv_cartao'])):

                    $data_cartao = array(
                            'franquia_id'      => $idFranquia,
                            'id_plano'         => $this->input->post('plano'),
                            'nome_cartao'      => $this->input->post('nome_cartao'),
                            'cpf_cartao'       => $this->input->post('cpf_cartao'),
        					'num_cartao'       => $this->input->post('num_cartao'),
        					'mes_cartao'       => $this->input->post('mes_cartao'),
        					'ano_cartao'       => $this->input->post('ano_cartao'),
                            'cvv_cartao'       => $this->input->post('cvv_cartao'),
                    );

                    $cad_cartao = $this->franquias_model->addCartao($data_cartao);

                    if(!$cad_cartao):
                        $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
                    endif;

                endif;

                $this->addMarcas($idFranquia);
                
                $this->session->set_flashdata('success','Franquia cadastrada com sucesso!');
                redirect(base_url() . 'index.php/franquias/adicionar/');
                
            else:

				$this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';

			endif;
		}
        
        $this->load->model('permissoes_model');
        $this->data['permissoes']= $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome');   
        // data['franquias'] verificar se está sendo utilizado
        $this->data['franquias'] = $this->franquias_model->listar();
        $this->data['planos'] = $this->planos_model->listar();
        $this->data['menuFranquias'] = 'adicionar';

	    $this->data['view'] = 'franquias/adicionarFranquia';
        $this->load->view('tema/topo',$this->data);
       
    }	
    
	public function addMarcas($idFranquia){
		 $data = array(
		 	'idFranquia' => $idFranquia,
            'nome' => 'Acer',
            'status' => 'ativo',   
       	 );
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Apple';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Asus';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'CCE';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Dell';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'DL';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Lenovo';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'LG';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Microsoft';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Motorola';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Multilaser';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Navcity';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Nintendo';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Nokia';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Outras';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Positivo';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Samsung';
		 $this->marcas_model->add('marcas', $data);
		 
		 $data['nome'] = 'Sony';
		 $this->marcas_model->add('marcas', $data);

         $data['nome'] = 'Beats';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Knup';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = '3M';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'GoPro. Baseus';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Kimaster';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Rino';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'OEX';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Motomo';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Geek';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Razer';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Republic Of Gamers';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Corsair';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'EVGA';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Genius';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'GX';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Logitech';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Steelseries';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'TP-Link';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'D-Link';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Kingston';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Microsoft';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'AMD';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Itautec';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Elgin';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Tectoy';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'PC Ware';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'MyMax';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Markvision';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Sandisk';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Lexmark';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'HP';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Bematech';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Seagate';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Canon';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Brother';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Xerox';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Leadership';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Intelbras';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Nvidia';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Epson';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Gigabyte';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Clone';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'ATI';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Cooler Master';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'IBM';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Acer';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Asus';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Multilaser';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'MaxPrint';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Bosch';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'C3 Tech';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Philco';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'AOC';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'STI';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Toshiba';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Philips';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'CCE';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Panasonic';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Semp Toshiba';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'JBL';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Aiwa';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Buster';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Fujitsu';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'PC Chips';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Intel';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Positivo';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Hitachi';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'TCL';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Sharp';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'NEC';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Sanyo';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Yamaha';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Inova';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Yaxun';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Belkin';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'Clicktech';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'AWEi';
            $this->marcas_model->add('marcas', $data);

        $data['nome'] = 'KL GO';
            $this->marcas_model->add('marcas', $data);
	}

    public function editar(){  
        
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rg', 'RG', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rua', 'Rua', 'trim|required|xss_clean');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('slotsp', 'Slot Pequeno', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('slotsm', 'Slot Médio', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('slotsg', 'Slot Grande', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('fileiras_slots', 'Qtd. por fileira', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('qtd_fileiras_slots_p', 'Qtd. de Fileiras para os Slots Pequenos', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('qtd_fileiras_slots_m', 'Qtd. de Fileiras para os Slots Médios', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('qtd_fileiras_slots_g', 'Qtd. de Fileiras para os Slots Grandes', 'trim|required|xss_clean');
        $this->form_validation->set_rules('situacao', 'Situação', 'trim|required|xss_clean');
        $this->form_validation->set_rules('permissoes_id', 'Permissão', 'trim|required|xss_clean');
        $this->form_validation->set_rules('statusOrcamento','Orçamentos Site','required|trim|xss_clean');
        $this->form_validation->set_rules('linkgoogle','Informar link de Avaliação do Gooogle','trim|xss_clean');

        if ($this->form_validation->run('franquias') == false) :
  
             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        else:

            if ($this->input->post('idFranquias') == 1 && $this->input->post('situacao') == 0) {
                $this->session->set_flashdata('error','O usuário super admin não pode ser desativado!');
                redirect(base_url().'index.php/franquias/editar/'.$this->input->post('idFranquias'));
            }

            $senha  = $this->input->post('senha'); 
            $equips = [];

            if(!empty($_POST['checkCelular']))
                $equips[] = 'Celular';

            if(!empty($_POST['checkTablet']))
                $equips[] = 'Tablet';

            if(!empty($_POST['checkVideogame']))
                $equips[] = 'Videogame';

            if(!empty($_POST['checkNotebook']))
                $equips[] = 'Notebook';

            if(!empty($_POST['checkTv']))
                $equips[] = 'Tv';

            if(!empty($_POST['checkComputador']))
                $equips[] = 'Computador';

            if(!empty($_POST['checkimac']))
                $equips[] = 'iMac';

            if(!empty($_POST['checkmac']))
                $equips[] = 'Mac';
            

            $data = array(
                'nome'                 => $this->input->post('nome'),
                'rg'                   => $this->input->post('rg'),
                'cpf'                  => $this->input->post('cpf'),
                'cep'                  => $this->input->post('cep'),
                'rua'                  => $this->input->post('rua'),
                'id_plano'             => $this->input->post('plano'),
                'id_plano2'            => $this->input->post('plano2'),
                'numero'               => $this->input->post('numero'),
                'bairro'               => $this->input->post('bairro'),
                'cidade'               => $this->input->post('cidade'),
                'estado'               => $this->input->post('estado'),
                'email'                => $this->input->post('email'),
                'telefone'             => $this->input->post('telefone'),
                'celular'              => $this->input->post('celular'),
                //'slotsp'               => $this->input->post('slotsp'),
                //'slotsm'               => $this->input->post('slotsm'),
                //'slotsg'               => $this->input->post('slotsg'),
                //'fileiras_slots'       => $this->input->post('fileiras_slots'),
                //'qtd_fileiras_slots_p' => $this->input->post('qtd_fileiras_slots_p'),
                //'qtd_fileiras_slots_m' => $this->input->post('qtd_fileiras_slots_m'),
                //'qtd_fileiras_slots_g' => $this->input->post('qtd_fileiras_slots_g'),
                'situacao'             => $this->input->post('situacao'),
                'permissoes_id'        => $this->input->post('permissoes_id'),
                'statusOrcamento'      => $this->input->post('statusOrcamento'),
                'linkgoogle'           => $this->input->post('linkgoogle'),
                'permissoes_equip'     => serialize($equips)
            );

            if($senha != null){

                $this->load->library('encrypt');   
                $senha         = $this->encrypt->sha1($senha);
                $data['senha'] = $senha;

            }

            if ($this->franquias_model->edit('franquias', $data, 'idFranquias', $this->input->post('idFranquias')) == TRUE):

     
                if (!empty($_POST['nome_cartao']) && !empty($_POST['num_cartao']) && 
                        !empty($_POST['mes_cartao']) && !empty($_POST['ano_cartao']) && 
                            !empty($_POST['cvv_cartao'])):

                    $data_cartao = array(
                            'franquia_id' => $this->input->post('idFranquias'),
                            'nome_cartao' => $this->input->post('nome_cartao'),
                            'cpf_cartao'  => $this->input->post('cpf_cartao'),
                            'num_cartao'  => $this->input->post('num_cartao'),
                            'mes_cartao'  => $this->input->post('mes_cartao'),
                            'ano_cartao'  => $this->input->post('ano_cartao'),
                            'cvv_cartao'  => $this->input->post('cvv_cartao'),
                    );

                    $cad_cartao = $this->franquias_model->addCartao($data_cartao);

                    // if(!$cad_cartao):
                    //     $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
                    // endif;

                endif;
                
                $this->session->set_flashdata('success','Franquia editada com sucesso!');
                redirect(base_url() . 'index.php/franquias/editar/' . $this->input->post('idFranquias'));
                
			else:

				$this->data['custom_error'] = '<div class="form_error programador" ><p>Ocorreu um erro: '.error_reporting(E_ALL).'</p></div>';

            endif;

        endif;


        // $this->load->model('novos_slots_model');
        // echo '<pre>';
        // var_dump($this->novos_slots_model->obterSlots($this->uri->segment(3)));
        // exit;

        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome'); 
        $this->data['planos'] = $this->planos_model->listar();
		$this->data['result'] = $this->franquias_model->getById($this->uri->segment(3));

	    $this->data['view'] = 'franquias/editarFranquia';
        $this->load->view('tema/topo',$this->data);

    }

    public function areaSlots(){

        $this->load->library('form_validation');    
        $this->data['custom_error'] = '';

        $this->form_validation->set_rules('slotsp', 'Slot Pequeno', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slotsm', 'Slot Médio', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slotsg', 'Slot Grande', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('fileiras_slots', 'Qtd. por fileira', 'trim|required|xss_clean');
        $this->form_validation->set_rules('qtd_fileiras_slots_p', 'Qtd. de Fileiras para os Slots Pequenos', 'trim|required|xss_clean');
        $this->form_validation->set_rules('qtd_fileiras_slots_m', 'Qtd. de Fileiras para os Slots Médios', 'trim|required|xss_clean');
        $this->form_validation->set_rules('qtd_fileiras_slots_g', 'Qtd. de Fileiras para os Slots Grandes', 'trim|required|xss_clean');
        
        if ($this->form_validation->run('franquias') == false) :

             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        else:

            $data = array(
                'slotsp'               => $this->input->post('slotsp'),
                'slotsm'               => $this->input->post('slotsm'),
                'slotsg'               => $this->input->post('slotsg'),
                //'fileiras_slots'       => $this->input->post('fileiras_slots'),
                'qtd_fileiras_slots_p' => $this->input->post('qtd_fileiras_slots_p'),
                'qtd_fileiras_slots_m' => $this->input->post('qtd_fileiras_slots_m'),
                'qtd_fileiras_slots_g' => $this->input->post('qtd_fileiras_slots_g')
            );

        if($this->franquias_model->editarSlots('franquias', $data, $this->input->post('idFranquias')) == TRUE){
            $this->session->set_flashdata('success','Sltos Franquia editada com sucesso!');
            redirect(base_url() . 'index.php/franquias/areaSlots/' . $this->input->post('idFranquias'));
        }else{
            $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
        }

        endif;
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome'); 
        //$this->data['planos'] = $this->planos_model->listar();
        $this->data['result'] = $this->franquias_model->getById($this->uri->segment(3));
        
        $this->data['view'] = 'franquias/slotsFranq';
        $this->load->view('tema/topo',$this->data);
    }
	
    public function excluir()
    {
        $ID = $this->input->post('id');
        $this->franquias_model->delete('franquias', 'idFranquias', $ID);
        redirect(base_url() . 'index.php/franquias/gerenciar/');
    }
}

