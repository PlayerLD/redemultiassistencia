var _quality = 75;
var _inview = "";
var _enabled = true;

function base_url(){
	var url = window.location.href.split('/');
	var result = url[0];
	for (var i = 1; i < url.length; i++){
		if (url[i] != "index.php"){
			result += '/' + url[i];
		}else{
			break;
		}
	}
	
	result += '/application/controllers/reisdev-api/cam-api/';
	
	return result;
}
function changedQuality(combo)
{
	var _index = combo.selectedIndex;
	_quality = combo.options[_index].value;
};
function saveImage()
{
	var temp;	
	temp = window.location.href.split('/');

	if(typeof document.idEquipamento != "undefined"){

		var idEquipamento = document.idEquipamento;
		console.log(idEquipamento);
	}else
		var idEquipamento = temp[temp.length - 1];

	if(temp.indexOf('produtos') != -1)
		var table = 'produtos';
	else if(temp.indexOf('equipamentos') != -1)
		var table = 'equipamentos';
	else
		var table = false;

	
	temp = _inview.split('/');
	var name = temp[temp.length - 1];
	
	if(table){
		var parametros =
		{
			"base_url" : base_url(),
			"type" : "save",
			"table" : table,
			"name" : name,
			"id"   : idEquipamento,
			"tipo_foto" : $("#tipoEntrega").val() 
		};
		$.ajax(
		{
			url: base_url() + "io.php",
			type: "POST",
			data: parametros,
			success: function(result)
			{
				
				updatePreview();
			}
		});
	}else{

		var dir = base_url() + "saved_img/";

		
		if($('#divImg01').html() == ""){
			$('#divImg01').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img01" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}

		else if($('#divImg02').html() == ""){
			$('#divImg02').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img02" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}

		else if($('#divImg03').html() == ""){
			$('#divImg03').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img03" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}

		else if($('#divImg04').html() == ""){
			$('#divImg04').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img04" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}

		else if($('#divImg05').html() == ""){
			$('#divImg05').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img05" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}

		else if($('#divImg06').html() == ""){
			$('#divImg06').html('<a href="argin:0px;" src="' + dir + name + '" data-fancybox="galeria"><img id="Img06" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + name + '"></img></a>'); 
			
			if(typeof localStorage.redemult_OI_Fotos == "string")
				localStorage.redemult_OI_Fotos += (localStorage.redemult_OI_Fotos == "" ? '' : ',') + name ;
		}
	}
}
function discardImage()
{
	var parametros =
	{
		"type" : "delete",
		"file" : _inview
	};
	$.ajax(
	{
		url: base_url() + "io.php",
		type: "POST",
		data: parametros,
		success: function(result)
		{ }
	});
};
function excludeImage(id)
{
	var file = "";
	switch (id)
	{
		case 1: file = $('#Img01').attr('src'); break;
		case 2: file = $('#Img02').attr('src'); break;
		case 3: file = $('#Img03').attr('src'); break;
		case 4: file = $('#Img04').attr('src'); break;
		case 5: file = $('#Img05').attr('src'); break;
		case 6: file = $('#Img06').attr('src'); break;
		case 7: file = $('#Img07').attr('src'); break;
		case 8: file = $('#Img08').attr('src'); break;
		case 9: file = $('#Img09').attr('src'); break;
		case 10: file = $('#Img10').attr('src'); break;
		case 11: file = $('#Img11').attr('src'); break;
		case 12: file = $('#Img12').attr('src'); break;
	}	
	
	if (file != "")
	{
		var temp;
		temp = window.location.href.split('/');

		if(typeof document.idEquipamento != "undefined"){

			var idEquipamento = document.idEquipamento;
			console.log(idEquipamento);
		}else
			var idEquipamento = temp[temp.length - 1];

		file = file.substring(file.lastIndexOf("/") + 1);
		if(temp.indexOf('produtos') != -1)
		var table = 'produtos';
	else if(temp.indexOf('equipamentos') != -1)
		var table = 'equipamentos';
	else
		var table = false;

		if(table){
			var parametros =
			{
				"base_url" : base_url(),
				"type" : "exclude",
				"table" : table,
				"id"   : idEquipamento,
				"imgid": id,			
				"file" : file
			};
			$.ajax(
			{
				url: base_url() + "io.php",
				type: "POST",
				data: parametros,
				success: function(result)
				{
					updatePreview();
				}
			});
		}else{

			var dir = base_url() + "saved_img/";

			
			$('#divImg0'+id).html('')
		}
	}
	else
		updatePreview();
};
function updatePreview()
{
	var temp;	
	temp = window.location.href.split('/');

	if(typeof document.idEquipamento != "undefined"){

		var idEquipamento = document.idEquipamento;
		console.log(idEquipamento);
	}else
		var idEquipamento = temp[temp.length - 1];
	if(temp.indexOf('produtos') != -1)
		var table = 'produtos';
	else if(temp.indexOf('equipamentos') != -1)
		var table = 'equipamentos';
	else
		var table = false;

	var parametros =
	{
		"base_url" : base_url(),
		"type" : "get",
		"table" : table,
		"id"   : idEquipamento 
	};
	$.ajax(
	{
		url: base_url() + "io.php",
		type: "POST",
		data: parametros,
		success: function(result)
		{
			var dir = base_url() + "saved_img/";
			
			var Array = JSON.parse(result);
			var count = 0;
			
			if (Array['img01'] != "") { 
				$('#delImg01').removeClass("elemHidden"); 
				$('#divImg01').html('<a href="' + dir + Array['img01'] + '" data-fancybox="galeria"><img id="Img01" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img01'] + '"></img></a>'); 
				count = count + 1;
			}else{ 
				$('#delImg01').addClass("elemHidden");
				$('#divImg01').html(''); 
			}
			
			if (Array['img02'] != "") { 
				$('#delImg02').removeClass("elemHidden"); 
				$('#divImg02').html('<a href="' + dir + Array['img02'] + '" data-fancybox="galeria"><img id="Img02" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img02'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg02').addClass("elemHidden");    
				$('#divImg02').html(''); 
			}
			
			if (Array['img03'] != "") { 
				$('#delImg03').removeClass("elemHidden"); 
				$('#divImg03').html('<a href="' + dir + Array['img03'] + '" data-fancybox="galeria"><img id="Img03" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img03'] + '"></img></a>'); 
				count = count + 1;
			}else { 
				$('#delImg03').addClass("elemHidden"); 
				$('#divImg03').html('');
			}
			
			if (Array['img04'] != "") { 
				$('#delImg04').removeClass("elemHidden"); 
				$('#divImg04').html('<a href="' + dir + Array['img04'] + '" data-fancybox="galeria"><img id="Img04" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img04'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg04').addClass("elemHidden");
				$('#divImg04').html(''); 
			}
			
			if (Array['img05'] != "") { 
				$('#delImg05').removeClass("elemHidden"); 
				$('#divImg05').html('<a href="' + dir + Array['img05'] + '" data-fancybox="galeria"><img id="Img05" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img05'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg05').addClass("elemHidden"); 
				$('#divImg05').html(''); 
			}
			
			if (Array['img06'] != "") { 
				$('#delImg06').removeClass("elemHidden"); 
				$('#divImg06').html('<a href="' + dir + Array['img06'] + '" data-fancybox="galeria"><img id="Img06" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img06'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg06').addClass("elemHidden");
				$('#divImg06').html(''); 
			}
			
			if (Array['img07'] != "") { 
				$('#delImg07').removeClass("elemHidden"); 
				$('#divImg07').html('<a href="' + dir + Array['img07'] + '" data-fancybox="galeria"><img id="Img07" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img07'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg07').addClass("elemHidden");
				$('#divImg07').html(''); 
			}
			
			if (Array['img08'] != "") { 
				$('#delImg08').removeClass("elemHidden"); 
				$('#divImg08').html('<a href="' + dir + Array['img08'] + '" data-fancybox="galeria"><img id="Img08" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img08'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg08').addClass("elemHidden");
				$('#divImg08').html(''); 
			}
			
			if (Array['img09'] != "") { 
				$('#delImg09').removeClass("elemHidden"); 
				$('#divImg09').html('<a href="' + dir + Array['img09'] + '" data-fancybox="galeria"><img id="Img09" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img09'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg09').addClass("elemHidden");
				$('#divImg09').html(''); 
			}
			
			if (Array['img10'] != "") { 
				$('#delImg10').removeClass("elemHidden"); 
				$('#divImg10').html('<a href="' + dir + Array['img10'] + '" data-fancybox="galeria"><img id="Img10" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img10'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg10').addClass("elemHidden"); 
				$('#divImg10').html(''); 
			}
			
			if (Array['img11'] != "") { 
				$('#delImg11').removeClass("elemHidden"); 
				$('#divImg11').html('<a href="' + dir + Array['img11'] + '" data-fancybox="galeria"><img id="Img11" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img11'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg11').addClass("elemHidden");
				$('#divImg11').html(''); 
			}
			
			if (Array['img12'] != "") { 
				$('#delImg12').removeClass("elemHidden"); 
				$('#divImg12').html('<a href="' + dir + Array['img12'] + '" data-fancybox="galeria"><img id="Img12" class="imgView" style="widht:100%; height:100%; margin:0px;" src="' + dir + Array['img12'] + '"></img></a>'); 
				count = count + 1; 
			}else{ 
				$('#delImg12').addClass("elemHidden"); 
				$('#divImg12').html(''); 
			}
						
			var disabled = false;
			if (!_enabled)
			{
				$('#pnPreview').html('<div style="display:table-cell;font-weight:bold;color:rgba(66,68,78, .5);width:440px;height:240px;background:white;border:1px solid #C6C9CC;text-align:center;vertical-align:middle;">Não foi possível carregar a câmera</div>');
				disabled = true;
			}
			
			$("#cbQuality").attr("disabled", disabled);
			$("#btCapture").attr("disabled", disabled);
			$("#btSave").attr("disabled", disabled);
			$("#btDiscard").attr("disabled", disabled);
		}
	});
};
function updatePreview2(prefix, idEquipamento)
{
	
	var parametros =
	{
		"base_url" : base_url(),
		"type" : "get",
		"table" : "equipamentos",
		"id"   : idEquipamento 
	};
	$.ajax(
	{
		url: base_url() + "io.php",
		type: "POST",
		data: parametros,
		success: function(result)
		{
			var dir = base_url() + "saved_img/";
			
			var Array = JSON.parse(result);
			var count = 0;
						
			if (Array['img01'] != "") {
							$('#'+prefix+'Img01T1').html('<img id="'+prefix+'Img01T1" onclick="imgView2(\'' + dir + Array['img01'] + '\', 1)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img01'] + '"></img>');
							$('#'+prefix+'Img01T2').html('<img id="'+prefix+'Img01T2" onclick="imgView2(\'' + dir + Array['img01'] + '\', 1)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img01'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img01T1').html(''); $('#'+prefix+'Img01T2').html(''); }
			
			if (Array['img02'] != "") {
							$('#'+prefix+'Img02T1').html('<img id="'+prefix+'Img02T1" onclick="imgView2(\'' + dir + Array['img02'] + '\', 2)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img02'] + '"></img>');
							$('#'+prefix+'Img02T2').html('<img id="'+prefix+'Img02T2" onclick="imgView2(\'' + dir + Array['img02'] + '\', 2)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img02'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img02T1').html(''); $('#'+prefix+'Img02T2').html(''); }
			
			if (Array['img03'] != "") {
							$('#'+prefix+'Img03T1').html('<img id="'+prefix+'Img03T1" onclick="imgView2(\'' + dir + Array['img03'] + '\', 3)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img03'] + '"></img>');
							$('#'+prefix+'Img03T2').html('<img id="'+prefix+'Img03T2" onclick="imgView2(\'' + dir + Array['img03'] + '\', 3)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img03'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img03T1').html(''); $('#'+prefix+'Img03T2').html(''); }
			
			if (Array['img04'] != "") {
							$('#'+prefix+'Img04T1').html('<img id="'+prefix+'Img04T1" onclick="imgView2(\'' + dir + Array['img04'] + '\', 4)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img04'] + '"></img>');
							$('#'+prefix+'Img04T2').html('<img id="'+prefix+'Img04T2" onclick="imgView2(\'' + dir + Array['img04'] + '\', 4)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img04'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img04T1').html(''); $('#'+prefix+'Img04T2').html(''); }
			
			if (Array['img05'] != "") {
							$('#'+prefix+'Img05T1').html('<img id="'+prefix+'Img05T1" onclick="imgView2(\'' + dir + Array['img05'] + '\', 5)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img05'] + '"></img>');
							$('#'+prefix+'Img05T2').html('<img id="'+prefix+'Img05T2" onclick="imgView2(\'' + dir + Array['img05'] + '\', 5)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img05'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img05T1').html(''); $('#'+prefix+'Img05T2').html(''); }
			
			if (Array['img06'] != "") {
							$('#'+prefix+'Img06T1').html('<img id="'+prefix+'Img06T1" onclick="imgView2(\'' + dir + Array['img06'] + '\', 6)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img06'] + '"></img>');
							$('#'+prefix+'Img06T2').html('<img id="'+prefix+'Img06T2" onclick="imgView2(\'' + dir + Array['img06'] + '\', 6)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img06'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img06T1').html(''); $('#'+prefix+'Img06T2').html(''); }
			
			
			if (Array['img07'] != "") {
							$('#'+prefix+'Img07T1').html('<img id="'+prefix+'Img07T1" onclick="imgView2(\'' + dir + Array['img07'] + '\', 7)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img07'] + '"></img>');
							$('#'+prefix+'Img07T2').html('<img id="'+prefix+'Img07T2" onclick="imgView2(\'' + dir + Array['img07'] + '\', 7)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img07'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img07T1').html(''); $('#'+prefix+'Img07T2').html(''); }
			
			
			if (Array['img08'] != "") {
							$('#'+prefix+'Img08T1').html('<img id="'+prefix+'Img08T1" onclick="imgView2(\'' + dir + Array['img08'] + '\', 8)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img08'] + '"></img>');
							$('#'+prefix+'Img08T2').html('<img id="'+prefix+'Img08T2" onclick="imgView2(\'' + dir + Array['img08'] + '\', 8)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img08'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img08T1').html(''); $('#'+prefix+'Img08T2').html(''); }
			
			
			if (Array['img09'] != "") {
							$('#'+prefix+'Img09T1').html('<img id="'+prefix+'Img09T1" onclick="imgView2(\'' + dir + Array['img09'] + '\', 9)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img09'] + '"></img>');
							$('#'+prefix+'Img09T2').html('<img id="'+prefix+'Img09T2" onclick="imgView2(\'' + dir + Array['img09'] + '\', 9)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img09'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img09T1').html(''); $('#'+prefix+'Img09T2').html(''); }
			
			
			if (Array['img10'] != "") {
							$('#'+prefix+'Img10T1').html('<img id="'+prefix+'Img10T1" onclick="imgView2(\'' + dir + Array['img10'] + '\', 10)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img10'] + '"></img>');
							$('#'+prefix+'Img10T2').html('<img id="'+prefix+'Img10T2" onclick="imgView2(\'' + dir + Array['img10'] + '\', 10)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img10'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img10T1').html(''); $('#'+prefix+'Img10T2').html(''); }
			
			
			if (Array['img11'] != "") {
							$('#'+prefix+'Img11T1').html('<img id="'+prefix+'Img11T1" onclick="imgView2(\'' + dir + Array['img11'] + '\', 11)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img11'] + '"></img>');
							$('#'+prefix+'Img11T2').html('<img id="'+prefix+'Img11T2" onclick="imgView2(\'' + dir + Array['img11'] + '\', 11)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img11'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img11T1').html(''); $('#'+prefix+'Img11T2').html(''); }
			
			
			if (Array['img12'] != "") {
							$('#'+prefix+'Img12T1').html('<img id="'+prefix+'Img12T1" onclick="imgView2(\'' + dir + Array['img12'] + '\', 12)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img12'] + '"></img>');
							$('#'+prefix+'Img12T2').html('<img id="'+prefix+'Img12T2" onclick="imgView2(\'' + dir + Array['img12'] + '\', 12)" class="imgView" style="width:100%; margin:0px;" src="' + dir + Array['img12'] + '"></img>');
							count = count + 1;
						  }
			else 			  { $('#'+prefix+'Img12T1').html(''); $('#'+prefix+'Img12T2').html(''); }
		}
	});
};
function imgView2(file, id)
{
	if (file != "")
	{
		var back = document.getElementById('imgPopupBack');
		var view = document.getElementById("imgPopupView");
		var text = document.getElementById("imgPopupText");
		var close = document.getElementsByClassName("imgPopupClose")[0];
		
	    	back.style.display = "block";
	    	view.src = file;
	    	
		text.innerHTML = "Image: 0" + id;
		close.onclick = back.onclick = function() { back.style.display = "none"; }
	}
};
function resize()
{
};
function imgView(id)
{
	var file = "";
	switch (id)
	{
		case 1: file = $('#Img01').attr('src'); break;
		case 2: file = $('#Img02').attr('src'); break;
		case 3: file = $('#Img03').attr('src'); break;
		case 4: file = $('#Img04').attr('src'); break;
		case 5: file = $('#Img05').attr('src'); break;
		case 6: file = $('#Img06').attr('src'); break;
		case 7: file = $('#Img07').attr('src'); break;
		case 8: file = $('#Img08').attr('src'); break;
		case 9: file = $('#Img09').attr('src'); break;
		case 10: file = $('#Img10').attr('src'); break;
		case 11: file = $('#Img11').attr('src'); break;
		case 12: file = $('#Img12').attr('src'); break;
	}	
	
	if (file != "")
	{
		var back = document.getElementById('imgPopupBack');
		var view = document.getElementById("imgPopupView");
		var text = document.getElementById("imgPopupText");
		var close = document.getElementsByClassName("imgPopupClose")[0];
		
	    	back.style.display = "block";
	    	view.src = file;
	    	
		text.innerHTML = "Image: 0" + id;
		close.onclick = back.onclick = function() { back.style.display = "none"; }
	}
	else
		updatePreview();
};
$(function()
{
	webcam.set_api_url(base_url() + "io.php" );	//give the php file path
	webcam.set_swf_url(base_url() + "player.swf" );	//flash file (SWF) file path
	webcam.set_quality( _quality ); 					// Image quality
	webcam.set_shutter_sound( true ); 			// play shutter click sound
	
	var cam = $('#pnCam');

	$('#cbQuality').val('75').trigger('change');
	cam.html(webcam.get_html(560, 420));
	//generate and put the flash embed code on page
	
	$('#cbQuality').on('change', function()
	{
		if($('#cbQuality').val() == 25)
		cam.html(webcam.get_html(320, 240)); 		
		else if($('#cbQuality').val() == 50)
		cam.html(webcam.get_html(440, 330)); 		
		else if($('#cbQuality').val() == 75)
		cam.html(webcam.get_html(560, 420)); 		
		else if($('#cbQuality').val() == 90)
		cam.html(webcam.get_html(680, 510)); 		
		else if($('#cbQuality').val() == 100)
		cam.html(webcam.get_html(800, 600)); 		
	});
		
	
	$('#btCapture').click(function()
	{
		$('#pnPreview').html('Processando...');
		webcam.set_quality( _quality ); 			// Image quality
		webcam.reset();
		webcam.snap();
	});
	
	$('#btSave').click(function()
	{
		if (_inview != "")
		{
			saveImage();
			_inview = "";
		}
		$('#btCapture').show();
		$('#cbQuality').show();
		$('#pnPreview').hide();
		$('#btSave').hide();
		$('#btDiscard').hide();
	});
	
	$('#btDiscard').click(function()
	{
		if (_inview != "")
		{
			discardImage();
			_inview = "";
		}
		$('#btCapture').show();
		$('#cbQuality').show();
		$('#pnPreview').hide();
		$('#btSave').hide();
		$('#btDiscard').hide();
	});
	
	//after taking snap call show image
	webcam.set_hook( 'onComplete', function(img)
	{
		_inview = img;
		$('#pnPreview').html('<img src="' + base_url() + '' + _inview + '">');
		
		//reset camera for the next shot
		webcam.reset();
		$('#btCapture').hide();
		$('#cbQuality').hide();
		$('#pnPreview').show();
		$('#btSave').show();
		$('#btDiscard').show();
	});				
});
	
//
// WEBCAM SCRIPT -----------------------------------------------------------------------------------------------------------------------------------------------------
//
window.webcam = 
{
	version: '1.0.9',
	
	// globals
	ie: !!navigator.userAgent.match(/MSIE/),
	protocol: location.protocol.match(/https/i) ? 'https' : 'http',
	callback: null, 			// user callback for completed uploads
	swf_url: base_url() + 'player.swf', 		// URI to webcam.swf movie (defaults to cwd)
	shutter_url: base_url() + 'sound.mp3', // URI to shutter.mp3 sound
	api_url: '',				// URL to upload script
	loaded: false, 				// true when webcam movie finishes loading
	quality: 100, 				// JPEG quality (1 - 100)
	shutter_sound: true, 		// shutter sound effect on/off
	stealth: false, 			// stealth mode (do not freeze image upon capture)
	hooks: 
	{
		onLoad: null,
		onComplete: null,
		onError: null
	}, // callback hook functions
	
	set_hook: function(name, callback)
	{
		// set callback hook
		// supported hooks: onLoad, onComplete, onError
		if (typeof(this.hooks[name]) == 'undefined')
			return alert("Hook type not supported: " + name);
		
		this.hooks[name] = callback;
	},
	
	fire_hook: function(name, value)
	{
		// fire hook callback, passing optional value to it
		if (this.hooks[name])
			{
			if (typeof(this.hooks[name]) == 'function') 
			{
				// callback is function reference, call directly
				this.hooks[name](value);
			}
			else if (typeof(this.hooks[name]) == 'array')
			{
				// callback is PHP-style object instance method
				this.hooks[name][0][this.hooks[name][1]](value);
			}
			else if (window[this.hooks[name]])
			{
				// callback is global function name
				window[ this.hooks[name] ](value);
			}
			return true;
		}
		return false; // no hook defined
	},
	
	set_api_url: function(url) 
	{
		// set location of upload API script
		this.api_url = url;
	},
	
	set_swf_url: function(url) 
	{
		// set location of SWF movie (defaults to webcam.swf in cwd)
		this.swf_url = url;
	},
	
	get_html: function(width, height, server_width, server_height) 
	{
		// Return HTML for embedding webcam capture movie
		// Specify pixel width and height (640x480, 320x240, etc.)
		// Server width and height are optional, and default to movie width/height
		if (!server_width) 
			server_width = width;
		
		if (!server_height) 
			server_height = height;
		
		var html = '';
		var flashvars = 'shutter_enabled=' + (this.shutter_sound ? 1 : 0) + 
			'&shutter_url=' + escape(this.shutter_url) + 
			'&width=' + width + 
			'&height=' + height + 
			'&server_width=' + server_width + 
			'&server_height=' + server_height;
		
		if (this.ie) 
		{
			html += '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="'+this.protocol+'://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'+width+'" height="'+height+'" id="webcam_movie" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="'+this.swf_url+'" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="'+flashvars+'"/></object>';
		}
		else 
		{
			html += '<embed id="webcam_movie" src="'+this.swf_url+'" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="'+width+'" height="'+height+'" name="webcam_movie" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="'+flashvars+'" />';
		}
		
		this.loaded = false;
		return html;
	},
	
	get_movie: function() 
	{
		// get reference to movie object/embed in DOM
		if (!this.loaded) return alert("ERROR: Movie is not loaded yet");
		var movie = document.getElementById('webcam_movie');
		if (!movie) alert("ERROR: Cannot locate movie 'webcam_movie' in DOM");
		return movie;
	},
	
	set_stealth: function(stealth) 
	{
		// set or disable stealth mode
		this.stealth = stealth;
	},
	
	snap: function(url, callback, stealth) 
	{
		// take snapshot and send to server
		// specify fully-qualified URL to server API script
		// and callback function (string or function object)
		if (callback) 
			this.set_hook('onComplete', callback);
		
		if (url) 
			this.set_api_url(url);
		
		if (typeof(stealth) != 'undefined') 
			this.set_stealth( stealth );
		
		this.get_movie()._snap( this.api_url, this.quality, this.shutter_sound ? 1 : 0, this.stealth ? 1 : 0 );
	},
	
	freeze: function() 
	{
		// freeze webcam image (capture but do not upload)
		this.get_movie()._snap('', this.quality, this.shutter_sound ? 1 : 0, 0 );
	},
	
	upload: function(url, callback) 
	{
		// upload image to server after taking snapshot
		// specify fully-qualified URL to server API script
		// and callback function (string or function object)
		if (callback) 
			this.set_hook('onComplete', callback);
		if (url) 
			this.set_api_url(url);
		
		this.get_movie()._upload( this.api_url );
	},
	
	reset: function() 
	{
		// reset movie after taking snapshot
		this.get_movie()._reset();
	},
	
	configure: function(panel) 
	{
		// open flash configuration panel -- specify tab name:
		// "camera", "privacy", "default", "localStorage", "microphone", "settingsManager"
		if (!panel) 
			panel = "camera";
		
		this.get_movie()._configure(panel);
	},
	
	set_quality: function(new_quality) 
	{
		// set the JPEG quality (1 - 100)
		// default is 90
		this.quality = new_quality;
	},
	
	set_shutter_sound: function(enabled, url) 
	{
		// enable or disable the shutter sound effect
		// defaults to enabled
		this.shutter_sound = enabled;
		this.shutter_url = url ? url : base_url() + 'sound.mp3';
	},
	
	flash_notify: function(type, msg) 
	{
		// receive notification from flash about event
		switch (type) 
		{
			case 'flashLoadComplete':
				// movie loaded successfully
				this.loaded = true;
				this.fire_hook('onLoad');
				$('#img-flash').hide();
				break;
			case 'error':
				// HTTP POST error most likely
				if (!this.fire_hook('onError', msg)) 
				{
					_enabled = false;
					updatePreview();
				}
				break;
			case 'success':
				// upload complete, execute user callback function
				// and pass raw API script results to function
				this.fire_hook('onComplete', msg.toString());
				break;
			default:
				// catch-all, just in case
				alert("jpegcam flash_notify: " + type + ": " + msg);
				break;
		}
	}
};
