<?php

class DataTable

{

	public $inTable = false;

	public $inBody = false;

    private $list = array();

   	function __construct()

	{

		echo (' <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

			<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-1.12.4.js"></script>

			<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

			<style> 

				select[name=example_length]

				{

					width: 60px !important;

				}

				.dataTables_filter

				{

					position: absolute !important;

					margin: 0 !important;

					margin-right: 4px !important;

					padding: 0 !important;

					top: -50px !important;

					right: 63px !important;

					left: auto !important;

					width: auto !important;

					float: right !important;

				}

				.dataTables_length

				{

					position: absolute !important;

					margin: 0 !important;

					padding: 0 !important;

					top: -50px !important;

					right: 4px !important;

					left: auto !important;

					width: auto !important;

					float: right !important;

				}

				.dataTables_table

				{

					top: -15px !important;

					position: relative !important;

				}

			</style>

			<script>

				 $(function()

				{

					  $("#example").DataTable

					  ({

        						"order": [[ 0, "desc" ]],

						        "paging":   true,

						        "ordering": true,

						        "info":     true,

							"oLanguage":

							{

								 "sEmptyTable": "Nenhum registro encontrado",

								 "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",

								 "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",

								 "sInfoFiltered": "(Filtrados de _MAX_ registros)",

								 "sInfoPostFix": "",

								 "sInfoThousands": ".",

								 "sLengthMenu": "_MENU_",

								 "sLoadingRecords": "Carregando...",

								 "sProcessing": "Processando...",

								 "sZeroRecords": "Nenhum registro encontrado",

								 "sSearch": "Pesquisar",

								 "oPaginate":

								 {

									  "sNext": "Próximo",

									  "sPrevious": "Anterior",

									  "sFirst": "Primeiro",

									  "sLast": "Último"

								 },

								 "oAria":

								 {

									  "sSortAscending": ": Ordenar colunas de forma ascendente",

									  "sSortDescending": ": Ordenar colunas de forma descendente"

								 }

							}

					  });

					  $(document).on("click", "a", function(event)

					  {

					  

							var os = $(this).attr("os");

							$("#idOs").val(os);

					  });

					  

					  $("#example tfoot th").each( function () {

                            var title = $(this).text();

                            $(this).html( \'<input type="text" style="width: 100%;" placeholder="\' + title + \'" />\' );

                        } );

                    

                        // DataTable

                        var table = $("#example").DataTable();

                    

                        // Apply the search

                        table.columns().every( function () {

                            var that = this;

                    

                            $( \'input\', this.footer() ).on( \'keyup change\', function () {

                                if ( that.search() !== this.value ) {

                                    that

                                        .search( this.value )

                                        .draw();

                                }

                            } );

                        } );

				 })

			</script>'

		);

	}

	public function Columns($list)

	{

	    $this->list = $list;

		if (!$this->inTable)

			$this->Start();

			

		if ($this->inBody)

			$this->end_Items();

		

		echo ('<thead>');

		echo ('	<tr style="backgroud-color: #2D335B" role="row">');

		foreach ($list as $c)

			echo ('<th>'.$c.'</th>');

			

		echo ('	</tr>');

		echo ('</thead>');

	}

	public function item($list)

	{

// dd($list); exit;
		if (!$this->inTable)

			$this->Start();

			

		if (!$this->inBody)

			$this->start_Items();

		
		echo ('<tr>');

		foreach ($list as $i)

			echo ('<td>'.$i.'</td>');

			

		echo ('</tr>');

	}

	public function add_Item($list){

		if (!$this->inTable)

			$this->Start();

			

		if (!$this->inBody)

			$this->start_Items();

		

		echo ('<tr>');

		foreach ($list as $i)

			echo ('<td>'.$i.'</td>');

			

		echo ('</tr>');

	}

	public function start_Items()

	{

		if (!$this->inBody)

		{

			echo ('<tbody>');

			$this->inBody = true;

		}

	}

	public function end_Items()

	{

		if ($this->inBody)

		{

			echo ('</tbody>');

			$this->inBody = false;

		}

	}

	public function Start()

	{

		if (!$this->inTable)

		{

			echo ('<table id="example" class="table table-bordered no-footer dataTables_table" role="grid">');

			$this->inTable = true;

			$this->inBody = false;

		}

	}

	public function End()

	{

		if ($this->inBody)

			$this->end_Items();

			

		if ($this->inTable)

		{

            echo ('<tfoot>');

            echo ('	<tr>');

            foreach ($this->list as $c)

                echo ('<th>'.$c.'</th>');



            echo ('	</tr>');

            echo ('</tfoot>');

			echo ('</table>');

			$this->inTable = false;

		}

	}

}

?>