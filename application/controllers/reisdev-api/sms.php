<?php
include_once('dao.php');

	class UpSMS
	{
		public static function Credito($idFranquia) 
		{
			try
			{
				$r = (array)json_decode(DAO::Get("mensagens", "`idFranquia`='".$idFranquia."'")['sms']);
				$url = 'http://painel.hotmobile.com.br/SendAPI/getSaldo.aspx?usr='.$r['user']
												  .'&pwd='.$r['psw'];
				
				$result = file_get_contents($url);
				if (!isset($result) || $result < 0)
					return '-1';
				
				return $result;
            		}
            		catch (Exception $e) { return '-1'; }
        	}
		
		public static function Enviar($idFranquia, $sender, $number, $msg) 
		{
			try
			{
				$r = (array)json_decode(DAO::Get("mensagens", "`idFranquia`='".$idFranquia."'")['sms']);
				$url = 'http://painel.hotmobile.com.br/SendAPI/Send.aspx?usr='.$r['user']
				                                                      .'&pwd='.$r['psw']
				                                                      .'&number='.urlencode($number)
				                                                      .'&sender='.urlencode($sender)
				                                                      .'&msg='.urlencode(' ' . $msg);
				
				$result = file_get_contents($url);
				if (!isset($result) || $result < 1)
					return '-1';
				
				return $result;
            		}
            		catch (Exception $e) { return '-1'; }
		}
		
		public static function Consultar($idFranquia, $msgid) 
		{
			try
			{
				$r = (array)json_decode(DAO::Get("mensagens", "`idFranquia`='".$idFranquia."'")['sms']);
				$url = 'http://painel.hotmobile.com.br/SendAPI/ReportMessage.aspx?usr='.$r['user']
												       .'&pwd='.$r['psw']
												       .'&msgID='.$msgid;

				$result = file_get_contents($url);
				if (!isset($result) || $result < 1)
					return '-1';
				
				return $result;
            		}
            		catch (Exception $e) { return '-1'; }
		}
	}
?>