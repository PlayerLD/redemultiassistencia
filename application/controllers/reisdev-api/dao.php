<?php
	class DAO
	{
		public static function Connect()
		{
			// OBTÉM DADOS DO SERVIDOR
			//include($_SERVER['DOCUMENT_ROOT'].'/sistema/application/config/database.php');
			include($_SERVER['DOCUMENT_ROOT'].'/sistema/application/config/database.php');
			
			$activedb  = $db[$active_group];
			{
				$host      = $activedb['hostname']; // IP DO SERVIDOR MYSQL
				$dbstr     = $activedb['database']; // NOME DO BANCO DE DADOS
				$uid       = $activedb['username']; // USUÁRIO
				$psw       = $activedb['password']; // SENHA
			}
					
			$connected = true;
			$conn = mysqli_connect($host, $uid, $psw, $dbstr) or $connected = false;
			
			if (!$connected)
				return null;
			return $conn;
		}
	
		// ACESSA BANCO DE DADOS VIA MYSQLI E RETORNA RESPOSTA QUERY
		public static function Select($tablestr, $filter)
		{
			$conn = DAO::Connect();
			if ($conn == null) return null;
				
			$cmd = "SELECT * FROM `".$tablestr."` WHERE " . $filter;
			$query = mysqli_query($conn, $cmd);
			mysqli_close($conn);
			return $query;
		}
	
		// ESTRUTURA RESPOSTA QUERY PARA ARRAY VIA NOME/NÚMERO
		public static function Get($tablestr, $filter)
		{
			$query = DAO::Select($tablestr, $filter);
			if ($query == null || mysqli_num_rows($query) < 1)
				return null;		
				
	  		return mysqli_fetch_array($query, MYSQLI_BOTH);
		}
		
		// ESTRUTURA RESPOSTA QUERY PARA ARRAY VIA NOME
		public static function GetASSOC($tablestr, $filter)
		{
			$query = DAO::Select($tablestr, $filter);
			if ($query == null || mysqli_num_rows($query) < 1)
				return null;
				
	  		return mysqli_fetch_array($query, MYSQLI_ASSOC);
		}
		
		// ESTRUTURA RESPOSTA QUERY PARA ARRAY VIA NÚMERO
		public static function GetNUM($tablestr, $filter)
		{
			$query = DAO::Select($tablestr, $filter);
			if ($query == null || mysqli_num_rows($query) < 1)
				return null;
						
	  		return mysqli_fetch_array($query, MYSQLI_NUM);
		}
		
		// ADICIONA LINHA A TABELA
		public static function Add($tablestr, $columns, $values)
		{
			$conn = DAO::Connect();
			if ($conn == null) return null;
				
			$cmd = "INSERT INTO `".$tablestr."` (".$columns.") VALUES (".$values.")";
	                mysqli_query($conn, $cmd);
			mysqli_close($conn);
	                return 1;
		}
		
		// REMOVE LINHA DA TABELA
		public static function Del($tablestr, $filter)
		{
			$conn = DAO::Connect();
			if ($conn == null) return null;
				
			$cmd = "DELETE FROM `".$tablestr."` WHERE ".$filter;
	                mysqli_query($conn, $cmd);
			mysqli_close($conn);
	                return 1;
		}
		
		// ATUALIZA DADOS DA LINHA
		public static function Upd($tablestr, $set, $filter)
		{
			$conn = DAO::Connect();
			if ($conn == null) return null;
				
			$cmd = "UPDATE `".$tablestr."` SET ".$set." WHERE ".$filter;
	                mysqli_query($conn, $cmd);
			mysqli_close($conn);
	                return $cmd;
		}
	}
?>
