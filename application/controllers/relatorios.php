<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Relatorios extends CI_Controller{





    /**

     * author: Ramon Silva 

     * email: silva018-mg@yahoo.com.br

     * 

     */

    

    public function __construct() {

        parent::__construct();

        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){

            redirect('sistemaos/login');

        }

        

        $this->load->model('Relatorios_model','',TRUE);

		$this->load->model('Franquias_model','',TRUE);

		

        $this->data['menuRelatorios'] = 'Relatórios';



    }



    public function clientes(){



        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rCliente')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de clientes.');

           redirect(base_url());

        }

        $this->data['menuRelatorios'] = 'clientes';

        $this->data['view'] = 'relatorios/rel_clientes';

       	$this->load->view('tema/topo',$this->data);

    }

	

	public function fornecedores(){



        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de fornecedores.');

           redirect(base_url());

        }

        $this->data['menuRelatorios'] = 'fornecedores';

        $this->data['view'] = 'relatorios/rel_fornecedores';

       	$this->load->view('tema/topo',$this->data);

    }

	

	public function marcas(){



        $this->data['menuRelatorios'] = 'marcas';

        $this->data['view'] = 'relatorios/rel_marcas';

		$this->data['franquias'] = $this->Franquias_model->get(100,0,false);

       	$this->load->view('tema/topo',$this->data);

    }



    public function clientesCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rCliente')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de clientes.');

           redirect(base_url());

        }



        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');



        $data['clientes'] = $this->Relatorios_model->clientesCustom($dataInicial,$dataFinal);



        $this->load->helper('mpdf');

        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);

        $this->data['menuRelatorios'] = 'clientes';

        $html = $this->load->view('relatorios/imprimir/imprimirClientes', $data, true);

        pdf_create($html, 'relatorio_clientes' . date('d/m/y'), TRUE);

    

    }



    public function clientesRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rCliente')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de clientes.');

           redirect(base_url());

        }



        $data['clientes'] = $this->Relatorios_model->clientesRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'clientes';

        $html = $this->load->view('relatorios/imprimir/imprimirClientes', $data, true);

        pdf_create($html, 'relatorio_clientes' . date('d/m/y'), TRUE);

    }

	

	public function fornecedoresCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de fornecedores.');

           redirect(base_url());

        }



        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');



        $data['fornecedores'] = $this->Relatorios_model->fornecedoresCustom($dataInicial,$dataFinal);



        $this->load->helper('mpdf');

        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);

        $this->data['menuRelatorios'] = 'fornecedores';

        $html = $this->load->view('relatorios/imprimir/imprimirFornecedores', $data, true);

        pdf_create($html, 'relatorio_fornecedores' . date('d/m/y'), TRUE);

    

    }



    public function fornecedoresRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de fornecedores.');

           redirect(base_url());

        }



        $data['fornecedores'] = $this->Relatorios_model->fornecedoresRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'fornecedores';

        $html = $this->load->view('relatorios/imprimir/imprimirFornecedores', $data, true);

        pdf_create($html, 'relatorio_fornecedores' . date('d/m/y'), TRUE);

    }



    public function despesasRapidP(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFornecedor')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de fornecedores.');

           redirect(base_url());

        }



        $data['fornecedores'] = $this->Relatorios_model->despesasRapidP();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'fornecedores';

        $html = $this->load->view('relatorios/imprimir/imprimirFornecedores', $data, true);

        pdf_create($html, 'relatorio_fornecedores' . date('d/m/y'), TRUE);

    }

	

	public function marcasCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rMarca')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de marcas.');

           redirect(base_url());

        }



        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');

		$franquia = $this->input->get('franquia');



        $data['marcas'] = $this->Relatorios_model->marcasCustom($dataInicial,$dataFinal,$franquia);



        $this->load->helper('mpdf');

        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);

        $this->data['menuRelatorios'] = 'marcas';

        $html = $this->load->view('relatorios/imprimir/imprimirMarcas', $data, true);

        pdf_create($html, 'relatorio_marcas' . date('d/m/y'), TRUE);

    

    }



    public function marcasRapid(){

        $und = false;

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rMarca')){

           $und = true;

        }



        $data['marcas'] = $this->Relatorios_model->marcasRapid($und);



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'marcas';

        $html = $this->load->view('relatorios/imprimir/imprimirMarcas', $data, true);

        pdf_create($html, 'relatorio_marcas' . date('d/m/y'), TRUE);

    }

    public function produtos(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de produtos.');

           redirect(base_url());

        }

        $this->data['menuRelatorios'] = 'produtos';

        $this->data['view'] = 'relatorios/rel_produtos';

        $this->load->view('tema/topo',$this->data);



    }

    public function produtosRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de produtos.');

           redirect(base_url());

        }



        $data['produtos'] = $this->Relatorios_model->produtosRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'produtos';

        //$this->load->view('relatorios/imprimir/imprimirProdutos', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);

        pdf_create($html, 'relatorio_produtos' . date('d/m/y'), TRUE);

    }



    public function produtosRapidMin(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de produtos.');

           redirect(base_url());

        }



        $data['produtos'] = $this->Relatorios_model->produtosRapidMin();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'produtos';

        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);

        pdf_create($html, 'relatorio_produtos' . date('d/m/y'), TRUE);

        

    }



    public function produtosCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de produtos.');

           redirect(base_url());

        }



        $precoInicial = $this->input->get('precoInicial');

        $precoFinal = $this->input->get('precoFinal');

        $estoqueInicial = $this->input->get('estoqueInicial');

        $estoqueFinal = $this->input->get('estoqueFinal');



        $data['produtos'] = $this->Relatorios_model->produtosCustom($precoInicial,$precoFinal,$estoqueInicial,$estoqueFinal);



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'produtos';

        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);

        pdf_create($html, 'relatorio_produtos' . date('d/m/y'), TRUE);

    }

    public function despesas(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());
        }

        $this->data['menuRelatorios'] = 'despesas';

        $this->data['view'] = 'relatorios/rel_despesas';

        $this->load->view('tema/topo',$this->data);



    }

    public function despesasRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());

        }



        $data['despesas'] = $this->Relatorios_model->despesasRapid();


        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'despesas';

        //$this->load->view('relatorios/imprimir/imprimirDespesas', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirDespesas', $data, true);
        pdf_create($html, 'relatorio_despesas' . date('d/m/y'), TRUE);

    }

    public function receitasRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());

        }



        $data['despesas'] = $this->Relatorios_model->receitasRapid();


        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'despesas';

        //$this->load->view('relatorios/imprimir/imprimirDespesas', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirDespesas', $data, true);
        pdf_create($html, 'relatorio_despesas' . date('d/m/y'), TRUE);

    }

    public function despesaRapidP(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());

        }



        $data['despesas'] = $this->Relatorios_model->despesaRapidP();


        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'despesas';

        //$this->load->view('relatorios/imprimir/imprimirDespesas', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirDespesas', $data, true);
        pdf_create($html, 'relatorio_despesas' . date('d/m/y'), TRUE);

    }

    public function contasRapidR(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());

        }



        $data['despesas'] = $this->Relatorios_model->contasRapidR();


        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'despesas';

        //$this->load->view('relatorios/imprimir/imprimirDespesas', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirDespesas', $data, true);
        pdf_create($html, 'relatorio_despesas' . date('d/m/y'), TRUE);

    }

    public function despesasCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rDespesa')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de despesas.');

           redirect(base_url());

        }



        $dataInicial           = $this->input->get('dataInicial');
        $dataFinal             = $this->input->get('dataFinal');
        $dataInicial_emissao   = $this->input->get('dataInicial_emissao');
        $dataFinal_emissao     = $this->input->get('dataFinal_emissao');
        $dataInicial_pagamento = $this->input->get('dataInicial_pagamento');
        $dataFinal_pagamento   = $this->input->get('dataFinal_pagamento');
        $cliente               = $this->input->get('cliente');
        $fornecedor            = $this->input->get('fornecedor');
        $tipo                  = $this->input->get('tipo');
        $status                = $this->input->get('status');


        $data['despesas'] = $this->Relatorios_model->despesasCustom($dataInicial, $dataFinal, $dataInicial_emissao, $dataFinal_emissao, $dataInicial_pagamento, $dataFinal_pagamento, $cliente, $fornecedor, $tipo, $status);



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'despesas';

        $html = $this->load->view('relatorios/imprimir/imprimirDespesas', $data, true);

        pdf_create($html, 'relatorio_despesas' . date('d/m/y'), TRUE);

    }



    public function orcamentos(){



        // if(!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOrcamento')){

        //    $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de orçamentos.');

        //    redirect(base_url());

        // }

        $this->data['menuRelatorios'] = 'orcamentos';

        $this->data['view'] = 'relatorios/rel_orcamentos';

        $this->data['franquias'] = $this->Franquias_model->get(100,0,false);

        $this->load->view('tema/topo',$this->data);



    }



    public function orcamentosCustom(){

        // if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rOrcamento')){
        //    $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de orçamentos.');
        //    redirect(base_url());
        // }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        if($this->session->userdata('id') == 4)
            $franquia = $this->input->get('franquia');
        else
            $franquia = $this->session->userdata('id');

        

        $data['orcamentos'] = $this->Relatorios_model->orcamentosCustom($dataInicial,$dataFinal,$franquia);

        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'orcamentos';

        

        $html = $this->load->view('relatorios/imprimir/imprimirOrcamentos', $data, true);
        pdf_create($html, 'relatorio_orcamentos' . date('d/m/y'), TRUE);

    }



    public function orcamentosRapid(){

        // if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rOrcamento')){

        //    $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de orçamentos.');

        //    redirect(base_url());

        // }



        $data['orcamentos'] = $this->Relatorios_model->orcamentosRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'orcamentos';

        //$this->load->view('relatorios/imprimir/imprimirOrcamentos', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirOrcamentos', $data, true);
        pdf_create($html, 'relatorio_orcamentos' . date('d/m/y'), TRUE);

    }



    public function servicos(){



        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rServico')){

           $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');

           redirect(base_url());

        }

        $this->data['menuRelatorios'] = 'servicos';

        $this->data['view'] = 'relatorios/rel_servicos';

		$this->data['franquias'] = $this->Franquias_model->get(100,0,false);

		

       	$this->load->view('tema/topo',$this->data);



    }



    public function servicosCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rServico')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de serviços.');

           redirect(base_url());

        }



        $precoInicial = $this->input->get('precoInicial');

        $precoFinal = $this->input->get('precoFinal');

		$franquia = $this->input->get('franquia');

		

        $data['servicos'] = $this->Relatorios_model->servicosCustom($precoInicial,$precoFinal,$franquia);

        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'servicos';

        

        $html = $this->load->view('relatorios/imprimir/imprimirServicos', $data, true);

        pdf_create($html, 'relatorio_servicos' . date('d/m/y'), TRUE);

    }



    public function servicosRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rServico')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de serviços.');

           redirect(base_url());

        }



        $data['servicos'] = $this->Relatorios_model->servicosRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'servicos';

        //$this->load->view('relatorios/imprimir/imprimirServicos', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirServicos', $data, true);

        pdf_create($html, 'relatorio_servicos' . date('d/m/y'), TRUE);

    }



    public function os(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rOs')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de OS.');

           redirect(base_url());

        }

        $this->data['menuRelatorios'] = 'os';

        $this->data['view'] = 'relatorios/rel_os';

       	$this->load->view('tema/topo',$this->data);

    }



    public function osRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rOs')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de OS.');

           redirect(base_url());

        }



        $data['os'] = $this->Relatorios_model->osRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'os';

        //$this->load->view('relatorios/imprimir/imprimirOs', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirOs', $data, true);

        pdf_create($html, 'relatorio_os' . date('d/m/y'), TRUE);

    }



    public function osCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rOs')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de OS.');

           redirect(base_url());

        }

        

        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');

        $cliente = $this->input->get('cliente');

        $responsavel = $this->input->get('responsavel');

        $status = $this->input->get('status');

        $data['os'] = $this->Relatorios_model->osCustom($dataInicial,$dataFinal,$cliente,$responsavel,$status);

        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'os';

        //$this->load->view('relatorios/imprimir/imprimirOs', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirOs', $data, true);

        pdf_create($html, 'relatorio_os' . date('d/m/y'), TRUE);

    }





    public function financeiro(){



        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios financeiros.');

           redirect(base_url());

        }



        $this->data['menuRelatorios'] = 'financeiro';

        $this->data['view'] = 'relatorios/rel_financeiro';

        $this->load->view('tema/topo',$this->data);

    

    }





    public function financeiroRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios financeiros.');

           redirect(base_url());

        }



        $data['lancamentos'] = $this->Relatorios_model->financeiroRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'financeiro';

        //$this->load->view('relatorios/imprimir/imprimirFinanceiro', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirFinanceiro', $data, true);

        pdf_create($html, 'relatorio_os' . date('d/m/y'), TRUE);

    }



    public function financeiroCustom(){



        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios financeiros.');

           redirect(base_url());

        }



        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');

        $tipo = $this->input->get('tipo');

        $situacao = $this->input->get('situacao');



        $data['lancamentos'] = $this->Relatorios_model->financeiroCustom($dataInicial,$dataFinal,$tipo,$situacao);

        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'financeiro';

        //$this->load->view('relatorios/imprimir/imprimirFinanceiro', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirFinanceiro', $data, true);

        pdf_create($html, 'relatorio_financeiro' . date('d/m/y'), TRUE);

    }







    public function vendas(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de vendas.');

           redirect(base_url());

        }



        $this->data['menuRelatorios'] = 'vendas';

        $this->data['view'] = 'relatorios/rel_vendas';

        $this->load->view('tema/topo',$this->data);

    }



    public function vendasRapid(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de vendas.');

           redirect(base_url());

        }

        $data['vendas'] = $this->Relatorios_model->vendasRapid();



        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'vendas';

        //$this->load->view('relatorios/imprimir/imprimirOs', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);

        pdf_create($html, 'relatorio_vendas' . date('d/m/y'), TRUE);

    }



    public function vendasCustom(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){

           $this->session->set_flashdata('error','Você não tem permissão para gerar relatórios de vendas.');

           redirect(base_url());

        }

        $dataInicial = $this->input->get('dataInicial');

        $dataFinal = $this->input->get('dataFinal');

        $cliente = $this->input->get('cliente');

        $responsavel = $this->input->get('responsavel');



        $data['vendas'] = $this->Relatorios_model->vendasCustom($dataInicial,$dataFinal,$cliente,$responsavel);

        $this->load->helper('mpdf');

        $this->data['menuRelatorios'] = 'vendas';

        //$this->load->view('relatorios/imprimir/imprimirOs', $data);

        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);

        pdf_create($html, 'relatorio_vendas' . date('d/m/y'), TRUE);

    }

}

