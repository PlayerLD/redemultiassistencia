<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// use Bcash\Exception\ValidationException;
// use Bcash\Exception\ConnectionException;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Sistemaos extends CI_Controller {
    /** 
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */

    public $STATUS_DESCRIPTION = array(
        'new'       => 1,
        'waiting'   => 2,
        'paid'      => 3,
        'unpaid'    => 4,
        'refunded'  => 5,
        'contested' => 6,
        'canceled'  => 7,
        'link'      => 8,
        'expired'   => 9
    );
    
    public function __construct() {
        parent::__construct();
        $this->load->model('sistemaos_model','',TRUE);
        $this->load->model('os_model', '', TRUE);
        $this->load->model('orcamentos_model', '', TRUE);
        $this->load->model('franquias_model', '', TRUE);
        $this->load->model('fornecedores_model','',TRUE);
        $this->load->model('planos_model','',TRUE);
        $this->load->model('clientes_model','',TRUE);
        $this->load->model('pagamentos_model','',TRUE);

        $this->load->model('produtos_model', '', TRUE);
         
    }
    public function index() {
        // echo "<pre>";
        // var_dump($this->session->userdata('user_id'));
        // echo "</pre>";
        // exit;
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('sistemaos/login');
        }
        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $franquia = $this->db->get('franquias')->row();
        $this->pagamentos_model->verify($franquia, false);

        $this->data['ordens'] = $this->sistemaos_model->getOsAbertas();
        $this->data['produtos'] = $this->sistemaos_model->getProdutosMinimo();
        $this->data['os'] = $this->sistemaos_model->getOsEstatisticas();
        $this->data['estatisticas_financeiro'] = $this->sistemaos_model->getEstatisticasFinanceiro();
        $this->data['orcamentos_pendentes'] = $this->sistemaos_model->getOrcamentosPendentes();
        $this->data['fornecedores'] = $this->fornecedores_model->count('fornecedores');
        $this->data['menuPainel'] = 'Painel';
        $this->data['view'] = 'sistemaos/painel';        
        $this->data['taxas'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$this->produtos_model->count('produto_taxas', $this->session->userdata('id')),$this->uri->segment(3));
        $this->load->view('tema/topo',  $this->data);

    }

    function countOrcamentos(){
        $this->db->select("count(*) as total");
        $this->db->from("orcamentos");
        $this->db->where("idFranquia", $this->session->userdata('id'));
        $this->db->where("status = 0 OR  emailResposta = NULL");
        //$this->db->where("status != 2 OR status = 3 OR status = 4");
        //$this->db->or("status != 3");
        //$this->db->or("status != 4");
        $result = $this->db->get()->row();
        return $result;
    }

    public function verificarOrcamentosPendentes(){
        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $franquia = $this->db->get('franquias')->row();
        $this->pagamentos_model->verify($franquia, false, false, true);
      $orcamentosPendentes = $this->sistemaos_model->getOrcamentosPendentes();
      echo json_encode($orcamentosPendentes);   
  }

  public function count_if($table, $column = 'idFranquia', $value = '')
  {
     if ($value == '')
      $value = $this->session->userdata('id');

  if ($column != '')
      $this->db->where($column, $value);
  $query = $this->db->get($table)->result();
  return count($query);
}
public function minhaConta() {
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }

    $this->db->where('idFranquias',$this->session->userdata('id'));
    $this->db->limit(1);
    $franquia = $this->db->get('franquias')->row();
    $this->pagamentos_model->verify($franquia, false);

    if(!empty($_POST['id'])){
        $data['facebook'] = $this->input->post('facebook');
        $arquivo_assinatura = false;
        if(!empty($_FILES['ass_email']['tmp_name']))
            $arquivo_assinatura = $this->do_upload('ass_email');
        if($arquivo_assinatura)
            $data['ass_email'] = $arquivo_assinatura;
        $arquivo_emailmarketing = false;
        if(!empty($_FILES['email_marketing']['tmp_name']))
            $arquivo_emailmarketing = $this->do_upload('email_marketing');
        if($arquivo_emailmarketing)
            $data['email_marketing'] = $arquivo_emailmarketing;
        $this->franquias_model->info_email('franquias', $data, 'idFranquias', $this->input->post('id'));
    }

    $this->data['franquia'] = $this->franquias_model->getById($this->session->userdata('id'));
    $this->data['view'] = 'sistemaos/minhaConta';
    $this->data['taxas'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$this->produtos_model->count('produto_taxas', $this->session->userdata('id')),$this->uri->segment(3));
    $this->load->view('tema/topo',  $this->data);

}
public function excluir_ass_email() {
    $id = $this->session->userdata('id');
    $this->franquias_model->excluir_ass_email($id);
    redirect('sistemaos/minhaConta');

}
public function excluir_email_marketing() {
    $id = $this->session->userdata('id');
    $this->franquias_model->excluir_email_marketing($id);
    redirect('sistemaos/minhaConta');

}
public function alterarCartao() {
    $id = $this->session->userdata('id');
    if (!empty($_POST['nome_cartao']) && !empty($_POST['num_cartao']) && !empty($_POST['mes_cartao']) && !empty($_POST['ano_cartao']) && !empty($_POST['cvv_cartao'])){
        $data_cartao = array(
            'franquia_id'      => $id,
            'nome_cartao'      => $this->input->post('nome_cartao'),
            'cpf_cartao'       => $this->input->post('cpf_cartao'),
            'num_cartao'       => $this->input->post('num_cartao'),
            'mes_cartao'       => $this->input->post('mes_cartao'),
            'ano_cartao'       => $this->input->post('ano_cartao'),
            'cvv_cartao'       => $this->input->post('cvv_cartao')
        );

        $cad_cartao = $this->franquias_model->addCartao($data_cartao);

        if(!$cad_cartao){
            $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
        }
    }
    redirect('sistemaos/minhaConta');

}

public function alterarGN() {
    $id = $this->session->userdata('id');
    if (!empty($_POST['client_id']) || !empty($_POST['client_secret']) || !empty($_POST['dados_bancarios'])|| !empty($_POST['identificador_conta'])){
        $data = array(
            'identificador_conta'      => $this->input->post('identificador_conta'),
            'client_id'      => $this->input->post('client_id'),
            'client_secret'       => $this->input->post('client_secret'),
            'dados_bancarios'       => $this->input->post('dados_bancarios')
        );

        $cad_cartao = $this->franquias_model->edit('franquias',$data,'idFranquias',$id);

        if(!$cad_cartao){
            $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
        }
    }
    redirect('sistemaos/minhaConta');

}
public function alterarSenha() {
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }
    $oldSenha = $this->input->post('oldSenha');
    $senha = $this->input->post('novaSenha');
    $result = $this->sistemaos_model->alterarSenha($senha,$oldSenha,$this->session->userdata('id'));
    if($result){
        $this->session->set_flashdata('success','Senha Alterada com sucesso!');
        redirect(base_url() . 'index.php/sistemaos/minhaConta');
    }
    else{
        $this->session->set_flashdata('error','Ocorreu um erro ao tentar alterar a senha!');
        redirect(base_url() . 'index.php/sistemaos/minhaConta');

    }
}
public function pesquisar() {
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }

    $termo = $this->input->get('termo');
    $data['results'] = $this->sistemaos_model->pesquisar($termo);
    $this->data['produtos'] = $data['results']['produtos'];
    $this->data['servicos'] = $data['results']['servicos'];
    $this->data['marcas'] = $data['results']['marcas'];
    $this->data['os'] = $data['results']['os'];
    $this->data['clientes'] = $data['results']['clientes'];
    $this->data['view'] = 'sistemaos/pesquisa';
    $this->data['taxas'] = $this->produtos_model->getTaxas('produto_taxas','idTaxas, titulo, valor','',$this->produtos_model->count('produto_taxas', $this->session->userdata('id')),$this->uri->segment(3));
    $this->load->view('tema/topo',  $this->data);

}
public function login(){

    $this->load->view('sistemaos/login');

}
public function sair(){
    $this->session->sess_destroy();
    redirect('sistemaos/login');
}
public function verificarLogin(){
    $ajax = $this->input->get('ajax');

    $this->load->library('form_validation');
    if ($this->form_validation->run('sistemaos') == false) {
        if($ajax == true){
            $json = array('result' => false, 'type' => '1' , 'error' => 'Preencha os dados de acesso corretamente.');
            echo json_encode($json);
        }else{
            $this->session->set_flashdata('error','Os dados de acesso estão incorretos.');
            redirect($this->login);
        }
    }else{
        $tipoLogin = $this->input->post('tipo_login');
        $email     = $this->input->post('email');
        $senha     = $this->input->post('senha');
        $cod       = $idv = $this->input->post('idv');

        $this->load->library('encrypt');   
        $senha = $this->encrypt->sha1($senha);
        //$this->db->where('email',$email);
        //$this->db->where('senha',$senha);
        //$this->db->where('idFranquias',$idv);
        //$this->db->where('situacao',1);
        //$this->db->limit(1);
        //$franquia = $this->db->get('franquias')->row();

        $resultfran = $this->sistemaos_model->buscaUsuarioFranquia($email, $senha, $cod);
        $resultfranuser = $this->sistemaos_model->buscaUsuarioUser($email, $senha, $cod);
       

        if (count($resultfran) > 0 && $tipoLogin == 'is_franquia') {
            
            $this->pagamentos_model->verify($resultfran, $ajax);
            
            $dados = array('nome' => $resultfran->nome, 'id' => $resultfran->idFranquias,'permissao' => $resultfran->permissoes_id , 'logado' => TRUE);
            $this->session->set_userdata($dados);

            if($ajax == true){
                $json = array('result' => true, 'type' => '2' , 'error' => 'Os dados de acesso estão incorretos.');
                echo json_encode($json);
            }else{
                redirect(base_url().'sistemaos');
            }

        } else if(count($resultfranuser) > 0 && $tipoLogin == 'is_usuario'){

            $resulfranlogin = $this->sistemaos_model->dadosFranquiaLogin($cod);

            $this->pagamentos_model->verify($resulfranlogin, $ajax);
            
            //$dados = array('session_id' => md5($resultfranuser->idFranquias), 'nome' => $resultfranuser->nome, 'id' => $resultfranuser->idFranquias,'permissao' => $resultfranuser->permissoes_id , 'logado' => TRUE);
            $dados = array('nome' => $resultfranuser->nome, 'id' => $resultfranuser->idFranquias, 'user_id' => $resultfranuser->idUsuarios,'permissao' => $resultfranuser->permissoes_id , 'logado' => TRUE);
            //$dados = array('nome' => $resultfranuser->nome, 'id' => $resultfranuser->idFranquias,'permissao' => $resultfranuser->permissoes_id , 'logado' => TRUE);

            $this->session->set_userdata($dados);

        // echo "<pre>";
        // var_dump($dados);
        // echo "</pre>";
        // exit;

            if($ajax == true){
                $json = array('result' => true, 'type' => '2' , 'error' => 'Os dados de acesso estão incorretos.');
                echo json_encode($json);
            }else{
                redirect(base_url().'sistemaos');
            }

        }else{
            // só  preicisei alterar o result para verdadeiro.
            if($ajax == true){
                $json = array('result' => true, 'type' => '3' , 'error' => 'Os dados de acesso estão incorretos.');
                echo json_encode($json);
            }else{
                $this->session->set_flashdata('error','Os dados de acesso estão incorretos.');
                redirect($this->login);
            }
        }

        /*
        if(count($franquia) > 0){

            $this->pagamentos_model->verify($franquia, $ajax);

            $dados = array('nome' => $franquia->nome, 'id' => $franquia->idFranquias,'permissao' => $franquia->permissoes_id , 'logado' => TRUE);
            $this->session->set_userdata($dados);

            if($ajax == true){
                $json = array('result' => true, 'type' => '2' , 'error' => '');
                echo json_encode($json);
            }else{
                redirect(base_url().'sistemaos');
            }

        }else{
            if($ajax == true){
                $json = array('result' => false, 'type' => '3' , 'error' => 'Os dados de acesso estão incorretos.');
                echo json_encode($json);
            }else{
                $this->session->set_flashdata('error','Os dados de acesso estão incorretos.');
                redirect($this->login);
            }
        }*/

}

}

public function comprovante_pagamento($orcamento_id) {

    $this->data['custom_error'] = '';

    if(!empty($_FILES)) {
    if (!empty($_FILES['arquivo'])){
        $comprovante_pagamento = false;
        if(!empty($_FILES['arquivo']['tmp_name']))
            $comprovante_pagamento = $this->in_upload('arquivo');
        if($comprovante_pagamento){
            $this->orcamentos_model->edit('orcamentos', array('comprovante_pagamento'  => $comprovante_pagamento, 'status'  => 3), 'idOrcamento', $orcamento_id);
            $this->session->set_flashdata('success', 'Comprovante enviado com sucesso!');
                redirect(base_url() . 'index.php/sistemaos/comprovante_pagamento/'.$orcamento_id);
            }
        }


    }

    
    $this->load->view('pagamentos/comprovante', $this->data);
}

public function cadastrar_cartao($user_id, $os_id) {

    $this->load->library('form_validation');
    $this->data['custom_error'] = '';

    if(!empty($_POST)) {
        if($this->form_validation->run('cadastro_cartao') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {
            if (!empty($_POST['nome_cartao']) && !empty($_POST['cpf_cartao']) && !empty($_POST['num_cartao']) && !empty($_POST['vencimento_cartao']) && !empty($_POST['nascimento'])){
                $nascimento = explode('/', $this->input->post('nascimento'));
                $nascimento = $nascimento[2].'-'.$nascimento[1].'-'.$nascimento[0];
                $data_cartao = array(
                    'cliente_id'      => $user_id,
                    'nome_cartao'      => $this->input->post('nome_cartao'),
                    'cpf_cartao'       => $this->input->post('cpf_cartao'),
                    'nascimento'       => $nascimento,
                    'mes_cartao'       => explode('/', $this->input->post('vencimento_cartao'))[0],
                    'ano_cartao'       => explode('/', $this->input->post('vencimento_cartao'))[1],
                    'num_cartao'       => $this->input->post('num_cartao'),
                            // 'cvv_cartao'       => $this->input->post('cvv_cartao'),
                );

                $cad_cartao = $this->franquias_model->addCartao($data_cartao);

                if(!$cad_cartao){
                    $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
                }
            }

            $this->session->set_flashdata('success', 'Cartão cadastrado com sucesso!.');
            redirect(base_url() . 'index.php/sistemaos/cliente_pagamento/'.$os_id);
        }
    }
    
    $this->load->view('pagamentos/cadastrar_cartao', $this->data);
}

public function cadastrar_cartao_orcamento($orcamento_id) {

    $this->load->library('form_validation');
    $this->data['custom_error'] = '';

    $orcamento = $this->orcamentos_model->getById($orcamento_id);

    if(empty($orcamento)){
        echo 'Este orçamento não existe';
        exit;
    }

    if(!empty($_POST)) {
        if($this->form_validation->run('cadastro_cartao') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        }else{
            if (!empty($_POST['nome_cartao']) && !empty($_POST['cpf_cartao']) && !empty($_POST['num_cartao']) && !empty($_POST['vencimento_cartao'])&& !empty($_POST['nascimento'])){
                if(empty($_POST['token_pagamento'])){
                    $this->session->set_flashdata('error','Verifique os dados e tente novamente.');
                }else{
                    
                    $this->orcamentos_model->edit('orcamentos', array('token_pagamento'  => $this->input->post('token_pagamento')), 'idOrcamento', $orcamento_id);
                    $nascimento = explode('/', $this->input->post('nascimento'));
                    $nascimento = $nascimento[2].'-'.$nascimento[1].'-'.$nascimento[0];
                    
                    $data_cartao = array(
                        'orcamento_id'      => $orcamento_id,
                        'nome_cartao'      => $this->input->post('nome_cartao'),
                        'cpf_cartao'       => $this->input->post('cpf_cartao'),
                        'nascimento'       => $nascimento,
                        'mes_cartao'       => explode('/', $this->input->post('vencimento_cartao'))[0],
                        'ano_cartao'       => explode('/', $this->input->post('vencimento_cartao'))[1],
                        'num_cartao'       => $this->input->post('num_cartao'),
                        'cvv_cartao'       => $this->input->post('cvv_cartao'),
                    );

                    $cad_cartao = $this->franquias_model->addCartao($data_cartao);
                    if(!$cad_cartao){
                        $this->session->set_flashdata('error','Já existe um cartão com este número cadastrado.');
                    }else
                    $this->session->set_flashdata('success', 'Cartão cadastrado com sucesso!.');
                }

                redirect(base_url() . 'index.php/sistemaos/cliente_pagamento_orcamento/'.$orcamento_id);
            }
        }
    }

    $this->db->where('idFranquias', $orcamento->idFranquia);
    $this->db->limit(1);
    $franquia = $this->db->get('franquias')->row();

    $this->data['franquia'] = $franquia;
    $this->data['cvv'] = true;
    $this->data['id_orcamento'] = $orcamento_id.' - Conserto de '.$orcamento->tipo.' (marca '.$orcamento->marca.', modelo '.$orcamento->modelo.')';
    
    $this->load->view('pagamentos/cadastrar_cartao', $this->data);
}

public function cliente_pagamento_orcamento($orcamento_id){
    $this->data['custom_error'] = '';
    $orcamento = $this->orcamentos_model->getById($orcamento_id);

        // valida OS inexistente
    if(empty($orcamento)){
        echo 'parou aki';
        exit;
    }

    $cartao = $this->planos_model->cartoes(array('orcamento_id' => $orcamento_id));

        // Se não existir cartões cadastrados, redireciona para o cadastro de cartão de crédito
    if(empty($cartao)){
        // $this->session->set_flashdata('success', 'Adicione os dados do seu cartão.');
        redirect('sistemaos/cadastrar_cartao_orcamento/'.$orcamento->clientes_id.'/'.$orcamento_id);
    }

    $bandeira = $this->getCreditCardType($cartao[0]->num_cartao);
    $pagamento    = $this->pagamentos_model->obter(array('orcamento_id'=>$orcamento_id, 'status !=' => 0));

    if(!empty($pagamento)){
        if($pagamento->status == 3){
            $this->data['card_process'] = 'Um pagamento já foi realizado para este orçamento.';
            $this->load->view('pagamentos/confirmar_pagamento', $this->data);
        }else{
            $this->data['card_process'] = 'Um pagamento está sendo processado para este orçamento.';
            $this->load->view('pagamentos/confirmar_pagamento', $this->data);
        }
    }
    if(!empty($_POST['cvv_cartao'])){
        if(!empty($orcamento)){
            $this->db->where('idFranquias', $orcamento->idFranquia);
            $this->db->limit(1);
            $franquia = $this->db->get('franquias')->row();

            $dados  = (object)array(
                'orcamento_id' => $orcamento_id,
                'os_id'     => '',
                'plano_id'     => '',
                'franquia_id'  => '',
                'clientId'     => $franquia->client_id,
                'clientSecret' => $franquia->client_secret,
                'nome'         => $orcamento->nome,
                'empresa'      => '',
                'cpf'          => '',
                'telefone'     => $orcamento->telefone,
                'celular'      => @$this->input->post('celular'),
                'logradouro'   => @$this->input->post('rua'),
                'numero'       => @$this->input->post('numero'),
                'complemento'  => @$this->input->post('complemento'),
                'bairro'       => @$this->input->post('bairro'),
                'cidade'       => @$this->input->post('cidade'),
                'estado'       => @$this->input->post('estado'),
                'cep'          => @$this->input->post('cep'),
                'email'        => $orcamento->email
            );


            $cartao  = (object)array(
                'token'           => $orcamento->token_pagamento,
                'cpf'             => $cartao[0]->cpf_cartao,
                'nascimento'      => $cartao[0]->nascimento,
                'vezes'           => $_POST['installments'],
                'cartao_validade' => $cartao[0]->mes_cartao."/".$cartao[0]->ano_cartao,
                'cartao_nome'     => $cartao[0]->nome_cartao,
                'cartao_numero'   => $cartao[0]->num_cartao,
                'cartao_codigo'   => $_POST['cvv_cartao']
            );

            $total    = str_replace('.', '', $orcamento->preco);
            $itens    = array();
            $itens[]    = (object) array(
                'id'          => $orcamento->idOrcamento,
                'titulo'      => 'Pagamento do orçamento #'.$orcamento->idOrcamento,
                'valor'       => $orcamento->preco,
                'obs'         => $orcamento->obs,
                            // 'desconto' => $plano->,
                'qty'         => 1
            );

            $pag = $this->pagamentos_model->pagamento($dados, $itens, $total, $cartao);
            $this->data['card_process'] = $pag;

        }else{
            $this->session->set_flashdata('error', 'Orçamento não existe.');
            redirect($this->login);
        }
    }
    if(is_array($cartao))
        $this->data['final_cartao'] = substr(@$cartao[0]->num_cartao, -4, 4);
    else
        $this->data['final_cartao'] = substr(@$cartao->num_cartao, -4, 4);


    $this->data['cvv_cartao'] = @$cartao->cvv_cartao;
    $this->db->where('idFranquias', $orcamento->idFranquia);
    $this->db->limit(1);
    $franquia = $this->db->get('franquias')->row();

    $this->data['franquia'] = $franquia;
    $this->data['orcamento'] = $orcamento;
    $this->data['bandeira']  = $bandeira;
    $this->data['servicos']  = array(
        (object) array(
            'nomeServico'      => 'Conserto de '.$orcamento->tipo.' (marca '.$orcamento->marca.', modelo '.$orcamento->modelo.')',
            'descricaoServico' => $orcamento->obs,
            'precoServico'     => $orcamento->preco
        )
    );
    $this->data['endereco'] = true;
    $this->load->view('pagamentos/confirmar_pagamento', $this->data);
}

function getCreditCardType($str)
    {

        $matchingPatterns = [
            'visa' =>  '/^4[0-9]{12}(?:[0-9]{3})?$/',
            'mastercard' =>  '/^5[1-5][0-9]{14}$/',
            'amex' =>  '/^3[47][0-9]{13}$/',
            'diners' =>  '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
            'discover' =>  '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
            'elo' =>  '/^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/',
            'jcb' =>  '/^(?:2131|1800|35\d{3})\d{11}$/',
            'hipercard' =>  '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
            'aura' =>  '/^50[0-9]{17}$/'
        ];

        $type = false;
        if(preg_match($matchingPatterns['visa'], $str))
            $type = 'visa';
        elseif(preg_match($matchingPatterns['mastercard'], $str))
            $type = 'mastercard';
        elseif(preg_match($matchingPatterns['amex'], $str))
            $type = 'american_express';
        elseif(preg_match($matchingPatterns['diners'], $str))
            $type = 'diners';
        elseif(preg_match($matchingPatterns['elo'], $str))
            $type = 'elo';
        elseif(preg_match($matchingPatterns['hipercard'], $str))
            $type = 'hipercard';
        elseif(preg_match($matchingPatterns['aura'], $str))
            $type = 'aura';
        elseif(preg_match($matchingPatterns['discover'], $str))
            $type = 'discover';
        elseif(preg_match($matchingPatterns['jcb'], $str))
            $type = 'jcb';

            return $type;
    }

public function cliente_pagamento($os_id){
    $this->data['custom_error'] = '';
    $os           = $this->os_model->getById($os_id);

        // valida OS inexistente
    if(empty($os)){
        echo 'parou aki';
        exit;
    }

    $cartao = $this->planos_model->cartoes(array('cliente_id' => $os->clientes_id));

        // Se não existir cartões cadastrados, redireciona para o cadastro de cartão de crédito
    if(empty($cartao)){
        $this->session->set_flashdata('success', 'Você ainda não possui um cartão cadastrado.');
        redirect('sistemaos/cadastrar_cartao/'.$os->clientes_id.'/'.$os_id);
    }

    $pagamento    = $this->pagamentos_model->obter(array('os_id'=>$os_id));

    if(!empty($pagamento)){
        if($pagamento->status != 7 || $pagamento->status != 8 || $pagamento->status != 6){
            $this->data['card_process'] = 'Um pagamento está sendo processado para esta ordem de serviço.';
            $this->load->view('pagamentos/confirmar_pagamento', $this->data);
        }elseif($pagamento->status == 3 || $pagamento->status == 4){
            $this->data['card_process'] = 'Um pagamento já foi realizado para esta ordem de serviço.';
            $this->load->view('pagamentos/confirmar_pagamento', $this->data);
        }
    }

    $produtos = $this->os_model->getProdutos($os_id);
    $servicos = $this->os_model->getServicos($os_id);
    if(!empty($_POST['cvv_cartao'])){
        $cliente =  $this->clientes_model->getById($os->clientes_id);
        $this->db->where('idFranquias',$this->session->userdata('id'));
        $this->db->limit(1);
        $franquia = $this->db->get('franquias')->row();

        if(!empty($cliente)){
            $dados  = (object)array(
                'orcamento_id' => '',
                'os_id'        => $os_id,
                'plano_id'     => '',
                'franquia_id'  => '',
                'clientId'     => $franquia->client_id,
                'clientSecret' => $franquia->client_secret,
                'nome'         => $cliente->nomeCliente,
                'empresa'      => '',
                'cpf'          => $cliente->documento,
                'telefone'     => $cliente->telefone,
                'celular'      => $cliente->celular,
                'logradouro'   => $cliente->rua,
                'numero'       => $cliente->numero,
                'complemento'  => $cliente->complemento,
                'bairro'       => $cliente->bairro,
                'cidade'       => $cliente->cidade,
                'estado'       => $cliente->estado,
                'cep'          => $cliente->cep,
                'email'        => $cliente->email,
            );

            $bandeira = $this->bcash->getCreditCardType($cartao[0]->num_cartao);

            $cartao  = (object)array(
                'cpf'             => $cartao[0]->cpf_cartao,
                'cartao_bandeira' => $bandeira,
                'cartao_validade' => $cartao[0]->mes_cartao."/".$cartao[0]->ano_cartao,
                'cartao_nome'     => $cartao[0]->nome_cartao,
                'cartao_numero'   => $cartao[0]->num_cartao,
                'cartao_codigo'   => $_POST['cvv_cartao']
            );

            $total    = (float)0;
            $itens    = array();
            if(!empty($produtos)){
                foreach($produtos as $p){
                    $itens[]    = (object) array(
                        'id'          => $p->idProdutos,
                        'titulo'      => $p->descricao,
                        'valor'       => $p->precoVenda,
                                    // 'desconto' => $plano->,
                        'qty'         => $p->quantidade
                    );
                    $total += $p->precoVenda;
                }
            }

            if(!empty($servicos)){
                foreach($servicos as $p){
                    $itens[]    = (object) array(
                        'id'          => $p->idServicos_os,
                        'titulo'      => 'Mão de obra - '.$p->nomeServico,
                        'valor'       => $p->precoServico,
                                    // 'desconto' => $p->,
                        'qty'         => 1
                    );
                    $total += $p->precoServico;
                }
            }

            $pag = $this->pagamentos_model->pagamento($dados, $itens, $total, $cartao, date('d'));
            $this->data['card_process'] = $pag;

        }else{
            $this->session->set_flashdata('error', 'Sua franquia não tem um plano definido.');
            redirect($this->login);
        }
    }
    if(is_array($cartao))
        $this->data['final_cartao'] = substr(@$cartao[0]->num_cartao, -4, 4);
    else
        $this->data['final_cartao'] = substr(@$cartao->num_cartao, -4, 4);

    $this->data['clientes_id']  = $os->clientes_id;
    $this->data['os_id']        = $os_id;
    $this->data['equipamentos'] = $this->os_model->getEquipamentos($os_id);
    $this->data['produtos']     = $produtos;
    $this->data['servicos']     = $servicos;
        // $this->data['pagamentos']   = $this->os_model->getPagamentos($os_id);
        // $this->data['emitente']     = $this->sistemaos_model->getEmitente($this->session->userdata('id'));
    $this->load->view('pagamentos/confirmar_pagamento', $this->data);
}

function notificacao($pagamentos_id) {

    
    $token = $this->input->post('notification');
    $pagamento = $this->pagamentos_model->obter(array('id'=>$pagamentos_id));

    $this->db->where('idFranquias',$pagamento->franquia_id);
    $this->db->limit(1);
    $franquia = $this->db->get('franquias')->row();
    
    if(!empty($franquia->client_id) && !empty($franquia->client_secret)){
        $clientId = $franquia->clientId;
        $clientSecret = $franquia->clientSecret;
    }else{
        $clientId = 'Client_Id_946fe867391443411ea92a569460cac54a4dc9c4'; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
        $clientSecret = 'Client_Secret_6d9be7458f2df3c4eeb196dcfaf8f56b91499e14'; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
    }

    $options = [
      'client_id' => $clientId,
      'client_secret' => $clientSecret,
      ];
    
    if(empty($pagamento) || empty($token))
        return;
     
    $params = array(
      'token' => $token
    );
     
    try {
        $api = new Gerencianet($options);
        $chargeNotification = $api->getNotification($params, array());
      // Para identificar o status atual da sua transação você deverá contar o número de situações contidas no array, pois a última posição guarda sempre o último status. Veja na um modelo de respostas na seção "Exemplos de respostas" abaixo.
      
      // Veja abaixo como acessar o ID e a String referente ao último status da transação.
        
        // Conta o tamanho do array data (que armazena o resultado)
        $i = count($chargeNotification["data"]);
        // Pega o último Object chargeStatus
        $ultimoStatus = $chargeNotification["data"][$i-1];
        // Acessando o array Status
        $status = $ultimoStatus["status"];
        // Obtendo o ID da transação    
        $charge_id = $ultimoStatus["identifiers"]["charge_id"];
        // Obtendo a String do status atual
        

        if(isset($this->STATUS_DESCRIPTION[$status["current"]]))
            $status_id = $this->STATUS_DESCRIPTION[$status["current"]];
        else
            return;
        

        // Com estas informações, você poderá consultar sua base de dados e atualizar o status da transação especifica, uma vez que você possui o "charge_id" e a String do STATUS
        $this->pagamentos_model->editar($pagamento->id, array('status' => $status_id));
        
        if(!empty($pagamento->orcamento_id))
                    $this->orcamentos_model->edit('orcamentos', array('status'  => 4), 'idOrcamento', $pagamento->orcamento_id);

        // Pedido aprovado
        switch ($status_id) {
            case 3:
                $message = "Pagamento #{$pagamento->bcash_id} efetuado com sucesso!";
                break;
            case 4:
                $message = "Não foi possível confirmar o pagamento (#{$pagamento->bcash_id}) da cobrança.";
                break;
            case 5:
                $message = "Pagamento #{$pagamento->bcash_id} devolvido pelo lojista ou pelo intermediador Gerencianet.";
                break;
            case 6:
                $message = "Pagamento #{$pagamento->bcash_id} em processo de contestação.";
                break;
            case 7:
                $message = "Cobrança cancelada pelo vendedor ou pelo pagador (#{$pagamento->bcash_id}).";
                break;
            
            default:
                $message = '';
                break;
        }

        if($message)
            $this->pagamentos_model->email_pedido($pagamentos_id, $message);
     
    } catch (GerencianetException $e) {
    } catch (Exception $e) {
    }








}

public function backup(){
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){
       $this->session->set_flashdata('error','Você não tem permissão para efetuar backup.');
       redirect(base_url());
   }


   $this->load->dbutil();
   $prefs = array(
    'format'      => 'zip',
    'filename'    => 'backup'.date('d-m-Y').'.sql'
);
   $backup =& $this->dbutil->backup($prefs);
   $this->load->helper('file');
   write_file(base_url().'backup/backup.zip', $backup);
   $this->load->helper('download');
   force_download('backup'.date('d-m-Y H:m:s').'.zip', $backup);
   $data['menuConfiguracoes'] = 'backup';
}
public function emitente(){   
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
       $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
       redirect(base_url());
   }
   $data['menuConfiguracoes'] = 'emitente';
   $data['dados'] = $this->sistemaos_model->getEmitente($this->session->userdata('id'));

   $data['view'] = 'sistemaos/emitente';
   $this->load->view('tema/topo',$data);
   $this->load->view('tema/rodape');
}
function do_upload($name = ''){
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
       $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
       redirect(base_url());
   }
   $this->load->library('upload');
   $image_upload_folder = FCPATH . 'assets/uploads';
   if (!file_exists($image_upload_folder)) {
    mkdir($image_upload_folder, DIR_WRITE_MODE, true);
}
$this->upload_config = array(
    'upload_path'   => $image_upload_folder,
    'allowed_types' => 'png|jpg|jpeg|bmp',
    'max_size'      => 2048,
    'remove_space'  => TRUE,
    'encrypt_name'  => TRUE,
);
$this->upload->initialize($this->upload_config);
if (!$this->upload->do_upload($name)) {
    $upload_error = $this->upload->display_errors();
    exit();
} else {
    $file_info = array($this->upload->data());
    return $file_info[0]['file_name'];
}
}
function in_upload($name = ''){
   $this->load->library('upload');
   $image_upload_folder = FCPATH . 'assets/uploads';
   if (!file_exists($image_upload_folder)) {
    mkdir($image_upload_folder, DIR_WRITE_MODE, true);
}
$this->upload_config = array(
    'upload_path'   => $image_upload_folder,
    'allowed_types' => 'png|jpg|jpeg|bmp',
    'max_size'      => 2048,
    'remove_space'  => TRUE,
    'encrypt_name'  => TRUE,
);
$this->upload->initialize($this->upload_config);
if (!$this->upload->do_upload($name)) {
    $upload_error = $this->upload->display_errors();
    exit();
} else {
    $file_info = array($this->upload->data());
    return $file_info[0]['file_name'];
}
}
public function cadastrarEmitente() {
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('index.php/sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
       $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
       redirect(base_url());
   }
   $this->load->library('form_validation');
   $this->form_validation->set_rules('nome','Razão Social','required|xss_clean|trim');
   $this->form_validation->set_rules('cnpj','CNPJ','required|xss_clean|trim');
   $this->form_validation->set_rules('ie','IE','required|xss_clean|trim');
   $this->form_validation->set_rules('logradouro','Logradouro','required|xss_clean|trim');
   $this->form_validation->set_rules('numero','Número','required|xss_clean|trim');
    
   $this->form_validation->set_rules('complemento','Complemento','xss_clean|trim');

   $this->form_validation->set_rules('bairro','Bairro','required|xss_clean|trim');
   $this->form_validation->set_rules('cidade','Cidade','required|xss_clean|trim');
   $this->form_validation->set_rules('uf','UF','required|xss_clean|trim');
   $this->form_validation->set_rules('telefone','Telefone','required|xss_clean|trim');
   $this->form_validation->set_rules('email','E-mail','required|xss_clean|trim');
   $this->form_validation->set_rules('hora_inicio','Início do expediente','required|xss_clean|trim');
   $this->form_validation->set_rules('hora_termino','Término do expediente','required|xss_clean|trim');
   $this->form_validation->set_rules('fuso_horario','Fuso Horário','required|xss_clean|trim');

   if ($this->form_validation->run() == false) {

    $this->session->set_flashdata('error','Campos obrigatórios não foram preenchidos.');
    redirect(base_url().'index.php/sistemaos/emitente');

} 
else {
    $nome = $this->input->post('nome');
    $cnpj = $this->input->post('cnpj');
    $ie = $this->input->post('ie');
    $logradouro = $this->input->post('logradouro');
    $numero = $this->input->post('numero');
    $complemento = $this->input->post('complemento');
    $bairro = $this->input->post('bairro');
    $cidade = $this->input->post('cidade');
    $uf = $this->input->post('uf');
    $telefone = $this->input->post('telefone');
    $email = $this->input->post('email');
    $hora_inicio = $this->input->post('hora_inicio');
    $hora_termino = $this->input->post('hora_termino');
    $fuso_horario = $this->input->post('fuso_horario');
    $image = $this->do_upload('userfile');
    $logo = base_url().'assets/uploads/'.$image;
    $retorno = $this->sistemaos_model->addEmitente($this->session->userdata('id'), $nome, $cnpj, $ie, $logradouro, $numero, $complemento, $bairro, $cidade, $uf,$telefone,$email, $logo, $hora_inicio, $hora_termino, $fuso_horario);
    if($retorno){
        $this->session->set_flashdata('success','As informações foram inseridas com sucesso.');
        redirect(base_url().'index.php/sistemaos/emitente');
    }
    else{
        $this->session->set_flashdata('error','Ocorreu um erro ao tentar inserir as informações.');
        redirect(base_url().'index.php/sistemaos/emitente');
    }

}
}
public function editarEmitente() {
    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('index.php/sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
       $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
       redirect(base_url());
   }
   $this->load->library('form_validation');
   $this->form_validation->set_rules('nome','Razão Social','required|xss_clean|trim');
   $this->form_validation->set_rules('cnpj','CNPJ','required|xss_clean|trim');
   $this->form_validation->set_rules('ie','IE','required|xss_clean|trim');
   $this->form_validation->set_rules('logradouro','Logradouro','required|xss_clean|trim');
   $this->form_validation->set_rules('numero','Número','required|xss_clean|trim');
   
   $this->form_validation->set_rules('complemento','Complemento','xss_clean|trim');
  
   $this->form_validation->set_rules('bairro','Bairro','required|xss_clean|trim');
   $this->form_validation->set_rules('cidade','Cidade','required|xss_clean|trim');
   $this->form_validation->set_rules('uf','UF','required|xss_clean|trim');
   $this->form_validation->set_rules('telefone','Telefone','required|xss_clean|trim');
   $this->form_validation->set_rules('email','E-mail','required|xss_clean|trim');
   $this->form_validation->set_rules('hora_inicio','Início do expediente','required|xss_clean|trim');
   $this->form_validation->set_rules('hora_termino','Término do expediente','required|xss_clean|trim');
   $this->form_validation->set_rules('fuso_horario','Fuso Horário','required|xss_clean|trim');

   if ($this->form_validation->run() == false) {

    $this->session->set_flashdata('error','Campos obrigatórios não foram preenchidos.');
    redirect(base_url().'index.php/sistemaos/emitente');

} 
else {
    $nome = $this->input->post('nome');
    $cnpj = $this->input->post('cnpj');
    $ie = $this->input->post('ie');
    $logradouro = $this->input->post('logradouro');
    $numero = $this->input->post('numero');
    $complemento = $this->input->post('complemento');
    $bairro = $this->input->post('bairro');
    $cidade = $this->input->post('cidade');
    $uf = $this->input->post('uf');
    $telefone = $this->input->post('telefone');
    $email = $this->input->post('email');
    $hora_inicio = $this->input->post('hora_inicio');
    $hora_termino = $this->input->post('hora_termino');
    $fuso_horario = $this->input->post('fuso_horario');
    $retorno = $this->sistemaos_model->editEmitente($this->session->userdata('id'), $nome, $cnpj, $ie, $logradouro, $numero, $complemento, $bairro, $cidade, $uf,$telefone,$email,$hora_inicio,$hora_termino,$fuso_horario);
    if($retorno){
        $this->session->set_flashdata('success','As informações foram alteradas com sucesso.');
        redirect(base_url().'index.php/sistemaos/emitente');
    }
    else{
        $this->session->set_flashdata('error','Ocorreu um erro ao tentar alterar as informações.');
        redirect(base_url().'index.php/sistemaos/emitente');
    }

}
}
public function editarLogo(){

    if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        redirect('index.php/sistemaos/login');
    }
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
       $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
       redirect(base_url());
   }
   $id = $this->input->post('id');
   if($id == null || !is_numeric($id)){
       $this->session->set_flashdata('error','Ocorreu um erro ao tentar alterar a logomarca.');
       redirect(base_url().'index.php/sistemaos/emitente'); 
   }
   $this->load->helper('file');
   $result = $this->sistemaos_model->getEmitente($id);
   if (count($result) > 0) {
    try {
        $emitente = $result[0];
                //die(FCPATH  . 'assets/uploads/' . basename($emitente->url_logo));
        @unlink(FCPATH  . 'assets/uploads/' . basename($emitente->url_logo));
    } catch (Exception $err) {
        error_log($err->getMessage());
    }
}
$image = $this->do_upload('userfile');
$logo = base_url().'assets/uploads/'.$image;
$retorno = $this->sistemaos_model->editLogo($this->session->userdata('id'), $logo);
if($retorno){
    $this->session->set_flashdata('success','As informações foram alteradas com sucesso.');
    redirect(base_url().'index.php/sistemaos/emitente');
}
else{
    $this->session->set_flashdata('error','Ocorreu um erro ao tentar alterar as informações.');
    redirect(base_url().'index.php/sistemaos/emitente');
}
}

}
