<?php

class Planos extends CI_Controller {
    

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */
    
    function __construct() {

        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cPlanos')){
          $this->session->set_flashdata('error','Você não tem permissão para configurar os planos.');
          redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('planos_model', '', TRUE);
		$this->load->model('marcas_model', '', TRUE);
        $this->data['menuPlanos'] = 'planos';
    }

    function index(){
		$this->gerenciar();
	}

    function gerenciar(){
        
    $countrows = $this->planos_model->count('planos');
        $this->data['results'] = $this->planos_model->get($countrows,$this->uri->segment(3));
       
    $this->data['view'] = 'planos/planos';
        $this->load->view('tema/topo',$this->data);

       
        
    }
	
    function adicionar(){  
          
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('planos') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="alert alert-danger">'.validation_errors().'</div>' : false);

        } else
        {

            $this->load->library('encrypt');                       
            $data = array(
                    'recorrencia'      => $this->input->post('recorrencia'),
                    'dia_recorrencia'  => $this->input->post('dia_recorrencia'),
                    'nome' => set_value('nome'),
                    'valor' => $this->input->post('valor'),
                    'dataCadastro' => date('Y-m-d')
            );
            
            $idPlano = $this->planos_model->add('planos',$data);
            
            if ($idPlano !== FALSE){                
                $this->session->set_flashdata('success','Plano cadastrada com sucesso!');
				redirect(base_url().'index.php/planos/adicionar/');
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';

			}
		}
        
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome');   
        $this->data['planos'] = $this->planos_model->listar();
        $this->data['menuPlanos'] = 'adicionar';
	    $this->data['view'] = 'planos/adicionarPlano';
        $this->load->view('tema/topo',$this->data);
       
    }	
	
    function editar(){ 
        
        if(!$this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('sistemaos');
        }

        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
        $this->form_validation->set_rules('nome',  'Nome',  'trim|required|xss_clean');
        $this->form_validation->set_rules('valor', 'Valor', 'trim|required|xss_clean');
        $this->form_validation->set_rules('recorrencia', 'Período de recorrência', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dia_recorrencia', 'Dia de recorrência', 'trim|required|xss_clean');

        if ($this->form_validation->run('planos') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        } else
        {

            $data = array(
                    'recorrencia'      => $this->input->post('recorrencia'),
                    'dia_recorrencia'  => $this->input->post('dia_recorrencia'),
                    'nome'            => $this->input->post('nome'),
                    'valor'              => $this->input->post('valor')
            );

           
			if ($this->planos_model->edit('planos',$data,'idPlanos',$this->input->post('idPlanos')) == TRUE)
			{
                $this->session->set_flashdata('success','Plano editada com sucesso!');
				redirect(base_url().'index.php/planos/editar/'.$this->input->post('idPlanos'));
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';

			}
		}

		$this->data['result'] = $this->planos_model->getById($this->uri->segment(3));
        $this->load->model('permissoes_model');
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes','permissoes.idPermissao,permissoes.nome'); 

	    $this->data['view'] = 'planos/editarPlano';
        $this->load->view('tema/topo',$this->data);

    }
	
    public function excluir(){

            $ID =  $this->input->post('id');
            $this->planos_model->delete('planos','idPlanos',$ID);             
            redirect(base_url().'index.php/planos/gerenciar/');
    }
}

