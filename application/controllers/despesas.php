<?php
class Despesas extends CI_Controller {

    /**
     * author: Ramon Silva 
     * email: silva018-mg@yahoo.com.br
     * 
     */

    private $idFranquia = '';
    
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('sistemaos/login');
        }
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('despesas_model', '', TRUE);
        $this->data['menuFinanceiro'] = 'despesas';

        $this->idFranquia=$this->session->userdata('id');
    }
    function index(){
     $this->gerenciar();
 }
 function gerenciar(){

   if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vDespesa')){
    $this->session->set_flashdata('error','Você não tem permissão para visualizar despesas.');
     redirect(base_url());
 }
 $countrows = $this->despesas_model->count('despesas', $this->session->userdata('id'));        
 $this->data['results'] = $this->despesas_model->get('despesas',"idDespesas,codigo,nome,valor,data_vencimento,data_emissao,data_pagamento,tipo,status",'',$countrows,$this->uri->segment(3));

 $this->data['view'] = 'despesas/despesas';
 $this->load->view('tema/topo',$this->data);      

}

function adicionar() {
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aDespesa')){
        $this->session->set_flashdata('error','Você não tem permissão para adicionar despesas.');
     redirect(base_url());
 }
 $this->load->library('form_validation');
 $this->data['custom_error'] = '';
 if ($this->form_validation->run('despesas') == false) {
    $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
} else {
    $data = array(
        'idFranquia'      => (int)$this->idFranquia,
        'tipo'            => $this->input->post('tipo'),
        'status'            => $this->input->post('status'),
        'nome'            => $this->input->post('nome'),
        'data_vencimento' => date('Y-m-d', strtotime($this->input->post('data_vencimento'))),
        'valor'           => $this->input->post('valor'),
        'forma_pagamento' => $this->input->post('forma_pagamento'),
        'codigo'          => $this->input->post('codigo'),
        'fornecedor'   => (int)$this->input->post('fornecedor_id'),
        'data_emissao'    => date('Y-m-d', strtotime($this->input->post('data_emissao'))),
        'data_pagamento'  => date('Y-m-d', strtotime($this->input->post('data_pagamento'))),
        'valor_pago'      => $this->input->post('valor_pago'),
        'juros'           => $this->input->post('juros'),
        'desconto'        => $this->input->post('desconto'),
        'obs'             => $this->input->post('obs')
    );

    if ($this->despesas_model->add('despesas', $data) == TRUE) {
        $this->session->set_flashdata('success','Despesa adicionada com sucesso!');
        redirect(base_url() . 'index.php/despesas/adicionar/');
    } else {
        $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
    }
}
$this->data['view'] = 'despesas/adicionarDespesa';
$this->load->view('tema/topo', $this->data);

}

function editar() {
    $id = (int)$this->uri->segment(3);
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eDespesa')){
        $this->session->set_flashdata('error','Você não tem permissão para editar despesas.');
     redirect(base_url());
 }
 $this->load->library('form_validation');
 $this->data['custom_error'] = '';
 if ($this->form_validation->run('despesas') == false && empty($_POST['modal_ajax'])) {
    $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
} else {
    $data = array(
        'idFranquia'      => (int)$this->idFranquia,
        'tipo'            => $this->input->post('tipo'),
        'status'          => $this->input->post('status'),
        'nome'            => $this->input->post('nome'),
        'data_vencimento' => date('Y-m-d', strtotime($this->input->post('data_vencimento'))),
        'valor'           => $this->input->post('valor'),
        'forma_pagamento' => $this->input->post('forma_pagamento'),
        'codigo'          => $this->input->post('codigo'),
        'fornecedor'      => (int)$this->input->post('fornecedor_id'),
        'data_emissao'    => date('Y-m-d', strtotime($this->input->post('data_emissao'))),
        'data_pagamento'  => date('Y-m-d', strtotime($this->input->post('data_pagamento'))),
        'valor_pago'      => $this->input->post('valor_pago'),
        'juros'           => $this->input->post('juros'),
        'desconto'        => $this->input->post('desconto'),
        'obs'             => $this->input->post('obs')
    );
    if ($this->despesas_model->edit('despesas', $data, 'idDespesas', $id) == TRUE) {
        if(!empty($_POST['modal_ajax'])){
            $json = array("result" => true);
            echo json_encode($json);
            exit;
        }else{
            $this->session->set_flashdata('success','Despesa editada com sucesso!');
            redirect(base_url() . 'index.php/despesas/editar/'.$id);
        }
    } else {
        if(!empty($_POST['modal_ajax'])){
            $json = array("result" => false);
            echo json_encode($json);
            exit;
        }else{
            $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
        }
    }
}
$this->data['result'] = $this->despesas_model->getById($id);
$this->data['view'] = 'despesas/editarDespesa';
$this->load->view('tema/topo', $this->data);

}



function excluir(){
    if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dDespesa')){
        $this->session->set_flashdata('error','Você não tem permissão para excluir despesas.');
     redirect(base_url());
 }

 $id =  $this->input->post('id');
 if ($id == null){
    $this->session->set_flashdata('error','Erro ao tentar excluir despesa.');            
    redirect(base_url().'index.php/despesas/gerenciar/');
}
$this->db->where('despesas_id', $id);
$this->db->delete('despesas_os');
$this->db->where('despesas_id', $id);
$this->db->delete('itens_de_vendas');

$this->despesas_model->delete('despesas','idDespesas',$id);             

$this->session->set_flashdata('success','Despesa excluido com sucesso!');            
redirect(base_url().'index.php/despesas/gerenciar/');
}
}
