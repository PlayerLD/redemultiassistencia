<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'plugins/bcash/AutoLoader.php';
use Bcash\Domain\PaymentMethodEnum;
use Bcash\Domain\Address;
use Bcash\Domain\CreditCard;
use Bcash\Domain\Customer;
use Bcash\Domain\Product;
use Bcash\Domain\TransactionRequest;
use Bcash\Domain\DependentTransaction;
use Bcash\Service\Payment;
use Bcash\Service\Notification;
use Bcash\Service\Cancellation;
use Bcash\Service\Consultation;
use Bcash\Service\Installments;
use Bcash\Domain\NotificationContent;
use Bcash\Domain\NotificationStatusEnum;

class Bcash {

	public $email = '';
	public $consumer_key = '';
	public $token = '';
	public $payment_method = '';
	public $sandbox = '';
    
    function __construct($params = array()) {

    	Bcash\AutoLoader::register();
        if (count($params) > 0) {

            $this->initialize($params);
        }
        log_message('debug', "Bcash Class Initialized");
    }
    
    function initialize($params = array()) {

        if (count($params) > 0) {

            foreach ($params as $key => $val) {

                if (isset($this->$key)) {

                    $this->$key = $val;
                }
            }
        }
    }
	
	function payment() {

		$payment = new Payment($this->consumer_key);
		$payment->enableSandBox($this->sandbox);
		return $payment;
	}
	
	function address() {

		return new Address();
	}
	
	function customer() {

		return new Customer();
	}
	
	function product() {

		return new Product();
	}
	
	function transaction() {

		$transaction = new TransactionRequest();
		$transaction->setSellerMail($this->email);
		return $transaction;
	}
	
	function dependent_transaction() {

		return new DependentTransaction();
	}
	
	function credit_card() {

		return new CreditCard();
	}
	
	function notification_content($transaction_id, $order_id, $status_id) {

		return new NotificationContent($transaction_id, $order_id, $status_id);
	}
	
	function notification($token, $notification_content) {

		$notification = new Notification($this->email, $token, $notification_content);
		$notification->enableSandBox($this->sandbox);
		return $notification;
	}
	
	function get_payment_method($method) {

		switch ($method) {

			case 'VISA': $code = PaymentMethodEnum::VISA; break;
			case 'MASTERCARD': $code = PaymentMethodEnum::MASTERCARD; break;
			case 'AMERICAN_EXPRESS': $code = PaymentMethodEnum::AMERICAN_EXPRESS; break;
			case 'AURA': $code = PaymentMethodEnum::AURA; break;
			case 'DINERS': $code = PaymentMethodEnum::DINERS; break;
			case 'HIPERCARD': $code = PaymentMethodEnum::HIPERCARD; break;
			case 'ELO': $code = PaymentMethodEnum::ELO; break;
			default: $code = false; break;
		}
		return $code;
	}

	function getCreditCardType($str)
    {

        $matchingPatterns = [
            'visa' =>  '/^4[0-9]{12}(?:[0-9]{3})?$/',
            'mastercard' =>  '/^5[1-5][0-9]{14}$/',
            'amex' =>  '/^3[47][0-9]{13}$/',
            'diners' =>  '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
            'discover' =>  '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
            'elo' =>  '/^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/',
            'jcb' =>  '/^(?:2131|1800|35\d{3})\d{11}$/',
            'hipercard' =>  '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
            'aura' =>  '/^50[0-9]{17}$/'
        ];

        $type = false;
        if(preg_match($matchingPatterns['visa'], $str))
            $type = 'VISA';
        elseif(preg_match($matchingPatterns['mastercard'], $str))
            $type = 'MASTERCARD';
        elseif(preg_match($matchingPatterns['amex'], $str))
            $type = 'AMERICAN_EXPRESS';
        elseif(preg_match($matchingPatterns['diners'], $str))
            $type = 'DINERS';
        elseif(preg_match($matchingPatterns['elo'], $str))
            $type = 'ELO';
        elseif(preg_match($matchingPatterns['hipercard'], $str))
            $type = 'HIPERCARD';
        elseif(preg_match($matchingPatterns['aura'], $str))
            $type = 'AURA';
        elseif(preg_match($matchingPatterns['discover'], $str))
            $type = 'DISCOVER';
        elseif(preg_match($matchingPatterns['jcb'], $str))
            $type = 'JCB';

            return $type;
    }
	
	function consultation() {

		$consultation = new Consultation($this->email, $this->consumer_key);
		$consultation->enableSandBox($this->sandbox);
		return $consultation;
	}
	
	function cancellation() {

		$cancellation = new Cancellation($this->email, $this->consumer_key);
		$cancellation->enableSandBox($this->sandbox);
		return $cancellation;
	}
	
	function installments() {

		$installments = new Installments($this->email, $this->token);
		$installments->enableSandBox($this->sandbox);
		return $installments;
	}
}