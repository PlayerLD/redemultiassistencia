<?php
require("funcoes-banco.php");
ini_set('max_execution_time',0);
if(!empty($_GET["idVenda"])){

	$idVenda = $_GET["idVenda"];
	$idFranquia = $_GET["idFranquia"];
	$dadosVenda = selecionar("SELECT * FROM vendas WHERE idVendas = ".$idVenda);
	$dadosCliente = selecionar("SELECT * FROM clientes WHERE idClientes = ".$dadosVenda[0]["clientes_id"]);
  
	$dadosConfiguracaoNfe = selecionar("SELECT * FROM configuracao_nota_fiscal WHERE idFranquia = ".$idFranquia);
	if(!empty($_GET['cancelarNF']) && !empty($dadosConfiguracaoNfe)){
		$urlIntegracao      = 'http://www.agilcontabil.net/sistemaInstalado/ajax';
		$dados['usuario'] = $dadosConfiguracaoNfe[0]["nomeAgil"];
		$dados['senha']   = $dadosConfiguracaoNfe[0]["senhaAgil"];
		$dados['acao']      = 'cancelarNfeA3';
		  //enviar código de segurança do contribuinte quando for NFC-e
		  //$dados['idCsc'] = '000001';
		  //$dados['csc'] = 'FFABDA2E-1A3E-48B7-A964-A9D6782AD664';
		$dados["ambiente"]  = '1'; //1 = produção, 2 = homologação 
		$dados["modelo"]    = '55'; //55 = Nfe, 65 = Nfce
		$dados["uf"]        = $dadosConfiguracaoNfe[0]["estado"]; //Sigla do estado do seu cliente (emitente da Nfe)
		$dados["chave"]     = $dadosVenda[0]["chaveNfe"]; //chave da Nfe a ser cancelada
		$dados["protocolo"] = $dadosVenda[0]["protocolo"]; //número de protocolo da Nfe a ser cancelada
		$dados["justificativa"] = $_GET['cancelarNF'];
    //Inicia comunicação com servidor agilcontabil.net
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $urlIntegracao);
    curl_setopt($ch,CURLOPT_POST, 1);
    curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($dados));
    //recebe a resposta
    $resposta = curl_exec($ch);
    //finaliza comunicação
    curl_close($ch);
		$arrayResposta = json_decode($resposta,true); //transforma resposta json em array do PHP
		//mostra a resposta do cancelamento da nota
		print_r($arrayResposta);
	}
	elseif($dadosVenda[0]["chaveNfe"] != ""){
	 //já foi emitida
		//mostra pdf
		?>
	    <script>
			url = "../assets/arquivos/danfe/nfe<?php echo $dadosVenda[0]["chaveNfe"];?>.pdf";
			window.open(url, "_blank");
			window.location = "/sistema/index.php/vendas/visualizar/<?php echo $idVenda;?>";
		</script>
		<?php		
	}
	else{
	 //ainda não foi emitida
		//Emitente
			if($dadosConfiguracaoNfe[0]["producao"]=="1"){
			 //produção
				$numeroNfe = $dadosConfiguracaoNfe[0]["numeroNfeProducao"]+1;
				$ambienteNfe = 1;
			}
			else{
			 //homologação
				$numeroNfe = $dadosConfiguracaoNfe[0]["numeroNfeHomologacao"]+1;
				//$numeroNfe = 149;
				$ambienteNfe = 2;
			}

		
			$numeroNf = $numeroNfe;
			$ambiente = $ambienteNfe;
			$cUF = ufToCodigo($dadosConfiguracaoNfe[0]["estado"]);
			$cnpj = soNumeros($dadosConfiguracaoNfe[0]["cnpj"]);
			$aamm = soNumeros(date('y-m'));
			$mod = $dadosConfiguracaoNfe[0]["tipo"]; //NFe = 55
			$serie = $dadosConfiguracaoNfe[0]["serieNfe"];
			$tpEmis='1';//normal = 1
			$num = str_pad($numeroNf, 9, '0',STR_PAD_LEFT);
			$cn = zeroEsquerda($num+0,8);
			$chave = "$cUF$aamm$cnpj$mod$serie$num$tpEmis$cn"; //2345234523452345234532452435234
			$dv = calculaDV($chave);
			$chave .= $dv;
			$datanfe = date('Y-m-d');
			$horanfe = date('H:i:sP');
			$formatadata = $datanfe.'T'.$horanfe;
			$codmunicipio = buscaCodigoMunicipio(nfce($dadosConfiguracaoNfe[0]["cidade"]),$dadosConfiguracaoNfe[0]["estado"]);
			$razao =  nfce($dadosConfiguracaoNfe[0]["razaoSocial"]);
			$endereco =  nfce($dadosConfiguracaoNfe[0]["logradouro"]);
			$numero =  nfce($dadosConfiguracaoNfe[0]["numero"]);
			$complemento = nfce($dadosConfiguracaoNfe[0]["complemento"]);
			$bairro =  nfce($dadosConfiguracaoNfe[0]["bairro"]);
			$cidade =  ($dadosConfiguracaoNfe[0]["cidade"]);
			$estado =  nfce(($dadosConfiguracaoNfe[0]["estado"]));
			$cep = soNumeros($dadosConfiguracaoNfe[0]["cep"]);
			$fone = soNumeros(($dadosConfiguracaoNfe[0]["telefone"]));
			$ie = soNumeros($dadosConfiguracaoNfe[0]["inscricaoEstadual"]);
			$crtEmitente = soNumeros($dadosConfiguracaoNfe[0]["crt"]);
		
			//Cliente
			$cpfcliente = soNumeros($dadosCliente[0]["documento"]);	
			$nomecliente =  nfce($dadosCliente[0]["nomeCliente"]);	
			$enderecocliente =  nfce($dadosCliente[0]["rua"]);
			$numerocliente =  nfce($dadosCliente[0]["numero"]);
			$complementocliente = nfce($dadosCliente[0]["complemento"]);
			$bairrocliente =  nfce($dadosCliente[0]["bairro"]);
			$cidadecliente = nfce($dadosCliente[0]["cidade"]);
			$estadocliente = estadoToUf($dadosCliente[0]["estado"]);
			$codigoMunicipioCliente = buscaCodigoMunicipio($cidadecliente,$estadocliente);
			$cepcliente = soNumeros($dadosCliente[0]["cep"]);
			$fonecliente = soNumeros($dadosCliente[0]["telefone"]);
			//$ieCliente = soNumeros($dadosCliente[0]["inscricaoEstadual"]);
			$ieCliente = soNumeros($dadosCliente[0]["rg"]);
			$indicacaoIeCliente = !empty($ieCliente) ? "1" : "9"; //1-tem inscrição | 9 - não tem inscrição
		  $indFinal = '0'; //cnpj
		  if(strlen($cpfcliente)<12){ //se for cpf
				$indFinal = '1'; //consumidor final
			  $ieCliente = '';//soNumeros($dadosCliente[0]["inscricaoEstadual"]);
			  $indicacaoIeCliente = "9"; //1-tem inscrição | 9 - não tem inscrição
			}

			$idDest = "1"; //venda dentro estado
		  if($estadocliente!=$dadosConfiguracaoNfe[0]["estado"]){
				$idDest = "2";
			}		
		
			// Escrevendo XML
			$nomeArquivo = "../assets/arquivos/xml/nfe$chave.xml";
			$ponteiro = fopen($nomeArquivo, "w");
		
			fwrite($ponteiro, "<?xml version='1.0' encoding='utf-8'?>");
			fwrite($ponteiro, '<NFe xmlns="http://www.portalfiscal.inf.br/nfe">');
			fwrite($ponteiro, '<infNFe Id="NFe'.$chave.'" versao="3.10">');
			fwrite($ponteiro, "<ide>");
			fwrite($ponteiro, "<cUF>".$cUF."</cUF>");
			fwrite($ponteiro, "<cNF>$cn</cNF>");
			fwrite($ponteiro, "<natOp>VENDA DE MERCADORIA</natOp>");
			fwrite($ponteiro, "<indPag>1</indPag>"); // 1 = a vista
			fwrite($ponteiro, "<mod>".$mod."</mod>");
			fwrite($ponteiro, "<serie>".($serie+0)."</serie>");
			fwrite($ponteiro, "<nNF>".($num+0)."</nNF>");
			fwrite($ponteiro, "<dhEmi>$formatadata</dhEmi>");
			fwrite($ponteiro, "<tpNF>1</tpNF>"); //normal = 1
			fwrite($ponteiro, "<idDest>".$idDest."</idDest>"); 
			fwrite($ponteiro, "<cMunFG>$codmunicipio</cMunFG>");
			fwrite($ponteiro, "<tpImp>1</tpImp>"); //vertical
			fwrite($ponteiro, "<tpEmis>1</tpEmis>"); // normal = 1
			fwrite($ponteiro, "<cDV>".$dv."</cDV>");
			fwrite($ponteiro, "<tpAmb>$ambiente</tpAmb>");
			fwrite($ponteiro, "<finNFe>1</finNFe>");
			fwrite($ponteiro, "<indFinal>".$indFinal."</indFinal>"); //1 = consumidor final | 0 = nao final
			fwrite($ponteiro, "<indPres>1</indPres>"); // 1 = venda presencial
			fwrite($ponteiro, "<procEmi>0</procEmi>");
			fwrite($ponteiro, "<verProc>V2.245</verProc>");
			fwrite($ponteiro, "</ide>");
		
			fwrite($ponteiro, "<emit>");
			fwrite($ponteiro, "<CNPJ>$cnpj</CNPJ>");
			fwrite($ponteiro, "<xNome>$razao</xNome>");
			fwrite($ponteiro, "<enderEmit>");
			fwrite($ponteiro, "<xLgr>$endereco</xLgr>");
			fwrite($ponteiro, "<nro>$numero</nro>");
			if(!empty($complemento)) fwrite($ponteiro, "<xCpl>$complemento</xCpl>");
			fwrite($ponteiro, "<xBairro>$bairro</xBairro>");
			fwrite($ponteiro, "<cMun>$codmunicipio</cMun>");
			fwrite($ponteiro, "<xMun>$cidade</xMun>");
			fwrite($ponteiro, "<UF>$estado</UF>");
			fwrite($ponteiro, "<CEP>$cep</CEP>");
			fwrite($ponteiro, "<cPais>1058</cPais>");
			fwrite($ponteiro, "<xPais>Brasil</xPais>");
			if(!empty($fone)) fwrite($ponteiro, "<fone>$fone</fone>");
			fwrite($ponteiro, "</enderEmit>");
			fwrite($ponteiro, "<IE>$ie</IE>");
			fwrite($ponteiro, "<CRT>$crtEmitente</CRT>");
			fwrite($ponteiro, "</emit>");
		
		
			fwrite($ponteiro, "<dest>");
			if(strlen($cpfcliente)==14) fwrite($ponteiro, "<CNPJ>$cpfcliente</CNPJ>");
			if(strlen($cpfcliente)==11) fwrite($ponteiro, "<CPF>$cpfcliente</CPF>");
			fwrite($ponteiro, "<xNome>$nomecliente</xNome>");
			fwrite($ponteiro, "<enderDest>");
			fwrite($ponteiro, "<xLgr>$enderecocliente</xLgr>");
			fwrite($ponteiro, "<nro>$numerocliente</nro>");
			if(!empty($complementocliente)) fwrite($ponteiro, "<xCpl>$complementocliente</xCpl>");
			fwrite($ponteiro, "<xBairro>$bairrocliente</xBairro>");
			fwrite($ponteiro, "<cMun>$codigoMunicipioCliente</cMun>");
			fwrite($ponteiro, "<xMun>$cidadecliente</xMun>");
			fwrite($ponteiro, "<UF>$estadocliente</UF>");
			fwrite($ponteiro, "<CEP>$cepcliente</CEP>");
			fwrite($ponteiro, "<cPais>1058</cPais>");
			fwrite($ponteiro, "<xPais>BRASIL</xPais>");
			if(!empty($fonecliente)) fwrite($ponteiro, "<fone>$fonecliente</fone>");
			fwrite($ponteiro, "</enderDest>");
			fwrite($ponteiro, "<indIEDest>$indicacaoIeCliente</indIEDest>");
			if(!empty($ieCliente)) fwrite($ponteiro, "<IE>$ieCliente</IE>");
			fwrite($ponteiro, "</dest>");
		
		  if(!empty($dadosConfiguracaoNfe[0]["cnpjContador"])){
        fwrite($ponteiro, "<autXML>");
        fwrite($ponteiro, "<CNPJ>".$dadosConfiguracaoNfe[0]["cnpjContador"]."</CNPJ>");
        fwrite($ponteiro, "</autXML>");
      }
    
			$item = 0; //numero do item
			$soma_total = 0;
			
			$itensVenda = selecionar("SELECT * FROM itens_de_vendas WHERE vendas_id = ".$idVenda);
			foreach($itensVenda as $itemVenda){

				$produto = selecionar("SELECT * FROM produtos WHERE idProdutos = ".$itemVenda["produtos_id"]);
				$item++;
		
				$ean = '';// !empty($produto[0]["codigo_barras"]) ? soNumeros($produto[0]["codigo_barras"]) : ""; 
				$nomeproduto =  (nfce($produto[0]["descricao"]));
				$ncm = soNumeros($produto[0]["ncm"]); //NCM
				$cfop = soNumeros($produto[0]["cfop"]);  //CFOP
				$uTrib = nfce("UN");  //UN
				$quantity = float4($itemVenda["quantidade"]);
				$preco = float4(($itemVenda["subTotal"]/$itemVenda["quantidade"]));
				$subtotal = float2($itemVenda["subTotal"]);
				$detalhesproduto = "";//strip_tags($prds['product_details']);
		
				$soma_total = $subtotal + $soma_total;
		
				fwrite($ponteiro, '<det nItem="'.$item.'">');
				fwrite($ponteiro, "<prod>");
				fwrite($ponteiro, "<cProd>".$produto[0]["idProdutos"]."</cProd>");
				if(!empty($ean)) fwrite($ponteiro, "<cEAN>$ean</cEAN>");
				fwrite($ponteiro, "<xProd>$nomeproduto</xProd>");
				fwrite($ponteiro, "<NCM>$ncm</NCM>");
				fwrite($ponteiro, "<CFOP>$cfop</CFOP>");
				fwrite($ponteiro, "<uCom>$uTrib</uCom>");
				fwrite($ponteiro, "<qCom>$quantity</qCom>");
				fwrite($ponteiro, "<vUnCom>$preco</vUnCom>");
				fwrite($ponteiro, "<vProd>$subtotal</vProd>");
				fwrite($ponteiro, "<cEANTrib/>");
				fwrite($ponteiro, "<uTrib>$uTrib</uTrib>");
				fwrite($ponteiro, "<qTrib>$quantity</qTrib>");
				fwrite($ponteiro, "<vUnTrib>$preco</vUnTrib>");
				fwrite($ponteiro, "<indTot>1</indTot>");
				fwrite($ponteiro, "</prod>");
		
				fwrite($ponteiro, "<imposto>");
				fwrite($ponteiro, "<vTotTrib>0</vTotTrib>");
				fwrite($ponteiro, "<ICMS>");
				fwrite($ponteiro, "<ICMSSN102>");
				fwrite($ponteiro, "<orig>1</orig>");
				fwrite($ponteiro, "<CSOSN>102</CSOSN>");
				fwrite($ponteiro, "</ICMSSN102>");
				fwrite($ponteiro, "</ICMS>");
				fwrite($ponteiro, "<PIS>");
				fwrite($ponteiro, "<PISOutr>");
				fwrite($ponteiro, "<CST>49</CST>");
				fwrite($ponteiro, "<vBC>0.00</vBC>");
				fwrite($ponteiro, "<pPIS>0.00</pPIS>");
				fwrite($ponteiro, "<vPIS>0.00</vPIS>");
				fwrite($ponteiro, "</PISOutr>");
				fwrite($ponteiro, "</PIS>");
				fwrite($ponteiro, "<COFINS>");
				fwrite($ponteiro, "<COFINSOutr>");
				fwrite($ponteiro, "<CST>49</CST>");
				fwrite($ponteiro, "<vBC>0.00</vBC>");
				fwrite($ponteiro, "<pCOFINS>0.00</pCOFINS>");
				fwrite($ponteiro, "<vCOFINS>0.00</vCOFINS>");
				fwrite($ponteiro, "</COFINSOutr>");
				fwrite($ponteiro, "</COFINS>");
				fwrite($ponteiro, "</imposto>");
		
				if(!empty($detalhesproduto)) fwrite($ponteiro, "<infAdProd>$detalhesproduto</infAdProd>");
				fwrite($ponteiro, "</det>");
			}
			//produtos
		
			fwrite($ponteiro, "<total>");
			fwrite($ponteiro, "<ICMSTot>");
			fwrite($ponteiro, "<vBC>0.00</vBC>"); //0
			fwrite($ponteiro, "<vICMS>0.00</vICMS>");
			fwrite($ponteiro, "<vICMSDeson>0.00</vICMSDeson>");
			fwrite($ponteiro, "<vBCST>0.00</vBCST>");
			fwrite($ponteiro, "<vST>0.00</vST>");
			fwrite($ponteiro, "<vProd>$soma_total</vProd>");
			fwrite($ponteiro, "<vFrete>0.00</vFrete>");
			fwrite($ponteiro, "<vSeg>0.00</vSeg>");
			fwrite($ponteiro, "<vDesc>0.00</vDesc>");
			fwrite($ponteiro, "<vII>0.00</vII>");
			fwrite($ponteiro, "<vIPI>0.00</vIPI>");
			fwrite($ponteiro, "<vPIS>0.00</vPIS>");
			fwrite($ponteiro, "<vCOFINS>0.00</vCOFINS>");
			fwrite($ponteiro, "<vOutro>0.00</vOutro>");
			fwrite($ponteiro, "<vNF>$soma_total</vNF>");
			fwrite($ponteiro, "</ICMSTot>");
			fwrite($ponteiro, "</total>");
		
			fwrite($ponteiro, "<transp>");
			fwrite($ponteiro, "<modFrete>9</modFrete>"); //sem frete
			fwrite($ponteiro, "</transp>");
		
		
			$nota = "";
			$adFisco = "";
			fwrite($ponteiro, "<infAdic>");
			if(!empty($nota)) fwrite($ponteiro, "<infCpl>$nota</infCpl>");
			if(!empty($adFisco)) fwrite($ponteiro, "<infAdFisco>$adFisco</infAdFisco>");
			fwrite($ponteiro, "</infAdic>");
		
			/*
			fwrite($ponteiro, "<pag>");
			fwrite($ponteiro, "<tPag>$formapgto</tPag>");
			fwrite($ponteiro, "<vPag>$totalvendas</vPag>");
			fwrite($ponteiro, "</pag>");
			*/
			// Fecha XML
			fwrite($ponteiro, "</infNFe>");
			fwrite($ponteiro, "</NFe>");
		
			fclose($ponteiro); //salva o xml no arquivo
		
			//Código de envio do xml:
			$urlIntegracao    = 'http://www.agilcontabil.net/sistemaInstalado/ajax';
			$dados['usuario'] = $dadosConfiguracaoNfe[0]["nomeAgil"];
			$dados['senha']   = $dadosConfiguracaoNfe[0]["senhaAgil"];
			$dados['acao']    = 'emitirNfeA3';
				//enviar código de segurança do contribuinte quando for NFC-e
				//$dados['idCsc'] = '000001';
				//$dados['csc'] = 'FFABDA2E-1A3E-48B7-A964-A9D6782AD664';
			$dados['xml']     = base64_encode(file_get_contents($nomeArquivo)); //o xml deve ser enviado em formato base64
		
	?>
	<textarea cols="150" rows="20"><?php echo file_get_contents($nomeArquivo);?></textarea><br><br>
	<?php
				//Inicia comunicação com servidor agilcontabil.net
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $urlIntegracao);
				curl_setopt($ch,CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($dados));
				//recebe a resposta
				$resposta = curl_exec($ch);
				//finaliza comunicação
				curl_close($ch);
			$arrayResposta = json_decode($resposta,true); //transforma resposta json em array do PHP
			//mostra a resposta da emissão da nota
			//o xml da nota fiscal emitida está dentro da variavel $resposta["xml"] e deve ser gravado em sua base de dados
			//o pdf da nota fiscal emitida está dentro da variavel $resposta["pdf"] em formato texto hexadecimal
			if(@$arrayResposta["cStat"]==100){
			 //sucesso
				//grava xml de resposta na pasta nfe/aprovadas
				file_put_contents("../assets/arquivos/xml/nfe".$chave.".xml",base64_decode($arrayResposta["xml"]));
				//grava pdf
				$pdf = hex2bin($arrayResposta["pdf"]);
				file_put_contents("../assets/arquivos/danfe/nfe".$chave.".pdf",$pdf);
		
				//grava a chave da nfe na venda
				$campos = array("chaveNfe", "protocolo");
				$valores = array($chave, @$arrayResposta['protocolo']);
				$result = alterar("vendas",$campos,$valores,"idVendas = ".$idVenda);
		
				//altera número da ultima nota emitida na empresa
				if($dadosConfiguracaoNfe[0]["producao"]=="1"){

					$campos = array("numeroNfeProducao");
				}
				else{

					$campos = array("numeroNfeHomologacao");
				}

				$valores = array($numeroNf);
				$result = alterar("configuracao_nota_fiscal", $campos, $valores, "idFranquia = ".$idFranquia);
				//die();
				?>
				<script>
					url = "../assets/arquivos/danfe/nfe<?php echo $chave;?>.pdf";
					window.open(url, "_blank");
					window.location = "/sistema/index.php/vendas/visualizar/<?php echo $idVenda;?>";
				</script>
				<?php	
			}
			else{
			 //erro na emissão (mostra o erro)
				echo "Erro: ".(@$arrayResposta["cStat"])." - ".(@$arrayResposta["xMotivo"]);
			}

	/*	}
	else{

			?>
			<script>
				window.location = "/sistema/index.php/vendas/visualizar/<?php echo $idVenda;?>?nfe-sempermissao=true";
			</script>
	        <?php
		}

	*/
	}
	//else (ainda não foi emitida)
}
elseif(!empty($_GET["idOs"])){

	$idVenda = $_GET["idOs"];
	$idFranquia = $_GET["idFranquia"];
	$dadosOs = selecionar("SELECT * FROM os WHERE idOs = ".$_GET["idOs"]);
	$dadosCliente = selecionar("SELECT * FROM clientes WHERE idClientes = ".$dadosOs[0]["clientes_id"]);
	$dadosServicoOs = selecionar("SELECT * FROM servicos_os WHERE idServicos_os = ".$_GET["idServicos"]);
	$dadosConfiguracaoNfe = selecionar("SELECT * FROM configuracao_nota_fiscal WHERE idFranquia = ".$idFranquia);

	//var_dump("SELECT * FROM servicos WHERE idServicos = ".$_GET["idServicos"]);
	//die();
	if(empty($dadosConfiguracaoNfe)){
		echo "<h1>Dados de configura&#231;&#227;o n&#227;o encontrados, entre em contato com o administrador.</h1>";
		exit;
	}
	
	if($dadosConfiguracaoNfe[0]["producao"]=="1"){
	 //produção
		$numeroNfe = $dadosConfiguracaoNfe[0]["numeroNFSEProducao"]+1;
		$ambienteNfe = 1;
	}
	else{
	 //homologação
		$numeroNfe = $dadosConfiguracaoNfe[0]["numeroNFSEHomologacao"]+1;
		//$numeroNfe = 149;
		$ambienteNfe = 2;
	}
	
	//verifica se já foi emitida
	if(!empty($dadosOs[0]["numeroNfse"]))
	{
		$pdf = file_get_contents("../assets/arquivos/nfse/danfe/nfse".$dadosOs[0]["numeroNfse"].".pdf");
		?>
		<object data="data:application/pdf;base64,<?php echo base64_encode($pdf); ?>" type="application/pdf" width="100%" height="800px"></object>
		<?php
	}else{ //emite

		$numeroNf = $numeroNfe;
		$ambiente = $ambienteNfe;
		$cUF = ufToCodigo($dadosConfiguracaoNfe[0]["estado"]);
		$cnpj = soNumeros($dadosConfiguracaoNfe[0]["cnpj"]);
		$inscricaoMunicipal = soNumeros($dadosConfiguracaoNfe[0]["inscricaoMunicipal"]);
		$aamm = soNumeros(date('y-m'));
		$mod = $dadosConfiguracaoNfe[0]["tipo"]; //NFe = 55
		$serie = $dadosConfiguracaoNfe[0]["serieNfe"];
		$tpEmis='1';//normal = 1
		$num = str_pad($numeroNf, 9, '0',STR_PAD_LEFT);
		$cn = zeroEsquerda($num+0,8);
		$chave = "$cUF$aamm$cnpj$mod$serie$num$tpEmis$cn"; //2345234523452345234532452435234
		$dv = calculaDV($chave);
		$chave .= $dv;
		$datanfe = date('Y-m-d');
		$horanfe = date('H:i:sP');
		$formatadata = $datanfe.'T'.$horanfe;
		$codmunicipio = buscaCodigoMunicipio(nfce($dadosConfiguracaoNfe[0]["cidade"]),$dadosConfiguracaoNfe[0]["estado"]);
		$razao =  nfce($dadosConfiguracaoNfe[0]["razaoSocial"]);
		$endereco =  nfce($dadosConfiguracaoNfe[0]["logradouro"]);
		$numero =  nfce($dadosConfiguracaoNfe[0]["numero"]);
		$complemento = nfce($dadosConfiguracaoNfe[0]["complemento"]);
		$bairro =  nfce($dadosConfiguracaoNfe[0]["bairro"]);
		$cidade =  ($dadosConfiguracaoNfe[0]["cidade"]);
		$estado =  nfce(($dadosConfiguracaoNfe[0]["estado"]));
		$cep = soNumeros($dadosConfiguracaoNfe[0]["cep"]);
		$fone = soNumeros(($dadosConfiguracaoNfe[0]["telefone"]));
		$ie = soNumeros($dadosConfiguracaoNfe[0]["inscricaoEstadual"]);
		$crtEmitente = soNumeros($dadosConfiguracaoNfe[0]["crt"]);
		$cnaeEmitente = soNumeros($dadosConfiguracaoNfe[0]["cnae"]);
		$usuarioPrefeitura = $dadosConfiguracaoNfe[0]["usuarioPrefeitura"];
		$senhaPrefeitura = $dadosConfiguracaoNfe[0]["senhaPrefeitura"];
		$aliquotaIss = $dadosConfiguracaoNfe[0]["aliquotaIss"];
		$serieNfse = $dadosConfiguracaoNfe[0]["serieNfse"];
		$regimeEspecialTributacao = $dadosConfiguracaoNfe[0]["regimeEspecialTributacao"];
		//Cliente
		$cpfcliente = soNumeros($dadosCliente[0]["documento"]);	
		$nomecliente =  nfce($dadosCliente[0]["nomeCliente"]);	
		$enderecocliente =  nfce($dadosCliente[0]["rua"]);
		$numerocliente =  nfce($dadosCliente[0]["numero"]);
		$complementocliente = nfce($dadosCliente[0]["complemento"]);
		$bairrocliente =  nfce($dadosCliente[0]["bairro"]);
		$codigoMunicipioCliente = buscaCodigoMunicipio(nfce($dadosCliente[0]["cidade"]),estadoToUf($dadosCliente[0]["estado"]));
		$cidadecliente =  nfce($dadosCliente[0]["cidade"]);
		$estadocliente =  (estadoToUf($dadosCliente[0]["estado"]));
		$cepcliente = soNumeros($dadosCliente[0]["cep"]);
		$fonecliente = soNumeros($dadosCliente[0]["telefone"]);
    //$ieCliente = soNumeros($dadosCliente[0]["inscricaoEstadual"]);
    $ieCliente = soNumeros($dadosCliente[0]["rg"]);
    $indicacaoIeCliente = !empty($ieCliente) ? "1" : "9"; //1-tem inscrição | 9 - não tem inscrição
    $indFinal = '0'; //cnpj
    if(strlen($cpfcliente)<12){ //se for cpf
      $indFinal = '1'; //consumidor final
      $ieCliente = '';//soNumeros($dadosCliente[0]["inscricaoEstadual"]);
      $indicacaoIeCliente = "9"; //1-tem inscrição | 9 - não tem inscrição
    }

    $idDest = "1"; //venda dentro estado
    if($estadocliente!=$dadosConfiguracaoNfe[0]["estado"]){
      $idDest = "2";
    }
    
		$nomeServico = array();
		$descricaoServico = array();
		$precoServico = (float)0;
		foreach ($dadosServicoOs as $v) {
			$nomeServico[] = $v["nomeServico"];
			$descricaoServico[] = $v["descricaoServico"];
			$precoServico += $v["precoServico"];

			$dadosServico = selecionar("SELECT * FROM servicos WHERE idServicos = ".$v["idServico"]);
		}

		$nomeServico = implode(', ', $nomeServico);
		$descricaoServico = implode(', ', $descricaoServico);

		//Servico
		$servicoNome = nfce(substr($nomeServico, 0, 100));
		$servicoDescricao = nfce(substr($descricaoServico, 0, 100));
		$servicoPreco = float4($precoServico);
		$itemListaServico = $dadosServico[0]["itemListaServico"];
		$codigoTributacaoMunicipio = soNumeros($dadosServico[0]["codigoTributacaoMunicipio"]);
    
    //retirar isso quando tiver o campo
    if(empty($codigoTributacaoMunicipio) and $idFranquia =='150') $codigoTributacaoMunicipio = '140200100';
    //*******
    
    
    
    $issRetido = $dadosConfiguracaoNfe[0]["issRetido"]; //sempre o mesmo
    //caso especial que aplica issRetido = 2 para venda externa e 1 para venda interna se for cnpj
    if($dadosConfiguracaoNfe[0]["issRetido"]=="99"){
      if(($codigoMunicipioCliente==$codmunicipio) and (strlen($cpfcliente)>12)){
        $issRetido = '1'; // 1- retido
      }else{
        $issRetido = '2'; // 2 - normal
      }
    }

		$urlIntegracao    = 'http://www.agilcontabil.net/sistemaInstalado/ajax';
		$dados['usuario'] = $dadosConfiguracaoNfe[0]["nomeAgil"];
		$dados['senha']   = $dadosConfiguracaoNfe[0]["senhaAgil"];
		$dados['acao']    = 'emitirNfseA3';
		//dados da nota fiscal de serviços
		$nfse = ["numeroNfse"=>$numeroNf,
						 "numeroLote"=>$numeroNf,
						 "serie"=>$serieNfse,         
             "regimeEspecialTributacao" =>$regimeEspecialTributacao, //( retNenhum, retMicroempresaMunicipal, retEstimativa, retSociedadeProfissionais, retCooperativa, retMicroempresarioIndividual, retMicroempresarioEmpresaPP ); 
						 "ambiente"=>$ambienteNfe, //1=produção, 2=homologação
						 "simplesNacional"=> $crtEmitente=="1" ? "1" : "2", //1=sim, 2=não
						 "tipo"=>"1", //(1= trRPS, 2=trNFConjugada, 3=trCupom );  
						 "naturezaOperacao"=>$dadosConfiguracaoNfe[0]["naturezaOperacao"], //(1=no1, 2=no2, 3=no3, 4=no4, 5=no5, 6=no6, 7=no7, 8=no9, 9=no11, 10=no12, 11=no14,
																//12=no50, 13=no51, 14=no52, 15=no53, 16=no54, 17=no55, 18=no56, 19=no57, 20=no58, 21=no59,
																//22=no60, 23=no61, 24=no62, 25=no63, 26=no64, 27=no65, 28=no66, 29=no67, 30=no68, 31=no69,
																//32=no70, 33=no71, 34=no72, 35=no78, 36=no79, 37=no101, 38=no111, 39=no121, 40=no201, 41=no301,
																//42=no501, 43=no511, 44=no541, 45=no551, 46=no601, 47=no701 );  
						 "incentivadorCultural"=>"2", //1=sim, 2=não 
						 "codigoObra"=>"",
						 "codigoArt"=>"",
						 "servico"=>[
												 "valor"                 =>float2($servicoPreco),
												 "deducoes"              =>0.00,
												 "pis"                   =>0.00,
												 "cofins"                =>0.00,
												 "inss"                  =>0.00,
												 "ir"                    =>0.00,
												 "csll"                  =>0.00,
												 "issRetido"             =>$issRetido, //1=stRetencao(sim), 2=stNormal(não), 3=stSubstituicao
												 "valorIssRetido"        =>$issRetido=="1" ? float2($servicoPreco*$aliquotaIss) : 0.00,
												 "outrasRetencoes"       =>0.00,
												 "descontoIncondicionado"=>0.00,
												 "descontoCondicionado"  =>0.00,
												 "aliquota"              =>float4($aliquotaIss),
												 "responsavelRetencao"   =>1, //1=ptTomador, 2=rtPrestador
												 "itemListaServico"      =>$itemListaServico,
												 "codigoTributacaoMunicipio"=>$codigoTributacaoMunicipio,
												 "codigoCnae"            => $cnaeEmitente,
												 "discriminacao"         => $servicoNome,
												 "codigoMunicipio"       => $codmunicipio, //código do município onde ocorreu o fato gerador do serviço (geralmente o mesmo do prestador do serviço)
												 "municipioIncidencia"   => $codmunicipio,
												 "exigibilidadeISS"      => 1 // 1=exiExigivel, 2=exiNaoIncidencia, 3=exiIsencao, 4=exiExportacao, 5=exiImunidade, 6=exiSuspensaDecisaoJudicial, 7=exiSuspensaProcessoAdministrativo
												],
						 "prestador"=>[
													 "cnpj"               =>$cnpj,
													 "inscricaoMunicipal" =>$inscricaoMunicipal,
													 "endereco"           =>$endereco,
													 "numero"             =>$numero,
													 "complemento"        =>$complemento,
													 "bairro"             =>$bairro,
													 "municipio"          =>$codmunicipio,
													 "uf"                 =>$estado,
													 "cep"                =>$cep,
													 "razaoSocial"        =>$razao,
													 "nomeFantasia"       =>$razao,
													 "telefone"           =>$fone,
													 "email"              =>"contato@redemultiassistencia.com.br",
													 "usuario"            =>$usuarioPrefeitura,
													 "senha"              =>$senhaPrefeitura,
													 "fraseSecreta"       =>"",
													 "codigoUf"           =>$cUF
													],
						 "tomador"=>[
												 "cpfCnpj"            =>$cpfcliente,
												 "inscricaoMunicipal" =>"",
												 "endereco"           =>$enderecocliente,
												 "numero"             =>$numerocliente,
												 "complemento"        =>$complementocliente,
												 "bairro"             =>$bairrocliente,
												 "municipio"          =>$codigoMunicipioCliente,
												 "uf"                 =>$estadocliente,
												 "cep"                =>$cepcliente,
												 "razaoSocial"        =>$nomecliente,
												 "telefone"           =>$fonecliente,
												 "email"              =>""
												]
						];

		if(!empty($_REQUEST["mostraDados"])){
			echo "<pre>";
			var_dump($nfse);
			echo "</pre>";
			die();
		}

		$dados['nfse']   = base64_encode(json_encode($nfse)); //os dados da nfse devem ser enviados em formato json e base64
			//Inicia comunicação com servidor agilcontabil.net
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $urlIntegracao);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados));
			//recebe a resposta
			$resposta = curl_exec($ch);

			//finaliza comunicação
			curl_close($ch);
		//mostra a resposta da emissão da nota
		//o xml da nota fiscal emitida está dentro da variavel $resposta["xml"] e deve ser gravado em sua base de dados

		//mostra a resposta da emissão da nota
		//o xml da nota fiscal emitida está dentro da variavel $resposta["xml"] e deve ser gravado em sua base de dados
		if(!empty($_REQUEST["mostraResposta"])){
			echo $resposta;
			echo "<br>";
			echo "<pre>";
			print_r(json_decode($resposta,true));
			echo "</pre>";
			echo '<textarea rows="40" cols="100">';
			echo base64_decode($array["xml"]);
			echo "</textarea>";
			die();
		}
		//die();
		$array = json_decode($resposta,true);
		if(!empty($array["cStat"]) and $array["cStat"]=="100"){ //sucesso

			//grava dados de sucesso
				//grava xml de resposta na pasta nfe/aprovadas
				file_put_contents("../assets/arquivos/nfse/xml/nfse".$numeroNfe.".xml",base64_decode($array["xml"]));
				//grava pdf
				$pdf = hex2bin($array["pdf"]);
				file_put_contents("../assets/arquivos/nfse/danfe/nfse".$numeroNfe.".pdf",$pdf);

				//grava a chave da nfe na venda
				$campos = array("numeroNfse");
				$valores = array($numeroNfe);
				$result = alterar("os",$campos,$valores,"idOs = ".$_GET["idOs"]);

				//altera número da ultima nota emitida na empresa
				if($dadosConfiguracaoNfe[0]["producao"]=="1"){

					$campos = array("numeroNFSEProducao");
				}
				else{

					$campos = array("numeroNFSEHomologacao");
				}

				$valores = array($numeroNfe);
				$result = alterar("configuracao_nota_fiscal", $campos, $valores, "idFranquia = ".$idFranquia);
				//die();
			//**********************
			?>
			<object data="data:application/pdf;base64,<?php echo base64_encode(hex2bin($array["pdf"])); ?>" type="application/pdf" width="100%" height="800px"></object>
			<?php
		}//if !empty(xml)
		else{
			//mostra resposta de erro
			echo "<h3>Erro ao emitir nota fiscal de servi&ccedil;o</h3><br>";
			echo "<pre>";
			print_r(json_decode($resposta,true));
			echo "</pre>";
			echo "<pre>";
			print_r($nfse);
			echo "</pre>";
		}
		
		
	}//else já emitida


}//if Serviço

function zeroEsquerda($valor,$quantidade){

	return str_pad($valor, $quantidade, '0', STR_PAD_LEFT);
}

function buscaCodigoMunicipio($municipioX,$ufX){
	$municipioDados = selecionar("SELECT * FROM municipio WHERE nome = '".trim($municipioX)."' AND sigla = '".trim($ufX)."';");  
	if(!empty($municipioDados))
		return $municipioDados[0]["codigo"];
	else{
		return "";
	}
}

/*Calcula dígito verificador do município*/
function calcula_dv_municipio($codigo_municipio){

	$peso = "1212120";
	//echo "".substr($peso,0,1)."";
	$soma = 0;
	for($i = 0; $i < 7; $i++){
	 $valor = substr($codigo_municipio,$i,1) * substr($peso,$i,1); if($valor>9)
	$soma = $soma + substr($valor,0,1) + substr($valor,1,1);
	else
	$soma = $soma + $valor;
	}

	$dv = (10 - ($soma % 10));
	if(($soma % 10)==0)
	$dv = 0;
	return $dv;
}

function float2($valor){
 //transforma para: 10.00{

	return number_format((float)$valor,2,".","");
}

function float4($valor){
  //transforma para: 10.0000
	return number_format((float)$valor,4,".","");
}

function ufToCodigo($uf){

	if($uf=="RO") return "11";
	if($uf=="AC") return "12";
	if($uf=="AM") return "13";
	if($uf=="RR") return "14";
	if($uf=="PA") return "15";
	if($uf=="AP") return "16";
	if($uf=="TO") return "17";
	if($uf=="MA") return "21";
	if($uf=="PI") return "22";
	if($uf=="CE") return "23";
	if($uf=="RN") return "24";
	if($uf=="PB") return "25";
	if($uf=="PE") return "26";
	if($uf=="AL") return "27";
	if($uf=="SE") return "28";
	if($uf=="BA") return "29";
	if($uf=="MG") return "31";
	if($uf=="ES") return "32";
	if($uf=="RJ") return "33";
	if($uf=="SP") return "35";
	if($uf=="PR") return "41";
	if($uf=="SC") return "42";
	if($uf=="RS") return "43";
	if($uf=="MS") return "50";
	if($uf=="MT") return "51";
	if($uf=="GO") return "52";
	if($uf=="DF") return "53";
}
//ufToCodigo
function estadoToUf($estado){

	$uf = nfce($estado);
	
	switch($estado){

		case "Rondonia": $uf = "RO"; break;
		case "Acre": $uf = "AC"; break;
		case "Amazonas": $uf = "AM"; break;
		case "Roraima": $uf = "RO"; break;
		case "Para": $uf = "PP"; break;
		case "Amapa": $uf = "AP"; break;
		case "Tocantins": $uf = "TO"; break;
		case "Maranhao": $uf = "MA"; break;
		case "Piauí": $uf = "PI"; break;
		case "Ceara": $uf = "CE"; break;
		case "Rio Grande do Sul": $uf = "RN"; break;
		case "Paraiba": $uf = "PB"; break;
		case "Pernambuco": $uf = "PE"; break;
		case "Alagoas": $uf = "AL"; break;
		case "Sergipe": $uf = "SE"; break;
		case "Bahia": $uf = "BA"; break;
		case "Minas Gerais": $uf = "MG"; break;
		case "Espirito Santo": $uf = "ES"; break;
		case "Rio de Janeiro": $uf = "RJ"; break;
		case "São Paulo": $uf = "SP"; break;
		case "Parana": $uf = "PR"; break;
		case "Santa Catarina": $uf = "SC"; break;
		case "Rio Grande do Sul": $uf = "RS"; break;
		case "Mato Grosso do Sul": $uf = "MS"; break;
		case "Mato Grosso": $uf = "MT"; break;
		case "Goias": $uf = "GO"; break;
		case "Distrito Federal": $uf = "DF"; break;
	}

	
	return $uf;
}

function limita_caracteres($texto, $limite, $quebra = true){

   $tamanho = strlen($texto);
   if($tamanho <= $limite){
    //Verifica se o tamanho do texto é menor ou igual ao limite
      $novo_texto = $texto;
   }
   else{
    // Se o tamanho do texto for maior que o limite
      if($quebra == true){
       // Verifica a opção de quebrar o texto
         $novo_texto = trim(substr($texto, 0, $limite))."...";
      }
      else{
       // Se não, corta $texto na última palavra antes do limite
         $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
         $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada
      }

   }

   return $novo_texto; // Retorna o valor formatado
}

function nfce($string){

  $string = str_replace('?','a',$string);
  $string = str_replace('?','A',$string);
  $string = str_replace('?','a',$string);
  $string = str_replace('?','A',$string);
  $string = str_replace('?','a',$string);
  $string = str_replace('?','A',$string);
  $string = str_replace('?','a',$string);
  $string = str_replace('?','A',$string);
  $string = str_replace('?','c',$string);
  $string = str_replace('?','C',$string);
  $string = str_replace('?','e',$string);
  $string = str_replace('?','E',$string);
  $string = str_replace('?','e',$string);
  $string = str_replace('?','E',$string);
  $string = str_replace('?','e',$string);
  $string = str_replace('?','E',$string);
  $string = str_replace('?','i',$string);
  $string = str_replace('?','I',$string);
  $string = str_replace('?','o',$string);
  $string = str_replace('?','O',$string);
  $string = str_replace('?','o',$string);
  $string = str_replace('?','O',$string);
  $string = str_replace('?','o',$string);
  $string = str_replace('?','O',$string);
  $string = str_replace('?','u',$string);
  $string = str_replace('?','U',$string);
	
  $string = utf8_encode($string);
	
  $string = str_replace('~','',$string);
  $string = str_replace('&','e',$string);
  $string = str_replace('.','',$string);
  $string = str_replace('-','',$string);
  $string = str_replace(',','',$string);
  $string = str_replace(';','',$string);
  $string = str_replace(':','',$string);
  $string = str_replace('(','',$string);
  $string = str_replace(')','',$string);
  $string = str_replace('/','',$string);
  $string = str_replace('ç','c',$string);
  $string = str_replace('ã','a',$string);
  $string = str_replace('ê','e',$string);
  $string = str_replace('í','i',$string);
	//$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	
	return $string;
}

function icms($preco, $icms){

	$valor = $preco; // valor original
	$percentual = $icms / 100.0; // 8%
	$valor_final = $valor - ($percentual * $valor);
	$creditoicms = $valor - $valor_final;
	return $creditoicms;
}

function geraCN($length=8){

  $numero = '';    
  for ($x=0;$x<$length;$x++){

	  $numero .= rand(0,9);
  }

  return $numero;
}

function calculaDV($chave43){

  $multiplicadores = array(2,3,4,5,6,7,8,9);
  $i = 42;
  $soma_ponderada = 0;
  while ($i >= 0) {

	  for ($m=0; $m<count($multiplicadores) && $i>=0; $m++) {

		  $soma_ponderada+= $chave43[$i] * $multiplicadores[$m];
		  $i--;
	  }

  }

  $resto = $soma_ponderada % 11;
  if ($resto == '0' || $resto == '1') {

	  return 0;
  }
   else {

	  return (11 - $resto);
 }

}

function clear_tags($str){

	return htmlentities(
		strip_tags($str,
			'<p>'
		),
		ENT_QUOTES | ENT_XHTML | ENT_HTML5,
		'UTF-8'
	);
}

function decode_html($str){

	return html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
}

function soNumeros($texto){

	return preg_replace("/[^0-9]/","",$texto);
}
//sonumeros
//funções uteis fim
//*************************