<?
/**
* Conecta com o banco de dados
*
* @return object - Retorna um objeto que representa a conexão com um servidor MySQL .
*/
// Alterando caminho para localhost
//echo 'Caminho: '.$_SERVER['DOCUMENT_ROOT'].'/_framework/redemulti/sistema/application/config/database.php';
//include($_SERVER['DOCUMENT_ROOT'].'/sistema/application/config/database.php');
include($_SERVER['DOCUMENT_ROOT'].'/_framework/redemulti/sistema/application/config/database.php');

function  conectaDB(){	
	$db = $GLOBALS['db'];
	$activedb  = $db["default"];
	//var_dump($activedb);
	//die();
	$host      = $activedb['hostname']; // IP DO SERVIDOR MYSQL
	$dbstr     = $activedb['database']; // NOME DO BANCO DE DADOS
	$uid       = $activedb['username']; // USUÁRIO
	$psw       = $activedb['password']; // SENHA
	
	$db2 = mysqli_connect($host , $uid, $psw, $dbstr);
	if(mysqli_connect_errno()) printf("A conexão falhou: %s\n", mysqli_connect_error());
	return $db2;
}