<?php

class Slotter
{
    private $nichos_pequenos = [];
    private $nichos_medios = [];
    private $nichos_grandes = [];

    /**
     * Inicializa quantos nichos existem
     * @param int $qtd_pequeno Quantidade de nichos Pequenos disponíveis
     * @param int $qtd_medio Quantidade de nichos Médios disponíveis
     * @param int $qtd_grande Quantidade de nichos Grandes disponíveis
     * 
     * @return void
     */
    public function __construct(
        $qtd_pequeno = 2
        , $qtd_medio = 2
        , $qtd_grande = 2
        , $num_slots_p = 2
        , $num_slots_m = 2
        , $num_slots_g = 2
    ) {

        $this->aloca_nichos($qtd_pequeno, $qtd_medio, $qtd_grande, $num_slots_p, $num_slots_m, $num_slots_g);
        // TESTING
        // echo '<pre>';
        // var_dump($this->nichos_pequenos);
        // var_dump($this->nichos_medios);
        // var_dump($this->nichos_grandes);
        // exit;
    }
    /**
     * Aloca em memória quantos nichos existem
     * @param int $qtd_medio Quantidade de nichos Médios disponíveis
     * @param int $qtd_grande Quantidade de nichos Grandes disponíveis
     */
    private function aloca_nichos($qtd_pequeno, $qtd_medio, $qtd_grande, $num_slots_p, $num_slots_m, $num_slots_g)
    {
        /*
        gera um array com a seguinte estrutura:
        $variacao_letra_pequena = [
            0 = 'A', 
            1 = 'B', 
            2 = 'C', 
            3 = 'D', 
            ...
        ]
        */
        $variacao_letra_pequena = range('A', echoCorrespondentLetter($qtd_pequeno));
        $variacao_letra_media   = range('A', echoCorrespondentLetter($qtd_medio));
        $variacao_letra_grande  = range('A', echoCorrespondentLetter($qtd_grande));

        /*
        gera um array com a seguinte estrutura:
        $nicho = [
            'P1' = ['0','0','0','0','0','0','0','0',]
            'P2' = ['0','0','0','0','0','0','0','0',]
            ...
            ...
            'M1' = ['0','0','0','0','0','0','0','0',]
            'M2' = ['0','0','0','0','0','0','0','0',]
            ...
            ...
            'G1' = ['0','0','0','0','0','0','0','0',]
            'G2' = ['0','0','0','0','0','0','0','0',]
            
            P = PEQUENO
            M = MÉDIO
            G = GRANDE
        ]
        
         */
        for ($i = 1, $v = 0; $i <= $qtd_pequeno; $i++, $v++) {
            $this->nichos_pequenos[$variacao_letra_pequena[$v]] = array_fill(0, $num_slots_p, '0');
        }

        for ($i = 1, $z = 0; $i <= $qtd_medio; $i++, $z++) {
            $this->nichos_medios[$variacao_letra_media[$z]] = array_fill(0, $num_slots_m, '0');
        }
        
        for ($i = 1, $m = 0; $i <= $qtd_grande; $i++, $m++) {
            $this->nichos_grandes[$variacao_letra_grande[$m]] = array_fill(0, $num_slots_g, '0');
        }
    }

    /**
     * reserva slots disponíveis em um nicho
     * 
     * @param string $tam_nicho 'medio' ou 'grande'
     * @param int $qtd_slots Quantos slots precisa?
     * @param string $tipo_de_equip Qual o tipo de equipamento será alocado: 'celular', 'notebook', 'tablet', 'tv' ou 'pc'?
     * @param int $id_equip ID do equipamento que será alocado
     */
    // public function reserva_slots_nicho(string $tam_nicho, int $qtd_slots)
    public function reserva_slots_nicho($tam_nicho, $qtd_slots, $tipo_de_equip, $id_equip)
    {
        $num_alocados = 0;

        if ($tam_nicho == 'pequeno' && $tipo_de_equip == 'celular') {

            $nichos = &$this->nichos_pequenos;

        } else if($tam_nicho == 'medio' && ($tipo_de_equip == 'notebook' || $tipo_de_equip == 'tablet' || $tipo_de_equip == 'video game') ) {

            $nichos = &$this->nichos_medios;

        } else {

            // nichos grandes recebem tv e computador
            $nichos = &$this->nichos_grandes;

        }

        foreach($nichos as $cod_nicho => &$slots):
            if ($num_alocados == $qtd_slots) {
                break;
            }
            
            foreach($slots as $letra => &$slot):
                if ($num_alocados == $qtd_slots) {
                    break;
                }

                // echo '<pre>';
                // var_dump($nichos);
                // // var_dump($letra);
                // echo '</pre>';
                // exit;

                // Se está vazio, aloca
                if ($slot == '0') {

                    $slot = '1';

                    // $nichos[$cod_nicho][$letra][3] = '1978';
                    // $nichos[$cod_nicho][$letra] = '1978';
                    $nichos[$cod_nicho][$letra] = $id_equip;
                    // break 2;

                    $num_alocados++;
                }

                // var_dump($this->nichos_pequenos, $cod_nicho . $letra); // pegar $cod_nicho . $letra e gravar em equipamento_os
                // exit;

                // foreach($slot as $posicao_slot => $id_equip):

                //     if($id_equip == '1978'){
                //         $letra_alf = $letra;
                //         $posicao = $posicao_slot;
                //         break 3;
                //     }

                // endforeach;
            endforeach;
        endforeach;

        // echo '<pre>';
        // // var_dump($this->nichos_pequenos, $cod_nicho . $letra); // pegar $cod_nicho . $letra e gravar em equipamento_os
        // var_dump($this->nichos_pequenos, $this->nichos_medios); // pegar $cod_nicho . $letra e gravar em equipamento_os
        // exit;

        // // refazer essa gambi com array_walk ou array_map
        // foreach ($nichos as $cod_nicho => &$slots) {
        //     if ($num_alocados == $qtd_slots) {
        //         break;
        //     }

        //     // Varre os slots e os aloca
        //     foreach ($slots as &$slot) {
        //         if ($num_alocados == $qtd_slots) {
        //             // echo 'Num Alocado!! ' . $num_alocados . ' x ' . $qtd_slots . ' qtde slot <br>';
        //             break;
        //         }
        //         // Se está vazio, aloca
        //         if ($slot == '0') {
        //             $slot = '1';
        //             // $slot = '134';

        //             $num_alocados++;
        //         }
        //     }
        // }
    }

    /**
     * Mostra nichos pequenos e seus conteúdos
     */
    public function echo_nichos_pequenos()
    {

        // echo '<pre>';
        // var_dump($this->nichos_pequenos);
        // echo '</pre>';
        // exit;

        foreach ($this->nichos_pequenos as $nicho => $conteudo) {

            // var_dump($nicho);

            $os = 123456789;
            $name = 'teste';
            $tipo = 'celular';

            echo "<tr>";

            echo '<td class="letter-column">' . $nicho . "</td>";

            foreach ($conteudo as $slot) {

                echo '<td class="itembg1 1 P">';

                // Se está vazio...
                if ($slot == '0'):

                    // echo '<i class="fas fa-check-square green"></i>';

                else:

                    // echo '<i class="fas fa-mobile-alt red"></i>';

                    echo "\t\t\t\t\t\t\t\t\t\t" . '<div class="divimg">'  . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a class="os-link" href="editar/'.$os.'">' . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<img class="tip-bottom iconimg" src="'. base_url() . echoEquipmentTypeImage($tipo) . '" ';

                    echo 'data-original-title="Equipamento:&nbsp;'.$name.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$os.')">' . "\n";
                    
                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n";
                
                    echo "\t\t\t\t\t\t\t\t\t\t" . '</div>' . "\n";

                endif;

                echo "</td>";

            }

            echo "</tr>";

        }

        // echo '<pre>';
        // var_dump($this->nichos_pequenos);
        // echo '</pre>';

    }

    /**
     * Mostra nichos médios e seus conteúdos
     */
    public function echo_nichos_medios()
    {
        foreach ($this->nichos_medios as $nicho => $conteudo):

            $os = 1212;
            $name = 'teste2';
            $tipo = 'notebook';

            echo "<tr>";

            echo '<td class="letter-column">' . $nicho . "</td>";

            foreach ($conteudo as $slot):

                echo '<td class="itembg1 1 M">';

                // Se está vazio...
                if ($slot == '0'):

                    // echo '<i class="fas fa-check-square green"></i>';

                else:

                    // echo '<i class="fas fa-mobile-alt red"></i>';

                    echo "\t\t\t\t\t\t\t\t\t\t" . '<div class="divimg">'  . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a class="os-link" href="editar/'.$os.'">' . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<img class="tip-bottom iconimg" src="'. base_url() . echoEquipmentTypeImage($tipo) . '" ';

                    echo 'data-original-title="Equipamento:&nbsp;'.$name.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$os.')">' . "\n";
                    
                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n";
                
                    echo "\t\t\t\t\t\t\t\t\t\t" . '</div>' . "\n";

                endif;

                echo "</td>";

            endforeach;

            echo "</tr>";

        endforeach;
    }

    /**
     * Mostra nichos médios e seus conteúdos
     */
    public function echo_nichos_grandes()
    {
        foreach ($this->nichos_grandes as $nicho => $conteudo):

            $os = 8976;
            $name = 'teste3';
            $tipo = 'tv';

            // var_dump($this->nichos_grandes);

            echo "<tr>";

            echo '<td class="letter-column">' . $nicho . "</td>";

            foreach ($conteudo as $slot):

                // var_dump($slot);

                echo '<td class="itembg1 1 G">';

                // Se está vazio...
                if ($slot == '0'):

                    // echo '<i class="fas fa-check-square green"></i>';

                else:

                    // echo '<i class="fas fa-mobile-alt red"></i>';

                    echo "\t\t\t\t\t\t\t\t\t\t" . '<div class="divimg">'  . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a class="os-link" href="editar/'.$os.'">' . "\n";

                    echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<img class="tip-bottom iconimg" src="'. base_url() . echoEquipmentTypeImage($tipo) . '" ';

                    echo 'data-original-title="Equipamento:&nbsp;'.$name.'&#09;Clique&nbsp;pra&nbsp;ver&nbsp;OS&nbsp;(#'.$os.')">' . "\n";
                    
                    echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n";
                
                    echo "\t\t\t\t\t\t\t\t\t\t" . '</div>' . "\n";

                endif;

                echo "</td>";

            endforeach;

            echo "</tr>";

        endforeach;
    }
    // REPETIR PARA NICHOS GRANDES OU ADAPTAR PARA REUSO

    /**
     * Mostra o resultado final do array
      */
    public function __destruct()
    {
        // echo '<pre>';
        // echo "<h6>DEBUGGING - array final </h6>";
        // print_r($this->nichos_medios);
        // echo '</pre>';
    }
}
