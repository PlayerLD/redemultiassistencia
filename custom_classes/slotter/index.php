<?php

require_once("Slotter.php");
require_once("../laboratorios/functions.php");

$slot = new Slotter(2, 1, 8, 5, 7); //$qtd_medio = 2, $qtd_grande = 2, $num_slots = 10

// Fazer aqui via GET/POST, whatever
$slot->reserva_slots_nicho('medio', 2);
$slot->reserva_slots_nicho('grande', 1);
// $slot->reserva_slots_nicho('medio', 1, 'celular', 123);
// $slot->reserva_slots_nicho('grande', 1, 'notebook', 410);

// $m = 3;
// $qtd_medio = 2;

// $numbers = range(1, $m);
// $letters_range = range('A', echoCorrespondentLetter($qtd_medio));
// // echo '<pre>';
// // print_r( range('A', 'Z') );
// // exit;
// $array_m = array_fill(0, $m, '0'); // a partir do zero, preencha 8 posicoes, com a string '0'
// $array_g = array_fill(0, $m, '0'); // a partir do zero, preencha 8 posicoes, com a string '0'
// $array2 = [0 => ['A' => $array_m, 'B' => $array_m]];
// // $array2 = [0 => ['B' => $array_g]];

// $array2[0]['A'][3] = '1978';
// $array2[0]['B'][2] = '145';

// $letra_alf = null;
// $posicao = null;



// foreach($array2 as $key => $ar):
//     foreach($ar as $letra => $arr):
//         foreach($arr as $posicao_slot => $id_equip):
//             if($id_equip == '1978'){
//                 $letra_alf = $letra;
//                 $posicao = $posicao_slot;
//                 break 3;
//             }
//         endforeach;
//     endforeach;
// endforeach;

// // foreach($array as $key => $ar):
// //     if($ar == '1'){
// //         $posicao = $key;
// //         break;
// //     }
// // endforeach;

// echo '<pre>';
// echo( $letra_alf . $posicao );
// // var_dump( $numbers );
// // var_dump( $array2 );
// echo '<br>';
// print_r( $letters_range );

// echo '</pre>';

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>SLOTS LABORATÓRIO</title>
  </head>
  <body>
    <style>
        .green {
            color: green;
            padding: 1px;
            font-size: 25px;
        }

        .red {
            color: red;
            padding: 1px;
            font-size: 25px;
        }
    </style>

  	<div class="container">
		<div class="row">
			<div class="col-lg-12">

			    <h1>Tabajara Slot Alocator</h1>

			
				<table id="table2" align="center" class="table">
				    <tbody>
                        <tr>
                            <th>Nicho</th>
                            <th>Conteúdo</th>
                        </tr>
                            
                        <?php
                            $slot->echo_nichos_medios();
                        ?>
                            
                        <?php
                            $slot->echo_nichos_grandes();
                        ?>

				    </tbody>
				</table>

			</div>

		</div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>


